#ifndef MODULES_H
#define MODULES_H

#include "math.h"
#include "qglobal.h"
#include "qlist.h"
#include "qvector.h"
#include "qcustomplot.h"
#include <QMediaPlayer>
#include <QWidget>

class Modules
{
public:
    Modules();
};


class GainSettings
{
public:
    GainSettings();

    enum channelType {
        LfPort,
        LfStbd,
        HfPort,
        HfStbd
    };

    double calculateTvg(int sampleNum, double resolutionLat, channelType type, bool findMax = false, bool clipGain = 0);
    double calculateTvg(double distance, channelType type, bool findMax = false, bool clipGain = 0);
    double calculateBValue(channelType type, double maxRange);

    double tvg_ALfP=10, tvg_CLfP=0;
    double tvg_ALfS=10, tvg_CLfS=0;
    double tvg_AHfP=10, tvg_CHfP=0;
    double tvg_AHfS=10, tvg_CHfS=0;
    int tvg_ALfPInd=0, tvg_ALfSInd=0, tvg_AHfPInd=0, tvg_AHfSInd=0;
    double tvg_ratioLfP = 1, tvg_ratioHfP = 1;
    double tvg_BLf=0, tvg_BHf=0;
    double tvg_T = 10;
    bool tvg_on = true, tvg_syncOn = true;
    int gainSliderIndexLf = 0, gainSliderIndexHf = 0;
    int gainSliderIndexDepth = 0;
    int balanceSliderIndex = 0, balanceSliderIndex_HF = 0;
    bool calculateBLf = false, calculateBHf = false;
    int tvg_maxLf = 80, tvg_maxHf = 80;
    int tvg_minLf = 0, tvg_minHf = 0;
    double tvg_frequencyLow = 0, tvg_frequencyHigh = 0;
    double starGain = 1, portGain = 1;
    double starGainHF = 1;
    double portGainHF = 1;
    double C_lfOffset = 0;
    double C_hfOffset = 0;
    double C_depthOffset = 0;
    int DigitalGainIndex = 0;
};

class SonarPing
{
public:
    SonarPing();

    // Keep all global variables related to the ping header in here to make debugging easier
    quint32 firmwareVer = 0;
    quint16 lowFrequency = 0, highFrequency = 0, depthFrequency = 0;
    quint16 angleOrientation=90;
    bool recieveDepth = false;
    bool Oct31ASTest = 0;
    int samplesToPlotLF = 0, samplesToPlotHF = 0, samplesToPlotDepth = 0;
    quint16 rangeLf_tmp = 0, rangeHf_tmp = 0;
    double rangeLf_double = 60, rangeHf_double = 60; //for correct values of zoom
    int rangeLf_int = 0, rangeHf_int = 0;
    quint32 pingNumFirst = 0, pingNumCurrent = 0, pingNumEnd = 0;
    double degree = 90;
    QVector<float> nav;
    quint32 pingHeaderSize;

    float roll = 0, lastTraceRoll = 0;
    float pitch = 0, lastTracePitch = 0;
    float Depth = 0, lastTraceAlt = 0;
    float temperature = 0;
    float lastTracedyaw = 0;
    float heading = 0;
    double altitudeFactor = 1;
    double y_LowerBound = 0;
    double y_UpperBound = 0;
    double y_boatPos = 0;

    double res_fwd = 0;
    double res_lat_lf = 0;
    double res_fwd_hf = 0;
    double res_lat_hf = 0;
    double lastTracePos = 0;
    double currentSamplingFrequency = 0;
    double freq_rep = 10;
    bool bottomTrackDetected = false;

    int firstRtcTime = 0;

    QList <quint16> data_portLf, data_portHf;
    QList <quint16> data_stbdLf, data_stbdHf;
    QList <quint16> data_depth;
};

class GnssController
{
public:
    void updateCoords(double Lat, double Lon, int Ping);
    void updateTime(short Hour, short Minute, short Second);
    void updateDate(short Day, short Month, int Year);
    void updateSpeed(double Val);
    void updateHeading(double Val);

    void reset();

    // Getter functions
    double Latitude();
    double Longitude();
    double Speed();
    double Heading();
    int Year();
    short Second();
    short Minute();
    short Hour();
    short Day();
    short Month();
    int Ping();

    bool validHeading = false;
    bool validSpeed = false;
    bool validCoords = false;
    bool validTime = false;
    int HeadingType = -1;
    int SpeedType = -1;

private:
    double latitude = -500, longitude = -500;
    double speed = -1, heading = -900;
    int year = 0;
    short month = 0, day = 0, hour = 0, minute = 0, second = 0;
    int currentPing = -1;
};

class Crosshair
{
public:
    void remove();
    void setVisible(bool state);
    void setPosition(double x, double y, double yDelta, double x_text, double y_text);
    bool isValid();
    void add(QCustomPlot *plotPtr);
    bool isVisible();

    QCPItemStraightLine *line_x = nullptr;
    QCPItemStraightLine *line_y = nullptr;
    QCPItemText *text_x = nullptr;
    QCPItemText *text_y = nullptr;
    QCustomPlot *parentPlot = nullptr;
};

class JasReader
{
public:
    JasReader();

};

struct PingMapItem
{
public:
    PingMapItem();
    PingMapItem(double y, int ping, QString time, double temp, float d, double alt, double r, double p, double Yaw, double Azimuth,
             double lat, double lon, double head, double Speed, double dist, double df);
    bool isValid = false;

    double yCoord = -1;
    int pingNumber = -1;
    QString rtcTime = "";
    float depth = -100;
    double temperature = 0;
    double altitude = -100;
    double roll = -500;
    double pitch = -500;
    double yaw = -500;
    double azimuth = 0;
    double altitudeFactor = 1;

    // For the GPS plot
    double lattitude = -500;
    double longitude = -500;
    double heading = -500;
    double speed = -1;
    double distance = -1;
};

class PingMap
{
public:
//    PingMap();
    void append(double yCoord, int ping, QString time, float depth, double alt, double roll,
            double pitch, double yaw, double Azimuth, double lat, double lon, double head, double Speed, double dist);
    void append(PingMapItem item);
    PingMapItem findItem(int key);
    PingMapItem findItemWithY(double value);
    PingMapItem last();
    int firstKey();
    int lastKey();
    void removeItems (int startKey, int endKey);
    void removeItemsWithY (double startY, double endY);
    void clearAll();
    bool containsPing(int pingNum);
    QMap<int, PingMapItem> dataMap;
    int size = 0;

};

class targetItem
{
public:
    targetItem();
    targetItem(int index, QString name, double Lat, double Lon, int PingNum, int SampleNum, double x, double y);
    // RenameTarget
    // SelectItem
    //

    void removeIconFromLfHfPlot();
    void removeIconFromAllPlots();
    void addIconToPlot(QCustomPlot *plotLf, QCustomPlot *plotHf, QCustomPlot *plotAlt, QCustomPlot *plotGps, QCustomPlot *plotSensor, int type);
    void resizeIcon(double width, double height, double x = 0, double y = -1);
    void setGpsLinePosition(QPointF center, QPointF port, QPointF stbd);
    void setAltItemPosition(double maxAlt, double x_text, double y);
    void setSensorItemPosition(double y);
    void showScanLines(bool state);

    void deleteItem();
    void selectItem(bool select, int type);
    void removeItemFromList();
    void deleteItemImage();
    void updateTargetIndex(int newIndex, QFont font);
    void renameTarget(QString newName);
    void copyTarget(QString newName);
    QString determineName(QString text, QFont font, int wptListLength = 0);
    QString determineAreaText();

    static QPixmap getIconPixmap(bool solidIcon, int type);
    QPen getAltLinePen(bool selected, int type);

    int listIndex = -1;
    bool selected = false;
    bool importedWaypoint = false;
    QString Name = ""; // Target name
    double Lattitude = -500;
    double Longitude = -500;
    double GpsHeading = 0;
    int ping=0;
    int sample=0;
    double xCoord=0;
    double yCoord=0; // -1 also means that icon is not on Lf/Hf plots
    int snippedImage = 0; // 0 = No snipped image, 1 = Snipped image on LF plot, 2 = on HF plot
    QString imageName = ""; // Keep track of file name of the saved snipped image
    QString imagePath = ""; // Full path of image file
    double imageHeight = 0, imageWidth = 0; // In meters
    double area = 0;
    double speed = 0;
    double heading = 0;
    double distance = 0;
    targetItem *baseWpt = nullptr;
    int baseWptNum = 0;

    QCPItemPixmap *icon_lf = nullptr;
    QCPItemPixmap *icon_chart = nullptr;
    QCPItemPixmap *icon_hf = nullptr;

    QCPItemText *text_lf = nullptr;
    QCPItemText *text_hf = nullptr;
    QCPItemText *text_chart = nullptr;
    QCPItemText *text_altNum = nullptr;
    QCPItemText *text_sensor = nullptr;

    QCPItemLine *line_alt = nullptr;
    QCPItemLine *line_chart_port = nullptr, *line_chart_stbd = nullptr;
    QCPItemStraightLine *line_sensor = nullptr;

    QListWidgetItem *ListItem = nullptr;

};

class waypointItem : public targetItem {
public:
    waypointItem();
    waypointItem(targetItem);
    waypointItem(int index, QString name, double Lat, double Lon, int PingNum, int SampleNum, double x, double y);
    void removeItemFromList();
    void deleteItem();
    void addScanAreaToSonarPlot(QCustomPlot *plotLf, int type, double radius, double yCrossPoint, GnssController *gnss);
    void removeScanAreaFromPlot();
    void updateTargetIndex(int newIndex, QFont font);
    QString determineName(QString text, QFont font);
    QString determineAreaText();

    QList <targetItem*> ConvertedTargets;
    double previousDistance = -500;
    double previousBearing = -500;
    bool startPoint = false;
    int convertedTgts = 0;

    double xMin = 0, xMax = 0, yMax = -1, yMin = -1;
    GnssController gnssInstance;

    QCPItemRect *scanArea_lf = nullptr;
    QList <QCPItemEllipse*> scanAreaCircle;
};

class measuringItem
{
public:
//    measuringItem();
    void setStartPoint(double x, double y, QCustomPlot *plotPtr);
    void addPoint(double x, double y, QCustomPlot *plotPtr);
    void setEndPoint(double x, double y, QCustomPlot *plotPtr);
    void setLine(double x1, double y1, double x2, double y2, double shadowCoeff = 1);
    bool setShadowEndPoint(double x, double y, QCustomPlot *plotPtr, double shadowCoeff);
    void createText(double x, double y, QCustomPlot *plotPtr);

    QPointF start_point;
    QPointF end_point;
    double length = 0;
    bool start_measuring = false;
    QList<QCPItemLine*>line_back;
    QList<QCPItemLine*>line_top;
    QCPItemText *distanceText = nullptr;
    QCPItemText *areaText = nullptr;
    bool selected = false;
    bool shadowTool = false;
    double shadowAngle = 90;

    QCPItemText *textGps = nullptr;
    QList<QCPItemLine*>lineGps;
    QList <QPointF> coordinates;
};

class Tracker
{
public:
    void setupPlotItems(QCustomPlot *plotPtr);
    void anchorWaypoint(waypointItem *waypoint);
    void appendRoute(waypointItem *waypoint);
    void removeRoutePoint(waypointItem *waypoint);
    int findWptIndex(waypointItem *item);
    void displayRoute();
    void resetRoute();
    void updateBoatPosition(double x, double y, double heading);
    void updateDistanceText();
    void updateBearingDisplay(double x, double y, double boatHeading);
    void updateBearingLineSize();
    void updateBearingIcon(double x, double y, double heading);
    waypointItem* passedWaypoint(waypointItem *item);
    void updateText();
    void setVisibility(bool hideAll = false);
    void remove();
    void resetWptAnchor();
    static double wrapAngle(double x);
    static double calculateGpsDistance (double LatOld, double LonOld, double LatNew, double LonNew);
    static double calculateGpsBearing(double LatOld, double LonOld, double LatNew, double LonNew);
    static void calculateGpsEndPoints(double LatOld, double LonOld, double angle, double length, double *LatNew, double *LonNew);

    QList <waypointItem*> route;
    waypointItem *nextWpt = nullptr;

    QCustomPlot *parentPlot = nullptr;
    QCPItemLine *distanceLine = nullptr, *distanceLine_back = nullptr, *bearingLine = nullptr;
    QCPItemPixmap *bearingIcon = nullptr;
    QList <QCPItemLine*> routeLines, routeLinesBack;
    QCPItemText *distanceText = nullptr, *bearingText = nullptr;
    double BoatHeading = -500;
    QString text_bearing = "", text_distance = "", text_wpt = "";
    bool validWpt = false, validBoat = false;
};

class PlotTools
{
public:
    PlotTools();

    // Target Tool
    void appendTargetItem(targetItem item);
    void removeAllTargetIcons();
    void removeAllTargetIconsFromAllPlots();
    void copyTargetImages(QString newBaseName);
    bool checkIfTargetNameExists(int index, QString text);
    void updateAllTargetIcons(int type);
    void updateAllTargetSizes();
    bool isTargetInList(targetItem item, QList<QListWidgetItem*> list);
    targetItem findTargetItem(QListWidgetItem *item);
    void convertWaypointToTarget(waypointItem *item);
    void renumberTargets();
    int determineNewName(bool isWaypoint);

    void sortAllTargets();
    void removeAllTargetsFromList();
    void deleteTarget(targetItem *item);
    QList <targetItem*> selectedTargets();
    void selectAllTargets(bool setSelected, int iconType);
    void sortAllWaypoints(double currentLat, double currentLon);
    void removeAllWaypointsFromList();
    void deleteWaypoint(waypointItem *item);
    QList <waypointItem*> selectedWaypoints();
    void selectAllWaypoints(bool setSelected, int iconType);

    void setStartPoint(waypointItem *item);
    void removeAllWaypointScanAreas();

    // Snip Tool
    void removeSnippedImage();
    void showImage(QCustomPlot *plotImage);
    // Measuring Tape Tool
    void addNewTapeMeasurement();
    // Tracking Tool
    int findNearWaypoints(double currentLat, double currentLon, double currentHeading, double sonarRange);
    bool findNearWaypoints(double currentLat, double currentLon, double currentHeading, double sonarRange, waypointItem *item);

    void notifyUser();
    void updateIconPanel(bool lfPlotClosed, bool target, bool tape, bool height, bool crop);
    void updatePanelPosition(double xPosition);
    void addAllPanelIcons(QCustomPlot *plotLf, QCustomPlot *plotHf);
    QCPItemPixmap* addPanelIcon(QCustomPlot *plot);
    void setEnabled(bool enable);
    int checkToolSelection(bool target, bool tape, bool height, bool crop);
    int checkToolHover(double x, double y, bool lfClosed, bool target, bool tape, bool height, bool crop);
    int checkMousePosition(double x, double y, bool lfClosed);

    QList <targetItem> targetList;
    QString importedFile = "";
    QList <waypointItem> waypointList;
    Tracker tracker;
    QListWidget *TargetListWidget = nullptr, *WaypointListWidget = nullptr;
    bool targetState = false; // 1 on first click, 0 on second click
    QMediaPlayer *targetAudio;
    QMediaPlayer *notificationAudio;

    measuringItem activeMeasuringTapeObject;
    QList<measuringItem> measuringTapeList;

    bool enabled = false;   // 0 no tools are enabled, 1 all tools are enabled

    struct imageBox_object {
        QPointF start_point;
        QPointF end_point;
        double width = 0;
        double height = 0;
        int plotType = 0;
        int currentItem = -1;
        QString fileName = "";
        QString filePath = "";

        QCPItemRect *LfBox = nullptr;
        QCPItemRect *HfBox = nullptr;
        QCPItemRect *SnipBox = nullptr; // original box item
        QCPItemPixmap *targetImage = nullptr;
    };
    imageBox_object activeImageBox;

    struct panelIcons {
        QCPItemPixmap *legendModeIcon_lf = nullptr, *legendModeIcon_hf = nullptr;
        QCPItemPixmap *crop_lf = nullptr, *crop_hf = nullptr;
        QCPItemPixmap *tape_lf = nullptr, *tape_hf = nullptr;
        QCPItemPixmap *height_lf = nullptr, *height_hf = nullptr;
        QCPItemPixmap *target_lf = nullptr, *target_hf = nullptr;
        bool target_higlighted = false, tape_higlighted = false;
        bool height_higlighted = false, crop_higlighted = false;
        bool separateIcons = false;
    };
    panelIcons panel;

};

#endif // MODULES_H
