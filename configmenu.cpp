#include "configmenu.h"
#include "ui_configmenu.h"
#include "datarecorder.h"
#include "ui_datarecorder.h"
#include <math.h>
#include <cmath>
#include <QMessageBox>
#include<QTextStream>
#include<QFileDialog>
#include<QDateTime>
#include <QSqlQueryModel>
#include<QLabel>
#include<mainwindow.h>



configMenu::configMenu(QWidget *parent) :
    QDialog(parent),
    config_ui(new Ui::configMenu)
{
    config_ui->setupUi(this);

}

configMenu::~configMenu()
{
    delete config_ui;
}
void configMenu :: assignFileName(QString dataFileName) {
    filename = dataFileName;
}
void configMenu::row1()
{

    bool validDepth1,validCompFactor1;
    //Grabbing Comp Factor and Depth values and converting to Double.
    d1 = config_ui->depth1->text();
    double dep1 = d1.toDouble(&validDepth1);
    cmp1 = config_ui->cmpFact1->text();
    double comp1 = cmp1.toDouble(&validCompFactor1);

    if(traceFreqvald() && validDepth1 && validCompFactor1 && !d1.isEmpty() && !cmp1.isEmpty()){


















        /* Start calulation of r_fwd_max*/

        double rfwd_max1 = ceil((dep1/(tan(setval)))/10)*10;

        rfwdout1 = QString::number(rfwd_max1,'f',0);
        config_ui->r1->setText(rfwdout1);
        /*----------------End--------------------*/

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation1 = round((1.5*2*rfwd_max1)/1500*1000); //-------- propagation1 =  2*t+50% ------- //

        propagationout1 = QString::number(propagation1,'g',15);
//        config_ui->propagation1->setText(propagationout1);
        //------------------- End Calculation of 2*t+50% --------------------//

        //----------------Start calculation of frep --------------------//
        double freqn1 = (1000/(propagation1+14));
        freqout1 = QString::number(freqn1,'f',5);
        config_ui->freq1->setText(freqout1);
        //--------------End calculation of frep --------------------//

        //--------------------calculation of N_max -----------------------//
        double nMax1 = round(((frequency_s*1000000)/freqn1)/10)*10;
        maxOut1 = QString::number(nMax1,'g',15);
        config_ui->nmax1->setText(maxOut1);
        //--------------------End of N_max -----------------------//


        //---------------calculation of comp Factor --------------//


        double nComp1 = round(nMax1/comp1);
        ncompOut1 = QString::number(nComp1,'g',15);
        config_ui->ncomp1->setText(ncompOut1);
        //---------------End of comp Factor --------------//


        /*... Start L_PLot Calculation and display on ui .... */

        double calc = (traceAngle*PI)/180;
        double lplot1 = round((rfwd_max1*cos(calc))*10)/10;
        lplotOut1 = QString::number(lplot1,'f',2);
        config_ui->Lplot1->setText(lplotOut1);

        /*.. End N_plot ... */



        /* ....start calculating N_plot... */

        double nplot1 = round((lplot1*frequency_s*1000000/comp1)/1500);
        nplotOut1 = QString::number(nplot1,'f',1);
        config_ui->nplot1->setText(nplotOut1);


        /* ... End Calculating N_plot... */



        /* ... start calculating Swath ... */

        double r = round((lplot1*sin(traceAngle*PI/180))*10)/10;
        double swath1 = 2*r;
        swathOut1 = QString::number(swath1,'f',1);
        config_ui->swath1->setText(swathOut1);

        /* ... End Calculating Swath ... */


        /*start reslat calculation*/


        double  reslat1 = (((100*swath1)/2)/nplot1);

        reslatOut1 = QString::number(reslat1,'f',5);

        config_ui->res_lat1->setText(reslatOut1);

        /*end reslat calculation*/


        //-------------res_fwd Calculation----------//

        double res_fwd1 = (((boatpropagation*1852*100)/3600)/freqn1)/100;

        resfwdout1 = QString::number(res_fwd1,'f',5);

        config_ui->rf1->setText(resfwdout1);




        //---------------End res_fwd Calculation ---------------//





    }
    else if(!validDepth1){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 1"));
    }
    else if(!validCompFactor1){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 1"));
    }

}
void configMenu::row2()
{
    bool validDepth2,validCompFactor2;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d2 = config_ui->depth2->text();
    double dep2 = d2.toDouble(&validDepth2);
    cmp2 = config_ui->cmpFact2->text();
    double comp2 = cmp2.toDouble(&validCompFactor2);


    if(validangle && validCompFactor && validDepth2 && validCompFactor2 && !d2.isEmpty() && !cmp2.isEmpty()){







        /* Start calulation of r_fwd_max*/

        double rfwd_max2 = ceil((dep2/(tan(setval)))/10)*10;
        rfwdout2 = QString::number(rfwd_max2,'g',15);
        config_ui->r2->setText(rfwdout2);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation2 = round((1.5*2*rfwd_max2)/1500*1000);

        propagationout2 = QString::number(propagation2,'g',15);
  //      config_ui->propagation2->setText(propagationout2);
        //-------------End --------------//

        //--------------- Start f_rep--------------- //
        double freqn2 = (1000/(propagation2+37.5));
        freqout2 = QString::number(freqn2,'f',5);
        config_ui->freq2->setText(freqout2);
        //--------------- End f_rep--------------- //


        //-----------------Calculation N_max ------------//
        double nMax2 = round(((frequency_s*1000000)/freqn2)/10)*10;
        maxOut2 = QString::number(nMax2,'g',15);
        config_ui->nmax2->setText(maxOut2);
        // ------------------End of N_max out --------------//


        //-----------------Calculation Comp factor ------------//

        double nComp2 = round(nMax2/comp2);
        ncompOut2 = QString::number(nComp2,'g',15);
        config_ui->ncomp2->setText(ncompOut2);
        //-----------------End Of Comp factor ------------//


        double calc = traceAngle*3.14159/180;
        double lplot2 = round(rfwd_max2*cos(calc)*10)/10;
        lplotOut2 = QString::number(lplot2,'f',1);
        config_ui->Lplot2->setText(lplotOut2);
        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot2 = round((lplot2*frequency_s*1000000/comp2)/1500);
        nplotOut2 = QString::number(nplot2,'f',1);
        config_ui->nplot2->setText(nplotOut2);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot2*sin(traceAngle*PI/180))*10)/10;
        double swath2 = 2* r;
        swathOut2 = QString::number(swath2,'f',1);
        config_ui->swath2->setText(swathOut2);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat2 ;
        reslat2 = (((100*swath2)/2)/nplot2);
        reslatOut2 = QString::number(reslat2,'f',5);
        config_ui->res_lat2->setText(reslatOut2);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd2 = (((boatpropagation*1852*100)/3600)/freqn2)/100;

        resfwdout2 = QString::number(res_fwd2,'f',5);

        config_ui->rf2->setText(resfwdout2);




        //---------------End res_fwd Calculation ---------------//
    }
    else if(!validDepth2){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 2"));
    }
    else if(!validCompFactor2){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 2"));    }

}
void configMenu::row3(){

    bool validDepth3,validCompFactor3;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d3 = config_ui->depth3->text();
    double dep3 = d3.toDouble(&validDepth3);
    cmp3 = config_ui->cmpFact3->text();
    double comp3 = cmp3.toDouble(&validCompFactor3);


    if(validangle && validFrequency && validDepth3 && validCompFactor3 && !d3.isEmpty() && !cmp3.isEmpty()){















        /* Start calulation of r_fwd_max*/

        double rfwd_max3 = ceil((dep3/(tan(setval)))/10)*10;

        rfwdout3 = QString::number(rfwd_max3,'g',15);
        config_ui->r3->setText(rfwdout3);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation3 = round((1.5*2*rfwd_max3)/1500*1000);
        propagationout3 = QString::number(propagation3,'g',15);
//        config_ui->propagation3->setText(propagationout3);
        //-----------------------------End ---------------------------//

        //----------------Start calculation of frep --------------------//
        double freqn3 = (1000/(propagation3+50));
        freqout3 = QString::number(freqn3,'f',5);
        config_ui->freq3->setText(freqout3);
        //-------------------End of F_rep-------------------------------//


        double frequency_s ; // value in Mhz
        if(config_ui->smpFreq->text().isEmpty()){
            frequency_s = 1.25; //default  Sampling Frequency if box left blank
        }

        else{
            frequency_s = config_ui->smpFreq->text().toDouble();// takinging Sampling Frequency inputted.
        }


        //----------Start N_max Calculation ------------//
        double nMax3 = round(((frequency_s*1000000)/freqn3)/10)*10;
        maxOut3 = QString::number(nMax3,'g',15);
        config_ui->nmax3->setText(maxOut3);
        //----------End N_max Calculation ------------//

        //----------Start Comp factor Calculation ------------//

        double nComp3 = round(nMax3/comp3);
        ncompOut3 = QString::number(nComp3,'g',15);
        config_ui->ncomp3->setText(ncompOut3);
        //----------End Comp factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */
        double traceAngle;
        if(config_ui->angle->text().isEmpty()){
            traceAngle = 29.04; //default trace angle if box left blank
        }

        else{
            traceAngle = config_ui->angle->text().toDouble();// taking trace angle inputted.
        }
        double calc = traceAngle*3.14159/180;
        double lplot3 = round(rfwd_max3*cos(calc)*10)/10;
        lplotOut3 = QString::number(lplot3,'f',1);
        config_ui->Lplot3->setText(lplotOut3);

        /*.. End N_plot ... */

        /* ... start calculating N_plot ... */
        double nplot3 = round((lplot3*frequency_s*1000000/comp3)/1500);
        nplotOut3 = QString::number(nplot3,'f',1);
        config_ui->nplot3->setText(nplotOut3);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot3*sin(traceAngle*PI/180))*10)/10;
        double swath3 = 2* r;
        swathOut3 = QString::number(swath3,'f',1);
        config_ui->swath3->setText(swathOut3);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat3 ;
        reslat3 = (((100*swath3)/2)/nplot3);
        reslatOut3 = QString::number(reslat3,'f',5);
        config_ui->res_lat3->setText(reslatOut3);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd3 = (((boatpropagation*1852*100)/3600)/freqn3)/100;

        resfwdout3 = QString::number(res_fwd3,'f',5);

        config_ui->rf3->setText(resfwdout3);




        //---------------End res_fwd Calculation ---------------//

    }
    else if(!validDepth3){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 3"));    }
    else if(!validCompFactor3){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 3"));    }

}
void configMenu::row4(){

    bool validDepth4,validCompFactor4;


    //Grabbing Comp Factor and Depth values and converting to Double.
    d4 = config_ui->depth4->text();
    double dep4 = d4.toDouble(&validDepth4);
    cmp4 = config_ui->cmpFact4->text();
    double comp4 = cmp4.toDouble(&validCompFactor4);



    if(validangle && validFrequency && validDepth4 && validCompFactor4 && !d4.isEmpty() && !cmp4.isEmpty()){

























        /* --------------Start calulation of r_fwd_max--------------------*/

        double rfwd_max4 = ceil((dep4/(tan(setval)))/10)*10;

        rfwdout4 = QString::number(rfwd_max4,'g',15);
        config_ui->r4->setText(rfwdout4);
        //--------------------End-------------------------//


        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation4 = round((1.5*2*rfwd_max4)/1500*1000);

        propagationout4 = QString::number(propagation4,'g',15);
  //      config_ui->propagation4->setText(propagationout4);
        //----------------------End----------------------------------------//


        //----------------Start calculation of frep --------------------//
        double freqn4 = (1000/(propagation4+112));
        freqout4 = QString::number(freqn4,'f',5);
        config_ui->freq4->setText(freqout4);
        //-------------------End of F_rep-------------------------------//

        //----------Start N_max Calculation ------------//
        double nMax4 = round(((frequency_s*1000000)/freqn4)/10)*10;
        maxOut4 = QString::number(nMax4,'g',15);
        config_ui->nmax4->setText(maxOut4);
        //----------End N_man Calculation ------------//

        //----------start Comp Factor Calculation ------------//

        double nComp4 = round(nMax4/comp4);
        ncompOut4 = QString::number(nComp4,'g',15);
        config_ui->ncomp4->setText(ncompOut4);
        //----------End Comp Factor Calculation ------------//

        /*.............Start L_PLot Calculation and display on ui ............. */

        double calc = traceAngle*PI/180;
        double lplot4 = round((rfwd_max4*cos(calc))*10)/10;
        lplotOut4 = QString::number(lplot4,'f',4);
        config_ui->Lplot4->setText(lplotOut4);
        /*......................... End N_plot ................................. */


        /* ....start calculating N_plot... */
        double nplot4 = round((lplot4*frequency_s*1000000/comp4)/1500);
        nplotOut4 = QString::number(nplot4,'f',1);
        config_ui->nplot4->setText(nplotOut4);
        /* ... End Calculating N_plot... */

        /* .............start calculating Swath............... */
        double r = round((lplot4*sin(traceAngle*PI/180))*10)/10;
        double swath4 = 2* r;
        swathOut4 = QString::number(swath4,'f',1);
        config_ui->swath4->setText(swathOut4);
        /*................End Calculating Swath...................*/


        /*...............start reslat calculation....................*/
        double reslat4 ;
        reslat4 = (((100*swath4)/2)/nplot4);
        reslatOut4 = QString::number(reslat4,'f',5);
        config_ui->res_lat4->setText(reslatOut4);
        /*....................end reslat calculation.........................*/

        //-------------res_fwd Calculation----------//

        double res_fwd4 = (((boatpropagation*1852*100)/3600)/freqn4)/100;

        resfwdout4 = QString::number(res_fwd4,'f',5);

        config_ui->rf4->setText(resfwdout4);




        //---------------End res_fwd Calculation ---------------//
    }
    else if(!validDepth4){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 4"));    }
    else if(!validCompFactor4){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 4"));    }

}
void configMenu::row5(){

    bool validDepth5,validCompFactor5;



    //Grabbing Comp Factor and Depth values and converting to Double.
    d5 = config_ui->depth5->text();
    double dep5 = d5.toDouble(&validDepth5);
    cmp5 = config_ui->cmpFact5->text();
    double comp5 = cmp5.toDouble(&validCompFactor5);


    if(validangle && validFrequency && validDepth5 && validCompFactor5 && !d5.isEmpty() && !cmp5.isEmpty()){
















        /* Start calulation of r_fwd_max*/

        double rfwd_max5 = ceil((dep5/(tan(setval)))/10)*10;
        rfwdout5 = QString::number(rfwd_max5,'g',15);
        config_ui->r5->setText(rfwdout5);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation5 = round((1.5*2*rfwd_max5)/1500*1000);
        propagationout5 = QString::number(propagation5,'g',15);
  //      config_ui->propagation5->setText(propagationout5);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn5 = (1000/(propagation5+140));
        freqout5 = QString::number(freqn5,'f',5);
        config_ui->freq5->setText(freqout5);

        //-------------------End of F_rep-------------------------------//

        //----------Start N_max Calculation ------------//
        double nMax5 = round(((frequency_s*1000000)/freqn5)/10)*10;
        maxOut5 = QString::number(nMax5,'g',15);
        config_ui->nmax5->setText(maxOut5);
        //----------End N_max Calculation ------------//

        //----------Start comp factor Calculation ------------//

        double nComp5 = round(nMax5/comp5);
        ncompOut5 = QString::number(nComp5,'g',15);
        config_ui->ncomp5->setText(ncompOut5);
        //----------End Comp Factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */


        double calc = traceAngle*3.14159/180;
        double lplot5 = round(rfwd_max5*cos(calc)*10)/10;
        lplotOut5 = QString::number(lplot5,'f',1);
        config_ui->Lplot5->setText(lplotOut5);

        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot5 = round((lplot5*frequency_s*1000000/comp5)/1500);
        nplotOut5 = QString::number(nplot5,'f',1);
        config_ui->nplot5->setText(nplotOut5);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot5*sin(traceAngle*PI/180))*10)/10;
        double swath5 = 2* r;
        swathOut5 = QString::number(swath5,'f',1);
        config_ui->swath5->setText(swathOut5);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat5 ;
        reslat5 = (((100*swath5)/2)/nplot5);
        reslatOut5 = QString::number(reslat5,'f',5);
        config_ui->res_lat5->setText(reslatOut5);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd5 = (((boatpropagation*1852*100)/3600)/freqn5)/100;

        resfwdout5 = QString::number(res_fwd5,'f',5);

        config_ui->rf5->setText(resfwdout5);




        //---------------End res_fwd Calculation ---------------//
    }

    else if(!validDepth5){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 5"));    }
    else if(!validCompFactor5){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 5"));    }


}
void configMenu::row6(){

    bool validDepth6,validCompFactor6;


    //Grabbing Comp Factor and Depth values and converting to Double.
    d6 = config_ui->depth6->text();
    double dep6 = d6.toDouble(&validDepth6);
    cmp6 = config_ui->cmpFact6->text();
    double comp6 = cmp6.toDouble(&validCompFactor6);


    if(validangle && validFrequency && validDepth6 && validCompFactor6 && !d6.isEmpty() && !cmp6.isEmpty()){

















        //-------Calculation of R_max-------//

        double rfwd_max6 = ceil((dep6/(tan(setval)))/10)*10;

        rfwdout6 = QString::number(rfwd_max6,'g',15);
        config_ui->r6->setText(rfwdout6);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation6 = round((1.5*2*rfwd_max6)/1500*1000);

        propagationout6 = QString::number(propagation6,'g',15);
 //       config_ui->propagation6->setText(propagationout6);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn6 = (1000/(propagation6+248));
        freqout6 = QString::number(freqn6,'f',5);
        config_ui->freq6->setText(freqout6);
        //-------------------End of F_rep-------------------------------//


        //----------Start N_max Calculation ------------//
        double nMax6 = round(((frequency_s*1000000)/freqn6)/10)*10;;
        maxOut6 = QString::number(nMax6,'g',15);
        config_ui->nmax6->setText(maxOut6);
        //----------End N_max Calculation ------------//

        //----------Start Comp Factor Calculation ------------//

        double nComp6 = round(nMax6/comp6);
        ncompOut6 = QString::number(nComp6,'g',15);
        config_ui->ncomp6->setText(ncompOut6);
        //----------End Comp Factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */

        double calc = traceAngle*3.14159/180;
        double lplot6 = round(rfwd_max6*cos(calc)*10)/10;
        lplotOut6 = QString::number(lplot6,'f',1);
        config_ui->Lplot6->setText(lplotOut6);
        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot6 = round((lplot6*frequency_s*1000000/comp6)/1500);;
        nplotOut6 = QString::number(nplot6,'f',1);
        config_ui->nplot6->setText(nplotOut6);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot6*sin(traceAngle*PI/180))*10)/10;
        double swath6 = 2* r;
        swathOut6 = QString::number(swath6,'f',1);
        config_ui->swath6->setText(swathOut6);
        /* ... End Calculating Swath ... */


        /*start reslat calculation*/
        double reslat6 ;
        reslat6 = (((100*swath6)/2)/nplot6);
        reslatOut6 = QString::number(reslat6,'f',5);
        config_ui->res_lat6->setText(reslatOut6);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd6 = (((boatpropagation*1852*100)/3600)/freqn6)/100;

        resfwdout6 = QString::number(res_fwd6,'f',5);

        config_ui->rf6->setText(resfwdout6);




        //---------------End res_fwd Calculation ---------------//
    }

    else if(!validDepth6){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 6"));    }
    else if(!validCompFactor6){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 6"));    }

}
void configMenu::row7(){















    bool validDepth7,validCompFactor7;


    //Grabbing Comp Factor and Depth values and converting to Double.
    d7 = config_ui->depth7->text();
    double dep7 = d7.toDouble(&validDepth7);
    cmp7 = config_ui->cmpFact7->text();
    double comp7 = cmp7.toDouble(&validCompFactor7);

    if(validangle && validFrequency && validDepth7 && validCompFactor7 && !d7.isEmpty() && !cmp7.isEmpty()){

        /* Start calulation of r_fwd_max*/

        double rfwd_max7 = ceil((dep7/(tan(setval)))/10)*10;

        rfwdout7 = QString::number(rfwd_max7,'g',15);
        config_ui->r7->setText(rfwdout7);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation7 = round((1.5*2*rfwd_max7)/1500*1000);

        propagationout7 = QString::number(propagation7,'g',15);
  //      config_ui->propagation7->setText(propagationout7);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn7 = (1000/(propagation7+248));
        freqout7 = QString::number(freqn7,'f',5);
        config_ui->freq7->setText(freqout7);
        //-------------------End of F_rep-------------------------------//


        //----------Start N_max Calculation ------------//
        double nMax7 = round(((frequency_s*1000000)/freqn7)/10)*10;
        maxOut7 = QString::number(nMax7,'g',15);
        config_ui->nmax7->setText(maxOut7);
        //----------End N_max Calculation ------------//

        //----------Start Comp factor Calculation ------------//

        double nComp7 = round(nMax7/comp7);
        ncompOut7 = QString::number(nComp7,'g',15);
        config_ui->ncomp7->setText(ncompOut7);
        //----------End Comp factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */
        double calc = traceAngle*3.14159/180;
        double lplot7 = round(rfwd_max7*cos(calc)*10)/10;
        lplotOut7 = QString::number(lplot7,'f',1);
        config_ui->Lplot7->setText(lplotOut7);
        /*.. End N_plot ... */


        /* ....start calculating N_plot... */
        double nplot7 = round((lplot7*frequency_s*1000000/comp7)/1500);
        nplotOut7 = QString::number(nplot7,'f',1);
        config_ui->nplot7->setText(nplotOut7);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot7*sin(traceAngle*PI/180))*10)/10;
        double swath7 = 2* r;
        swathOut7 = QString::number(swath7,'f',1);
        config_ui->swath7->setText(swathOut7);
        /* ... End Calculating Swath ... */


        /*start reslat calculation*/
        double reslat7 ;
        reslat7 = (((100*swath7)/2)/nplot7);
        reslatOut7 = QString::number(reslat7,'f',5);
        config_ui->res_lat7->setText(reslatOut7);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd7 = (((boatpropagation*1852*100)/3600)/freqn7)/100;

        resfwdout7 = QString::number(res_fwd7,'f',5);

        config_ui->rf7->setText(resfwdout7);




        //---------------End res_fwd Calculation ---------------//
    }


    else if(!validDepth7){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 7"));    }
    else if(!validCompFactor7){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 7"));    }



}
void configMenu::row8(){

    bool validDepth8,validCompFactor8;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d8 = config_ui->depth8->text();
    double dep8 = d8.toDouble(&validDepth8);
    cmp8 = config_ui->cmpFact8->text();
    double comp8 = cmp8.toDouble(&validCompFactor8);
    //start validation

    if(validangle && validFrequency && validDepth8 && validCompFactor8 && !d8.isEmpty() && !cmp8.isEmpty()){

        /* Start calulation of r_fwd_max*/

        double rfwd_max8 = ceil((dep8/(tan(setval)))/10)*10;
        rfwdout8 = QString::number(rfwd_max8,'g',15);
        config_ui->r8->setText(rfwdout8);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation8 = round((1.5*2*rfwd_max8)/1500*1000);
        propagationout8 = QString::number(propagation8,'g',15);
    //    config_ui->propagation8->setText(propagationout8);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn8 = (1000/(propagation8+248));
        freqout8 = QString::number(freqn8,'f',5);
        config_ui->freq8->setText(freqout8);
        //-------------------End of F_rep-------------------------------//



        //----------Start N_max Calculation ------------//
        double nMax8 = round(((frequency_s*1000000)/freqn8)/10)*10;
        maxOut8 = QString::number(nMax8,'g',15);
        config_ui->nmax8->setText(maxOut8);
        //----------End N_max Calculation ------------//

        //----------Start comp factor Calculation ------------//

        double nComp8 = round(nMax8/comp8);
        ncompOut8 = QString::number(nComp8,'g',15);
        config_ui->ncomp8->setText(ncompOut8);
        //----------End comp factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */

        double calc = traceAngle*3.14159/180;
        double lplot8 = round(rfwd_max8*cos(calc)*10)/10;
        lplotOut8 = QString::number(lplot8,'f',1);
        config_ui->Lplot8->setText(lplotOut8);

        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot8 = round((lplot8*frequency_s*1000000/comp8)/1500);
        nplotOut8 = QString::number(nplot8,'f',1);
        config_ui->nplot8->setText(nplotOut8);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot8*sin(traceAngle*PI/180))*10)/10;
        double swath8 = 2* r;
        swathOut8 = QString::number(swath8,'f',1);
        config_ui->swath8->setText(swathOut8);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat8 ;
        reslat8 = (((100*swath8)/2)/nplot8);
        reslatOut8 = QString::number(reslat8,'f',5);
        config_ui->res_lat8->setText(reslatOut8);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd8 = (((boatpropagation*1852*100)/3600)/freqn8)/100;

        resfwdout8 = QString::number(res_fwd8,'f',5);

        config_ui->rf8->setText(resfwdout8);




        //---------------End res_fwd Calculation ---------------//
    }


    else if(!validDepth8){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 8"));    }
    else if(!validCompFactor8){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 8"));    }

}
void configMenu::row9(){
    bool validDepth9,validCompFactor9;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d9 = config_ui->depth9->text();
    double dep9 = d9.toDouble(&validDepth9);
    cmp9 = config_ui->cmpFact9->text();
    double comp9 = cmp9.toDouble(&validCompFactor9);

    if(validangle && validFrequency && validDepth9 && validCompFactor9 && !d9.isEmpty() && !cmp9.isEmpty()){



        /* Start calulation of r_fwd_max*/

        double rfwd_max9 = ceil((dep9/(tan(setval)))/10)*10;

        rfwdout9 = QString::number(rfwd_max9,'g',15);
        config_ui->r9->setText(rfwdout9);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation9 = round((1.5*2*rfwd_max9)/1500*1000);
        propagationout9 = QString::number(propagation9,'g',15);
  //      config_ui->propagation9->setText(propagationout9);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn9 = (1000/(propagation9+297));
        freqout9 = QString::number(freqn9,'f',5);
        config_ui->freq9->setText(freqout9);
        //-------------------End of F_rep-------------------------------//

        //----------Start N_max Calculation ------------//
        double nMax9 = round(((frequency_s*1000000)/freqn9)/10)*10;
        maxOut9 = QString::number(nMax9,'g',15);
        config_ui->nmax9->setText(maxOut9);
        //----------End N_max Calculation ------------//

        //----------Start comp factor Calculation ------------//
        double nComp9 = round(nMax9/comp9);
        ncompOut9 = QString::number(nComp9,'g',15);
        config_ui->ncomp9->setText(ncompOut9);
        //----------End Comp Factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */
        double calc = traceAngle*3.14159/180;
        double lplot9 = round(rfwd_max9*cos(calc)*10)/10;
        lplotOut9 = QString::number(lplot9,'f',1);
        config_ui->Lplot9->setText(lplotOut9);
        /*...................End N_plot .................... */


        /* .............start calculating N_plot...............*/
        double nplot9 = round((lplot9*frequency_s*1000000/comp9)/1500);;
        nplotOut9 = QString::number(nplot9,'f',1);
        config_ui->nplot9->setText(nplotOut9);
        /* ... End Calculating N_plot... */


        /* .............. start calculating Swath ...................... */
        double r = round((lplot9*sin(traceAngle*PI/180))*10)/10;
        double swath9 = 2* r;
        swathOut9 = QString::number(swath9,'f',1);
        config_ui->swath9->setText(swathOut9);
        /* ...........................End Calculating Swath ................. */


        /* ..................start reslat calculation.............. */
        double reslat9 = (((100*swath9)/2)/nplot9);
        reslatOut9 = QString::number(reslat9,'f',5);
        config_ui->res_lat9->setText(reslatOut9);
        /*...........end reslat calculation....................*/

        //-------------res_fwd Calculation----------//

        double res_fwd9 = (((boatpropagation*1852*100)/3600)/freqn9)/100;

        resfwdout9 = QString::number(res_fwd9,'f',5);

        config_ui->rf9->setText(resfwdout9);

        //---------------End res_fwd Calculation ---------------//

    }

    else if(!validDepth9){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 9"));    }
    else if(!validCompFactor9){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 9"));    }
}
void configMenu::row10(){
    /* Start calulation of r_fwd_max*/
    cmp10 = config_ui->cmpFact10->text();
    double comp10 = cmp10.toDouble(&validCompFactor);
    d10 = config_ui->depth10->text();
    double dep10 = d10.toDouble(&validDepth);

    if(validangle && validFrequency && validDepth && validCompFactor && !d10.isEmpty() && !cmp10.isEmpty()){


        double rfwd_max10 = ceil((dep10/(tan(setval)))/10)*10;

        rfwdout10 = QString::number(rfwd_max10,'g',15);
        config_ui->r10->setText(rfwdout10);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation10 = round((1.5*2*rfwd_max10)/1500*1000);

        propagationout10 = QString::number(propagation10,'g',15);
  //      config_ui->propagation10->setText(propagationout10);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn10 = (1000/(propagation10+320));
        freqout10 = QString::number(freqn10,'f',5);
        config_ui->freq10->setText(freqout10);
        //-------------------End of F_rep-------------------------------//

        // value in Mhz


        //----------Start N_max Calculation ------------//
        double nMax10 = round(((frequency_s*1000000)/freqn10)/10)*10;
        maxOut10 = QString::number(nMax10,'g',15);
        config_ui->nmax10->setText(maxOut10);
        //----------End N_max Calculation ------------//

        //----------Start Comp_factor Calculation ------------//


        double nComp10 = round(nMax10/comp10);
        ncompOut10 = QString::number(nComp10,'g',15);
        config_ui->ncomp10->setText(ncompOut10);
        //----------End Comp Factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */
        double calc = traceAngle*3.14159/180;
        double lplot10 = round(rfwd_max10*cos(calc)*10)/10;
        lplotOut10 = QString::number(lplot10,'f',1);
        config_ui->Lplot10->setText(lplotOut10);
        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot10 = round((lplot10*frequency_s*1000000/comp10)/1500);
        nplotOut10 = QString::number(nplot10,'f',1);
        config_ui->nplot10->setText(nplotOut10);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot10*sin(traceAngle*PI/180))*10)/10;
        double swath10 = 2* r;
        swathOut10 = QString::number(swath10,'f',1);
        config_ui->swath10->setText(swathOut10);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat10 ;
        reslat10 = (((100*swath10)/2)/nplot10);
        reslatOut10 = QString::number(reslat10,'f',5);
        config_ui->res_lat10->setText(reslatOut10);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd10 = (((boatpropagation*1852*100)/3600)/freqn10)/100;

        resfwdout10 = QString::number(res_fwd10,'f',5);

        config_ui->rf10->setText(resfwdout10);




        //---------------End res_fwd Calculation ---------------//
    }

    else if(!validDepth){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 10"));    }
    else if(!validCompFactor){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 10"));    }

}
void configMenu::row11(){

    bool validDepth11,validCompFactor11;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d11 = config_ui->depth11->text();
    double dep11 = d11.toDouble(&validDepth11);
    cmp11 = config_ui->cmpFact11->text();
    double comp11 = cmp11.toDouble(&validCompFactor11);




    if(validangle && validFrequency && validDepth11 && validCompFactor11 && !d11.isEmpty() && !cmp11.isEmpty()){







        /* Start calulation of r_fwd_max*/

        double rfwd_max11 = ceil((dep11/(tan(setval)))/10)*10;
        rfwdout11 = QString::number(rfwd_max11,'g',15);
        config_ui->r11->setText(rfwdout11);
        //-----------End--------------//


        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation11 = round((1.5*2*rfwd_max11)/1500*1000);
        propagationout11 = QString::number(propagation11,'g',15);
   //     config_ui->propagation11->setText(propagationout11);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn11 = (1000/(propagation11+350));
        freqout11 = QString::number(freqn11,'f',5);
        config_ui->freq11->setText(freqout11);
        //-------------------End of F_rep-------------------------------//

        //----------Start N_max Calculation ------------//
        double nMax11 = round(((frequency_s*1000000)/freqn11)/10)*10;
        maxOut11 = QString::number(nMax11,'g',15);
        config_ui->nmax11->setText(maxOut11);
        //----------End N_max Calculation ------------//

        //----------Start comp factor Calculation ------------//

        double nComp11 = round(nMax11/comp11);
        ncompOut11 = QString::number(nComp11,'g',15);
        config_ui->ncomp11->setText(ncompOut11);
        //---------- End Comp Factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */

        double calc = traceAngle*3.14159/180;
        double lplot11 = round(rfwd_max11*cos(calc)*10)/10;
        lplotOut11 = QString::number(lplot11,'f',1);
        config_ui->Lplot11->setText(lplotOut11);
        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot11 = round((lplot11*frequency_s*1000000/comp11)/1500);
        nplotOut11 = QString::number(nplot11,'f',1);
        config_ui->nplot11->setText(nplotOut11);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot11*sin(traceAngle*PI/180))*10)/10;
        double swath11 = 2* r;
        swathOut11 = QString::number(swath11,'f',1);
        config_ui->swath11->setText(swathOut11);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat11 ;
        reslat11 = (((100*swath11)/2)/nplot11);
        reslatOut11 = QString::number(reslat11,'f',5);
        config_ui->res_lat11->setText(reslatOut11);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd11 = (((boatpropagation*1852*100)/3600)/freqn11)/100;

        resfwdout11= QString::number(res_fwd11,'f',5);

        config_ui->rf11->setText(resfwdout11);




        //---------------End res_fwd Calculation ---------------//
    }

    else if(!validDepth11){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 11"));    }
    else if(!validCompFactor11){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 11"));    }


}
void configMenu::row12(){

    bool validDepth12,validCompFactor12;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d12 = config_ui->depth12->text();
    double dep12 = d12.toDouble(&validDepth12);
    cmp12 = config_ui->cmpFact12->text();
    double comp12 = cmp12.toDouble(&validCompFactor12);


    if(validangle && validFrequency && validDepth12 && validCompFactor12 && !d12.isEmpty() && !cmp12.isEmpty()){









        /* Start calulation of r_fwd_max*/

        double rfwd_max12 = ceil((dep12/(tan(setval)))/10)*10;
        rfwdout12 = QString::number(rfwd_max12,'g',15);
        config_ui->r12->setText(rfwdout12);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation12 = round((1.5*2*rfwd_max12)/1500*1000);
        propagationout12 = QString::number(propagation12,'g',15);
 //       config_ui->propagation12->setText(propagationout12);
        //-------------End --------------//7

        //----------------Start calculation of frep --------------------//
        double freqn12 = (1000/(propagation12+380));
        freqout12 = QString::number(freqn12,'f',5);
        config_ui->freq12->setText(freqout12);
        //-------------------End of F_rep-------------------------------//

        //----------Start N_max Calculation ------------//
        double nMax12 = round(((frequency_s*1000000)/freqn12)/10)*10;
        maxOut12 = QString::number(nMax12,'g',15);
        config_ui->nmax12->setText(maxOut12);
        //----------End N_max Calculation ------------//

        //----------Start comp factor Calculation ------------//

        double nComp12 = round(nMax12/comp12);
        ncompOut12 = QString::number(nComp12,'g',15);
        config_ui->ncomp12->setText(ncompOut12);
        //----------End Comp_factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */

        double calc = traceAngle*3.14159/180;
        double lplot12 = round(rfwd_max12*cos(calc)*10)/10;
        lplotOut12 = QString::number(lplot12,'f',1);
        config_ui->Lplot12->setText(lplotOut12);
        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot12 = round((lplot12*frequency_s*1000000/comp12)/1500);
        nplotOut12 = QString::number(nplot12,'f',1);
        config_ui->nplot12->setText(nplotOut12);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot12*sin(traceAngle*PI/180))*10)/10;
        double swath12 = 2* r;
        swathOut12 = QString::number(swath12,'f',1);
        config_ui->swath12->setText(swathOut12);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat12 ;
        reslat12 = (((100*swath12)/2)/nplot12);
        reslatOut12 = QString::number(reslat12,'f',5);
        config_ui->res_lat12->setText(reslatOut12);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd12 = (((boatpropagation*1852*100)/3600)/freqn12)/100;

        resfwdout12 = QString::number(res_fwd12,'f',5);

        config_ui->rf12->setText(resfwdout12);




        //---------------End res_fwd Calculation ---------------//
    }

    else if(!validDepth12){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 12"));    }
    else if(!validCompFactor12){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 12"));    }


}
void configMenu::row13()
{
    bool validDepth13,validCompFactor13;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d13 = config_ui->depth13->text();
    double dep13 = d13.toDouble(&validDepth13);
    cmp13 = config_ui->cmpFact13->text();
    double comp13 = cmp3.toDouble(&validCompFactor13);



    if(validangle && validFrequency && validDepth13 && validCompFactor13 && !d13.isEmpty() && !cmp13.isEmpty()){










        /* Start calulation of r_fwd_max*/

        double rfwd_max13 = ceil((dep13/(tan(setval)))/10)*10;
        rfwdout13 = QString::number(rfwd_max13,'g',15);
        config_ui->r13->setText(rfwdout13);
        //-----------End--------------//

        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation13 = round((1.5*2*rfwd_max13)/1500*1000);
        propagationout13 = QString::number(propagation13,'g',15);
  //      config_ui->propagation13->setText(propagationout13);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn13 = (1000/(propagation13+400));
        freqout13 = QString::number(freqn13,'g',2);
        config_ui->freq13->setText(freqout13);
        //-------------------End of F_rep-------------------------------//


        //----------Start N_max Calculation ------------//
        double nMax13 = round(((frequency_s*1000000)/freqn13)/10)*10;
        maxOut13 = QString::number(nMax13,'g',15);
        config_ui->nmax13->setText(maxOut13);
        //----------End N_max Calculation ------------//

        //----------Start comp factor Calculation ------------//

        double nComp13 = round(nMax13/comp13);
        ncompOut13 = QString::number(nComp13,'g',15);
        config_ui->ncomp13->setText(ncompOut13);
        //----------End Cpmp factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */
        double calc = traceAngle*3.14159/180;
        double lplot13 = round(rfwd_max13*cos(calc)*10)/10;
        lplotOut13 = QString::number(lplot13,'f',1);
        config_ui->Lplot13->setText(lplotOut13);
        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot13 = round((lplot13*frequency_s*1000000/comp13)/1500);
        nplotOut13 = QString::number(nplot13,'f',1);
        config_ui->nplot13->setText(nplotOut13);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot13*sin(traceAngle*PI/180))*10)/10;
        double swath13 = 2* r;
        swathOut13 = QString::number(swath13,'f',1);
        config_ui->swath13->setText(swathOut13);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat13 ;
        reslat13 = (((100*swath13)/2)/nplot13);
        reslatOut13 = QString::number(reslat13,'f',5);
        config_ui->res_lat13->setText(reslatOut13);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd13 = (((boatpropagation*1852*100)/3600)/freqn13)/100;

        resfwdout13 = QString::number(res_fwd13,'f',5);

        config_ui->rf13->setText(resfwdout13);




        //---------------End res_fwd Calculation ---------------//
    }

    else if(!validDepth13){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 13"));    }
    else if(!validCompFactor13){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 13"));    }


}
void configMenu::row14(){
    bool validDepth14,validCompFactor14;

    //Grabbing Comp Factor and Depth values and converting to Double.
    d14 = config_ui->depth14->text();
    double dep14 = d14.toDouble(&validDepth14);
    cmp14 = config_ui->cmpFact14->text();
    double comp14 = cmp14.toDouble(&validCompFactor14);





    if(validangle && validFrequency && validDepth14 && validCompFactor14 && !d14.isEmpty() && !cmp14.isEmpty()){










        /* Start calulation of r_fwd_max*/

        double rfwd_max14 = ceil((dep14/(tan(setval)))/10)*10;
        rfwdout14 = QString::number(rfwd_max14,'g',15);
        config_ui->r14->setText(rfwdout14);
        //-----------End--------------//


        //--------------- Start calculation of 2*t+50% --------------- //
        double propagation14 = round((1.5*2*rfwd_max14)/1500*1000);
        propagationout14 = QString::number(propagation14,'g',15);
 //       config_ui->propagation14->setText(propagationout14);
        //-------------End --------------//

        //----------------Start calculation of frep --------------------//
        double freqn14 = ((1000/(propagation14+440)));
        freqout14 = QString::number(freqn14,'f',2);
        config_ui->freq14->setText(freqout14);
        //-------------------End of F_rep-------------------------------//

        //----------Start N_max Calculation ------------//
        double nMax14 = round(((frequency_s*1000000)/freqn14)/10)*10;
        maxOut14 = QString::number(nMax14,'g',15);
        config_ui->nmax14->setText(maxOut14);
        //----------End N_max Calculation ------------//

        //----------Start Comp factor Calculation ------------//

        double nComp14 = round(nMax14/comp14);
        ncompOut14 = QString::number(nComp14,'g',15);
        config_ui->ncomp14->setText(ncompOut14);
        //----------End comp Factor Calculation ------------//

        /*... Start L_PLot Calculation and display on ui .... */

        double calc = traceAngle*3.14159/180;
        double lplot14 = ceil(rfwd_max14*cos(calc)*10)/10;
        lplotOut14 = QString::number(lplot14,'f',1);
        config_ui->Lplot14->setText(lplotOut14);

        /*.. End N_plot ... */

        /* ....start calculating N_plot... */
        double nplot14 = round((lplot14*frequency_s*1000000/comp14)/1500);
        nplotOut14 = QString::number(nplot14,'f',1);
        config_ui->nplot14->setText(nplotOut14);
        /* ... End Calculating N_plot... */

        /* ... start calculating Swath ... */
        double r = round((lplot14*sin(traceAngle*PI/180))*10)/10;
        double swath14 = 2* r;
        swathOut14 = QString::number(swath14,'f',1);
        config_ui->swath14->setText(swathOut14);
        /* ... End Calculating Swath ... */

        /*start reslat calculation*/
        double reslat14 ;
        reslat14 = (((100*swath14)/2)/nplot14);
        reslatOut14 = QString::number(reslat14,'f',5);
        config_ui->res_lat14->setText(reslatOut14);
        /*end reslat calculation*/

        //-------------res_fwd Calculation----------//

        double res_fwd14 = (((boatpropagation*1852*100)/3600)/freqn14)/100;

        resfwdout14 = QString::number(res_fwd14,'f',5);

        config_ui->rf14->setText(resfwdout14);




        //---------------End res_fwd Calculation ---------------//

    }

    else if(!validDepth14){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Depth box 14"));    }
    else if(!validCompFactor14){
        QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Comp factor Box 14"));    }



}
void configMenu::writefile(){
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::AnyFile);
    QString strFile = dialog.getSaveFileName(NULL, "Create New File","","");
    //qDebug()<<strFile;
    QFile file(strFile);
    file.open(QIODevice::WriteOnly);
    file.close();
}
void configMenu::readfile(){
    QPushButton * button = (QPushButton*) sender();
    button->setChecked(true);

    filename = QFileDialog::getOpenFileName(this,"Open File" , "");
    QMessageBox::information(this,tr("File Name"),filename);

}
void configMenu::savedata(){
    //MainWindow conn;





    /* ------------------------------ Data Saving Codes using SQLite ----------------------------------------------------------  */



    if(!connOpen()){
        qDebug()<<("Database not connected");
        return;
    }
    connOpen();
    QSqlQuery qry;
    qry.prepare( "CREATE TABLE IF NOT EXISTS ConfigDB (ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, Depth REAL, Comp_Factor REAL, R_fwd_max REAL, propagation REAL, f_rep REAL,N_max REAL, N_comp REAL, N_plot REAL, Swath REAL, L_plot REAL , Res_lat REAL,  Res_fwd REAL)" );
    if( !qry.exec() )
        qDebug() << qry.lastError();
    else
        qDebug() << "Table created!";
    qry.prepare("delete from ConfigDB where ID>0");
    if( !qry.exec() )
        qDebug() << qry.lastError();

    else{qDebug() << "Table Cleared";

    }
    qry.prepare("insert into ConfigDB (Depth,Comp_Factor,R_fwd_max,propagation,f_rep,N_max,N_comp,N_plot,Swath,L_plot,Res_lat,Res_fwd) "
                "values('"+d1+"','"+cmp1+"','"+rfwdout1+"','"+propagationout1+"','"+freqout1+"','"+maxOut1+"','"+ncompOut1+"','"+nplotOut1+"','"+swathOut1+"','"+lplotOut1+"','"+reslatOut1+"','"+resfwdout1+"'),"
                                                                                                                                                                                                             "('"+d2+"','"+cmp2+"','"+rfwdout2+"','"+propagationout2+"','"+freqout2+"','"+maxOut2+"','"+ncompOut2+"','"+nplotOut2+"','"+swathOut2+"','"+lplotOut2+"','"+reslatOut2+"','"+resfwdout2+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                    "('"+d3+"','"+cmp3+"','"+rfwdout3+"','"+propagationout3+"','"+freqout3+"','"+maxOut3+"','"+ncompOut3+"','"+nplotOut3+"','"+swathOut3+"','"+lplotOut3+"','"+reslatOut3+"','"+resfwdout3+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           "('"+d4+"','"+cmp4+"','"+rfwdout4+"','"+propagationout4+"','"+freqout4+"','"+maxOut4+"','"+ncompOut4+"','"+nplotOut4+"','"+swathOut4+"','"+lplotOut4+"','"+reslatOut4+"','"+resfwdout4+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  "('"+d5+"','"+cmp5+"','"+rfwdout5+"','"+propagationout5+"','"+freqout5+"','"+maxOut5+"','"+ncompOut5+"','"+nplotOut5+"','"+swathOut5+"','"+lplotOut5+"','"+reslatOut5+"','"+resfwdout5+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         "('"+d6+"','"+cmp6+"','"+rfwdout6+"','"+propagationout6+"','"+freqout6+"','"+maxOut6+"','"+ncompOut6+"','"+nplotOut6+"','"+swathOut6+"','"+lplotOut6+"','"+reslatOut6+"','"+resfwdout6+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "('"+d7+"','"+cmp7+"','"+rfwdout7+"','"+propagationout7+"','"+freqout7+"','"+maxOut7+"','"+ncompOut7+"','"+nplotOut7+"','"+swathOut7+"','"+lplotOut7+"','"+reslatOut7+"','"+resfwdout7+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       "('"+d8+"','"+cmp8+"','"+rfwdout8+"','"+propagationout8+"','"+freqout8+"','"+maxOut8+"','"+ncompOut8+"','"+nplotOut8+"','"+swathOut8+"','"+lplotOut8+"','"+reslatOut8+"','"+resfwdout8+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              "('"+d9+"','"+cmp9+"','"+rfwdout9+"','"+propagationout9+"','"+freqout9+"','"+maxOut9+"','"+ncompOut9+"','"+nplotOut9+"','"+swathOut9+"','"+lplotOut9+"','"+reslatOut9+"','"+resfwdout9+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     "('"+d10+"','"+cmp10+"','"+rfwdout10+"','"+propagationout10+"','"+freqout10+"','"+maxOut10+"','"+ncompOut10+"','"+nplotOut10+"','"+swathOut10+"','"+lplotOut10+"','"+reslatOut10+"','"+resfwdout10+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "('"+d11+"','"+cmp11+"','"+rfwdout11+"','"+propagationout11+"','"+freqout11+"','"+maxOut11+"','"+ncompOut11+"','"+nplotOut11+"','"+swathOut11+"','"+lplotOut11+"','"+reslatOut11+"','"+resfwdout11+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           "('"+d12+"','"+cmp12+"','"+rfwdout12+"','"+propagationout12+"','"+freqout12+"','"+maxOut12+"','"+ncompOut12+"','"+nplotOut12+"','"+swathOut12+"','"+lplotOut12+"','"+reslatOut12+"','"+resfwdout12+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              "('"+d13+"','"+cmp13+"','"+rfwdout13+"','"+propagationout13+"','"+freqout13+"','"+maxOut13+"','"+ncompOut13+"','"+nplotOut13+"','"+swathOut13+"','"+lplotOut13+"','"+reslatOut13+"','"+resfwdout13+"'),"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 "('"+d14+"','"+cmp14+"','"+rfwdout14+"','"+propagationout14+"','"+freqout14+"','"+maxOut14+"','"+ncompOut14+"','"+nplotOut14+"','"+swathOut14+"','"+lplotOut14+"','"+reslatOut14+"','"+resfwdout14+"')");
    if(qry.exec()){

        QMessageBox::critical(this,tr("DATA SAVING"),tr("DATA SAVED!!"));
        connClose();
    }

    else
    {
        QMessageBox::critical(this,tr("ERROR"),qry.lastError().text());
    }
    /* ------------------------------------ END of SAVING -------------------------------------------------------------------------------- */


}
void configMenu::date_time(){
    QTime timeSvd = QTime::currentTime();
    QString time_text = timeSvd.toString("hh: mm: ss:");
    config_ui->timE->setText("Time Data Saved: "+time_text);
}
void configMenu::showDefault(){
    if(!connOpen()){
        qDebug()<<("Database not connected");
        return;
    }
    connOpen();
    QSqlQuery qry;

    qry.prepare( "CREATE TABLE IF NOT EXISTS ConfigDB (ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, Depth REAL, Comp_Factor REAL, R_fwd_max REAL, propagation REAL, f_rep REAL,N_max REAL, N_comp REAL, N_plot REAL, Swath REAL, L_plot REAL , Res_lat REAL,Res_fwd REAL)" );
    if( !qry.exec() )
        qDebug() << qry.lastError();
    else
        qDebug() << "Table created!";
    QSqlQueryModel model;

    model.setQuery("SELECT * FROM ConfigDB");
    if(model.query().exec()){
        for (int i = 0; i < 14; ++i) {
            Rfwdtemp.append(model.record(i).value("R_fwd_max").toString());
            depthTemp.append(model.record(i).value("Depth").toString());
            CompTemp.append(model.record(i).value("Comp_Factor").toString());
            propagationTemp.append(model.record(i).value("propagation").toString());
            f_rep_temp.append(model.record(i).value("f_rep").toString());
            n_max_temp.append(model.record(i).value("N_max").toString());
            n_comp_temp.append(model.record(i).value("N_comp").toString());
            n_plot_temp.append(model.record(i).value("N_plot").toString());
            swath_temp.append(model.record(i).value("Swath").toString());
            l_plot_temp.append(model.record(i).value("L_plot").toString());
            res_lat_temp.append(model.record(i).value("Res_lat").toString());
            res_fwd_temp.append(model.record(i).value("Res_fwd").toString());
        }
        for (int i = 0; i < 14; ++i) {
            rfwd_list[i]->setText(Rfwdtemp[i]);
            depthlist[i]->setText(depthTemp[i]);
            compmx_list[i]->setText(CompTemp[i]);
            propagationtm_list[i]->setText(propagationTemp[i]);
            f_rep_list[i]->setText(f_rep_temp[i]);
            n_max_list[i]->setText(n_max_temp[i]);
            n_comp_list[i]->setText(n_comp_temp[i]);

            n_plot_list[i]->setText(n_plot_temp[i]);
            swath_list[i]->setText(swath_temp[i]);
            l_plot_list[i]->setText(l_plot_temp[i]);
            res_lat_list[i]->setText(res_lat_temp[i]);
            res_fwd_list[i]->setText(res_fwd_temp[i]);

        }
        connClose();
    }

    else{
        QMessageBox::critical(this,tr("ERROR"),qry.lastError().text());

    }


}
bool configMenu::traceFreqvald(){

    if(config_ui->angle->text().isEmpty()){
        traceAngle = 29.04; //default trace angle if box left blank
    }

    else{

       traceAngle = config_ui->angle->text().toDouble(&validangle);// taking trace angle inputted.
    }
    if(config_ui->smpFreq->text().isEmpty()){
        frequency_s = 1.25; //default  Sampling Frequency if box left blank
    }

    else{
       frequency_s= config_ui->smpFreq->text().toDouble(&validFrequency);// takinging Sampling Frequency inputted.
    }


    if(!validangle){
       QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Trace Angle"));
        return false;
    }

    else if(!validFrequency){

    QMessageBox::critical(this,tr("ERROR"),tr("Enter a Digit in Sampling Frequency"));
        return false;
    }

    else{
        traceAngle = config_ui->angle->text().toDouble(&validangle);
         frequency_s= config_ui->smpFreq->text().toDouble(&validFrequency);
        qDebug()<< "generated";
        return true;
    }


}
void configMenu::setConfigInfo(QString confile,QString dateCrt , QString dateUpd)
{


        confileName = confile;
        dateCreated = dateCrt;
        dateUpdated = dateUpd;





}


void configMenu::showConfig(){
//    QMessageBox::information(this,tr("Database Information"),
//                             QString("File Name:  %1. \n\nDate Created:  %2. \n\nDate Updated: %3. \n\n")
//                             .arg(confileName)
//                             .arg(dateCreated)
//                             .arg(dateUpdated));

}
