#ifndef CHARTDIALOG_H
#define CHARTDIALOG_H

#include <QDialog>

#include "qcustomplot.h"
#include<cmath>
#include<QtSql>
#include<QFileInfo>
namespace Ui {
class chartdialog;
}

class chartdialog : public QDialog
{
    Q_OBJECT

public:
    explicit chartdialog(QWidget *parent = nullptr);
    ~chartdialog();

    QString name;
    QList<QString>filenames;
    QSqlDatabase coordinates;
    QString coords = "coordinates.db";

    void saveCoordinates();
    void readFromSavedCoords();
    void checkIfFileSaved();

    double minLong, minLat, maxLong, maxLat;
    QString tiffName;

    bool connOpenCoordinates(){
        coordinates = QSqlDatabase::addDatabase("QSQLITE","userConnection");
        coordinates.setDatabaseName(coords);

        if(!coordinates.open()){
            qDebug()<<("Could not Connect to Database");
            QMessageBox::critical(this,tr("Database Info"),tr("COORDINATES DATABASE NOT FOUND!!"));
            return false;
        }else{
            return true;
        }

        coordinates = QSqlDatabase::database("userConnecion",true);
    }

    void connCloseCoordinates(){
        QString con = coordinates.connectionName();
        coordinates.close();
        QSqlDatabase::removeDatabase(con);
    }


private:
    Ui::chartdialog *ui;


public slots:
    void on_cancelPushButtton_clicked();
    void on_okPushButton_clicked();
    void tiffFileName(QString name);

signals:
    void chartValues(double w,double x,double y,double z);

};

#endif // CHARTDIALOG_H
