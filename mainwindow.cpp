﻿/*************************************************************************************
 *   PATHWAY 1.03.009 (Jetasonic Technologies Inc.)
 *   General User Interface, to display data from a forward looking sonar.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * ************************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "graphdialog.h"
#include "ui_graphdialog.h"
#include "systemsettings.h"
#include "devwindow.h"
#include "ui_devwindow.h"
#include "main.cpp"
#include <QtWidgets>
#include <QtNetwork>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QRegularExpression>
#include <QTimer>
#include <QTime>
#include <algorithm>
#include <QtMath>
#include <QElapsedTimer>
#include <cmath>
#include <QSysInfo>
#include <QList>
#include <QtConcurrent/QtConcurrent>    //Thread
#include <QThreadPool>                  //Thread
#include <QSound>
//#include <QtWebKit>

MainWindow::MainWindow(QWidget *parent) :
//    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->targetListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->waypointListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connectSignals_Slots(); // Set up all Signal/Slot connections

    //     fetchUser();
    line = new QCPItemStraightLine(ui->sensor_graph);
    line_depthTrace= new QCPItemStraightLine(ui->depthCPlot);
    line_depthBoat = new QCPItemLine(ui->depthCPlot);
    line->setVisible(false);
    line_depthTrace->setVisible(false);
    line_depthBoat->setVisible(false);

    /* Setup Plots */
    customPlotLF = ui->customPlot;
    depthPlot = ui->depthCPlot;//depthWindowInstance->getDepthPlot();
    customPlotHF = ui->customPlot2;
    ChartWindowInstance->mapPlotPtr = ui->gpsPlot;

    plot_configureUi(ui->customPlot); //Parametrize the plots
    plot_configureUi(ui->customPlot2);

    //setupPlottingArea(depthWindowInstance->getDepthPlot());
    plot_configureUi(ui->depthCPlot);
    plot_configureUi(ui->sensor_graph);
    plot_configureUi(ui->gpsPlot);
    plot_configureUi(ui->imagePlot);
    //ui->imagePlot->hide();

    //ui->imagePlot-setWidth(400);

    lfBoatPixmap = new QCPItemPixmap(customPlotLF);
    lfBoatPixmap->setParent(customPlotLF);
    lfBoatPixmap->setSelectable(false);

    hfBoatPixmap = new QCPItemPixmap(customPlotHF);
    hfBoatPixmap->setParent(customPlotHF);
    hfBoatPixmap->setSelectable(false);

    depthBoatPixmap = new QCPItemPixmap(depthPlot);
    depthBoatPixmap->setParent(depthPlot);
    depthBoatPixmap->setSelectable(false);

    lfCompassPixmap = new QCPItemPixmap(customPlotLF);
    lfCompassPixmap->setParent(customPlotLF);
    lfCompassPixmap->setSelectable(false);
    QPixmap pixmap (":/icons/icons/Compass.png");
    lfCompassPixmap->setPixmap(pixmap);
    lfCompassPixmap->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->setScaled(true);
    lfCompassPixmap->setLayer("fixedIcons");
    lfCompassPixmap->topLeft->setCoords(30,50);
    lfCompassPixmap->bottomRight->setCoords(100,120);
    lfCompassPixmap->setVisible(false);

    hfCompassPixmap = new QCPItemPixmap(customPlotHF);
    hfCompassPixmap->setParent(customPlotHF);
    hfCompassPixmap->setSelectable(false);
    hfCompassPixmap->setPixmap(pixmap);
    hfCompassPixmap->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->setScaled(true);
    hfCompassPixmap->setLayer("fixedIcons");
    hfCompassPixmap->topLeft->setCoords(30,50);
    hfCompassPixmap->bottomRight->setCoords(100,120);
    hfCompassPixmap->setVisible(false);
    plot_icons_updateCompassIcon();

    lfLegendIcon = new QCPItemPixmap(customPlotLF);
    lfLegendIcon->setParent(customPlotLF);
    lfLegendIcon->setSelectable(false);

    hfLegendIcon = new QCPItemPixmap(customPlotHF);
    hfLegendIcon->setParent(customPlotHF);
    hfLegendIcon->setSelectable(false);
    tools->addAllPanelIcons(customPlotLF, customPlotHF);
    tools->TargetListWidget = ui->targetListWidget;
    tools->WaypointListWidget = ui->waypointListWidget;

    ui->portSide_graph->xAxis->setRange(-1250,1250);
    ui->portSide_graph->yAxis->setRange(0,65000);
    ui->portSide_graph->rescaleAxes(true);
    ui->portSide_graph->addGraph(ui->portSide_graph->xAxis, ui->portSide_graph->yAxis);
    ui->portSide_graph->graph(0)->setPen(QPen(Qt::white));
    ui->portSide_graph->xAxis->setVisible(false);
    ui->portSide_graph->yAxis->setVisible(false);
    ui->portSide_graph->setBackground(QBrush(QColor(50,50,50,255)));
    ui->portSide_graph->xAxis->grid()->setVisible(false);
    ui->portSide_graph->yAxis->grid()->setVisible(false);

    ui->altPlot->xAxis->setRange(-1250,1250);
    ui->altPlot->yAxis->setRange(0,65000);
    ui->altPlot->rescaleAxes(true);
    ui->altPlot->addGraph(ui->altPlot->xAxis, ui->altPlot->yAxis);
    ui->altPlot->graph(0)->setPen(Qt::NoPen/*QPen(Qt::white)*/);
    ui->altPlot->xAxis->setVisible(false);
    ui->altPlot->yAxis->setVisible(false);
    ui->altPlot->setBackground(QBrush(QColor(50,50,50,255)));
    ui->altPlot->xAxis->grid()->setVisible(false);
    ui->altPlot->yAxis->grid()->setVisible(false);

    //    ui->altPlot->axisRect()->setAutoMargins(QCP::msBottom|QCP::msTop|QCP::msRight);
    //    ui->altPlot->axisRect()->setMargins(QMargins(30,0,0,0));

    //    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->customPlot);
    //    ui->customPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, marginGroup);
    //    ui->portSide_graph->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, marginGroup);

    //            QCPMarginGroup *marginGroup2 = new QCPMarginGroup(ui->depthCPlot);
    //            ui->depthCPlot->axisRect()->setMarginGroup(QCP::msLeft, marginGroup2);
    //            ui->altPlot->axisRect()->setMarginGroup(QCP::msLeft, marginGroup2);
    //            ui->altPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, marginGroup2);
    //            ui->depthCPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, marginGroup2);

    //ui->gpsPlot->setMinimumWidth(431);
    //ui->gpsPlot->setMinimumHeight(488);
    customPlotLF->setMultiSelectModifier(Qt::ControlModifier);
    ui->imagePlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectItems);
    ui->imagePlot->setFocusPolicy(Qt::NoFocus);
    setupInterface();
    format_preparePlots();
    resetPlotView();
    plot_replotAll();
    //setPlotRanges();

    //timer is constructed in the main windwo for quick recording
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(showTime()));

    setupPlayerDemo(PLAYER_DEMO);
//    this->layout()->addWidget(tvgWindowInstance);

    this->setMouseTracking(true);
    this->layout()->addWidget(depthAlarm);
    depthAlarm->hide();
    ui->centralWidget->layout()->addWidget(cropTool);
    cropTool->hide();
//    ui->actionCrop_File->setVisible(false);
    QString link = QFileInfo("audio/interface-option-select.wav").absoluteFilePath();
    tools->targetAudio = new QMediaPlayer;
    tools->targetAudio->setMedia(QUrl::fromLocalFile(link));
    link = QFileInfo("audio/pop-sound-effect.mp3").absoluteFilePath();
    tools->notificationAudio = new QMediaPlayer;
    tools->notificationAudio->setMedia(QUrl::fromLocalFile(link));
}

MainWindow :: ~MainWindow()
{
    delete ui;
}

/**********signals & slots funtions And  Setup GUI*****************/

void MainWindow :: setupInterface () {
//    this->setStyleSheet("");
//    userWindowInstance->setStyleSheet()

    gain = userWindowInstance->gain;
    tvgWindowInstance->gain = userWindowInstance->gain;
//    tvgWindowInstance->gain_old = gain_old;
    ping = new SonarPing();
    tvgWindowInstance->ping = ping;
    navSettingInstance->ping = ping;
    userWindowInstance->ping = ping;
    gnssController = new GnssController();
    navSettingInstance->gnssController = gnssController;

    // Setup Menu Bar
    QFont font10;                 //change the Family and color of the menuBar
    font10.setPointSize(10);
    font10.setFamily("Microsoft YaHei");
    ui->menuBar->setFont(font10);
    //    ui->menuBar->setStyleSheet("color: white");

    QAction *helpAction = ui->menuBar->addAction("Settings");
    QAction *formatWindow = ui->menuBar->addAction("Format");
    //   QAction *chartDialog = ui->menuBar->addAction("Chart");

    ui->menuBar->insertMenu(helpAction,ui->menuFile);
    ui->menuBar->insertMenu(formatWindow,ui->menuTools);
    ui->menuBar->addMenu(ui->menuTools);
    ui->menuBar->addMenu(ui->menuHelp);
    ui->menuBar->insertSeparator(helpAction);
    connect(helpAction, SIGNAL(triggered()), this, SLOT(on_actionUserMenu_triggered()));
    connect(formatWindow, SIGNAL(triggered()),this,SLOT(format_actionTriggered()));

    // connect(chartDialog, SIGNAL(triggered()),this, SLOT(actionChartDialog_triggered()));

    // Setup Toolbar
    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->toolBar_Sonar->addWidget(ui->sonarWidget);
    ui->toolBar_GNSS->addWidget(spacer);
    ui->toolBar_GNSS->addWidget(ui->gnssWidget);

    // Setup Status Bar
    //ui->status_zoom->setText("100 %");
    //ui->status_zoom->setStyleSheet("background-color:rgb(255, 0, 0, 0.1)");
    ui->xy_val->setStyleSheet("color: white");
    ui->status_sensor->setStyleSheet("color: white");
    //ui->status_zoom->setStyleSheet("QToolTip{color:#000000;background-color:#ffffff;}");
    //ui->status_zoom->setStyleSheet("QLabel{color:#ffffff}");
    ui->pingNum->setText("ping #:        sample #:");
    //ui->status_zoom->setText("100 %");

    //instantiating the status bar//
    QMainWindow::statusBar();
    avg_label = new QLabel;
    tem_label = new QLabel;
    ping_label = new QLabel;
    time_label = new QLabel;
    depth_label = new QLabel;
    fileSize_lbl = new QLabel;
    file_Name = new QLabel;
    rollValue = new QLabel;
    pitchValue = new QLabel;
    yawValue = new QLabel;
    progressBar = new QProgressBar(ui->statusBar);

    rollValue->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 55, 55);}"));
    pitchValue->setStyleSheet(QStringLiteral("QLabel{color: rgb(153, 204, 255);}"));
    yawValue->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 102);}"));

    QFont font = avg_label->font();
    font.setPointSize(11);

    avg_label->setFont(font);
    tem_label->setFont(font);
    fileSize_lbl->setFont(font);
    file_Name->setFont(font);
    ui->degree_lbl->setFont(font);
    //ui->status_zoom->setFont(font);
    ui->xy_val->setFont(font);
    ping_label->setFont(font);
    time_label->setFont(font);
    depth_label->setFont(font);
    ui->status_sensor->setFont(font);
    rollValue->setFont(font);
    pitchValue->setFont(font);
    yawValue->setFont(font);
    ping_label->setStyleSheet("color: white");
    time_label->setStyleSheet("color: white");
    depth_label->setStyleSheet("color: white");
    avg_label->setStyleSheet("color: white");
    tem_label->setStyleSheet("color: white");
    fileSize_lbl->setStyleSheet("color: white");
    file_Name->setStyleSheet("color: white");
    ui->degree_lbl->setStyleSheet("color: white");

    statusBar()->setStyleSheet("background-color:rgb(0,0,0); padding-right:5px;");
    //statusBar()->addWidget(ui->status_zoom);
    ui->xy_val->hide();
//    statusBar()->addWidget(ui->xy_val);
    statusBar()->addWidget(ping_label);
    statusBar()->addWidget(time_label);
    statusBar()->addWidget(depth_label);
    statusBar()->addWidget(tem_label);
    statusBar()->addWidget(rollValue);
    statusBar()->addWidget(pitchValue);
    statusBar()->addWidget(yawValue);
    //statusBar()->addWidget(avg_label);
    statusBar()->addWidget(ui->degree_lbl);
    statusBar()->addPermanentWidget(file_Name, 1);
    statusBar()->addPermanentWidget(progressBar);
    statusBar()->addPermanentWidget(fileSize_lbl);

    ping_label->setToolTip("Ping");
    time_label->setToolTip("RTC");
    depth_label->setToolTip("Depth");
    tem_label->setToolTip("Temp");
    ui->degree_lbl->setToolTip("Azimuth");

    QFontMetrics fm(font);
    //avg_label->setMinimumWidth(95);
    //ui->status_zoom->setMinimumWidth(100);
    ui->xy_val->setMinimumWidth(fm.horizontalAdvance("_X: 99999.9 Y: 99999.9_"));
    ui->status_sensor->setMinimumWidth(400);
    ping_label->setMinimumWidth(fm.horizontalAdvance("_0000000_"));
    time_label->setMinimumWidth(fm.horizontalAdvance("_00:00:00_"));
    depth_label->setMinimumWidth(fm.horizontalAdvance("_900.0 m_"));
    tem_label->setMinimumWidth(fm.horizontalAdvance("_900.0°C_"));
    rollValue->setMinimumWidth(fm.horizontalAdvance("_Roll: 100.0°_"));
    pitchValue->setMinimumWidth(fm.horizontalAdvance("_Pitch: 100.0°_"));
    yawValue->setMinimumWidth(fm.horizontalAdvance("_Yaw: 100.0°_"));
    ui->degree_lbl->setMinimumWidth(fm.horizontalAdvance("_100.0°_"));
    file_Name->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    progressBar->setMinimumWidth(150);
    progressBar->setMaximumWidth(100);

    file_Name->setAlignment(Qt::AlignRight);
    progressBar->setAlignment(Qt::AlignRight);
    //    avg_label->setAlignment(Qt::AlignCenter);
    tem_label->setAlignment(Qt::AlignCenter);
    ping_label->setAlignment(Qt::AlignCenter);
    time_label->setAlignment(Qt::AlignCenter);
    depth_label->setAlignment(Qt::AlignCenter);
    ui->status_sensor->setAlignment(Qt::AlignCenter);
    //ui->status_zoom->setAlignment(Qt::AlignCenter);
    ui->xy_val->setAlignment(Qt::AlignCenter);
    progressBar->setAlignment(Qt::AlignRight);
    status_resetStatusBar();

    // setup GUI objects
    QFont preferredBold, preferredSizeSlider;
    preferredBold.setBold(true);
    preferredSizeSlider.setPointSize(12);

    on_range_btn_clicked(30);//30 meters

    // Blance Slider
    ui->digitalGainLabel->setText("  0%");
    ui->digitalGainBalance->hide();
    ui->digitalGainLabel->hide();
    ui->digitalGainTxt->hide();

    ui->digitalGainLabel->setFont(preferredBold);                  //change the balance slider
    ui->digitalGainLabel->setFont(preferredSizeSlider);
    ui->digitalGainLabel->setStyleSheet("color: white");
    ui->digitalGainTxt->setFont(preferredBold);
    ui->digitalGainTxt->setFont(preferredSizeSlider);
    ui->digitalGainTxt->setStyleSheet("color: white");


    //Hide the Gain Slider
    ui->gainSlider->hide();
    ui->mainGain_btn->hide();
    ui->mainGain_txt->hide();
    ui->mainGain_btn->setText("  "+QString::number(gain->C_lfOffset)+" dB");
    ui->slider_gainDepth->hide();
    ui->label_gainDepth->hide();
    ui->label_gainDepth->setFont(preferredSizeSlider);
    ui->label_gainDepth->setStyleSheet("color: white");
    ui->label_gainDepthTxt->hide();
    ui->label_gainDepthTxt->setFont(preferredSizeSlider);
    ui->label_gainDepthTxt->setStyleSheet("color: white");

    ui->mainGain_btn->setFont(preferredBold);
    ui->mainGain_btn->setFont(preferredSizeSlider);
//    ui->mainGain_btn->setStyleSheet("color: white");
    ui->mainGain_txt->setFont(preferredBold);
    ui->mainGain_txt->setFont(preferredSizeSlider);
    ui->mainGain_txt->setStyleSheet("color: white");

    ui->hfGainSlider->hide();
    ui->hfGain_btn->hide();
    ui->hfGain_txt->hide();
    ui->hfGain_btn->setText("  "+QString::number(gain->C_hfOffset)+" dB");

    ui->hfGain_btn->setFont(preferredBold);
    ui->hfGain_btn->setFont(preferredSizeSlider);
//    ui->hfGain_btn->setStyleSheet("color: white");
    ui->hfGain_txt->setFont(preferredBold);
    ui->hfGain_txt->setFont(preferredSizeSlider);
    ui->hfGain_txt->setStyleSheet("color: white");
//    ui->label_alt->setFont(preferredBold);
//    ui->label_alt->setFont(QFont("MS Shell Dlg 2", 26, 5, false));
//    ui->label_alt2->setFont(QFont("MS Shell Dlg 2", 16, 5, false));
//    ui->label_alt3->setFont(QFont("MS Shell Dlg 2", 16, 3, false));

    MainWindow :: setWindowTitle(windowTitle);

    /* Add options to menu (right-click) - Gain and Balance Sliders  */
    customPlotLF->setContextMenuPolicy(Qt::ActionsContextMenu);//sets up right click menu on customplotLF
    customPlotLF->addAction(ui->actionFlag);
    customPlotLF->addAction(ui->actionMeasuring_Tape); //add measuring tape to right click menu
    customPlotLF->addAction(ui->actionShadow);

    if (PLAYER_DEMO)    ui->actionSave->setEnabled(false);
    //    QFont save = ui->actionSave->font();
    //    ui->actionSave->set
    //    save.setStyle(Qt::grey)
    //    if (!ui->actionSave->isEnabled())   ui->actionSave->setFont()

    customPlotHF->setContextMenuPolicy(Qt::ActionsContextMenu);//sets up right click menu on customplotLF
    customPlotHF->addAction(ui->actionFlag);
    customPlotHF->addAction(ui->actionMeasuring_Tape);
    customPlotHF->addAction(ui->actionShadow);

    ui->gpsPlot->setContextMenuPolicy(Qt::ActionsContextMenu);
    QAction *clearTrackpoints = new QAction(tr("Clear All Trackpoints"), this);
    ui->gpsPlot->addAction(clearTrackpoints);
    connect(clearTrackpoints, SIGNAL(triggered()), this, SLOT(on_trackpointsBtn_clicked()));
    QAction *loadMap = new QAction(tr("Load Map"), this);
    ui->gpsPlot->addAction(loadMap);
    connect(loadMap, SIGNAL(triggered()), this, SLOT(on_openFileButton_clicked()));

    Format->userFormatSettings();
    userWindowInstance->readUserSettings();//get user values from database

    emit setUpConfigDatabase();
    readConfig(); //read the values from the config database
    //Gnss_setupSerialPort(); // to read the sensor information

    this->move(0,0);

    /* Configure UI (stylesheets and fonts) */
    ui->nav_btn->setStyleSheet("QToolTip{color:#000000;background-color:#ffffff;}");
    ui->gain_btn->setStyleSheet("QToolTip{color:#000000;background-color:#ffffff;}");
    ui->gain_btn->setStyleSheet("background-color: rgb(255, 255, 255);border: none;\n\n");

    ui->hide_btn->setStyleSheet("color: white");

    ui->nav_btn->setVisible(false);
    ui->hide_btn->setVisible(false);

    ui->xy_val->setText("X:0 Y:0");
    ui->degree_lbl->setStyleSheet("color: white");

    setup_buttonsAndTools();
    ui->targetListWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->targetListWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->waypointListWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->waypointListWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->targetListWidget->hide();
    ui->waypointListWidget->show();

    if (ui->auto_btn->isChecked()) {
        ui->auto_btn->setText("H");
        ui->auto_btn->setStyleSheet("color: rgb(255, 255, 255);background-color: rgb(200, 102, 102);");
        // Set up HF ranges
//            if (NumHfRanges < 4)        return 0;
//            ui->btn_range1->setText(QString::number(hfRangeList[0]));
//            ui->btn_range2->setText(QString::number(hfRangeList[1]));
//            ui->btn_range3->setText(QString::number(hfRangeList[2]));
//            ui->btn_range4->setText(QString::number(hfRangeList[3]));
    } else {
        ui->auto_btn->setText("L");
        ui->auto_btn->setStyleSheet("color: rgb(255, 255, 255);background-color: rgb(102, 102, 200);");
//            if (NumLfRanges < 4)        return 0;
//            ui->btn_range1->setText(QString::number(lfRangeList[0]));
//            ui->btn_range2->setText(QString::number(lfRangeList[1]));
//            ui->btn_range3->setText(QString::number(lfRangeList[2]));
//            ui->btn_range4->setText(QString::number(lfRangeList[3]));
    }

//    this->layout()->addWidget(depthAlarmWidget);
//    depthAlarmWidget->move(100,100);
}

/**
 * @brief MainWindow::setupPlottingArea
 * @param customPlot
 * @param depthPlot
 * Called in constructor to paramaterize the plotting area(custom plot and depth plot).
 * Defines main window widgets as well.
 */
void MainWindow :: plot_configureUi(QCustomPlot *customPlot) {
    /*  Define Colors */
    QColor lightBlue;
    lightBlue.setRgb(98, 228, 255,255);
    QColor red;
    red.setRgb(255, 0, 0, 50);
    QColor grey;
    grey.setRgb(50, 50, 50,255);
    QColor silver;
    silver.setRgb(225, 225, 225);
    QColor white;
    white.setRgb(255, 255, 255);
    QColor green;
    green.setRgb(0, 96, 40,255);
    QPen line_color;
    line_color.setWidth(2);
    line_color.setColor(Qt::green);
    QFont preferredBold, preferredSizeAxis, preferredSizeLabel;
    QFont secondSize;//change the bold type of X-Axis and Y-Axis
    preferredSizeAxis.setPointSize(12);
    secondSize.setPointSize(10);
    QPen bluePen;
    bluePen.setColor(QColor(169, 214, 252));
    bluePen.setWidthF(5);

    // Configure QCustomPlot Appearance
    customPlot->setBackground(QBrush(grey));
    customPlot->axisRect()->setBackground(QBrush(Qt::black));

    customPlot->setInteractions(QCP::iRangeDrag /*| QCP::iRangeZoom */| QCP::iSelectItems|QCP::iMultiSelect); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox(true);
    customPlot->axisRect()->setRangeZoomFactor(0.9, 0.9); //Sets the zoom ratio, so when zooming in with mouse wheel both axis are zoomed by the same factor (0.9), re-taining a square zoom

    //customize the line or roll/pitch traceline
    customPlot->xAxis->setTickLabels(false);
    customPlot->xAxis->grid()->setVisible(false);
    customPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);

    customPlot->xAxis2->grid()->setVisible(true);
    customPlot->xAxis2->setTickLabels(true);
    customPlot->xAxis2->setLabelColor(white);
    customPlot->xAxis2->setTickLabelFont(secondSize);//change to size 10
    customPlot->xAxis2->setLabelFont(secondSize);
    customPlot->xAxis2->setTickLabelColor(white);
    customPlot->xAxis2->ticker()->setTickStepStrategy(QCPAxisTicker::tssReadability);
    customPlot->xAxis2->ticker()->setTickCount(10); // tick step shall be 5.0

    customPlot->yAxis->setTickLabels(false);
    customPlot->yAxis->setLabelColor(white);
    customPlot->yAxis->setTickLabelFont(secondSize);
    customPlot->yAxis->setLabelFont(secondSize);
    customPlot->yAxis->setTickLabelColor(white);
    customPlot->yAxis->grid()->setVisible(true);
    customPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->yAxis2->grid()->setVisible(false);

    customPlot->addLayer("colormap"); // Sonar colormaps and ENC chart
    customPlot->addLayer("tools1"); // Target Box and crop tool
    customPlot->addLayer("tools2"); // Target icons and lines
    customPlot->addLayer("tools3"); // Measuring Tape and Target Labels
    customPlot->addLayer("graphs"); // curves on GPSplot and graphs on altplot and zoom boxes
    customPlot->addLayer("trace"); //
    customPlot->addLayer("boatLayer");
    customPlot->addLayer("fixedIcons"); // Compass icon, GPS legend, pause icon and tool icon
    customPlot->moveLayer(customPlot->layer("grid"),customPlot->layer("fixedIcons"));
    customPlot->moveLayer(customPlot->layer("axes"),customPlot->layer("grid"));

//    customPlot->axisRect()->setAutoMargins(/*QCP::msLeft|*/QCP::msTop/*|QCP::msRight*/);
//    customPlot->axisRect()->setMargins(QMargins(0,0,0,0));

    if(customPlot->objectName().compare("customPlot")==0){
        customPlot->setInteractions(/*QCP::iRangeDrag | QCP::iRangeZoom | */QCP::iSelectItems|QCP::iMultiSelect);
        customPlot->xAxis->setTickLabels(true);
        customPlot->xAxis->setTickLabelSide(QCPAxis::lsOutside);
        customPlot->xAxis->setLabelColor(white);
        customPlot->xAxis->setTickLabelFont(secondSize);//change to size 12
        customPlot->xAxis->setLabelFont(secondSize);
        customPlot->xAxis->setTickLabelColor(white);
        customPlot->xAxis->ticker()->setTickStepStrategy(QCPAxisTicker::tssReadability);
        customPlot->xAxis->ticker()->setTickCount(10); // tick step shall be 5.0

        customPlotLfLegend = new QCPItemText(customPlot);
        customPlotLfLegend->setLayer("fixedIcons");
        customPlotLfLegend->setFont(QFont("sans", 12));
        customPlotLfLegend->setColor(Qt::white);
        customPlotLfLegend->position->setTypeX(QCPItemPosition::ptAbsolute);
        customPlotLfLegend->position->setTypeY(QCPItemPosition::ptAbsolute);
        customPlotLfLegend->position->setCoords(40, 100); //80
        customPlotLfLegend->setPositionAlignment(Qt::AlignLeft|Qt::AlignBottom);
        customPlotLfLegend->setBrush(QBrush(Qt::black));
        customPlotLfLegend->setText("");
//        customPlot->moveLayer(customPlot->layer("grid"),customPlot->layer("colormap"));

        crosshair_lf.add(customPlot);

        legendText = new QCPTextElement(customPlot);
        customPlot->legend->setVisible(true);
        customPlot->legend->clearItems();
        customPlot->legend->setBrush(QBrush(Qt::NoBrush));
        customPlot->legend->setLayer("fixedIcons");
        customPlot->legend->setBorderPen(QPen(Qt::NoPen));
        legendText->setLayer("fixedIcons");
        legendText->setTextColor(Qt::white);
        legendText->setFont(QFont("sans", 20, QFont::Bold));
        customPlot->legend->setBrush(QBrush(Qt::NoBrush));
        legendText->setText("");
        customPlot->legend->addElement(0,0,legendText);
        customPlot->setAutoAddPlottableToLegend(false);

        // make sure the axisrect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->customPlot);
        customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

        colorScale = new QCPColorScale(customPlot);
        //assigning the color range to color scale according to output bits
        QCPRange colorRange = QCPRange(0, 8191); // 13 bit color scale
        colorScale->setDataRange(colorRange);
        colorScale->setRangeDrag(false);
        colorScale->setRangeZoom(false);
        colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        colorScale->axis()->setVisible(false);
        colorScale->axis()->setTickLabelColor(white);
        colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        //customPlot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
        customPlot->layer("main")->setMode(QCPLayer::lmBuffered);
    }
    else if (customPlot->objectName().compare("sensor_graph")==0) {
        customPlot->xAxis2->ticker()->setTickCount(3);
        customPlot->xAxis2->setRange(-10,10);
        customPlot->xAxis2->setTickLabelSide(QCPAxis::lsInside);

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(0)->setPen(bluePen);
        customPlot->graph(0)->setName("Roll");
        customPlot->graph(0)->setLayer("graphs");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(1)->setPen(bluePen);
        customPlot->graph(1)->setName("Pitch");
        customPlot->graph(1)->setLayer("graphs");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(2)->setName("Heading");

        // make sure the axisrect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlot);
        customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        customPlot->layer("main")->setMode(QCPLayer::lmBuffered);

        QCPLegend *arLegend = customPlot->legend;
        arLegend->setIconSize(5,5);
        customPlot->plotLayout()->addElement(0,0,arLegend);
        customPlot->legend->setVisible(false);
        line->setPen(line_color);
        line->setSelectedPen(line_color);
        line->setLayer("tools1");
    }
    else if (customPlot->objectName().compare("depthCPlot")==0) {
        customPlot->setInteractions(QCP::iRangeDrag | QCP::iSelectItems);
        customPlot->xAxis2->ticker()->setTickCount(3);
        customPlot->xAxis2->setTickLabelSide(QCPAxis::lsInside);

        QLinearGradient depthPlotBackground2;
        depthPlotBackground2.setStart(350, 0);
        depthPlotBackground2.setFinalStop(0, 0);
        depthPlotBackground2.setColorAt(0.5, QColor(160,82,45));
        depthPlotBackground2.setColorAt(0.2, QColor(210,105,30));
        depthPlotBackground2.setColorAt(0.5, QColor(160,82,45));
        depthPlotBackground2.setColorAt(1, QColor(128,0,0));

        QPen brownPen, greenPen, greyPen;
        QColor color(0,128,255), color2(0,0,0), color3(255,255,255);

        QPen redPen2;
        redPen2.setWidthF(5);
        redPen2.setColor(Qt::red);
        redPen2.setStyle(Qt::DashLine);

        greyPen.setColor(color3);
        greenPen.setWidthF(5);
        greenPen.setColor(Qt::green);
        brownPen.setColor(color2);
        line_depthTrace->setPen(line_color);
        line_depthTrace->setSelectedPen(line_color);
        line_depthBoat->setPen(redPen2);
        line_depthBoat->setSelectedPen(redPen2);
        line_depthBoat->start->setCoords(0, 0);
        line_depthBoat->end->setCoords(0, 0);
        line_depthBoat->setLayer("tools1");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(0)->setPen(bluePen);
        customPlot->graph(0)->setName("Average Depth");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(1)->setPen(bluePen);
        customPlot->graph(1)->setName("Average Depth");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(2)->setPen(greenPen);
        customPlot->graph(2)->setName("Average Depth");
        customPlot->graph(2)->setLayer("graphs");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(3)->setBrush(QBrush(color2));  //black
        customPlot->graph(3)->setChannelFillGraph(customPlot->graph(2));
        customPlot->graph(3)->setPen(color2);
        customPlot->graph(3)->setLayer("graphs");

        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(4)->setBrush(QBrush(color3));  //white
        customPlot->graph(4)->setPen(greyPen);
        customPlot->graph(4)->setChannelFillGraph(customPlot->graph(3));
        customPlot->graph(4)->setLayer("graphs");

        QPen redPen;
        redPen.setColor(QColor(252, 100, 100));
        redPen.setWidthF(5);
        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(5)->setPen(bluePen);
        customPlot->addGraph(customPlot->yAxis, customPlot->xAxis);
        customPlot->graph(6)->setPen(redPen);
        customPlot->graph(5)->setLayer("graphs");
        customPlot->graph(6)->setLayer("graphs");

        setTransparency(50,1);

        /**the order of the setlayer of the graph should be maintained**/
        customPlot->graph(3)->setLayer("graphs");
        customPlot->graph(4)->setLayer("graphs");
        customPlot->graph(2)->setLayer("graphs");
        customPlot->graph(1)->setLayer("graphs");
        customPlot->graph(0)->setLayer("graphs");
        line_depthTrace->setLayer("trace");

//        customPlot->moveLayer(customPlot->layer("grid"),customPlot->layer("colormap"));

        newDepthcolorScale = new QCPColorScale(customPlot);
        //assigning the color range to color scale according to output bits
        QCPRange colorRange = QCPRange(0, 8191);
        newDepthcolorScale->setDataRange(colorRange);
        newDepthcolorScale->setRangeDrag(false);
        newDepthcolorScale->setRangeZoom(false);
        newDepthcolorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        newDepthcolorScale->axis()->setVisible(true);
        newDepthcolorScale->axis()->setTickLabelColor(white);
        // make sure the axisrect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->customPlot);
        customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        newDepthcolorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        //            customPlot->plotLayout()->addElement(0, 1, newDepthcolorScale); // add it to the right of the main axis rect
        customPlot->layer("main")->setMode(QCPLayer::lmBuffered);

        //        customPlot->legend->setVisible(true);
        //        customPlot->legend->clearItems();
        //        customPlot->legend->setBrush(QBrush(Qt::NoBrush));
        customPlot->legend->setLayer("colormap");
        //        customPlot->legend->setBorderPen(QPen(Qt::NoPen));

        //        QCPMarginGroup *marginGroup2 = new QCPMarginGroup(ui->depthCPlot);
        //        ui->depthCPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, marginGroup2);
        //        ui->altPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, marginGroup2);

        depthLegend = new QCPItemText(customPlot);
        depthLegend->setLayer("fixedIcons");
        depthLegend->setFont(QFont("sans", 10, QFont::Bold));
        depthLegend->setColor(Qt::white);
//        depthLegend->position->setTypeX(QCPItemPosition::ptAbsolute);
//        depthLegend->position->setTypeY(QCPItemPosition::ptAbsolute);
        depthLegend->position->setCoords(60, 80);
        depthLegend->setPositionAlignment(Qt::AlignRight|Qt::AlignTop);
        depthLegend->setBrush(QBrush(Qt::black));
        depthLegend->setText("");

        depthLegend2 = new QCPItemText(customPlot);
        depthLegend2->setLayer("fixedIcons");
        depthLegend2->setFont(QFont("sans", 14, QFont::Bold));
        depthLegend2->setColor(QColor(255, 255, 100));
        depthLegend2->position->setTypeX(QCPItemPosition::ptAbsolute);
        depthLegend2->position->setTypeY(QCPItemPosition::ptAbsolute);
        depthLegend2->position->setCoords(60, 80);
        depthLegend2->setPositionAlignment(Qt::AlignLeft|Qt::AlignTop);
        depthLegend2->setBrush(QBrush(Qt::black));
        depthLegend2->setText("");

        customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop);//setAutoMargins(QCP::msLeft|QCP::msTop);
        customPlot->setAutoAddPlottableToLegend(false);

//        DepthAlarmHandle = new QCPItemPixmap(customPlot);
//        DepthAlarmHandle->setParent(depthPlot);
//        DepthAlarmHandle->setSelectable(true);

        depthAlarm->handle = new QCPItemLine(customPlot);
        depthAlarm->handle->setPen(redPen2);
        depthAlarm->handle->setSelectedPen(redPen2);
        depthAlarm->handle->start->setCoords(10, 0);
        depthAlarm->handle->end->setCoords(10, 10);
        depthAlarm->handle->setLayer("tools1");
    }
    else if (customPlot->objectName().compare("customPlot2")==0) {
        customPlot->setInteractions(/*QCP::iRangeDrag | QCP::iRangeZoom | */QCP::iSelectItems|QCP::iMultiSelect);

        customPlot->xAxis->setTickLabels(true);
        customPlot->xAxis->setTickLabelSide(QCPAxis::lsOutside);
        customPlot->xAxis->setLabelColor(white);
        customPlot->xAxis->setTickLabelFont(secondSize);//change to size 12
        customPlot->xAxis->setLabelFont(secondSize);
        customPlot->xAxis->setTickLabelColor(white);
        customPlot->xAxis->ticker()->setTickStepStrategy(QCPAxisTicker::tssReadability);
        customPlot->xAxis->ticker()->setTickCount(10); // tick step shall be 5.0

        //customPlot->xAxis2->grid()->setVisible(false);

        customPlotHfLegend = new QCPItemText(customPlot);
        customPlotHfLegend->setLayer("fixedIcons");
        customPlotHfLegend->setFont(QFont("sans", 12));
        customPlotHfLegend->setColor(Qt::white);
        customPlotHfLegend->setBrush(QBrush(Qt::black));
        customPlotHfLegend->position->setTypeX(QCPItemPosition::ptAbsolute);
        customPlotHfLegend->position->setTypeY(QCPItemPosition::ptAbsolute);
        customPlotHfLegend->position->setCoords(40, 100);
        customPlotHfLegend->setPositionAlignment(Qt::AlignLeft|Qt::AlignBottom);
        customPlotHfLegend->setText("");
//        customPlot->moveLayer(customPlot->layer("grid"),customPlot->layer("colormap"));

        crosshair_hf.add(customPlot);

        legendTextHf = new QCPTextElement(customPlot);
        customPlot->legend->setVisible(true);
        customPlot->legend->clearItems();
        customPlot->legend->setBrush(QBrush(Qt::NoBrush));
        customPlot->legend->setLayer("grid");
        customPlot->legend->setBorderPen(QPen(Qt::NoPen));
        legendTextHf->setLayer("grid");
        legendTextHf->setTextColor(Qt::white);
        legendTextHf->setFont(QFont("sans", 20, QFont::Bold));
        customPlot->legend->setBrush(QBrush(Qt::NoBrush));
        legendTextHf->setText("");
        customPlot->legend->addElement(0,0,legendTextHf);
        customPlot->setAutoAddPlottableToLegend(false);

        colorScaleHF = new QCPColorScale(customPlot);
        //assigning the color range to color scale according to output bits
        QCPRange colorRange = QCPRange(0, 8191);
        colorScaleHF->setDataRange(colorRange);
        colorScaleHF->setRangeDrag(true);
        colorScaleHF->setRangeZoom(false);
        // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        colorScaleHF->setType(QCPAxis::atRight);
        colorScaleHF->axis()->setVisible(false);
        colorScaleHF->axis()->setTickLabelColor(white);
        // make sure the axisrect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->customPlot2);
        customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        colorScaleHF->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        //        customPlot->plotLayout()->addElement(0, 1, colorScaleHF); // add it to the right of the main axis rect
        customPlot->layer("main")->setMode(QCPLayer::lmBuffered);
    }
    else if (customPlot->objectName().compare("gpsPlot")==0) {
        ChartWindowInstance->mapPlotPtr = ui->gpsPlot;
        ChartWindowInstance->plot_setupItems(ui->gpsPlot);
        tools->tracker.setupPlotItems(customPlot);

        customPlot->xAxis->grid()->setVisible(false);
        customPlot->xAxis->setTickLabels(false);
        customPlot->xAxis->setTickLabelSide(QCPAxis::lsInside);
        customPlot->xAxis->setTickLabelRotation(-70);
        customPlot->xAxis->ticker()->setTickCount(5);
        customPlot->xAxis->setTickLabelColor(white);
        customPlot->xAxis->setLabelColor(white);
        customPlot->xAxis->setTickLabelFont(secondSize);//change to size 10
        customPlot->xAxis->setLabelFont(secondSize);
        customPlot->xAxis2->setTickLabels(false);
        customPlot->xAxis2->grid()->setVisible(false);

        customPlot->yAxis->grid()->setVisible(false);
        customPlot->yAxis->setTickLabelColor(white);
        customPlot->yAxis->setLabelColor(white);
        customPlot->yAxis->setTickLabels(false);
        customPlot->yAxis->setTickLabelRotation(-20);
        customPlot->yAxis->setTickLabelSide(QCPAxis::lsInside);
        customPlot->setAutoAddPlottableToLegend(false);

        customPlot->setInteractions(QCP::iRangeDrag | QCP::iSelectItems);
    }
    else if (customPlot->objectName().compare("sensor_graph")==0) {
        customPlot->yAxis->setRangeReversed(true);
        customPlot->yAxis->setTickLabels(false);
        customPlot->xAxis->setTickLabels(false);

        customPlot->addGraph(customPlot->xAxis, customPlot->yAxis);
        customPlot->graph(0)->setPen(bluePen);
        customPlot->graph(0)->setName("Average Depth");
        customPlot->graph(0)->setLayer("graphs");

        customPlot->addGraph(customPlot->xAxis, customPlot->yAxis);
        customPlot->graph(1)->setPen(bluePen);
        customPlot->graph(1)->setName("Average Depth");
        customPlot->graph(1)->setLayer("graphs");

        customPlot->addGraph(customPlot->xAxis, customPlot->yAxis);
        customPlot->graph(2)->setPen(bluePen);
        customPlot->graph(2)->setName("Average Depth");
        customPlot->graph(2)->setLayer("graphs");

        QCPItemText *kalmanDisplay = new QCPItemText(customPlot);
        //        kalmanDisplay->setLayer("readout");
        kalmanDisplay->setPositionAlignment(Qt::AlignTop|Qt::AlignRight);
        kalmanDisplay->setColor(Qt::black);
        kalmanDisplay->position->setType(QCPItemPosition::ptAxisRectRatio);
        kalmanDisplay->setText("--");
        kalmanDisplay->setFont(QFont(font().family(), 18));

        QCPRange colorRange = QCPRange(0, 8191);
        newDepthcolorScale = new QCPColorScale(customPlot);
        newDepthcolorScale->setType(QCPAxis::atRight);
        newDepthcolorScale->setDataRange(colorRange);

        QCPColorGradient depthCustomColor;
        //setup depth plot colorscale gradient
        depthCustomColor.clearColorStops();
        depthCustomColor.setColorStopAt(0,QColor(255,255,255));
        depthCustomColor.setColorStopAt(0.02,QColor(204,204,0));
        depthCustomColor.setColorStopAt(0.06,QColor(63,42,20));
        depthCustomColor.setColorStopAt(0.5,QColor(75,0,130));
        depthCustomColor.setColorStopAt(0.9,QColor(255,0,0));
        depthCustomColor.setColorStopAt(1.0,QColor(0,0,0));
        depthCustomColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
        newDepthcolorScale->setGradient(depthCustomColor);

        newDepthcolorScale->setDataRange(colorRange);
        QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlot);
        customPlot->plotLayout()->addElement(0, 1, newDepthcolorScale);
        customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop,marginGroup);
        newDepthcolorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        newDepthcolorScale->axis()->setVisible(true);
        newDepthcolorScale->axis()->setTickLabelColor(Qt::white);
        customPlot->layer("main")->setMode(QCPLayer::lmBuffered);
        customPlot->axisRect()->setupFullAxesBox(true);
        customPlot->replot();
    }
    else if (customPlot->objectName().compare("imagePlot")==0) {
        QCPColorScale *colorScaleImage = new QCPColorScale(customPlot);
        //assigning the color range to color scale according to output bits
        QCPRange colorRange = QCPRange(0, 8191);
        colorScaleImage->setDataRange(colorRange);
        colorScaleImage->setRangeDrag(false);
        colorScaleImage->setRangeZoom(false);
        colorScaleImage->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        colorScaleImage->axis()->setVisible(false);

        customPlot->xAxis2->grid()->setVisible(false);
        customPlot->xAxis2->setTickLabels(false);

        customPlot->yAxis->grid()->setVisible(false);

        //        customPlot->xAxis2->setLabelPadding(0);
        //        customPlot->xAxis2->setOffset(0);
        customPlot->axisRect()->setAutoMargins(QCP::msNone);
        customPlot->axisRect()->setMargins(QMargins(0,0,0,0));
    }

    customPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: format_preparePlots() {
    /* Prepare the custom plots to show LF/HF/Depth based on the
     * format settings and sonar configuration */

//    Format->userFormatSettings();
    format_updatePlotSizes();
    plot_replotAll(1);
    depthPlot->graph(0)->setVisible(false);
    depthPlot->graph(1)->setVisible(false);
    depthPlot->graph(2)->setVisible(true);
    if (state.GUI_MODE == 1 && state.PLAY_MODE == 0 && state.RT_MODE == 0) {
        plot_offset_zoomResetHF(nullptr);
    }

    // If plotting side scan, then disable "showGap"
    //  Otherwise set showGap based on userMenu setting
    flag.showGap = userWindowInstance->enableGap(flag.PlotAngleScan);

    if (!flag.PlotAngleScan) {
        // If the data does not contain a depth channel, then don't show the plot and lock the splitter
        ui->splitter_depth->setStyleSheet("QSplitter::handle:horizontal {\nbackground: #aaa;\nborder: 0px solid #777;"
                                      "\nwidth: 0px;\nmargin-left: 0px;\nmargin-right: 0px;\nborder-radius: 0px;\n}");
        ui->splitter_depth->setHandleWidth(0);
        ui->splitter_depth->setEnabled(false);
        if (Format->isOpen_DepthPlot) {
            format_showDepthPlot(false);
            ui->splitter_sonar->setSizes({ui->splitter_depth->width(), (ui->splitter_sonar->width()-ui->splitter_depth->width())});
        }
    } else {
        // Unlock the splitter
        ui->splitter_depth->setStyleSheet("QSplitter::handle:horizontal {\nbackground: #aaa;\nborder: 2px solid #777;"
                                      "\nwidth: 4px;\nmargin-left: 10px;\nmargin-right: 10px;\nborder-radius: 4px;\n}");
        ui->splitter_depth->setHandleWidth(5);
        ui->splitter_depth->setEnabled(true);
    }
    ui->btn_depth->setEnabled(flag.PlotAngleScan);

    if (!flag.PlotHfSidescan && Format->isOpenHf) {
        // If the data does not contain a HF channel, then don't show the plot
        format_showHfPlot(false);
    }

    // Allow options on format window to be (or not) clickable
    format_enableGnssFeatures(state.GNSS_MODE == 2);
    emit showHfClickable(flag.PlotHfSidescan);
    emit showAltimeterClickable(flag.PlotAngleScan);
}

/**
 * @brief MainWindow::connectSignals_Slots
 * To connect all the signals and slots throughout the application.
 * This function is called in the constructor.
 * When you emit signals, it triggers their connected slot
 */
void MainWindow :: connectSignals_Slots() {
    // TCP
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(Tcp_receiveDisconnection()));
    connect(tcpSocket, SIGNAL(readyRead()),this, SLOT(Tcp_readData()));

    // Mouse actions
    connect(ui->customPlot, &QCustomPlot::mouseMove, this, &MainWindow::plot_onMouseHover);
    connect(ui->sensor_graph, &QCustomPlot::mouseMove, this, &MainWindow::plot_onMouseHover);
    connect(ui->customPlot2, &QCustomPlot::mouseMove, this, &MainWindow::plot_onMouseHover);
    connect(ui->depthCPlot, &QCustomPlot::mouseMove, this, &MainWindow::plot_onMouseHover);
    connect(ui->gpsPlot, &QCustomPlot::mouseMove, this, &MainWindow::plot_tools_onMouseMoveGps);
    //     connect(ui->depthCPlot, &QCustomPlot::mouseMove, this, &MainWindow::onDepthMouseMove);
    //        connect(ui->flagList, &QCustomPlot::mousePress, this, &MainWindow::onMousePressed);
    connect(ui->customPlot, &QCustomPlot::mousePress, this, &MainWindow::plot_Lf_onMousePressed);
    connect(ui->customPlot2, &QCustomPlot::mousePress, this, &MainWindow::plot_Hf_onMousePressed);
    connect(ui->gpsPlot, &QCustomPlot::mousePress, this, &MainWindow::plot_Gnss_onMousePressed);
    connect(ui->imagePlot, &QCustomPlot::mousePress, this, &MainWindow::plot_image_onMousePressed);

    connect(ui->customPlot, &QCustomPlot::mouseRelease, this, &MainWindow::plot_Lf_onMouseReleased);
    connect(ui->customPlot2, &QCustomPlot::mouseRelease, this, &MainWindow::plot_Hf_onMouseReleased);
    connect(ui->gpsPlot, &QCustomPlot::mouseRelease, this, &MainWindow::plot_Gnss_onMouseReleased);

    connect(ui->customPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(plot_range_zoomMouseEvent(QWheelEvent*)));
    connect(ui->customPlot2, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(plot_range_zoomMouseEvent(QWheelEvent*)));
    connect(ui->depthCPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(plot_range_zoomMouseEvent(QWheelEvent*)));
    connect(ui->sensor_graph, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(plot_range_zoomMouseEvent(QWheelEvent*)));

    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(plot_range_zoomUpdate()));
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(plot_range_zoomUpdate()));
    connect(ui->customPlot2->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(plot_range_zoomUpdate()));
    connect(ui->customPlot2->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(plot_range_zoomUpdate()));

    connect(ui->depthCPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(plot_range_zoomUpdate()));
    connect(ui->sensor_graph->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(plot_range_zoomUpdate()));

    //    connect(ui->gpsPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ChartWindowInstance, SLOT(resizeAllTargets()));
    //    connect(ui->gpsPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ChartWindowInstance, SLOT(resizeAllTargets()));

    //    connect(ui->gpsPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(gpsPlotReplot()));
    //    connect(ui->gpsPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(gpsPlotReplot()));

    connect(ui->gpsPlot, SIGNAL(mouseWheel(QWheelEvent*)), ChartWindowInstance, SLOT(plot_gpsZoom(QWheelEvent*)));

    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(plot_click_activateMouseFlag(QMouseEvent*)));
    connect(ui->customPlot2, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(plot_click_activateMouseFlag(QMouseEvent*)));
    connect(ui->depthCPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(plot_click_activateMouseFlag(QMouseEvent*)));
    connect(ui->sensor_graph, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(plot_click_activateMouseFlag(QMouseEvent*)));
    connect(ui->customPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(plot_click_deactivateMouseFlag()));
    connect(ui->customPlot2, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(plot_click_deactivateMouseFlag()));
    connect(ui->depthCPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(plot_click_deactivateMouseFlag()));
    connect(ui->sensor_graph, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(plot_click_deactivateMouseFlag()));

    connect(ui->customPlot, &QCustomPlot::mouseDoubleClick, this, &MainWindow::plot_offset_zoomResetLF);
    connect(ui->customPlot2, &QCustomPlot::mouseDoubleClick, this, &MainWindow:: plot_offset_zoomResetHF);
    connect(ui->imagePlot, &QCustomPlot::mouseDoubleClick, this, &MainWindow::plot_offset_zoomResetImagePlot);
    connect(ui->sensor_graph, &QCustomPlot::mouseDoubleClick, this, &MainWindow:: plot_offset_zoomResetSensor);
    connect(ui->depthCPlot, &QCustomPlot::mouseDoubleClick, this, &MainWindow:: plot_offset_zoomResetDepth);
    connect(ui->imagePlot, &QCustomPlot::mouseDoubleClick, this, &MainWindow:: plot_offset_zoomResetImage);
    connect(ui->gpsPlot, &QCustomPlot::mouseDoubleClick, this, &MainWindow::plot_offset_zoomResetGps);

//    connect(ui->flagList, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(plot_click_activateMouseFlag(QMouseEvent*)));
//    connect(ui->depthCPlot, &QCustomPlot::mouseMove, this, &MainWindow::depthAlarm_moveHandle);

//    connect(ui->depthCPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(depthAlarm_selectHandle(QMouseEvent*)));
//    connect(ui->depthCPlot, &QCustomPlot::mouseMove, this, &MainWindow::depthAlarm_moveHandle);
//    connect(ui->depthCPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(depthAlarm_deselectHandle(QMouseEvent*)));

    // Timers
    if (!PLAYER_DEMO)    connect(&simulationTimer, SIGNAL(timeout()), this, SLOT(data_simulate()));

    connect(&readFileTimer, SIGNAL(timeout()), this, SLOT(player_sortFileData()));
    connect(&sortFileTimer, SIGNAL(timeout()), this, SLOT(xtfConverter_SortFileData()));
    connect(&zoomRatioTimer, SIGNAL(timeout()), this, SLOT(plot_range_hideZoomRatioLabel()));
    connect(&replotTimer, SIGNAL(timeout()), this, SLOT(plot_replotAll()));
    connect(&gainSliderTimer, SIGNAL(timeout()), this, SLOT(hideGainSliders()));
    connect(&mouseTimer, SIGNAL(timeout()), this, SLOT(mouseCursorTimeout()));
    connect(&depthAlarm->timer, SIGNAL(timeout()), this, SLOT(depthAlarm_run()));
    connect(depthAlarm, SIGNAL(stopAlarm()), this, SLOT(depthAlarm_run()));
    connect(&depthAlarm->hideTimer, SIGNAL(timeout()), this, SLOT(depthAlarm_hide()));
    connect(&track.signalTimer, SIGNAL(timeout()), this, SLOT(bottomTrack_updateIcon()));

    // connect(&replotTimer, SIGNAL(timeout()), this, SLOT(convertSortFileData()));

    //connect(&UsbTimer, SIGNAL(timeout()), this, SLOT(processData())); //USB Timer
    //connect(&UsbTimer, SIGNAL(timeout()), this, SLOT(send_nmea_zda())); //NMEA messages test
    //connect(&UsbTimer, SIGNAL(timeout()), this, SLOT(send_nmea_gga())); //NMEA messages test

    //user menu signal connections
    connect(userWindowInstance,SIGNAL(emitDepthGradient(QCPColorGradient)),this,SLOT(plot_colorScale_getDepthGrad(QCPColorGradient)));
    connect(userWindowInstance,SIGNAL(emitCustomGradient(QCPColorGradient)),this,SLOT(plot_colorScale_getCustomGrad(QCPColorGradient)));
    connect(userWindowInstance,SIGNAL(updateCL(double)),this,SLOT(updateCableLength(double)));
    connect(userWindowInstance,SIGNAL(updateSS(double)),this,SLOT(updateSpeedOfSound(double)));
    connect(this,SIGNAL(updateRecPath(QString)),userWindowInstance,SLOT(updateDirPath(QString)));
    connect(this,SIGNAL(updateConverterDirPath(QString)),converterDialogInstance, SLOT(updateConverterDirPath(QString)));
    connect(this,SIGNAL(changeFreq(bool)),userWindowInstance,SLOT(updatelfMain(bool)));
    connect(this,SIGNAL(changeSS(double)),userWindowInstance,SLOT(changeSoundSpeed(double)));
    connect(this,SIGNAL(changeCL(double)),userWindowInstance,SLOT(changeCableLength(double)));

    connect(systemSettingInstance,SIGNAL(ethernetActive(bool)), this, SLOT(receiveEthernetActive(bool)));
    connect(userWindowInstance,SIGNAL(sendBoatSpeed(int)), this, SLOT(receiveBoatSpeed(int)));
    connect(userWindowInstance, SIGNAL(sendDepthRange(int)), this, SLOT(receiveDepthRange(int)));
    connect(userWindowInstance,SIGNAL(send_alt_length(int)), this, SLOT(receive_alt_length(int)));
    connect(userWindowInstance,SIGNAL(send_yaw_length(int)), this, SLOT(receive_yaw_length(int)));
    connect(this, SIGNAL(sendDisableBoatSpeed(bool)), userWindowInstance, SLOT(receiveDisableBoatSpeed(bool)));
    //connect(userWindowInstance,SIGNAL(signalToMain(bool,bool,bool)), this,SLOT(activeSignal(bool, bool, bool)));

    connect(userWindowInstance, SIGNAL(depthLabelHide(bool)),this,SLOT(depthLabelHide(bool)));
    connect(userWindowInstance, SIGNAL(tractionStripHide(bool)),this,SLOT(tractionStripHide(bool)));

    connect(this, SIGNAL(clickCounter(int,int,int)), userWindowInstance, SLOT(clickCounter(int,int,int)));


    // TVG tab
    connect(tvgWindowInstance,SIGNAL(tvgWindowClosed()), this,SLOT(ui_updateButtons()));
    connect(tvgWindowInstance,SIGNAL(toggleTVG(bool)), this,SLOT(setTvg(bool)));
    connect(tvgWindowInstance,SIGNAL(GainsToMainwindow(bool,  QString, int, int, int)),this,
            SLOT(GainsFromUsermenu(bool, QString, int, int, int)));
    connect(this,SIGNAL(GainsToUsermenu(int,int,int)),tvgWindowInstance,SLOT(GainsFromMainwindow(int,int,int)));
    connect(this,SIGNAL(freqToUsermenu()),tvgWindowInstance,SLOT(freqFromMainwindow()));
    connect(userWindowInstance, SIGNAL(updateTvgWindow()), tvgWindowInstance, SLOT(updateTvgVariablesFromUserMenu()));
    connect(tvgWindowInstance, SIGNAL(saveGainSettings()), userWindowInstance, SLOT(saveGainSettings()));
    connect(tvgWindowInstance, SIGNAL(resetGainSettings()), userWindowInstance, SLOT(resetGainDefaults()));
    connect(tvgWindowInstance, SIGNAL(closeEvent(QCloseEvent*)), this, SLOT(closeTvg()));
    connect(tvgWindowInstance,SIGNAL(send_default_range(int,int)),systemSettingInstance,SLOT(receive_default_range(int,int)));
    connect(tvgWindowInstance, SIGNAL(updateMainPlots(GainSettings::channelType, GainSettings, int)), this, SLOT(plot_data_updateTVG(GainSettings::channelType, GainSettings, int)));
    connect(systemSettingInstance,SIGNAL(send_dynamic_range(int,int)),tvgWindowInstance,SLOT(get_Dynamic_range(int,int)));
    // Compass Tab
    connect(userWindowInstance,SIGNAL(sendCompass(int)), this, SLOT(compass_receive(int))); //Compass tab
    connect(userWindowInstance,SIGNAL(sendDateData(int)), this, SLOT(receiveDataToSend(int))); //Compass tab
    connect(ui->gainSlider, SIGNAL(valueChanged(int)), this, SLOT(gainSlider_userMovedSliderLf(int)));
    connect(ui->hfGainSlider, SIGNAL(valueChanged(int)), this, SLOT(gainSlider_userMovedSliderHf(int)));
    connect(ui->slider_gainDepth, SIGNAL(valueChanged(int)), this, SLOT(gainSlider_userMovedSliderDepth(int)));

    connect(this,SIGNAL(sendLFDGain(double, double)),userWindowInstance,SLOT(setLFDGain(double, double)));
    connect(this,SIGNAL(sendLFBalance(int)),userWindowInstance,SLOT(setLFBalance(int)));
    connect(userWindowInstance,SIGNAL(sendLFGains(double,double,int)),this,SLOT(applyGains(double,double,int)));

    connect(userWindowInstance,SIGNAL(sendGainAdd(int)),systemSettingInstance,SLOT(on_gainAddBox_valueChanged(int)));
    connect(this,SIGNAL(sendNoiseThresh(double)),userWindowInstance,SLOT(setNoiseThreshold(double)));
    connect(this,SIGNAL(sendFilterSize(int)),userWindowInstance,SLOT(setFSize(int)));
    connect(userWindowInstance,SIGNAL(sendFSize(int)),systemSettingInstance,SLOT(on_filterSizeBox_valueChanged(int)));
    connect(userWindowInstance,SIGNAL(compassClicked(bool)),this,SLOT(compass_receiveClicked(bool)));
    connect(userWindowInstance, SIGNAL(setTransparency(int,int)), this, SLOT(setTransparency(int,int)));
    connect(userWindowInstance, SIGNAL(setAltGradient(int)), this, SLOT(setAltGradient(int)));
    connect(userWindowInstance, SIGNAL(depthLabelHide(bool)),this,SLOT(depthLabelHide(bool)));

    //CreateFlag signal connections
    connect(flagInstance,SIGNAL(flagInfo(QString, targetItem*, bool)),this,SLOT(target_DialogOkay(QString, targetItem*, bool)));

    connect(ui->splitter_sonarGnss, SIGNAL(splitterMoved(int, int)), this, SLOT(format_splitterHandleChanged()));
    connect(ui->splitter_Gnss,SIGNAL(splitterMoved(int, int)), this, SLOT(format_splitterHandleChanged()));
    connect(ui->splitter_depth, SIGNAL(splitterMoved(int, int)), this, SLOT(format_splitterHandleChanged()));
    connect(ui->splitter_sonar, SIGNAL(splitterMoved(int, int)), this, SLOT(format_splitterHandleChanged()));
    connect(ui->splitter_tools, SIGNAL(splitterMoved(int, int)), this, SLOT(format_splitterHandleChanged()));
    connect(ui->splitter_LfHf, SIGNAL(splitterMoved(int, int)), this, SLOT(format_splitterHandleChanged()));

    // Flag Interaction
    connect(ui->customPlot, &QCustomPlot::selectionChangedByUser, this, &MainWindow::plot_Lf_PlotItemSelectionChanged);
    connect(ui->customPlot2, &QCustomPlot::selectionChangedByUser, this, &MainWindow::plot_Hf_PlotItemSelectionChanged);
    connect(ui->gpsPlot, &QCustomPlot::selectionChangedByUser, this, &MainWindow::plot_Gnss_PlotItemSelectionChanged);

    //system settings signal connections
    connect(systemSettingInstance,SIGNAL(set_simulation(bool)),this,SLOT(simulate_Plot(bool)));

    connect(systemSettingInstance,SIGNAL(compressionRate360(double)),this,SLOT(setCompressionRate360(double)));
    connect(systemSettingInstance,SIGNAL(compressionRate480(double)),this,SLOT(setCompressionRate480(double)));

    connect(systemSettingInstance,SIGNAL(attenuationVal(int)),this,SLOT(on_att_slider_valueChanged(int)));
    connect(systemSettingInstance,SIGNAL(gainMultVal(int)),this,SLOT(on_gain_mult_slider_valueChanged(int)));
    connect(systemSettingInstance,SIGNAL(addGain()),this,SLOT(on_add_gain_clicked()));
    connect(systemSettingInstance,SIGNAL(subGain()),this,SLOT(on_sub_gain_clicked()));
    connect(systemSettingInstance,SIGNAL(transConfig(int)), this, SLOT(setTransmitter(int)));
    connect(systemSettingInstance,SIGNAL(setEdgeAlign(bool)), this, SLOT(setEdgeAlign(bool)));
    connect(systemSettingInstance,SIGNAL(sendGainVal(int)),this,SLOT(setGainSliderVal(int)));
    connect(systemSettingInstance,SIGNAL(sendNoiseThresh(double)),this,SLOT(changeNoiseThresh(double)));
    connect(systemSettingInstance,SIGNAL(changeCompression(int)),this,SLOT(setCompressionFactor(int)));
    connect(systemSettingInstance,SIGNAL(sendFilterSize(int)),this,SLOT(setFilterSize(int)));
    connect(systemSettingInstance,SIGNAL(sendFilterEnabled(bool)),this,SLOT(setFilterEnabled(bool)));

    connect(this,SIGNAL(sendSys(float, float)), userWindowInstance, SLOT(receiveSys(float, float)));
    connect(this,SIGNAL(defaultAngle(double)),systemSettingInstance,SLOT(defaultAngle(double)));
//    connect(this,SIGNAL(noAngleControl()),systemSettingInstance,SLOT(noAngleControl()));
    connect(this,SIGNAL(sendMessage(QString)),systemSettingInstance,SLOT(receiveMessage(QString)));
    connect(this, SIGNAL(requestConfigDatabaseClose()), systemSettingInstance, SLOT(closeConfigDatabase()));

    //dev window signal connections
    connect(devWindowInstance,SIGNAL(openSys()),this,SLOT(openSystemSettings()));

    //scope window signal connections
    connect(graphWindowInstance,SIGNAL(graph_windowClosed(bool)),this,SLOT(setScopeWindowOpen(bool)));

    //Thread connections
    qRegisterMetaType< QVector<float> >("QVector<float>"); //This is used because the connect below cannot normally queue QVectors
    connect(this, SIGNAL(sendSysFromThread(float, float)), this, SLOT(receiveSysFromThread(float, float)), Qt::QueuedConnection);


    connect(userWindowInstance,SIGNAL(showGap(bool)),this, SLOT(format_showGap(bool)));

    //format window connections
    connect(Format, SIGNAL(showStatus(bool)),this, SLOT(format_showStatusBar(bool)));

    connect(Format,SIGNAL(showGap(bool)),this, SLOT(showGap_onplot(bool)));
    connect(Format,SIGNAL(showBalance(bool)),this, SLOT( format_showBalance(bool)));
//    connect(Format,SIGNAL(showGps(bool)),this, SLOT( format_showGpsPlot(bool)));
    connect(Format,SIGNAL(showColorScale(bool)),this, SLOT( format_showColorScale(bool)));
    connect(Format,SIGNAL(showSignal(bool)),this, SLOT( format_showSignalPlot(bool)));
    connect(Format,SIGNAL(showGain(bool)),this, SLOT( format_showGainSlider(bool)));
    connect(Format,SIGNAL(showGrid(bool)),this, SLOT( format_showGridOnPlot(bool)));
    connect(Format,SIGNAL(showZoomBox(bool)),this, SLOT(format_showZoomBox(bool)));
    connect(Format,SIGNAL(showCompass(bool)),this, SLOT(format_showCompass(bool)));
    connect(Format,SIGNAL(showCrosshair(bool)),this, SLOT(format_showCrosshair(bool)));

    connect(this, SIGNAL(formatStatusBar(bool)),Format,SLOT(selectStatusBar(bool)));
    connect(this, SIGNAL(showGnssClickable(bool)),Format, SLOT(showGnssClickable(bool)));
    connect(this, SIGNAL(showHfClickable(bool)),Format, SLOT(showHfClickable(bool)));
    connect(this, SIGNAL(showAltimeterClickable(bool)),Format, SLOT(showAltimeterClickable(bool)));
    connect(this, SIGNAL(showFormatGainSlider(bool)), Format, SLOT(showFormatGainSlider(bool)));
//    connect(this, SIGNAL(selectAltimeter(bool)), Format, SLOT(selectAltimeter(bool)));
//    connect(this, SIGNAL(selectHF(bool)), Format, SLOT(selectHF(bool)));
//    connect(this, SIGNAL(selectLF(bool)), Format, SLOT(selectLF(bool)));
//    connect(this, SIGNAL(selectGPS(bool)), Format, SLOT(selectGPS(bool)));
//    connect(this, SIGNAL(selectSensor(bool)), Format, SLOT(selectSensor(bool)));
    connect(this, SIGNAL(updateSplitterSizeInfo(double, double, double, double, double)),
            Format, SLOT(updateSplitterSizeInfo(double, double, double, double, double)));

    connect(chartDialogInstance, SIGNAL(chartValues(double,double,double,double)),this, SLOT(Chart_Values(double,double,double,double)));
    connect(this,SIGNAL(tiffFileName(QString)),chartDialogInstance,SLOT(tiffFileName(QString)));

    //connect(converterDialogInstance, SIGNAL(convertFile()), this, SLOT(convertFile()));
    connect(converterDialogInstance, SIGNAL(converterStartBtnClicked(bool)), this, SLOT(xtfConverter_StartBtnClicked(bool)));
    connect(converterDialogInstance, SIGNAL(converterCancelBtnClicked(bool)), this, SLOT(xtfConverter_CancelBtnClicked(bool)));
    // connect(this, SIGNAL(selectDataFile()),converterDialogInstance,SLOT(selectDataFile()));
    connect(converterDialogInstance, SIGNAL(changeDataFilePath(QList<QString>,QList<QString>)), this, SLOT(xtfConverter_changeDataFilePath(QList<QString>,QList<QString>)));
    connect(converterDialogInstance, SIGNAL(convertFileCounter(int)), this, SLOT(xtfConverter_FileCounter(int)));

    connect(this, SIGNAL(converterFileName(QString)), converterDialogInstance, SLOT(converterFileName(QString)));
    connect(this, SIGNAL(converterDialogProgressBar(double)), converterDialogInstance, SLOT(converterDialogProgressBar(double)));
    connect(this, SIGNAL(progressBarRange(int)), converterDialogInstance, SLOT(progressBarRange(int)));
    connect(this, SIGNAL(fileNameToXtfConverterWindow(QString)), converterDialogInstance, SLOT(fileNameToXtfConverterWindow(QString)));
    connect(this, SIGNAL(renameFileName(QString)), converterDialogInstance, SLOT(renameFileName(QString)));
    connect(this, SIGNAL(isConversionOver(bool)), converterDialogInstance, SLOT(isConversionOver(bool)));
    connect(this, SIGNAL(updateConvertedFileSize(int)), converterDialogInstance, SLOT(updateConvertedFileSize(int)));
    connect(this, SIGNAL(angleFileDetected(bool)), converterDialogInstance, SLOT(angleFileDetected(bool)));

    connect(userWindowInstance, SIGNAL(displayWayPoint(int)),this, SLOT(target_DisplayWayPoint(int)));

    connect(this, SIGNAL(createHelpWindow()),helpDialogInstance, SLOT(createHelpWindow()));

    connect(userWindowInstance, SIGNAL(bottomTrackingClicked(bool)),Format, SLOT(bottomTrackingClicked(bool)));
    connect(this, SIGNAL(untiePlots(bool)),userWindowInstance, SLOT(untiePlots(bool)));
    connect(this, SIGNAL(uncheckShowForwardRng(bool)),userWindowInstance, SLOT(uncheckShowForwardRng(bool)));
    connect(userWindowInstance, SIGNAL(numberOfPingsToPlot(int)),this, SLOT(plot_data_numberOfPingsToPlot(int)));
    connect(this, SIGNAL(enableTvgTab(bool)),userWindowInstance, SLOT(enableTvgTab(bool)));

    connect(this, SIGNAL(updateChartProgress(int)), ChartWindowInstance, SLOT(updateLoadingChartText(int)));

    connect(depthAlarm, SIGNAL(updateHandlePosition(int)), this, SLOT(depthAlarm_updateHandlePosition(int)));
    connect(depthAlarm, SIGNAL(updateButtonIcon()),this,SLOT(depthAlarm_updateButtonState()));
    connect(userWindowInstance, SIGNAL(updateAlarmSettings()), depthAlarm, SLOT(updateAlarmSettings()));
    connect(this, SIGNAL(updateAlarmSettings()), depthAlarm, SLOT(updateAlarmSettings()));
    connect(cropTool, SIGNAL(SaveCropping()), this, SLOT(crop_saveFile()));
}

void MainWindow :: setupPlayerDemo(bool mode) {
    if (mode == 0)      return;

    ui->connect_btn->setDisabled(true);
    ui->auto_btn->setDisabled(true);
    ui->btn_range1->setDisabled(true);
    ui->btn_range2->setDisabled(true);
    ui->btn_range3->setDisabled(true);
    ui->btn_range4->setDisabled(true);
    ui->gnssBtn->setDisabled(true);
    //    on_openFileButton_clicked(); // Open Glen Haven chart
    //    splitterHandleChanged();
}

/**
 * @brief MainWindow::fetchUser
 * To read the saved user settings from the user database,
 * and assign the value to the variables, for plotting.
 */
void MainWindow :: fetchUser() {
    QSqlQueryModel *model = new QSqlQueryModel();
    userWindowInstance->connOpenUser();
    // userWindowInstance->connOpenDefault();
    if(QSqlDatabase::contains("userConnection")){
        myUserDb = QSqlDatabase::database("userConnection");
        QSqlQuery *userqry = new QSqlQuery(myUserDb);
        userqry->prepare("SELECT * FROM userDB ");
        userqry->exec();
        model->setQuery(*userqry);
        if(model->query().exec()){
            QString pallettefromDB = model->record(1).value("Pallette").toString();
            double cl = model->record(0).value("Cable_Length").toDouble();
            updateCableLength(cl);
            recorder.recPath = model->record(0).value("Rec_Path").toString();
            //qDebug() << "Da: Open: " << recPath;
            emit updateRecPath(recorder.recPath);

            //            converterRecPath = model->record(0).value("converterRecPath").toString();
            //            emit updateConverterDirPath(converterRecPath);



            delete userqry;
        }
        else{
            QMessageBox::critical(this,tr("ERROR"),userqry->lastError().text());
        }
    }
    else{
        qDebug()<<"not available";
    }
    delete model;
}

/**
 * @brief MainWindow::readConfig
 * Called in the constructor, it reads the parameters for all the ranges
 * from the configuration.db database and assigns the values to the variables
 * during startup.
 */
void MainWindow :: readConfig() {
    userWindowInstance->connOpen(); //open connection
    configDB1 = QSqlDatabase::database("config1");//set configDB1 to config1 in database list
    configDB2 = QSqlDatabase::database("config2");//set configDB2 to config2 in database list
    QSqlQuery qry1(configDB1);//assign qry1 to configDB1
    QSqlQuery qry2(configDB2);//assign qry2 to configDB2
    //read all the values from the configuration databases.
    qry1.prepare("SELECT * FROM ConfigDB ");//select all records from ConfigDB table
    qry1.exec();//execute query
    qry2.prepare("SELECT * FROM ConfigDB ");
    qry2.exec();
    QSqlQueryModel config1, config2;//QSqlQueryModels are used to organize data returned by sql querys and make it accessible
    config1.setQuery(qry1);
    config2.setQuery(qry2);
    QList<double> f_repList_config1;
    QList<double> n_plotList_config1,swathList_config1;
    QList<double> n_plotList_config2,swathList_config2;
    //assign all the values from the databse to variables.
    if(config1.query().exec() && config2.query().exec()){//if both queries were successful
        for (int i = 0; i < 14; ++i) {
            //r_fwdList_config1.append(config1.record(i).value("R_fwd_max_m").toDouble());
            //depthList_config1.append(config1.record(i).value("Depth_m").toDouble());
            //compressionList_config1.append(config1.record(i).value("Comp_Factor").toDouble());
            //propagationList_config1.append(config1.record(i).value("Propagation_Delay_50_ms").toDouble());
            f_repList_config1.append(config1.record(i).value("F_rep_Hz").toDouble());
            n_plotList_config1.append(config1.record(i).value("N_plot").toDouble());
            swathList_config1.append(config1.record(i).value("Swath_m").toDouble());

            n_plotList_config2.append(config2.record(i).value("N_plot").toDouble());
            swathList_config2.append(config2.record(i).value("Swath_m").toDouble());
        }
        QString version = config1.record(0).value("Version").toString();
    }
    else{//if one or both queries were unsuccessful return an error message
        QMessageBox::critical(this,tr("ERROR"),qry1.lastError().text());
    }
}

/**
 * @brief MainWindow::closeEvent
 * @param e
 * To save all the user settings when the application is closed.
 */
void MainWindow :: closeEvent(QCloseEvent *e){
    if (state.PLAY_MODE == 1 && state.RT_MODE > 0) { // Check if the user actually wants to close and stop sonar
        Tcp_sendCommand(0,0,0); // pause sonar
        resetPlotLists();
        replotTimer.stop();

        //changing state for quick record button
        timer->stop();
        recorder_startStop(false);

        if (state.RT_MODE == 2) {
            state.RT_MODE = 1;
            QMessageBox::StandardButton checkMsg = QMessageBox::question(this, "Disconnect Sonar?",
                                                                         "Are you sure you want to disconnect sonar?");
            setup_buttonsAndTools();
            if (checkMsg != QMessageBox::Yes)    return;
        }
        tcpSocket->disconnectFromHost();
        state.RT_MODE = 0;
    }

    if(e->isAccepted()){
        userWindowInstance->saveUserData();
        userWindowInstance->saveZoomClicks();
        Format->saveFormatSettings();
        e->setAccepted(true);
        QDateTime time = QDateTime::currentDateTime();

        requestConfigDatabaseClose();

        //These allow the instances the be closed whent the main GUI is closed
        //  They are only needed when they're are not children of the mainWindow
        //  See the mainwindow.h file
        //        delete hfWindowInstance;
        //        delete depthWindowInstance;
        //        delete navWindowInstance;
        //        delete tcperrWindowInstance;
        //        delete conerrInstance;
        //        delete graphWindowInstance;
        //        delete dataRecorderInstance;
        //        delete flagInstance;
        //        delete userWindowInstance;
        //        delete devWindowInstance;
        //        delete systemSettingInstance;
    }
}

void MainWindow :: ui_updateButtons() {
    ui->tvg_btn->setChecked(false);
}

/************ To/From Second Thread*************************/

void MainWindow :: receiveSysFromThread(float Altitude, float Depth) {
    sendSys(Altitude, Depth);
}

/*****************Color Map functions*****************/

/**
 * @brief MainWindow::resetDataList1
 * Clears all the plotting buffers.
 */
void MainWindow :: resetPlotLists() {
    ping->data_portLf.clear();
    ping->data_stbdLf.clear();
    ping->data_depth.clear();
    ping->data_portHf.clear();
    ping->data_stbdHf.clear();
}

/**
 * @brief MainWindow::resizeEvent
 * @param e
 * Sets the viewing range of the x axis of main plotting area,
 * to keep a constant angle of the plotted image.
 * Is called when user resizes window
 */
void MainWindow :: resizeEvent(QResizeEvent *e ) {
    Chart_UnstretchPlot();

    QFont font = avg_label->font();
    font.setPointSize(11);
    QFontMetrics fm(font);
    qDebug() << "File width: " << file_Name->width() << file_Name->x()-progressBar->x() << fm.horizontalAdvance(displayName) << displayName;

    if (fm.horizontalAdvance(displayName) + 30 > progressBar->x() - file_Name->x())
        file_Name->setText("..."+displayName.mid(displayName.size()-16,displayName.size()));
    else
        file_Name->setText(displayName);
    file_Name->setMinimumWidth(fm.horizontalAdvance("__"+file_Name->text()));

    if (e != nullptr) {
        plot_range_unstretchLfHfPlots();
    }

    Format->windowMaximized = this->isMaximized();
    Format->widthWindow = this->width();
    Format->heightWindow = this->height();
}

/*****************Reading Data functions*****************/

void MainWindow :: data_simulate () {
    ping->pingNumCurrent++;
    currentTime = QTime::currentTime();
    currentRtcTime.hour = currentTime.hour();
    currentRtcTime.minute = currentTime.minute();
    currentRtcTime.second = currentTime.second();
    ping->Depth = 0;
    ping->lastTracedyaw = 0;
    ping->angleOrientation = 90;

    QDateTime time = QDateTime::currentDateTime();
    QString rtc = time.toString("hh:mm:ss");

//    GpsPoint gpsPoint;
//    gpsPoint.Lat = GpsItems.Lattitude_Deg;
//    gpsPoint.Lon = GpsItems.Longitude_Deg;
//    gpsPoint.Dir = GpsItems.Heading;
    ping->lastTraceAlt = 0;

    ping->rangeLf_int = ping->rangeLf_tmp;
    ping->rangeHf_int = ping->rangeHf_tmp;
    flag.PlotHfSidescan = 1;
    flag.PlotAngleScan = false;
    for (quint16 i = 0; i < ping->samplesToPlotLF; i++) {
        ping->data_portLf.append(i+1);
        ping->data_stbdLf.append(i+1);
    }
    for (quint16 i = 0; i < ping->samplesToPlotHF; i++) {
        ping->data_portHf.append(i+1);
        ping->data_stbdHf.append(i+1);
    }

    if (recorder_checkState()) {
        pingCounter++;//used to keep track of when to write buffer to file
        if(!flag.recordXtfFormat) {
            //                buffer.prepend(headerData);
            QByteArray tmp;
            tmp = data_simulateCreateBuffer();
            recorder_saveData(tmp); //record data in native format
            Gnss_saveGpsPoints();
        }
        else {
            xtf_writePing();
        }
    }
    plot_data_addTrace();
    plot_data_checkToAddNewMap();
    plot_replotAll();
    data_hashMap_update();
}

QByteArray MainWindow :: data_simulateCreateBuffer() {
    QByteArray data;
    JAS_PINGHEADER JasTemplate;
    JasTemplate.LfSamples = ping->samplesToPlotLF;
    JasTemplate.HfSamples = ping->samplesToPlotHF;
    JasTemplate.LfRange = ping->rangeLf_int;
    JasTemplate.HfRange = ping->rangeHf_int;
    JasTemplate.PingNumber = ping->pingNumCurrent;
    JasTemplate.PacketSize = JasTemplate.HeaderSize + JasTemplate.LfSamples * 4 + JasTemplate.HfSamples * 4 + 12;

    currentDate = QDate::currentDate();
    currentTime = QTime::currentTime();
    JasTemplate.Time1 = (currentTime.msec() << 16) || (currentTime.second() << 8) || (currentTime.minute());
    JasTemplate.Time2 = (currentTime.hour() << 24) || (currentDate.day() << 16) || (currentDate.month() << 8) || (currentDate.year()%2000);

    data.append((const char *)&JasTemplate.SyncWord, 4);
    data.append((const char *)&JasTemplate.MessageType, 4);
    data.append((const char *)&JasTemplate.PacketSize, 4);

    data.append((const char *)&JasTemplate.HeaderSize, 4);
    data.append((const char *)&JasTemplate.Time1, 4);
    data.append((const char *)&JasTemplate.Time2, 4);
    data.append((const char *)&JasTemplate.Firmware, 4);
    data.append((const char *)&JasTemplate.LfValue, 2);
    data.append((const char *)&JasTemplate.HfValue, 2);
    data.append((const char *)&JasTemplate.nullByte, 1);
    data.append((const char *)&JasTemplate.nullByte, 1);
    data.append((const char *)&JasTemplate.nullInt, 4);

    data.append((const char *)&JasTemplate.AngleOrientation, 1);
    data.append((const char *)&JasTemplate.BytesPerSample, 1);
    data.append((const char *)&JasTemplate.LfSamples, 2);
    data.append((const char *)&JasTemplate.HfSamples, 2);
    data.append((const char *)&JasTemplate.nullInt, 4);

    data.append((const char *)&JasTemplate.LfRange, 2);
    data.append((const char *)&JasTemplate.HfRange, 2);
    data.append((const char *)&JasTemplate.nullInt, 4);

    data.append((const char *)&JasTemplate.PingNumber, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);
    data.append((const char *)&JasTemplate.nullInt, 4);

    data.append((const char *)&JasTemplate.Roll, 4);
    data.append((const char *)&JasTemplate.Pitch, 4);
    data.append((const char *)&JasTemplate.Yaw, 4);
    data.append((const char *)&JasTemplate.Heading, 4);
    data.append((const char *)&JasTemplate.Altitude, 4);
    data.append((const char *)&JasTemplate.Depth, 4);
    data.append((const char *)&JasTemplate.Temperature, 4);

    for (int i = 0; i < ping->samplesToPlotLF; i++) {
        data.append((const char *)&ping->data_portLf.at(i), 2);
        data.append((const char *)&ping->data_stbdLf.at(i), 2);
    }
    for (int i = 0; i < ping->samplesToPlotHF; i++) {
        data.append((const char *)&ping->data_portHf.at(i), 2);
        data.append((const char *)&ping->data_stbdHf.at(i), 2);
    }
    data.append((const char *)&JasTemplate.EndSync, 4);
    return data;
}

int  MainWindow :: data_copy(void * __restrict__ variable, char *data_pointer, int NumChar, int *NumChar2) {
    *NumChar2 += NumChar;
    memcpy(variable, data_pointer, NumChar);
    return NumChar;
}

void MainWindow :: data_hashMap_update() {
    // Update Hashmaps
    if (state.GNSS_MODE == 2 && (state.RT_MODE && state.GUI_MODE < 2)) {
        Gnss_Update();
    }

    ping->roll = ping->lastTraceRoll;
    ping->pitch = ping->lastTracePitch;
    float heading = ping->heading;
    if (ping->roll < -3000) ping->roll = -3000; if (ping->roll > 3000) ping->roll = 3000;
    if (ping->pitch < -3000) ping->pitch = -3000; if (ping->pitch > 3000) ping->pitch = 3000;
    if (ping->bottomTrackDetected)  ping->altitudeFactor = sin(qDegreesToRadians(ping->degree/* - ping->pitch*/));

    static int temp = 0;
    double sum_hdg=0, sum_depth=0;
    double avg_hdg=0, avg_depth=0;
    double yaw =0;
    static QQueue<double> hdg_queue, depth_queue;

    if (heading < 0){
        hdg_queue.enqueue(heading + 360);
    }
    else if(heading !=0){
        hdg_queue.enqueue(heading);
    }
    QListIterator<double>i(hdg_queue);
    while(i.hasNext()){
        sum_hdg += i.next();
    }
    if (hdg_queue.size() > 0)    avg_hdg = sum_hdg/hdg_queue.size();

    if (hdg_queue.size() == setting.yawRunningLength){ hdg_queue.dequeue();}

//    if (ping->Depth*setting.convert_unit > 0)       depth_queue.enqueue(ping->Depth*setting.convert_unit);
//    QListIterator<double> j(depth_queue);
//    if (depth_queue.size() == setting.averageDepthQueueLength)     depth_queue.dequeue();
//    while(j.hasNext())      sum_depth += j.next();
//    avg_depth = sum_depth/depth_queue.size();

    yaw = (avg_hdg-heading);
    if (abs(avg_hdg-heading) > 180) {
        yaw = (avg_hdg-heading) - 360;
    }
    ping->lastTracedyaw = yaw;

    ui->sensor_graph->graph(0)->addData(ping->lastTracePos, ping->lastTraceRoll);
    ui->sensor_graph->graph(1)->addData(ping->lastTracePos, ping->lastTracePitch);
    ui->sensor_graph->graph(2)->addData(ping->lastTracePos, yaw);

    double Altimeter = ping->lastTraceAlt;

    // Add the RTC time to the hashmap
    int hour_display;
    int minute_display;
    int second_display;
    if (ping->firstRtcTime == 0) {
        ping->firstRtcTime = currentRtcTime.hour*3600 + currentRtcTime.minute*60 + currentRtcTime.second;
    }
    int totalCurrentTime = currentRtcTime.hour*3600 + currentRtcTime.minute*60 + currentRtcTime.second;

    hour_display   = abs(totalCurrentTime - ping->firstRtcTime) / 3600;
    minute_display = (abs(totalCurrentTime - ping->firstRtcTime) % 3600) / 60;
    second_display = abs(totalCurrentTime - ping->firstRtcTime) % 60;
    QString hour_format   = QString :: number(hour_display);
    QString minute_format = QString :: number(minute_display);
    QString second_format = QString :: number(second_display);

    if (hour_display   < 10) hour_format   = hour_format.rightJustified(2,'0');
    if (minute_display < 10) minute_format = minute_format.rightJustified(2,'0');
    if (second_display < 10) second_format = second_format.rightJustified(2,'0');
    QString rtc = hour_format + ":" + minute_format + ":" + second_format;

    PingMapItem tmpData(ping->lastTracePos, ping->pingNumCurrent, rtc, ping->temperature, ping->Depth*setting.convert_unit, ping->lastTraceAlt,
                     ping->lastTraceRoll, ping->lastTracePitch, ping->lastTracedyaw, heading, gnssController->Latitude(),
                     gnssController->Longitude(), gnssController->Heading(), ChartWindowInstance->gpsSpeed, ChartWindowInstance->totalDistance,
                     ping->altitudeFactor);
    dataMap.append(tmpData);

    status_updateStatusBar(ping->pingNumCurrent - ping->pingNumFirst, rtc, ping->Depth*setting.convert_unit,
                           ping->temperature, ping->roll, ping->pitch, yaw, Altimeter, heading);
}

void MainWindow :: Jas_dataProcess(char *data_pointer, quint32 packetID, int *BytesPointer) {
    //This function was originally the else body in processData
    //This else body is still called exactly where it should,
    //it is just threaded instead. In other words, the order
    //of processData's code is not changed

    QList<quint16> listA1, listB1, listC1, listA2, listB2, listC2, listD1, listD2, listE1, listE2;
    QByteArray b;

    // Read JAS ping header
    data_pointer += Jas_readPingHeader(data_pointer, packetID, BytesPointer);

    flag.thread_middlePoint = true; //This tells the outside of the thread that it has reached this point
    while (!flag.thread_rangeDone) //Waits for the outside code to be finished
    {

    }
    flag.thread_rangeDone = false;
    //Continues thread

    // Read LF data samples
    for(int i = 0; i < ping->samplesToPlotLF; i++) {
        quint16 x, y;
        data_pointer += data_copy(&x, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&y, data_pointer, 2, BytesPointer);
        listA1.append(x);
        listB1.append(y);
    }
    // Read HF data samples
    for(int j = 0; j < ping->samplesToPlotHF; j++) {
        quint16 x, y;
        data_pointer += data_copy(&x, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&y, data_pointer, 2, BytesPointer);
        listA2.append(x);
        listB2.append(y);
    }
    if (packetID && ping->depthFrequency) {//if depth sounder detected
        for (int i = 0; i < ping->samplesToPlotDepth; i++) {//retrieve depth sounder data
            quint16 x;
            data_pointer += data_copy(&x, data_pointer, 2, BytesPointer);
            listC1.append(x);
        }
        ping->data_depth = listC1;
    }

    //This filter is used to filter out noise from the transmitters
    //I didn't design this, if you have any questions about how it works talk to Caleb
    int size = realSize, factor = size/2, bounds = factor+5;
    quint16 filter1, filter2;
    /*if (filterOn) {
                          for(int i = 0; i< bounds; i++) {
                              listD1.append(listA1.at(i));
                              listE1.append(listB1.at(i));
                          }
                          for(int i = bounds; i< amtLFsamp-bounds; i++) {
                              int tmp = 0, tmp2 = 0;
                              for(int j = -factor; j < factor; j++) {
                                      tmp += listA1.at(i+j);
                                      tmp2 += listB1.at(i+j);
                              }
                              tmp /= (2*factor);
                              tmp2 /= (2*factor);
                                  if(listA1.at(i)-tmp < 0) {
                                      listD1.append(0);
                                  } else {
                                      listD1.append(listA1.at(i)-tmp);
                                  }
                                  if(listB1.at(i)-tmp2 < 0) {
                                      listE1.append(0);
                                  } else {
                                      listE1.append(listB1.at(i)-tmp2);
                                  }
                                  filter1 = tmp;
                                  filter2 = tmp2;
                          }
                          for(int i = amtLFsamp-bounds; i< amtLFsamp; i++) {
                              if(listA1.at(i)-filter1 < 0) {
                                  listD1.append(0);
                              } else {
                                  listD1.append(listA1.at(i)-filter1);
                              }
                              if(listB1.at(i)-filter2 < 0) {
                                  listE1.append(0);
                              } else {
                                  listE1.append(listB1.at(i)-filter2);
                              }
                          }
    }*/

    if (ping->rangeLf_int == 360 || ping->rangeLf_int == 480) {//if compression is required for current range
        if (flag.filterOn) {//if filter is on
            ping->data_portLf = compressList(listA1);//(listD1);
            ping->data_stbdLf = compressList(listB1);//(listE1);
            ping->data_portHf = compressList(listD2);
            ping->data_stbdHf = compressList(listE2);
        }
        else {//if filter is off
            ping->data_portLf = compressList(listA1);
            ping->data_stbdLf = compressList(listB1);
            ping->data_portHf = compressList(listA2);
            ping->data_stbdHf = compressList(listB2);
        }
    }
    else {//if compression is not needed for current range
        if (flag.filterOn) {//if filter is on
            ping->data_portLf = listA1;
            ping->data_stbdLf = listB1;
            ping->data_portHf = listD2;
            ping->data_stbdHf = listE2;
        }
        else {//if filter is off
            ping->data_portLf = listA1;//listA1;//compressList(listA1);
            ping->data_stbdLf = listB1;//compressList(listB1);
            ping->data_portHf = listA2;
            ping->data_stbdHf = listB2;
        }
    }

    if (flag.useDualFrequency || (packetID == 5)) {//if dualfrequency detected from sonar
        //This filter is used to filter out noise from the transmitters
        //I didn't design this, if you have any questions about how it works talk to Caleb
        if (flag.filterOn) {
            static int tmpPort = 0, tmpStbd = 0;
            int tmpAvg = 0, tmpAvg2 = 0;
            if (traceCounter1-filterStartTrace < 2) {
                tmpPort = 0; tmpStbd = 0;
            }
            if (traceCounter1-filterStartTrace < 20) {
                listC1 = listA2;
                qSort(listC1.begin(),listC1.end());
                for(int i = 50; i< bounds+50; i++) {
                    tmpAvg += listC1.at(i);
                }
                tmpAvg /= bounds;
                tmpPort = (tmpAvg + tmpPort)/2;
                listC1 = listB2;
                qSort(listC1.begin(),listC1.end());
                for(int i = 50; i< bounds+50; i++) {
                    tmpAvg2 += listC1.at(i);
                }
                tmpAvg2 /= bounds;
                tmpStbd = (tmpAvg2 + tmpStbd)/2;
            }
            if (traceCounter1-filterStartTrace == 20) filterStartTrace = -50;
            for(int i = 0; i< ping->samplesToPlotHF; i++) {
                if(listA2.at(i)-tmpPort < 0) {
                    listD2.append(0);
                } else {
                    listD2.append(listA2.at(i)-tmpPort);
                }
                if(listB2.at(i)-tmpStbd < 0) {
                    listE2.append(0);
                } else {
                    listE2.append(listB2.at(i)-tmpStbd);
                }
            }
        }
        /*if (filterOn) {
            for(int i = 0; i< bounds; i++) {
                listD2.append(listA2.at(i));
                listE2.append(listB2.at(i));
            }
            for(int i = bounds; i< amtHFsamp-bounds; i++) {
                int tmp = 0, tmp2 = 0;
                for(int j = -factor; j < factor; j++) {
                        tmp += listA2.at(i+j);
                        tmp2 += listB2.at(i+j);
                }
                tmp /= (2*factor);
                tmp2 /= (2*factor);
                    if(listA2.at(i)-tmp < 0) {
                        listD2.append(0);
                    } else {
                        listD2.append(listA2.at(i)-tmp);
                    }
                    if(listB2.at(i)-tmp2 < 0) {
                        listE2.append(0);
                    } else {
                        listE2.append(listB2.at(i)-tmp2);
                    }
                    filter1 = tmp;
                    filter2 = tmp2;
            }
            for(int i = amtHFsamp-bounds; i< amtHFsamp; i++) {
                if(listA2.at(i)-filter1 < 0) {
                    listD2.append(0);
                } else {
                    listD2.append(listA2.at(i)-filter1);
                }
                if(listB2.at(i)-filter2 < 0) {
                    listE2.append(0);
                } else {
                    listE2.append(listB2.at(i)-filter2);
                }
            }
        }
        */

        if (flag.filterOn && flag.convertToXtf) {
            portSide2_bytes.clear();
            starboardSide2_bytes.clear();
            for(int i = 0; i< ping->samplesToPlotHF; i++) {
                b.append(listD2.at(i));
                b.resize(2);
                portSide2_bytes.prepend(b);
                b.clear();

                b.append(listE2.at(i));
                b.resize(2);
                starboardSide2_bytes.append(b);
                b.clear();
            }
        }
    }

    quint32 trailer;
    data_pointer += data_copy(&trailer, data_pointer, 4, BytesPointer);
    if (trailer != 0xFACE)
        qDebug() << "JAS data misaligned";
}

int  MainWindow :: Jas_readPingHeader(char *data_pointer, quint32 packetID, int *BytesPointer) {
    //This function was originally the else body in processData
    //This else body is still called exactly where it should,
    //it is just threaded instead. In other words, the order
    //of processData's code is not changed

    int ByteCounterStart = *BytesPointer;
    static double lastMilliSec = 0;
    //    qDebug() << "ProcessDataToPlot()" << myTimer.restart();
    //    qint32 BytesPointer = 0;
    quint16 CoordUnits;
    quint16 recievedRange_depth;
    quint32 kalmanNum = 0, kalmanNum2 = 0;
    float AccX, AccY, AccZ, RollDeg, PitchDeg, YawDeg;
    float HeadingDeg, Altitude, Depth, Temperature;
    float LFSampInterval, HFSampInterval, depthSampInterval;
    float XCoord, YCoord;

    quint32 pingSize;
    quint8 seconds, minutes, hours, days, months, years;
    quint16 milliseconds;
    ping->recieveDepth = false;

    if (packetID == 100 | packetID == 101) {
        data_pointer += data_copy(&ping->pingHeaderSize, data_pointer, 4, BytesPointer);

        data_pointer += data_copy(&milliseconds, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&seconds, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&minutes, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&hours, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&days, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&months, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&years, data_pointer, 1, BytesPointer);

        qDebug() << "Time: " << /*QString::number(hours).rightJustified(2,'0') << ":" <<*/ QString::number(minutes).rightJustified(2,'0')
                 << ":" << QString::number(seconds).rightJustified(2,'0') << "." << QString::number(milliseconds).rightJustified(3,'0');

        int diff = 0;
        if(lastMilliSec == 0){
            diff = milliseconds;
        }
        double currentms = ((int)milliseconds)+1000*((int)seconds)+((int)minutes)*60000;
        diff = currentms - lastMilliSec;
        if (diff < 0)   qDebug() << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Error!!!!!!!!!!!!!!!!!!!!!!!!!";
        //qDebug() << "Time Difference from RX Board" << diff << " = " << QString::number(currentms, 'd', 0) << " - " << QString::number(lastMilliSec, 'd', 0);
        lastMilliSec = currentms;
        int changeTime = (seconds + (minutes*60) + (hours*60*60) + (days*24*60*60)) - diffTime;
        changeTime = (changeTime*1000)+milliseconds - diffMili;
        diffTime = (seconds + (minutes*60) + (hours*60*60) + (days*24*60*60));
        diffMili = milliseconds;

        data_pointer += data_copy(&ping->firmwareVer, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->lowFrequency, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->highFrequency, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->depthFrequency, data_pointer, 2, BytesPointer);
        if (ping->depthFrequency > 0)     ping->recieveDepth = 1;

        data_pointer += 4;
        *BytesPointer += 4;
        data_pointer += data_copy(&ping->angleOrientation, data_pointer, 1, BytesPointer);
        if (ping->firmwareVer == 2021005204) {
            ping->Oct31ASTest = 1;
            ping->angleOrientation = 70;
        } else if (ping->firmwareVer < 2000000000) {
            ping->Oct31ASTest = 0;
            ping->angleOrientation = 90;
        } else {
            ping->Oct31ASTest = 0;
        }
        //angleOrientation = 90;
        ping->degree = ping->angleOrientation;

        quint8 BytesPerSample;
        data_pointer += data_copy(&BytesPerSample, data_pointer, 1, BytesPointer);

        data_pointer += data_copy(&ping->samplesToPlotLF, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->samplesToPlotHF, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->samplesToPlotDepth, data_pointer, 2, BytesPointer);
        data_pointer += 2;
        *BytesPointer += 2;

        data_pointer += data_copy(&ping->rangeLf_tmp, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->rangeHf_tmp, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&recievedRange_depth, data_pointer, 2, BytesPointer);
        data_pointer += 2;
        *BytesPointer += 2;

        ping->rangeLf_double = ping->rangeLf_tmp*setting.convert_unit;
        ping->rangeHf_double = ping->rangeHf_tmp*setting.convert_unit;

        data_pointer += data_copy(&ping->pingNumCurrent, data_pointer, 4, BytesPointer);
        if(ping->pingNumFirst == 0){
            ping->pingNumFirst = ping->pingNumCurrent;
            // Send the sonar frequencies to the user menu on connect
            gain->tvg_frequencyLow = ping->lowFrequency;
            gain->tvg_frequencyHigh = ping->highFrequency;
            emit freqToUsermenu();
        }

        data_pointer += 14;
        *BytesPointer += 14;

        data_pointer += data_copy(&CoordUnits, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&XCoord, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&YCoord, data_pointer, 4, BytesPointer);

        data_pointer += 28;
        *BytesPointer += 28;

        if (ping->firmwareVer == 2051009206) {
            data_pointer += data_copy(&YawDeg, data_pointer, 4, BytesPointer); // Reads 0
            data_pointer += data_copy(&ping->lastTraceRoll, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->lastTracePitch, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->Depth, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->heading, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->temperature, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&Altitude, data_pointer, 4, BytesPointer); // Dummy number
        } else {
            data_pointer += data_copy(&ping->lastTraceRoll, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->lastTracePitch, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&YawDeg, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->heading, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&Altitude, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->Depth, data_pointer, 4, BytesPointer);
            data_pointer += data_copy(&ping->temperature, data_pointer, 4, BytesPointer);
        }
    } else if (packetID == 40) {
        quint8 LfAvailable, HfAvailable, BothAvailable, NumLfRanges, NumHfRanges, Reserved;
        QList<quint16> LfRanges, HfRanges;

        // Configure range buttons
        data_pointer += data_copy(&LfAvailable, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&HfAvailable, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&Reserved, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&BothAvailable, data_pointer, 1, BytesPointer);
        setting.lfHfIndependent = !BothAvailable;

        data_pointer += data_copy(&NumLfRanges, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&NumHfRanges, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&Reserved, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&Reserved, data_pointer, 1, BytesPointer);

        for (int i = 0; i < NumLfRanges; i++) {
            quint16 tmp;
            data_pointer += data_copy(&tmp, data_pointer, 2, BytesPointer);
            LfRanges.append(tmp);
            if (i < 4)  lfRangeList[i] = tmp;
        }
        for (int i = 0; i < NumHfRanges; i++) {
            quint16 tmp;
            data_pointer += data_copy(&tmp, data_pointer, 2, BytesPointer);
            HfRanges.append(tmp);
            if (i < 4)  hfRangeList[i] = tmp;
        }

        return 0;
    } else {
        // Legacy format
        data_pointer += data_copy(&ping->pingHeaderSize, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&milliseconds, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&seconds, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&minutes, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&hours, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&days, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&months, data_pointer, 1, BytesPointer);
        data_pointer += data_copy(&years, data_pointer, 1, BytesPointer);

        int diff = 0;
        if(lastMilliSec == 0){
            diff = milliseconds;
        }

        double currentms = ((int)milliseconds)+1000*((int)seconds)+((int)minutes)*60000;
        diff = currentms - lastMilliSec;
        //qDebug() << "Time Difference from RX Board" << diff << " = " << QString::number(currentms, 'd', 0) << " - " << QString::number(lastMilliSec, 'd', 0);
        lastMilliSec = currentms;

        int changeTime = (seconds + (minutes*60) + (hours*60*60) + (days*24*60*60)) - diffTime;

        changeTime = (changeTime*1000)+milliseconds - diffMili;
        diffTime = (seconds + (minutes*60) + (hours*60*60) + (days*24*60*60));
        diffMili = milliseconds;

        data_pointer += data_copy(&ping->firmwareVer, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->lowFrequency, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->highFrequency, data_pointer, 2, BytesPointer);
        if (packetID > 0 && packetID < 20) {
            data_pointer += data_copy(&ping->depthFrequency, data_pointer, 2, BytesPointer);
            ping->recieveDepth = true;
            if (ping->depthFrequency && packetID == 6) {
                ping->depthFrequency = 0;
            }
        }

        data_pointer += data_copy(&ping->angleOrientation, data_pointer, 2, BytesPointer);
        if (ping->firmwareVer == 2021005204) {
            ping->Oct31ASTest = 1;
            ping->angleOrientation = 70;
        } else if (ping->firmwareVer < 2000000000) {
            ping->Oct31ASTest = 0;
            ping->angleOrientation = 90;
        } else {
            ping->Oct31ASTest = 0;
        }
        //angleOrientation = 90;
        ping->degree = ping->angleOrientation;

        if(!packetID){
            data_pointer += 2;
            *BytesPointer += 2;
        }

        data_pointer += data_copy(&ping->samplesToPlotLF, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->samplesToPlotHF, data_pointer, 2, BytesPointer);
        if (packetID) {
            data_pointer += data_copy(&ping->samplesToPlotDepth, data_pointer, 2, BytesPointer);
        }

        data_pointer += data_copy(&ping->rangeLf_tmp, data_pointer, 2, BytesPointer);
        data_pointer += data_copy(&ping->rangeHf_tmp, data_pointer, 2, BytesPointer);
        if (packetID) {
            data_pointer += data_copy(&recievedRange_depth, data_pointer, 2, BytesPointer);
        }

        ping->rangeLf_double = ping->rangeLf_tmp*setting.convert_unit;
        ping->rangeHf_double = ping->rangeHf_tmp*setting.convert_unit;

        if (packetID == 6) {
            flag.PlotAngleScan = true;
            flag.PlotAngleScanColorMap = false;

        }
        data_pointer += data_copy(&ping->pingNumCurrent, data_pointer, 4, BytesPointer);
        if(ping->pingNumFirst == 0){
            ping->pingNumFirst = ping->pingNumCurrent;
            // Send the sonar frequencies to the user menu on connect
            gain->tvg_frequencyLow = ping->lowFrequency;
            gain->tvg_frequencyHigh = ping->highFrequency;
            emit freqToUsermenu();
        }

        // reading the kalman number for bottom tracking
        data_pointer += data_copy(&kalmanNum, data_pointer, 2, BytesPointer);  // Port Side Bottom Tracking
        data_pointer += data_copy(&kalmanNum2, data_pointer, 2, BytesPointer); // Starboard Side Bottom Tracking

        if (packetID == 6) {
            //        quint16 extraSamples = (kalmanNum > kalmanNum2) ? kalmanNum : kalmanNum2;
            //        kalmanDepth = (speed_of_sound * kalmanNum) / (2* currentSamplingFrequency_config1);
            //        samplesToPlot_config1 = amtLFsamp - extraSamples;
            //        samplesToPlot_config2 = amtHFsamp - extraSamples;
        }
        //     kalmanDepth = (kalmanNum)*0.024;
        //     kalmanDepth2 = (kalmanNum2)*0.024;

        data_pointer += data_copy(&AccX, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&AccY, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&AccZ, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->lastTraceRoll, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->lastTracePitch, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->Depth, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->heading, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&ping->temperature, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&YawDeg, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&LFSampInterval, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&HFSampInterval, data_pointer, 4, BytesPointer);
        if (packetID) {
            data_pointer += data_copy(&depthSampInterval, data_pointer, 4, BytesPointer);
        }
    }

    currentRtcTime.year = years;
    currentRtcTime.month = months;
    currentRtcTime.day = days;
    currentRtcTime.hour = hours;
    currentRtcTime.minute = minutes;
    currentRtcTime.second = seconds;
    currentRtcTime.millisecond = milliseconds;

    return *BytesPointer - ByteCounterStart;
}

/**
 * @brief MainWindow::sortFileData
 * To read the file data and assign the data to the plotting
 * buffers for playing back the recorded data stored in the file.
 */
void MainWindow :: player_sortFileData() {
    state.GUI_MODE = 2;
    quint32 check;
    quint32 packetID, syncword, packetsize;
    QElapsedTimer checkTime;
    checkTime.start();

    int readFileBytes = 0;
    if (player_loadToBuffer() < 0)      return;
    if (!player.typeXtf) {//if jss file

        memcpy(&check, player.dataPtr, 4);//checks for footer value used to stop playback at end of file
        if ((check & 0x0000FFFF) == 0xFAAF) {//if footer detected on .jss file, end playback
            readFileTimer.stop();
            replotTimer.stop();
            customPlotLF->replot(QCustomPlot::rpQueuedReplot);
            customPlotHF->replot(QCustomPlot::rpQueuedReplot);
            ui->sensor_graph->replot(QCustomPlot::rpQueuedReplot);

            if (cropTool->toolActive && cropTool->ping_start > 0) {
                player_playData(false);
                return;
            }

            //emit converterDialogProgressBar(playBackDataBuffer.size());
            status_setProgressBarValue(player.fileBuffer.size());
            bottomTrack_clearQueue();

            tools->removeAllTargetsFromList();
            plot_replotAll();
            ChartWindowInstance->target_resizeAllTargets();
            ChartWindowInstance->plot_replot();
            ChartWindowInstance->target_resizeAllTargets();
            //                selectDataFile();
            player_reset();//selectDataFile();
            player_addColorMap();
            player_playData(true);
            setup_buttonsAndTools();
            return;
        }

        //sync word in packet header
        player.dataPtr += data_copy(&syncword, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
        player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);
        player.dataPtr += data_copy(&packetsize, player.dataPtr, 4, &readFileBytes);

        if (packetID > 0 && packetID < 20)      ping->recieveDepth = true;
        else                                    ping->recieveDepth = false;

        QThreadPool pool;
        QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess, player.dataPtr, packetID, &readFileBytes); //Runs processDataToPlot in a thread
        while (!processDataThreadProgress.isFinished()) //During the function's execution
        {
            if (flag.thread_middlePoint == true) //When it reaches this point
            {
                processDataThreadProgress.togglePaused(); //Pause the function
                flag.thread_middlePoint = false;
                break;
            }
        }

        //While the function is paused, do the code below
        if(ping->rangeLf_tmp != ping->rangeLf_int) {//if range recieved from the board is not the current GUI range, set GUI range to board range
            if (ping->rangeLf_tmp > 0) {
                setRecordedRange(ping->rangeLf_tmp);
                flag.highRangeChange = true;
            }
            ping->rangeLf_int = ping->rangeLf_tmp; // Only use this after setRecordedRange is called
        }
        if(ping->rangeHf_tmp != ping->rangeHf_int) {//same as above
            if (ping->rangeHf_tmp > 0 && !flag.highRangeChange) {
                setRecordedRange(ping->rangeHf_tmp);
            }
            ping->rangeHf_int = ping->rangeHf_tmp; // Only use this after setRecordedRange is called
            flag.highRangeChange = false;
        }
        flag.highRangeChange = false;

        flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue

        processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data
        player.dataPtr += readFileBytes - 12;
        //move on to the packet trailer
    }
    else { //Else if xtf is detected
        int fileFormat = 0;
        memcpy(&fileFormat, player.dataPtr, 1);
        if (fileFormat == 0x7B) {
            player.dataPtr = player.fileBuffer.data();
            readFileBytes = 0;
            // Read XTFFILEHEADER
            player.dataPtr += xtf_readFile(player.dataPtr, 1, &readFileBytes);

            qDebug() << "XTFFILEHEADER 1" << fileHeaderObject.RecordingProgramName << fileHeaderObject.RecordingProgramVersion <<
                        fileHeaderObject.SonarName << fileHeaderObject.SonarType << fileHeaderObject.ThisFileName <<
                        fileHeaderObject.NumberOfSonarChannels;

            // Read CHANINFO 6 times
            player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
            qDebug() << "CHANINFO 1" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
            player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
            qDebug() << "CHANINFO 2" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
            player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
            qDebug() << "CHANINFO 3" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
            player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
            qDebug() << "CHANINFO 4" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
            player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
            qDebug() << "CHANINFO 5" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
            player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
            qDebug() << "CHANINFO 6" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;

            player.bufferPos = readFileBytes;
            player.filePos = readFileBytes;
            return;
        }
        else
            readFileBytes += xtf_dataProcess();
    }

    player.filePos += readFileBytes;
    player.bufferPos += readFileBytes;

    // Take data from JAS/XTF and plot data
    plot_data_addTrace();
    status_setProgressBarValue(player.filePos);
    if (ping->lastTraceAlt < depthAlarm->level && ping->lastTraceAlt > 0.5)      depthAlarm_start();
    depthAlarm_updateHandlePosition(depthAlarm->level);

    // Read GPS Lattitude and Longitude
    Gnss_readGpsPointsFromFile();
    data_hashMap_update();
    if(player.filePos >= player.fileSize) {
        plot_range_moveRanges();
        plot_replotAll();
    }

    static int playbackCounter = 0;
    playbackCounter++;
    static QTime PlayerTimer;

    if (playbackCounter >= setting.plotRate) {
        //        qDebug() << "File Playback Timer: " << PlayerTimer.elapsed();
        PlayerTimer.start();
        plot_range_moveRanges();
        plot_replotAll();
        playbackCounter = 0;
    }
}

int MainWindow :: player_loadToBuffer() {
    if (player.filePos + 10000 > player.fileSize) {
        readFileTimer.stop();
        replotTimer.stop();
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
        ui->sensor_graph->replot(QCustomPlot::rpQueuedReplot);

        if (cropTool->toolActive && cropTool->ping_start > 0) {
            player_playData(false);
            return 0;
        }

        //emit converterDialogProgressBar(playBackDataBuffer.size());
        status_setProgressBarValue(player.fileBuffer.size());
        bottomTrack_clearQueue();

        tools->removeAllTargetsFromList();
        plot_replotAll();
        ChartWindowInstance->target_resizeAllTargets();
        ChartWindowInstance->plot_replot();
        ChartWindowInstance->target_resizeAllTargets();
        //                selectDataFile();
        player_reset();//selectDataFile();
        player_addColorMap();
        player_playData(true);
        setup_buttonsAndTools();
        return -1;
    }
    if (player.bufferPos + 100000 > player.fileBuffer.size()) {
        player.fileBuffer.remove(0, player.bufferPos);
        player.bufferPos = 0;
        player.fileBuffer.append(player.sonarFile.read(5000000));
        player.dataPtr = player.fileBuffer.data();
        return 1;
    }
    return 0;
}

void MainWindow :: player_restartFile() {
    player.sonarFile.reset();
    player.fileBuffer = player.sonarFile.read(5000000);
    player.dataPtr = player.fileBuffer.data();
    player.filePos = 0;
    player.bufferPos = 0;
}

void MainWindow :: player_reset() {
    // Turn off all tools
    if (flag.measuringTapeActive)   on_actionMeasuring_Tape_triggered();
    if (flag.targetToolActive)         on_actionFlag_triggered();
    if (flag.shadowToolActive)      on_actionShadow_triggered();
    if (cropTool->toolActive)       on_actionCrop_File_triggered();

    // Clear the sonar plots first
    ping->lastTracePos = 0;
    plot_range_moveRanges();
    plot_clearSonarPlots();
    //resetPlotView();
    readFileTimer.stop();
    player_restartFile();
    cropTool->resetSlider();
    if (cropTool->toolActive)   on_actionCrop_File_triggered();
    ChartWindowInstance->totalDistance = 0;
    ChartWindowInstance->gpsSpeed = 0;
    dataMap.clearAll();

    bottomTrack_clearQueue();

    ChartWindowInstance->newCurve->data()->clear();
    ChartWindowInstance->newCurve2->data()->clear();
    ChartWindowInstance->newCurve3->data()->clear();
    ChartWindowInstance->portLine->setVisible(false);
    ChartWindowInstance->stbdLine->setVisible(false);

    // Now bring all properties back to a default state
    plot_showUpdatedZoomLegends();

    // Open the JASLOG file to match GPS coordinates to sonar pings
    qDebug()<<"opened line 3620";
    player.GPSFile.setFileName(player.openedFileName+"LOG");
    if (player.GPSFile.exists()) {
        player.GPSFile.open(QIODevice::ReadOnly);
        state.GNSS_MODE = 3;
    } else      state.GNSS_MODE = 0;
    target_LoadFromFile();
}

/**
 * @brief MainWindow::selectDataFile
 * To select the data file for the playback and read the
 * primary parameters to intialise the plotting area with the
 * detected range variables.
 */
void MainWindow :: player_selectFile() {
    if (state.GUI_MODE == 1 && state.RT_MODE > 0)   tcpSocket->disconnectFromHost();

    // Set to player mode
    state.GUI_MODE = 2;
    if (state.PLAY_MODE == 6)     state.PLAY_MODE = 1;
    int readFileBytes = 0;//readFileBytes is used to keep track of the bytes read, and show that percentage on the recorder's progress bar
    readFileTimer.stop(); // stop last file from playing

    // Measuring tape should be switched to false when changing files can cause crashes
    if (flag.measuringTapeActive)    on_actionMeasuring_Tape_triggered();

    if (state.PLAY_MODE == 1) { // If no file selected previously
        state.PLAY_MODE = 2;
        QString data_filePath = QFileDialog::getOpenFileName(this,"Select the data file",player.openPath,
                                                             "Data File (*.jss *.JAS *.xtf)\nChart File (*.gpx)");

        if(data_filePath.size() == 0) {
            // If the user presses cancel on the file dialog window
            state.PLAY_MODE = 3;
            status_showFileName("");//sets file name to blank text
            if (player.fileBuffer == nullptr) {//if no file selected
                state.PLAY_MODE = 0;
                state.GUI_MODE = 1;
                setup_buttonsAndTools();
                return;
            }
            return;
        }

        on_actionClose_triggered();
        state.GUI_MODE = 2;
        state.PLAY_MODE = 2;
        plot_range_moveRanges();
        plot_replotAll();
        ChartWindowInstance->path_resetTrackpoints();
        tools->removeAllWaypointsFromList();
        resetGui();

        // Read the sonar/chart/gpx file
        QFileInfo fi(data_filePath);
        QString data_fileName = fi.filePath();
        QString extension = fi.suffix();
        if ((extension.compare("xtf") == 0) || (extension.compare("XTF") == 0)) {
            player.typeXtf = true;
            state.GUI_MODE =3; //new mode that sets to converting mode
        }
        else if (extension.compare("gpx") == 0) {
            // Read .GPX file to plot chart
            trkptGPX.setFileName(data_filePath);
            trkptGPX.open(QIODevice::ReadWrite | QIODevice::Text);
            Gnss_readGpxFile();
            state.PLAY_MODE = 1;
            return;
        }
        else { // .JAS or .jss file
            player.typeXtf = false;
            flag.convertToXtf = false;
        }

        resetGui();
        player.openedFileName = data_fileName;

        // Open the JASLOG file to match GPS coordinates to sonar pings
        player.GPSFile.setFileName(player.openedFileName+"LOG");
        if(player.GPSFile.exists()){
            player.GPSFile.open(QIODevice::ReadOnly);
            state.GNSS_MODE = 3;

            // Open corresponding Flag file, if exists
            QFile file(player.openedFileName+"FLAG");
            if(file.exists())   file.open(QIODevice::ReadOnly);
        } else      state.GNSS_MODE = 0;

        setup_buttonsAndTools();
        player.sonarFile.setFileName(data_filePath);
        status_setFileSize(player.sonarFile.size());

        //read the initial ping, so the percentage on progress bar is 0.
        status_setProgressBarValue(0);
        status_showFileName(data_fileName);

        // Open file, read first 5MB, and copy data into buffer
        player.fileSize = player.sonarFile.size();
        player.sonarFile.open(QIODevice::ReadOnly);
//        playBackDataBuffer = sonarReadFile.readAll();
        player.fileBuffer = player.sonarFile.read(5000000);
    }

    if (player.fileBuffer.data() == nullptr) {//if no file selected
        state.PLAY_MODE = 1; //cancel playback
        if(ui->quick_playpause->isChecked()){
            ui->quick_playpause->setChecked(false);
        }
        return;
    }
    else if (state.PLAY_MODE == 3) {
        // Reset GPS Plot
        ChartWindowInstance->newCurve->data()->clear();
        ChartWindowInstance->newCurve2->data()->clear();
        ChartWindowInstance->newCurve3->data()->clear();
        ChartWindowInstance->portLine->setVisible(false);
        ChartWindowInstance->stbdLine->setVisible(false);

        // Open the JASLOG file to match GPS coordinates to sonar pings
        qDebug()<<"opened line 3831";
        player.GPSFile.setFileName(player.openedFileName+"LOG");
        if(player.GPSFile.exists()){
            player.GPSFile.open(QIODevice::ReadOnly);
            state.GNSS_MODE = 3;
        } else      state.GNSS_MODE = 0;
    }

    if(!player.typeXtf) {
        readFileBytes = 0;
        char *data_pointer = player.fileBuffer.data();//set data pointer to beginning of file

        track.sampleNum_port = 300;
        //header data
        quint32 syncBytes, packetID, dataSize;
        //For the very first ping
        //Packet Header
        data_pointer += data_copy(&syncBytes, data_pointer, 4, &readFileBytes);   //Sync Word 1&2
        data_pointer += data_copy(&packetID, data_pointer, 4, &readFileBytes);    //Message Type is 0 for the very first ping
        data_pointer += data_copy(&dataSize, data_pointer, 4, &readFileBytes);    //Packet Size

        if (state.PLAY_MODE == 2) {
            ping->rangeLf_double =  60.0;
            ping->rangeLf_int =  60;
        }

        QThreadPool pool;
        QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess,
                                                                    data_pointer, packetID, &readFileBytes); //Runs processDataToPlot in a thread
        while (!processDataThreadProgress.isFinished()) //During the function's execution
        {
            if (flag.thread_middlePoint == true) //When it reaches this point
            {
                processDataThreadProgress.togglePaused(); //Pause the function
                flag.thread_middlePoint = false;
                break;
            }
        }
        data_hashMap_update();

        if (state.PLAY_MODE == 2) {
            if (ping->highFrequency) {
                flag.PlotHfSidescan = 1;
                flag.useDualFrequency = 1;
            } else {
                flag.PlotHfSidescan = 0;
                flag.useDualFrequency = 0;
            }
            if (ping->recieveDepth) {
                flag.PlotAngleScan = 1;
            } else {
                flag.PlotAngleScan = 0;
            }

            if (ping->Oct31ASTest) {
                flag.PlotAngleScan = 1;
                flag.PlotHfSidescan = 0;
                flag.useDualFrequency = 1;
            }

            format_preparePlots();
        }

        if (ping->rangeLf_tmp > 0) {
            setRecordedRange(ping->rangeLf_tmp);
        }
        else if (ping->rangeHf_tmp > 0) {
            setRecordedRange(ping->rangeHf_tmp);
        }
        ping->rangeLf_int = ping->rangeLf_tmp;
        if (flag.useDualFrequency)       ping->rangeHf_int = ping->rangeHf_tmp;

        flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue
        processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data
        int lastPing = player_findLastPing();
        player_restartFile();

        ChartWindowInstance->plot_configureBoatIcon(flag.PlotAngleScan);
        target_LoadFromFile();
        player.dataPtr = player.fileBuffer.data();
        player.bufferPos = 0;
        player.filePos = 0;
    }
    else  {
        player.dataPtr = player.fileBuffer.data();
        readFileBytes = 0;
        // Read XTFFILEHEADER
        player.dataPtr += xtf_readFile(player.dataPtr, 1, &readFileBytes);

        qDebug() << "XTFFILEHEADER 1" << fileHeaderObject.RecordingProgramName << fileHeaderObject.RecordingProgramVersion <<
                    fileHeaderObject.SonarName << fileHeaderObject.SonarType << fileHeaderObject.ThisFileName <<
                    fileHeaderObject.NumberOfSonarChannels;

        // Read CHANINFO 6 times
        player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
        qDebug() << "CHANINFO 1" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
        player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
        qDebug() << "CHANINFO 2" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
        player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
        qDebug() << "CHANINFO 3" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
        player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
        qDebug() << "CHANINFO 4" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
        player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
        qDebug() << "CHANINFO 5" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;
        player.dataPtr += xtf_readFile(player.dataPtr, 4, &readFileBytes);
        qDebug() << "CHANINFO 6" << chanInfoObject.TypeOfChannel << chanInfoObject.Frequency << chanInfoObject.SampleFormat << readFileBytes;


        // Read XTFPINGHEADER
        player.dataPtr += xtf_readFile(player.dataPtr, 2, &readFileBytes);
        qDebug() << "XTFPINGHEADER" << pingHeaderObject.HeaderType << pingHeaderObject.NumChansToFollow <<
                    pingHeaderObject.NumBytesThisRecord << pingHeaderObject.PingNumber << readFileBytes;
        //        if (pingHeaderObject.HeaderType == 0) {
        if (fileHeaderObject.NumberOfSonarChannels == 2) {
            flag.PlotHfSidescan = 0;
        }
        else if (fileHeaderObject.NumberOfSonarChannels == 4) {
            flag.PlotHfSidescan = 1;
        }
        //        }
        if(flag.convertToXtf==false){
            format_preparePlots();
        }

        // Read XTFPINGCHANHEADER
        player.dataPtr += xtf_readFile(player.dataPtr, 3, &readFileBytes);
        qDebug() << "XTFPINGCHANHEADER" << pingChHeaderObject.ChannelNumber << pingChHeaderObject.SlantRange <<
                    pingChHeaderObject.Frequency << pingChHeaderObject.NumSamples << readFileBytes;

        if (ping->rangeLf_tmp > 0) {
            setRecordedRange(ping->rangeLf_tmp);
        }
        else if (ping->rangeHf_tmp > 0) {
            setRecordedRange(ping->rangeHf_tmp);
        }
        ping->rangeLf_int = ping->rangeLf_tmp;
        if (flag.useDualFrequency)       ping->rangeHf_int = ping->rangeHf_tmp;

        player.dataPtr = player.fileBuffer.data();
        readFileBytes = 0;
        player.dataPtr += 1024;
        readFileBytes += 1024;
        player.bufferPos = readFileBytes;
        player.filePos = readFileBytes;
    }

    ChartWindowInstance->target_resizeAllTargets();
    target_ShowIconsOnPlot();

    state.PLAY_MODE = 4; // pause file
    setup_rangeButtonActivate(false);
    //setPlotRanges();
    resetPlotView();

    if (PLAYER_DEMO)    on_openFileButton_clicked(); // Open Glen Haven chart
}

void MainWindow :: player_playFromPing (int pingStart) {
    if (pingStart < 0)      return;
    if (cropTool->toolActive && cropTool->ping_start > pingStart)   return;
    // Start from the beginning of file
    player_restartFile();
    int readFileBytes = 0;

    if(!player.typeXtf) {
        bottomTrack_clearQueue();
        dataMap.clearAll();

        player.GPSFile.seek(0);
        player.GPSFile.readLine();

        int pingDifference = pingStart - ping->pingNumCurrent;
        if (pingDifference < 0) {

            tools->removeAllTargetIcons();
            target_removeSnipBoxes();
            target_selectAllTargets(false);

            ChartWindowInstance->newCurve->data()->remove(ChartWindowInstance->newCurve->data()->size()+pingDifference-1, ChartWindowInstance->newCurve->data()->size());
            ChartWindowInstance->newCurve2->data()->remove(ChartWindowInstance->newCurve2->data()->size()+pingDifference-1, ChartWindowInstance->newCurve2->data()->size());
            ChartWindowInstance->newCurve3->data()->remove(ChartWindowInstance->newCurve3->data()->size()+pingDifference-1, ChartWindowInstance->newCurve3->data()->size());
            ChartWindowInstance->path_addData(ChartWindowInstance->newCurve->data()->at(ChartWindowInstance->newCurve->data()->size()-1)->key,
                                         ChartWindowInstance->newCurve->data()->at(ChartWindowInstance->newCurve->data()->size()-1)->value);
            ChartWindowInstance->plot_correctBoatHeading();

        } else {
            // Fast forward by pingDifference
            for (int i = 0; i < pingDifference; i++) {
                Gnss_readGpsPointsFromFile();
                ping->pingNumCurrent++;
            }
        }
        ChartWindowInstance->target_resizeAllTargets();
        ChartWindowInstance->mapPlotPtr->replot();

        ping->pingNumCurrent = pingStart;

        player.dataPtr = player.fileBuffer.data();
        while (player.filePos < player.fileSize - 50) {
            readFileBytes = 0;

            quint32 syncBytes, packetID, dataSize;
            //For the very first ping
            //Packet Header
            player.dataPtr += data_copy(&syncBytes, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
            if (syncBytes != 0x8000FFFF) {
                qDebug() << "Misaligned";
            }
            player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);        //Message Type is 0 for the very first ping
            player.dataPtr += data_copy(&dataSize, player.dataPtr, 4, &readFileBytes);                 //Packet Size

            player.dataPtr += Jas_readPingHeader(player.dataPtr, packetID, &readFileBytes);

            // Go back to start of ping
            player.dataPtr -= ping->pingHeaderSize + 12;
            readFileBytes -= ping->pingHeaderSize + 12;
            if (ping->pingNumCurrent >= pingStart) {
                // Stop here
                //player.filePos += readFileBytes;
                //player.bufferPos += readFileBytes;
                break;
            } else {
                // go to next ping
                player.dataPtr += dataSize + 4;
                readFileBytes += dataSize + 4;
                player.filePos += readFileBytes;
                player.bufferPos += readFileBytes;

                player_loadToBuffer();
            }
        }
    }
    status_setProgressBarValue(player.filePos);
}

void MainWindow :: player_skipPings (int pingsToSkip) {
    if(!player.typeXtf) {
        quint32 pingNum = 0;
        int readFileBytes = 0;
        bottomTrack_clearQueue();
        dataMap.clearAll();
        tools->removeAllTargetIcons();
        target_removeSnipBoxes();
        target_selectAllTargets(false);

        for (int i = 0; i < pingsToSkip; i++) {
            readFileBytes = 0;

            quint32 check;
            memcpy(&check, player.dataPtr, 4);//checks for footer value used to stop playback at end of file
            if (check == 0xFAAF && !player.typeXtf) {//if footer detected on .jss file, end playback
                status_setProgressBarValue(player.fileBuffer.size());
                return;
            }

            quint32 syncBytes, packetID, dataSize;
            //For the very first ping
            //Packet Header
            player.dataPtr += data_copy(&syncBytes, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
            if (syncBytes != 0x8000FFFF) {
                qDebug() << "Misaligned";
            }
            player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);        //Message Type is 0 for the very first ping
            player.dataPtr += data_copy(&dataSize, player.dataPtr, 4, &readFileBytes);                 //Packet Size

            player.dataPtr += Jas_readPingHeader(player.dataPtr, packetID, &readFileBytes);
            Gnss_readGpsPointsFromFile();

            // Go back to start of ping
            player.dataPtr -= ping->pingHeaderSize + 12;
            readFileBytes -= ping->pingHeaderSize + 12;
            if (player.filePos > player.fileSize - 50) {
                // Stop here
                break;
            } else {
                // go to next ping
                player.dataPtr += dataSize + 4;
                readFileBytes += dataSize + 4;
                player.filePos += readFileBytes;
                player.bufferPos += readFileBytes;
                player_loadToBuffer();
            }
        }
        //emit converterDialogProgressBar(numOfFileBytes);
        status_setProgressBarValue(player.filePos);
    }
}

void MainWindow :: player_showSegment (int pingStart, int pingEnd) {
    player_restartFile();
    int readFileBytes = 0;
    int playbackCounter = 0;

    if(!player.typeXtf) {
        bottomTrack_clearQueue();
        dataMap.clearAll();

        player.GPSFile.seek(0);
        player.GPSFile.readLine();

        int pingDifference = pingStart - ping->pingNumCurrent;
        if (pingDifference < 0) {

            tools->removeAllTargetIcons();

            ChartWindowInstance->newCurve->data()->remove(ChartWindowInstance->newCurve->data()->size()+pingDifference-1, ChartWindowInstance->newCurve->data()->size());
            ChartWindowInstance->newCurve2->data()->remove(ChartWindowInstance->newCurve2->data()->size()+pingDifference-1, ChartWindowInstance->newCurve2->data()->size());
            ChartWindowInstance->newCurve3->data()->remove(ChartWindowInstance->newCurve3->data()->size()+pingDifference-1, ChartWindowInstance->newCurve3->data()->size());
            ChartWindowInstance->path_addData(ChartWindowInstance->newCurve->data()->at(ChartWindowInstance->newCurve->data()->size()-1)->key,
                                         ChartWindowInstance->newCurve->data()->at(ChartWindowInstance->newCurve->data()->size()-1)->value);

        } else {
            // Fast forward by pingDifference
            for (int i = 0; i < pingDifference; i++) {
                Gnss_readGpsPointsFromFile();
                ping->pingNumCurrent++;
            }
        }
        ChartWindowInstance->target_resizeAllTargets();
        ChartWindowInstance->mapPlotPtr->replot();

        ping->pingNumCurrent = pingStart;

        player.dataPtr = player.fileBuffer.data();

        // Find the start ping
        while (player.filePos < player.fileSize - 50) {
            readFileBytes = 0;

            quint32 syncBytes, packetID, dataSize;
            //For the very first ping
            //Packet Header
            player.dataPtr += data_copy(&syncBytes, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
            if (syncBytes != 0x8000FFFF) {
                qDebug() << "Misaligned";
            }
            player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);        //Message Type is 0 for the very first ping
            player.dataPtr += data_copy(&dataSize, player.dataPtr, 4, &readFileBytes);                 //Packet Size

            player.dataPtr += Jas_readPingHeader(player.dataPtr, packetID, &readFileBytes);

            // Go back to start of ping
            player.dataPtr -= ping->pingHeaderSize + 12;
            readFileBytes -= ping->pingHeaderSize + 12;
            if (ping->pingNumCurrent >= pingStart) {
                // Stop here
                //player.filePos += readFileBytes;
                //player.bufferPos += readFileBytes;
                break;
            } else {
                // go to next ping
                player.dataPtr += dataSize + 4;
                readFileBytes += dataSize + 4;
                player.filePos += readFileBytes;
                player.bufferPos += readFileBytes;

                player_loadToBuffer();
            }
        }

        // Plot area between start to end
        for (int k = 0; k < pingEnd - pingStart; k++) {
            quint32 check;
            quint32 packetID, syncword, packetsize;

            readFileBytes = 0;

            memcpy(&check, player.dataPtr, 4);//checks for footer value used to stop playback at end of file
            if ((check & 0x0000FFFF) == 0xFAAF && !player.typeXtf) {//if footer detected on .jss file, end playback
                readFileTimer.stop();
                replotTimer.stop();
                customPlotLF->replot(QCustomPlot::rpQueuedReplot);
                customPlotHF->replot(QCustomPlot::rpQueuedReplot);
                ui->sensor_graph->replot(QCustomPlot::rpQueuedReplot);

                //emit converterDialogProgressBar(playBackDataBuffer.size());
                status_setProgressBarValue(player.fileBuffer.size());
                bottomTrack_clearQueue();

                tools->removeAllTargetsFromList();
                plot_replotAll();
                ChartWindowInstance->target_resizeAllTargets();
                ChartWindowInstance->plot_replot();
                ChartWindowInstance->target_resizeAllTargets();
                //                selectDataFile();
                player_reset();//selectDataFile();
                player_addColorMap();
                player_playData(true);
                setup_buttonsAndTools();
                break;
            }

            //sync word in packet header
            player.dataPtr += data_copy(&syncword, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
            player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);

            if (packetID > 0 && packetID < 20) {//packetID = 3 represents the presence of a additional depth sounder on the board
                ping->recieveDepth = true;
            } else {
                ping->recieveDepth = false;
            }

            player.dataPtr += data_copy(&packetsize, player.dataPtr, 4, &readFileBytes);

            QThreadPool pool;
            QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess, player.dataPtr, packetID, &readFileBytes); //Runs processDataToPlot in a thread
            while (!processDataThreadProgress.isFinished()) //During the function's execution
            {
                if (flag.thread_middlePoint == true) //When it reaches this point
                {
                    processDataThreadProgress.togglePaused(); //Pause the function
                    flag.thread_middlePoint = false;
                    break;
                }
            }
            data_hashMap_update();

            //While the function is paused, do the code below
            if(ping->rangeLf_tmp != ping->rangeLf_int) {//if range recieved from the board is not the current GUI range, set GUI range to board range
                if (ping->rangeLf_tmp > 0) {
                    setRecordedRange(ping->rangeLf_tmp);
                    flag.highRangeChange = true;
                }
                ping->rangeLf_int = ping->rangeLf_tmp; // Only use this after setRecordedRange is called
            }
            if(ping->rangeHf_tmp != ping->rangeHf_int) {//same as above
                if (ping->rangeHf_tmp > 0 && !flag.highRangeChange) {
                    setRecordedRange(ping->rangeHf_tmp);
                }
                ping->rangeHf_int = ping->rangeHf_tmp; // Only use this after setRecordedRange is called
                flag.highRangeChange = false;
            }
            flag.highRangeChange = false;

            flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue

            processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data

            player.dataPtr += readFileBytes - 12;

            player.filePos += readFileBytes;
            player.bufferPos += readFileBytes;
            plot_data_addTrace();
            plot_data_checkToAddNewMap();
            status_setProgressBarValue(player.filePos);

            // Read GPS Lattitude and Longitude
            Gnss_readGpsPointsFromFile();
            if(player.filePos > player.fileSize - 50) {
                plot_range_moveRanges();
                plot_replotAll();
            }
            player_loadToBuffer();

            playbackCounter++;
            if (playbackCounter >= setting.plotRate) {
                plot_range_moveRanges();
                plot_replotAll(1);
                playbackCounter = 0;
            }
        }

        plot_range_moveRanges();
        plot_replotAll();
    }
    status_setProgressBarValue(player.filePos);
    target_ShowIconsOnPlot();
    player_playData(false);
}

int MainWindow :: player_findLastPing () {
    player.sonarFile.reset();
    player.filePos = player.sonarFile.skip(player.fileSize - 100000);
    player.fileBuffer = player.sonarFile.read(100000);
    player.dataPtr = player.fileBuffer.data();
    player.bufferPos = 0;
    player_loadToBuffer();
    int readFileBytes = 0;
    quint32 syncBytes;

    data_copy(&syncBytes, player.dataPtr, 4, &readFileBytes);
    while (syncBytes != 0x8000FFFF) {
        player.dataPtr++;
        player.filePos++;
        player.bufferPos++;
        data_copy(&syncBytes, player.dataPtr, 4, &readFileBytes);
//        qDebug() << "Misaligned";
    }

    while (player.filePos < player.fileSize - 50) {
        readFileBytes = 0;

        quint32 packetID, dataSize;
        //For the very first ping
        //Packet Header
        player.dataPtr += data_copy(&syncBytes, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
        if (syncBytes != 0x8000FFFF) {
            qDebug() << "Misaligned";
        }
        player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);        //Message Type is 0 for the very first ping
        player.dataPtr += data_copy(&dataSize, player.dataPtr, 4, &readFileBytes);                 //Packet Size

        player.dataPtr += Jas_readPingHeader(player.dataPtr, packetID, &readFileBytes);

        // Go back to start of ping
        player.dataPtr -= ping->pingHeaderSize + 12;
        readFileBytes -= ping->pingHeaderSize + 12;
        player.dataPtr += dataSize + 4;
        readFileBytes += dataSize + 4;
        player.filePos += readFileBytes;
        player.bufferPos += readFileBytes;

//        player_loadToBuffer();
        ping->pingNumEnd = ping->pingNumCurrent;
    }
    return ping->pingNumEnd;
}

void MainWindow :: Gnss_Update() {
    if (state.GNSS_MODE > 1 && state.GUI_MODE < 2 && state.RT_MODE < 1 && state.SIM_MODE < 1) {  // Simulation mode or real time mode
        if (!gnssController->validCoords)   return;
        ChartWindowInstance->path_addData(gnssController->Longitude(), gnssController->Latitude());
        Gnss_writeTrackPoints();
        ChartWindowInstance->mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
    }
    else if (state.GNSS_MODE > 1) {
        if (!gnssController->validCoords)   return;
        ChartWindowInstance->path_addData(gnssController->Longitude(), gnssController->Latitude());
        Gnss_writeTrackPoints();
        if (gnssController->SpeedType != 1)     Gnss_determineSpeed();
        if (gnssController->HeadingType != 1)
            gnssController->updateHeading(ChartWindowInstance->gnss_calculateHeading(gnssController->Longitude(), gnssController->Latitude(), gnssController->Heading()));
        ChartWindowInstance->legend_updateSpeedText(gnssController->Speed());
        ChartWindowInstance->legend_updateHeadingText(gnssController->Heading());

        double dist = ping->rangeLf_double;
        if (flag.showGap)   dist -= ping->lastTraceAlt;
        ChartWindowInstance->path_addBoundaryPoints(gnssController->Longitude(), gnssController->Latitude(),
                                               gnssController->Heading() + ping->lastTracedyaw, dist, ping->angleOrientation);

        ChartWindowInstance->mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: Gnss_updateButtonState(bool buttonActive) {
    if (buttonActive) {
        state.GNSS_MODE = 1;
        QIcon connect_icon(":/icons/icons/gps_on.png");
        connect_icon.addPixmap(QPixmap(":/icons/icons/gps_disabled.png"), QIcon::Disabled);
        ui->gnssBtn->setIcon(connect_icon);
        ui->gnssBtn->setToolTip("GNSS Receiver Connected");
        ChartWindowInstance->plot_showBoatIcon(buttonActive);
    }
    else {
        state.GNSS_MODE = 0;
        QIcon connect_icon(":/icons/icons/gps_off.png");
        connect_icon.addPixmap(QPixmap(":/icons/icons/gps_disabled.png"), QIcon::Disabled);
        ui->gnssBtn->setIcon(connect_icon);
        ui->gnssBtn->setToolTip("Connect to GNSS Receiver");
        ChartWindowInstance->plot_showBoatIcon(buttonActive);
    }
}

int  MainWindow :: Gnss_connectUsbNmea() {
    sonarComPort  = new QSerialPort(this);
    bool adruino_isAvailable = false;
    QString adruino_portname = "";
    foreach(const QSerialPortInfo &serialPortInfo,QSerialPortInfo::availablePorts()){//creates temporary serialportinfo we use to detect if an arduino is connected

        if(serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier()){
            if(serialPortInfo.vendorIdentifier() == 0x03fd){
                if(serialPortInfo.productIdentifier() == 0x0105){
                    adruino_portname = serialPortInfo.portName();
                    adruino_isAvailable = true;
                }
            }
        }

    }

    if(adruino_isAvailable){
        //open port
        QSysInfo *info = new QSysInfo();
        if (info->kernelType().compare("linux") == 0) {//if a linux operating system is detected
            adruino_portname = "/dev/" + adruino_portname;
            //the commented out code below is to automate the process of giving permissions to access the arduino's serial port, it currently doesn't work and must be done manually
            //don't worry about giving permissions on the jetasonic account though, I configured it to always have permissions
        }

        //if windows the all of the above is done automatically by the OS

        free(info);//frees memory used by info as we don't need it any more
        sonarComPort->setPortName(adruino_portname);//sets the port name of the arduino
        sonarComPort->open(QSerialPort::WriteOnly);//sets arduino to read only
        sonarComPort->setBaudRate(QSerialPort::Baud115200);//sets arduino baud rate to 115200, at this rate we can recieve clear data
        sonarComPort->setDataBits(QSerialPort::Data8);//set arduino data size to 8
        sonarComPort->setParity(QSerialPort::NoParity);//no parity

        sonarComPort->setStopBits(QSerialPort::OneStop);
        sonarComPort->setFlowControl(QSerialPort::NoFlowControl);

        //connect(adruino,SIGNAL(readyRead()),this, SLOT(readSerial()));

        UsbTimer.start(1000);
        sonarComPortReady = true;
        return true;
    }
    else{
        sonarComPortReady = false;
        sonarComPort->close();

        return false;
    }

    /*
    qDebug() << "T - USB Active";
    libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
    libusb_context *ctx = NULL; //a libusb session
    int r; //for return values
    ssize_t cnt; //holding number of devices in list
    r = libusb_init(&ctx); //initialize a library session
    if(r < 0) {
        qDebug()<<"Init Error "<<r; //there was an error
    }
    libusb_set_debug(ctx, 3); //set verbosity level to 3, as suggested in the documentation
    cnt = libusb_get_device_list(ctx, &devs); //get the list of devices

    //        libusb_device_handle *dev_handle; //a device handle
    dev_handle = libusb_open_device_with_vid_pid(ctx, 1021, 257); //Looks for FPGA with Vendor ID 1021 and Product ID 257
    if(dev_handle == NULL) {
        qDebug() <<"Cannot open device"<<endl;
        return;
    } else {
        qDebug()<<"Device Opened"<<endl;
    }

    unsigned char *data = new unsigned char[10]; //data to write
    data[0]=1;data[1]=15;data[2]=30;data[3]=250; //some dummy values
    int actual; //used to find out how many bytes were written
    if(libusb_kernel_driver_active(dev_handle, 0) == 1) { //find out if kernel driver is attached
        qDebug()<<"Kernel Driver Active";
        if(libusb_detach_kernel_driver(dev_handle, 0) == 0) //detach it
            qDebug()<<"Kernel Driver Detached!";
    }
    r = libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)
    if(r < 0) {
        qDebug()<<"Cannot Claim Interface";
    }

    //        qDebug()<<"Data->"<<data[0]<<data[1]<<data[2]<<data[3] <<"<-"; //just to see the data we want to write : abcd
    //        qDebug()<<"Writing Data...";
    //        r = libusb_bulk_transfer(dev_handle, (3 | LIBUSB_ENDPOINT_OUT), data, 4, &actual, 0); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129
    //        if(r == 0 && actual == 4) //we wrote the 4 bytes successfully
    //            qDebug()<<"Writing Successful!";
    //        else
    //            qDebug()<<"Write Error";

    //        unsigned char *data2 = new unsigned char[10];
    //    //    memset(my_string, '\0', 64);
    //    //    data2 = (unsigned char *)malloc(10);
    //        r = libusb_bulk_transfer(dev_handle, (2 | LIBUSB_ENDPOINT_IN), data2, 256, &actual, 10); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129
    //        qDebug() << "Incoming:  " << data2[0]<< data2[1] << data2[2] << "Actual: " << actual;

    //    r = libusb_bulk_transfer(dev_handle, (2 | 128), data2, 9, &actual, 0); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129
    //    qDebug() << "Incoming:  " << data << "Actual: " << actual;
    if(dev_handle == NULL) {
        qDebug() <<"Cannot open device"<<endl;
        return;
    } else {
        qDebug()<<"USB NMEA Device Opened"<<endl;
    }*/
}

void MainWindow :: Gnss_sendNmeaZda(QString data) {
    if (sonarComPortReady == false)    return;

    QByteArray sendArr;
    quint32 packetSize = 15;
    QString ArrString;
    if (data.length() == 0) {
        ArrString.append("$GPZDA,");

        QDate date = QDate::currentDate();
        QByteArray year = QString::number(date.year()).toLatin1();
        QByteArray day = QString::number(date.day()).toLatin1();
        QByteArray month = QString::number(date.month()).toLatin1();

        QTime time = QTime:: currentTime();
        QByteArray hour = QString::number(time.hour()).rightJustified(2,'0').toLatin1();
        QByteArray minutes = QString::number(time.minute()).rightJustified(2,'0').toLatin1();
        QByteArray seconds = QString::number(time.second()).rightJustified(2,'0').toLatin1();

        ArrString.append(hour);
        ArrString.append(minutes);
        ArrString.append(seconds);
        ArrString.append(".000,");
        ArrString.append(day);
        ArrString.append(",");
        ArrString.append(month);
        ArrString.append(",");
        ArrString.append(year);
        ArrString.append(",00*45\r\n");
    } else {
        ArrString = data;
        ArrString.append("\r\n");
    }
    sendArr.append(ArrString);

    int num = 0;
    num = sonarComPort->write(sendArr, sendArr.size());

    if (data.length() == 0) {
        Gnss_sendNmeaGga();
    }
    //    adruino->waitForBytesWritten(1000);
    //    adruino->readAll();
}

void MainWindow :: Gnss_sendNmeaGga(QString data) {
    if (sonarComPortReady == false)    return;

    QByteArray sendArr;
    quint32 packetSize = 15;
    QString ArrString;
    if (data.length() == 0) {
        ArrString.append("$GPGGA,");

        QDate date = QDate::currentDate();
        QByteArray year = QString::number(date.year()).toLatin1();
        QByteArray day = QString::number(date.day()).toLatin1();
        QByteArray month = QString::number(date.month()).toLatin1();

        QTime time = QTime:: currentTime();
        QByteArray hour = QString::number(time.hour()).rightJustified(2,'0').toLatin1();
        QByteArray minutes = QString::number(time.minute()).rightJustified(2,'0').toLatin1();
        QByteArray seconds = QString::number(time.second()).rightJustified(2,'0').toLatin1();

        ArrString.append(hour);
        ArrString.append(minutes);
        ArrString.append(seconds);
        ArrString.append(".000,4439.64626,N,06332.54922,W,2,6,1.2,18.893,M,-25.669,M,2.0,0031*4F\r\n");
    } else {
        ArrString = data;
        ArrString.append("\r\n");
    }
    sendArr.append(ArrString);


    /*if (dev_handle != NULL) { //Else if USB
        //qDebug() << "Sending USB";
        unsigned char *usbArr = new unsigned char[50]; //data to write
        quint8 ArrPtr = 0;  // keeps track of where we are in usbArr
        memcpy(&usbArr[0], sendArr.constData(), sendArr.size());

        int actual = 0;
        int r=0;
        //qDebug() << "Before bulk";
        r = libusb_bulk_transfer(dev_handle, (3 | LIBUSB_ENDPOINT_OUT), usbArr, sendArr.size(), &actual, 0);

        //qDebug() << "After bulk";
        delete [] usbArr;

        if(r == 0) //we wrote the 4 bytes successfully
            qDebug()<<"Writing Successful!" << actual << sizeof(sendArr);
        else
            qDebug()<<"Write Error";
    }*/
    sonarComPort->write(sendArr, sendArr.size());
}

/***************** ethernet connection functions*****************/

bool MainWindow :: Tcp_collectData(int size) {
    QElapsedTimer tcpTimer;
    tcpTimer.start();
    while(tcpBuffer.size() < size ){ // only part of the message has been received
        tcpSocket->waitForReadyRead(200); // alternatively, store the buffer and wait for the next readyRead()
        tcpBuffer.append(tcpSocket->readAll());//(dataSize - buffer.size())); // append the remaining bytes of the message
        if (tcpBuffer.size() > 1000000) return 0;
        if (tcpTimer.elapsed() > 200)   return 0; // Prevent GUI from freezing
    }
    return 1;
}

/**
 * @brief MainWindow::processData
 * Funtion to read and sort the data packet received via TCP.
 * It reads the first 4 bytes of the packet to determine the
 * expected length of the data packet and waits for the expected number
 * of bytes to be received and then it sorts the data into Port Side, StarBoard Side,
 * Optional Channel data buffer. These buffers are used for plotting the trace.
 */
void MainWindow :: Tcp_readData() {

    QDataStream stream(tcpSocket);
    stream.setByteOrder(QDataStream::LittleEndian);
    QByteArray headerData = nullptr;//, b;

    if (tcpSocket->isWritable()) //Ethernet Mode
    {
        if (!Tcp_collectData(12))   return;
    }

    else if (dev_handle != NULL) //USB Mode
    {
        int r = 0, actual;
        unsigned char *headerUsb = new unsigned char[15];

        r = libusb_bulk_transfer(dev_handle, (2 | 128), headerUsb, 12, &actual, 10); // only read data for 10ms
        //        qDebug() << "USB - Incoming:  " << headerUsb << "Actual: " << actual;
        if (r!=0) {
            // Reading Failed, exit function
            //            qDebug() << "USB - No reading";
            return;
        } else {
            if (actual < 12) {
                int actual2 = actual;
                r = libusb_bulk_transfer(dev_handle, (2 | 128), &headerUsb[actual2], 12-actual, &actual2, 10);
                qDebug() << "USB - Read another " << actual2 << "bytes";
            }
        }
        //        headerData = (const char *)headerUsb;
        headerData = QByteArray::fromRawData((const char *)headerUsb, 12);
        //        memcpy(headerData.constData(), &headerUsb[0], 12);

        delete [] headerUsb;

        //        return;
    }

    char *headerPtr = tcpBuffer.data();

    // Read JAS Packet Header
    quint32 dataSize = 0, firmwareVersion, packetID = 0, syncBytes;
    int BytesPointer = 0;
    headerPtr += data_copy(&syncBytes, headerPtr, 4, &BytesPointer);

    int bytesToRemove = 0;
    while (syncBytes != 0x8000FFFF && bytesToRemove < tcpBuffer.length()) {
        bytesToRemove++;
        headerPtr += 1;
        memcpy(&syncBytes, headerPtr, 4);
        qDebug() << "TCP - out if sync!!!!";
    }
    if (bytesToRemove > 0)  tcpBuffer.remove(0, bytesToRemove);

    headerPtr += data_copy(&packetID, headerPtr, 4, &BytesPointer);
    headerPtr += data_copy(&dataSize, headerPtr, 4, &BytesPointer);
    dataSize += 4;

    if (packetID > 0 && packetID < 20) {//packetID = 3 represents the presence of a additional depth sounder on the board
        ping->recieveDepth = true;
    }

    if(tcpSocket->isWritable())  //Ethernet Mode
    {
        /* Ethernet Mode */
        if (!Tcp_collectData(dataSize))   return;
    }
    else
    {
        /* USB Mode */
        static quint8 first = 1;
        int r = 0, actual;
        static unsigned char *dataUsb;
        if(first) {
            dataUsb = new unsigned char[10000];
            first--;
        }

        qint32 index = 0;

        r = libusb_bulk_transfer(dev_handle, (2 | 128), dataUsb, (int)dataSize, &actual, 10); // only read data for 10ms
        // qDebug() << "USB - Incoming2:  " << dataSize << "Actual: " << actual;
        index += actual;
        QByteArray AppendArray = QByteArray::fromRawData((const char *)dataUsb, (int)actual);
        tcpBuffer.append(AppendArray);
        if (r!=0) {
            //qDebug() << "R is not 0";
            // Reading Failed, exit function
            return;
        } else {
            while (dataSize-index > 0) {
                r = libusb_bulk_transfer(dev_handle, (2 | 128), &dataUsb[0], (int)(dataSize-index), &actual, 100);
                //qDebug() << "USB - 2Read another " << dataSize-index << "bytes - " << actual;
                index += actual;
                QByteArray AppendArray = QByteArray::fromRawData((const char *)dataUsb, (int)actual);
                tcpBuffer.append(AppendArray);
                //qDebug() << "USB - next" << dataSize-index;
                //                memcpy(&dataUsb[index], &dataUsb2[0], actual);
            }

        }
        //        buffer = QByteArray::fromRawData((const char *)dataUsb, (int)dataSize);

        //        delete [] dataUsb;

        //   elapsedTimeUsb = timeout_timer.nsecsElapsed();
        //        qDebug() <<"USB - Elapsed time: " << elapsedTimeUsb;

        //        return;
    }

    char *data_pointer = tcpBuffer.data();//set pointer to beginning of buffer, this is used to navigate the tcp data
    data_pointer += 12;
    qint32 dummy_pointer = 0;

    if (packetID == 4)
    {
        quint32 pingSize;
        memcpy(&pingSize, data_pointer, 4);
        data_pointer += 4;

        quint8 seconds, minutes, hours, days, months;
        quint16 years, milliseconds;

        quint16 amtLFsamp;
        quint16 amtHFsamp;
        quint16 amtDepthsamp;
        quint16 lowRange;
        quint16 highRange;
        quint16 depthRange;
        quint32 pingNum;
        quint32 kalmanNum;

        data_pointer += data_copy(&milliseconds, data_pointer, 2, &dummy_pointer);
        data_pointer += data_copy(&seconds, data_pointer, 1, &dummy_pointer);
        data_pointer += data_copy(&minutes, data_pointer, 1, &dummy_pointer);
        data_pointer += data_copy(&hours, data_pointer, 1, &dummy_pointer);
        data_pointer += data_copy(&days, data_pointer, 1, &dummy_pointer);
        data_pointer += data_copy(&months, data_pointer, 1, &dummy_pointer);
        data_pointer += data_copy(&years, data_pointer, 1, &dummy_pointer);
        data_pointer += data_copy(&firmwareVersion, data_pointer, 4, &dummy_pointer);
        data_pointer += data_copy(&ping->lowFrequency, data_pointer, 2, &dummy_pointer);
        data_pointer += data_copy(&ping->highFrequency, data_pointer, 2, &dummy_pointer);

        diffTime = (seconds + (minutes*60) + (hours*60*60) + (days*24*60*60));
        diffMili = milliseconds;

        ping->firmwareVer = firmwareVersion;
        //qDebug() << "Depth value: " << recieveDepth;
        if(ping->recieveDepth){
            data_pointer += data_copy(&ping->depthFrequency, data_pointer, 2, &dummy_pointer);
        }

        if (ping->highFrequency == 0) {
            if(packetID == 5) {
                flag.useDualFrequency = 1;
            } else {
                flag.useDualFrequency = 0;
            }
        }
        else {
            flag.useDualFrequency = 1;
        }

        quint16 angle;
        memcpy(&angle, data_pointer, 2);
        ping->degree = angle;
        data_pointer += 2;
        emit defaultAngle(ping->degree);


        if(!ping->recieveDepth){
            data_pointer += 2;
        }

        data_pointer += data_copy(&amtLFsamp, data_pointer, 2, &dummy_pointer);
        data_pointer += data_copy(&amtHFsamp, data_pointer, 2, &dummy_pointer);

        if(ping->recieveDepth){
            data_pointer += data_copy(&amtDepthsamp, data_pointer, 2, &dummy_pointer);
        }

        data_pointer += data_copy(&lowRange, data_pointer, 2, &dummy_pointer);
        data_pointer += data_copy(&highRange, data_pointer, 2, &dummy_pointer);

        if(ping->recieveDepth){
            data_pointer += data_copy(&depthRange, data_pointer, 2, &dummy_pointer);
        }

        data_pointer += data_copy(&pingNum, data_pointer, 4, &dummy_pointer);
        data_pointer += data_copy(&kalmanNum, data_pointer, 4, &dummy_pointer);

        data_pointer += 32;

        float Altitude; //Compass X
        float Depth;    //Compass Y
        data_pointer += data_copy(&Altitude, data_pointer, 4, &dummy_pointer);
        data_pointer += data_copy(&Depth, data_pointer, 4, &dummy_pointer);

        //        if(recieveDepth){
        //            data_pointer += 4;
        //        }

        sendSys(Altitude, Depth);

        int messageSize = dataSize - pingSize - 12;
        QString message;
        quint8 character;

        for (int i = 0; i < messageSize; i++)
        {

            memcpy(&character, data_pointer, 1);
            if (i == 300)
            {
                if (character < 35)
                {
                    QMessageBox::StandardButton reply;
                    reply = QMessageBox::warning(this, "Warning", "Compass not calibrated");
                }
            }
            message.append(character);
            data_pointer++;
        }

        emit sendMessage(message);
    }
    else if (packetID == 0x100 || packetID == 0x101) {
        quint32 fileSize;
        char str[128];
        int dummyPtr;
        int checkFtr;

        data_pointer += data_copy(str, data_pointer, 128, &dummyPtr);
        data_pointer += data_copy(&fileSize, data_pointer, 4, &dummyPtr);
        if (packetID == 0x100) {
            data_pointer += data_copy(&checkFtr, data_pointer, 4, &dummyPtr);
        }
        qDebug() << dataSize << "Received File Info" << str << "Size" << fileSize << "Check" << checkFtr;

        if (packetID == 0x101) {
            char *fileBuffer[4];
            int index = 0;
            quint32 index2 = fileSize;
            QString fileName = recorder.recPath + "/" + str;
            recorder.sonarFile.setFileName(fileName);
            recorder.sonarFile.open(QIODevice::WriteOnly);

            tcpBuffer.remove(0, 144);
            recorder.buffer.append(tcpBuffer);
            index += tcpBuffer.size();
            index2 -= tcpBuffer.size();
            tcpBuffer.clear();
            while (index2 > 0) {
                tcpSocket->waitForReadyRead();
                tcpBuffer.append(tcpSocket->readAll());
                if (index2 < tcpBuffer.size()) {
                    int tmpLen = recorder.buffer.size();
                    recorder.buffer.append(tcpBuffer);
                    recorder.buffer.remove(tmpLen + index2, tcpBuffer.size() - index2);
                    tcpBuffer.remove(0, index2);
                    index += index2;
                    index2 = 0;
                    break;
                }
                recorder.buffer.append(tcpBuffer);
                index2 -= tcpBuffer.size();
                index += tcpBuffer.size();
                tcpBuffer.clear();
                if (recorder.buffer.size() > 10000000) {
                    recorder.sonarFile.write(recorder.buffer);
                    recorder.buffer.clear();
                }
            }
            tcpBuffer.clear();
            recorder.sonarFile.write(recorder.buffer);
            recorder.buffer.clear();
            recorder.sonarFile.close();
        }
    }
    else if (packetID == 40) {
        // Do not record
        QThreadPool pool;
        QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess, data_pointer, packetID, &dummy_pointer); //Runs processDataToPlot in a thread
        while (!processDataThreadProgress.isFinished()) //During the function's execution
        {
            if (flag.thread_middlePoint == true) //When it reaches this point
            {
                processDataThreadProgress.togglePaused(); //Pause the function
                flag.thread_middlePoint = false;
                break;
            }
        }
        flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue
        processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data
        data_hashMap_update();
    }
    else {
        QThreadPool pool;
        QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess, data_pointer, packetID, &dummy_pointer); //Runs processDataToPlot in a thread
        while (!processDataThreadProgress.isFinished()) //During the function's execution
        {
            if (flag.thread_middlePoint == true) //When it reaches this point
            {
                processDataThreadProgress.togglePaused(); //Pause the function
                flag.thread_middlePoint = false;
                break;
            }
        }

        flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue

        if (packetID) {
            flag.tmpPlotAngle = 1;
        } else {
            flag.tmpPlotAngle = 0;
        }
        if (packetID == 6)   flag.PlotAngleScanColorMap = 0;
        else    flag.PlotAngleScanColorMap = 1;

        if (flag.useDualFrequency) {
            flag.tmpPlotHF = 1;
        } else {
            flag.tmpPlotHF = 0;
        }
        if (ping->Oct31ASTest) {
            flag.tmpPlotHF = 0;
            flag.tmpPlotAngle = 1;
        }
        if (packetID == 100) {
            flag.tmpPlotHF = 1;
            flag.tmpPlotAngle = 0;
            flag.PlotAngleScanColorMap = 0;
        } else if (packetID == 101) {
            flag.tmpPlotHF = 1;
            flag.tmpPlotAngle = 1;
            flag.PlotAngleScanColorMap = 1;
        }
        if ((flag.PlotAngleScan != flag.tmpPlotAngle) || (flag.tmpPlotHF != flag.PlotHfSidescan)) {
            flag.PlotHfSidescan = flag.tmpPlotHF;
            flag.PlotAngleScan = flag.tmpPlotAngle;
            format_preparePlots();
        }

        if(ping->rangeLf_tmp != ping->rangeLf_int) {//if range recieved from the board is not the current GUI range, set GUI range to board range
            if (ping->rangeLf_tmp != 0) {
                setRecordedRange(ping->rangeLf_tmp);
                flag.highRangeChange = true;
            }
            ping->rangeLf_int = ping->rangeLf_tmp; // Only use this after setRecordedRange is called
        }
        if(ping->rangeHf_tmp != ping->rangeHf_int) {//same as above
            if (ping->rangeHf_tmp > 0 && !flag.highRangeChange) {
                setRecordedRange(ping->rangeHf_tmp);
            }
            flag.highRangeChange = false;
            ping->rangeHf_int = ping->rangeHf_tmp; // Only use this after setRecordedRange is called
        }
        flag.highRangeChange = false;

        processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data
        data_hashMap_update();
        //plot trace if the data is sorted into the designated buffers correctly

        plot_data_addTrace();
        plot_data_checkToAddNewMap();
        ChartWindowInstance->plot_configureBoatIcon(flag.PlotAngleScan);
        if (ping->lastTraceAlt < depthAlarm->level && ping->lastTraceAlt > 0.5)      depthAlarm_start();
    //    else                    stopDepthAlarm();
        depthAlarm_updateHandlePosition(depthAlarm->level);
        plot_replotAll();

        if (recorder_checkState()) {
            pingCounter++;//used to keep track of when to write buffer to file
            if(!flag.recordXtfFormat) {
                //                buffer.prepend(headerData);
                QByteArray tmp;
                tmp = tcpBuffer.mid(0,dataSize);
                recorder_saveData(tmp); //record data in native format
                Gnss_saveGpsPoints();
            }
            else {
                xtf_writePing();
            }
        }// end record data
    }

    tcpBuffer.remove(0, dataSize);
    //    buffer = tmp;
    if (tcpBuffer.size() > 0) {
        //        qDebug() << "                               Called ProcessData Directly";
        Tcp_readData();
        //        sendCommand(1,0,180);
    }
}

/**
 * @brief MainWindow::on_connected
 * This slot is called when the application has successfully established a
 * connection to the host for the data transmission.
 */
void MainWindow :: Tcp_onConnected() {

    flag.readyToTransmit = 1;
    Tcp_sendCommand(2,0,0);//sends confirmation command to board, lets board know that GUI detects board

    Tcp_sendCommand(11,0,0);
    Tcp_sendCommand(12,0,0);
    Tcp_sendCommand(13,0,0);

    QIcon connect_icon(":/icons/icons/power_on.png");
    //    connect_icon.pixmap(QSize(100,100));
    connect_icon.addPixmap(QPixmap(":/icons/icons/power_disabled.png"), QIcon::Disabled);
    ui->connect_btn->setIcon(connect_icon);
    ui->connect_btn->setText("Connected");
    //    ui->connect_btn->setIconSize(QSize(20, 20));
    ui->connect_btn->setToolTip("Sonar Connected");
    emit connected(true);

    state.GUI_MODE = 1;
    state.PLAY_MODE = 0;
    state.RT_MODE = 1;
    tcpBuffer.clear();
    on_gain_mult_slider_valueChanged(2);//apply systemmenu gains to board
    on_add_gain_clicked();//apply add gain to board

    statusBar()->show();
    emit formatStatusBar(true);

    if (ui->btn_range1->isChecked())       Tcp_sendCommand(1,0,30);
    else if (ui->btn_range2->isChecked())    Tcp_sendCommand(1,0,60);
    else if (ui->btn_range3->isChecked())    Tcp_sendCommand(1,0,120);
    else if (ui->btn_range4->isChecked())    Tcp_sendCommand(1,0,240);
    setup_rangeButtonActivate(true);

    // Begin Pinging
    Tcp_sendCommand(0,1,0);
    resetPlotLists();
    state.RT_MODE = 2;
    replotTimer.start(10);//start replot timer
    setup_buttonsAndTools();

    Gnss_connectUsbNmea();
}

void MainWindow :: Tcp_receiveDisconnection() {

    QIcon disconnect_icon(":/icons/icons/power_off.png");
    disconnect_icon.addPixmap(QPixmap(":/icons/icons/power_disabled.png"), QIcon::Disabled);
    ui->connect_btn->setIcon(disconnect_icon);

    ui->connect_btn->setText("Disconnected");
    ui->connect_btn->setToolTip("Connect to Sonar");
    // ui->connect_btn->setStyleSheet("color: white");
    Tcp_connectError(2); // when conncection is disconnected the msg is displayed
    state.RT_MODE = 0;

    if(state.RT_MODE == 2) {//if plotting
        if (state.GUI_MODE == 2 || state.GUI_MODE == 0) {
            //            simulationTimer.stop();
        }

        else {
            quint8 cmdType = 0;
            quint32 cmd_data = 0;
            Tcp_sendCommand(cmdType,cmd_data,0); //send
            resetPlotLists();
        }
        replotTimer.stop();
        state.RT_MODE = 0;
        //when stop running, we first close two GPX files, and then reopen them and merge them
        trkptGPX.close();
    }

    state.RT_MODE = 0;
    state.GUI_MODE = 1;

    setup_buttonsAndTools();
}
/**
 * @brief MainWindow::on_connect_btn_clicked
 * Function to establish connection to the server(Board). It shows an error,
 * if the application is unable to establish a connection.
 */
void MainWindow :: on_connect_btn_clicked() {
    // int UsbMode = 1; //This will be used for adding the switch on GUI - if the user wants USB reading or not //It is currently hardcoded
    if(flag.ethernetActive == false) {
        qDebug() << "T - USB Active";
        libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
        libusb_context *ctx = NULL; //a libusb session
        int r; //for return values
        ssize_t cnt; //holding number of devices in list
        r = libusb_init(&ctx); //initialize a library session
        if(r < 0) {
            qDebug()<<"Init Error "<<r; //there was an error
        }
        libusb_set_debug(ctx, 3); //set verbosity level to 3, as suggested in the documentation
        cnt = libusb_get_device_list(ctx, &devs); //get the list of devices

        //        libusb_device_handle *dev_handle; //a device handle
        dev_handle = libusb_open_device_with_vid_pid(ctx, 1021, 257); //Looks for FPGA with Vendor ID 1021 and Product ID 257
        if(dev_handle == NULL) {
            qDebug() <<"Cannot open device"<<endl;
            return;
        } else {
            qDebug()<<"Device Opened"<<endl;
        }

        unsigned char *data = new unsigned char[10]; //data to write
        data[0]=1;data[1]=15;data[2]=30;data[3]=250; //some dummy values
        int actual; //used to find out how many bytes were written
        if(libusb_kernel_driver_active(dev_handle, 0) == 1) { //find out if kernel driver is attached
            qDebug()<<"Kernel Driver Active";
            if(libusb_detach_kernel_driver(dev_handle, 0) == 0) //detach it
                qDebug()<<"Kernel Driver Detached!";
        }
        r = libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)
        if(r < 0) {
            qDebug()<<"Cannot Claim Interface";
        }

        //        qDebug()<<"Data->"<<data[0]<<data[1]<<data[2]<<data[3] <<"<-"; //just to see the data we want to write : abcd
        //        qDebug()<<"Writing Data...";
        //        r = libusb_bulk_transfer(dev_handle, (3 | LIBUSB_ENDPOINT_OUT), data, 4, &actual, 0); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129
        //        if(r == 0 && actual == 4) //we wrote the 4 bytes successfully
        //            qDebug()<<"Writing Successful!";
        //        else
        //            qDebug()<<"Write Error";

        //        unsigned char *data2 = new unsigned char[10];
        //    //    memset(my_string, '\0', 64);
        //    //    data2 = (unsigned char *)malloc(10);
        //        r = libusb_bulk_transfer(dev_handle, (2 | LIBUSB_ENDPOINT_IN), data2, 256, &actual, 10); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129
        //        qDebug() << "Incoming:  " << data2[0]<< data2[1] << data2[2] << "Actual: " << actual;

        //    r = libusb_bulk_transfer(dev_handle, (2 | 128), data2, 9, &actual, 0); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129
        //    qDebug() << "Incoming:  " << data << "Actual: " << actual;
        if(dev_handle == NULL) {
            //            qDebug() <<"Cannot open device"<<endl;
            //            return;
        } else {
            //            qDebug()<<"Device Opened"<<endl;
            Tcp_onConnected();
            UsbTimer.start(100);
        }
    } else {
        if(tcpSocket->state() != QTcpSocket::ConnectedState) {
            //ui->connect_btn->setStyleSheet("background-color:rgb(255, 255, 255)");
            Tcp_connect();
        }
        if (ui->connect_btn->text().compare("connected")==0) {
            setup_buttonsAndTools();
        }
    }
}

void MainWindow :: Tcp_connect(){
    if (PLAYER_DEMO)    return;

    tcpSocket->connectToHost("192.168.1.10", 7); //Host_address is "192.168.1.10" //port_num is 7
    tcpSocket->setReadBufferSize(200);//limits the buffer size to save memory
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption, 1);
    tcpSocket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    tcpSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 100);
    tcpSocket->setSocketOption(QAbstractSocket::SendBufferSizeSocketOption, 100);
    connect(tcpSocket, SIGNAL(connected()),this, SLOT(Tcp_onConnected())); //if connection is successful, trigger on_connected

    if (!(tcpSocket->waitForConnected(1000))) {
        if(ui->connect_btn->text().compare("Disconnected")==0) {
            Tcp_connectError(2);
        } else {
            Tcp_connectError(1);
        }
    }
}

/**
 * @brief MainWindow::sendCommand
 * @param cmdType - Specific command you want to send
 * @param on_off - The Command data as seen in the TCP Dissection section of the Sonar Data Formatting Report
 * @param range - Only used in command 1, used to send range to board.  Set to 0 if not using command 1
 * Creates the custom command data packet using the parameters, according to the protocol
 * and sends it to the board via TCP.
 */
void MainWindow :: Tcp_sendCommand(quint8 cmdType, quint32 on_off, quint32 range) {
    qDebug() << "Type: " << cmdType << "On: " << on_off << "Range: " << range;
    QByteArray sendArr;
    quint16 syncWord1 = 0x8000;
    quint16 syncWord2 = 0xFFFF;
    quint32 packetID = 0;
    quint32 packetSize = 15;
    quint16 endSync = 0xFACE;

    if(cmdType == 2) {
        packetSize = 19;
    }
    else if (cmdType == 31) {
        packetSize = 139;
    }

    //Packet Header
    sendArr.append((const char*)&syncWord1, sizeof(syncWord1));
    sendArr.append((const char*)&syncWord2, sizeof(syncWord2));
    sendArr.append((const char*)&packetID, sizeof(packetID));
    sendArr.append((const char*)&packetSize, sizeof(packetSize));
    sendArr.append((const char*)&cmdType, sizeof(cmdType));
    if(cmdType == 0 || cmdType == 3) {
        sendArr.append((const char*)&on_off, sizeof(on_off));
    }
    else if(cmdType == 1) {
        sendArr.append((const char*)&range, sizeof(range));
    }
    else if(cmdType == 6) {

    }
    else if(cmdType == 2) { //Sends date to board
        QDate date = QDate::currentDate();
        quint16 year= date.year();
        quint8 day = date.day();
        quint8 month = date.month();
        quint8 yr1;
        yr1 = year % 100;

        QTime time = QTime:: currentTime();
        quint8 hour = time.hour();
        quint8 minutes = time.minute();
        quint8 seconds = time.second();
        quint8 val = 0x00;
        sendArr.append(val);
        sendArr.append(val);
        sendArr.append(seconds);
        sendArr.append(minutes);
        sendArr.append(hour);
        sendArr.append(day);
        sendArr.append(month);
        sendArr.append(yr1);
    }
    else if (cmdType == 3) {
        quint32 freq;
        switch (on_off) {
        case 0: //transmitter off
            freq = 0;
            break;
        case 1://LF only
            freq = 1;
            break;
        case 2: //HF only
            freq = 2;
            break;
        case 3: //both frequencies on
            freq = 3;
            break;
        default:
            break;
        }
        sendArr.append((const char*)&freq, sizeof(freq));
    }
    else if(cmdType == 7) { //adds or subtracts 12dB from the gain
        quint32 addGain;
        if (on_off == 0) {//subtracts
            addGain = 0;
        }
        else if (on_off == 1) {//adds
            addGain = 1;
        }
        sendArr.append((const char*)&addGain, sizeof(addGain));
    }
    else if (cmdType == 8) { //sends gain multiplier value to board
        quint32 gainmult;
        gainmult = on_off;
        sendArr.append((const char*)&gainmult, sizeof(gainmult));
    }
    else if (cmdType == 9) { //sends attenuation value to the board
        quint32 atten;
        atten = on_off;
        sendArr.append((const char*)&atten, sizeof(atten));
    }
    else if (cmdType == 10) {
        quint32 enable;
        switch (on_off) {
        case 0:
            enable = 0;
            break;
        case 1:
            enable = 1;
            break;
        }
        sendArr.append((const char*)&enable, sizeof(enable));
    }
    else if (cmdType == 11) {
        quint16 num1 = (quint16) on_off;
        quint16 num2 = (quint16)range;
        if(on_off < 3){
            num1 = 0;
            num2 = on_off;
        }
        if(on_off >= 3 && on_off < 6){
            num1 = 1;
            num2 = on_off % 3;
        }
        if(on_off >= 6 && on_off < 9){
            num1 = 2;
            num2 = on_off % 3;
        }
        if(on_off >= 9 && on_off < 12){
            num1 = 3;
            num2 = on_off % 3;
        }
        if(on_off >= 12 && on_off < 15){
            num1 = 4;
            num2 = on_off % 3;
        }

        sendArr.append((const char*)&num1, sizeof(num1));
        sendArr.append((const char*)&num2, sizeof(num2));
    }
    else if(cmdType == 12) {
        quint16 num1 = (quint16)on_off;
        quint16 num2 = (quint16)range;

        if(on_off <= 30){
            num1 = on_off = 0;
            num2 = on_off % 31;
        }
        if(on_off > 30 && on_off <= 61){
            num1 = on_off = 1;
            num2 = on_off % 31;
        }
        if(on_off > 61 && on_off <= 92){
            num1 = on_off = 2;
            num2 = on_off % 31;
        }
        if(on_off > 92 && on_off <= 123){
            num1 = on_off = 3;
            num2 = on_off % 31;
        }
        if(on_off > 123 && on_off <= 154){
            num1 = on_off = 4;
            num2 = on_off % 31;
        }

        sendArr.append((const char*)&num1, sizeof(num1));
        sendArr.append((const char*)&num2, sizeof(num2));
    }
    else if (cmdType == 13) {
        quint32 dB;
        switch(on_off){
        case 0:
            dB = 0;
            break;
        case 1:
            dB = 1;
            break;
        }
        sendArr.append((const char*)&dB, sizeof(dB));
    }
    else if (cmdType == 20) { //Compass calibration
        quint32 dataC = (quint32) range;
        sendArr.append((const char*)&dataC, sizeof(dataC));
    }
    else if (cmdType == 21) { //Compass calibration
        sendArr.append((const char*)&range, sizeof(range));
    }
    else if (cmdType == 31) { //Compass calibration
        char str[128] = "Angle_Scan_Fri_10.18.19_11-06-21.JAS";
        sendArr.append((const char*)&str, 128);
    }
    //trailer
    sendArr.append((const char*)&endSync, sizeof(endSync));

    if(tcpSocket->isWritable()) { //If TCP
        tcpSocket->write(sendArr);
    }
    /*else if (dev_handle != NULL) { //Else if USB
        //qDebug() << "Sending USB";
        unsigned char *usbArr = new unsigned char[30]; //data to write
        quint8 ArrPtr = 0;  // keeps track of where we are in usbArr
        memcpy(&usbArr[0], sendArr.constData(), packetSize+4);

        int actual = 0;
        int r=0;
        //qDebug() << "Before bulk";
        r = libusb_bulk_transfer(dev_handle, (3 | LIBUSB_ENDPOINT_OUT), usbArr, packetSize+4, &actual, 0);

        //qDebug() << "After bulk";
        delete [] usbArr;

        if(r == 0) //we wrote the 4 bytes successfully
            qDebug()<<"Writing Successful!" << actual << sizeof(sendArr);
        else
            qDebug()<<"Write Error";
    } */else {
        qDebug() << "command not sent";
    }
}

void MainWindow :: Tcp_connectError( int errorMsg ) {

    QString warningList = "";
    if (errorMsg == 2) {
        warningList = "Sonar hardware disconnected";
    }
    else if(errorMsg == 1) {
        warningList = "Sonar hardware not found";
    }
    QMessageBox::critical(this, "TCP Error", warningList);
    ui->connect_btn->setToolTip("Connect to Sonar");
}

/** ********************Features ******************** **/

/****************Recording Feature****************/

/**
 * @brief MainWindow::recordLiveData
 * @param traceData
 * Appends the current boatspeed index and incoming traces
 * to a Byte array. After, recording is complete this byte
 * array is then written to a file in binary format.
 */
void MainWindow :: recorder_saveData(QByteArray traceData) {
    recorder.buffer.append(traceData);//append the processdata buffer to recordedDataBuffer

    if(recorder.buffer.length() >= 100000){//if 200 pings have passed
        recorder.sonarFile.write(recorder.buffer);//write rec buffer to file
        recorder.buffer.clear();//clear buffer to save memory
        pingCounter = 0;//reset ping counter
    }
}

/**
 * @brief MainWindow::recordingSwitch
 * @param activate
 * To activate/deactivate the recording mode.
 * This slot is called when  signal is emitted from
 * the data recorder window to set the recording mode.
 */
bool MainWindow :: recorder_startStop(bool activate) {
    if (!((state.GUI_MODE == 0 && state.SIM_MODE > 0) ||
            (state.GUI_MODE == 1 && state.RT_MODE > 1)))    return 0;

    if(activate == true) {
        // First remove all previous targets
        tools->removeAllTargetsFromList();

        QString filter = "Jetasonic Scan Data (*.JAS)\neXtended Triton Format (*.xtf)";
        QDateTime time = QDateTime::currentDateTime();
        QString sonarFileType = "";
        //if (PlotAngleScan) {
        sonarFileType = "/";//Angle_Scan_";
        //} else {
        //    sonarFileType = "/Side_Scan_";
        //}g
        // Open the File Dialog Window to Select File Name
        QString fileName = recorder.recPath + sonarFileType + time.toString("ddMMyy_hhmm") + ".JAS";
        fileName  = QFileDialog::getSaveFileName(this, tr("Save Recorded Sonar File"),
                                                 recorder.recPath + sonarFileType + time.toString("ddMMyy_hhmm"), filter, &filter);
        if (fileName == "")     return 0;  // User clicked cancel
        player.openedFileName = fileName;
        recorder.fileName = fileName;
        //recFile records data
        recorder.sonarFile.setFileName(fileName);
        recorder.sonarFile.open(QIODevice::WriteOnly);
        if (QFileInfo(fileName).suffix().toLower() == "xtf") {
            // Record the XTF file
            xtf_writeFileHeader();
        } else {
            flag.recordXtfFormat = 0;
        }

        //GPSFile records GPS position
        recorder.GPSFile.setFileName(fileName+"LOG");
        recorder.GPSFile.open(QIODevice::ReadWrite | QIODevice::Text);

        //flagFile records flag information
//        recorder.targetFile.setFileName(fileName+"FLAG");
//        recorder.targetFile.open(QIODevice::ReadWrite | QIODevice::Text);

        ping->pingNumFirst = ping->pingNumCurrent;
        if (state.GUI_MODE == 0 && state.SIM_MODE == 1)     state.SIM_MODE = 2;
        else if (state.GUI_MODE == 1 && state.RT_MODE == 2)     state.RT_MODE = 3;
        elTime.start();
        timer->start(500);
    }
    else if(activate == false) {
        if (state.GUI_MODE == 0 && state.SIM_MODE == 2)     state.SIM_MODE = 1;
        else if (state.GUI_MODE == 1 && state.RT_MODE == 3)     state.RT_MODE = 2;
        timer->stop();

        if(!flag.recordXtfFormat) {
            qint16 footer = 0xFAAF;
            recorder.buffer.append((const char*)&footer, sizeof(footer));//appends footer to end of file
            pingCounter = 0;
        }
        //record in xtf
        else {
            recorder.xtfFileData.clear();
        }
        recorder.sonarFile.write(recorder.buffer);
        recorder.sonarFile.close();
        recorder.buffer.clear();
//        recorder.targetFile.close();
        recorder.GPSFile.close();
        recorder.fileName = "";
        pingCounter = 0;
    }
    setup_buttonsAndTools();
    return 1;
}

bool MainWindow :: recorder_checkState() {
    // The purpose of this function is to reduce the number of times the state variable
    // is used so it is easier to search for it
    return ((state.GUI_MODE == 0 && state.SIM_MODE == 2) ||
            (state.GUI_MODE == 1 && state.RT_MODE == 3));
}

/**
 * @brief MainWindow::setButtonActivation
 * @param activate
 * To enable and disable the widget activation on the main window.
 * activate = true; when the user in the recording mode.
 * activate = false; when the user the playback mode.
 */
void MainWindow :: setup_rangeButtonActivate(bool activate) {
    ui->btn_range1->setEnabled(activate);
    ui->btn_range2->setEnabled(activate);
    ui->btn_range3->setEnabled(activate);
    ui->btn_range4->setEnabled(activate);
    ui->auto_btn->setEnabled(activate);
}

//function to enable quick record option on the main window//
void MainWindow :: on_quick_recording_clicked()
{
    if(!recorder_checkState() && ui->quick_recording->isChecked()==true){
        if (!recorder_startStop(true)) {
            return;
        }
        elTime.start();
        timer->start(500);

        plot_updateLfHfLegend(" R ", true);
    }

    //commented out code used for quick play and stop button//
    /*else if( player_flag ==0 ){

        if(ui->quick_replay->isChecked()==true){
           resetPlayback();}
        else{
            selectDataFile();
            if(player_flag!=2){playRecordedData(true);
                ui->quick_stop->setVisible(true);
                ui->quick_replay->setVisible(true);}
            recordData=0;
        }
   }*/

    else{
        timer->stop();
        recorder_startStop(false);
        plot_updateLfHfLegend("", true);
    }
    setup_buttonsAndTools();
}

void MainWindow :: on_quick_stop_btn_clicked(){
    if (state.GUI_MODE == 2) {
        on_actionClose_triggered();
        player.fileBuffer = nullptr;
        state.GUI_MODE = 1;
        state.PLAY_MODE =0;
    }
    else if (state.GUI_MODE == 0) {
        simulationTimer.stop();
        state.GUI_MODE = 1;
        systemSettingInstance->setSimulation(0);
    }
    else if (state.GUI_MODE == 1 && state.RT_MODE > 0) {
        Tcp_sendCommand(0,0,0); // pause sonar
        resetPlotLists();
        replotTimer.stop();

        //changing state for quick record button
        timer->stop();
        recorder_startStop(false);

        if (state.RT_MODE >= 2) {
            state.RT_MODE = 1;
            QMessageBox::StandardButton checkMsg = QMessageBox::question(this, "Disconnect Sonar?",
                                                                         "Are you sure you want to disconnect sonar?");
            setup_buttonsAndTools();
            if (checkMsg != QMessageBox::Yes)    return;
        }
        tcpSocket->disconnectFromHost();
        state.RT_MODE = 0;
    }
    resetGui();
    if(ui->quick_replay_btn->isChecked()){
        ui->quick_replay_btn->setChecked(false);
    }
    plot_updateLfHfLegend("", false);
    setup_buttonsAndTools();
}

void MainWindow :: on_quick_playpause_clicked(){
    if (ui->quick_playpause->isChecked()){
        if (state.GUI_MODE == 1 && state.RT_MODE == 1) {
            // Real Time Mode and connected to board
            quint8 cmdType = 0;
            quint32 cmd_data = 1;
            Tcp_sendCommand(cmdType,cmd_data,0);
            resetPlotLists();
            state.RT_MODE = 2;

            replotTimer.start(10);//start replot timer
        }
        else if (state.GUI_MODE == 0) {
            // Simulation Mode
            state.SIM_MODE = 1;
            simulationTimer.start(300);//simulationTimer calls plottrace on timeout
            statusBar()->show();
            flag.PlotAngleScan=0;
            flag.PlotHfSidescan=0;
            ping->degree=90;
            flag.useDualFrequency=0;

            ui->quick_playpause->setChecked(true);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));
            plot_updateLfHfLegend("", false);
            replotTimer.start(10);//start replot timer
        }
        else if (state.PLAY_MODE == 0) {
            // Open File at anytime
            on_actionOpen_triggered();
        }
        else if (state.GUI_MODE == 2) {
            // Actions related to file playing
            if (state.PLAY_MODE == 6) {
                state.PLAY_MODE = 3;
                player_selectFile();
                player_addColorMap();
                player_playData(true);
            }
            else if ((state.PLAY_MODE == 1 || state.PLAY_MODE == 0)){
                on_actionOpen_triggered();
            }
            else if (state.PLAY_MODE == 4) {
                player_playData(true);
                ui->customPlot->setFocusPolicy(Qt::StrongFocus);
            }
            else if (state.PLAY_MODE == 5) {
                player_addColorMap();
                ui->customPlot->setFocusPolicy(Qt::StrongFocus);
            }
        }
        else {
            // Something went wrong, and its time to reset
            on_quick_stop_btn_clicked();
            state.GUI_MODE = 1;
            state.PLAY_MODE = 0;
            state.RT_MODE = 0;
            state.SIM_MODE = 0;
        }
    }
    else{
        if (state.GUI_MODE == 2 && state.PLAY_MODE == 3){
            // Pause Playback
            player_playData(false);
        }
        else if (state.GUI_MODE == 1 && state.RT_MODE >= 2) {
            quint8 cmdType = 0;
            quint32 cmd_data = 0;
            Tcp_sendCommand(cmdType,cmd_data,0); //send
            resetPlotLists();
            replotTimer.stop();
            state.RT_MODE = 1;

            //changing state for quick record button
            timer->stop();
            recorder_startStop(false);
        }
        else if (state.GUI_MODE == 0) {
            state.SIM_MODE = 0;
            simulationTimer.stop();
            replotTimer.stop();

            ui->quick_playpause->setChecked(false);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/play.png"));
            plot_updateLfHfLegend(" ll ", false);
        }
        else {
            // Something went wrong, and its time to reset
            on_quick_stop_btn_clicked();
            state.GUI_MODE = 1;
            state.PLAY_MODE = 0;
            state.RT_MODE = 0;
            state.SIM_MODE = 0;
        }
    }
    setup_buttonsAndTools();
}

void MainWindow :: plot_updateLfHfLegend(QString text, bool replotRequired) {
    // Always put the text on LF plot, but only show on HF if only the HF plot is shown
    double xPos = Format->widthLfHf;
    lfLegendIcon->topLeft->setCoords(xPos - 70,30);
    lfLegendIcon->bottomRight->setCoords(xPos - 30,70);
    if (!customPlotHF->hasItem(hfLegendIcon)) {
        hfLegendIcon = new QCPItemPixmap(customPlotHF);
        hfLegendIcon->setParent(customPlotHF);
        hfLegendIcon->setSelectable(false);
        hfLegendIcon->setVisible(false);
        hfLegendIcon->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
        hfLegendIcon->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
        hfLegendIcon->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
        hfLegendIcon->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
        hfLegendIcon->setScaled(true);
        hfLegendIcon->setLayer("fixedIcons");
    }
    hfLegendIcon->topLeft->setCoords(xPos - 70,30);
    hfLegendIcon->bottomRight->setCoords(xPos - 30,70);
    tools->updatePanelPosition(xPos);
    legendTxt = text;
    tools->updateIconPanel(!Format->isOpenLf, flag.targetToolActive, flag.measuringTapeActive, flag.shadowToolActive, cropTool->toolActive);

    if (text == "") {
        QPixmap clear(20,20);
        lfLegendIcon->setPixmap(clear);
        hfLegendIcon->setPixmap(clear);
        lfLegendIcon->setVisible(false);
        hfLegendIcon->setVisible(false);
    } else {
        if (text == " ll ") {
            QPixmap pause(":/icons/icons/pause_plotIcon.png");
            lfLegendIcon->setPixmap(pause);
            lfLegendIcon->setVisible(true);
            if (!Format->isOpenLf) {
                hfLegendIcon->setPixmap(pause);
                hfLegendIcon->setVisible(true);
            }
        } else if (text == " R ") {
            QPixmap pause(":/icons/icons/record_plotIcon.png");
            lfLegendIcon->setPixmap(pause);
            lfLegendIcon->setVisible(true);
            if (!Format->isOpenLf) {
                hfLegendIcon->setPixmap(pause);
                hfLegendIcon->setVisible(true);
            }
        }
        if (Format->isOpenLf) {
            hfLegendIcon->setVisible(false);
        }
    }

    if (replotRequired) {
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: plot_updateLfHfZoomLegendPosition() {
    double xPos = Format->widthLfHf;
    double yPosLf = Format->heightLf;
    double yPosHf = Format->heightHf;

    customPlotLfLegend->position->setCoords(customPlotLF->axisRect()->margins().left(), yPosLf);
    customPlotHfLegend->position->setCoords(customPlotHF->axisRect()->margins().left(), yPosHf);
}

void MainWindow :: plot_showUpdatedZoomLegends() {
    int height = (Format->plotHeight_Lf);
    int width = (Format->plotWidth_LfHf);
    double updatedView = ((height * (ping->rangeLf_double * 2)) / width);

    double ratio=sqrt((updatedView/(customPlotLF->yAxis->range().upper - customPlotLF->yAxis->range().lower))
               *(2*ping->rangeLf_double)/(customPlotLF->xAxis->range().upper - customPlotLF->xAxis->range().lower));

    QString test = " " + QString::number(ratio*100, 'f', 0) + " % ";
    if (setting.zoomMagnitude_lf < 0.01 && flag.zoomLfFixed) {
        test = " A ";
    } else if (flag.zoomLfFixed) {
        test = " " + QString::number(setting.zoomMagnitude_lf*100, 'f', 0) + " % ";
    }
    customPlotLfLegend->setText(test);

    double ratio1=0;
    height = (Format->plotHeight_Hf);
    width = (Format->plotWidth_LfHf);
    updatedView = ((height * (ping->rangeHf_double * 2)) / width);

    ratio1=sqrt((updatedView/(customPlotHF->yAxis->range().upper - customPlotHF->yAxis->range().lower))
                *(2*ping->rangeHf_double)/(customPlotHF->xAxis->range().upper - customPlotHF->xAxis->range().lower));
    QString test2 = " " + QString::number(ratio1*100, 'f', 0) + " % ";
    if (setting.zoomMagnitude_hf < 0.01 && flag.zoomHfFixed) {
        test2 = " A ";
    } else if (flag.zoomHfFixed) {
        test2 = " " + QString::number(setting.zoomMagnitude_hf*100, 'f', 0) + " % ";
    }

    customPlotHfLegend->setText(test2);
}

void MainWindow :: setup_buttonsAndTools () {
    if (PLAYER_DEMO == 1)    state.GUI_MODE = 2;
    if (state.GUI_MODE == 0) {
        if (state.SIM_MODE == 0) {
            ui->quick_recording->setEnabled(false);
            ui->quick_recording->setChecked(false);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/recording.png")));
            ui->actionSave->setEnabled(false);
        } else if (state.SIM_MODE == 1) {
            ui->quick_recording->setEnabled(true);
            ui->quick_recording->setChecked(false);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/recording.png")));
            ui->actionSave->setEnabled(true);
            tools->enabled = true;
        } else if (state.SIM_MODE == 2) {
            ui->quick_recording->setEnabled(true);
            ui->quick_recording->setChecked(true);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/stop.png")));
            ui->actionSave->setEnabled(true);
            tools->enabled = true;
        }
        ui->connect_btn->setEnabled(false);
        ui->quick_stop_btn->setEnabled(true);
        ui->quick_replay_btn->setEnabled(false);
        ui->actionSaveAs->setEnabled(false);
        ui->actionAbout_3->setEnabled(false);
        ui->actionClose->setEnabled(false);
        emit enableTvgTab(false);
        tools->enabled = false;
    }
    else if (state.GUI_MODE == 1) {
        if (state.RT_MODE == 0) {
            ui->quick_recording->setEnabled(false);
            ui->quick_recording->setChecked(false);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/recording.png")));
            ui->quick_playpause->setChecked(false);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/play.png"));

            ui->quick_stop_btn->setEnabled(false);
            ui->quick_replay_btn->setEnabled(false);
            ui->actionSave->setEnabled(false); //save action is only enabled when running//
            ui->actionAbout_3->setEnabled(false);

            plot_updateLfHfLegend("", true);
            emit enableTvgTab(false);
            tools->enabled = false;
        }
        else if (state.RT_MODE == 1) {
            ui->quick_recording->setEnabled(true);
            ui->quick_recording->setChecked(false);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/recording.png")));
            ui->quick_playpause->setChecked(false);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/play.png"));

            ui->quick_stop_btn->setEnabled(true);
            ui->quick_replay_btn->setEnabled(false);
            ui->actionSave->setEnabled(true); //save action is only enabled when running//
            ui->actionAbout_3->setEnabled(true);

            plot_updateLfHfLegend(" ll ", true);
            emit enableTvgTab(true);
            tools->enabled = true;
        }
        else if (state.RT_MODE == 2) {
            ui->quick_recording->setEnabled(true);
            ui->actionSave->setEnabled(true);
            ui->quick_playpause->setChecked(true);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));

            ui->quick_stop_btn->setEnabled(true);
            ui->quick_replay_btn->setEnabled(false);
            ui->actionAbout_3->setEnabled(true);

            plot_updateLfHfLegend("", false);
            ui->quick_recording->setChecked(false);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/recording.png")));

            customPlotLF->replot(QCustomPlot::rpQueuedReplot);
            customPlotHF->replot(QCustomPlot::rpQueuedReplot);
            emit enableTvgTab(true);
            tools->enabled = true;
        }
        else if (state.RT_MODE == 3) {
            ui->quick_recording->setEnabled(true);
            ui->actionSave->setEnabled(true);
            ui->quick_playpause->setChecked(true);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));

            ui->quick_stop_btn->setEnabled(true);
            ui->quick_replay_btn->setEnabled(false);
            ui->actionAbout_3->setEnabled(true);

            plot_updateLfHfLegend(" R ", false);
            ui->quick_recording->setChecked(true);
            ui->quick_recording->setIcon((QIcon(":/icons/icons/stop.png")));

            customPlotLF->replot(QCustomPlot::rpQueuedReplot);
            customPlotHF->replot(QCustomPlot::rpQueuedReplot);
            emit enableTvgTab(true);
            tools->enabled = true;
        }
        ui->actionSaveAs->setEnabled(false);
        ui->connect_btn->setEnabled(true);
        ui->actionClose->setEnabled(false);
        tools->enabled = false;
    }
    else if (state.GUI_MODE == 2) {
        if (state.PLAY_MODE < 3) {
            ui->quick_playpause->setChecked(false);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/play.png"));

            ui->quick_stop_btn->setEnabled(true);

            plot_updateLfHfLegend("", false);
            ui->actionAbout_3->setEnabled(false);
            ui->actionClose->setEnabled(false);
        }
        else if (state.PLAY_MODE == 4 || state.PLAY_MODE == 5) {
            ui->quick_playpause->setChecked(false);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/play.png"));
            ui->quick_stop_btn->setEnabled(true);
            ui->quick_replay_btn->setEnabled(false);

            plot_updateLfHfLegend(" ll ", true);
            ui->actionAbout_3->setEnabled(true);
            ui->actionClose->setEnabled(true);
        }
        else if (state.PLAY_MODE == 3) {
            ui->quick_playpause->setChecked(true);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));
            ui->quick_stop_btn->setEnabled(true);
            ui->quick_replay_btn->setEnabled(false);

            plot_updateLfHfLegend("", false);
            ui->actionAbout_3->setEnabled(true);
            ui->actionClose->setEnabled(true);
        }
        else if (state.PLAY_MODE == 6) {
            ui->quick_playpause->setChecked(false);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/play.png"));
            ui->quick_stop_btn->setEnabled(true);

            plot_updateLfHfLegend(" ll ", true);

            ui->actionAbout_3->setEnabled(true);
            ui->actionClose->setEnabled(true);
        }

        ui->connect_btn->setEnabled(false);
        ui->quick_recording->setEnabled(false);
        ui->quick_recording->setChecked(false);
        ui->quick_recording->setIcon((QIcon(":/icons/icons/recording.png")));
        ui->quick_replay_btn->setEnabled(true);
        ui->actionSaveAs->setEnabled(true); // Save As is only available on playback
        ui->actionSave->setEnabled(false); //save action is only enabled when running//
        emit enableTvgTab(true);
        tools->enabled = true;
    }

    ui->quick_playpause->setEnabled(true);

    ui->customPlot->setFocusPolicy(Qt::StrongFocus);

    if (state.GUI_MODE != 2) {
        file_Name->setVisible(false);
        progressBar->setVisible(false);
        fileSize_lbl->setVisible(false);
    } else {
        file_Name->setVisible(true);
        progressBar->setVisible(true);
        fileSize_lbl->setVisible(true);
    }

    if (/*(state.GNSS_MODE > 1 || gnssController->validCoords) && */state.GUI_MODE != 2)
        ui->actionWaypoint->setEnabled(true);
    else {
        ui->actionWaypoint->setEnabled(false);
        if (flag.waypointToolActive)    on_actionWaypoint_triggered();
    }

    ui->actionCrop_File->setEnabled(tools->enabled);
    ui->actionMeasuring_Tape->setEnabled(tools->enabled);
    ui->actionShadow->setEnabled(tools->enabled);
    ui->actionFlag->setEnabled(tools->enabled);

    if (!tools->enabled) {
        if (flag.measuringTapeActive)   on_actionMeasuring_Tape_triggered();
        if (flag.targetToolActive)      on_actionFlag_triggered();
        if (flag.shadowToolActive)      on_actionShadow_triggered();
        if (cropTool->toolActive)       on_actionCrop_File_triggered();
    }
    tools->setEnabled(tools->enabled);
}

/**
 * @brief MainWindow::setFileSize
 * @param size
 * When file is selected, sets the filesize label to the size of the file in megabytes
 */
void MainWindow :: status_setFileSize(double size) {
    player.fileSize = size;
    double fileSizeAsMB = player.fileSize/1000000;
    fileSize_lbl->setText(QString::number(fileSizeAsMB, 'f', 2) +" MB");

    //emit converterFileSize(size);
    progressBar->setRange(0, size - 4);
}

void MainWindow :: status_showFileName(QString name){
    displayName =name.left(name.lastIndexOf(".")).split('/').last();
    QFont font = avg_label->font();
    font.setPointSize(11);
    QFontMetrics fm(font);
    qDebug() << "File width: " << file_Name->width() << file_Name->x()-progressBar->x()/*ui->degree_lbl->x()*/ << fm.horizontalAdvance(displayName) << displayName;

    if (fm.horizontalAdvance(displayName) + 30 > /*ui->degree_lbl->x()*/progressBar->x() - file_Name->x())
        file_Name->setText("..."+displayName.mid(displayName.size()-16,displayName.size()));
    else
        file_Name->setText(displayName);
    //ui->fileName->setText(file_Name->text());
    if (name.mid(84) != "") //If the user does not choose a file and instead clicks exit, this prevents the button from being enabled
        //May not work if file is already in play
        // ui->playpause->setEnabled(true);
        if (name.length()> 0) {
            //        rMainWindow->PLAY_MODE = 4;
            //} else {
            //        rMainWindow->PLAY_MODE = 1;
        }
}

/**
 * @brief dataRecorder::setStatusBarValue
 * @param val
 * Updates the current value of the progress bar to the percentage of bytes read from max file size
 */
void MainWindow :: status_setProgressBarValue(double val) {
    if (val >=progressBar->maximum()) //If the val exceeds the progress bar max, it will stay 100%
    {
        progressBar->setValue(progressBar->maximum());
        //        ui->quick_playpause->setEnabled(false);
    }
    else
        progressBar->setValue(val);    //Otherwise, set it to the current value as percentage
    /*if (cropTool->toolActive)   */cropTool->updatePlayerPosition(val/(progressBar->maximum()*1.0));
}

/**
 * @brief MainWindow::playRecordedData
 * @param play
 * Timer setup to plot the recorded from the selected file.
 */
void MainWindow :: player_playData(bool play) {
    if (((flag.measuringTapeActive && tools->activeMeasuringTapeObject.start_measuring) || (flag.targetToolActive && tools->targetState)) && play) {
        // Do not play if measuring tape or snipping tool are active
        setup_buttonsAndTools();
        return;
    }

    if(play) {
        //to set the timer according to the rep frequency of the current range
        readFileTimer.start(1);
        state.GUI_MODE = 2;
        state.PLAY_MODE = 3;
        // ui->customPlot->setFocusPolicy(Qt::StrongFocus);
    } else {
        readFileTimer.stop();
        replotTimer.stop();
        state.PLAY_MODE = 4;
        depthAlarm->audio->stop();
    }

    setup_buttonsAndTools();
}

/**
 * @brief MainWindow::recAddCMapLF
 * triggers a prompt on the recorder for the user to "press ↓ to continue"
 * This is only used during playback
 * Its purpose is to give the user some time to look at the plotted colormaps before adding a new one and deleting an older one
 */
void MainWindow :: player_nextColorMap() {
    readFileTimer.stop();
    player_addColorMap();

    setup_buttonsAndTools();
}

/**
 * @brief MainWindow::recCompleteCMapLF
 * This function is triggered when the user decides to continue after the prompt
 */
void MainWindow :: player_addColorMap() {
    plot_data_addColorMap();
    readFileTimer.start();
    //setPlotRanges();
    state.PLAY_MODE = 3;
    setup_buttonsAndTools();
}

/**
 * @brief MainWindow::resetPlayback
 * Restarts playback of selected recorded file
 */
void MainWindow :: resetGui() {
    readFileTimer.stop();
    //    if (PAUSE_MODE == 0 && PLAY_MODE > 2)     PLAY_MODE = 3;

    //reset plotting variables to their initial values
    ping->lastTracePos = 0;
    flag.setupFirstColorMap = false;
    traceCounter1 = 0;
    ping->rangeLf_double = 0;
    ping->rangeHf_double = 0;
    state.GNSS_MODE = 0;
    flag.PlotAngleScan = false;
    flag.PlotHfSidescan = false;
    ui->splitter_depth->setStyleSheet("QSplitter::handle:horizontal {\nbackground: #aaa;\nborder: 2px solid #777;"
                                  "\nwidth: 4px;\nmargin-left: 10px;\nmargin-right: 10px;\nborder-radius: 4px;\n}");
    ui->splitter_depth->setHandleWidth(5);
    ui->splitter_depth->setEnabled(true);
    flag.showGap = userWindowInstance->enableGap(true);
    bottomTrack_clearQueue();
    cropTool->resetSlider();
    if (cropTool->toolActive)   on_actionCrop_File_triggered();
    target_closeFile();  // Call before clearing plot items

    format_enableGnssFeatures(true);
    emit showHfClickable(true);
    emit showAltimeterClickable(true);

    plot_clearAllPlots();

    resetPlotView();
    status_resetStatusBar();
    plot_replotAll();

    dataMap.clearAll();
    ping->firstRtcTime = 0;
    ping->pingNumFirst = 0;
    ChartWindowInstance->totalDistance = 0;
    ChartWindowInstance->gpsSpeed = 0;
}

void MainWindow :: plot_clearSonarPlots() {
    //remove all colormaps from plots
    while (colorMapNum_queue.size() != 0) {
        customPlotLF->removePlottable(colorMapNum_queue.dequeue());
    }
    colorMap = nullptr;
    ping->y_LowerBound = 0;
    ping->y_UpperBound = 0;
    while (colorMapHFNum_queue.size() != 0) {
        customPlotHF->removePlottable(colorMapHFNum_queue.dequeue());
    }
    while (depthMapNum_queue.size() != 0) {
        depthPlot->removePlottable(depthMapNum_queue.dequeue());
    }

    tools->removeAllTargetIcons();
    target_clearImagePlot();
    ui->imagePlot->replot(QCustomPlot::rpQueuedReplot);
    // Add boat icon back onto plot
    cropTool->clearItems(); // Keep this before ->clearItems(), or it will crash GUI
    customPlotLF->replot();
    customPlotLF->clearItems();
    lfBoatPixmap = new QCPItemPixmap(customPlotLF);
    lfBoatPixmap->setParent(customPlotLF);
    lfBoatPixmap->setSelectable(false);
    QPixmap pixmapLf(":/icons/icons/arrow_blue.png");
    lfBoatPixmap->setScaled(true);
    lfBoatPixmap->setPixmap(pixmapLf.scaled(40,40));
    lfBoatPixmap->setVisible(false);

    customPlotHF->replot();
    customPlotHF->clearItems();
    hfBoatPixmap = new QCPItemPixmap(customPlotHF);
    hfBoatPixmap->setParent(customPlotHF);
    hfBoatPixmap->setSelectable(false);
    hfBoatPixmap->setScaled(true);
    QPixmap pixmapHf(":/icons/icons/arrow_red.png");
    hfBoatPixmap->setPixmap(pixmapHf.scaled(40,40));
    hfBoatPixmap->setVisible(false);

    QPixmap pixmapCompass (":/icons/icons/Compass.png");
    lfCompassPixmap = new QCPItemPixmap(customPlotLF);
    lfCompassPixmap->setParent(customPlotLF);
    lfCompassPixmap->setSelectable(false);
    lfCompassPixmap->setPixmap(pixmapCompass);
    lfCompassPixmap->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    lfCompassPixmap->setScaled(true);
    lfCompassPixmap->setLayer("fixedIcons");
    lfCompassPixmap->topLeft->setCoords(30,50);
    lfCompassPixmap->bottomRight->setCoords(100,120);

    hfCompassPixmap = new QCPItemPixmap(customPlotHF);
    hfCompassPixmap->setParent(customPlotHF);
    hfCompassPixmap->setSelectable(false);
    hfCompassPixmap->setPixmap(pixmapCompass);
    hfCompassPixmap->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    hfCompassPixmap->setScaled(true);
    hfCompassPixmap->setLayer("fixedIcons");
    hfCompassPixmap->topLeft->setCoords(30,50);
    hfCompassPixmap->bottomRight->setCoords(100,120);

    double xPos = Format->widthLfHf;
    lfLegendIcon = new QCPItemPixmap(customPlotLF);
    lfLegendIcon->setParent(customPlotLF);
    lfLegendIcon->setSelectable(false);
    lfLegendIcon->setVisible(false);
    lfLegendIcon->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    lfLegendIcon->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    lfLegendIcon->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    lfLegendIcon->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    lfLegendIcon->setScaled(true);
    lfLegendIcon->setLayer("fixedIcons");
    lfLegendIcon->topLeft->setCoords(xPos - 70,30);
    lfLegendIcon->bottomRight->setCoords(xPos - 30,70);

    hfLegendIcon = new QCPItemPixmap(customPlotHF);
    hfLegendIcon->setParent(customPlotHF);
    hfLegendIcon->setSelectable(false);
    hfLegendIcon->setVisible(false);
    hfLegendIcon->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    hfLegendIcon->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    hfLegendIcon->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    hfLegendIcon->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    hfLegendIcon->setScaled(true);
    hfLegendIcon->setLayer("fixedIcons");
    hfLegendIcon->topLeft->setCoords(xPos - 70,30);
    hfLegendIcon->bottomRight->setCoords(xPos - 30,70);

    QPen line_color;
    line_color.setWidth(2);
    line_color.setColor(Qt::darkGray);

    crosshair_lf.add(customPlotLF);
    crosshair_hf.add(customPlotHF);

    plot_icons_updateCompassIcon();

    depthPlot->replot();

    customPlotLfLegend = new QCPItemText(customPlotLF);
    customPlotLfLegend->setLayer("fixedIcons");
    customPlotLfLegend->setFont(QFont("sans", 12));
    customPlotLfLegend->setColor(Qt::white);
    customPlotLfLegend->setBrush(QBrush(Qt::black));

    customPlotLfLegend->position->setTypeX(QCPItemPosition::ptAbsolute);
    customPlotLfLegend->position->setTypeY(QCPItemPosition::ptAbsolute);
//    customPlotLfLegend->position->setCoords(40, 100);
    customPlotLfLegend->setPositionAlignment(Qt::AlignLeft|Qt::AlignBottom);

    customPlotHfLegend = new QCPItemText(customPlotHF);
    customPlotHfLegend->setLayer("fixedIcons");
    customPlotHfLegend->setFont(QFont("sans", 12));
    customPlotHfLegend->setColor(Qt::white);
    customPlotHfLegend->setBrush(QBrush(Qt::black));

    customPlotHfLegend->position->setTypeX(QCPItemPosition::ptAbsolute);
    customPlotHfLegend->position->setTypeY(QCPItemPosition::ptAbsolute);
//    customPlotHfLegend->position->setCoords(40, 100);
    customPlotHfLegend->setPositionAlignment(Qt::AlignLeft|Qt::AlignBottom);
    plot_updateLfHfZoomLegendPosition();
    plot_showUpdatedZoomLegends();

    // Clear all graphs
    ui->sensor_graph->graph(0)->data()->clear();
    ui->sensor_graph->graph(1)->data()->clear();
    ui->sensor_graph->graph(2)->data()->clear();

    depthPlot->graph(0)->data()->clear();
    depthPlot->graph(1)->data()->clear();
    depthPlot->graph(2)->data()->clear();
    depthPlot->graph(3)->data()->clear();
    depthPlot->graph(4)->data()->clear();
    depthPlot->graph(5)->data()->clear();
    depthPlot->graph(6)->data()->clear();
    for (int i = depthPlot->itemCount() - 1; i > 0; i--) {
        // Remove all items, except for *line_depthTrace and *tracer_alt
        if (depthPlot->item(i) == line_depthTrace)  continue;
        else if (depthPlot->item(i) == depthLegend)  continue;
        else if (depthPlot->item(i) == depthLegend2)  continue;
        else if (depthPlot->item(i) == line_depthBoat)  continue;
        //else if (depthPlot->item(i) == depthBoatPixmap)  continue;
        else  depthPlot->removeItem(i);
    }
    QPen redPen2;
    redPen2.setWidthF(5);
    redPen2.setColor(Qt::red);
    redPen2.setStyle(Qt::DashLine);
    depthAlarm->handle = new QCPItemLine(depthPlot);
    depthAlarm->handle->setPen(redPen2);
    depthAlarm->handle->setSelectedPen(redPen2);
    depthAlarm->handle->start->setCoords(10, 0);
    depthAlarm->handle->end->setCoords(10, 0);
    depthAlarm->handle->setSelectable(false);
    depthAlarm->handle->setVisible(false);
    depthAlarm->handle->setLayer("tools1");
    line_depthTrace->setVisible(false);
    line->setVisible(false);
    line_depthBoat->setVisible(false);
    depthLegend->setText("");

//    QPixmap pixmap3(":/icons/icons/AlarmHandle.png");
//    QPixmap pixmap4 = pixmap3.scaled(40,40);
//    DepthAlarmHandle = new QCPItemPixmap(depthPlot);
//    DepthAlarmHandle->setParent(depthPlot);
//    DepthAlarmHandle->setSelectable(true);
//    DepthAlarmHandle->setScaled(true);
//    DepthAlarmHandle->setPixmap(pixmap4);
//    DepthAlarmHandle->setVisible(false);
//    DepthAlarmHandle->setLayer("overlay");
//    connect(DepthAlarmLine, SIGNAL(selectionChanged(bool)), this, SLOT(depthAlarm_selectHandle(bool)));
//    connect(DepthAlarmHandle, SIGNAL(selectionChanged(bool)), this, SLOT(depthAlarm_selectHandle(bool)));

    ui->portSide_graph->graph(0)->data()->clear();

    QPixmap pixmap3(":/icons/icons/rocket.png");
    depthBoatPixmap = new QCPItemPixmap(depthPlot);
    depthBoatPixmap->setParent(depthPlot);
    depthBoatPixmap->setLayer("boatLayer");
    depthBoatPixmap->setSelectable(false);
    depthBoatPixmap->setScaled(true);
    depthBoatPixmap->setPixmap(pixmap3.scaled(40,40));
    depthBoatPixmap->setVisible(false);
    depthLegend->setText("");

    tools->addAllPanelIcons(customPlotLF, customPlotHF);
    tools->updatePanelPosition(xPos);
}

void MainWindow :: plot_clearAllPlots() {
    plot_clearSonarPlots();

    // Clear flag list
    tools->removeAllTargetsFromList();
    tools->tracker.remove();
    tools->tracker.setupPlotItems(ui->gpsPlot);
    plot_replotAll();

    ChartWindowInstance->newCurve->data()->clear();
    ChartWindowInstance->newCurve2->data()->clear();
    ChartWindowInstance->newCurve3->data()->clear();
    ChartWindowInstance->portLine->setVisible(false);
    ChartWindowInstance->stbdLine->setVisible(false);
    ChartWindowInstance->plot_reset();
}

void MainWindow :: resetPlotView() {
    plot_offset_zoomResetLF(nullptr);
    plot_offset_zoomResetHF(nullptr);
    plot_range_unstretchLfHfPlots();
}

/**
 * @brief MainWindow::setRecPath
 * @param path
 * Sets path to save recorded files
 * path is given by path stored in user database, or it is the default recording directory set up on install
 */
void MainWindow :: setRecPath(QString path) {
    recorder.recPath = path;
    qDebug() << "SetRecPath: " << path;
}

/**
 * @brief MainWindow::setPlaySpeed
 * @param newPlot
 * Controls frequency of replots during file playback
 * The less frequent the replots, the faster the playback
 * Therefore the greater newPlot is, the faster files will play back
 */
void MainWindow :: setPlaySpeed(int newPlot) {
    setting.plotRate = newPlot;
}

/****************Range functions****************/

/**
 * @brief MainWindow::setRecordedRange_high
 * @param range
 * Sets the range according to the range received in the header information
 * of the trace data packet. If a range change is detected, the function
 * reassigns the variables for plotting according to the new received range.
 */
void MainWindow :: setRecordedRange(int range) {
    ping->rangeLf_double = ping->rangeLf_tmp;//range;
    ping->rangeHf_double = ping->rangeHf_tmp;
    if (ping->rangeLf_tmp != 0)   recentLfRange = ping->rangeLf_tmp;
    if (ping->rangeHf_tmp != 0)   recentHfRange = ping->rangeHf_tmp;

    plot_data_setParameters();
    ui_resetRangeButtons();

    switch (range) {
    case 40:
    case 80:
    case 160:
    case 200:
    case 400:
    case 600:
    case 800:
        // For msgType 100, set buttons based on range compared to the available ranges
        if (range == lfRangeList[0] || range == hfRangeList[0]) {
            ui->btn_range1->setChecked(true);
            ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        }
        else if (range == lfRangeList[1] || range == hfRangeList[1]) {
            ui->btn_range2->setChecked(true);
            ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        }
        else if (range == lfRangeList[2] || range == hfRangeList[2]) {
            ui->btn_range3->setChecked(true);
            ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        }
        else if (range == lfRangeList[3] || range == hfRangeList[3]) {
            ui->btn_range4->setChecked(true);
            ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        }

        break;
    case 30:
        ui->btn_range1->setChecked(true);
        ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        break;
    case 60:
        ui->btn_range2->setChecked(true);
        ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        break;
    case 120:
        ui->btn_range3->setChecked(true);
        ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        break;
    case 180:
        break;
    case 240:
        ui->btn_range4->setChecked(true);
        ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
        break;
    case 10:
    case 15:
    case 20:
    case 25:
    case 360:
        break;
    case 480:
        break;
    default:
        // If the range is not one of the available sonar ranges, then data is misaligned
        readFileTimer.stop();
        replotTimer.stop();
        return;
    }
}

/**
 * @brief MainWindow::on_range_btn_clicked
 * Sends the range command to the zync over TCP.
 */
void MainWindow :: on_range_btn_clicked(int index){
    if(state.GUI_MODE == 1) {
        Tcp_sendCommand(1, 0, index); //send
        traceCounter1 = 0;
    }
    else if(state.GUI_MODE == 0) {
        //get new values from database based on range and setup new colormaps
        plot_data_setParameters();
        traceCounter1 = 0;
    }
}

//Range buttons functions

void MainWindow :: on_auto_btn_clicked() {
    flag.EdgeAlignActive = ui->auto_btn->isChecked();

    // When the "auto" button is clicked, all range button properties should reset and be reconfigured
//    ui->btn_range1->setChecked(false);
//    ui->btn_range2->setChecked(false);
//    ui->btn_range3->setChecked(false);
//    ui->btn_range4->setChecked(false);
    ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
    ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
    ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
    ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
//    ui->btn_range1->setToolTip("");
//    ui->btn_range2->setToolTip("");
//    ui->btn_range3->setToolTip("");
//    ui->btn_range4->setToolTip("");

    if (ui->auto_btn->isChecked()) {
        // Set up HF ranges
        ui->auto_btn->setText("H");
        ui->auto_btn->setStyleSheet("color: rgb(255, 255, 255);background-color: rgb(200, 102, 102);");
//        ui->btn_range1->setText(QString::number(hfRangeList[0]));
//        ui->btn_range2->setText(QString::number(hfRangeList[1]));
//        ui->btn_range3->setText(QString::number(hfRangeList[2]));
//        ui->btn_range4->setText(QString::number(hfRangeList[3]));
        if (ping->rangeHf_tmp == hfRangeList[0]) {
            ui->btn_range1->setChecked(true);
            ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range1->setToolTip(QString::number(ping->rangeHf_tmp) + "m Range");
        }
        else if (ping->rangeHf_tmp == hfRangeList[1]) {
            ui->btn_range2->setChecked(true);
            ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range2->setToolTip(QString::number(ping->rangeHf_tmp) + "m Range");
        }
        else if (ping->rangeHf_tmp == hfRangeList[2]) {
            ui->btn_range3->setChecked(true);
            ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range3->setToolTip(QString::number(ping->rangeHf_tmp) + "m Range");
        }
        else if (ping->rangeHf_tmp == hfRangeList[3]) {
            ui->btn_range4->setChecked(true);
            ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range4->setToolTip(QString::number(ping->rangeHf_tmp) + "m Range");
        }

        // Force sonar to go to last HF range
//        if (recentHfRange == hfRangeList[0])        on_btn_range1_clicked();
//        else if (recentHfRange == hfRangeList[1])   on_btn_range2_clicked();
//        else if (recentHfRange == hfRangeList[2])   on_btn_range3_clicked();
//        else if (recentHfRange == hfRangeList[3])   on_btn_range4_clicked();

        if (ui->btn_range1->isChecked())        on_btn_range1_clicked();
        else if (ui->btn_range2->isChecked())   on_btn_range2_clicked();
        else if (ui->btn_range3->isChecked())   on_btn_range3_clicked();
        else if (ui->btn_range4->isChecked())   on_btn_range4_clicked();
    } else {
        // Set up LF ranges
        ui->auto_btn->setText("L");
        ui->auto_btn->setStyleSheet("color: rgb(255, 255, 255);background-color: rgb(102, 102, 200);");

//        ui->btn_range1->setText(QString::number(lfRangeList[0]));
//        ui->btn_range2->setText(QString::number(lfRangeList[1]));
//        ui->btn_range3->setText(QString::number(lfRangeList[2]));
//        ui->btn_range4->setText(QString::number(lfRangeList[3]));
        if (ping->rangeLf_tmp == lfRangeList[0]) {
            ui->btn_range1->setChecked(true);
            ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range1->setToolTip(QString::number(ping->rangeLf_tmp) + "m Range");
        }
        else if (ping->rangeLf_tmp == lfRangeList[1]) {
            ui->btn_range2->setChecked(true);
            ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range2->setToolTip(QString::number(ping->rangeLf_tmp) + "m Range");
        }
        else if (ping->rangeLf_tmp == lfRangeList[2]) {
            ui->btn_range3->setChecked(true);
            ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range3->setToolTip(QString::number(ping->rangeLf_tmp) + "m Range");
        }
        else if (ping->rangeLf_tmp == lfRangeList[3]) {
            ui->btn_range4->setChecked(true);
            ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
//            ui->btn_range4->setToolTip(QString::number(ping->rangeLf_tmp) + "m Range");
        }

        // Force sonar to go to last LF range
//        if (recentLfRange == lfRangeList[0])        on_btn_range1_clicked();
//        else if (recentLfRange == lfRangeList[1])   on_btn_range2_clicked();
//        else if (recentLfRange == lfRangeList[2])   on_btn_range3_clicked();
//        else if (recentLfRange == lfRangeList[3])   on_btn_range4_clicked();

        if (ui->btn_range1->isChecked())        on_btn_range1_clicked();
        else if (ui->btn_range2->isChecked())   on_btn_range2_clicked();
        else if (ui->btn_range3->isChecked())   on_btn_range3_clicked();
        else if (ui->btn_range4->isChecked())   on_btn_range4_clicked();
    }
}

void MainWindow :: ui_resetRangeButtons() {
    ui->btn_range1->setChecked(false);
    ui->btn_range2->setChecked(false);
    ui->btn_range3->setChecked(false);
    ui->btn_range4->setChecked(false);

    ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
    ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
    ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");
    ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(102, 102, 102);");

//    ui->btn_range1->setToolTip("");
//    ui->btn_range2->setToolTip("");
//    ui->btn_range3->setToolTip("");
//    ui->btn_range4->setToolTip("");
}

void MainWindow :: on_btn_range1_clicked(){
    ui_resetRangeButtons();
    ui->btn_range1->setChecked(true);
    ui->btn_range1->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");

    // ToDo: Compare range button to sonar model and set range variables accordingly
    if (ui->auto_btn->isChecked())  on_range_btn_clicked(hfRangeList[0] << 16);//(40 << 16);
    else                            on_range_btn_clicked(lfRangeList[0]);//(200);
}

void MainWindow :: on_btn_range2_clicked(){
    ui_resetRangeButtons();
    ui->btn_range2->setChecked(true);
    ui->btn_range2->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");

    if (ui->auto_btn->isChecked())  on_range_btn_clicked(hfRangeList[1] << 16);//(80 << 16);
    else                            on_range_btn_clicked(lfRangeList[1]);//(400);
}

void MainWindow :: on_btn_range3_clicked(){
    ui_resetRangeButtons();
    ui->btn_range3->setChecked(true);
//    ui->btn_range3->setToolTip("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");
    ui->btn_range3->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");

    if (ui->auto_btn->isChecked())  on_range_btn_clicked(hfRangeList[2] << 16);//(120 << 16);
    else                            on_range_btn_clicked(lfRangeList[2]);//(600);
}

void MainWindow :: on_btn_range4_clicked(){
    ui_resetRangeButtons();
    ui->btn_range4->setChecked(true);
    ui->btn_range4->setStyleSheet("color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);");

    if (ui->auto_btn->isChecked())  on_range_btn_clicked(hfRangeList[3] << 16);//(160 << 16);
    else                            on_range_btn_clicked(lfRangeList[3]);//(800);
}

/**************** GPS/Compass/nav ****************/

void MainWindow :: compass_receive(int data) {
    Tcp_sendCommand(20, 0, data);
}

void MainWindow :: compass_receiveClicked(bool bo) {
    flag.compassClicked = bo;
}

void MainWindow :: Gnss_determineSpeed () {
    //    int DD, MM, SSSS;

    double currentTime;
    double currentLat = 0, currentLon = 0;

    currentLat = gnssController->Latitude();//Lattitude_Deg;
    currentLon = gnssController->Longitude();
    currentTime = gnssController->Hour()*3600 + gnssController->Minute()*60 + gnssController->Second();
    gnssController->updateSpeed(ChartWindowInstance->gnss_calculateSpeed(currentLat, currentLon, currentTime));
    gnssController->SpeedType = 0;
}

void MainWindow :: Gnss_calcDistanceAndBearing
(double LatOld, double LonOld, double LatNew, double LonNew, double *distance, double *bearing) {
    // Calculate the distance between two points
    double c;
    double r = 6371000.0; //the radius of the earth
    c = qSin(LatNew * M_PI/180) * qSin(LatOld * M_PI/180) * qCos((LonNew - LonOld) * M_PI/180)
            + qCos(LatNew * M_PI/180) * qCos(LatOld * M_PI/180); //Calcs distance between boat gps and flag gps
    *distance = r * qAcos(c);

    //now finish calculating the distance, start with calculating the bearing angle
    double bearingNew = qAtan2(qSin((LonNew - LonOld) * M_PI/180) * qCos(LatNew * M_PI/180),
                               qCos(LatOld * M_PI/180) * qSin(LatNew*M_PI/180)
                               - qSin(LatOld * M_PI/180) * qCos(LatNew * M_PI/180) * qCos((LonNew - LonNew) * M_PI/180));
    *bearing = bearingNew * 180 / M_PI;
}

/**************** GPX and JASLOG Files ****************/

void MainWindow :: Gnss_createTrackPointFile () {
    QDateTime time = QDateTime::currentDateTime();

    QString fileName = recorder.recPath + "/trkpt-" + time.toString("MMddyyyy") + ".gpx";
    trkptGPX.setFileName(fileName);
    trkptGPX.open(QIODevice::ReadWrite | QIODevice::Text);
    fileName.replace("trkpt", "wpt");

    if (trkptGPX.size() == 0) {
        QByteArray trkptTxt;
        trkptTxt.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
        trkptTxt.append("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\" creator =\"Pathway\"> \n");
        trkptTxt.append("  <name>"+windowTitle+"</name> \n");
        trkptTxt.append("  <trk><trkseg> \n");
        trkptTxt.append("    </trkseg></trk> \n");
        trkptTxt.append("</gpx>");
        trkptGPX.write(trkptTxt);
    }
    else {
        trkptGPX.seek(trkptGPX.size()-15);
        QByteArray trkptTxt;
        trkptTxt.append("\n    <trkseg>\n");
        trkptTxt.append("    </trkseg></trk> \n");
        trkptTxt.append("</gpx>");
        trkptGPX.write(trkptTxt);
    }
}

void MainWindow :: Gnss_writeTrackPoints () {
    return;
    //after we get boat gps information, we need to send them to GPX file
    static int trkptNum = 1;
    //    trkptGPX.setFileName("C:/Users/macdo/Documents/Qt Projects/build-SonarProject-Desktop_Qt_5_12_3_MinGW_32_bit-Debug/recorded_files/trkpt.gpx");
    //    trkptGPX.open(QIODevice::WriteOnly | QIODevice::Text);
    //    trkptGPX.seek(0);
    QTextStream out(&trkptGPX);
    QByteArray trkptTxt;

    trkptGPX.seek(trkptGPX.size()-28);

    //    QDateTime time = QDateTime::currentDateTime();
    //    int trkptLine = 3;
    //    for(int i = 0; i < 4 + trkptLine *(trkptNum-1); i ++){
    //        QByteArray line = trkptGPX.readLine();
    //    }

    trkptTxt.append("        <trkpt lat=\""+QString::number(gnssController->Latitude(), 'f', 7)+"\" lon=\""+QString::number(gnssController->Longitude(), 'f', 7)+"\"> \n");
    trkptTxt.append("        <time>"+QString::number(gnssController->Year()) + "-" + QString("%1").arg(gnssController->Month(), 2, 10, QChar('0')) + "-"
                    +QString("%1").arg(gnssController->Day(), 2, 10, QChar('0')) + "T" + QString("%1").arg(gnssController->Hour(), 2, 10, QChar('0')) + ":"
                    +QString("%1").arg(gnssController->Minute(), 2, 10, QChar('0')) + ":" + QString("%1").arg(gnssController->Second(), 2, 10, QChar('0'))+
                    "Z</time></trkpt> \n");
    trkptTxt.append("    </trkseg></trk> \n");
    trkptTxt.append("</gpx>");
    trkptNum ++;
    trkptGPX.write(trkptTxt);
}

void MainWindow :: Gnss_readGpxFile () {
    ui->targetListWidget->hide();
    if (DEVELOPER_TESTING)      ui->targetListWidget->show();
    ui->waypointListWidget->show();
    ChartWindowInstance->path_resetTrackpoints();
    tools->removeAllWaypointsFromList();
    trkptGPX.seek(0);
    QByteArray trkptTxt = trkptGPX.readAll();
    QString trkptStr = trkptTxt;

    // Split file text to create a list of <trkpt> blocks
    QStringList trkptList = trkptStr.split("<trkpt ");
    QStringList wptList = trkptList.at(0).split("<wpt ");
    QStringList trkptItems, wptItems;
    double Lattitude = 0;
    double Longitude = 0;

    // Read waypoints
    int index = 0;
    for (int i = 1; i < wptList.length(); i++) {
        wptItems = wptList.at(i).split("\"");
        if (wptItems.length() > 3) {
            // Find the waypoint name
            QStringList wptName = wptItems.at(4).split("<name>");
            QStringList wptName2;
            QString waypointName = "";
            if (wptName.length() > 1) {
                wptName2 = wptName.at(1).split("</name>");
                waypointName = wptName2.at(0);
            }

            Lattitude = wptItems.at(1).toDouble();
            Longitude = wptItems.at(3).toDouble();

            QIcon cameraIcon(":/icons/icons/blankFlagIcon.png");

            // Add waypoint to flag list
            waypointItem obj(tools->waypointList.length(), waypointName, Lattitude, Longitude, -1, -1, -1, -1);
            obj.ListItem = new QListWidgetItem(obj.Name);
            obj.GpsHeading = -1;
            //            obj.startPoint =  QString::number(activeImageBox.start_point, 'd', 4);
            //            obj.end_point = activeImageBox.end_point;
            obj.ListItem->setIcon(cameraIcon);
            obj.importedWaypoint = true;

            // Add waypoint to chart window
            tools->appendTargetItem(obj);
            ChartWindowInstance->target_addWaypoint(Lattitude, Longitude, 0.001, index);
            index++;
        }
    }

    // Read all trackpoints in the file
    for (int i = 1; i < trkptList.length(); i++) {
        trkptItems = trkptList.at(i).split("\"");
        if (trkptItems.length() > 3) {
            Lattitude = trkptItems.at(1).toDouble();
            Longitude = trkptItems.at(3).toDouble();

            // add trackpoints to chart window

            ChartWindowInstance->path_addTrackpoints(Longitude, Lattitude);
            // resizeEvent(NULL);
            //            ChartWindowInstance->resizeAllTargets();
        }
    }
    bool found = 0;
    ChartWindowInstance->plot_resize(ChartWindowInstance->trackpoints->getKeyRange(found),
                                    ChartWindowInstance->trackpoints->getValueRange(found));
    target_sortWaypoints();
    ChartWindowInstance->mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: Gnss_saveGpsPoints () {
    if (!gnssController->validCoords)         return;
    //    liveBoatGPS.setFileName("C:/Users/macdo/Documents/Qt Projects/build-SonarProject-Desktop_Qt_5_12_3_MinGW_32_bit-Debug/recorded_files/boat.JASLOG");
    //    liveBoatGPS.open(QIODevice::WriteOnly | QIODevice::Text);
    //GPSFile.seek(GPSFile.size());
    QByteArray boatGpsTxt;
    boatGpsTxt.append(QString("%1").arg(ping->pingNumCurrent, 8, 10, QChar('0')) + "\t");
    boatGpsTxt.append("TIME=" + QString("%1").arg(gnssController->Hour(), 2, 10, QChar('0'))
                      + ":" + QString("%1").arg(gnssController->Minute(), 2, 10, QChar('0'))
                      + ":" + QString("%1").arg(gnssController->Second(), 2, 10, QChar('0'))
                      + ":000\t");// + QString("%1").arg(GpsItems.Millisecond, 3, 10, QChar('0')) + "\t");
    //    boatGpsTxt.append("LOCATION: " + QString::number(GpsItems.Lattitude_DMS, 'd', 4) + " " + GpsItems.Lattitude_NS
    //                      + " " + QString::number(GpsItems.Longitude_DMS, 'd', 4) + " " + GpsItems.Longitude_EW + "\n");
    boatGpsTxt.append("LOCATION=" + QString::number(gnssController->Latitude(), 'd', 7) + " "
                      + QString::number(gnssController->Longitude(), 'd', 7) + "\t");
    boatGpsTxt.append("HEADING=" + QString::number(gnssController->Heading(), 'd', 7) + "\t");
    boatGpsTxt.append("SPEED=" + QString::number(gnssController->Speed(), 'd', 7) + "\n");

    recorder.GPSFile.write(boatGpsTxt);
}

int  MainWindow :: Gnss_readGpsPointsFromFile () {
    QString tiffName;
    //qDebug()<<GPSFile.readLine();
    // Read GPS Lattitude and Longitude from .JASLOG, if file exists
    if (state.GNSS_MODE == 3) {
        QString GpsFileLine;
        QStringList GpsFileItems;
        QStringList fName;

        // Search for PING number
        while (1) {
            // Read line from file
            GpsFileLine = player.GPSFile.readLine();

            // Split items from line to read individual words and numbers
            GpsFileItems = GpsFileLine.split("\t");

            if (GpsFileLine == "") {
                // Break loop if the file has reached the end
                break;
            }

            if (ping->pingNumCurrent == GpsFileItems.at(0).mid(0,8).toInt()) {
                // The ping number of the sonar file and the .JASLOG file match -> therefore files are aligned
                if (GpsFileItems.length() < 2)  break;

                // Split text into separate QStringLists
                QStringList time = GpsFileItems.at(1).split(" ");
                QStringList time1 = time.at(0).split("=");
                QStringList time2 = time1.at(1).split(":");
                QStringList coords = GpsFileItems.at(2).split(" ");
                QStringList coords1 = coords.at(0).split("=");

                // read time
                if (time2.length() > 3) {
                    gnssController->updateTime(time2.at(0).toInt(), time2.at(1).toInt(), time2.at(2).toInt());
//                    GpsItems.Hour = time2.at(0).toInt();
//                    GpsItems.Minute = time2.at(1).toInt();
//                    GpsItems.Second = time2.at(2).toInt();
//                    GpsItems.Millisecond = time2.at(3).toInt();
                }

                // read the GPS coordinates
                if (coords.length() > 1) {
                    gnssController->updateCoords(coords1.at(1).toDouble(), coords.at(1).toDouble(), GpsFileItems.at(0).mid(0,8).toInt());
//                    GpsItems.Lattitude_Deg = coords1.at(1).toDouble();
//                    GpsItems.Longitude_Deg = coords.at(1).toDouble();
                } else      break;

                if (!gnssController->validCoords)     break;

                ChartWindowInstance->path_addData(gnssController->Longitude(), gnssController->Latitude());

                double dist = ping->rangeLf_double - ping->lastTraceAlt;
                if (flag.showGap)   dist = ping->rangeLf_double;

                if (GpsFileItems.length() > 4) {
                    QStringList heading = GpsFileItems.at(3).split(" ");
                    QStringList heading1 = heading.at(0).split("=");
                    gnssController->updateHeading(heading1.last().toDouble());

                    QStringList speed = GpsFileItems.at(4).split(" ");
                    QStringList speed1 = speed.at(0).split("=");
                    gnssController->updateSpeed(speed1.last().toDouble());

                    if (gnssController->validHeading)   gnssController->HeadingType = 1;
                    else {
                        gnssController->updateHeading(ChartWindowInstance->gnss_calculateHeading(gnssController->Longitude(), gnssController->Latitude(), gnssController->Heading()));
                        gnssController->HeadingType = 0;
                    }
                    if (gnssController->validSpeed)     gnssController->SpeedType = 1;
//                    else                                Gnss_determineSpeed();
                    ChartWindowInstance->legend_updateSpeedText(gnssController->Speed());
                    ChartWindowInstance->legend_updateHeadingText(gnssController->Heading());
                } else {
                    Gnss_determineSpeed();
                    ChartWindowInstance->legend_updateSpeedText(gnssController->Speed());
                    gnssController->updateHeading(ChartWindowInstance->gnss_calculateHeading(gnssController->Longitude(), gnssController->Latitude(), gnssController->Heading()));
                    gnssController->HeadingType = 0;
                }
                ChartWindowInstance->path_addBoundaryPoints(gnssController->Longitude(), gnssController->Latitude(), gnssController->Heading(), dist, ping->angleOrientation);
                break;
            }
            else if (GpsFileItems.at(0).mid(0,8).toInt() > ping->pingNumCurrent) {
                // Files are misaligned
                player.GPSFile.seek(0);
                return (GpsFileItems.at(0).mid(0,8).toInt() - ping->pingNumCurrent);
            }
            else if (GpsFileItems.at(0).mid(0,8).toInt() < ping->pingNumCurrent) {
                continue;
            }
        }
    }
    else {
        // If no GPS file is found, then reset the coordinates
        gnssController->reset();
    }

    //qDebug()<<tiffNameArray.at(0);
    ChartWindowInstance->mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
    return 0;
}

/****************Plots****************/

/**
 * @brief MainWindow::getCustomGrad
 * @param gradient
 * To set the gradient of the color scale on the main plotting area.
 */
void MainWindow :: plot_colorScale_getCustomGrad(QCPColorGradient gradient){
    QColor nullColor, firstColor;
    QMap<double, QColor> getGradientColors = gradient.colorStops();
    firstColor = getGradientColors.first();
    nullColor.setRgb(0,0,0);
    //nullColor.setAlpha(255);
    gradient.setColorStopAt(0, nullColor);
    gradient.setColorStopAt(0.001, firstColor);
    //assign the gradient selected on the user setting menu
    colorScale->setGradient(gradient);
    colorScaleHF->setGradient(gradient);
    plot_replotAll();
}

void MainWindow :: plot_colorScale_getDepthGrad(QCPColorGradient gradient){
    QColor nullColor, firstColor;
    QMap<double, QColor> getGradientColors = gradient.colorStops();
    firstColor = getGradientColors.first();
    nullColor.setRgb(0,0,0);
    //nullColor.setAlpha(255);
    gradient.setColorStopAt(0, nullColor);
    gradient.setColorStopAt(0.001, firstColor);
    //assign the gradient selected on the user setting menu
    newDepthcolorScale->setGradient(gradient);
    plot_replotAll();
}

void MainWindow :: plot_colorScale_hide(bool hide_color_scale){
    if(hide_color_scale){
        colorScale->setVisible(false);
        colorScaleHF->setVisible(false);
    }
    else{
        colorScale->setVisible(true);
        colorScaleHF->setVisible(true);
    }


    //the goal is to remove the plot completely//
    //the commented code crashes the GUI on attempt

    //see how many elements there are
    /*int elementCount = ui->customPlot->plotLayout()->elementCount();
    //loop over the elements
    for(int i = 0; i < elementCount; i++){
       //test to see if any of the layout elements are of QCPColorScale type
        if(qobject_cast<QCPColorScale*>(ui->customPlot->plotLayout()->elementAt(i)))
                ui->customPlot->plotLayout()->removeAt(i);

        //collapse the empty elements
        ui->customPlot->plotLayout()->simplify();

     }*/

    /*    if(hide_color_scale){
        if (customPlotLF->plotLayout()->hasElement(0, 1)) {
            customPlotLF->plotLayout()->remove(customPlotLF->plotLayout()->element(0,1));
            customPlotLF->plotLayout()->simplify();
        }
        if (customPlotHF->plotLayout()->hasElement(0, 1)) {
            customPlotHF->plotLayout()->remove(customPlotHF->plotLayout()->element(0,1));
            customPlotHF->plotLayout()->simplify();
        }
    }
    else{
        if (!customPlotLF->plotLayout()->hasElement(0, 1)) {
            colorScale = new QCPColorScale(customPlotLF);
            //assigning the color range to color scale according to output bits
            QCPRange colorRange = QCPRange(colorScaleMin,colorScaleMax);
            colorScale->setDataRange(colorRange);
            colorScale->setRangeDrag(false);
            colorScale->setRangeZoom(false);
            colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)

            colorScale->axis()->setVisible(false);
            // make sure the axisrect and color scale synchronize their bottom and top margins (so they line up):
            QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlotLF);
            customPlotLF->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
            colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
            customPlotLF->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
            customPlotLF->layer("main")->setMode(QCPLayer::lmBuffered);
        }
        if (!customPlotHF->plotLayout()->hasElement(0, 1)) {
            colorScaleHF = new QCPColorScale(customPlotHF);
            //assigning the color range to color scale according to output bits
            QCPRange colorRange = QCPRange(colorScaleMin,colorScaleMax);
            colorScaleHF->setDataRange(colorRange);
            colorScaleHF->setRangeDrag(false);
            colorScaleHF->setRangeZoom(false);
            colorScaleHF->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)

            colorScaleHF->axis()->setVisible(false);
            // make sure the axisrect and color scale synchronize their bottom and top margins (so they line up):
            QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlotHF);
            customPlotHF->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
            colorScaleHF->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
            customPlotHF->plotLayout()->addElement(0, 1, colorScaleHF); // add it to the right of the main axis rect
            customPlotHF->layer("main")->setMode(QCPLayer::lmBuffered);
        }
    }*/
}

/**
 * @brief MainWindow::plot_addColorMap
 * Adds a new color map after plotting 500 traces.
 * Removes the old color maps from the plot if the
 * number of color maps on the plot exeeds the count of
 * 2. Paramaterize and add a new color map according to
 * various ranges.
 */
void MainWindow :: plot_data_addColorMap() {
    plot_offset_verticalShiftCalculator();
    if (ping->rangeLf_tmp != ping->rangeLf_int) {
        // remove all colormaps from plot
        plot_clearSonarPlots();
        dataMap.clearAll();
        if (ping->rangeHf_tmp == 0 && ping->rangeLf_tmp > 0 && ping->rangeLf_int == 0) {
            // Hide HF plot and show LF plot only
            format_showLfPlot(true);
            format_showHfPlot(false);
        }
        if (ping->rangeLf_tmp != 0)      plot_offset_zoomResetLF(nullptr);
        // Updates ranges on TVG window
        tvgWindowInstance->updateTvgCurve();
    }
    if (ping->rangeHf_tmp != ping->rangeHf_int) {
        // remove all colormaps from plot
        plot_clearSonarPlots();
        dataMap.clearAll();
        if (ping->rangeLf_tmp == 0 && ping->rangeHf_tmp > 0 && ping->rangeHf_int == 0) {
            // Hide LF plot and show HF plot only
            format_showHfPlot(true);
            format_showLfPlot(false);
        }
        if (ping->rangeHf_tmp != 0)      plot_offset_zoomResetHF(nullptr);
        if (ping->rangeLf_tmp == ping->rangeLf_int)     tvgWindowInstance->updateTvgCurve();
    }
    if (colorMapNum_queue.size()+1 > 3 ) {
        bool found;
        customPlotLF->removePlottable(colorMapNum_queue.dequeue());

        ping->y_LowerBound = colorMapNum_queue.first()->getValueRange(found).lower; //customPlotLF->plottable(0)->getValueRange(found).lower;
        ui->sensor_graph->graph(0)->data()->removeBefore(ping->y_LowerBound);
        ui->sensor_graph->graph(1)->data()->removeBefore(ping->y_LowerBound);
        ui->sensor_graph->graph(2)->data()->removeBefore(ping->y_LowerBound);

        depthPlot->graph(0)->data()->removeBefore(ping->y_LowerBound);
        depthPlot->graph(1)->data()->removeBefore(ping->y_LowerBound);
        depthPlot->graph(2)->data()->removeBefore(ping->y_LowerBound);
        depthPlot->graph(3)->data()->removeBefore(ping->y_LowerBound);
        depthPlot->graph(4)->data()->removeBefore(ping->y_LowerBound);
        depthPlot->graph(5)->data()->removeBefore(ping->y_LowerBound);
        depthPlot->graph(6)->data()->removeBefore(ping->y_LowerBound);

        for(int i=item_text.size()-1; i>=0;i--){
            //depthPlot->graph(4)->data()->removeBefore(ping->y_LowerBound);
            if (item_text.at(i).yCoord < ping->y_LowerBound) {
                depthPlot->removeItem(item_text.at(i).item);
                item_text.removeAt(i);
            }
        }

        cropTool->removeItemsBelowY(ping->y_LowerBound);
        target_RemoveAllIconsBelowMap(ping->y_LowerBound);
        measuringTape_RemoveItemsFromPlot(ping->y_LowerBound);
        dataMap.removeItemsWithY(0, ping->y_LowerBound);
    }

    if (flag.PlotHfSidescan) {
        if (colorMapHFNum_queue.size()+1 > 3) {
            customPlotHF->removePlottable(colorMapHFNum_queue.dequeue());
        }
    }
    if (flag.PlotAngleScan) {
        if (depthMapNum_queue.size()+1>3) {
            depthPlot->removePlottable(depthMapNum_queue.dequeue());
        }
    }
    customPlotLF->replot(QCustomPlot::rpQueuedReplot);
    customPlotHF->replot(QCustomPlot::rpQueuedReplot);

    // setUpColorMap
    if(state.GUI_MODE == 2){
        if (ping->rangeLf_int < 200)
            setting.numOfTraces = 500;
        else
            setting.numOfTraces = 300;
    }
    else{
        setting.numOfTraces = 500;
    }

    traceCounter1 = 0;
    ping->res_lat_lf = (ping->rangeLf_double / ping->samplesToPlotLF); // Lateral Resolution
    ping->res_lat_hf = (ping->rangeHf_double / ping->samplesToPlotHF);
    ping->res_fwd = round(0.00068533*setting.boatSpeed*ping->rangeLf_double * 100000)*0.00001;
    if (ping->rangeLf_double < 1)    ping->res_fwd = 0.00068533*setting.boatSpeed*ping->rangeHf_double;
    int nx_lf = ping->samplesToPlotLF * 2;//we multiply this by 2 for 2 channels, port and starboard
    int ny_lf = setting.numOfTraces;// + (ping->rangeLf_double * qCos(ping->degree * M_PI / 180) / ping->res_fwd);
    double x1_lf = -ping->rangeLf_double;
    double x2_lf = ping->rangeLf_double;
    double y1_lf = ping->lastTracePos + 0.5*ping->res_fwd;
    double y2_lf = ping->lastTracePos + ((setting.numOfTraces) * ping->res_fwd) + 0.5*ping->res_fwd;

    int nx_hf = ping->samplesToPlotHF * 2;//we multiply this by 2 for 2 channels, port and starboard
    int ny_hf = setting.numOfTraces;// + (ping->rangeHf_double * qCos(ping->degree * M_PI / 180) / ping->res_fwd);
    double x1_hf = -ping->rangeHf_double;
    double x2_hf = ping->rangeHf_double;
    double y1_hf = y1_lf;
    double y2_hf = ping->lastTracePos + ((setting.numOfTraces) * ping->res_fwd) + 0.5*ping->res_fwd;

    if (ping->degree < 88) {
        ny_lf += (ping->rangeLf_double * qCos(ping->degree * M_PI / 180) / ping->res_fwd);
        y2_lf += (ping->rangeLf_double * qCos(ping->degree * M_PI / 180));
        ny_hf += (ping->rangeHf_double * qCos(ping->degree * M_PI / 180) / ping->res_fwd);
        y2_hf += (ping->rangeHf_double * qCos(ping->degree * M_PI / 180));
    }

    /*******Setup LF Plot********/
    if (ping->rangeLf_double > 0) {
        // set up the QCPColorMap:N
        colorMap = new QCPColorMap(customPlotLF->xAxis, customPlotLF->yAxis);//assign colormap to customplot

        colorMap->data()->setSize(nx_lf, ny_lf); // we want the color map to have nx * ny data points
        colorMap->data()->setRange(QCPRange(x1_lf, x2_lf), QCPRange(y1_lf, y2_lf));//sets physical size of colormap
        colorMap->setInterpolate(false);//to get visible pixel pattern
        colorMap->setLayer("colormap");
//        colorMap->data()->fill(0); // make the color map transparent
        // add a color scale:
        colorMap->setColorScale(colorScale); // associate the color map with the color scale

        if (colorMapNum_queue.length() > 0) {
            // Copy data from the old color map onto the new color map to avoid the blanked out areas
            QCPColorMap *previousMap = colorMapNum_queue.last();

            // Find the size of each color map (in meters) to find a reasonable plotting area to copy
            double xMin = previousMap->data()->keyRange().lower, xMax = previousMap->data()->keyRange().upper;
            double yMin = previousMap->data()->valueRange().lower, yMax = previousMap->data()->valueRange().upper;
            double xStart = (x1_lf < xMin) ? xMin : x1_lf;
            double xEnd = (x2_lf > xMax) ? xMax : x2_lf;
            double yStart = (y1_lf < yMin) ? yMin : y1_lf;
            double yEnd = (y2_lf > yMax) ? yMax : y2_lf;

            // Find the X/Y resolution of each plot
            double newMapXRes = (colorMap->data()->keyRange().upper - colorMap->data()->keyRange().lower) / (colorMap->data()->keySize() * 1.0);
            double newMapYRes = (colorMap->data()->valueRange().upper - colorMap->data()->valueRange().lower) / (colorMap->data()->valueSize() * 1.0);
            double oldMapXRes = (previousMap->data()->keyRange().upper - previousMap->data()->keyRange().lower) / (previousMap->data()->keySize() * 1.0);
            double oldMapYRes = (previousMap->data()->valueRange().upper - previousMap->data()->valueRange().lower) / (previousMap->data()->valueSize() * 1.0);

            // Find a smallest X/Y resolution used to copy the data
            double XResolution = (newMapXRes < oldMapXRes) ? newMapXRes : oldMapXRes;
            double YResolution = (newMapYRes < oldMapYRes) ? newMapYRes : oldMapYRes;

            int xSize = (xEnd - xStart) / XResolution;
            int ySize = (yEnd - yStart) / YResolution;

            // Start with the bottom row and move up
            for (int j = 0; j < ySize; j++) {
                double xCoord, yCoord = yStart + j*YResolution, color;

                // Start from left side and go right
                for (int i = 0; i < xSize; i++) {
                    xCoord = xStart + i*XResolution;

                    // Otherwise, we can copy the cell to the new colormap
                    color = previousMap->data()->data(xCoord, yCoord);
                    colorMap->data()->setData(xCoord, yCoord, color);
                }
            }
        }
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        //to keep track of the color maps on the plotting area.
        colorMapNum_queue.enqueue(colorMap);
    }

    if(flag.PlotHfSidescan){
        if (ping->rangeHf_double > 0) {
            colorMapHF = new QCPColorMap(customPlotHF->xAxis, customPlotHF->yAxis);//assigns colormap to customplot
            colorMapHF->data()->setSize(nx_hf, ny_hf);
            colorMapHF->setLayer("colormap");
            colorMapHF->data()->setRange(QCPRange(x1_hf, x2_hf), QCPRange(y1_hf, y2_hf));//sets physical size of colormap
            colorMapHF->setInterpolate(false);
            colorMapHF->data()->fill(0);//make the color map transparent
            //add a color scale:
            colorMapHF->setColorScale(colorScaleHF);//associate the color map with the color scale

            if (colorMapHFNum_queue.length() > 0 && ping->angleOrientation < 89) {
                // Copy data from the old color map onto the new color map to avoid the blanked out areas
                QCPColorMap *previousMap = colorMapHFNum_queue.last();

                // Find the size of each color map (in meters) to find a reasonable plotting area to copy
                double xMin = previousMap->data()->keyRange().lower, xMax = previousMap->data()->keyRange().upper;
                double yMin = previousMap->data()->valueRange().lower, yMax = previousMap->data()->valueRange().upper;
                double xStart = (x1_lf < xMin) ? xMin : x1_lf;
                double xEnd = (x2_lf > xMax) ? xMax : x2_lf;
                double yStart = (y1_lf < yMin) ? yMin : y1_lf;
                double yEnd = (y2_lf > yMax) ? yMax : y2_lf;

                // Find the X/Y resolution of each plot
                double newMapXRes = (colorMapHF->data()->keyRange().upper - colorMapHF->data()->keyRange().lower) / (colorMapHF->data()->keySize() * 1.0);
                double newMapYRes = (colorMapHF->data()->valueRange().upper - colorMapHF->data()->valueRange().lower) / (colorMapHF->data()->valueSize() * 1.0);
                double oldMapXRes = (previousMap->data()->keyRange().upper - previousMap->data()->keyRange().lower) / (previousMap->data()->keySize() * 1.0);
                double oldMapYRes = (previousMap->data()->valueRange().upper - previousMap->data()->valueRange().lower) / (previousMap->data()->valueSize() * 1.0);

                // Find a smallest X/Y resolution used to copy the data
                double XResolution = (newMapXRes < oldMapXRes) ? newMapXRes : oldMapXRes;
                double YResolution = (newMapYRes < oldMapYRes) ? newMapYRes : oldMapYRes;

                int xSize = (xEnd - xStart) / XResolution;
                int ySize = (yEnd - yStart) / YResolution;

                // Start with the bottom row and move up
                for (int j = 0; j < ySize; j++) {
                    double xCoord, yCoord = yStart + j*YResolution, color;

                    // Start from left side and go right
                    for (int i = 0; i < xSize; i++) {
                        xCoord = xStart + i*XResolution;

                        // Otherwise, we can copy the cell to the new colormap
                        color = previousMap->data()->data(xCoord, yCoord);
                        colorMapHF->data()->setData(xCoord, yCoord, color);
                    }
                }
            }
            customPlotHF->replot(QCustomPlot::rpQueuedReplot);
            colorMapHFNum_queue.enqueue(colorMapHF);//used to keep track of how many colormaps are on the plot
        }
    }
    if(flag.PlotAngleScan && flag.PlotAngleScanColorMap){
        depthMap = new QCPColorMap(depthPlot->xAxis, depthPlot->yAxis);
        depthMapNum_queue.enqueue(depthMap);
        double y2_depth = y1_lf + ((setting.numOfTraces - 1) * ping->res_fwd) + 0.5*ping->res_fwd;
        depthMap->data()->setSize(nx_lf/2, setting.numOfTraces);
        depthMap->data()->setRange(QCPRange(0, x2_lf), QCPRange(y1_lf, y2_depth));
        depthMap->setInterpolate(false);
        depthMap->data()->fill(0);
        depthMap->setColorScale(newDepthcolorScale);
        depthMap->setLayer("colormap");
        depthPlot->replot(QCustomPlot::rpQueuedReplot);
    }
//    scaleXaxisZoom();
}

void MainWindow :: plot_data_setParameters() {
    if (state.GUI_MODE == 0) {
        // LF Parameters
        int index = 1;
        if (ui->btn_range1->isChecked())  index = 1;
        else if (ui->btn_range2->isChecked())  index = 2;
        else if (ui->btn_range3->isChecked())  index = 3;
        else if (ui->btn_range4->isChecked())  index = 4;
        QList<double> swathList_config1 = {5,10,15,20,25,30,35,40};
        ping->rangeLf_double =  (swathList_config1.at(index));  // Sonar Range
        ping->rangeLf_tmp = (quint16) ping->rangeLf_double;
        ping->samplesToPlotLF = 1000;   // Number of Samples per Ping
        ping->freq_rep = 125 * ping->rangeLf_double;           // Repittion Frequency of Sonar
        ping->res_fwd = (setting.boatSpeed*0.5144/ping->freq_rep)*setting.convert_unit;  // Forward resolution based on boat speed

        // HF Parameters
        flag.PlotHfSidescan = 1;
        ping->rangeHf_double =  (swathList_config1.at(index));
        ping->rangeHf_tmp = (quint16) ping->rangeHf_double;
        ping->samplesToPlotHF = 1000; //higher range colormaps use up a lot of memory, so we compress the data to reduce it
    }
    ping->currentSamplingFrequency = 31250;

    // LF Parameters
    ping->res_lat_lf = (ping->rangeLf_double / ping->samplesToPlotLF); // Lateral Resolution
    ping->freq_rep = 125 * ping->rangeLf_double;            // Repittion Frequency of Sonar
    ping->res_fwd = round(0.00068533*setting.boatSpeed*ping->rangeLf_double * 100000)*0.00001;//(boatSpeed*0.5144/f_rep_config1)*convert_unit;  // Forward resolution based on boat speed

    // HF Parameters
    ping->res_lat_hf = (ping->rangeHf_double / ping->samplesToPlotHF);//map_x2_config2 / samplesToPlot_config2; // Lateral Resolution
    //res_fwd_config2 = 0.00068533*boatSpeed*rangeHf_double;
    if (ping->rangeLf_tmp == 0)   ping->res_fwd = round(0.00068533*setting.boatSpeed*ping->rangeHf_double * 100000)*0.00001;

    if (state.GUI_MODE == 2 && flag.setupFirstColorMap) {
        player_nextColorMap(); // Pause plotting and wait for user to continue player
    } else {
        plot_data_addColorMap();
        flag.setupFirstColorMap = true;
    }
}

void MainWindow :: plot_data_checkToAddNewMap() {
    if(traceCounter1 == setting.numOfTraces) {// || freqChanged == true) {//if maximum number of traces plotted for this colormap
        if (state.GUI_MODE == 2)    player_nextColorMap();
        else                        plot_data_addColorMap();

        traceCounter1 = 0;
    }
}

void MainWindow :: plot_data_updateTVG(GainSettings::channelType chType, GainSettings gain_old, int gainType) {
    if (gainType == 3) {
        if (chType == GainSettings::LfStbd || chType == GainSettings::LfPort)
            plot_data_changeGainOnLf(gain->tvg_CLfP - gain_old.tvg_CLfP, gain->tvg_CLfS - gain_old.tvg_CLfS);
        else
            plot_data_changeGainOnHf(gain->tvg_CHfP - gain_old.tvg_CHfP, gain->tvg_CHfS - gain_old.tvg_CHfS);
        return;
    }

    if (chType == GainSettings::LfStbd || chType == GainSettings::LfPort) {
        if (colorMapNum_queue.length() == 0)      return;
        int pingStart = dataMap.firstKey(), pingEnd = dataMap.lastKey();
        for (int j = pingStart; j <= pingEnd; j++) {
            PingMapItem item = dataMap.findItem(j);
            if (!item.isValid)    return;

            int sampleNumber = 0;
            if (!flag.showGap) {
                sampleNumber = item.altitude*41/item.altitudeFactor;
                if (ping->lastTraceAlt < 1 || ping->lastTraceAlt > 1000) {
                    sampleNumber = 1;
                }
                if (flag.PlotHfSidescan && !flag.PlotAngleScan) {
                    sampleNumber = 1;
                }
            }

            double tanDegree = 0;
            if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
            double xCoord = 0, yCoord = 0;
            double tvgGainLfPort, tvgGainLfStbd, tvgGainLfPort_old, tvgGainLfStbd_old;
            float starboardSideColor1, portSideColor1;
            QCPColorMap *cMap = colorMapNum_queue.first();
            bool found;
            int k = colorMapNum_queue.length()-1;
            for (k = colorMapNum_queue.length()-1; k >= 0; k--) {
                if (item.yCoord > colorMapNum_queue.at(k)->getValueRange(found).lower) {
                    cMap = colorMapNum_queue.at(k);
                    break;
                }
            }
            double yBound = colorMapNum_queue.last()->getValueRange(found).upper;
            if (colorMapNum_queue.length() > k+1)
                yBound = colorMapNum_queue.at(k+1)->getValueRange(found).lower;
            for (int i = sampleNumber; i < ping->samplesToPlotLF; i++) {
                xCoord = ((i - sampleNumber) * ping->res_lat_lf);
                yCoord = (xCoord*tanDegree) + item.yCoord;
                if (yCoord + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > k+1)
                        cMap = colorMapNum_queue.at(k+1);
                }

                // Get old data value
                portSideColor1 = cMap->data()->data(-xCoord, yCoord);
                starboardSideColor1 = cMap->data()->data(xCoord, yCoord);

                // Add TVG
                tvgGainLfPort_old = (gain_old.calculateTvg(i, ping->res_lat_lf, GainSettings::LfPort, false, true));
                tvgGainLfPort = (gain->calculateTvg(i, ping->res_lat_lf, GainSettings::LfPort, false, true));
                tvgGainLfStbd_old = (gain_old.calculateTvg(i, ping->res_lat_lf, GainSettings::LfStbd, false, true));
                tvgGainLfStbd = (gain->calculateTvg(i, ping->res_lat_lf, GainSettings::LfStbd, false, true));
                portSideColor1 *= pow(10.0,((tvgGainLfPort-tvgGainLfPort_old)/20.0));
                starboardSideColor1 *= pow(10.0,((tvgGainLfStbd-tvgGainLfStbd_old)/20.0));;

                // Assign data to Port Side
                cMap->data()->setData(-xCoord, yCoord, portSideColor1);
                // Assign data to Starboard Side
                cMap->data()->setData(xCoord, yCoord, starboardSideColor1);
            }
        }
        customPlotLF->replot();
    } else if (chType == GainSettings::HfStbd || chType == GainSettings::HfPort) {
        if (colorMapHFNum_queue.length() == 0)      return;
        int pingStart = dataMap.firstKey(), pingEnd = dataMap.lastKey();
        for (int j = pingStart; j <= pingEnd; j++) {
            PingMapItem item = dataMap.findItem(j);
            if (!item.isValid)    return;

            int sampleNumber = 0;
            if (!flag.showGap) {
                sampleNumber = item.altitude*41/item.altitudeFactor;
                if (ping->lastTraceAlt < 1 || ping->lastTraceAlt > 1000) {
                    sampleNumber = 1;
                }
                if (flag.PlotHfSidescan && !flag.PlotAngleScan) {
                    sampleNumber = 1;
                }
            }

            double tanDegree = 0;
            if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
            double xCoord = 0, yCoord = 0;
            double tvgGainPort, tvgGainStbd, tvgGainPort_old, tvgGainStbd_old;
            float starboardSideColor1, portSideColor1;
            QCPColorMap *cMap = colorMapHFNum_queue.first();
            bool found;
            int k = colorMapHFNum_queue.length()-1;
            for (k = colorMapHFNum_queue.length()-1; k >= 0; k--) {
                if (item.yCoord > colorMapHFNum_queue.at(k)->getValueRange(found).lower) {
                    cMap = colorMapHFNum_queue.at(k);
                    break;
                }
            }
            double yBound = colorMapHFNum_queue.last()->getValueRange(found).upper;
            if (colorMapHFNum_queue.length() > k+1)
                yBound = colorMapHFNum_queue.at(k+1)->getValueRange(found).lower;
            for (int i = sampleNumber; i < ping->samplesToPlotHF; i++) {
                xCoord = ((i - sampleNumber) * ping->res_lat_hf);
                yCoord = (xCoord*tanDegree) + item.yCoord;
                if (yCoord + 0.5*ping->res_fwd_hf > yBound) {
                    if (colorMapHFNum_queue.length() > k+1)
                        cMap = colorMapHFNum_queue.at(k+1);
                }

                // Get old data value
                portSideColor1 = cMap->data()->data(-xCoord, yCoord);
                starboardSideColor1 = cMap->data()->data(xCoord, yCoord);

                // Add TVG
                tvgGainPort_old = (gain_old.calculateTvg(i, ping->res_lat_hf, GainSettings::HfPort, false, true));
                tvgGainPort = (gain->calculateTvg(i, ping->res_lat_hf, GainSettings::HfPort, false, true));
                tvgGainStbd_old = (gain_old.calculateTvg(i, ping->res_lat_hf, GainSettings::HfStbd, false, true));
                tvgGainStbd = (gain->calculateTvg(i, ping->res_lat_hf, GainSettings::HfStbd, false, true));
                portSideColor1 *= pow(10.0,((tvgGainPort-tvgGainPort_old)/20.0));
                starboardSideColor1 *= pow(10.0,((tvgGainStbd-tvgGainStbd_old)/20.0));;

                // Assign data to Port Side
                cMap->data()->setData(-xCoord, yCoord, portSideColor1);
                // Assign data to Starboard Side
                cMap->data()->setData(xCoord, yCoord, starboardSideColor1);
            }
        }
        customPlotHF->replot();
    }
}

void MainWindow :: plot_data_changeGainOnLf(double newMainGainPort, double newMainGainStbd){
    // When the user changes gain settings on the gain slider, the
    //   plot will update all plots with new gain setting
    //if (abs(newMainGain) < 0.001)       return;

    double gainMultiplier = pow(10.0,(newMainGainPort/20.0));

    double color =0;
    if (colorMap == nullptr)      return;
    for(int i=0; i < colorMapNum_queue.size(); i++){
        for(int j=0; j < colorMapNum_queue.at(i)->data()->keySize()/2; j++){
            for(int k=0; k < colorMapNum_queue.at(i)->data()->valueSize(); k++){

                color = colorMapNum_queue.at(i)->data()->cell(j, k);
                color *= gainMultiplier;

                colorMapNum_queue.at(i)->data()->setCell(j, k, color);
            }
        }
    }
    gainMultiplier = pow(10.0,(newMainGainStbd/20.0));
    for(int i=0; i < colorMapNum_queue.size(); i++){
        for(int j=colorMapNum_queue.at(i)->data()->keySize()/2; j < colorMapNum_queue.at(i)->data()->keySize(); j++){
            for(int k=0; k < colorMapNum_queue.at(i)->data()->valueSize(); k++){

                color = colorMapNum_queue.at(i)->data()->cell(j, k);
                color *= gainMultiplier;

                colorMapNum_queue.at(i)->data()->setCell(j, k, color);
            }
        }
    }
    customPlotLF->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: plot_data_changeGainOnHf(double newMainGainPort, double newMainGainStbd){
    //if (abs(newMainGain) < 0.001)       return;

    double gainMultiplier = pow(10.0,(newMainGainPort/20.0));
    double color = 0;
    if (colorMapHF == nullptr)      return;
    for(int i=0; i < colorMapHFNum_queue.size(); i++){
        for(int j=0; j < colorMapHFNum_queue.at(i)->data()->keySize()/2; j++){
            for(int k=0; k < colorMapHFNum_queue.at(i)->data()->valueSize(); k++){

                color = colorMapHFNum_queue.at(i)->data()->cell(j, k);
                color *=gainMultiplier;

                colorMapHFNum_queue.at(i)->data()->setCell(j, k, color);
            }
        }
    }
    gainMultiplier = pow(10.0,(newMainGainStbd/20.0));
    for(int i=0; i < colorMapHFNum_queue.size(); i++){
        for(int j=colorMapHFNum_queue.at(i)->data()->keySize()/2; j < colorMapHFNum_queue.at(i)->data()->keySize(); j++){
            for(int k=0; k < colorMapHFNum_queue.at(i)->data()->valueSize(); k++){

                color = colorMapHFNum_queue.at(i)->data()->cell(j, k);
                color *=gainMultiplier;

                colorMapHFNum_queue.at(i)->data()->setCell(j, k, color);
            }
        }
    }
    customPlotHF->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: plot_data_changeGainOnDepth(double newMainGain){
    //if (abs(newMainGain) < 0.001)       return;

    double gainMultiplier = pow(10.0,(newMainGain/20.0));
    double color = 0;
    if (depthMap == nullptr)      return;
    for(int i=0; i < depthMapNum_queue.size(); i++){
        for(int j=0; j < depthMapNum_queue.at(i)->data()->keySize(); j++){
            for(int k=0; k < depthMapNum_queue.at(i)->data()->valueSize(); k++){

                color = depthMapNum_queue.at(i)->data()->cell(j, k);
                color *=gainMultiplier;

                depthMapNum_queue.at(i)->data()->setCell(j, k, color);
            }
        }
    }
    depthPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: plot_data_showGap(bool show) {
    double angle_coefficient = 0;
    if (ping->angleOrientation==90)   angle_coefficient = 0;    // Side scan
    else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
    if (!show) {
        double x = 0, y = 0;
        for(int i=colorMapNum_queue.size() - 1; i>=0; i--){ // for each colormap in queue
            bool found;
            double xCellDiff = ping->lastTraceAlt/ping->altitudeFactor / ping->res_lat_lf;
            double xStart = colorMapNum_queue.at(i)->getKeyRange(found).lower;
            double yStart = colorMapNum_queue.at(i)->getValueRange(found).lower + abs(xStart*angle_coefficient);
            int kStart, jStart;
            colorMapNum_queue.at(i)->data()->coordToCell(xStart, yStart, &jStart, &kStart);

            for(int k = 0; k < colorMapNum_queue.at(i)->data()->valueSize(); k++){
                // for each row
                for (int j = colorMapNum_queue.at(i)->data()->keySize()/2 - xCellDiff; j >= 0; j--) {
//                    colorMapNum_queue.at(i)->data()->cellToCoord(jStart, kStart, &x, &y);
                    double val = colorMapNum_queue.at(i)->data()->cell(j, k);
                    colorMapNum_queue.at(i)->data()->setCell(j + xCellDiff, k, val);
                }
                for (int j = colorMapNum_queue.at(i)->data()->keySize()/2 + xCellDiff - 1; j < colorMapNum_queue.at(i)->data()->keySize(); j++) {
//                    colorMapNum_queue.at(i)->data()->cellToCoord(jStart, kStart, &x, &y);
                    double val = colorMapNum_queue.at(i)->data()->cell(j, k);
                    colorMapNum_queue.at(i)->data()->setCell(j - xCellDiff, k, val);
                }
                for (int j = 0; j < xCellDiff; j++) {
                    colorMapNum_queue.at(i)->data()->setCell(j, k, 0);
                }
                for (int j = colorMapNum_queue.at(i)->data()->keySize() - xCellDiff; j < colorMapNum_queue.at(i)->data()->keySize(); j++) {
                    colorMapNum_queue.at(i)->data()->setCell(j, k, 0);
                }
            }
        }
        customPlotLF->replot();

        return;
        for(int i=colorMapNum_queue.size() - 1; i>=0; i--){ // for each colormap in queue
            for(int k = 0; k < colorMapNum_queue.at(i)->data()->valueSize()/2; k++){
                // for each yCell
                for (int j = colorMapNum_queue.at(i)->data()->keySize()/2; j > 0; j--) { // for each xCell
                    double x = 0, y = 0;
                    colorMapNum_queue.at(i)->data()->cellToCoord(j, k, &x, &y);
                    if (abs(x) < ping->lastTraceAlt)     continue;
                    double xDiff = abs(x) - ping->lastTraceAlt;
                    double yNew = y-abs(xDiff*angle_coefficient);
                    double val = colorMapNum_queue.at(i)->data()->cell(j, k);
                    colorMapNum_queue.at(i)->data()->setData(x + xDiff, yNew, val);
                }
//                customPlotLF->replot(QCustomPlot::rpImmediateRefresh);
            }
        }
    } else {
        double x = 0, y = 0;
        for(int i=colorMapNum_queue.size() - 1; i>=0; i--){ // for each colormap in queue
            bool found;
            double xCellDiff = ping->lastTraceAlt/ping->altitudeFactor / ping->res_lat_lf;
            double xStart = colorMapNum_queue.at(i)->getKeyRange(found).lower;
            double yStart = colorMapNum_queue.at(i)->getValueRange(found).lower + abs(xStart*angle_coefficient);
            int kStart, jStart;
            colorMapNum_queue.at(i)->data()->coordToCell(xStart, yStart, &jStart, &kStart);

            for(int k = 0; k < colorMapNum_queue.at(i)->data()->valueSize(); k++){
                // for each row
                for (int j = xCellDiff; j < colorMapNum_queue.at(i)->data()->keySize()/2; j++) {
//                    colorMapNum_queue.at(i)->data()->cellToCoord(jStart, kStart, &x, &y);
                    double val = colorMapNum_queue.at(i)->data()->cell(j, k);
                    colorMapNum_queue.at(i)->data()->setCell(j - xCellDiff, k, val);
                }
                for (int j = colorMapNum_queue.at(i)->data()->keySize() - xCellDiff - 1; j > colorMapNum_queue.at(i)->data()->keySize()/2; j--) {
//                    colorMapNum_queue.at(i)->data()->cellToCoord(jStart, kStart, &x, &y);
                    double val = colorMapNum_queue.at(i)->data()->cell(j, k);
                    colorMapNum_queue.at(i)->data()->setCell(j + xCellDiff, k, val);
                }
                for (int j = colorMapNum_queue.at(i)->data()->keySize()/2 - xCellDiff; j < colorMapNum_queue.at(i)->data()->keySize()/2 + xCellDiff; j++) {
                    colorMapNum_queue.at(i)->data()->setCell(j, k, 0);
                }
            }
        }
        customPlotLF->replot();
    }
}

void MainWindow :: plot_data_showGap2(bool show) {
    double angle_coefficient = 0;
    if (ping->angleOrientation==90)   angle_coefficient = 0;    // Side scan
    else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
    if (!show) {
        double x = 0, y = 0;
        int pingStart = dataMap.firstKey(), pingEnd = dataMap.lastKey();  // Find the beginning and end of the plot
        tools->removeAllTargetIcons();
        target_ShowIconsOnPlot();

        // Move through the plot data by going through each ping
        for (int m = pingStart; m <= pingEnd; m++) {
            PingMapItem item = dataMap.findItem(m);
            if (!item.isValid)    return;

            // Find the number of samples needed to move the plot
            double xCellDiffDouble = item.altitude*41/item.altitudeFactor; // / ping->res_lat_lf;
            int xCellDiff = xCellDiffDouble;
            double tanDegree = 0;
            if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
            QCPColorMap *cMap1 = colorMapNum_queue.first(), *cMap2 = colorMapNum_queue.first();
            bool found;

            // Find the correct colormap needed for the given ping
            int c1 = colorMapNum_queue.length()-1, c2 = colorMapNum_queue.length()-1;
            for (c1 = colorMapNum_queue.length()-1; c1 >= 0; c1--) {
                if (item.yCoord > colorMapNum_queue.at(c1)->getValueRange(found).lower) {
                    cMap1 = colorMapNum_queue.at(c1);
                    cMap2 = cMap1;
                    c2 = c1;
                    break;
                }
            }
            if (c1 < 0)     continue;

            // Find the top boundary of the colormap
            double yBound = colorMapNum_queue.last()->getValueRange(found).upper;
            if (colorMapNum_queue.length() > c1+1)
                yBound = colorMapNum_queue.at(c1+1)->getValueRange(found).lower;
            double x1, x2, y1, y2;

            if (m == pingStart) {
                double pingsToRemoveDouble = ping->samplesToPlotLF * ping->res_lat_lf * tanDegree / ping->res_fwd;
                int pingsToRemove = pingsToRemoveDouble;
                for (int i = 1; i < pingsToRemoveDouble; i++) {
                    for (int j = ping->samplesToPlotLF; j >= 0; j--) {
                        x1 = ((j) * ping->res_lat_lf);
                        y1 = (x1*tanDegree) + item.yCoord - i*ping->res_fwd;

                        if (y1 < item.yCoord - 0.5*ping->res_fwd)   break;

                        cMap1->data()->setData(-x1, y1, 0);
                        cMap1->data()->setData(x1, y1, 0);
                    }
                }
            }

            // For each sample, from the first sample after the gap to the last sample,
            //    move all data inwards
            for (int j = xCellDiff; j < ping->samplesToPlotLF; j++) {
                x1 = ((j) * ping->res_lat_lf);
                y1 = (x1*tanDegree) + item.yCoord;
                x2 = ((j - xCellDiff) * ping->res_lat_lf);
                y2 = (x2*tanDegree) + item.yCoord;
                if (y1 + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > c1+1)
                        cMap1 = colorMapNum_queue.at(c1+1);
                } else {
                    cMap1 = colorMapNum_queue.at(c1);
                }

                if (y2 + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > c2+1)
                        cMap2 = colorMapNum_queue.at(c2+1);
                } else {
                    cMap2 = colorMapNum_queue.at(c2);
                }

                double val = cMap1->data()->data(-x1, y1);
                cMap2->data()->setData(-x2, y2, val);
                val = cMap1->data()->data(x1, y1);
                cMap2->data()->setData(x2, y2, val);
            }
            // Fill the remaining samples as black
            for (int j = ping->samplesToPlotLF - xCellDiff; j < ping->samplesToPlotLF; j++) {
                x2 = ((j) * ping->res_lat_lf);
                y2 = (x2*tanDegree) + item.yCoord;
                if (y2 + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > c2+1)
                        cMap2 = colorMapNum_queue.at(c2+1);
                } else {
                    cMap2 = colorMapNum_queue.at(c2);
                }

                cMap2->data()->setData(-x2, y2, 0);
                cMap2->data()->setData(x2, y2, 0);
            }
        }

        if (flag.PlotHfSidescan) {
            for (int m = pingStart; m <= pingEnd; m++) {
                PingMapItem item = dataMap.findItem(m);
                if (!item.isValid)    return;

                // Find the number of samples needed to move the plot
                double xCellDiffDouble = item.altitude*41/item.altitudeFactor; // / ping->res_lat_lf;
                int xCellDiff = xCellDiffDouble;
                double tanDegree = 0;
                if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
                QCPColorMap *cMap1 = colorMapHFNum_queue.first(), *cMap2 = colorMapHFNum_queue.first();
                bool found;

                // Find the correct colormap needed for the given ping
                int c1 = colorMapHFNum_queue.length()-1, c2 = colorMapHFNum_queue.length()-1;
                for (c1 = colorMapHFNum_queue.length()-1; c1 >= 0; c1--) {
                    if (item.yCoord > colorMapHFNum_queue.at(c1)->getValueRange(found).lower) {
                        cMap1 = colorMapHFNum_queue.at(c1);
                        cMap2 = cMap1;
                        c2 = c1;
                        break;
                    }
                }
                if (c1 < 0)     continue;

                // Find the top boundary of the colormap
                double yBound = colorMapHFNum_queue.last()->getValueRange(found).upper;
                if (colorMapHFNum_queue.length() > c1+1)
                    yBound = colorMapHFNum_queue.at(c1+1)->getValueRange(found).lower;
                double x1, x2, y1, y2;

                if (m == pingStart) {
                    double pingsToRemoveDouble = ping->samplesToPlotHF * ping->res_lat_hf * tanDegree / ping->res_fwd;
                    int pingsToRemove = pingsToRemoveDouble;
                    for (int i = 1; i < pingsToRemoveDouble; i++) {
                        for (int j = ping->samplesToPlotHF; j >= 0; j--) {
                            x1 = ((j) * ping->res_lat_hf);
                            y1 = (x1*tanDegree) + item.yCoord - i*ping->res_fwd;

                            if (y1 < item.yCoord - 0.5*ping->res_fwd)   break;

                            cMap1->data()->setData(-x1, y1, 0);
                            cMap1->data()->setData(x1, y1, 0);
                        }
                    }
                }

                // For each sample, from the first sample after the gap to the last sample,
                //    move all data inwards
                for (int j = xCellDiff; j < ping->samplesToPlotHF; j++) {
                    x1 = ((j) * ping->res_lat_hf);
                    y1 = (x1*tanDegree) + item.yCoord;
                    x2 = ((j - xCellDiff) * ping->res_lat_hf);
                    y2 = (x2*tanDegree) + item.yCoord;
                    if (y1 + 0.5*ping->res_fwd > yBound) {
                        if (colorMapHFNum_queue.length() > c1+1)
                            cMap1 = colorMapHFNum_queue.at(c1+1);
                    } else {
                        cMap1 = colorMapHFNum_queue.at(c1);
                    }

                    if (y2 + 0.5*ping->res_fwd > yBound) {
                        if (colorMapHFNum_queue.length() > c2+1)
                            cMap2 = colorMapHFNum_queue.at(c2+1);
                    } else {
                        cMap2 = colorMapHFNum_queue.at(c2);
                    }

                    double val = cMap1->data()->data(-x1, y1);
                    cMap2->data()->setData(-x2, y2, val);
                    val = cMap1->data()->data(x1, y1);
                    cMap2->data()->setData(x2, y2, val);
                }
                // Fill the remaining samples as black
                for (int j = ping->samplesToPlotHF - xCellDiff; j < ping->samplesToPlotHF; j++) {
                    x2 = ((j) * ping->res_lat_hf);
                    y2 = (x2*tanDegree) + item.yCoord;
                    if (y2 + 0.5*ping->res_fwd > yBound) {
                        if (colorMapHFNum_queue.length() > c2+1)
                            cMap2 = colorMapHFNum_queue.at(c2+1);
                    } else {
                        cMap2 = colorMapHFNum_queue.at(c2);
                    }

                    cMap2->data()->setData(-x2, y2, 0);
                    cMap2->data()->setData(x2, y2, 0);
                }
            }
        }
    } else {
        double x = 0, y = 0;
        int pingStart = dataMap.firstKey(), pingEnd = dataMap.lastKey();
        tools->removeAllTargetIcons();
        target_ShowIconsOnPlot();

        for (int m = pingStart; m <= pingEnd; m++) {
            PingMapItem item = dataMap.findItem(m);
            if (!item.isValid)    return;

            double xCellDiffDouble = item.altitude*41/item.altitudeFactor; // / ping->res_lat_lf;
            int xCellDiff = xCellDiffDouble;
            double tanDegree = 0;
            if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
            QCPColorMap *cMap1 = colorMapNum_queue.first(), *cMap2 = colorMapNum_queue.first();
            bool found;
            int c1 = colorMapNum_queue.length()-1, c2 = colorMapNum_queue.length()-1;
            for (c1 = colorMapNum_queue.length()-1; c1 >= 0; c1--) {
                if (item.yCoord > colorMapNum_queue.at(c1)->getValueRange(found).lower) {
                    cMap1 = colorMapNum_queue.at(c1);
                    cMap2 = cMap1;
                    c2 = c1;
                    break;
                }
            }
            if (c1 < 0)     continue;
            double yBound = colorMapNum_queue.last()->getValueRange(found).upper;
            if (colorMapNum_queue.length() > c1+1)
                yBound = colorMapNum_queue.at(c1+1)->getValueRange(found).lower;
            double x1, x2, y1, y2;

            if (m == pingStart) {
                double pingsToRemoveDouble = ping->samplesToPlotLF * ping->res_lat_lf * tanDegree / ping->res_fwd;
                int pingsToRemove = pingsToRemoveDouble;
                for (int i = 1; i < pingsToRemoveDouble; i++) {
                    for (int j = ping->samplesToPlotLF; j >= 0; j--) {
                        x1 = ((j) * ping->res_lat_lf);
                        y1 = (x1*tanDegree) + item.yCoord - i*ping->res_fwd;

                        if (y1 < item.yCoord - 0.5*ping->res_fwd)   break;

                        cMap1->data()->setData(-x1, y1, 0);
                        cMap1->data()->setData(x1, y1, 0);
                    }
                }
            }

            for (int j = ping->samplesToPlotLF - 1; j >= xCellDiff; j--) {
                x1 = ((j - xCellDiff) * ping->res_lat_lf);
                y1 = (x1*tanDegree) + item.yCoord;
                x2 = ((j) * ping->res_lat_lf);
                y2 = (x2*tanDegree) + item.yCoord;
                if (y1 + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > c1+1)
                        cMap1 = colorMapNum_queue.at(c1+1);
                } else {
                    cMap1 = colorMapNum_queue.at(c1);
                }

                if (y2 + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > c2+1)
                        cMap2 = colorMapNum_queue.at(c2+1);
                } else {
                    cMap2 = colorMapNum_queue.at(c2);
                }

//                    colorMapNum_queue.at(i)->data()->cellToCoord(jStart, kStart, &x, &y);
                double val = cMap1->data()->data(-x1, y1);
                cMap2->data()->setData(-x2, y2, val);
                val = cMap1->data()->data(x1, y1);
                cMap2->data()->setData(x2, y2, val);
            }
            for (int j = xCellDiff-1; j >= 0; j--) {
                x2 = ((j) * ping->res_lat_lf);
                y2 = (x2*tanDegree) + item.yCoord;
                if (y2 + 0.5*ping->res_fwd > yBound) {
                    if (colorMapNum_queue.length() > c2+1)
                        cMap2 = colorMapNum_queue.at(c2+1);
                } else {
                    cMap2 = colorMapNum_queue.at(c2);
                }

                cMap2->data()->setData(-x2, y2, 0);
                cMap2->data()->setData(x2, y2, 0);
            }
        }

        if (flag.PlotHfSidescan) {
            for (int m = pingStart; m <= pingEnd; m++) {
                PingMapItem item = dataMap.findItem(m);
                if (!item.isValid)    return;

                double xCellDiffDouble = item.altitude*41/item.altitudeFactor; // / ping->res_lat_hf;
                int xCellDiff = xCellDiffDouble;
                double tanDegree = 0;
                if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
                QCPColorMap *cMap1 = colorMapHFNum_queue.first(), *cMap2 = colorMapHFNum_queue.first();
                bool found;
                int c1 = colorMapHFNum_queue.length()-1, c2 = colorMapHFNum_queue.length()-1;
                for (c1 = colorMapHFNum_queue.length()-1; c1 >= 0; c1--) {
                    if (item.yCoord > colorMapHFNum_queue.at(c1)->getValueRange(found).lower) {
                        cMap1 = colorMapHFNum_queue.at(c1);
                        cMap2 = cMap1;
                        c2 = c1;
                        break;
                    }
                }
                if (c1 < 0)     continue;
                double yBound = colorMapHFNum_queue.last()->getValueRange(found).upper;
                if (colorMapHFNum_queue.length() > c1+1)
                    yBound = colorMapHFNum_queue.at(c1+1)->getValueRange(found).lower;
                double x1, x2, y1, y2;

                if (m == pingStart) {
                    double pingsToRemoveDouble = ping->samplesToPlotHF * ping->res_lat_hf * tanDegree / ping->res_fwd;
                    int pingsToRemove = pingsToRemoveDouble;
                    for (int i = 1; i < pingsToRemoveDouble; i++) {
                        for (int j = ping->samplesToPlotHF; j >= 0; j--) {
                            x1 = ((j) * ping->res_lat_hf);
                            y1 = (x1*tanDegree) + item.yCoord - i*ping->res_fwd;

                            if (y1 < item.yCoord - 0.5*ping->res_fwd)   break;

                            cMap1->data()->setData(-x1, y1, 0);
                            cMap1->data()->setData(x1, y1, 0);
                        }
                    }
                }

                for (int j = ping->samplesToPlotHF - 1; j >= xCellDiff; j--) {
                    x1 = ((j - xCellDiff) * ping->res_lat_hf);
                    y1 = (x1*tanDegree) + item.yCoord;
                    x2 = ((j) * ping->res_lat_hf);
                    y2 = (x2*tanDegree) + item.yCoord;
                    if (y1 + 0.5*ping->res_fwd > yBound) {
                        if (colorMapHFNum_queue.length() > c1+1)
                            cMap1 = colorMapHFNum_queue.at(c1+1);
                    } else {
                        cMap1 = colorMapHFNum_queue.at(c1);
                    }

                    if (y2 + 0.5*ping->res_fwd > yBound) {
                        if (colorMapHFNum_queue.length() > c2+1)
                            cMap2 = colorMapHFNum_queue.at(c2+1);
                    } else {
                        cMap2 = colorMapHFNum_queue.at(c2);
                    }

                    double val = cMap1->data()->data(-x1, y1);
                    cMap2->data()->setData(-x2, y2, val);
                    val = cMap1->data()->data(x1, y1);
                    cMap2->data()->setData(x2, y2, val);
                }
                for (int j = xCellDiff-1; j >= 0; j--) {
                    x2 = ((j) * ping->res_lat_hf);
                    y2 = (x2*tanDegree) + item.yCoord;
                    if (y2 + 0.5*ping->res_fwd > yBound) {
                        if (colorMapHFNum_queue.length() > c2+1)
                            cMap2 = colorMapHFNum_queue.at(c2+1);
                    } else {
                        cMap2 = colorMapHFNum_queue.at(c2);
                    }

                    cMap2->data()->setData(-x2, y2, 0);
                    cMap2->data()->setData(x2, y2, 0);
                }
            }
        }
    }
    plot_replotAll();
}

/**
 * @brief MainWindow::plotTrace
 * This function/slot is called everytime a new trace is received via TCP.
 * It plots a trace on the main plotting area,and calls functions which computes
 * depth and plots depth on the depth plot and update the displays/readouts.
 */
void MainWindow :: plot_data_addTrace() {
    if ((colorMap == nullptr && ping->rangeLf_int > 0) || (colorMapHF == nullptr && ping->rangeHf_int > 0)) {
        plot_data_addColorMap();
        flag.setupFirstColorMap = true;
    }
    plot_range_moveRanges();
    //if signal plot is active, then clear the portside_graph data.
    if(Format->isSignalPlotOpen){
        ui->portSide_graph->graph()->data()->clear();
        if (flag.PlotAngleScanColorMap) {
            ui->altPlot->graph()->data()->clear();
        }
    }

    if(flag.graphDialogActive) {
        graphWindowInstance->removeData1();
        graphWindowInstance->removeData2();
    }

    double tanDegree = 0;
    if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);

    double xCoord1 = 0, yCoord1 = 0;
    double tvgGainLfPort, tvgGainLfStbd;
    float starboardSideColor1, portSideColor1;
    int sampleNumberPort = 1;
    double sampleNumberDouble = 1;
    ping->lastTracePos += ping->res_fwd;
    ping->y_UpperBound = ping->lastTracePos + ping->res_fwd*0.5;

    if (flag.PlotAngleScan && !flag.PlotAngleScanColorMap) {
        bottomTrack_addTrackedPoint(1);
    }
    if (!flag.showGap) {
        sampleNumberDouble = ping->lastTraceAlt/ping->res_lat_lf/ping->altitudeFactor;//(kalmanSampleNum1 + kalmanSampleNum2)/2;
        sampleNumberPort = sampleNumberDouble;
        if (ping->lastTraceAlt < 1 || ping->lastTraceAlt > 1000) {
            sampleNumberPort = 1;
        }
        if (flag.PlotHfSidescan && !flag.PlotAngleScan) {
            sampleNumberPort = 1;
        }
    }

    if (ping->Oct31ASTest) {
        bottomTrack_addTrackedPoint(2);
        flag.PlotAngleScan = 1;
        flag.PlotHfSidescan = 0;
    } else {
        if (state.GUI_MODE != 0) {
            flag.PlotAngleScan = ping->recieveDepth;
            flag.PlotHfSidescan = flag.useDualFrequency;
        }
    }

    // Plot LF
    for(int sampleNumber=sampleNumberPort; sampleNumber<= ping->samplesToPlotLF; sampleNumber++) {//for loop navigates through data lists (e.g starboardSideData1)
        xCoord1 = ((sampleNumber - sampleNumberPort) * ping->res_lat_lf);
        yCoord1 = (xCoord1*tanDegree) + ping->lastTracePos;

        //plot live data
        if(state.GUI_MODE) {
            if (sampleNumber == 0) sampleNumber++; //Enabling hide Kalman when Kalman is 0 will cause a crash //This ensures that if this is the case no data is hidden
            portSideColor1 = ping->data_portLf.at(sampleNumber-1);//sets color of portside cell
            starboardSideColor1 = ping->data_stbdLf.at(sampleNumber-1);//sets color of starboard cell
        } else {
            portSideColor1 = sampleNumber;
            starboardSideColor1 = sampleNumber;
        }

        // Add TVG
        tvgGainLfPort = (gain->calculateTvg(sampleNumber, ping->res_lat_lf, GainSettings::LfPort, false, true));
        tvgGainLfPort = pow(10.0,(tvgGainLfPort/20.0));
        tvgGainLfStbd = (gain->calculateTvg(sampleNumber, ping->res_lat_lf, GainSettings::LfStbd, false, true));
        tvgGainLfStbd = pow(10.0,(tvgGainLfStbd/20.0));
        portSideColor1 *= tvgGainLfPort;
        starboardSideColor1 *= tvgGainLfStbd;

        // Assign data to Port Side
        colorMap->data()->setData(-xCoord1, yCoord1, portSideColor1);
        // Assign data to Starboard Side
        colorMap->data()->setData(xCoord1, yCoord1, starboardSideColor1);


        if(Format->isSignalPlotOpen && Format->isOpenLf) {
            ui->portSide_graph->graph()->addData(-xCoord1,portSideColor1);
            ui->portSide_graph->graph()->addData(xCoord1,starboardSideColor1);
        }
        if(flag.graphDialogActive) {
            graphWindowInstance->addDataToPortSideGraph1(sampleNumber - 1,portSideColor1);
            graphWindowInstance->addDataToStarBoardSideGraph1(sampleNumber - 1,starboardSideColor1);
        }
    }

    if (flag.PlotHfSidescan) {
        double xCoord2 = 0, yCoord2 = 0;
        double tvgGainHfPort, tvgGainHfStbd;
        float starboardSideColor2, portSideColor2;

        for(int sampleNumber=sampleNumberPort; sampleNumber<= ping->samplesToPlotHF; sampleNumber++) {//navigates high frequency data lists
            xCoord2 = ((sampleNumber - sampleNumberPort)/*sampleNumber*/* ping->res_lat_hf);
            yCoord2 = (xCoord2*tanDegree) + ping->lastTracePos;

            if(state.GUI_MODE) {
                if (sampleNumber == 0) sampleNumber++;
                starboardSideColor2 = ping->data_stbdHf.at(sampleNumber-1);
                portSideColor2 = ping->data_portHf.at(sampleNumber-1);
            } else {
                starboardSideColor2 = sampleNumber;
                portSideColor2 = sampleNumber;
            }

            // Add TVG
            tvgGainHfPort = (gain->calculateTvg(sampleNumber, ping->res_lat_hf, GainSettings::HfPort, false, true));
            tvgGainHfPort = pow(10.0,(tvgGainHfPort/20.0));
            tvgGainHfStbd = (gain->calculateTvg(sampleNumber, ping->res_lat_hf, GainSettings::HfStbd, false, true));
            tvgGainHfStbd = pow(10.0,(tvgGainHfStbd/20.0));
            starboardSideColor2 *= tvgGainHfStbd;//apply gains to color
            portSideColor2 *= tvgGainHfPort;//apply gains to color

            // Port
            colorMapHF->data()->setData(-xCoord2, yCoord2, portSideColor2);
            // Starboard
            colorMapHF->data()->setData(xCoord2, yCoord2, starboardSideColor2);

            if(Format->isSignalPlotOpen && !Format->isOpenLf) {
                ui->portSide_graph->graph()->addData(-xCoord2,portSideColor2);
                ui->portSide_graph->graph()->addData(xCoord2,starboardSideColor2);
            }
            if(flag.graphDialogActive) {
                graphWindowInstance->addDataToStarBoardSideGraph2(sampleNumber - 1,starboardSideColor2);
                graphWindowInstance->addDataToPortSideGraph2(sampleNumber - 1,portSideColor2);
            }
        }
    }
    if (flag.PlotAngleScan) {
        double xCoord, yCoord;
        double sampleColor;
        if (flag.PlotAngleScanColorMap) {
            if (!ping->Oct31ASTest) {
                if (ping->lowFrequency == 230)      bottomTrack_addTrackedPoint(3);
                else {
                    if (ping->pingNumCurrent % 2 == 0)      bottomTrack_addTrackedPoint(4);
                    else                                bottomTrack_addTrackedPoint(5);
                }
            }
            for (int sampleNumber = 1; sampleNumber <= ping->samplesToPlotDepth; sampleNumber++) {//2500 is number of samples for depth plot, compress factor is 3
                xCoord = sampleNumber * ping->res_lat_lf * ping->altitudeFactor;
                yCoord = ping->lastTracePos;

                if (state.GUI_MODE) {
                    sampleColor = ping->data_depth.at((sampleNumber-1));//get color from board data
                } else {
                    sampleColor = sampleNumber;
                }

                GainSettings::channelType chType = GainSettings::LfPort;
                if (ping->highFrequency == ping->depthFrequency)     chType = GainSettings::HfPort;
                tvgGainLfPort = gain->calculateTvg(sampleNumber, ping->res_lat_lf, chType, false, true);
                if (chType == GainSettings::LfPort)     tvgGainLfPort -= (gain->tvg_CLfP + gain->C_lfOffset);
                else if (chType == GainSettings::HfPort)     tvgGainLfPort -= (gain->tvg_CHfP + gain->C_hfOffset);
                tvgGainLfPort = pow(10.0,(tvgGainLfPort/20.0));
                sampleColor *= tvgGainLfPort;
                sampleColor *= pow(10.0, (gain->C_depthOffset)/20.0);

                if(Format->isSignalPlotOpen){
                    if (sampleNumber > 100 && xCoord<setting.globalDepth) {
                        ui->altPlot->graph()->addData(xCoord,sampleColor);
                    }
                }

                if(flag.graphDialogActive) {
                    if (ping->Oct31ASTest) {
                        //                        if (sampleNumber < 46) {
                        //                            graphWindowInstance->addDataToPortSideGraph3(sampleNumber, starboardSideData2.at(sampleNumber+46), 0);
                        //                        } else if (sampleNumber < samplesToPlot_config1 - 44) {
                        //                            graphWindowInstance->addDataToPortSideGraph3(sampleNumber, starboardSideData2.at(sampleNumber+46), 0);
                        //                            graphWindowInstance->addDataToPortSideGraph3(sampleNumber, portSideData2.at(sampleNumber-44), 1);
                        //                        } else {
                        //                            graphWindowInstance->addDataToPortSideGraph3(sampleNumber, portSideData2.at(sampleNumber-44), 1);
                        //                        }
                        graphWindowInstance->addDataToPortSideGraph3(sampleNumber - 1, ping->data_portHf.at(sampleNumber-1), 0);
                        graphWindowInstance->addDataToPortSideGraph3(sampleNumber - 1, ping->data_stbdHf.at(sampleNumber-1), 1);
                    } else {
                        graphWindowInstance->addDataToPortSideGraph4(sampleNumber - 1, sampleColor);
                    }
                }

                if(xCoord>setting.globalDepth){
                    continue;
                }
                depthMap->data()->setData(xCoord, yCoord, sampleColor);//assign color to cell
            }
        }
        showLabelOnAltGraph(ping->lastTracePos);
    }

    plot_updatePlot();
    traceCounter1++;

    plot_data_checkToAddNewMap();
    resetPlotLists();
}

void MainWindow :: plot_data_numberOfPingsToPlot(int numberOfPingsIndex){
    /*This function is a slot to signal emitted by changes to the number of pings combobox in usermenu
      *Sets the global variable pingplot
    */

    setting.plotRate = numberOfPingsIndex*10;
    if (numberOfPingsIndex == 0)     setting.plotRate = 1;
}

/**
 * @brief MainWindow::updatePlot1
 * Refreshes the main plotting area and depth plot with trace information.
 * This function is called everytime a trace is plotted.
 */
void MainWindow :: plot_updatePlot() {
    double width = (customPlotLF->xAxis->range().upper - customPlotLF->xAxis->range().lower)/((Format->plotWidth_LfHf)*1.0);
    double height = (customPlotLF->yAxis->range().upper - customPlotLF->yAxis->range().lower)/((Format->plotHeight_Lf)*1.0);

    double xInc = width * 30.0;
    double yInc = height * 30.0;

    ping->y_boatPos = ((ping->lastTracePos -(ping->lastTraceAlt*cos(qDegreesToRadians(ping->degree)))));

    lfBoatPixmap->setVisible(true);
    lfBoatPixmap->setLayer("boatLayer");
    lfBoatPixmap->topLeft->setCoords(-xInc/2, ping->y_boatPos);
    lfBoatPixmap->bottomRight->setCoords(xInc/2, ping->y_boatPos-yInc);
    target_ShowIconsOnPlot(true);
    ChartWindowInstance->track_moveCurve(0);

    if(flag.graphDialogActive) {
        graphWindowInstance->refreshPlot1();
    }

    if(flag.PlotHfSidescan){
        ui->customPlot2->clearGraphs();

        double widthHF = (customPlotHF->xAxis->range().upper - customPlotHF->xAxis->range().lower)/((Format->plotWidth_LfHf)*1.0);
        double heightHF = (customPlotHF->yAxis->range().upper - customPlotHF->yAxis->range().lower)/((Format->plotHeight_Hf)*1.0);

        double xIncHF = widthHF * 30.0;
        double yIncHF = heightHF * 30.0;

        double boatY = ((ping->lastTracePos -(ping->lastTraceAlt*cos(qDegreesToRadians(ping->degree)))));

        hfBoatPixmap->setVisible(true);
        hfBoatPixmap->setLayer("boatLayer");
        hfBoatPixmap->topLeft->setCoords(-xIncHF/2, boatY);
        hfBoatPixmap->bottomRight->setCoords(xIncHF/2, boatY-yIncHF);

        if(flag.graphDialogActive) {
            graphWindowInstance->refreshPlot2();
        }

        plot_zoomBox_updatePosition(0);
    }

    if (state.GUI_MODE > 0) {
        QPen pen;
        QPen pen2;

        pen.setColor(Qt::red);
        pen2.setColor(Qt::yellow);
        pen.setWidth(3);
        pen2.setWidth(3);
        ui->sensor_graph->graph(0)->setPen(pen);
        ui->sensor_graph->graph(2)->setPen(pen2);
    }

    if (flag.PlotAngleScan) {
        if(Format->isSignalPlotOpen && flag.PlotAngleScanColorMap) {
            ui->altPlot->xAxis->setRange(depthPlot->xAxis->range().lower,depthPlot->xAxis->range().upper);
            ui->altPlot->yAxis->rescale();
            ui->altPlot->replot(QCustomPlot::rpQueuedReplot);
        }
        if(flag.graphDialogActive) {
            graphWindowInstance->refreshPlot2();
        }

//        if(Format->isOpen_DepthPlot) {
            line_depthTrace->setVisible(true);
            line_depthTrace->point1->setCoords(0,ping->lastTracePos);
            line_depthTrace->point2->setCoords(1,ping->lastTracePos);
            depthLegend->position->setCoords(setting.globalDepth, ping->lastTracePos);
            double dist = ping->lastTracePos - ping->y_boatPos;
            depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");
//        }

        double widthDepth = (depthPlot->xAxis->range().upper - depthPlot->xAxis->range().lower)/(depthPlot->width()*1.0);
        double heightDepth = (depthPlot->yAxis->range().upper - depthPlot->yAxis->range().lower)/(depthPlot->height()*1.0);

        double xIncDepth = widthDepth * 30.0;
        double yIncDepth = heightDepth * 30.0;

        depthBoatPixmap->setVisible(true);
        depthBoatPixmap->topLeft->setCoords(-xIncDepth/2, ping->y_boatPos);
        depthBoatPixmap->bottomRight->setCoords(xIncDepth/2, ping->y_boatPos-yIncDepth);
        line_depthBoat->setVisible(true);
        line_depthBoat->start->setCoords(0, ping->y_boatPos);
        line_depthBoat->end->setCoords(setting.globalDepth, ping->y_boatPos);
    }

//    if (Format->isOpen_sensorPlot) {
        line->setVisible(true);
        line->point1->setCoords(0,ping->lastTracePos);
        line->point2->setCoords(1,ping->lastTracePos);
//    }
    if(Format->isSignalPlotOpen == true){
        if (setting.lfHfIndependent && Format->isOpenLf) {
            ui->portSide_graph->xAxis->setRange(ui->customPlot->xAxis->range().lower,ui->customPlot->xAxis->range().upper);
        }
        if (setting.lfHfIndependent && Format->isOpenHf) {
            ui->portSide_graph->xAxis->setRange(ui->customPlot2->xAxis->range().lower,ui->customPlot2->xAxis->range().upper);
        }
        ui->portSide_graph->yAxis->rescale();
        ui->portSide_graph->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: plot_configureYLabels() {
    return;
    // This function is not used anymore, but is still here just in case we need to use y-labels again
    if (Format->isOpen_DepthPlot) {
        ui->depthCPlot->yAxis->setTickLabels(false);
        ui->sensor_graph->yAxis->setTickLabels(false);
        ui->customPlot->yAxis->setTickLabels(false);
        ui->customPlot2->yAxis->setTickLabels(false);
    }
    else if (Format->isOpen_sensorPlot) {
        ui->depthCPlot->yAxis->setTickLabels(false);
        ui->sensor_graph->yAxis->setTickLabels(false);
        ui->customPlot->yAxis->setTickLabels(false);
        ui->customPlot2->yAxis->setTickLabels(false);
    }
    else {
        ui->depthCPlot->yAxis->setTickLabels(false);
        ui->sensor_graph->yAxis->setTickLabels(false);
        ui->customPlot->yAxis->setTickLabels(false);
        ui->customPlot2->yAxis->setTickLabels(false);
    }
}

void MainWindow :: plot_configureXLabels() {
    customPlotHF->xAxis2->setTickLabels(true);
    customPlotLF->xAxis2->setTickLabels(true);
    customPlotHF->xAxis->setTickLabels(false);
    customPlotLF->xAxis->setTickLabels(false);
    customPlotLF->xAxis2->setTickLabelSide(QCPAxis::lsOutside);
    customPlotHF->xAxis2->setTickLabelSide(QCPAxis::lsOutside);
    customPlotLF->axisRect()->setAutoMargins(QCP::msLeft|QCP::msTop|QCP::msRight);
    customPlotLF->axisRect()->setMargins(QMargins(0,0,0,0));
    customPlotHF->axisRect()->setAutoMargins(QCP::msLeft|QCP::msTop|QCP::msRight);
    customPlotHF->axisRect()->setMargins(QMargins(0,0,0,0));
//    customPlotLF->axisRect()->setAutoMargins(QCP::msAll);
//    customPlotHF->axisRect()->setAutoMargins(QCP::msAll);

    /*if (flag.PlotHfSidescan && Format->isOpenHf && Format->isOpenLf) {
        // Hide xaxis labels and remove bottom padding
        customPlotLF->axisRect()->setAutoMargins(QCP::msLeft|QCP::msTop|QCP::msRight);
        customPlotLF->axisRect()->setMargins(QMargins(0,0,0,0));
        customPlotLF->xAxis->setTickLabelSide(QCPAxis::lsInside);
        customPlotLF->xAxis->setTickLabels(true);

        // Remove top padding from HF
        customPlotHF->xAxis2->setTickLabelSide(QCPAxis::lsInside);
        customPlotHF->xAxis2->setTickLabels(true);
        customPlotHF->xAxis2->setLabelPadding(0);
        customPlotHF->xAxis2->setOffset(0);
        customPlotHF->axisRect()->setAutoMargins(QCP::msLeft|QCP::msBottom|QCP::msRight);
        customPlotHF->axisRect()->setMargins(QMargins(0,0,0,0));
    }
    else if (flag.PlotHfSidescan && Format->isOpenHf && !Format->isOpenLf) {
        customPlotHF->xAxis2->setTickLabels(true);
        customPlotHF->xAxis2->setTickLabelSide(QCPAxis::lsOutside);
    }
    else if (!Format->isOpenHf && Format->isOpenLf) {
        customPlotHF->xAxis2->setTickLabels(false);
        customPlotLF->xAxis->setTickLabels(true);
        customPlotLF->xAxis->setTickLabelSide(QCPAxis::lsOutside);
    }*/
    plot_replotAll(1);

    if (customPlotLF != nullptr) {
        // customPlotLF != nullptr is used to prevent crashing on start
        if (Format->isOpenLf) {
            Format->plotWidth_LfHf = Format->widthLfHf - customPlotLF->axisRect()->margins().left() - customPlotLF->axisRect()->margins().right();
            Format->plotHeight_Lf = Format->heightLf - customPlotLF->axisRect()->margins().top() - customPlotLF->axisRect()->margins().bottom();
        }
        if (Format->isOpenHf) {
            Format->plotWidth_LfHf = Format->widthLfHf - customPlotHF->axisRect()->margins().left() - customPlotHF->axisRect()->margins().right();
            Format->plotHeight_Hf = Format->heightHf - customPlotHF->axisRect()->margins().top() - customPlotHF->axisRect()->margins().bottom();
        }
        if (Format->isOpen_DepthPlot)
            Format->plotHeight_depth = depthPlot->height() - depthPlot->axisRect()->margins().top() - depthPlot->axisRect()->margins().bottom();
        if (Format->isOpen_sensorPlot)
            Format->plotHeight_sensor = ui->sensor_graph->height() - ui->sensor_graph->axisRect()->margins().top() - ui->sensor_graph->axisRect()->margins().bottom();
    }
}

void MainWindow :: plot_range_moveRanges() {
    // Update QCustomPlot ranges with the newest ping
    // If 10 pings were plotted, then the y-axis will move up 10 pings
    static double previousTracePos = 0;

    crosshair_hf.setVisible(false);
    crosshair_lf.setVisible(false);

    // Move all sonar plots up by the number of pings plotted since last time this function was called
    customPlotLF->yAxis->moveRange(ping->lastTracePos-previousTracePos);
    customPlotHF->yAxis->moveRange(ping->lastTracePos-previousTracePos);
    ui->sensor_graph->yAxis->moveRange(ping->lastTracePos-previousTracePos);
    depthPlot->yAxis->moveRange(ping->lastTracePos-previousTracePos);

    bool found;
    double roll_low = ui->sensor_graph->graph(0)->getValueRange(found).lower;
    double roll_high = ui->sensor_graph->graph(0)->getValueRange(found).upper;
    double pitch_low = ui->sensor_graph->graph(1)->getValueRange(found).lower;
    double pitch_high = ui->sensor_graph->graph(1)->getValueRange(found).upper;
    double yaw_low = ui->sensor_graph->graph(2)->getValueRange(found).lower;
    double yaw_high = ui->sensor_graph->graph(2)->getValueRange(found).upper;
    double range_low = std :: min(roll_low, pitch_low);
    range_low = std::min(range_low, yaw_low);
    double range_high = std :: max(roll_high, pitch_high);
    range_high = std :: max(range_high, yaw_high);
    ui->sensor_graph->xAxis->setRange(range_low,range_high);

    depthPlot->xAxis->setRange(-2, setting.globalDepth);

    if (flag.zoomLfFixed && setting.zoomMagnitude_lf < 0.01) {
        int width = (Format->plotWidth_LfHf);
        int height = (Format->plotHeight_Lf);
        double xMax = ping->rangeLf_double;
        double zoomMagnification = 1;
        if (setting.zoomMagnitude_lf > 0.01)
            zoomMagnification = setting.zoomMagnitude_lf;
        else if (state.GUI_MODE == 2 && !flag.showGap) {
            // "Auto" zoom mode
            xMax -= ping->lastTraceAlt / ping->altitudeFactor;
        }
        double updatedView = 2*xMax * (height*1.0) / (width*1.0) / zoomMagnification;
        double yView2 = ping->lastTracePos - setting.zoomStepLF;

        if (Format->isOpenLf) {
            customPlotLF->yAxis->setRange(yView2 - updatedView, yView2);
        } else {
            customPlotLF->yAxis->setRange(yView2 - 5, yView2);
        }
        customPlotLF->xAxis->setRange(-xMax/zoomMagnification, xMax/zoomMagnification);
//        plot_updateLfZoomLegendText("");

        // Update all other plots accordingly
        QCPRange newRangeY = ui->customPlot->yAxis->range();
        QCPRange newRangeX = ui->customPlot->xAxis->range();
        double updatedY1 = newRangeY.lower;
        double updatedY2 = newRangeY.upper;
        double updatedX1 = newRangeX.lower;
        double updatedX2 = newRangeX.upper;

        // Find the density to determine ratio of pixels to one meter
        double yDiff_depth = 20, yDiff_sensor = 20;//
        if (width > 0) {
            yDiff_depth = 2*xMax * (Format->plotHeight_depth*1.0) / (width*1.0) / zoomMagnification;
            yDiff_sensor = 2*xMax * (Format->plotHeight_sensor*1.0) / (width*1.0) / zoomMagnification;
        }

        ui->sensor_graph->yAxis->setRange(updatedY2 - yDiff_sensor, updatedY2);
        depthPlot->yAxis->setRange(updatedY2 - yDiff_depth, updatedY2);

        if(Format->isSignalPlotOpen) {
            ui->portSide_graph->xAxis->setRange(updatedX1,updatedX2);
            ui->portSide_graph->yAxis->rescale();
        }
    }
    if (flag.zoomHfFixed && setting.zoomMagnitude_hf < 0.01) {
        double xMax = ping->rangeHf_double;
        double zoomMagnification = 1;
        if (setting.zoomMagnitude_hf > 0.01)
            zoomMagnification = setting.zoomMagnitude_hf;
        else if (state.GUI_MODE == 2 && !flag.showGap) {
            xMax -= ping->lastTraceAlt / ping->altitudeFactor;
        }
        double rangeDiffHf = plot_range_determineYRange(2.0*xMax,
                                             Format->plotWidth_LfHf,
                                             Format->plotHeight_Hf)/zoomMagnification;
        double yView2HF = ping->lastTracePos - setting.zoomStepHF;

        if (Format->isOpenHf){
            customPlotHF->yAxis->setRange(yView2HF - rangeDiffHf, yView2HF);
        } else {
            customPlotHF->yAxis->setRange(yView2HF - 5, yView2HF);
        }
        customPlotHF->xAxis->setRange(-xMax/zoomMagnification, xMax/zoomMagnification);
//        plot_updateHfZoomLegendText("");

        // Update all other plots accordingly
        QCPRange newRangeY = ui->customPlot2->yAxis->range();
        QCPRange newRangeX = ui->customPlot2->xAxis->range();
        double updatedY1 = newRangeY.lower;
        double updatedY2 = newRangeY.upper;
        double updatedX1 = newRangeX.lower;
        double updatedX2 = newRangeX.upper;

        double density = 0;
        if (Format->plotHeight_Hf > 0)  density = (updatedY2 - updatedY1) / (Format->plotHeight_Hf * 1.0);

        if (setting.lfHfIndependent && Format->isOpenHf) {
            ui->sensor_graph->yAxis->setRange(updatedY1,updatedY2);
            ui->portSide_graph->xAxis->setRange(updatedX1,updatedX2);
            ui->portSide_graph->yAxis->rescale();
        }
    }

    previousTracePos = ping->lastTracePos;
    plot_icons_updateCompassIcon();

    crop_UpdateMarkers(0, ping->lastTracePos - ping->res_fwd);
}

void MainWindow :: plot_range_unstretchLfHfPlots() {
    // When the plots move, either from moving the splitter windows or
    // a window resize, the LF and HF plots stretch. Meaning that
    // the ratio between xAxis and yAxis range are not 1m:1m
    // This function helps to "unstretch" it to restore it back to 1m:1m
    double upperRange = customPlotLF->yAxis->range().upper;
    double rangeDiffLf = plot_range_determineYRange(customPlotLF->xAxis->range().upper - customPlotLF->xAxis->range().lower,
                                         Format->plotWidth_LfHf,
                                         Format->plotHeight_Lf);
    double rangeDiff = abs((upperRange - rangeDiffLf) - customPlotLF->yAxis->range().lower);

    if (flag.PlotHfSidescan && Format->isOpenHf) {
        // else, if HF is plotting, then unstretch HF too
        double upperRangeHf = customPlotHF->yAxis->range().upper;
        double rangeDiffHf = plot_range_determineYRange(customPlotHF->xAxis->range().upper - customPlotHF->xAxis->range().lower,
                                             Format->plotWidth_LfHf,
                                             Format->plotHeight_Hf);

//        if (abs((upperRangeHf - rangeDiffHf) - customPlotHF->yAxis->range().lower) < 0.01)  return;

        double ratioNewToOld = rangeDiffHf / (customPlotHF->yAxis->range().upper - customPlotHF->yAxis->range().lower);
        customPlotHF->yAxis->scaleRange(ratioNewToOld, customPlotHF->yAxis->range().upper);

        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
    }

    if (Format->isOpenLf) {
        if (flag.zoomAroundBoatLf) {
            int idealDrop = 160;
            double yDropMeters = (idealDrop*1.0) / ((Format->plotHeight_Lf) *1.0)
                    * (rangeDiffLf);
            upperRange = ping->lastTracePos + yDropMeters;
        }

        double ratioNewToOld = rangeDiffLf / (customPlotLF->yAxis->range().upper - customPlotLF->yAxis->range().lower);
        customPlotLF->yAxis->scaleRange(ratioNewToOld, customPlotLF->yAxis->range().upper);

        // Update all other plots accordingly
        flag.mouseClick = 2;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
}

double MainWindow :: plot_range_determineYRange(double xRange, double width, double height) {
    // Used for LF and HF plots to find the correct height based on x/y ranges (m)
    //  and height/width (px)
    double answer = 0;
    answer = xRange * (height*1.0) / (width*1.0);
    return answer;
}

void MainWindow :: plot_range_zoomMouseEvent(QWheelEvent* event) {
    // Function called whenever mouse wheel moved on any plot
    // This function will set the new range of the plot called
    // Then it calls plot_range_zoomUpdate() to update all other plots accordingly
    double factor;
    double wheelSteps = event->delta()/120; // a single step delta is +/-120 usually
    factor = qPow(0.9, wheelSteps);

    // Find parameters based on mouse position and plot in use
    if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf){
        if(flag.mousePressedFlagLf==1){
            flag.zoomAroundBoatLf=false;
        }

        if (flag.zoomAroundBoatLf==true){
            customPlotLF->xAxis->scaleRange(factor, 0);
            customPlotLF->yAxis->scaleRange(factor, ping->lastTracePos);
        } else {
            customPlotLF->xAxis->scaleRange(factor, customPlotLF->xAxis->pixelToCoord(event->pos().x()));
            customPlotLF->yAxis->scaleRange(factor, customPlotLF->yAxis->pixelToCoord(event->pos().y()));
        }
        flag.mouseClick = 2;

        if (flag.zoomLfFixed) {
            flag.zoomLfFixed = false;
            userWindowInstance->setCustomLfZoom();
        }
        double x = customPlotLF->xAxis->pixelToCoord(event->pos().x());
        double y = customPlotLF->yAxis->pixelToCoord(event->pos().y());
        crosshair_lf.setPosition(x, y, y -  ping->y_boatPos,
                                 customPlotLF->xAxis->range().upper,
                                 customPlotLF->yAxis->range().upper);
    }
    else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf){
        if(flag.mousePressedFlagHf==1){
            flag.zoomAroundBoatHf=false;
        }

        if (flag.zoomAroundBoatHf==true){
            customPlotHF->xAxis->scaleRange(factor, 0);
            customPlotHF->yAxis->scaleRange(factor, ping->lastTracePos);
        } else {
            customPlotHF->xAxis->scaleRange(factor, customPlotHF->xAxis->pixelToCoord(event->pos().x()));
            customPlotHF->yAxis->scaleRange(factor, customPlotHF->yAxis->pixelToCoord(event->pos().y()));
        }
        flag.mouseClick = 4;

        if (flag.zoomHfFixed) {
            flag.zoomHfFixed = false;
            userWindowInstance->setCustomHfZoom();
        }
        double x = customPlotHF->xAxis->pixelToCoord(event->pos().x());
        double y = customPlotHF->yAxis->pixelToCoord(event->pos().y());
        crosshair_hf.setPosition(x, y, y -  ping->y_boatPos,
                                 customPlotHF->xAxis->range().upper,
                                 customPlotHF->yAxis->range().upper);
    }
    else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot) {
        if (flag.zoomAroundBoatLf==true) {
            depthPlot->xAxis->scaleRange(factor, 0);
            depthPlot->yAxis->scaleRange(factor, ping->lastTracePos);
        } else {
            depthPlot->xAxis->scaleRange(factor, depthPlot->xAxis->pixelToCoord(event->pos().x()));
            depthPlot->yAxis->scaleRange(factor, depthPlot->yAxis->pixelToCoord(event->pos().y()));
        }
        flag.mouseClick = 8;

        if (flag.zoomLfFixed) {
            flag.zoomLfFixed = false;
            userWindowInstance->setCustomLfZoom();
        }
    }
    else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos())) && Format->isOpen_sensorPlot) {
        if (flag.zoomAroundBoatLf==true) {
            ui->sensor_graph->xAxis->scaleRange(factor, 0);
            ui->sensor_graph->yAxis->scaleRange(factor, ping->lastTracePos);
        } else {
            ui->sensor_graph->xAxis->scaleRange(factor, ui->sensor_graph->xAxis->pixelToCoord(event->pos().x()));
            ui->sensor_graph->yAxis->scaleRange(factor, ui->sensor_graph->yAxis->pixelToCoord(event->pos().y()));
        }
        flag.mouseClick = 16;

        if (flag.zoomLfFixed) {
            flag.zoomLfFixed = false;
            userWindowInstance->setCustomLfZoom();
        }
    }

    plot_range_zoomUpdate();
    tools->updateAllTargetSizes();
    plot_icons_resizeBoatIcon();
    plot_replotAll();
    flag.mouseClick = 0;
}

/**
 * @brief MainWindow::updateZoomRatio
 * To Update the zooming ratio displayed on the bottom left corner
 * of the main plotting area.
 */
void MainWindow :: plot_range_zoomUpdate() {
    if (flag.mouseClick == 0) return;

    if (state.GUI_MODE == 2 && state.PLAY_MODE >= 3) {

    }

    if (flag.mousePressedFlagLf == 1) {
        flag.zoomAroundBoatLf = 0;
    }
    if (flag.mousePressedFlagHf == 1) {
        flag.zoomAroundBoatHf = 0;
    }

    if (flag.mouseClick == 2 && !Format->isOpenLf)      flag.mouseClick = 4;

    bool UpdateLF = 0, UpdateHF = 0, UpdateDepth = 0, updateSensors = 0;
    QCPRange newRangeY = ui->customPlot->yAxis->range();
    QCPRange newRangeX = ui->customPlot->xAxis->range();

    if (flag.mouseClick == 2) {
        newRangeY = customPlotLF->yAxis->range();
        newRangeX = customPlotLF->xAxis->range();
        UpdateLF = 1;
    }
    else if (flag.mouseClick == 4) {
        newRangeY = customPlotHF->yAxis->range();
        newRangeX = customPlotHF->xAxis->range();
        UpdateHF = 1;
    }
    else if (flag.mouseClick == 8) {
        newRangeY = depthPlot->yAxis->range();
        newRangeX = depthPlot->xAxis->range();
        UpdateDepth = 1;
    }
    else if (flag.mouseClick == 16) {
        newRangeY = ui->sensor_graph->yAxis->range();
        newRangeX = ui->sensor_graph->xAxis->range();
        updateSensors = 1;
    }
    else {
        return;
    }

    double updatedY1 = newRangeY.lower;
    double updatedY2 = newRangeY.upper;
    double updatedX1 = newRangeX.lower;
    double updatedX2 = newRangeX.upper;

    // Update all other plot ranges
    if (UpdateLF) {
        // Find the density to determine ratio of pixels to one meter
        double density = 0;//
        if (Format->plotHeight_Lf > 0)  density = (updatedY2 - updatedY1) / (Format->plotHeight_Lf * 1.0);

        ui->sensor_graph->yAxis->setRange(updatedY2 - density*Format->plotHeight_sensor, updatedY2);
        depthPlot->yAxis->setRange(updatedY2 - density*Format->plotHeight_depth, updatedY2);

        if(Format->isSignalPlotOpen) {
            ui->portSide_graph->xAxis->setRange(updatedX1,updatedX2);
            ui->portSide_graph->yAxis->rescale();
        }
    }
    else if (UpdateHF) {
        double density = 0;
        if (Format->plotHeight_Hf > 0)  density = (updatedY2 - updatedY1) / (Format->plotHeight_Hf * 1.0);

        if (setting.lfHfIndependent && Format->isOpenHf) {
            ui->sensor_graph->yAxis->setRange(updatedY1,updatedY2);
            ui->portSide_graph->xAxis->setRange(updatedX1,updatedX2);
            ui->portSide_graph->yAxis->rescale();
        }
    }
    else if (UpdateDepth) {
        ui->sensor_graph->yAxis->setRange(updatedY1,updatedY2);
        double density = 0;
        if (Format->plotHeight_depth > 0)  density = (updatedY2 - updatedY1) / (Format->plotHeight_depth * 1.0);

        if (Format->isOpenLf) {
            double y1 = updatedY2 - density*Format->plotHeight_Lf, y2 = updatedY2;
            customPlotLF->yAxis->setRange(y1, y2);
            double xCenter = (customPlotLF->xAxis->range().lower+customPlotLF->xAxis->range().upper)/2.0;
            double pixelRatio = ((Format->plotWidth_LfHf)*1.0)/((Format->plotHeight_Lf)*1.0);
            double x1 = xCenter - (y2-y1)*pixelRatio/2.0;
            double x2 = xCenter + (y2-y1)*pixelRatio/2.0;
            customPlotLF->xAxis->setRange(x1,x2);
        }

        if(Format->isSignalPlotOpen && flag.PlotAngleScanColorMap){
            ui->altPlot->xAxis->setRange(updatedX1,updatedX2);
            ui->altPlot->yAxis->rescale();
        }
    }
    else if (updateSensors) {
        depthPlot->yAxis->setRange(updatedY1,updatedY2);
        double density = 0;
        if (Format->plotHeight_sensor > 0)  density = (updatedY2 - updatedY1) / (Format->plotHeight_sensor * 1.0);

        if (Format->isOpenLf) {
            double y1 = updatedY2 - density*Format->plotHeight_Lf, y2 = updatedY2;
            if (Format->plotHeight_Lf > 5000 || Format->plotHeight_Lf < 0)      y1 = updatedY2 - 5;
            customPlotLF->yAxis->setRange(y1, y2);
            double xCenter = (customPlotLF->xAxis->range().lower+customPlotLF->xAxis->range().upper)/2.0;
            double pixelRatio = ((Format->plotWidth_LfHf)*1.0)/((Format->plotHeight_Lf)*1.0);
            double x1 = xCenter - (y2-y1)*pixelRatio/2.0;
            double x2 = xCenter + (y2-y1)*pixelRatio/2.0;
            customPlotLF->xAxis->setRange(x1,x2);
        }

        if (Format->isSignalPlotOpen && flag.PlotAngleScanColorMap){
            ui->altPlot->xAxis->setRange(updatedX1,updatedX2);
            ui->altPlot->yAxis->rescale();
        }
        if (setting.lfHfIndependent && Format->isOpenHf) {
            double y1 = updatedY1, y2 = updatedY2;
            customPlotHF->yAxis->setRange(y1, y2);
            double xCenter = (customPlotHF->xAxis->range().lower+customPlotHF->xAxis->range().upper)/2.0;
            double pixelRatio = ((Format->plotWidth_LfHf)*1.0)/((Format->plotHeight_Hf)*1.0);
            double x1 = xCenter - (y2-y1)*pixelRatio/2.0;
            double x2 = xCenter + (y2-y1)*pixelRatio/2.0;
            customPlotHF->xAxis->setRange(x1,x2);
        }
    }

    tools->updateAllTargetSizes();
    plot_icons_resizeBoatIcon();

    // calculate zoom ratios
    if (UpdateLF || ((UpdateDepth || updateSensors))) {
        // Update LF zoom ratio if LF plot range changes at all
        plot_showUpdatedZoomLegends();

        // Get LF plot range boundaries
        plot_zoomBox_updatePosition(1);

        zoomRatioTimer.start(1000);
    }
    if (UpdateHF){
        // Update HF zoom ratio if HF plot range changes at all
        plot_showUpdatedZoomLegends();

        // Get LF plot range boundaries
        plot_zoomBox_updatePosition(1);

        zoomRatioTimer.start(1000);
    }

    plot_replotAll();
}

void MainWindow :: plot_range_hideZoomRatioLabel(){
    //this function is a slot to show zoom ratio timer

    customPlotHF->replot(QCustomPlot::rpQueuedReplot);
    customPlotLF->replot(QCustomPlot::rpQueuedReplot);
    zoomRatioTimer.stop();

    if (!Format->isZoomBoxAlwaysOn) {
//        plot_updateLfZoomLegendText("");
//        plot_updateHfZoomLegendText("");
        customPlotHF->removeItem(HfPlotViewBox);
        HfPlotViewBox = nullptr;
        customPlotLF->removeItem(LfPlotViewBox);
        LfPlotViewBox = nullptr;
    }
}

void MainWindow :: plot_updateLfHfPlots() {
    plot_configureXLabels();
    plot_range_unstretchLfHfPlots();
    plot_configureYLabels();
    plot_updateLfHfLegend(legendTxt, 0);
    plot_updateLfHfZoomLegendPosition();
    plot_icons_updateCompassIcon();
    plot_zoomBox_updatePosition(0);
}

/**
 * @brief MainWindow::replotGraphs
 * Replots all graphs
 * Usually connected to a timer during plotting real time data
 * Minimizing replots increases performance, so the timer is used to refresh the plot at a rate that the human eye can see
 * Originally we were replotting at every trace, which caused a performance impact
 */
void MainWindow :: plot_replotAll(bool requiredReplot) {
    // Replot all QCustomPlots shown on GUI
    //rpQueuedReplot is essentially a less demanding version of replot()
    QCustomPlot::RefreshPriority replotVal = QCustomPlot::rpQueuedReplot;
    if (requiredReplot == 1)    replotVal = QCustomPlot::rpQueuedRefresh;
    if (Format->isOpenLf) {
        customPlotLF->replot(replotVal);
    }
    if (Format->isOpenHf) {
        customPlotHF->replot(replotVal);
    }
    if (Format->isOpen_sensorPlot) {
        ui->sensor_graph->replot(replotVal);
    }
    if(Format->isOpen_DepthPlot) {
        depthPlot->replot(replotVal);
    }
    if(Format->isSignalPlotOpen) {
        ui->portSide_graph->replot(replotVal);

        if(flag.PlotAngleScanColorMap && flag.PlotAngleScan){
            ui->altPlot->replot(replotVal);
        }
    }
    if (Format->isOpen_gpsPlot) {
        ChartWindowInstance->target_resizeAllTargets();
        ui->gpsPlot->replot(replotVal);
    }
}

void MainWindow :: plot_setCursor(int state) {
    // state = 0 for normal cursor, 1 for cross cursor - if it is coming from plot_onMouseHover()
    // state = -1 if user changes mode from a menu action

    QCustomPlot *plot = nullptr;
    bool mode;

    mouseTimer.start(5000);
    if (flag.cursorHidden) {
        flag.cursorHidden = false;
        customPlotLF->setCursor(Qt::ArrowCursor);
        customPlotHF->setCursor(Qt::ArrowCursor);
        depthPlot->setCursor(Qt::ArrowCursor);
        ui->sensor_graph->setCursor(Qt::ArrowCursor);
        ui->gpsPlot->setCursor(Qt::ArrowCursor);
    }

    // Find the current plot
    double angle_coefficient = 0;
    if (ping->angleOrientation==90)   angle_coefficient = 0;    // Side scan
    else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
    int updatePlot = 0;
    if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf) {
        plot = customPlotLF;
        updatePlot = 1;
    } else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf){
        plot = customPlotHF;
        updatePlot = 2;
    } else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos()))  && Format->isOpen_sensorPlot) {
        plot = ui->sensor_graph;
        updatePlot = 3;
        angle_coefficient = 0;
    } else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot){
        plot = ui->depthCPlot;
        updatePlot = 4;
        angle_coefficient = 0;
    } else {
        return;
    }

    if (state == 1)  {
        plot->setCursor(Qt::CrossCursor);
        return;
    } else if (state == 0) {
        plot->setCursor(Qt::ArrowCursor);
        return;
    }

    double x = plot->xAxis->pixelToCoord(plot->mapFromGlobal(QCursor::pos()).x());
    double y = plot->yAxis->pixelToCoord(plot->mapFromGlobal(QCursor::pos()).y());
    double yCursorCorrected = y-abs(x*angle_coefficient);

    double x_range_upper = plot->xAxis->range().upper;
    double x_range_lower = plot->xAxis->range().lower;
    double y_range_upper = plot->yAxis->range().upper;
    double y_range_lower = plot->yAxis->range().lower;

    if (flag.measuringTapeActive || flag.shadowToolActive || flag.targetToolActive || cropTool->toolActive) {
        mode = 1;
    } else {
        mode = 0;
    }

    if (mode) {
        if (yCursorCorrected > ping->y_LowerBound && yCursorCorrected < ping->y_UpperBound &&
                x < x_range_upper && x > x_range_lower && y < y_range_upper && y > y_range_lower)
            plot->setCursor(Qt::CrossCursor);
        else
            plot->setCursor(Qt::ArrowCursor);
    } else {
        plot->setCursor(Qt::ArrowCursor);
    }
}

/**
 * @brief MainWindow::onMouseMove
 * @param event
 * Displays plot coordinates of mouse pointer when on the main plot
 */
void MainWindow :: plot_onMouseHover(QMouseEvent* event){
    // If GUI is playing data, do not enter this function because it will slow down GUI
    if (state.GUI_MODE == 2 && (state.PLAY_MODE > 0 && state.PLAY_MODE < 3)) return;

    double angle_coefficient = 0, ping_Num = 0, x_port = 0, x_star = 0;

    if (ping->angleOrientation==90)   angle_coefficient = 0;    // Side scan
    else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan

    QCursor cursor;
    QPixmap pixmap = targetItem::getIconPixmap(true, flag.targetIconType);
    QPixmap pixmap2 = pixmap.scaled(40,40);

    if (flag.mousePressedFlagLf) {
        QCPAxis *ax = customPlotLF->xAxis;
        QCPAxis *ay = customPlotLF->yAxis;
        double diffX = ax->pixelToCoord(mouseStartPos_lf.x()) - ax->pixelToCoord(event->pos().x());
        ax->setRange(startRangeX_lf.lower + diffX, startRangeX_lf.upper + diffX);
        double diffY = ay->pixelToCoord(mouseStartPos_lf.y()) - ay->pixelToCoord(event->pos().y());
        ay->setRange(startRangeY_lf.lower + diffY, startRangeY_lf.upper + diffY);
    } else if (flag.mousePressedFlagHf) {
        QCPAxis *ax = customPlotHF->xAxis;
        QCPAxis *ay = customPlotHF->yAxis;
        double diffX = ax->pixelToCoord(mouseStartPos_hf.x()) - ax->pixelToCoord(event->pos().x());
        ax->setRange(startRangeX_hf.lower + diffX, startRangeX_hf.upper + diffX);
        double diffY = ay->pixelToCoord(mouseStartPos_hf.y()) - ay->pixelToCoord(event->pos().y());
        ay->setRange(startRangeY_hf.lower + diffY, startRangeY_hf.upper + diffY);
    }

    QCustomPlot *plot = customPlotLF;
    int updatePlot = 1; // 1 is Lf, 2 is Hf, 3 is Sensor, 4 is Altimeter/Depth
    if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf) {
        plot = customPlotLF;
        updatePlot = 1;
    }
    else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf){
        plot = customPlotHF;
        updatePlot = 2;
    }
    else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos()))  && Format->isOpen_sensorPlot) {
        plot = ui->sensor_graph;
        updatePlot = 3;
        angle_coefficient = 0;
    }
    else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot){
        plot = ui->depthCPlot;
        updatePlot = 4;
        angle_coefficient = 0;
    }

    double x = plot->xAxis->pixelToCoord(event->pos().x());
    double y = plot->yAxis->pixelToCoord(event->pos().y());
    // The boundaries of the plot in meters
    double x_range_upper = plot->xAxis->range().upper;
    double x_range_lower = plot->xAxis->range().lower;
    double y_range_upper = plot->yAxis->range().upper;
    double y_range_lower = plot->yAxis->range().lower;

    double yCursorCorrected = y-abs(x*angle_coefficient);
    double alt_trace = 0, yaw = 0, roll = 0, pitch = 0, azimuth = 0, temperature = 0;

    QString status_rtc="00:00:00";
    float status_depth =0;
    QString mouseGpsString = "";
    GpsPoint readGpsPoint;
    double speed = 0, distance = 0;
    bool paused = ((state.PLAY_MODE==4 || state.PLAY_MODE == 5 || state.PLAY_MODE == 6) && state.GUI_MODE == 2)
            || (state.RT_MODE < 2 && state.GUI_MODE == 1) || (state.GUI_MODE == 0 && state.SIM_MODE == 0);

    PingMapItem item = dataMap.last();
    if (!item.isValid)      paused = false;
    readGpsPoint.Lat = item.lattitude;
    readGpsPoint.Lon = item.longitude;
    readGpsPoint.Dir = item.heading;
    speed = item.speed;
    distance = item.distance;
    alt_trace = item.altitude;
    yaw = item.yaw;
    roll = item.roll;
    pitch = item.pitch;
    azimuth = item.azimuth;
    status_depth = item.depth;
    status_rtc = item.rtcTime;
    ping_Num = item.pingNumber;
    temperature = item.temperature;

    if (!flag.showGap){
        double x_Gap = dataMap.findItemWithY(yCursorCorrected).altitude * 0.98 / dataMap.findItemWithY(yCursorCorrected).altitudeFactor;
        x_port = x_Gap;//alt_trace;//depthPlot->graph(1)->data()->at(yCursorCorrected)->value;
        x_star = x_Gap;//alt_trace;//depthPlot->graph(0)->data()->at(yCursorCorrected)->value;
    }

    if(x >  x_range_upper || x < x_range_lower || y > y_range_upper || y < y_range_lower){
        // If cursor is outside the plotting area

        plot_setCursor(0);

        if (paused && (updatePlot == 1 || updatePlot == 2) && cropTool->toolActive && cropTool->ping_start > 0)      crop_UpdatePlot(plot, event);

        if (paused) {
            crosshair_hf.setVisible(false);
            crosshair_lf.setVisible(false);

//            line->point1->setCoords(0,ping->lastTracePos);
//            line->point2->setCoords(1,ping->lastTracePos);
//            line_depthTrace->point1->setCoords(0,ping->lastTracePos);
//            line_depthTrace->point2->setCoords(1,ping->lastTracePos);
//            depthLegend->position->setCoords(setting.globalDepth, ping->lastTracePos);
//            double dist = ping->lastTracePos - ping->y_boatPos;
//            depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");

            ui->xy_val->setText("X: 0 Y: 0");
            ChartWindowInstance->legend_updateLegendText(readGpsPoint.Lat, readGpsPoint.Lon, readGpsPoint.Dir, speed, distance);
            ChartWindowInstance->track_moveCurve(ping->pingNumCurrent - ping_Num);
            status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature,
                                   roll, pitch, yaw, alt_trace, azimuth);
        }
    } else if(yCursorCorrected > ping->y_UpperBound){
        // Mouse is above plot

        plot_setCursor(0);

        if (paused && (updatePlot == 1 || updatePlot == 2) && cropTool->toolActive && cropTool->ping_start > 0)      crop_UpdatePlot(plot, event);

        if (paused) {
            crosshair_hf.setVisible(false);
            crosshair_lf.setVisible(false);

            line->point1->setCoords(0,ping->lastTracePos);
            line->point2->setCoords(1,ping->lastTracePos);
            line_depthTrace->point1->setCoords(0,ping->lastTracePos);
            line_depthTrace->point2->setCoords(1,ping->lastTracePos);
            depthLegend->position->setCoords(setting.globalDepth, ping->lastTracePos);
            double dist = ping->lastTracePos - ping->y_boatPos;
            depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");

            ui->xy_val->setText("X: "+QString::number(x, 'f', 1)+" Y: "+QString::number(y, 'f', 1));
            ChartWindowInstance->legend_updateLegendText(readGpsPoint.Lat, readGpsPoint.Lon, readGpsPoint.Dir, speed, distance);
            ChartWindowInstance->track_moveCurve(ping->pingNumCurrent - ping_Num);
            status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature,
                                   roll, pitch, yaw, alt_trace, azimuth);
        }
    } else if (yCursorCorrected < ping->y_LowerBound) {
        // Cursor is below plot

        plot_setCursor(0);

        if (paused && (updatePlot == 1 || updatePlot == 2) && cropTool->toolActive && cropTool->ping_start > 0)      crop_UpdatePlot(plot, event);

        if (paused) {
            crosshair_hf.setVisible(false);
            crosshair_lf.setVisible(false);

            line->point1->setCoords(0,ping->lastTracePos);
            line->point2->setCoords(1,ping->lastTracePos);
            line_depthTrace->point1->setCoords(0,ping->lastTracePos);
            line_depthTrace->point2->setCoords(1,ping->lastTracePos);
            depthLegend->position->setCoords(setting.globalDepth, ping->lastTracePos);
            double dist = ping->lastTracePos - ping->y_boatPos;
            depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");

            ui->xy_val->setText("X: "+QString::number(x, 'f', 1)+" Y: "+QString::number(y, 'f', 1));
            ChartWindowInstance->legend_updateLegendText(readGpsPoint.Lat, readGpsPoint.Lon, readGpsPoint.Dir, speed, distance);
            ChartWindowInstance->track_moveCurve(ping->pingNumCurrent - ping_Num);
            status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature,
                                   roll, pitch, yaw, alt_trace, azimuth);
        }
    } else if (x<(-ping->rangeLf_double+x_port) || x>(ping->rangeLf_double-x_star)){
        // Cursor is outside left/right boundary

        plot_setCursor(0);

        if (paused && (updatePlot == 1 || updatePlot == 2) && cropTool->toolActive && cropTool->ping_start > 0)      crop_UpdatePlot(plot, event);

        if (paused) {
            crosshair_hf.setVisible(false);
            crosshair_lf.setVisible(false);

            line->point1->setCoords(0,ping->lastTracePos);
            line->point2->setCoords(1,ping->lastTracePos);
            line_depthTrace->point1->setCoords(0,ping->lastTracePos);
            line_depthTrace->point2->setCoords(1,ping->lastTracePos);
            depthLegend->position->setCoords(setting.globalDepth, ping->lastTracePos);
            double dist = ping->lastTracePos - ping->y_boatPos;
            depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");

            ui->xy_val->setText("X: "+QString::number(x, 'f', 1)+" Y: "+QString::number(y, 'f', 1));
            ChartWindowInstance->legend_updateLegendText(readGpsPoint.Lat, readGpsPoint.Lon, readGpsPoint.Dir, speed, distance);
            ChartWindowInstance->track_moveCurve(ping->pingNumCurrent - ping_Num);
            status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature,
                                   roll, pitch, yaw, alt_trace, azimuth);
        }
    }
    else {
        // Cursor inside colormap area

        plot_setCursor(flag.measuringTapeActive || flag.shadowToolActive || flag.targetToolActive || cropTool->toolActive);

        if ((updatePlot == 1 || updatePlot == 2)) {
            if (flag.measuringTapeActive || flag.shadowToolActive)          measuringTape_UpdatePlot(plot, event);
            else if (flag.targetToolActive && tools->targetState)           target_UpdatePlot(plot, event);
            else if (cropTool->toolActive && cropTool->ping_start > 0)      crop_UpdatePlot(plot, event);
        }

        if (paused) {
            line->point1->setCoords(0, yCursorCorrected);
            line->point2->setCoords(1, yCursorCorrected);
            line_depthTrace->point1->setCoords(0, yCursorCorrected);
            line_depthTrace->point2->setCoords(1, yCursorCorrected);
            depthLegend->position->setCoords(setting.globalDepth, yCursorCorrected);
            double dist = yCursorCorrected - ping->y_boatPos;
            depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");

            PingMapItem item = dataMap.findItemWithY(yCursorCorrected);
            if (item.isValid) {
                readGpsPoint.Lat = item.lattitude;
                readGpsPoint.Lon = item.longitude;
                readGpsPoint.Dir = item.heading;
                speed = item.speed;
                distance = item.distance;
                alt_trace = item.altitude;
                yaw = item.yaw;
                roll = item.roll;
                pitch = item.pitch;
                status_depth = item.depth;
                status_rtc = item.rtcTime;
                ping_Num = item.pingNumber;
                azimuth = item.azimuth;
                temperature = item.temperature;
            }

            if (updatePlot == 1 && Format->isCrosshairShown) {
                crosshair_lf.setPosition(x, y, y -  ping->y_boatPos,
                                         customPlotLF->xAxis->range().upper,
                                         customPlotLF->yAxis->range().upper);

                crosshair_lf.setVisible(true);
                crosshair_hf.setVisible(false);
            } else if (updatePlot == 2 && Format->isCrosshairShown) {
                crosshair_hf.setPosition(x, y, y -  ping->y_boatPos,
                                         customPlotHF->xAxis->range().upper,
                                         customPlotHF->yAxis->range().upper);
                crosshair_hf.setVisible(true);
                crosshair_lf.setVisible(false);
            } else if (Format->isCrosshairShown) {
                if (Format->isOpenLf) {
                    crosshair_lf.setPosition(0, y, y -  ping->y_boatPos,
                                             customPlotLF->xAxis->range().upper,
                                             customPlotLF->yAxis->range().upper);
                    crosshair_lf.setVisible(true);
                    crosshair_hf.setVisible(false);
                } else {
                    crosshair_hf.setPosition(0, y, y -  ping->y_boatPos,
                                             customPlotHF->xAxis->range().upper,
                                             customPlotHF->yAxis->range().upper);
                    crosshair_hf.setVisible(true);
                    crosshair_lf.setVisible(false);
                }
            } else {
                crosshair_hf.setVisible(false);
                crosshair_lf.setVisible(false);
            }

            ui->xy_val->setText("X: "+QString::number(x, 'f', 1)+" Y: "+QString::number(y, 'f', 1));
            ChartWindowInstance->legend_updateLegendText(readGpsPoint.Lat, readGpsPoint.Lon, readGpsPoint.Dir, speed, distance);
            ChartWindowInstance->track_moveCurve(ping->pingNumCurrent - ping_Num);
            status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature,
                                   roll, pitch, yaw, alt_trace, azimuth);
        }
    }
    if (paused) {
//        ui->sensor_graph->replot(QCustomPlot::rpQueuedReplot);
//        ui->depthCPlot->replot(QCustomPlot::rpQueuedReplot);
        plot_replotAll();
    }
}

void MainWindow :: plot_tools_onMouseMoveGps(QMouseEvent* event) {
    QApplication::restoreOverrideCursor();

    bool paused = ((state.PLAY_MODE==4 || state.PLAY_MODE == 5 || state.PLAY_MODE == 6) && state.GUI_MODE == 2)
            || (state.RT_MODE < 2 && state.GUI_MODE == 1) || (state.GUI_MODE == 0 && state.SIM_MODE == 0);
    if (paused) {
        int pingDiff = ChartWindowInstance->track_CursorToCurve(event, ping->pingNumCurrent - dataMap.firstKey());
        double alt_trace = 0, yaw = 0, roll = 0, pitch = 0, speed = 0, distance = 0, yPos = 0, azimuth = 0, temperature = 0;
        QString status_rtc="00:00:00";
        float status_depth = 0;
        GpsPoint readGpsPoint;
        int ping_Num = 0;
        PingMapItem item = dataMap.findItem(ping->pingNumCurrent - pingDiff);
        if (item.isValid) {
            readGpsPoint.Lat = item.lattitude;
            readGpsPoint.Lon = item.longitude;
            readGpsPoint.Dir = item.heading;
            speed = item.speed;
            distance = item.distance;
            alt_trace = item.altitude;
            yaw = item.yaw;
            roll = item.roll;
            pitch = item.pitch;
            status_depth = item.depth;
            status_rtc = item.rtcTime;
            ping_Num = item.pingNumber;
            yPos = item.yCoord;
            azimuth = item.azimuth;
            temperature = item.temperature;

            if (Format->isOpenLf) {
                crosshair_lf.setPosition(0, yPos, yPos -  ping->y_boatPos,
                                         customPlotLF->xAxis->range().upper,
                                         customPlotLF->yAxis->range().upper);
                crosshair_lf.setVisible(true);
                crosshair_hf.setVisible(false);
            } else {
                crosshair_hf.setPosition(0, yPos, yPos -  ping->y_boatPos,
                                         customPlotHF->xAxis->range().upper,
                                         customPlotHF->yAxis->range().upper);
                crosshair_hf.setVisible(true);
                crosshair_lf.setVisible(false);
            }
        } else {
            item = dataMap.last();
            readGpsPoint.Lat = item.lattitude;
            readGpsPoint.Lon = item.longitude;
            readGpsPoint.Dir = item.heading;
            speed = item.speed;
            distance = item.distance;
            alt_trace = item.altitude;
            yaw = item.yaw;
            roll = item.roll;
            pitch = item.pitch;
            status_depth = item.depth;
            status_rtc = item.rtcTime;
            ping_Num = item.pingNumber;
            yPos = item.yCoord;
            azimuth = item.azimuth;
            temperature = item.temperature;
            crosshair_lf.setVisible(false);
            crosshair_hf.setVisible(false);
            if (!item.isValid) {
                plot_setCursor(0);
                return;
            }
        }
        line->point1->setCoords(0, yPos);
        line->point2->setCoords(1, yPos);
        line_depthTrace->point1->setCoords(0, yPos);
        line_depthTrace->point2->setCoords(1, yPos);
        depthLegend->position->setCoords(setting.globalDepth, yPos);
        double dist = yPos - ping->y_boatPos;
        depthLegend->setText(" "+ QString::number(dist, 'f', 1) +" ");

        ChartWindowInstance->legend_updateLegendText(readGpsPoint.Lat, readGpsPoint.Lon, readGpsPoint.Dir, speed, distance);
        status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature, roll, pitch, yaw, alt_trace, azimuth);
        plot_replotAll();
    }
    plot_setCursor(0);
}

/**
 * @brief MainWindow::onMousePressed
 * @param event
 * Displays plot coordinates of mouse pointer when on the main plot
 */
void MainWindow :: plot_Lf_onMousePressed(QMouseEvent* event) {
//    int hoveringPanel = tools->checkMousePosition(event->pos().x(), event->pos().y(), !Format->isOpenLf);
//    if (hoveringPanel)      return;
    if (event->button()==Qt::LeftButton) {
        flag.mousePressedFlagLf = 1;
        flag.mousePressedFlagHf = 0;
        mouseStartPos_lf = event->pos();
        startRangeX_lf = customPlotLF->xAxis->range();
        startRangeY_lf = customPlotLF->yAxis->range();
        if (flag.zoomAroundBoatLf) {
            userWindowInstance->setCustomLfZoom();
            flag.zoomAroundBoatLf = 0;
        }
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
    }

    if(event->button()==Qt::RightButton && customPlotLF->selectedItems().length() > 0 ){
        QMenu *popMenu = new QMenu(customPlotLF);
        QAction *deleteFlag = new QAction(tr("Delete"),customPlotLF);
        QAction *renameTarget = new QAction(tr("Rename"), customPlotLF);
        QAction *saveTarget = new QAction(tr("Export"), this);

        //QAction *goToFlag = new QAction(tr("Go To Flag"),this);
        if (tools->selectedTargets().count() == 1)
            popMenu->addAction(renameTarget);
        popMenu->addAction(saveTarget);
        popMenu->addAction(deleteFlag);
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedItems()));
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(measuringTape_DeleteSelectedItems()));
        connect(renameTarget, SIGNAL(triggered()),this,SLOT(target_renameTarget()));
        connect(saveTarget, SIGNAL(triggered()),this,SLOT(target_exportTarget()));
        popMenu->exec(QCursor::pos());
    }
    else if(event->button()==Qt::LeftButton && flag.measuringTapeActive || flag.shadowToolActive){
        measuringTape_mouseClick(customPlotLF, event);
    }
    //the counter for the measuring tape should be placed back ton  zero
    //causes overlapping of starting ang end points on file change
    else if (event->button()==Qt::LeftButton && flag.targetToolActive) {
        double x = ui->customPlot->xAxis->pixelToCoord(event->pos().x());
        double y = ui->customPlot->yAxis->pixelToCoord(event->pos().y());
//        target_NewTarget(x, y, nullptr);
        target_MouseClick(customPlotLF, event);
    }
}

void MainWindow :: plot_Hf_onMousePressed(QMouseEvent* event){
//    if (!Format->isOpenLf) {
//        int hoveringPanel = tools->checkMousePosition(event->pos().x(), event->pos().y(), !Format->isOpenLf);
//        if (hoveringPanel)      return;
//    }
    if (event->button()==Qt::LeftButton) {
        flag.mousePressedFlagHf = 1;
        flag.mousePressedFlagLf = 0;
        mouseStartPos_hf = event->pos();
        startRangeX_hf = customPlotHF->xAxis->range();
        startRangeY_hf = customPlotHF->yAxis->range();
        if (flag.zoomAroundBoatHf) {
            userWindowInstance->setCustomHfZoom();
            flag.zoomAroundBoatHf = 0;
        }
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
    }

    if(event->button()==Qt::RightButton && customPlotHF->selectedItems().length() > 0 ){
        QMenu *popMenu = new QMenu(customPlotHF);
        QAction *deleteFlag = new QAction(tr("Delete"),customPlotHF);
        QAction *renameTarget = new QAction(tr("Rename"), customPlotHF);
        QAction *saveTarget = new QAction(tr("Export"), this);

        //QAction *goToFlag = new QAction(tr("Go To Flag"),this);
        if (tools->selectedTargets().count() == 1)
            popMenu->addAction(renameTarget);
        popMenu->addAction(saveTarget);
        popMenu->addAction(deleteFlag);
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedItems()));
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(measuringTape_DeleteSelectedItems()));
        connect(renameTarget, SIGNAL(triggered()),this,SLOT(target_renameTarget()));
        connect(saveTarget, SIGNAL(triggered()),this,SLOT(target_exportTarget()));
        popMenu->exec(QCursor::pos());
    }
    else if(event->button()==Qt::LeftButton && flag.measuringTapeActive || flag.shadowToolActive){
        measuringTape_mouseClick(customPlotHF, event);
    }
    else if (event->button()==Qt::LeftButton && flag.targetToolActive) {
        double x = customPlotHF->xAxis->pixelToCoord(event->pos().x());
        double y = customPlotHF->yAxis->pixelToCoord(event->pos().y());
//        target_NewTarget(x, y, nullptr);
        target_MouseClick(customPlotHF, event);
    }

    //the counter for the measuring tape should be placed back to zero
    //causes overlapping of starting ang end points on file change
}

void MainWindow :: plot_Gnss_onMousePressed(QMouseEvent* event){
    ui->gpsPlot->replot(QCustomPlot::rpQueuedReplot);
    int waypointSize = tools->selectedWaypoints().count();
    int targetSize = tools->selectedTargets().count();

    if (event->button()==Qt::LeftButton) {
        ChartWindowInstance->lockToBoat = false;
    }
    if(event->button()==Qt::RightButton && (waypointSize + targetSize > 0)){
        QMenu *popMenu = new QMenu(ui->gpsPlot);
        QAction *deleteFlag = new QAction(tr("Delete"),ui->gpsPlot);
        QAction *renameTarget = new QAction(tr("Rename"), ui->gpsPlot);
        QAction *saveTarget = new QAction(tr("Export"), this);
        QAction *setStartPoint = new QAction(tr("Set to 1"),this);

        if (targetSize == 1)
            popMenu->addAction(renameTarget);
        if (waypointSize == 1) {
            popMenu->addAction(renameTarget);
            popMenu->addAction(setStartPoint);
        }
        if (targetSize > 1)
            popMenu->addAction(saveTarget);
        popMenu->addAction(deleteFlag);
        if (state.GUI_MODE == 2 && !DEVELOPER_TESTING)
            connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedItems()));
        else
            connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedWaypoints()));
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(measuringTape_DeleteSelectedItems()));
        connect(renameTarget, SIGNAL(triggered()),this,SLOT(target_renameTarget()));
        connect(saveTarget, SIGNAL(triggered()),this,SLOT(target_exportTarget()));
        connect(setStartPoint, SIGNAL(triggered()),this,SLOT(target_setStartWaypoint()));
        popMenu->exec(QCursor::pos());
    } else if(event->button()==Qt::LeftButton && flag.measuringTapeActive == true){
        //measuringTape_Clicked(ui->gpsPlot, event);
    } else if (event->button()==Qt::LeftButton && flag.waypointToolActive) {
        double xPosition = ui->gpsPlot->xAxis->pixelToCoord(event->pos().x());
        double yPosition = ui->gpsPlot->yAxis->pixelToCoord(event->pos().y());

        target_NewWaypoint(yPosition, xPosition);
    }
}

void MainWindow :: plot_image_onMousePressed(QMouseEvent* event) {
    if(event->button()==Qt::RightButton && ui->targetListWidget->selectedItems().length() > 0 ){
        QMenu *popMenu = new QMenu(customPlotLF);
        QAction *deleteFlag = new QAction(tr("Delete"),customPlotLF);
        popMenu->addAction(deleteFlag);
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedItems()));
        connect(deleteFlag,SIGNAL(triggered()),this,SLOT(measuringTape_DeleteSelectedItems()));
        popMenu->exec(QCursor::pos());
        delete deleteFlag;
    }
}

void MainWindow :: plot_Lf_onMouseReleased(QMouseEvent* event) {
    if (event->button()==Qt::LeftButton && cropTool->toolActive) {
        int hoveringPanel = tools->checkMousePosition(event->pos().x(), event->pos().y(), !Format->isOpenLf);
        if (hoveringPanel)      return;
        crop_MouseClick(customPlotLF, event);
    }
}

void MainWindow :: plot_Hf_onMouseReleased(QMouseEvent* event) {
    if (event->button()==Qt::LeftButton && cropTool->toolActive) {
        if (!Format->isOpenLf) {
            int hoveringPanel = tools->checkMousePosition(event->pos().x(), event->pos().y(), !Format->isOpenLf);
            if (hoveringPanel)      return;
        }
        crop_MouseClick(customPlotHF, event);
    }
}

void MainWindow :: plot_Gnss_onMouseReleased(QMouseEvent* event) {
    if (event->button()==Qt::LeftButton) {
        if (!ChartWindowInstance->plot_isCentered())
            ChartWindowInstance->lockToBoat = false;
        else
            ChartWindowInstance->lockToBoat = true;
    }
}


void MainWindow :: plot_Lf_PlotItemSelectionChanged(void) {
    int toolSelected = tools->checkToolSelection(flag.targetToolActive, flag.measuringTapeActive, flag.shadowToolActive, cropTool->toolActive);
    if (toolSelected) {
        if (toolSelected == 0x1)        on_actionFlag_triggered();
        else if (toolSelected == 0x2)   on_actionMeasuring_Tape_triggered();
        else if (toolSelected == 0x3)   on_actionShadow_triggered();
        else if (toolSelected == 0x4)   on_actionCrop_File_triggered();
        plot_replotAll();
        return;
    }
    if (flag.targetToolActive)   return;
    if (flag.measuringTapeActive)    return;
    bool ctrlKeyState = QGuiApplication::keyboardModifiers() & Qt::ControlModifier;

    if (customPlotLF->selectedItems().length() == 0) {
        flag.targetIgnoreSelectionChange = true;
        target_selectAllTargets(false);
        target_removeSnipBoxes();
        plot_replotAll();
        return;
    }
    QList<QCPAbstractItem*> tmpList = customPlotLF->selectedItems(); // For debugging

    // if there is at least one item selected from customPlotLF, then search for it
    for (int i = 0; i < tools->targetList.length(); i++) {
        if (ctrlKeyState == true) {
            // Select multiple items

            // Find the flag items selected on the plot
            if (tools->targetList[i].icon_lf->selected())
                target_selectTarget(&tools->targetList[i], true);

            if (i == tools->targetList.length()-1) {
                ctrlKeyState = false;
            }
            target_removeSnipBoxes();
        } else {
            if (customPlotLF->selectedItems().last() == tools->targetList[i].icon_lf) {
                target_selectAllTargets(false);
                target_selectTarget(&tools->targetList[i], true);
                target_ShowImageAndBox();
                plot_replotAll();
                break;
            }
        }
    }
    for (int i = 0; i < tools->measuringTapeList.length(); i++) {
        if (customPlotLF->selectedItems().last() == tools->measuringTapeList[i].distanceText) {
            // Search for text item
            measuringTape_SelectItem(i);

            return;
        } else {
            tools->measuringTapeList[i].selected = false;
        }

        for (int j = 0; j < tools->measuringTapeList[i].line_back.length(); j++){
            if (customPlotLF->selectedItems().last() == tools->measuringTapeList[i].line_back.at(j)
                    || customPlotLF->selectedItems().last() == tools->measuringTapeList[i].line_top.at(j)) {
                // Search for the measuring tape line
                measuringTape_SelectItem(i);
                return;
            } else {
                tools->measuringTapeList[i].selected = false;
            }
        }
    }
}

void MainWindow :: plot_Hf_PlotItemSelectionChanged(){
    int toolSelected = tools->checkToolSelection(flag.targetToolActive, flag.measuringTapeActive, flag.shadowToolActive, cropTool->toolActive);
    if (toolSelected) {
        if (toolSelected == 0x1)        on_actionFlag_triggered();
        else if (toolSelected == 0x2)   on_actionMeasuring_Tape_triggered();
        else if (toolSelected == 0x3)   on_actionShadow_triggered();
        else if (toolSelected == 0x4)   on_actionCrop_File_triggered();
        plot_replotAll();
        return;
    }
    if (flag.targetToolActive)   return;
    if (flag.measuringTapeActive)    return;
    bool ctrlKeyState = QGuiApplication::keyboardModifiers() & Qt::ControlModifier;

    if (customPlotHF->selectedItems().length() == 0) {
        target_selectAllTargets(false);
        target_removeSnipBoxes();
        plot_replotAll();
        return;
    }

    // if there is at least one item selected from customPlotLF, then search for it
    for (int i = 0; i < tools->targetList.length(); i++) {
        if (ctrlKeyState == true) {
            // Select multiple items
            QList<QCPAbstractItem*> tmpList = customPlotHF->selectedItems();

            // Find the flag items selected on the plot
            if (tools->targetList[i].icon_hf->selected())
                target_selectTarget(&tools->targetList[i], true);

            if (i == tools->targetList.length()-1) {
                ctrlKeyState = false;
            }
            target_removeSnipBoxes();
        } else {
            if (customPlotHF->selectedItems().last() == tools->targetList[i].icon_hf) {
                target_selectAllTargets(false);
                target_selectTarget(&tools->targetList[i], true);
                target_ShowImageAndBox();
                plot_replotAll();
                break;
            }
        }
    }
    for (int i = 0; i < tools->measuringTapeList.length(); i++) {
        if (customPlotHF->selectedItems().last() == tools->measuringTapeList[i].distanceText) {
            // Search for text item
            measuringTape_SelectItem(i);

            return;
        } else {
            tools->measuringTapeList[i].selected = false;
        }

        for (int j = 0; j < tools->measuringTapeList[i].line_back.length(); j++){
            if (customPlotHF->selectedItems().last() == tools->measuringTapeList[i].line_back.at(j)
                    || customPlotHF->selectedItems().last() == tools->measuringTapeList[i].line_top.at(j)) {
                // Search for the measuring tape line
                measuringTape_SelectItem(i);
                return;
            } else {
                tools->measuringTapeList[i].selected = false;
            }
        }
    }
}

void MainWindow :: plot_Gnss_PlotItemSelectionChanged(void) {
    QList<QCPAbstractItem*> tmpList = ui->gpsPlot->selectedItems();

    if (ChartWindowInstance->mapPlotPtr->selectedItems().length() == 0) {
        return;
        target_selectAllWaypoints(false);
        target_selectAllTargets(false);
        //tools->tracker.resetRoute();
        target_removeSnipBoxes();
        plot_replotAll();
        return;
    }
    bool ctrlKeyState = QGuiApplication::keyboardModifiers() & Qt::ControlModifier;

    // if there is at least one item selected from the chart, then search for it
    for (int i = 0; i < tools->waypointList.length(); i++) {
        if (tools->waypointList[i].icon_chart == nullptr)     continue;
        if (ctrlKeyState == true) {
            // Find the flag items selected on the plot
            if (tmpList.last() == tools->waypointList[i].icon_chart) {//if (tools->waypointList[i].icon_chart->selected())
                if (tools->waypointList[i].selected) {
                    if (&tools->waypointList[i] == tools->tracker.nextWpt) {
                        tools->tracker.resetWptAnchor();
                    }
                } else {
                    tools->tracker.anchorWaypoint(&tools->waypointList[i]);
                }
                target_selectWaypoint(&tools->waypointList[i], !tools->waypointList[i].selected, false);
                plot_replotAll();
                break;
            }
        } else {
            if (tmpList.last() == tools->waypointList[i].icon_chart) {
                target_selectAllWaypoints(false);
                target_selectWaypoint(&tools->waypointList[i], true, false);
                tools->tracker.anchorWaypoint(&tools->waypointList[i]);
                plot_replotAll();
                break;
            }
        }
    }
    QList <waypointItem*> selectedWpts = tools->selectedWaypoints();
    if (selectedWpts.length() > 1) {
        tools->tracker.resetRoute();
        // Multiple Items selected
        for (int i = 0; i < selectedWpts.length(); i++) {
            // Go through targets to select currentItem and deselect all others
            tools->tracker.appendRoute(selectedWpts[i]);
        }
        tools->tracker.displayRoute();
        if (tools->tracker.route.length() > 0) {
            tools->tracker.anchorWaypoint(tools->tracker.route[0]);
        }
    }


    for (int i = 0; i < tools->targetList.length(); i++) {
        if (tools->targetList[i].icon_chart == nullptr)     continue;
        if (ctrlKeyState == true) {
            // Select multiple items
            QList<QCPAbstractItem*> tmpList = ui->gpsPlot->selectedItems();

            // Find the flag items selected on the plot
            if (tools->targetList[i].icon_chart->selected())
                target_selectTarget(&tools->targetList[i], true);
            if (i == tools->targetList.length()-1) {
                ctrlKeyState = false;
            }
        } else {
            if (ui->gpsPlot->selectedItems().last() == tools->targetList[i].icon_chart) {
                target_selectAllTargets(false);
                target_selectTarget(&tools->targetList[i], true);
                target_ShowImageAndBox();
                plot_replotAll();
                break;
            }
        }
    }
    for (int i = 0; i < tools->measuringTapeList.length(); i++) {
        if (ui->gpsPlot->selectedItems().last() == tools->measuringTapeList[i].distanceText ||
                ui->gpsPlot->selectedItems().last() == tools->measuringTapeList[i].textGps ||
                ui->gpsPlot->selectedItems().last() == tools->measuringTapeList[i].lineGps.last()) {
            // Search for text item
            measuringTape_SelectItem(i);

            return;
        } else {
            tools->measuringTapeList[i].selected = false;
        }

        for (int j = 0; j < tools->measuringTapeList[i].line_back.length(); j++){
            if (ui->gpsPlot->selectedItems().last() == tools->measuringTapeList[i].line_back.at(j)
                    || ui->gpsPlot->selectedItems().last() == tools->measuringTapeList[i].line_top.at(j)) {
                // Search for the measuring tape line
                measuringTape_SelectItem(i);
                return;
            } else {
                tools->measuringTapeList[i].selected = false;
            }
        }
    }

    for (int i = 0; i < tmpList.length(); i++) {
        tmpList[i]->setSelected(false);
    }
}

void MainWindow :: plot_icons_resizeBoatIcon(){

    double width = (customPlotLF->xAxis->range().upper - customPlotLF->xAxis->range().lower);
    double height = (customPlotLF->yAxis->range().upper - customPlotLF->yAxis->range().lower);

    double xInc =  30*width/((Format->plotWidth_LfHf)*1.0);
    double yInc = 30*height/((Format->plotHeight_Lf)*1.0);

    double boatX = (lfBoatPixmap->topLeft->key() + lfBoatPixmap->bottomRight->key())/2.0;
    double boatY = (lfBoatPixmap->topLeft->value());

    lfBoatPixmap->topLeft->setCoords(boatX-xInc/2, boatY);
    lfBoatPixmap->bottomRight->setCoords(boatX+xInc/2, boatY-yInc);

    double widthHF = (customPlotHF->xAxis->range().upper - customPlotHF->xAxis->range().lower);
    double heightHF = (customPlotHF->yAxis->range().upper - customPlotHF->yAxis->range().lower);

    double xIncHF = 30*widthHF/((Format->plotWidth_LfHf)*1.0);
    double yIncHF =  30*heightHF/((Format->plotHeight_Hf)*1.0);

    double boatXHF = (hfBoatPixmap->topLeft->key() + hfBoatPixmap->bottomRight->key())/2.0;
    double boatYHF = (hfBoatPixmap->topLeft->value());

    hfBoatPixmap->topLeft->setCoords(boatXHF-xIncHF/2, boatYHF);
    hfBoatPixmap->bottomRight->setCoords(boatXHF+xIncHF/2, boatYHF-yIncHF);

    double widthDepth = (depthPlot->xAxis->range().upper - depthPlot->xAxis->range().lower);
    double heightDepth = (depthPlot->yAxis->range().upper - depthPlot->yAxis->range().lower);

    double xIncDepth =  30*widthDepth/(depthPlot->width()*1.0);
    double yIncDepth = 30*heightDepth/(depthPlot->height()*1.0);

    double boatXDepth = (depthBoatPixmap->topLeft->key() + depthBoatPixmap->bottomRight->key())/2.0;
    ping->y_boatPos = (depthBoatPixmap->topLeft->value());

    depthBoatPixmap->topLeft->setCoords(boatXDepth-xIncDepth/2, ping->y_boatPos);
    depthBoatPixmap->bottomRight->setCoords(boatXDepth+xIncDepth/2, ping->y_boatPos-yIncDepth);

    line_depthBoat->start->setCoords(0, ping->y_boatPos);
    line_depthBoat->end->setCoords(setting.globalDepth, ping->y_boatPos);

    xIncDepth =  20.0*widthDepth/(depthPlot->width()*1.0);
    yIncDepth =  20.0*heightDepth/(depthPlot->height()*1.0);
//    DepthAlarmHandle->topLeft->setCoords(depthAlarm->level-xIncDepth/2, ping->lastTracePos + yIncDepth);
//    DepthAlarmHandle->bottomRight->setCoords(depthAlarm->level+xIncDepth/2, ping->lastTracePos);

    customPlotLF->replot(QCustomPlot::rpQueuedReplot);
    customPlotHF->replot(QCustomPlot::rpQueuedReplot);
    depthPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: plot_icons_updateCompassIcon() {
    if (state.GNSS_MODE < 2) {
        lfCompassPixmap->setVisible(false);
        hfCompassPixmap->setVisible(false);
        return;
    } else {
        if (!Format->isDisplayCompass) {
            lfCompassPixmap->setVisible(false);
            hfCompassPixmap->setVisible(false);
            return;
        } else if (Format->isOpenLf) {
            lfCompassPixmap->setVisible(true);
            hfCompassPixmap->setVisible(false);
        } else {
            lfCompassPixmap->setVisible(true);
            hfCompassPixmap->setVisible(true);
        }
    }

    QPixmap pixmapArrow(":/icons/icons/Compass.png");
    QMatrix rm;
    QMatrix scaleMat(1, 0, 1, 0, 0, 0);
    rm.rotate(-ChartWindowInstance->boatHeading);
    scaleMat.rotate(-ChartWindowInstance->boatHeading);
    QPixmap pixmap2 = pixmapArrow.transformed(rm);
//    ui->label_compassIcon->setPixmap(pixmapArrow.scaled(40,40).transformed(rm));
    double rScale = 35*qMax(abs(scaleMat.m11()), abs(scaleMat.m21()));
    lfCompassPixmap->setPixmap(pixmap2);
    lfCompassPixmap->topLeft->setCoords(65-rScale, 85-rScale);
    lfCompassPixmap->bottomRight->setCoords(65+rScale, 85+rScale);

    hfCompassPixmap->setPixmap(pixmap2);
    hfCompassPixmap->topLeft->setCoords(65-rScale, 85-rScale);
    hfCompassPixmap->bottomRight->setCoords(65+rScale, 85+rScale);
}

void MainWindow :: plot_zoomBox_updatePosition(bool updateRequired) {
    if (!flag.PlotHfSidescan)    return;
    if (!Format->isOpenLf || !Format->isOpenHf) {
        if (customPlotHF->hasItem(HfPlotViewBox) && customPlotLF->hasItem(LfPlotViewBox)) {
            LfPlotViewBox->setVisible(false);
            HfPlotViewBox->setVisible(false);
        }
        return;
    }

    if ((Format->isZoomBoxAlwaysOn || updateRequired) && colorMap != nullptr) {
        QPen distancePen1;
        distancePen1.setColor(Qt::white);
        distancePen1.setWidth(2);
        distancePen1.setStyle(Qt::SolidLine);

        // Add the item to the plot if its missing
        if (!customPlotLF->hasItem(LfPlotViewBox)) {
            QPen distancePenRed;
            distancePen1.setColor(Qt::white);
            distancePen1.setWidth(2);
            distancePen1.setStyle(Qt::SolidLine);

            LfPlotViewBox = new QCPItemRect(customPlotLF);
            LfPlotViewBox->setPen(distancePen1);
            LfPlotViewBox->setLayer("graphs");
            LfPlotViewBox->setSelectable(false);
        }
        if (!customPlotHF->hasItem(HfPlotViewBox)) {
            HfPlotViewBox = new QCPItemRect(customPlotHF);
            HfPlotViewBox->setPen(distancePen1);
            HfPlotViewBox->setLayer("graphs");
            HfPlotViewBox->setSelectable(false);
        }
    }

    double XminLf = customPlotLF->xAxis->range().lower, XmaxLf = customPlotLF->xAxis->range().upper;
    double YminLf = customPlotLF->yAxis->range().lower, YmaxLf = customPlotLF->yAxis->range().upper;
    if (customPlotHF->hasItem(HfPlotViewBox)) {
        HfPlotViewBox->topLeft->setCoords(XminLf, YmaxLf);
        HfPlotViewBox->bottomRight->setCoords(XmaxLf, YminLf);
    }

    double XminHf = customPlotHF->xAxis->range().lower, XmaxHf = customPlotHF->xAxis->range().upper;
    double YminHf = customPlotHF->yAxis->range().lower, YmaxHf = customPlotHF->yAxis->range().upper;
    if (customPlotLF->hasItem(LfPlotViewBox)) {
        LfPlotViewBox->topLeft->setCoords(XminHf, YmaxHf);
        LfPlotViewBox->bottomRight->setCoords(XmaxHf, YminHf);
    }

    double ratioLf=0, ratioHf=0;
    int height = (Format->plotHeight_Lf);
    int width = (Format->plotWidth_LfHf);
    double updatedView = ((height * (ping->rangeLf_double * 2)) / width);

    ratioLf=sqrt((updatedView/(YmaxLf - YminLf))*(2*ping->rangeLf_double)/(XmaxLf - XminLf));

    height = (Format->plotHeight_Hf);
    width = (Format->plotWidth_LfHf);
    updatedView = ((height * (ping->rangeHf_double * 2)) / width);

    ratioHf=sqrt((updatedView/(YmaxHf - YminHf))*(2*ping->rangeHf_double)/(XmaxHf - XminHf));

    if (customPlotHF->hasItem(HfPlotViewBox) && customPlotLF->hasItem(LfPlotViewBox)) {
        if (abs(ratioLf - ratioHf) < 0.05) {
            LfPlotViewBox->setVisible(false);
            HfPlotViewBox->setVisible(false);
        } else if (ratioLf < ratioHf) {
            LfPlotViewBox->setVisible(true);
            HfPlotViewBox->setVisible(false);
        } else {
            LfPlotViewBox->setVisible(false);
            HfPlotViewBox->setVisible(true);
        }
    }
}

void MainWindow :: plot_click_activateMouseFlag(QMouseEvent *event) {
    if (event->buttons() & Qt::LeftButton) {
        flag.mouseClick = 1;
        if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf) {
            flag.mouseClick = 2;
        }
        else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf) {
            flag.mouseClick = 4;
        }
        else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot) {
            flag.mouseClick = 8;
        }
        else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos())) && Format->isOpen_sensorPlot) {
            flag.mouseClick = 16;
        }
    }
}

void MainWindow :: plot_click_deactivateMouseFlag() {
    flag.mouseClick = 0;
    flag.mousePressedFlagLf = 0;
    flag.mousePressedFlagHf = 0;
}

void MainWindow :: plot_offset_verticalShiftCalculator(){
    //This function calculates the vertical drop by using the clicks stored in the database and
    //current range of file being played

    setting.zoomStepLF = (setting.shiftCounterLf)*(0.05*ping->rangeLf_double);
    setting.zoomStepHF = (setting.shiftCounterHf)*(0.05*ping->rangeHf_double);
    setting.zoomStepDepthSense = (setting.shiftCounterDepthSensor)*(0.05*ping->rangeLf_double);
}

void MainWindow :: plot_offset_verticalShiftUp(){
    /*This function is called every time the ctrl + up arrow keys are pressed
     *moves the plots up by calculating the shift value stored by resultUpLf and resultUpHf
     *the shift value gets incremented every time the keys are pressed.
     */

    //calculates the drop in meters for evry click
    double resultUpHF = 0.05*ping->rangeHf_double;
    double resultUpLF = 0.05*ping->rangeLf_double;

    //if the mouse cursor is on LF plot
    if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf){
        //increments counter
        setting.shiftCounterLf++;
        //zoomStepLf is a global variable that stores the new shifts in meters
        //updates with every click
        setting.zoomStepLF = (setting.shiftCounterLf)*(0.05*ping->rangeLf_double);
        //moves the plots according to the drop value calculated above
        customPlotLF->yAxis->moveRange(-resultUpLF);

        //if the depth and sensor plots are tied to the LF
        setting.shiftCounterDepthSensor=setting.shiftCounterLf;
        setting.zoomStepDepthSense=setting.zoomStepLF;

        flag.mouseClick = 2;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
    //if the mouse cursor is on hf plot
    else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf){

        setting.shiftCounterHf++;
        setting.zoomStepHF = (setting.shiftCounterHf)*(0.05*ping->rangeHf_double);
        customPlotHF->yAxis->moveRange(-resultUpHF);

        flag.mouseClick = 4;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
    else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot) {
        setting.shiftCounterDepthSensor++;
        setting.zoomStepDepthSense = (setting.shiftCounterDepthSensor)*(0.05*ping->rangeLf_double);
        depthPlot->yAxis->moveRange(-resultUpLF);

        setting.zoomStepLF = setting.zoomStepDepthSense;
        setting.shiftCounterLf = setting.shiftCounterDepthSensor;

        flag.mouseClick = 8;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
    else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos())) && Format->isOpen_sensorPlot) {
        setting.shiftCounterDepthSensor++;
        setting.zoomStepDepthSense = (setting.shiftCounterDepthSensor)*(0.05*ping->rangeLf_double);
        ui->sensor_graph->yAxis->moveRange(-resultUpLF);

        setting.zoomStepLF = setting.zoomStepDepthSense;
        setting.shiftCounterLf = setting.shiftCounterDepthSensor;

        flag.mouseClick = 16;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }

    //updates the database
    emit clickCounter(setting.shiftCounterLf,setting.shiftCounterHf,setting.shiftCounterDepthSensor);
}

void MainWindow :: plot_offset_verticalShiftDown(){
    /*This function is called every time the ctrl + DOWN arrow keys are pressed
     *moves the plots up by calculating the shift value stored by resultDownLf and resultDownHf
     *the shift value gets incremented every time the keys are pressed.
     */

    //calculates the drop in meters for evry click
    double resultDownHF = 0.05*ping->rangeHf_double;
    double resultDownLF = 0.05*ping->rangeLf_double;

    //if the mouse cursor is on LF plot
    if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf){
        //zoomStepLf is a global variable that stores the new shifts in meters
        //updates with every click
        setting.shiftCounterLf--;
        setting.zoomStepLF = (setting.shiftCounterLf)*(0.05*ping->rangeLf_double);
        customPlotLF->yAxis->moveRange(resultDownLF);

        //if the depth and sensor plots are tied to the LF
        setting.shiftCounterDepthSensor = setting.shiftCounterLf;
        setting.zoomStepDepthSense = setting.zoomStepLF;

        // Update all other plots accordingly
        flag.mouseClick = 2;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
    else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf){

        setting.shiftCounterHf--;
        setting.zoomStepHF = (setting.shiftCounterHf)*(0.05*ping->rangeHf_double);
        customPlotHF->yAxis->moveRange(resultDownHF);

        setting.shiftCounterDepthSensor = setting.shiftCounterHf;
        setting.zoomStepDepthSense = setting.zoomStepHF;

        flag.mouseClick = 4;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
    else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot) {
        setting.shiftCounterDepthSensor--;
        setting.zoomStepDepthSense = (setting.shiftCounterDepthSensor)*(0.05*ping->rangeLf_double);
        depthPlot->yAxis->moveRange(resultDownLF);

        setting.shiftCounterLf = setting.shiftCounterDepthSensor;
        setting.zoomStepLF = setting.zoomStepDepthSense;

        flag.mouseClick = 8;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }
    else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos())) && Format->isOpen_sensorPlot) {
        setting.shiftCounterDepthSensor--;
        setting.zoomStepDepthSense = (setting.shiftCounterDepthSensor)*(0.05*ping->rangeLf_double);
        ui->sensor_graph->yAxis->moveRange(resultDownLF);

        setting.zoomStepLF = setting.zoomStepDepthSense;
        setting.shiftCounterLf = setting.shiftCounterDepthSensor;

        flag.mouseClick = 16;
        plot_range_zoomUpdate();
        flag.mouseClick = 0;
    }

    emit clickCounter(setting.shiftCounterLf,setting.shiftCounterHf,setting.shiftCounterDepthSensor);
}

void MainWindow :: plot_offset_depthSensorDefaultZoom(QMouseEvent* event){
    // User double clicks on plot while holding down ctrl
    if(flag.ctrlKeyPressed == true){
        setting.zoomStepDepthSense=0;

        emit uncheckShowForwardRng(false);
        setting.shiftCounterDepthSensor=0;

        setting.shiftCounterHf=0;
        setting.shiftCounterLf=0;
        setting.zoomStepLF=setting.zoomStepDepthSense;

        plot_offset_zoomResetDepth(event);
        plot_offset_zoomResetSensor(event);

        emit clickCounter(setting.shiftCounterLf,setting.shiftCounterHf,setting.shiftCounterDepthSensor);
    }
}

void MainWindow :: plot_offset_zoomResetPlots() {
    plot_offset_zoomResetLF(nullptr);
    plot_offset_zoomResetHF(nullptr);
}

void MainWindow :: plot_offset_zoomResetLf() {
    plot_offset_zoomResetLF(nullptr);
}

void MainWindow :: plot_offset_zoomResetHf() {
    plot_offset_zoomResetHF(nullptr);
}

void MainWindow :: plot_offset_zoomResetImagePlot(QMouseEvent* event){
    if (tools->selectedTargets().length() == 1)
        target_selectTarget(tools->selectedTargets().first(), true);
}

void MainWindow :: plot_offset_zoomResetLF(QMouseEvent* event){
    /* This function resets zoom to zoomStepLf varible.
     * Also is used to fully reset the lf plot
     * when ctrl + mouse double click
     */

    // User double clicks on plot while holding down ctrl
    if(flag.ctrlKeyPressed == true){
        //checks if the forward range is enabled
        setting.zoomStepLF = 0;

        //send a signal to uncheck the show forward range checkbox.
        emit uncheckShowForwardRng(false);
        //resets clicks
        setting.shiftCounterLf = 0;

        //if lf and depth plots tied
        setting.shiftCounterDepthSensor = 0;
        setting.zoomStepDepthSense = setting.zoomStepLF;

        //update database
        emit clickCounter(setting.shiftCounterLf,setting.shiftCounterHf,setting.shiftCounterDepthSensor);
    }

    // When the user double clicks on the plot, it should reset the plot back
    // to a default view (of 100% zoom)

    flag.zoomAroundBoatLf = true;
    flag.mousePressedFlagLf = 0;
    userWindowInstance->resetLfZoom();

    int width = (Format->plotWidth_LfHf);
    int height = (Format->plotHeight_Lf);

    // By default, set the x plot range based on the sonar range
    double xMax = ping->rangeLf_double;
    double zoomMagnification = 1;
    if (setting.zoomMagnitude_lf > 0.01)
        // Zoom mode that are not "Auto" mode
        zoomMagnification = setting.zoomMagnitude_lf;
    else if (state.GUI_MODE == 2 && !flag.showGap) {
        // "Auto" zoom mode
        xMax -= ping->lastTraceAlt / ping->altitudeFactor;
    }
    double updatedView = 2*xMax * (height*1.0) / (width*1.0) / zoomMagnification;
    double yView2 = ping->lastTracePos - setting.zoomStepLF;

    if (Format->isOpenLf) {
        customPlotLF->yAxis->setRange(yView2 - updatedView, yView2);
    } else {
        customPlotLF->yAxis->setRange(yView2 - 5, yView2);
    }
    customPlotLF->xAxis->setRange(-xMax/zoomMagnification, xMax/zoomMagnification);
    depthPlot->xAxis->setRange(-2, setting.globalDepth);

    bool found;
    double roll_low = ui->sensor_graph->graph(0)->getValueRange(found).lower;
    double roll_high = ui->sensor_graph->graph(0)->getValueRange(found).upper;
    double pitch_low = ui->sensor_graph->graph(1)->getValueRange(found).lower;
    double pitch_high = ui->sensor_graph->graph(1)->getValueRange(found).upper;
    double yaw_low = ui->sensor_graph->graph(2)->getValueRange(found).lower;
    double yaw_high = ui->sensor_graph->graph(2)->getValueRange(found).upper;

    double range_low = std :: min(roll_low,pitch_low);
    range_low = std :: min(range_low,yaw_low);
    double range_high = std :: max(roll_high, pitch_high);
    range_high = std :: max(range_high, yaw_high);
    ui->sensor_graph->xAxis->setRange(range_low,range_high);

    // Update all other plots accordingly
    flag.mouseClick = 2;
    plot_range_zoomUpdate();
    flag.mouseClick = 0;
}

void MainWindow :: plot_offset_zoomResetHF(QMouseEvent* event){
    /*This function is used to fully reset the hf plot
     * when ctrl + mouse double click
     */

    // User double clicks on plot while holding down ctrl
    if(flag.ctrlKeyPressed == true){
        setting.zoomStepHF = 0;
        setting.shiftCounterHf = 0;

        emit clickCounter(setting.shiftCounterLf,setting.shiftCounterHf,setting.shiftCounterDepthSensor);
    }

    userWindowInstance->resetHfZoom();
    flag.zoomAroundBoatHf=true;
    double xMax = ping->rangeHf_double;
    double zoomMagnification = 1;
    if (setting.zoomMagnitude_hf > 0.01)
        zoomMagnification = setting.zoomMagnitude_hf;
    else if (state.GUI_MODE == 2 && !flag.showGap) {
        xMax -= ping->lastTraceAlt / ping->altitudeFactor;
    }
    double rangeDiffHf = plot_range_determineYRange(2.0*xMax,
                                         Format->plotWidth_LfHf,
                                         Format->plotHeight_Hf)/zoomMagnification;
    double yView2HF = ping->lastTracePos - setting.zoomStepHF;

    if (Format->isOpenHf){
        customPlotHF->yAxis->setRange(yView2HF - rangeDiffHf, yView2HF);
    } else {
        customPlotHF->yAxis->setRange(yView2HF - 5, yView2HF);
    }
    customPlotHF->xAxis->setRange(-xMax/zoomMagnification, xMax/zoomMagnification);
//    plot_updateHfZoomLegendText("");

    // Update all other plots accordingly
    flag.mouseClick = 4;
    plot_range_zoomUpdate();
    flag.mouseClick = 0;
}

void MainWindow :: plot_offset_zoomResetDepth(QMouseEvent* event){
    plot_offset_depthSensorDefaultZoom(event);
//    plot_offset_zoomResetLF(nullptr);

//    double updatedView = plot_range_determineYRange(2.0*ping->rangeLf_double,
//                                                    Format->plotWidth_LfHf,
//                                                    Format->plotHeight_depth);
//    if (!Format->isOpenLf)
//        updatedView = plot_range_determineYRange(2.0*ping->rangeHf_double,
//                                             Format->plotWidth_LfHf,
//                                             Format->plotHeight_depth);
//    else if (!Format->isOpenLf && !Format->isOpenHf)
//        updatedView = 20;

//    depthPlot->xAxis->setRange(-2, setting.globalDepth);
//    depthPlot->yAxis->setRange(ping->lastTracePos - updatedView - setting.zoomStepLF, ping->lastTracePos - setting.zoomStepLF);

    flag.zoomAroundBoatLf = true;
    flag.mousePressedFlagLf = 0;
    userWindowInstance->resetLfZoom();

    int width = (Format->plotWidth_LfHf);
    int height = (Format->plotHeight_depth);

    // By default, set the x plot range based on the sonar range
    double xMax = ping->rangeLf_double;
    double zoomMagnification = 1;
    if (setting.zoomMagnitude_lf > 0.01)
        // Zoom mode that are not "Auto" mode
        zoomMagnification = setting.zoomMagnitude_lf;
    else if (state.GUI_MODE == 2 && !flag.showGap) {
        // "Auto" zoom mode
        xMax -= ping->lastTraceAlt / ping->altitudeFactor;
    }
    double updatedView = 20;
    if (width > 0)     updatedView = 2*xMax * (height*1.0) / (width*1.0) / zoomMagnification;
    double yView2 = ping->lastTracePos - setting.zoomStepLF;

    depthPlot->yAxis->setRange(yView2 - updatedView, yView2);
    depthPlot->xAxis->setRange(-2, setting.globalDepth);

    bool found;
    double roll_low = ui->sensor_graph->graph(0)->getValueRange(found).lower;
    double roll_high = ui->sensor_graph->graph(0)->getValueRange(found).upper;
    double pitch_low = ui->sensor_graph->graph(1)->getValueRange(found).lower;
    double pitch_high = ui->sensor_graph->graph(1)->getValueRange(found).upper;
    double yaw_low = ui->sensor_graph->graph(2)->getValueRange(found).lower;
    double yaw_high = ui->sensor_graph->graph(2)->getValueRange(found).upper;

    double range_low = std :: min(roll_low,pitch_low);
    range_low = std :: min(range_low,yaw_low);
    double range_high = std :: max(roll_high, pitch_high);
    range_high = std :: max(range_high, yaw_high);
    ui->sensor_graph->xAxis->setRange(range_low,range_high);

    // Update all other plots accordingly
    flag.mouseClick = 8;
    plot_range_zoomUpdate();
    flag.mouseClick = 0;
}

void MainWindow :: plot_offset_zoomResetSensor(QMouseEvent* event){
    plot_offset_depthSensorDefaultZoom(event);

//    int height = ui->sensor_graph->height();
//    int width = ui->sensor_graph->width();
//    double updatedView = plot_range_determineYRange(2.0*ping->rangeLf_double,
//                                                    Format->plotWidth_LfHf,
//                                                    Format->plotHeight_depth);
//    if (!Format->isOpenLf)
//        updatedView = plot_range_determineYRange(2.0*ping->rangeHf_double,
//                                             Format->plotWidth_LfHf,
//                                             Format->plotHeight_depth);
//    else if (!Format->isOpenLf && !Format->isOpenHf)
//        updatedView = 20;

//    ui->sensor_graph->yAxis->setRange(ping->lastTracePos - updatedView - setting.zoomStepLF, ping->lastTracePos - setting.zoomStepLF);

    flag.zoomAroundBoatLf = true;
    flag.mousePressedFlagLf = 0;
    userWindowInstance->resetLfZoom();

    int width = (Format->plotWidth_LfHf);
    int height = (Format->plotHeight_sensor);

    // By default, set the x plot range based on the sonar range
    double xMax = ping->rangeLf_double;
    double zoomMagnification = 1;
    if (setting.zoomMagnitude_lf > 0.01)
        // Zoom mode that are not "Auto" mode
        zoomMagnification = setting.zoomMagnitude_lf;
    else if (state.GUI_MODE == 2 && !flag.showGap) {
        // "Auto" zoom mode
        xMax -= ping->lastTraceAlt / ping->altitudeFactor;
    }
    double updatedView = 20;
    if (width > 0)     updatedView = 2*xMax * (height*1.0) / (width*1.0) / zoomMagnification;
    double yView2 = ping->lastTracePos - setting.zoomStepLF;

    ui->sensor_graph->yAxis->setRange(yView2 - updatedView, yView2);

    bool found;
    double roll_low = ui->sensor_graph->graph(0)->getValueRange(found).lower;
    double roll_high = ui->sensor_graph->graph(0)->getValueRange(found).upper;
    double pitch_low = ui->sensor_graph->graph(1)->getValueRange(found).lower;
    double pitch_high = ui->sensor_graph->graph(1)->getValueRange(found).upper;
    double yaw_low = ui->sensor_graph->graph(2)->getValueRange(found).lower;
    double yaw_high = ui->sensor_graph->graph(2)->getValueRange(found).upper;

    double range_low = std :: min(roll_low,pitch_low);
    range_low = std :: min(range_low,yaw_low);
    double range_high = std :: max(roll_high, pitch_high);
    range_high = std :: max(range_high, yaw_high);
    ui->sensor_graph->xAxis->setRange(range_low,range_high);
    depthPlot->xAxis->setRange(-2, setting.globalDepth);

    // Update all other plots accordingly
    flag.mouseClick = 16;
    plot_range_zoomUpdate();
    flag.mouseClick = 0;
}

void MainWindow :: plot_offset_zoomResetImage(QMouseEvent* event) {
    if (tools->activeImageBox.targetImage == nullptr)   return;
    if (!ui->imagePlot->hasItem(tools->activeImageBox.targetImage))     return;
    double x1 = tools->activeImageBox.targetImage->topLeft->coords().x();
    double y2 = tools->activeImageBox.targetImage->topLeft->coords().y();
    double x2 = tools->activeImageBox.targetImage->bottomRight->coords().x();
    double y1 = tools->activeImageBox.targetImage->bottomRight->coords().y();
    ui->imagePlot->xAxis->setRange(x1, x2);
    ui->imagePlot->yAxis->setRange(y1, y2);
    ui->imagePlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: plot_offset_zoomResetGps(QMouseEvent* event) {
    ChartWindowInstance->lockToBoat = true;
    ChartWindowInstance->plot_updateRange();
}

void MainWindow :: mouseCursorTimeout() {
    if (flag.measuringTapeActive || flag.targetToolActive || flag.shadowToolActive && !flag.targetToolActive || cropTool->toolActive) {
        mouseTimer.stop();
        flag.cursorHidden = false;
        return;
    }
    mouseTimer.stop();
    flag.cursorHidden = true;
    QCursor cursor = Qt::BlankCursor;
    customPlotLF->setCursor(cursor);
    customPlotHF->setCursor(cursor);
    depthPlot->setCursor(cursor);
    ui->sensor_graph->setCursor(cursor);
    ui->gpsPlot->setCursor(cursor);

}

/****************Bottom Tracking - Model 71****************/

void MainWindow :: bottomTrack_filter(QList<quint16> *list, int size, int multiplier) {
    QList<quint16> filteredList;
    int filterSize = size;
    for (int i = 0; i < 100; i++) {
        filteredList.append(0);
    }
    for (int i = 100; i < list->length(); i++) {
        int total = 0, count = 0;
        for (int j = 0; j < filterSize; j++) {
            int k = i + j - filterSize/2;
            if (k < 100 || k > list->length()-1) continue;
            total += list->at(k);
            count++;
        }
        int avg = 0;
        if (count > 0)  avg = multiplier*total/count;
        /*if (avg > list->at(i))   filteredList.append(0);
        else                     */filteredList.append(/*list->at(i) - */avg);
    }
    *list = filteredList;
}

void MainWindow :: bottomTrack_differentiate(QList<quint16> *list) {
    QList<quint16> filteredList;
    filteredList.append(0);
    for (int i = 1; i < list->length(); i++) {
        if (list->at(i) > list->at(i-1)/* && (list->at(i) - list->at(i-1)) > 0.3*filteredList.at(i-1)*/)   filteredList.append(list->at(i) - list->at(i-1));
        else                     filteredList.append(/*filteredList.at(i-1)*/0);
    }
    *list = filteredList;
}

void MainWindow :: bottomTrack_calibrate(QList<quint16> *list) {
    QList<quint16> filteredList;
    filteredList.append(0);
    int dn = 100;
//    for (int i = 1; i < list->length(); i++) {
//        filteredList.append((quint16)(list->at(i) * (1.0+i/1000.0)));
//    }
    for (int i = 0; i < 200; i++) {
        filteredList.append(list->at(i));
    }
    for (int i = 150; i < 450; i++) {
        int curve = (15*cos((i-300)*M_PI/150)+15);
        if (list->at(i) - curve > 0)
            filteredList.append(list->at(i) - curve);
        else
            filteredList.append(0);
    }
    for (int i = 500; i < list->length(); i++) {
        filteredList.append(list->at(i));
    }
    *list = filteredList;
}

void MainWindow :: bottomTrack_plotCurve(QList<quint16> *list, int curveNum) {
    if (curveNum == 0) {
        graphWindowInstance->removeData1();
        graphWindowInstance->removeData2();
    }
    if (curveNum == 1) {
        for (int i = 110; i < list->length(); i++) {
            graphWindowInstance->addDataToPortSideGraph1(i, list->at(i));
        }
    }
    if (curveNum == 2) {
        for (int i = 110; i < list->length(); i++) {
            graphWindowInstance->addDataToStarBoardSideGraph1(i, list->at(i));
        }
    }
    if (curveNum == 3) {
        for (int i = 110; i < list->length(); i++) {
            graphWindowInstance->addDataToPortSideGraph2(i, list->at(i));
        }
    }
    if (curveNum == 4) {
        for (int i = 110; i < list->length(); i++) {
            graphWindowInstance->addDataToStarBoardSideGraph2(i, list->at(i));
        }
    }
}

QList<qint16> MainWindow :: bottomTrack_convertToIntList(QList<quint16> *list) {
    QList<qint16> list2;
    for (int i = 0; i < list->length(); i++) {
        list2.append(list->at(i));
    }
    return list2;
}

QList<quint16> MainWindow :: bottomTrack_convertToUIntList(QList<qint16> *list) {
    QList<quint16> list2;
    for (int i = 0; i < list->length(); i++) {
        list2.append(list->at(i));
    }
    return list2;
}

QList<quint16> MainWindow :: bottomTrack_findPeaks(QList<quint16> data) {
    /* Find peaks and troughs in data */
    QList<quint16> peaks;
    int max = 0, maxPos = 0;
    for (int i = 120; i < data.size(); i++) {
        if (i > setting.globalDepth / ping->res_lat_lf) {
            break;
        } else if (data.at(i) > data.at(i - 1) && data.at(i) > max) {
            /* Found a peak - save peak index position */
            max = data.at(i);
            maxPos = i;
        } else if (/*data.at(i) == 0 &&*/ max != 0) {
            peaks.append(maxPos);
            max = 0;
            maxPos = 0;
        }
    }
    return peaks;
}

void MainWindow :: bottomTrack_reducePeaksList(QList<quint16> data, QList<quint16> *peaks) {
    QList<quint16> peakVals, peakPos;
    int max = 0, maxPos = 0;
    /* Create list of peak values */
    for (int i = 0; i < peaks->size(); i++) {
        peakVals.append(data.at(peaks->at(i)));
    }
    /* Sort the lists */
    for (int i = 1; i < peaks->size(); i++) {
        for (int j = 1; j < peaks->size() - i; j++) {
            if (peakVals.at(j-1) < peakVals.at(j)) {
                peaks->swap(j-1, j);
                peakVals.swap(j-1, j);
            }
        }
    }
    /* Reduce list to 5 values */
    for (int i = peaks->size(); i > 15; i--) {
        peaks->removeLast();
    }
}

int  MainWindow :: bottomTrack_findBestPeak(QList<quint16> *peaks, int lastPeakPosition) {
    double distanceCriteria = 10.0/((track.depth_queue_port.size()+1.0)/10.0) + 1.0, currentPeakIndex = -1;
    for (int i = 0; i < peaks->size() - 1; i++) {
        if (abs(peaks->at(i) - lastPeakPosition) < distanceCriteria/ping->res_lat_lf) {
            distanceCriteria = abs(peaks->at(i)-lastPeakPosition)*ping->res_lat_lf;
            currentPeakIndex = i;
        }
    }
    if (currentPeakIndex < 0)   return 0;
    else                        return peaks->at(currentPeakIndex);
}

void MainWindow :: bottomTrack_applyProbability(QList<quint16> *list, int kalmanInt) {
    // Apply a probility density function to data based on last result
    int dn = 1000;
    QList<quint16> output;
    bool PlotType = 0;
    if (PlotType == 0) {
        if (kalmanInt > list->length() || kalmanInt < 1) {
            output = *list;
        } else {
            /* Apply probability density function to data */
            int iEnd, iStart;
            if (kalmanInt+dn > list->length()) {
                iEnd = list->length();
            } else {
                iEnd = kalmanInt+dn;
            }
            if (kalmanInt-dn < 0) {
                iStart = 0;
            } else {
                iStart = kalmanInt-dn;
            }
            for (int i = 0; i < iStart; i++) {
                output.append(list->at(i)*0.5);
            }
            for (int i = iStart; i < iEnd; i++) {
                output.append(list->at(i)*(0.5*cos((i-kalmanInt)*M_PI/dn)+1));
            }
            for (int i = iEnd; i < list->length(); i++) {
                output.append(list->at(i)*0.5);
            }
        }
    } else {
        if (kalmanInt > list->length() || kalmanInt < 1) {
            output = *list;
        } else {
            /* Apply probability density function to data */
            int iEnd, iStart;
            if (kalmanInt+dn > list->length()) {
                iEnd = list->length();
            } else {
                iEnd = kalmanInt+dn;
            }
            if (kalmanInt-dn < 0) {
                iStart = 0;
            } else {
                iStart = kalmanInt-dn;
            }
            int tmp = 0;//(quint16)(list->size()-i)/3;
            for (int i = 0; i < iStart; i++) {
                tmp = 50-50*i/(list->size());
                output.append(list->at(i)*0.4*tmp);

            }
            for (int i = iStart; i < iEnd; i++) {
                tmp = 50-50*i/(list->size());
                output.append(list->at(i)*(0.6*cos((i-kalmanInt)*M_PI/dn)+1.0)*tmp);
                //                        output.append(list.at(i)*0.5*tmp);
            }
            for (int i = iEnd; i < list->length(); i++) {
                tmp = 50-50*i/(list->size());
                output.append(list->at(i)*0.3*tmp);
            }
        }
    }
    *list = output;
}

bool MainWindow :: bottomTrack_addTrackedPoint(int mode) {
    if (mode == 1) {
        bool portSideActive = 0;
        bool starBoardSideActive = 0;
        if (ping->lowFrequency == 120) {
            portSideActive = bottomTrack_calculate(ping->data_portHf, 0);
            starBoardSideActive = bottomTrack_calculate(ping->data_stbdHf, 1);
        } else {
            portSideActive = bottomTrack_calculate_72(ping->data_portHf, 0);
            starBoardSideActive = bottomTrack_calculate_72(ping->data_stbdHf, 1);
        }

        if( portSideActive || starBoardSideActive){
            depthPlot->graph(4)->addData(ping->lastTracePos,(track.depth_port+track.depth_stbd)/2);
        } else {
            depthPlot->graph(4)->addData(ping->lastTracePos, ((track.depth_port+track.depth_stbd)/2)+0.5);
        }
        depthPlot->graph(4)->addData(ping->lastTracePos, ((track.depth_port+track.depth_stbd)/2)+0.5);
        depthPlot->graph(2)->addData(ping->lastTracePos, (track.depth_port+track.depth_stbd)/2);
        depthPlot->graph(3)->addData(ping->lastTracePos, ((track.depth_port+track.depth_stbd)/2)+0.5);
        ping->bottomTrackDetected = portSideActive || starBoardSideActive;
        ping->lastTraceAlt = (track.depth_port+track.depth_stbd)/2;
        if(ping->lastTraceAlt > setting.maxAlt){  setting.maxAlt = ping->lastTraceAlt;}
    } else if (mode == 2) {
        bool portSideActive = 0;
        bool starBoardSideActive = 0;
        if (ping->lowFrequency == 120) {
            portSideActive = bottomTrack_calculate(ping->data_portHf, 0);
            starBoardSideActive = bottomTrack_calculate(ping->data_stbdHf, 1);
        } else {
            portSideActive = bottomTrack_calculate_72(ping->data_portHf, 0);
            starBoardSideActive = bottomTrack_calculate_72(ping->data_stbdHf, 1);
        }

        if( portSideActive || starBoardSideActive){
            depthPlot->graph(4)->addData(ping->lastTracePos,(track.depth_port+track.depth_stbd)/2);
        } else {
            depthPlot->graph(4)->addData(ping->lastTracePos, ((track.depth_port+track.depth_stbd)/2)+0.5);
        }
        depthPlot->graph(2)->addData(ping->lastTracePos, (track.depth_port+track.depth_stbd)/2);
        depthPlot->graph(3)->addData(ping->lastTracePos, ((track.depth_port+track.depth_stbd)/2)+0.5);

        ping->bottomTrackDetected = portSideActive || starBoardSideActive;
        ping->lastTraceAlt = (track.depth_port+track.depth_stbd)/2;
        if(ping->lastTraceAlt > setting.maxAlt){  setting.maxAlt = ping->lastTraceAlt;}

        for(int sampleNumber = 0; sampleNumber < ping->samplesToPlotLF; sampleNumber++) {
            if (sampleNumber < 45) {
                //                portSideData1.prepend(0);
                ping->data_portHf.prepend(0);
            }
            if (sampleNumber < ping->samplesToPlotLF - 45) {
                //                starboardSideData1.replace(sampleNumber, starboardSideData1.at(sampleNumber+45));
                ping->data_stbdHf.replace(sampleNumber, ping->data_stbdHf.at(sampleNumber+45));
            } else {
                //                starboardSideData1.append(starboardSideData1.at(sampleNumber-45));
                ping->data_stbdHf.append(ping->data_stbdHf.at(sampleNumber-45));
            }
            ping->data_depth.append(ping->data_portHf.at(sampleNumber) + ping->data_stbdHf.at(sampleNumber));
        }
        ping->samplesToPlotDepth = ping->samplesToPlotLF;
    } else if (mode == 3) {
        bool portSideActive = 0;
        if (ping->lowFrequency == 120)      portSideActive = bottomTrack_calculate(ping->data_depth, 1);
        else   {
            portSideActive = bottomTrack_calculate_72(ping->data_depth, 1);
            depthPlot->graph(0)->addData(ping->lastTracePos, (track.depth_stbd*setting.convert_unit));
        }
        double nonAvgAlt = track.depth_stbd;
        if (ping->lowFrequency == 120)      ping->lastTraceAlt = (bottomTrack_computeAverage1(track.sampleNum_stbd) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
        else  {
            if (portSideActive)
                ping->lastTraceAlt = bottomTracking_computeAverage_72(track.sampleNum_stbd) * ping->res_lat_lf * ping->altitudeFactor;
        }
        ping->bottomTrackDetected = portSideActive;
        if (portSideActive) {
            depthPlot->graph(4)->addData(ping->lastTracePos, ((nonAvgAlt)));
            depthPlot->graph(3)->addData(ping->lastTracePos,(nonAvgAlt));
        } else {
            depthPlot->graph(4)->addData(ping->lastTracePos, nonAvgAlt+0.5);
            depthPlot->graph(3)->addData(ping->lastTracePos, nonAvgAlt+0.5);
        }
        depthPlot->graph(2)->addData(ping->lastTracePos, (ping->lastTraceAlt));
        if(ping->lastTraceAlt > setting.maxAlt){  setting.maxAlt = ping->lastTraceAlt;}
    } else if (mode == 4) {
        bool portSideActive = bottomTrack_calculate(ping->data_depth, 0);
        double nonAvgAlt = ((track.sampleNum_port) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
        if (track.depth_queue_port.size() >= setting.averageDepthQueueLength-2 && abs(nonAvgAlt - ping->lastTraceAlt) < 3.0)
            track.depth_port = (bottomTrack_computeAverage1(track.sampleNum_port) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
        else if (track.depth_queue_port.size() < setting.averageDepthQueueLength-2)
            track.depth_port = (bottomTrack_computeAverage1(track.sampleNum_port) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;

        ping->bottomTrackDetected = portSideActive;
        if (portSideActive) {
            depthPlot->graph(4)->addData(ping->lastTracePos,(nonAvgAlt));
        } else {
            depthPlot->graph(4)->addData(ping->lastTracePos, ((nonAvgAlt))+0.5);
        }
        depthPlot->graph(2)->addData(ping->lastTracePos, (ping->lastTraceAlt));
        depthPlot->graph(3)->addData(ping->lastTracePos, ((nonAvgAlt)));
        if (track.depth_queue_stbd.length() > 0)      ping->lastTraceAlt = 0.5*(track.depth_stbd + track.depth_port);
        else                                          ping->lastTraceAlt = track.depth_port;
        depthPlot->graph(5)->addData(ping->lastTracePos, ((nonAvgAlt)));
        if(ping->lastTraceAlt > setting.maxAlt){  setting.maxAlt = ping->lastTraceAlt;}
    } else if (mode == 5) {
        bool stbdSideActive = bottomTrack_calculate(ping->data_depth, 1);
        double nonAvgAlt = ((track.sampleNum_stbd) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
        if (track.depth_queue_stbd.size() >= setting.averageDepthQueueLength-2 && abs(nonAvgAlt - ping->lastTraceAlt) < 3.0)
            track.depth_stbd = (bottomTrack_computeAverage2(track.sampleNum_stbd) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
        else if (track.depth_queue_stbd.size() < setting.averageDepthQueueLength-2)
            track.depth_stbd = (bottomTrack_computeAverage2(track.sampleNum_stbd) * setting.speed_of_sound)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;

        ping->bottomTrackDetected = stbdSideActive;
        if (stbdSideActive) {
            depthPlot->graph(4)->addData(ping->lastTracePos,(nonAvgAlt));
        } else {
            depthPlot->graph(4)->addData(ping->lastTracePos, ((nonAvgAlt))+0.5);
        }
        depthPlot->graph(2)->addData(ping->lastTracePos, (ping->lastTraceAlt));
        depthPlot->graph(3)->addData(ping->lastTracePos, ((nonAvgAlt)));
        if (track.depth_queue_port.length() > 0)      ping->lastTraceAlt = 0.5*(track.depth_stbd + track.depth_port);
        else                                          ping->lastTraceAlt = track.depth_stbd;
        depthPlot->graph(6)->addData(ping->lastTracePos, ((nonAvgAlt)));
        if(ping->lastTraceAlt > setting.maxAlt){  setting.maxAlt = ping->lastTraceAlt;}
    }
    return 1;
}

/**
 * @brief MainWindow::computeDepth
 * To calculate the current and average forward depth using the
 * cross point sample number.
 */
int  MainWindow :: bottomTrack_computeAverage1(int crossPointSampleNum1) {
    int sumOfQueue = 0;
    static int depthSimulVal = 1000;
    static int avgCrossPointSampleNum = 0;

    if(state.GUI_MODE == 0) {
        if(depthSimulVal < 200) {
            depthSimulVal = 4200;
        }
    }
    if(track.depth_queue_port.size() < setting.averageDepthQueueLength) {
        if(state.GUI_MODE) {
            track.depth_queue_port.enqueue(crossPointSampleNum1);
        }
        else {
            track.depth_queue_port.enqueue(depthSimulVal);
            depthSimulVal -= 10;
        }
    }
    else{
        track.depth_queue_port.dequeue();
        if(state.GUI_MODE) {
            track.depth_queue_port.enqueue(crossPointSampleNum1);
        }
        else {
            track.depth_queue_port.enqueue(depthSimulVal);
            depthSimulVal -= 0;
        }
    }
    for (int i = 0; i< track.depth_queue_port.size(); i++) {
        sumOfQueue += track.depth_queue_port.at(i);
    }
    double avgDepth = 0;
    avgDepth = sumOfQueue/track.depth_queue_port.size();
    return (int) avgDepth;
}

int  MainWindow :: bottomTrack_computeAverage2(int crossPointSampleNum1) {
    int sumOfQueue = 0;
    static int depthSimulVal = 1000;
    static int avgCrossPointSampleNum = 0;

    if(state.GUI_MODE == 0) {
        if(depthSimulVal < 200) {
            depthSimulVal = 4200;
        }
    }
    if(track.depth_queue_stbd.size() < setting.averageDepthQueueLength) {
        if(state.GUI_MODE) {
            track.depth_queue_stbd.enqueue(crossPointSampleNum1);
        }
        else {
            track.depth_queue_stbd.enqueue(depthSimulVal);
            depthSimulVal -= 10;
        }
    }
    else{
        track.depth_queue_stbd.dequeue();
        if(state.GUI_MODE) {
            track.depth_queue_stbd.enqueue(crossPointSampleNum1);
        }
        else {
            track.depth_queue_stbd.enqueue(depthSimulVal);
            depthSimulVal -= 0;
        }
    }
    for (int i = 0; i< track.depth_queue_stbd.size(); i++) {
        sumOfQueue += track.depth_queue_stbd.at(i);
    }
    double avgDepth = 0;
    avgDepth = sumOfQueue/track.depth_queue_stbd.size();
    return (int) avgDepth;
}

void MainWindow :: bottomTrack_clearQueue() {
    track.depth_queue_port.clear();
    track.depth_queue_stbd.clear();
    track.bottomTrackingCounter = 0;
}

/**
 * @brief MainWindow::kalman_preProcess1
 * @param list
 * This function isn't used anymore.  The board does this automatically.
 * Only use if reading old files from hardware that does no preprocess
 */
bool MainWindow :: bottomTrack_calculate(QList<quint16> list, int PlotType) {

    QString fileName = recorder.recPath + "/DepthChannel.txt";
    static QFile textFile;
    static int k = 0;
    if (k == 0) {
        textFile.setFileName(fileName);
        textFile.open(QIODevice::ReadWrite | QIODevice::Text);
        k++;
    }
    QByteArray data;
    for (int j = 0; j < list.length(); j++) {
        data.append(QString::number(list.at(j)) + " ");
    }
    data.append("\n");

    textFile.write(data);

    //QList<qint16> list2 = bottomTrack_convertToIntList(&list);
    int max = 0, maxPos = 0;
//    bottomTrack_plotCurve(&list, 0);
    bottomTrack_filter(&list, 100, 100);
//    bottomTrack_plotCurve(&list, 1);
    bottomTrack_filter(&list, 20, 1);
//    if (PlotType == 0)
//        bottomTrack_applyProbability(&list, kalmanNum_chA);
//    else
//        bottomTrack_applyProbability(&list, kalmanNum_chB);
//    bottomTrack_plotCurve(&list, 2);
    bottomTrack_differentiate(&list);
//    bottomTrack_plotCurve(&list, 3);
    bottomTrack_filter(&list, 20, 100);
//    bottomTrack_plotCurve(&list, 4);
    graphWindowInstance->refreshPlot1();
    graphWindowInstance->refreshPlot2();
    // Create a list of peaks
    QList<quint16> peaks = bottomTrack_findPeaks(list);
    // Reduce the list to the top 5 peaks
    bottomTrack_reducePeaksList(list, &peaks);
    // Pick peak that is most probable
    if (PlotType == 0)
        maxPos = bottomTrack_findBestPeak(&peaks, track.sampleNum_port);
    else
        maxPos = bottomTrack_findBestPeak(&peaks, track.sampleNum_stbd);
    //maxPos = bottomTrack_computeAverage1(maxPos);
    //bottomTrack_calibrate(&list);
    ping->data_depth = list;
    //bottomTrack_differentiate(&list);

//    for (int i = 110; i < list.length(); i++) {
//        if (list.at(i) > max) {
//            max = list.at(i);
//            maxPos = i;
//        }
//    }
    if (PlotType == 0)   {
//        if (abs(maxPos - kalmanNum_chA) > 50 && bottomTrackingCounter > 100) {
//            //                bottomTrackingCounter--;
//            return 0;
//        }
        //            pos_max = computeDepth(pos_max);
        track.sampleNum_port = maxPos;
        //track.depth_port = (setting.speed_of_sound * track.sampleNum_port)/ (2* ping->currentSamplingFrequency);
        track.bottomTrackingCounter++;
        depthPlot->graph(0)->addData(ping->lastTracePos, (track.depth_port*setting.convert_unit));//add kalmandepth (blue line) to plot
        return true;
    }  else {
//        if (abs(maxPos - kalmanNum_chB) > 50 && bottomTrackingCounter > 100) {
//            //                    bottomTrackingCounter--;
//            return 0;
//        }
        //                pos_max = computeDepth1(pos_max);
        track.sampleNum_stbd = maxPos;
        //track.depth_stbd = (setting.speed_of_sound * track.sampleNum_stbd)/ (2* ping->currentSamplingFrequency);
        track.bottomTrackingCounter++;
        depthPlot->graph(1)->addData(ping->lastTracePos, (track.depth_stbd*setting.convert_unit));
        return true;
    }


    int Nstart = 20, Nstop = 100, i, dn = 400;
    double tmp=0;
    int kalmanInt;
    if (!PlotType)  {
        kalmanInt = (int) track.sampleNum_port;
    }
    else {
        kalmanInt = (int) track.sampleNum_stbd;
    }
    bool useProbFunc = true;
    if (track.bottomTrackingCounter < 5 && track.bottomTrackingCounter < 5)    useProbFunc = false;
    double k_avg = 10, sum = 0;
    QList<quint16> Q1, Q2;

    //graphWindowInstance->removeData2();

    // Apply a probility density function to data based on last result
    if (PlotType == 0) {
        if (useProbFunc) {
            if (kalmanInt > list.length() || kalmanInt < 1) {
                Q1 = list;
            } else {
                /* Apply probability density function to data */
                i = 0;
                int iEnd, iStart;
                if (kalmanInt+dn > list.length()) {
                    iEnd = list.length();
                } else {
                    iEnd = kalmanInt+dn;
                }
                if (kalmanInt-dn < 0) {
                    iStart = 0;
                } else {
                    iStart = kalmanInt-dn;
                }
                for (i = 0; i < iStart; i++) {
                    Q1.append(list.at(i)*0.5);
                }
                for (i = iStart; i < iEnd; i++) {
                    Q1.append(list.at(i)*(0.5*cos((i-kalmanInt)*M_PI/dn)+1.0));
                }
                for (i = iEnd; i < list.length(); i++) {
                    Q1.append(list.at(i)*0.5);
                }
            }
        } else {
            Q1 = list;
        }
    } else {
        if (useProbFunc) {
            if (kalmanInt > list.length() || kalmanInt < 1) {
                Q1 = list;
            } else {
                /* Apply probability density function to data */
                i = 0;
                int iEnd, iStart;
                if (kalmanInt+dn > list.length()) {
                    iEnd = list.length();
                } else {
                    iEnd = kalmanInt+dn;
                }
                if (kalmanInt-dn < 0) {
                    iStart = 0;
                } else {
                    iStart = kalmanInt-dn;
                }
                tmp = (quint16)(list.size()-i)/3;
                for (i = 0; i < iStart; i++) {
                    tmp = 50-50*i/(list.size());
                    Q1.append(list.at(i)*0.4*tmp);

                }
                for (i = iStart; i < iEnd; i++) {
                    tmp = 50-50*i/(list.size());
                    Q1.append(list.at(i)*(0.6*cos((i-kalmanInt)*M_PI/dn)+1.0)*tmp);
                    //                        Q1.append(list.at(i)*0.5*tmp);
                }
                for (i = iEnd; i < list.length(); i++) {
                    tmp = 50-50*i/(list.size());
                    Q1.append(list.at(i)*0.3*tmp);
                }
            }
        } else {
            Q1 = list;
        }
    }

    int peak = 0, trough = 0, max_diff = 0, // maximum dsifference between peak and trough
            pos_max = 0,  // position max_diff was taken
            strength = 0; // strength of peak at max_diff
    for (i = 0; i < Nstart; i++) {
        Q2.append(0);
    }
    /* Create running average of ping data */
    for (i = Nstart; i < Q1.size() - Nstop; i++) {
        sum = 0;
        for (int j = 0; j < k_avg; j++) {
            sum += Q1.at(i+j)/k_avg;
        }
        Q2.append((quint16)sum);
    }
    for (i = Q1.size() - Nstop; i < Q1.size(); i++) {
        Q2.append(0);
    }

    /* Find peaks and troughs in data */
    for (i = Nstart + 20; i < list.size() - Nstop; i++) {
        //graphWindowInstance->addDataToPortSideGraph4(i - 1, Q2.at(i));
        if (Q2.at(i) > Q2.at(i - 1)) {
            /* Found a peak - save peak index position */
            peak = i;
        } else {
            if (peak == i - 1 && Q2.at(peak) - Q2.at(trough) > max_diff) {
                /* Found position of biggest peak to trough */
                int denominator = Q2.at(trough);
                if (!Q2.at(trough))  denominator = 1;
                max_diff = Q2.at(peak) - Q2.at(trough);
                pos_max = i;
                strength = Q2.at(peak)*100/denominator;
            }
            trough = i;
        }
    }
    if (pos_max < 50)   return false;

    int thresholdLevel = 125;
    if ((setting.speed_of_sound * pos_max)/ (2* ping->currentSamplingFrequency) < 8)  thresholdLevel = 200;

    if (strength > thresholdLevel) {
        if (PlotType == 0)   {
            if (abs(pos_max - track.sampleNum_port) > 50 && track.bottomTrackingCounter > 100) {
                //                bottomTrackingCounter--;
                return 0;
            }
            //            pos_max = computeDepth(pos_max);
            track.sampleNum_port = pos_max;
            track.depth_port = (setting.speed_of_sound * track.sampleNum_port)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
            track.bottomTrackingCounter++;
            depthPlot->graph(0)->addData(ping->lastTracePos, (track.depth_port*setting.convert_unit));//add kalmandepth (blue line) to plot
            return true;
        }  else {
            if (strength > 150) {
                if (abs(pos_max - track.sampleNum_stbd) > 50 && track.bottomTrackingCounter > 100) {
                    //                    bottomTrackingCounter--;
                    return 0;
                }
                //                pos_max = computeDepth1(pos_max);
                track.sampleNum_stbd = pos_max;
                track.depth_stbd = (setting.speed_of_sound * track.sampleNum_stbd)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
                track.bottomTrackingCounter++;
                depthPlot->graph(1)->addData(ping->lastTracePos, (track.depth_stbd*setting.convert_unit));
                return true;
            } else {
                return false;
            }
        }
    } else {
        if (PlotType == 0)   {
            depthPlot->graph(0)->addData(ping->lastTracePos, (track.depth_port*setting.convert_unit));
        } else {
            depthPlot->graph(1)->addData(ping->lastTracePos, (track.depth_stbd*setting.convert_unit));
        }
        //        bottomTrackingCounter--;
        return false;
    }

}

/****************Bottom Tracking - Model 92****************/

int  MainWindow :: bottomTracking_computeAverage_72(int crossPointSampleNum1) {
    int sumOfQueue = 0;
    static int depthSimulVal = 1000;
    static int avgCrossPointSampleNum = 0;

    if(state.GUI_MODE == 0) {
        if(depthSimulVal < 200) {
            depthSimulVal = 4200;
        }
    }
    if(track.depth_queue_port.size() < setting.averageDepthQueueLength) {
        if(state.GUI_MODE) {
            track.depth_queue_port.enqueue(crossPointSampleNum1);
        }
        else {
            track.depth_queue_port.enqueue(depthSimulVal);
            depthSimulVal -= 10;
        }
    }
    else{
        track.depth_queue_port.dequeue();
        if(state.GUI_MODE) {
            track.depth_queue_port.enqueue(crossPointSampleNum1);
        }
        else {
            track.depth_queue_port.enqueue(depthSimulVal);
            depthSimulVal -= 0;
        }
    }
    for (int i = 0; i< track.depth_queue_port.size(); i++) {
        sumOfQueue += track.depth_queue_port.at(i);
    }
    double avgDepth = 0;
    avgDepth = sumOfQueue/track.depth_queue_port.size();
    return (int) avgDepth;
}

bool MainWindow :: bottomTrack_calculate_72(QList<quint16> list, int PlotType) {

    int Nstart = 20, Nstop = 100, i, dn = 400;
    double tmp=0;
    int kalmanInt;
    if (!PlotType)  {
        kalmanInt = (int) track.sampleNum_port;
    }
    else {
        kalmanInt = (int) track.sampleNum_stbd;
    }
    bool useProbFunc = true;
    if (track.bottomTrackingCounter < 5 && track.bottomTrackingCounter < 5)    useProbFunc = false;
    double k_avg = 10, sum = 0;
    QList<quint16> Q1, Q2;

    //graphWindowInstance->removeData2();

    // Apply a probility denbsity function to data based on last result
    if (PlotType == 0) {
        if (useProbFunc) {
            if (kalmanInt > list.length() || kalmanInt < 1) {
                Q1 = list;
            } else {
                /* Apply probability density function to data */
                i = 0;
                int iEnd, iStart;
                if (kalmanInt+dn > list.length()) {
                    iEnd = list.length();
                } else {
                    iEnd = kalmanInt+dn;
                }
                if (kalmanInt-dn < 0) {
                    iStart = 0;
                } else {
                    iStart = kalmanInt-dn;
                }
                for (i = 0; i < iStart; i++) {
                    Q1.append(list.at(i)*0.5);
                }
                for (i = iStart; i < iEnd; i++) {
                    Q1.append(list.at(i)*(0.5*cos((i-kalmanInt)*M_PI/dn)+1.0));
                }
                for (i = iEnd; i < list.length(); i++) {
                    Q1.append(list.at(i)*0.5);
                }
            }
        } else {
            Q1 = list;
        }
    } else {
        if (useProbFunc) {
            if (kalmanInt > list.length() || kalmanInt < 1) {
                Q1 = list;
            } else {
                /* Apply probability density function to data */
                i = 0;
                int iEnd, iStart;
                if (kalmanInt+dn > list.length()) {
                    iEnd = list.length();
                } else {
                    iEnd = kalmanInt+dn;
                }
                if (kalmanInt-dn < 0) {
                    iStart = 0;
                } else {
                    iStart = kalmanInt-dn;
                }
                tmp = (quint16)(list.size()-i)/3;
                for (i = 0; i < iStart; i++) {
                    tmp = 50-50*i/(list.size());
                    Q1.append(list.at(i)*0.4*tmp);

                }
                for (i = iStart; i < iEnd; i++) {
                    tmp = 50-50*i/(list.size());
                    Q1.append(list.at(i)*(0.6*cos((i-kalmanInt)*M_PI/dn)+1.0)*tmp);
                    //                        Q1.append(list.at(i)*0.5*tmp);
                }
                for (i = iEnd; i < list.length(); i++) {
                    tmp = 50-50*i/(list.size());
                    Q1.append(list.at(i)*0.3*tmp);
                }
            }
        } else {
            Q1 = list;
        }
    }

    int peak = 0, trough = 0, max_diff = 0, // maximum dsifference between peak and trough
            pos_max = 0,  // position max_diff was taken
            strength = 0; // strength of peak at max_diff
    for (i = 0; i < Nstart; i++) {
        Q2.append(0);
    }
    /* Create running average of ping data */
    for (i = Nstart; i < Q1.size() - Nstop; i++) {
        sum = 0;
        for (int j = 0; j < k_avg; j++) {
            sum += Q1.at(i+j)/k_avg;
        }
        Q2.append((quint16)sum);
    }
    for (i = Q1.size() - Nstop; i < Q1.size(); i++) {
        Q2.append(0);
    }

    /* Find peaks and troughs in data */
    for (i = Nstart + 20; i < list.size() - Nstop; i++) {
        //graphWindowInstance->addDataToPortSideGraph4(i - 1, Q2.at(i));
        if (Q2.at(i) > Q2.at(i - 1)) {
            /* Found a peak - save peak index position */
            peak = i;
        } else {
            if (peak == i - 1 && Q2.at(peak) - Q2.at(trough) > max_diff) {
                /* Found position of biggest peak to trough */
                int denominator = Q2.at(trough);
                if (!Q2.at(trough))  denominator = 1;
                max_diff = Q2.at(peak) - Q2.at(trough);
                pos_max = i;
                strength = Q2.at(peak)*100/denominator;
            }
            trough = i;
        }
    }
    if (pos_max < 50)   return false;
    bottomTrack_setSignalStrength(strength, PlotType);
    //        starboardSideData1 = list;
    //        portSideData1 = Q1;
    //        portSideData2 = Q2;
    if (strength > 125) {
        if (PlotType == 0)   {
            if (abs(pos_max - track.sampleNum_port) > 50 && track.bottomTrackingCounter > 100) {
                //                bottomTrackingCounter--;
                return 0;
            }
            //            pos_max = computeDepth(pos_max);
            track.sampleNum_port = pos_max;
            track.depth_port = (setting.speed_of_sound * track.sampleNum_port)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
            track.bottomTrackingCounter++;
            depthPlot->graph(0)->addData(ping->lastTracePos, (track.depth_port*setting.convert_unit));//add kalmandepth (blue line) to plot
            return true;
        }  else {
            if (strength > 150) {
                if (abs(pos_max - track.sampleNum_stbd) > 50 && track.bottomTrackingCounter > 100) {
                    //                    bottomTrackingCounter--;
                    return 0;
                }
                //                pos_max = computeDepth1(pos_max);
                track.sampleNum_stbd = pos_max;
                track.depth_stbd = (setting.speed_of_sound * track.sampleNum_stbd)/ (2* ping->currentSamplingFrequency) * ping->altitudeFactor;
                track.bottomTrackingCounter++;
                depthPlot->graph(1)->addData(ping->lastTracePos, (track.depth_stbd*setting.convert_unit));
                return true;
            } else {
                return false;
            }
        }
    } else {
        if (PlotType == 0)   {
            depthPlot->graph(0)->addData(ping->lastTracePos, (track.depth_port*setting.convert_unit));
        } else {
            depthPlot->graph(1)->addData(ping->lastTracePos, (track.depth_stbd*setting.convert_unit));
        }
        //        bottomTrackingCounter--;
        return false;
    }

}

void MainWindow :: bottomTrack_updateIcon() {
    if (track.avg_signalStrength > 150) {
        track.signalTimer.stop();
    } else {
        track.signalTimer.start(1000);
    }
    if (track.signalState == 4)
        track.signalState = 0;
    else
        track.signalState++;

    if (track.signalState == 4)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal4.png"));
    else if (track.signalState == 3)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal3.png"));
    else if (track.signalState == 2)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal2.png"));
    else if (track.signalState == 1)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal1.png"));
    else
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal0.png"));
}

void MainWindow :: bottomTrack_setSignalStrength(int strength, int side) {
    if (side == 0)  strength += 25;
    track.signal_queue.enqueue(strength);
    if (track.signal_queue.length() > 9) {
        track.signal_queue.dequeue();
    }
    strength = 0;
    for (int i = 0; i< track.signal_queue.size(); i++) {
        strength += track.signal_queue.at(i);
    }
    strength /= 10;

    // Start the icon animation when signal strength drops below 150
    if (track.avg_signalStrength > 150 && strength <= 150) {
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal0.png"));
        track.signalState = 0;
        track.signalTimer.start(1000);
    } else if (strength > 150) {
        track.signalTimer.stop();
    }
    track.avg_signalStrength = strength;

    if (strength > 240)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal4.png"));
    else if (strength > 210)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal3.png"));
    else if (strength > 180)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal2.png"));
    else if (strength > 150)
        ui->label_signal->setPixmap(QPixmap(":/icons/icons/signal1.png"));
}

/****************Flag****************/

void MainWindow :: target_DeleteSelectedItems(){
    // Delete all flags selected on the flagList

    QList<targetItem*> selectedList = tools->selectedTargets();

    // For each item in the list, remove it
    for(int i = 0; i < selectedList.size(); i++){
        targetItem *item = selectedList.at(i);
        tools->deleteTarget(item);
    }

    target_SaveToFile();
    ui->actionSaveTarget->setEnabled(false);
    target_clearImagePlot();
    ChartWindowInstance->legend_updateAll();
    plot_replotAll();
    tools->renumberTargets();
}

void MainWindow :: target_DeleteSelectedWaypoints(){
    // Delete all flags selected on the flagList
    flag.targetIgnoreSelectionChange = true;

    QList<waypointItem*> selectedList = tools->selectedWaypoints();
//    if (selectedList.first())

    // For each item in the list, remove it
    for(int i = 0; i < selectedList.size(); i++){
        waypointItem *item = selectedList.at(i);
        if (item == tools->tracker.nextWpt)
            tools->tracker.resetWptAnchor();
        tools->deleteWaypoint(item);
    }

    ChartWindowInstance->legend_updateAll();
    tools->renumberTargets();
    tools->tracker.displayRoute();
    plot_replotAll();
    flag.targetIgnoreSelectionChange = false;
}

bool MainWindow :: target_NewTarget(double x, double y, QCustomPlot *plot){
    // Place a flag mark on the plot after mouse click
    // (if flag tool is activated)
    // Get the x/y coords of the mouse
    // *plot indicates snipping/target mode
    //    nullptr = placed target, lfPlot = snipped on Lf, hfPlot = snipped on Hf

    double angle_coefficient=0;
    if(ping->angleOrientation==70){ // Angle scan
        angle_coefficient = 1/qTan(ping->degree * M_PI/180);
    }
    double yCursorCorrected = y-abs(x*angle_coefficient);

    PingMapItem mapItem = dataMap.findItemWithY(yCursorCorrected);
    int pingNumber = mapItem.pingNumber;
    if (!mapItem.isValid)                    return 0;
    if (pingNumber < ping->pingNumFirst)     return 0;
    if (flag.PlotAngleScan && flag.showGap && abs(x) < mapItem.altitude)     return 0;

    // Create a new flag (on flagList and in the object QList)
    // Set default name
    //    flagActive=true;
    QString flagName = "Target";

    if(tools->targetList.isEmpty()){
        targetCounter=0;
    }

    //finds the largest number from the target numbers from the JASFLAG file
    //and sets target counter to that number
    for(int i=0;i<counterList.size();i++){
        if(targetCounter<counterList.at(i)){
            targetCounter = counterList.at(i);
        }
    }

    if(targetCounter <= 999){
        targetCounter++;
        QString c_format = QString::number(targetCounter);
        c_format = c_format.rightJustified(3,'0');
        flagName.prepend(c_format+" ");
    }
    else{
        flagName.prepend(QString::number(targetCounter)+" ");
    }

    // Search Hash maps to find required info
    QString mouseGpsString = "";

    int gpsValid = 0;
    double GpsLattitude = mapItem.lattitude;
    double GpsLongitude = mapItem.longitude;
    double GpsHeading = mapItem.heading;
    if (DEVELOPER_TESTING) {
        GpsLattitude = 44.5;
        GpsLongitude = -63.01;
        GpsHeading = 0;
    }
    if (GpsLattitude > -400)    gpsValid = 1;

    double ratio; //ratio = d / 6378137.0  radius of the earth
    ratio = abs(x) / 6378137.0;

    double sonarAngle = ping->angleOrientation;
    if (x < 0)  sonarAngle *= -1;

    double latitude2 = (180 / M_PI) * qAsin(qSin(GpsLattitude * M_PI/180) * qCos(ratio)
                                        + qCos(GpsLattitude * M_PI/180) * qSin(ratio)
                                        * qCos((sonarAngle + GpsHeading) * M_PI/180));
    double longitude2 = (180 / M_PI) * (GpsLongitude * M_PI/180
                                        + qAtan((qSin((sonarAngle + GpsHeading) * M_PI/180) * qSin(ratio)
                                        * qCos(GpsLattitude * M_PI/180))/(qCos(ratio)
                                        - qSin(GpsLattitude * M_PI/180) * qSin(latitude2 * M_PI/180))));
    if (!gpsValid) {
        latitude2 = -500;
        longitude2 = -500;
    }

    // Add to the object flag list
    double resolution = ping->res_lat_lf;
    if (plot == customPlotHF)
        resolution = ping->res_lat_hf;
    int sampleNum = x / resolution;
    if (!flag.showGap) {
        if (sampleNum > 0)
            sampleNum += mapItem.altitude / resolution;
        else
            sampleNum -= mapItem.altitude / resolution;
    }
    targetItem obj(tools->targetList.length(), flagName, latitude2, longitude2, pingNumber - ping->pingNumFirst,
                   sampleNum, x, y);
    obj.ListItem = new QListWidgetItem(flagName);
    obj.snippedImage = tools->activeImageBox.plotType;
    obj.imageHeight = tools->activeImageBox.height;
    obj.imageWidth = tools->activeImageBox.width;
    obj.area = obj.imageHeight * obj.imageWidth;
    if (gpsValid) {
        obj.speed = mapItem.speed;
        obj.distance = mapItem.distance;
        obj.heading = mapItem.heading;
    }
    QString text = obj.determineName(flagName, ui->targetListWidget->font());
    obj.ListItem->setText(text + obj.determineAreaText());
    if (obj.snippedImage > 0) {
        QString imageName = player.openedFileName.mid(player.openedFileName.lastIndexOf("/")+1);// obj.Name;//obj.imagePath.mid(obj.imagePath.lastIndexOf("/"));
        imageName = imageName.mid(0, imageName.length() - 4);
        imageName += " " + obj.Name + ".png";
        obj.imageName = imageName;
        obj.imagePath = player.openedFileName.mid(0, player.openedFileName.lastIndexOf("/")+1) + imageName;
        target_saveSnippedImage(obj.imagePath);
    }
    else {
        obj.imageName = "";
        obj.imagePath = "";
    }

    QIcon cameraIcon;
    if (obj.snippedImage == 0)
        cameraIcon = QIcon(":/icons/icons/blankFlagIcon.png");
    else if (obj.snippedImage == 1)
        cameraIcon = QIcon(":/icons/icons/camera_blue.png");
    else if (obj.snippedImage == 2)
        cameraIcon = QIcon(":/icons/icons/camera_red.png");
    obj.ListItem->setIcon(cameraIcon);

    // Add the item to flagList
    tools->appendTargetItem(obj);
    int flagList_currIndex = tools->targetList.length() - 1;

    // Add the icon to customPlotLF
    target_AddIconToPlot(x, y, 10, &tools->targetList[flagList_currIndex]);

    // Add flag to chart plot
    if (state.GNSS_MODE > 1 && gpsValid)
        ChartWindowInstance->target_addFlag(latitude2, longitude2, 0.0002, flagList_currIndex);

    QPixmap pixmap1 = targetItem::getIconPixmap(true, flag.targetIconType); // solid flag

    if (tools->targetList[flagList_currIndex].icon_chart != nullptr)
        tools->targetList[flagList_currIndex].icon_chart->setPixmap(pixmap1);
    ChartWindowInstance->target_resizeAllTargets();
    ChartWindowInstance->plot_replot();

    target_selectAllTargets(false);
    plot_replotAll();
    target_sortByPing();
    target_SaveToFile();
    for (int index = 0; index < tools->targetList.count(); index++) {
        if (tools->targetList[index].ListItem == obj.ListItem) {
            target_selectTarget(&tools->targetList[index], true);
            target_ShowImageAndBox();
            break;
        }
    }
    // Send information to confirm pop window, if the target was created by a mouse click
//    emit sendParametersToFlag(ui->flagList->count()-1, 1);
//    flagInstance->show();
//    flag.targetToolActive = false;
    return 1;
}

void MainWindow :: target_NewWaypoint(double Lattitude, double Longitude) {
    if (!flag.waypointToolActive)    return;
    if (state.GUI_MODE == 2)        return;
    target_selectAllWaypoints(false);
    QString name = QString::number(tools->determineNewName(true)).rightJustified(3, '0') + " Waypoint";
    waypointItem obj(tools->waypointList.length(), name, Lattitude, Longitude, -1, -1, -1, -1);
    obj.ListItem = new QListWidgetItem(obj.Name);
    obj.GpsHeading = -1;
    obj.importedWaypoint = true; // Not actually imported, but this variable helps convert to a waypointItem

    //splits the name so that we get the counter and Target
    obj.ListItem->setIcon(QIcon(":/icons/icons/blankFlagIcon.png"));
    tools->appendTargetItem(obj);
    ChartWindowInstance->target_addWaypoint(Lattitude, Longitude, 0.001, obj.listIndex);
    target_selectWaypoint(&tools->waypointList[obj.listIndex], true, false);
    ChartWindowInstance->plot_replot();

    tools->notifyUser();
//    tools->targetAudio->setVolume(50);
//    tools->targetAudio->play();
}

void MainWindow :: target_convertToTarget(waypointItem *item) {
    // Determine the sample number
    double resolution = ping->res_lat_lf;
    if (ping->rangeLf_int < 1)
        resolution = ping->res_lat_hf;
    int sampleNum = item->xCoord / resolution;
    PingMapItem mapItem = dataMap.last();//findItemWithY(yCursorCorrected);
    if (!mapItem.isValid)       return;
    if (!flag.showGap) {
        if (sampleNum > 0)
            sampleNum += mapItem.altitude / resolution;
        else
            sampleNum -= mapItem.altitude / resolution;
    }
    item->sample = sampleNum;

    // Determine ping number
    item->ping = ping->pingNumCurrent - ping->pingNumFirst;

    // Convert waypoint to target and append to list
    tools->convertWaypointToTarget(item);

    // Save new target to flagfile
    target_SaveToFile();

    // Select next waypoint
    waypointItem *nextItem = tools->tracker.passedWaypoint(item);
    if (tools->tracker.validWpt && nextItem != nullptr) {
        target_selectAllWaypoints(false);
        target_selectWaypoint(nextItem, true, false);
        tools->tracker.anchorWaypoint(nextItem);
    } else {
        if (tools->selectedWaypoints().length() == 1)
            target_selectAllWaypoints(false);
    }
}

void MainWindow :: target_MouseClick(QCustomPlot *plotPtr, QMouseEvent* event, bool doubleClick) {
    if (!flag.targetToolActive)     return;
    if (state.GUI_MODE == 2 && state.PLAY_MODE != 4)   player_playData(false);

    double xPosition = plotPtr->xAxis->pixelToCoord(event->pos().x());
    double yPosition = plotPtr->yAxis->pixelToCoord(event->pos().y());

    double angle_coefficient = 0;
    if (ping->angleOrientation!=90)     angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
    double yCursorCorrected = yPosition - abs(xPosition * angle_coefficient);
    if (yCursorCorrected > ping->y_UpperBound || yCursorCorrected < ping->y_LowerBound)     return;
    PingMapItem mapItem = dataMap.findItemWithY(yCursorCorrected);
    if (!mapItem.isValid)        return;

    if (doubleClick) {
        tools->targetState = false;
        tools->activeImageBox.plotType = 0;
        tools->activeImageBox.height = 0.0;
        tools->activeImageBox.width = 0.0;
        target_NewTarget(xPosition, yPosition, plotPtr);
    }
    else if (tools->targetState == false) {
        target_selectAllTargets(false);
        target_removeSnipBoxes();

        if (plotPtr == customPlotLF)    tools->activeImageBox.plotType = 1;
        else if (plotPtr == customPlotHF)    tools->activeImageBox.plotType = 2;

        // Clear any previous snipped images
        target_clearImagePlot();
        tools->activeImageBox.SnipBox = nullptr;
        double x1 = xPosition;
        double y1 = yPosition;

        tools->activeImageBox.start_point.setX(x1);
        tools->activeImageBox.start_point.setY(y1);

        QPen distancePen1;
        distancePen1.setColor(Qt::white);
        distancePen1.setWidth(3);
        distancePen1.setStyle(Qt::DashLine);

        tools->activeImageBox.SnipBox = new QCPItemRect(plotPtr);
        tools->activeImageBox.SnipBox->setPen(distancePen1);
        tools->activeImageBox.SnipBox->setSelectedPen(distancePen1);
        tools->activeImageBox.SnipBox->topLeft->setCoords(x1, y1);
        tools->activeImageBox.SnipBox->bottomRight->setCoords(x1, y1);
        tools->activeImageBox.SnipBox->setLayer("tools1");

        tools->targetState = true;
    }
    else {
        if (plotPtr == customPlotLF && tools->activeImageBox.plotType == 2)        return;
        else if (plotPtr == customPlotHF && tools->activeImageBox.plotType == 1)    return;
        // Do not continue if the user does not click on the same plot twice
        tools->targetState = false;

        double x1 = tools->activeImageBox.start_point.x();
        double y1 = tools->activeImageBox.start_point.y();
        double x2 = xPosition;
        double y2 = yPosition;

        tools->activeImageBox.end_point.setX(x2);
        tools->activeImageBox.end_point.setY(y2);

        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);

        tools->activeImageBox.width = qFabs(x2-x1);
        tools->activeImageBox.height = qFabs(y2-y1);

        target_CopyImageBoxToPlot(plotPtr);
        target_selectAllTargets(false);
        plot_replotAll();
        bool done = target_NewTarget((x1+x2)/2, (y1+y2)/2, plotPtr);
        if (!done) {
            tools->targetState = true;
            target_clearImagePlot();
            return;
        }
        plotPtr->removeItem(tools->activeImageBox.SnipBox);
        tools->activeImageBox.SnipBox = nullptr;
        tools->notifyUser();
    }
}

void MainWindow :: target_DialogOkay(QString flagName, targetItem *item, bool newItem){
    // Okay button was clicked on flagInstance window

    // Print the name of the flag on viewList
    QStringList list2;
    static int increment = 0;
    static int wptNum = 1;

    QDateTime time = QDateTime::currentDateTime();

    QListWidgetItem *listItem = item->ListItem;
    QString text = item->determineName(flagName, ui->targetListWidget->font());
    listItem->setText(text + item->determineAreaText());
    if (item->importedWaypoint)     ui->waypointListWidget->editItem(listItem);
    else {
        ui->targetListWidget->editItem(listItem);
        item->renameTarget(flagName);
        target_SaveToFile();
    }
}

void MainWindow :: target_ShowIconsOnPlot(bool playSound){
    // Plot all flags onto customPlot
    bool value_found = 0;
    if(colorMapNum_queue.size() > 0){
        // If there is a color map on plot, run a for loop to find which flags to add to plot
        for (int i = 0; i < tools->targetList.size(); ++i) {
            targetItem *item = &tools->targetList[i];
            if (item->icon_lf == nullptr && item->ping < ping->pingNumCurrent) {
                // If the flag is not on plot, and the ping number is below the current ping number,
                // then run a hashiterator to find corresponding yCoordinate
                if (dataMap.containsPing(item->ping + ping->pingNumFirst)) {
                    // It found the hash key (the ping number)
                    //qDebug() << "Verified HashMap >>" << abs(hash.value()) << flag_items[i].flag_ping + ping_change;
                    item->xCoord = item->sample * ping->res_lat_lf;
                    if (!flag.showGap) {
                        if (item->sample > 0)
                            item->xCoord -= ping->lastTraceAlt;
                        else
                            item->xCoord += ping->lastTraceAlt;
                    }
                    item->yCoord = dataMap.findItem(item->ping + ping->pingNumFirst).yCoord
                            + abs(item->xCoord*qCos(ping->degree*M_PI/180));

                    target_AddIconToPlot(item->xCoord, item->yCoord, 5, item);

                    if (playSound) {
                        tools->targetAudio->setVolume(50);
                        tools->targetAudio->play();
                    }
                    if (flag.targetAutoplay) {
                        // Automatically select the next target and show the image box
                        target_selectAllTargets(false);
                        target_selectTarget(&tools->targetList[i], true);
                        target_ShowImageAndBox();
                    }
                }
//            } else if (setting.projectTargets && tools->targetList[i].ping < ping->pingNumCurrent && tools->targetList[i].line_alt != nullptr) {
//                if (tools->targetList[i].yCoord > ping->y_LowerBound && tools->targetList[i].yCoord < ping->lastTracePos) {
//                    tools->targetList[i].showAltLine(true);
//                } else {
//                    tools->targetList[i].showAltLine(false);
//                }
            }
        }
    }
}

void MainWindow :: target_RemoveAllIconsBelowMap(double traceYCoord){
    // Remove all flags from plot if they are below traceYCoord
    for (int i = tools->targetList.size() - 1; i >= 0; i--){
        if (tools->targetList[i].yCoord < traceYCoord && tools->targetList[i].icon_lf != nullptr && tools->targetList[i].icon_hf != nullptr){
            tools->targetList[i].removeIconFromLfHfPlot();
            if (i == tools->activeImageBox.currentItem) {
                target_removeSnipBoxes();
            }
        } else if (tools->targetList[i].line_alt != nullptr) {
            if (tools->targetList[i].line_alt->start->coords().y() < traceYCoord)   tools->targetList[i].showScanLines(false);
        }
    }
    for (int i = tools->waypointList.size() - 1; i >= 0; i--){
        if (tools->waypointList[i].yCoord < traceYCoord && tools->waypointList[i].scanAreaCircle.length() > 0){
            tools->waypointList[i].removeScanAreaFromPlot();
        }
    }
    customPlotLF->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: target_AddIconToPlot (double xCoord, double yCoord, double size, targetItem *item) {
    // Add a flag to the given coordinates
    item->addIconToPlot(customPlotLF, customPlotHF, depthPlot, ui->gpsPlot, ui->sensor_graph, flag.targetIconType);
    item->resizeIcon(40, 40, xCoord, yCoord);
    ChartWindowInstance->track_getCurveCoordinates((ping->pingNumCurrent - ping->pingNumFirst) - item->ping, *item);

    double angle_coefficient=0;
    if(ping->angleOrientation==70){
        // Angle scan
        angle_coefficient = 1/qTan(ping->degree * M_PI/180);
    }
    yCoord = yCoord-abs(xCoord*angle_coefficient);

    item->setAltItemPosition(setting.globalDepth, 0, yCoord);
    item->setSensorItemPosition(yCoord);
    if (!setting.projectTargets) {
        item->showScanLines(false);
    } else if (yCoord > ping->y_LowerBound && yCoord < ping->lastTracePos) {
        item->showScanLines(true);
    } else {
        item->showScanLines(false);
    }
}

void MainWindow :: target_UpdateIconType() {
    flag.targetIgnoreSelectionChange = true;
    tools->updateAllTargetIcons(flag.targetIconType);
    flag.targetIgnoreSelectionChange = false;
    plot_replotAll();
}

void MainWindow :: target_updateAltPlotPosition() {
    for (int i = 0; i < tools->targetList.length(); i++) {
        if (tools->targetList[i].icon_lf == nullptr)    continue;
        double yCoord = tools->targetList[i].yCoord;
        if (!setting.projectTargets) {
            tools->targetList[i].showScanLines(false);
        } else if (yCoord > ping->y_LowerBound && yCoord < ping->y_UpperBound) {
            tools->targetList[i].showScanLines(true);
        } else {
            tools->targetList[i].showScanLines(false);
        }
    }
    ui->sensor_graph->replot(QCustomPlot::rpQueuedReplot);
    depthPlot->replot(QCustomPlot::rpQueuedReplot);
    ui->gpsPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: target_LoadFromFile(){
    // Load all flags from ".JASFLAG" file

    ui->targetListWidget->show();
    ui->waypointListWidget->hide();
    QFile file (player.openedFileName+"flag");
    file.open(QIODevice::ReadOnly);

    int i=0;
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        QString str(line);
        QStringList list;
        QString name;
        list=str.split(",");

        targetItem obj(tools->targetList.length(), list.at(0), list.at(1).toDouble(), list.at(2).toDouble(),
                       list.at(3).toInt(), list.at(4).toInt(), -1, -1);
        obj.ListItem = new QListWidgetItem(obj.Name);

        //splits the name so that we get the counter and Target
        name = list.at(0);
        QString firstThree = name.mid(0,3);
        counterList.append(firstThree.toInt());

        obj.xCoord = 0;//list.at(5).toDouble();
        obj.yCoord = -1;//list.at(6).toDouble();
        if (list.length() > 10) {
            obj.heading = list.at(7).toDouble();
            obj.snippedImage = list.at(8).toInt();
            obj.imageHeight=list.at(9).toDouble();
            obj.imageWidth=list.at(10).toDouble();
            obj.area = obj.imageHeight * obj.imageWidth;
            QString text = obj.determineName(list.at(0), ui->targetListWidget->font());
            obj.ListItem->setText(text + obj.determineAreaText());
            if (obj.snippedImage > 0) {
                QString imageName = player.openedFileName.mid(player.openedFileName.lastIndexOf("/")+1);// obj.Name;//obj.imagePath.mid(obj.imagePath.lastIndexOf("/"));
                imageName = imageName.mid(0, imageName.length() - 4);
                imageName += " " + obj.Name + ".png";
                obj.imageName = imageName;
                obj.imagePath = player.openedFileName.mid(0, player.openedFileName.lastIndexOf("/")+1) + imageName;
            }
            else {
                obj.imageName = "";
                obj.imagePath = "";
            }
        }

        QIcon cameraIcon;
        if (obj.snippedImage == 0)
            cameraIcon = QIcon(":/icons/icons/blankFlagIcon.png");
        else if (obj.snippedImage == 1)
            cameraIcon = QIcon(":/icons/icons/camera_blue.png");
        else if (obj.snippedImage == 2)
            cameraIcon = QIcon(":/icons/icons/camera_red.png");
        obj.ListItem->setIcon(cameraIcon);

        tools->appendTargetItem(obj);
        ChartWindowInstance->target_addFlag(obj.Lattitude, obj.Longitude, 0.0002, i);

        i++;
    }
    file.close();
    target_sortByPing();
}

void MainWindow :: target_SaveToFile() {
    // Save all Strings in Flag List to .JASFLAG file
    //    QFile file(openedFileName+"flag");
    //    file.open(QIODevice::ReadOnly);
    QStringList list;
    QFile fileNew;
    //create a file with the same name
    if (state.GUI_MODE == 2) {
        if (player.openedFileName == "")   return;
        else    fileNew.setFileName(player.openedFileName+"FLAG");
    } else {
        if (recorder.fileName == "" && state.GUI_MODE < 2)  return;
        else    fileNew.setFileName(recorder.fileName+"FLAG");
    }
//    QFile fileNew(player.openedFileName+"flag");
    bool status = fileNew.open(QIODevice::WriteOnly);
    QTextStream out(&fileNew);

    QByteArray flagData = "";
    for (int i = 0; i < tools->targetList.length(); i++) {
        out << tools->targetList[i].Name << ", " << QString::number(tools->targetList[i].Lattitude, 'f', 10) << ", "
            << QString::number(tools->targetList[i].Longitude, 'f', 10) << ", "
            << tools->targetList[i].ping << ", " << tools->targetList[i].sample << ", "
            << tools->targetList[i].xCoord << ", " << tools->targetList[i].yCoord << ", "
            << tools->targetList[i].heading<< ", " <<tools->targetList[i].snippedImage<< ", " <<tools->targetList[i].imageHeight<<", " <<tools->targetList[i].imageWidth
            << endl;
    }
    fileNew.close();

    if (trkptGPX.exists()) {
        // Copy entire file
        trkptGPX.seek(0);
        QByteArray trkptTxt = trkptGPX.readAll();
        QString trkptStr = trkptTxt;

        // Find section with all waypoints, if it exists
        QStringList trkptList = trkptStr.split("<trk>");
        QStringList wptList = trkptList.at(0).split("<wpt ");
        QStringList wptItems;
        int wptInsertPosition = wptList.at(0).length();
        if (wptList.length() > 1) {
            for (int i = 1; i < wptList.length(); i++) {
                wptItems = wptList.at(i).split("\"");
            }
        }

        // Remove all existing waypoints
        for (int i = 1; i < wptList.length(); i++) {
            trkptTxt = trkptTxt.remove(wptInsertPosition, wptList.at(i).length() + 5);
        }

        // Add all flags
        QByteArray wptString = "";
        for (int i = 0; i < tools->targetList.length(); i++) {
            wptString.append("<wpt lat=\"" + QString::number(tools->targetList[i].Lattitude, 'f', 7) + "\" lon=\""
                             + QString::number(tools->targetList[i].Longitude, 'f', 7) + "\">");
            wptString.append("<time>"+QString::number(gnssController->Year()) + "-" + QString("%1").arg(gnssController->Month(), 2, 10, QChar('0')) + "-"
                             +QString("%1").arg(gnssController->Day(), 2, 10, QChar('0')) + "T" + QString("%1").arg(gnssController->Hour(), 2, 10, QChar('0')) + ":"
                             +QString("%1").arg(gnssController->Minute(), 2, 10, QChar('0')) + ":" + QString("%1").arg(gnssController->Second(), 2, 10, QChar('0'))+
                             "Z</time>");
            wptString.append("<name>" + tools->targetList[i].Name + "</name></wpt>\n    ");
        }
        if (wptString.length() > 0) {
            trkptTxt.insert(wptInsertPosition, wptString);
            //
            trkptGPX.seek(0);
            trkptGPX.write(trkptTxt);
        }
    }
}

void MainWindow :: target_GoToTarget() {
    // Go to the selected target when user double clicks on flag list item
    targetItem item = tools->findTargetItem(ui->targetListWidget->currentItem());
    if (item.listIndex < 0)     return;
    int pingOnFile = item.ping+ping->pingNumFirst;

    // Remove all target icons from plot
    tools->removeAllTargetIcons();

    // Play 200 pings before the target
    player_playFromPing((int)pingOnFile - 200);
    // Unpause file
    if (state.PLAY_MODE == 4)   player_playData(true);
//    player_playData(true);
//    player_showSegment(((int)pingOnFile) - 200, ((int)pingOnFile) + 200);

//    pingHash.clear();
    //    removeFlagFromPlot(pingOnFile);
}

void MainWindow :: target_renameTarget(){
    targetItem *item;
    if (state.GUI_MODE == 2)
        item = tools->selectedTargets().last();
    else        item = tools->selectedWaypoints().last();
    if (item->listIndex < 0)    return;

    flagInstance->getParametersToFlag(item, 0);
    flagInstance->show();
}

void MainWindow :: target_exportTarget(){
    // Export to GPX
    QDateTime time = QDateTime::currentDateTime();
    QString filter = "GPS Exchange Format (*.gpx)";
    QString fileName = recorder.recPath + "/wpt-" + time.toString("ddMMyy_hhmm") + ".JAS";
    fileName  = QFileDialog::getSaveFileName(this, tr("Save Recorded Sonar File"),
                                             fileName, filter, &filter);
    QFile targetsGpx;
    targetsGpx.setFileName(fileName);
    targetsGpx.open(QIODevice::ReadWrite | QIODevice::Text);

    // Save all selected targets
    QByteArray wptString = "";
//    if (targetsGpx.size() == 0) {
    wptString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
    wptString.append("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\" creator =\"Jaimi\"> \n");
    wptString.append("  <name>"+windowTitle+"</name> \n");
    for (int j = 0; j < tools->targetList.length(); j++) {
        targetItem item = tools->targetList[j];
        if (!item.selected)     continue;
        wptString.append("  <wpt lat=\"" + QString::number(item.Lattitude, 'f', 7) + "\" lon=\""
                         + QString::number(item.Longitude, 'f', 7) + "\">");
        wptString.append("<time>"+QString::number(gnssController->Year()) + "-" + QString("%1").arg(gnssController->Month(), 2, 10, QChar('0')) + "-"
                         +QString("%1").arg(gnssController->Day(), 2, 10, QChar('0')) + "T" + QString("%1").arg(gnssController->Hour(), 2, 10, QChar('0')) + ":"
                         +QString("%1").arg(gnssController->Minute(), 2, 10, QChar('0')) + ":" + QString("%1").arg(gnssController->Second(), 2, 10, QChar('0'))+
                         "Z</time>");
        wptString.append("<name>" + item.Name + "</name></wpt>\n");
    }

    // Save all trackpoints of recorded file
    // Open the JASLOG file as a separate QFile
    if (!state.GNSS_MODE) {
        if (!player.GPSFile.exists()) {
            wptString.append("</gpx>");
            targetsGpx.write(wptString);
            targetsGpx.close();
            return;
        }
    }
    QFile GpsFile2(player.GPSFile.fileName());
    GpsFile2.open(QIODevice::ReadWrite | QIODevice::Text);

    QByteArray trkptTxt;
    trkptTxt.append("  <trk><trkseg> \n");

    QString GpsFileLine;
    QStringList GpsFileItems;
    QStringList fName;
    QString previousTimestamp;

    while (1) {
        GpsFileLine = GpsFile2.readLine();
        GpsFileItems = GpsFileLine.split("\t");
        if (GpsFileItems.length() < 2)  break;

        // Split text into separate QStringLists
        QStringList time = GpsFileItems.at(1).split(" ");
        QStringList time1 = time.at(0).split("=");
        QStringList time2 = time1.at(1).split(":");
        QStringList coords = GpsFileItems.at(2).split(" ");
        QStringList coords1 = coords.at(0).split("=");

        // read time
        if (time2.length() > 3) {
//            GpsItems.Hour = time2.at(0).toInt();
//            GpsItems.Minute = time2.at(1).toInt();
//            GpsItems.Second = time2.at(2).toInt();
//            GpsItems.Millisecond = time2.at(3).toInt();
        }

        if (previousTimestamp == GpsFileItems.at(1))    continue;
        previousTimestamp = GpsFileItems.at(1);

        trkptTxt.append("        <trkpt lat=\""+coords1.at(1)+"\" lon=\""+coords.at(1)+"\"> \n");
        trkptTxt.append("        <time>"+QString::number(gnssController->Year()) + "-" + QString("%1").arg(gnssController->Month(), 2, 10, QChar('0')) + "-"
                        + QString("%1").arg(gnssController->Day(), 2, 10, QChar('0')) + "T" + time2.at(0) + ":"
                        + time2.at(1) + ":" + time2.at(2) + "Z</time></trkpt> \n");
    }
    trkptTxt.append("    </trkseg></trk> \n");
    trkptTxt.append("</gpx>");

    targetsGpx.write(wptString);
    targetsGpx.write(trkptTxt);
    targetsGpx.close();
}

void MainWindow :: target_ImportWaypoints(QString sonarFileName) {
    // Load Waypoints to flagList and GPS plot
    ChartWindowInstance->path_resetTrackpoints();
    tools->removeAllWaypointsFromList();
    ui->targetListWidget->hide();
    if (DEVELOPER_TESTING)      ui->targetListWidget->show();
    ui->waypointListWidget->show();
    GnssController gnssChecker;
    QFile file (sonarFileName+"FLAG");
    if (file.exists()) {
        file.open(QIODevice::ReadOnly);

        int i=0;
        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            QString str(line);
            QStringList list;
            QString name;
            list=str.split(",");

            waypointItem obj(tools->waypointList.length(), list.at(0), list.at(1).toDouble(), list.at(2).toDouble(),
                           list.at(3).toInt(), list.at(4).toInt(), -1, -1);
            obj.ListItem = new QListWidgetItem(obj.Name);

            //splits the name so that we get the counter and Target
            name = list.at(0);
            QString firstThree = name.mid(0,3);
            counterList.append(firstThree.toInt());

            obj.xCoord = 0;//list.at(5).toDouble();
            obj.yCoord = -1;//list.at(6).toDouble();
            if (list.length() > 10) {
                obj.heading = list.at(7).toDouble();
                obj.snippedImage = list.at(8).toInt();
                obj.imageHeight=list.at(9).toDouble();
                obj.imageWidth=list.at(10).toDouble();
                obj.area = obj.imageHeight * obj.imageWidth;
                QString text = obj.determineName(list.at(0), ui->targetListWidget->font());
                obj.ListItem->setText(text + obj.determineAreaText());
                if (obj.snippedImage > 0) {
                    QString imageName = sonarFileName.mid(sonarFileName.lastIndexOf("/")+1);// obj.Name;//obj.imagePath.mid(obj.imagePath.lastIndexOf("/"));
                    imageName = imageName.mid(0, imageName.length() - 4);
                    imageName += " " + obj.Name + ".png";
                    obj.imageName = imageName;
                    obj.imagePath = sonarFileName.mid(0, sonarFileName.lastIndexOf("/")+1) + imageName;
                }
                else {
                    obj.imageName = "";
                    obj.imagePath = "";
                }
            }

            QIcon cameraIcon;
            if (obj.snippedImage == 0)
                cameraIcon = QIcon(":/icons/icons/blankFlagIcon.png");
            else
                cameraIcon = QIcon(":/icons/icons/camera_grey.png");
            obj.ListItem->setIcon(cameraIcon);
            obj.importedWaypoint = true;

            tools->appendTargetItem(obj);
            ChartWindowInstance->target_addWaypoint(obj.Lattitude, obj.Longitude, 0.0002, i);

            i++;
        }
    }
    file.close();

    // Load trackpoints to GPS Plot
    QFile TrackpointFile (sonarFileName+"LOG");
    if (TrackpointFile.exists()) {
        TrackpointFile.open(QIODevice::ReadOnly);

        QString GpsFileLine;
        QStringList GpsFileItems;
        QStringList fName;

        while (1) {
            // Read line from file
            GpsFileLine = TrackpointFile.readLine();

            // Split items from line to read individual words and numbers
            GpsFileItems = GpsFileLine.split("\t");

            if (GpsFileLine == "") {
                // Break loop if the file has reached the end
                break;
            }
            // The ping number of the sonar file and the .JASLOG file match -> therefore files are aligned
            if (GpsFileItems.length() < 2)  break;

            // Split text into separate QStringLists
            QStringList time = GpsFileItems.at(1).split(" ");
            QStringList time1 = time.at(0).split("=");
            QStringList time2 = time1.at(1).split(":");
            QStringList coords = GpsFileItems.at(2).split(" ");
            QStringList coords1 = coords.at(0).split("=");

            // read time
            if (time2.length() > 3) {
                gnssChecker.updateTime(time2.at(0).toInt(), time2.at(1).toInt(), time2.at(2).toInt());
    //                    GpsItems.Hour = time2.at(0).toInt();
    //                    GpsItems.Minute = time2.at(1).toInt();
    //                    GpsItems.Second = time2.at(2).toInt();
    //                    GpsItems.Millisecond = time2.at(3).toInt();
            }

            // read the GPS coordinates
            if (coords.length() > 1) {
                gnssChecker.updateCoords(coords1.at(1).toDouble(), coords.at(1).toDouble(), GpsFileItems.at(0).mid(0,8).toInt());
    //                    GpsItems.Lattitude_Deg = coords1.at(1).toDouble();
    //                    GpsItems.Longitude_Deg = coords.at(1).toDouble();
            } else      break;

            if (gnssChecker.validCoords)
                ChartWindowInstance->path_addTrackpoints(gnssChecker.Longitude(), gnssChecker.Latitude());
        }
    }
    TrackpointFile.close();

    // Resize window to fit all imported data
    bool found = 0;
    ChartWindowInstance->plot_resize(ChartWindowInstance->trackpoints->getKeyRange(found),
                                    ChartWindowInstance->trackpoints->getValueRange(found));
    target_sortWaypoints();
    ChartWindowInstance->plot_replot();
}

void MainWindow :: target_DisplayWayPoint(int num){
    setting.wayPointBtnState=num;
    for(int index=0;index<tools->targetList.size();index++){
        if(!customPlotLF->hasItem(tools->targetList[index].icon_lf)){
            continue;
        }

        if(num==1){
            tools->targetList[index].icon_lf->setVisible(true);
            tools->targetList[index].icon_hf->setVisible(false);
        }
        if(num==2){
            tools->targetList[index].icon_lf->setVisible(false);
            tools->targetList[index].icon_hf->setVisible(true);
        }
        if(num==3){
            tools->targetList[index].icon_lf->setVisible(true);
            tools->targetList[index].icon_hf->setVisible(true);
        }
        if(num==4){
            tools->targetList[index].icon_lf->setVisible(false);
            tools->targetList[index].icon_hf->setVisible(false);
        }
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: target_sortByPing() {
    flag.targetIgnoreSelectionChange = true;
    tools->sortAllTargets();
    flag.targetIgnoreSelectionChange = false;
    target_SaveToFile();
    tools->renumberTargets();
}

void MainWindow :: target_sortWaypoints() {
    flag.targetIgnoreSelectionChange = true;
    tools->sortAllWaypoints(gnssController->Latitude(), gnssController->Longitude());
    if (tools->tracker.route.length() > 0) {
        tools->tracker.route[0]->selectItem(select, flag.targetIconType);
        tools->tracker.anchorWaypoint(tools->tracker.route[0]);
    }
    flag.targetIgnoreSelectionChange = false;
    tools->renumberTargets();
    ui->gpsPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: target_closeFile() {
   tools->removeAllTargetsFromList();
   ui->targetListWidget->clear();
//   if (tools->importedFile != "") {
//       if (tools->importedFile.toLower() == "jas")
//           target_ImportTargets(tools->importedFile);
//       else if (tools->importedFile.toLower() == "gpx")
//           Gnss_readGpxFile();
//   }
}

void MainWindow :: target_selectAllTargets(bool select) {
    flag.targetIgnoreSelectionChange = true;
    tools->selectAllTargets(select, flag.targetIconType);
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: target_selectAllWaypoints(bool select) {
    flag.targetIgnoreSelectionChange = true;
    waypointItem item;
    if (!select)        tools->tracker.anchorWaypoint(&item);
    if (!select)        tools->tracker.setVisibility();
    tools->selectAllWaypoints(select, flag.targetIconType);
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: target_selectTarget(int index, bool select) {
    if ((index < 0) || (index >= tools->targetList.length()))     return;
    flag.targetIgnoreSelectionChange = true;
    tools->targetList[index].selectItem(select, flag.targetIconType);
    if (select) {
//        tools->tracker.anchorWaypoint(tools->targetList[index]);
        ui->targetListWidget->setCurrentItem(tools->targetList[index].ListItem);
        if (flagInstance->isVisible()) {
            flagInstance->getParametersToFlag(&tools->targetList[index], 0);
            flagInstance->show();
        }
    }
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: target_selectTarget(targetItem *item, bool select) {
    flag.targetIgnoreSelectionChange = true;
    item->selectItem(select, flag.targetIconType);
    if (select) {
        ui->targetListWidget->setCurrentItem(item->ListItem);
        if (flagInstance->isVisible()) {
            flagInstance->getParametersToFlag(item, 0);
            flagInstance->show();
        }
    }
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: target_selectWaypoint(waypointItem *item, bool select, bool addToRoute) {
    // First, deselect all plot items
    if (select) {
        QList <QCPAbstractItem*> selected = ui->gpsPlot->selectedItems();
        for (int i = 0; i < selected.length(); i++) {
            selected[i]->setSelected(false);
        }
    }

    flag.targetIgnoreSelectionChange = true;
    item->selectItem(select, flag.targetIconType);
    if (select) {
        ui->waypointListWidget->setCurrentItem(item->ListItem);
    }
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: target_setStartWaypoint() {
    if (tools->selectedWaypoints().count() != 1)    return;

    flag.targetIgnoreSelectionChange = true;
    tools->setStartPoint(tools->selectedWaypoints().first());
    flag.targetIgnoreSelectionChange = false;
    ui->gpsPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: target_clearImagePlot() {
    ui->imagePlot->clearItems();
    ui->imagePlot->clearPlottables();
    ui->imagePlot->replot(QCustomPlot::rpQueuedReplot);
    tools->activeImageBox.targetImage = nullptr;
}

void MainWindow :: target_removeSnipBoxes() {
    target_clearImagePlot();

    if (customPlotLF->hasItem(tools->activeImageBox.LfBox)) {
        customPlotLF->removeItem(tools->activeImageBox.LfBox);
    }
    tools->activeImageBox.LfBox = nullptr;
    if (customPlotHF->hasItem(tools->activeImageBox.HfBox)) {
        customPlotHF->removeItem(tools->activeImageBox.HfBox);
    }
    tools->activeImageBox.HfBox = nullptr;
    tools->activeImageBox.currentItem = -1;
}

void MainWindow :: target_ShowImageAndBox() {
    // Open the snipped flag image and display it on imageTool plot
    target_removeSnipBoxes();

    if (state.GUI_MODE == 2) {
        if (tools->selectedTargets().length() != 1) {    return;}
    } else {
        if (tools->selectedWaypoints().length() != 1) {    return;}
    }

//    int currentTargetIndex = tools->selectedTargets().first();
    targetItem *item;
    if (state.GUI_MODE == 2)
        item = tools->selectedTargets().first();//tools->waypointList[currentTargetIndex];
    else
        item = tools->selectedWaypoints().first();
    if (item->listIndex < 0)    return;

    tools->activeImageBox.targetImage = new QCPItemPixmap(ui->imagePlot);
    tools->activeImageBox.targetImage->setParent(ui->imagePlot);
    QPixmap imagePixmap(item->imagePath);
    tools->activeImageBox.targetImage->setPixmap(imagePixmap);

    ui->imagePlot->xAxis->setRange(0,imagePixmap.height());
    ui->imagePlot->yAxis->setRange(0,imagePixmap.height());
    tools->activeImageBox.targetImage->setScaled(true);
    tools->activeImageBox.targetImage->topLeft->setCoords(0, imagePixmap.height());
    tools->activeImageBox.targetImage->bottomRight->setCoords(imagePixmap.height(),0);
    ui->imagePlot->replot(QCustomPlot::rpQueuedReplot);

    if (item->yCoord < 0 | item->icon_lf == nullptr)
        return;
    tools->activeImageBox.LfBox = new QCPItemRect(customPlotLF);
    tools->activeImageBox.HfBox = new QCPItemRect(customPlotHF);
    tools->activeImageBox.currentItem = item->listIndex;

    QPen distancePen1;
    distancePen1.setColor(Qt::white);
    distancePen1.setWidth(3);
    distancePen1.setStyle(Qt::DashLine);

    tools->activeImageBox.LfBox->setPen(distancePen1);
    tools->activeImageBox.LfBox->setSelectedPen(distancePen1);
    tools->activeImageBox.LfBox->setLayer("tools1");
    tools->activeImageBox.LfBox->topLeft->setCoords(item->xCoord - item->imageWidth/2,
                            item->yCoord + item->imageHeight/2);
    tools->activeImageBox.LfBox->bottomRight->setCoords(item->xCoord + item->imageWidth/2,
                            item->yCoord - item->imageHeight/2);

    tools->activeImageBox.HfBox->setPen(distancePen1);
    tools->activeImageBox.HfBox->setSelectedPen(distancePen1);
    tools->activeImageBox.HfBox->setLayer("tools1");
    tools->activeImageBox.HfBox->topLeft->setCoords(item->xCoord - item->imageWidth/2,
                            item->yCoord + item->imageHeight/2);
    tools->activeImageBox.HfBox->bottomRight->setCoords(item->xCoord + item->imageWidth/2,
                            item->yCoord - item->imageHeight/2);
}

void MainWindow :: target_UpdatePlot(QCustomPlot *plotPtr, QMouseEvent* event) {
    // Called when mouse hovers over plot and the snipping tool is active
    if (!flag.targetToolActive)   return;

    if (tools->targetState) {
//        plotPtr->setCursor(Qt::CrossCursor);

        double x2 = plotPtr->xAxis->pixelToCoord(event->pos().x());
        double y2 = plotPtr->yAxis->pixelToCoord(event->pos().y());

        double x1 = tools->activeImageBox.start_point.x();
        double y1 = tools->activeImageBox.start_point.y();

        double minX = (x1 < x2) ? x1 : x2;
        double maxX = (x1 > x2) ? x1 : x2;
        double minY = (y1 < y2) ? y1 : y2;
        double maxY = (y1 > y2) ? y1 : y2;

        if (plotPtr->hasItem(tools->activeImageBox.SnipBox)) {
            tools->activeImageBox.SnipBox->topLeft->setCoords(minX, maxY);
            tools->activeImageBox.SnipBox->bottomRight->setCoords(maxX, minY);
        }

        plotPtr->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: target_saveSnippedImage(QString filePath) {
    int nx_lf = 1000;
    int ny_lf = 1000;

    double x1_lf = tools->activeImageBox.SnipBox->topLeft->key();
    double x2_lf = tools->activeImageBox.SnipBox->bottomRight->key();
    double y1_lf = tools->activeImageBox.SnipBox->bottomRight->value();
    double y2_lf = tools->activeImageBox.SnipBox->topLeft->value();

    double XYRatio = (x2_lf - x1_lf) / (y2_lf - y1_lf);
    if (XYRatio > 1)    ny_lf /= XYRatio;   else   nx_lf *= XYRatio;

    tools->activeImageBox.fileName = filePath.mid(filePath.lastIndexOf("/")+1);
    tools->activeImageBox.filePath = filePath;
    ui->imagePlot->savePng(tools->activeImageBox.filePath, nx_lf, ny_lf);
}

void MainWindow :: target_CopyImageBoxToPlot(QCustomPlot *plotPtr) {
    QCPColorMap *imageMap=nullptr;
    int nx_lf = 1000;
    int ny_lf = 1000;

    double x1_lf = tools->activeImageBox.SnipBox->topLeft->key();
    double x2_lf = tools->activeImageBox.SnipBox->bottomRight->key();
    double y1_lf = tools->activeImageBox.SnipBox->bottomRight->value();
    double y2_lf = tools->activeImageBox.SnipBox->topLeft->value();

    double XYRatio = (x2_lf - x1_lf) / (y2_lf - y1_lf);
    if (XYRatio > 1)    ny_lf /= XYRatio;   else   nx_lf *= XYRatio;
    target_clearImagePlot();
    imageMap = new QCPColorMap(ui->imagePlot->xAxis, ui->imagePlot->yAxis);//assign colormap to customplot

    imageMap->data()->setSize(nx_lf, ny_lf); // we want the color map to have nx * ny data points
    imageMap->data()->setRange(QCPRange(x1_lf, x2_lf), QCPRange(y1_lf, y2_lf));//sets physical size of colormap
    imageMap->setInterpolate(false);//to get visible pixel pattern
    imageMap->data()->fill(0); // make the color map transparent
    imageMap->setColorScale(colorScale); // associate the color map with the color scale

    QQueue <QCPColorMap*> colorMapQueue;
    if (customPlotLF == plotPtr)  colorMapQueue = colorMapNum_queue;
    else        colorMapQueue = colorMapHFNum_queue;

    if (colorMapQueue.length() < 1)     return;
    for (int k = 0; k < colorMapQueue.length(); k++) {
        // Copy data from the old color map onto the new color map to avoid the blanked out areas
        QCPColorMap *previousMap = colorMapQueue.at(k);

        // Find the size of each color map (in meters) to find a reasonable plotting area to copy
        double xMin = previousMap->data()->keyRange().lower, xMax = previousMap->data()->keyRange().upper;
        double yMin = previousMap->data()->valueRange().lower, yMax = previousMap->data()->valueRange().upper;

        // if the imageBox is placed outside of colorMap(k), then continue to the next colorMap
        if (xMin > x2_lf || xMax < x1_lf || yMin > y2_lf || yMax < y1_lf)   continue;

        double xStart = (x1_lf < xMin) ? xMin : x1_lf;
        double xEnd = (x2_lf > xMax) ? xMax : x2_lf;
        double yStart = (y1_lf < yMin) ? yMin : y1_lf;
        double yEnd = (y2_lf > yMax) ? yMax : y2_lf;

        // Find a smallest X/Y resolution used to copy the data
        double XResolution = (x2_lf - x1_lf) / nx_lf;
        double YResolution = (y2_lf - y1_lf) / ny_lf;

        int xSize = (xEnd - xStart) / XResolution;
        int ySize = (yEnd - yStart) / YResolution;

        // Start with the bottom row and move up
        for (int j = 0; j < ySize; j++) {
            double xCoord, yCoord = yStart + j*YResolution, color;

            // Start from left side and go right
            for (int i = 0; i < xSize; i++) {
                xCoord = xStart + i*XResolution;

                // Otherwise, we can copy the cell to the new colormap
                color = previousMap->data()->data(xCoord, yCoord);
                imageMap->data()->setData(xCoord, yCoord, color);
            }
        }
    }

    ui->imagePlot->xAxis->setRange(x1_lf, x2_lf);
    ui->imagePlot->yAxis->setRange(y1_lf, y2_lf);
    ui->imagePlot->replot(QCustomPlot::rpImmediateRefresh);
}

void MainWindow :: target_keyPressEvent(QKeyEvent *e) {
    if (state.GUI_MODE == 2 && !DEVELOPER_TESTING) {
        target_removeSnipBoxes();
        if (e->modifiers()==(Qt::ShiftModifier) && e->key() == Qt::Key_Up) {
            flag.ctrlKeyPressed = true;
            if (ui->targetListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->targetListWidget->currentRow();
                if (itemRowNum - 1 >= 0) {
                    target_selectTarget(itemRowNum - 1, true);
                    plot_replotAll();
                }
            } else {
                if (ui->targetListWidget->count() > 0) {
                    target_selectTarget(ui->targetListWidget->count()-1, true);
                    plot_replotAll();
                }
            }
            flag.ctrlKeyPressed = false;
        }
        else if (e->key() == Qt::Key_Up) {
            // Select next item up, or select bottom item
            if (ui->targetListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->targetListWidget->currentRow();
                if (itemRowNum - 1 >= 0) {
                    itemRowNum = itemRowNum-1;
                } else {
                    itemRowNum = ui->targetListWidget->count() - 1;
                }
                target_selectAllTargets(false); // Deselect all
                target_selectTarget(itemRowNum, true);
                plot_replotAll();
            }
            else {
                if (ui->targetListWidget->count() > 1) {
                    target_selectTarget(ui->targetListWidget->count()-1, true);
                    plot_replotAll();
                }
            }
        }
        else if (e->modifiers()==(Qt::ShiftModifier) && e->key() == Qt::Key_Down) {
            flag.ctrlKeyPressed = true;
            if (ui->targetListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->targetListWidget->currentRow();
                if (ui->targetListWidget->count() > itemRowNum + 1) {
                    target_selectTarget(itemRowNum + 1, true);
                    plot_replotAll();
                }
            }
            else {
                if (ui->targetListWidget->count() > 0) {
                    target_selectTarget(0, true);
                    plot_replotAll();
                }
            }
            flag.ctrlKeyPressed = false;
        }
        else if (e->key() == Qt::Key_Down) {
            // Select next item down, or select top item
            if (ui->targetListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->targetListWidget->currentRow();
                if (ui->targetListWidget->count() > itemRowNum + 1) {
                    itemRowNum += 1;
                } else {
                    // loop around to top of list
                    itemRowNum = 0;
                }
                target_selectAllTargets(false); // Deselect all
                target_selectTarget(itemRowNum, true); // Then select the next target down
                plot_replotAll();
            } else {
                if (ui->targetListWidget->count() > 1) {
                    target_selectTarget(0, true);
                    plot_replotAll();
                }
            }
        }
        if (ui->targetListWidget->selectedItems().count() == 1) {
            target_ShowImageAndBox();
            plot_replotAll();
        }
    } else {
        if (e->modifiers()==(Qt::ShiftModifier) && e->key() == Qt::Key_Up) {
            flag.ctrlKeyPressed = true;
            if (ui->waypointListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->waypointListWidget->currentRow();
                if (itemRowNum - 1 >= 0) {
                    target_selectWaypoint(&tools->waypointList[itemRowNum - 1], true, false);
                    tools->tracker.resetRoute();
                    QList <waypointItem*> selected = tools->selectedWaypoints();
                    for (int j = 0; j < selected.length(); j++)
                        tools->tracker.appendRoute(selected[j]);
                    tools->tracker.displayRoute();
                    if (selected.length() > 0)
                        tools->tracker.anchorWaypoint(selected[0]);
                    plot_replotAll();
                }
            } else {
                if (ui->waypointListWidget->count() > 0) {
                    target_selectWaypoint(&tools->waypointList[ui->waypointListWidget->count()-1], true, false);
                    tools->tracker.anchorWaypoint(&tools->waypointList[ui->waypointListWidget->count()-1]);
                    plot_replotAll();
                }
            }
            flag.ctrlKeyPressed = false;
        }
        else if (e->key() == Qt::Key_Up) {
            // Select next item up, or select bottom item
            if (ui->waypointListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->waypointListWidget->currentRow();
                if (itemRowNum - 1 >= 0) {
                    itemRowNum = itemRowNum-1;
                } else {
                    itemRowNum = ui->waypointListWidget->count() - 1;
                }
                target_selectAllWaypoints(false); // Deselect all
                target_selectWaypoint(&tools->waypointList[itemRowNum], true, false);
                tools->tracker.anchorWaypoint(&tools->waypointList[itemRowNum]);
                plot_replotAll();
            }
            else {
                if (ui->waypointListWidget->count() > 1) {
                    target_selectWaypoint(&tools->waypointList[ui->waypointListWidget->count()-1], true, false);
                    tools->tracker.anchorWaypoint(&tools->waypointList[ui->waypointListWidget->count()-1]);
                    plot_replotAll();
                }
            }
        }
        else if (e->modifiers()==(Qt::ShiftModifier) && e->key() == Qt::Key_Down) {
            flag.ctrlKeyPressed = true;
            if (ui->waypointListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->waypointListWidget->currentRow();
                if (ui->waypointListWidget->count() > itemRowNum + 1) {
                    target_selectWaypoint(&tools->waypointList[itemRowNum + 1], true, false);
                    tools->tracker.resetRoute();
                    QList <waypointItem*> selected = tools->selectedWaypoints();
                    for (int j = 0; j < selected.length(); j++)
                        tools->tracker.appendRoute(selected[j]);
                    tools->tracker.displayRoute();
                    if (selected.length() > 0)
                        tools->tracker.anchorWaypoint(selected[0]);
                    plot_replotAll();
                }
            }
            else {
                if (ui->waypointListWidget->count() > 0) {
                    target_selectWaypoint(&tools->waypointList[0], true, false);
                    tools->tracker.anchorWaypoint(&tools->waypointList[0]);
                    plot_replotAll();
                }
            }
            flag.ctrlKeyPressed = false;
        }
        else if (e->key() == Qt::Key_Down) {
            // Select next item down, or select top item
            if (ui->waypointListWidget->selectedItems().count() > 0) {
                int itemRowNum = ui->waypointListWidget->currentRow();
                if (ui->waypointListWidget->count() > itemRowNum + 1) {
                    itemRowNum += 1;
                } else {
                    // loop around to top of list
                    itemRowNum = 0;
                }
                target_selectAllWaypoints(false); // Deselect all
                target_selectWaypoint(&tools->waypointList[itemRowNum], true, false); // Then select the next target down
                tools->tracker.anchorWaypoint(&tools->waypointList[itemRowNum]);
                plot_replotAll();
            } else {
                if (ui->waypointListWidget->count() > 0) {
                    target_selectWaypoint(&tools->waypointList[0], true, false);
                    tools->tracker.anchorWaypoint(&tools->waypointList[0]);
                    plot_replotAll();
                }
            }
        }
    }
}

void MainWindow :: on_actionFlag_triggered(){

    if (flag.targetToolActive == false) {
        if (state.GUI_MODE != 2 && !DEVELOPER_TESTING)        return;
        if (DEVELOPER_TESTING) {
            ui->waypointListWidget->show();
            ui->targetListWidget->show();
        }
        // Turn off all other tools
        if (flag.measuringTapeActive)    on_actionMeasuring_Tape_triggered();
        if (flag.shadowToolActive)      on_actionShadow_triggered();
        if (cropTool->toolActive)       on_actionCrop_File_triggered();
        if (flag.waypointToolActive)    on_actionWaypoint_triggered();

        // Set flag tool active
        flag.targetToolActive = true;
        ui->actionFlag->setText("Target ✓");

        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
        ui->gpsPlot->setCursor(Qt::CrossCursor);
    } else {
        flag.targetToolActive = false;

        ui->actionFlag->setText("Target  ");

        if (tools->activeImageBox.SnipBox != nullptr) {
            customPlotLF->removeItem(tools->activeImageBox.SnipBox);
            customPlotHF->removeItem(tools->activeImageBox.SnipBox);
            tools->activeImageBox.SnipBox = nullptr;
            tools->targetState = false;
        }
    }
    plot_setCursor();
    plot_updateLfHfLegend(legendTxt, 1);
}

void MainWindow :: on_actionWaypoint_triggered() {
    if(flag.waypointToolActive == false){
        if (state.GUI_MODE == 2)        return;
        // Turn off all other tools
        if (flag.measuringTapeActive)    on_actionMeasuring_Tape_triggered();
        if (flag.shadowToolActive)      on_actionShadow_triggered();
        if (cropTool->toolActive)       on_actionCrop_File_triggered();
        if (flag.targetToolActive)      on_actionFlag_triggered();

        // Set flag tool active
        flag.waypointToolActive = true;
        ui->actionWaypoint->setText("Waypoint ✓");
        ui->gpsPlot->setCursor(Qt::CrossCursor);
    } else {
        flag.waypointToolActive = false;
        ui->actionWaypoint->setText("Waypoint  ");
        ui->gpsPlot->setCursor(Qt::ArrowCursor);
    }
    plot_updateLfHfLegend(legendTxt, 1);
}

void MainWindow :: on_actionSaveTarget_triggered() {
//    return; // disable it for now
    target_exportTarget();
}

void MainWindow :: on_btn_importWaypoints_clicked() {
    if (state.GUI_MODE == 2 && !DEVELOPER_TESTING)        return;
    QString data_filePath;
    data_filePath = QFileDialog::getOpenFileName(this,"Import Waypoints",recorder.recPath,"Jetasonic Files (*.JAS)\nGPS Exchange Format File (*.gpx)");

    if (data_filePath.size() == 0) {
        return;
    }

    QFileInfo fi(data_filePath);
    QString data_fileName = fi.filePath();
    QString extension = fi.suffix();

    if (!QFile(data_filePath).exists()) return;
    else if (extension.toLower().compare("gpx")==0){
        // Read .GPX file to plot chart
        trkptGPX.setFileName(data_filePath);
        trkptGPX.open(QIODevice::ReadWrite | QIODevice::Text);
        Gnss_readGpxFile();
        tools->importedFile = data_filePath;
    }
    else if (extension.toLower().compare("jas")==0) {
        target_ImportWaypoints(data_filePath);
        tools->importedFile = data_filePath;
        QFile sonarLogFile(data_filePath + "LOG");
        if (sonarLogFile.exists()) {

        }
        QFile sonarFlagFile(data_filePath + "FLAG");
        if (sonarFlagFile.exists()) {}
    }
}

void MainWindow :: on_targetListWidget_itemDoubleClicked(QListWidgetItem *item){
    target_GoToTarget();
}

void MainWindow :: on_targetListWidget_itemSelectionChanged() {
    if (flag.targetIgnoreSelectionChange)       return;
    flag.targetIgnoreSelectionChange = true;
    target_removeSnipBoxes();
    QList<QListWidgetItem*> selectedItems = ui->targetListWidget->selectedItems();

    // If no flagList items are selected, then deselect everything
    if (selectedItems.length() == 0) {
        target_selectAllTargets(false);
        ui->targetListWidget->setCurrentRow(-1);
        flag.targetIgnoreSelectionChange = false;
        plot_replotAll();
        return;
    }

    if (selectedItems.length() == 1) {
        int currentIndex = ui->targetListWidget->row(selectedItems.first());
        ui->targetListWidget->setCurrentRow(currentIndex);
        for (int i = 0; i < tools->targetList.length(); i++) {
            // Go through targets to select currentItem and deselect all others
            if (i == currentIndex)
                target_selectTarget(&tools->targetList[i], true);
            else
                target_selectTarget(&tools->targetList[i], false);
        }
        ui->targetListWidget->setCurrentRow(currentIndex);
        target_ShowImageAndBox();
    } else {
        // Multiple Items selected
        for (int i = 0; i < tools->targetList.length(); i++) {
            // Go through targets to select currentItem and deselect all others
            if (tools->isTargetInList(tools->targetList[i], selectedItems))
                target_selectTarget(&tools->targetList[i], true);
            else
                target_selectTarget(&tools->targetList[i], false);
        }
        ui->targetListWidget->setCurrentItem(selectedItems.last());
    }

    plot_replotAll();
    ui->targetListWidget->setFocus(Qt::NoFocusReason);
    ui->customPlot->setFocus(Qt::ActiveWindowFocusReason);
    ui->actionSaveTarget->setEnabled(true); // set false for now
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: on_targetListWidget_customContextMenuRequested(const QPoint &pos){
    //Add right click menu on the QListWidget (flagList), and connect this right
    //click signal to the slot
    if (ui->targetListWidget->count() == 0)     return;

    //create a popup menu, and add in an action(delete)
    QMenu *popMenu = new QMenu(this);
//    QMenu *subMenu = popMenu->addMenu("Sort");
    QAction *deleteFlag = new QAction(tr("Delete"),this);
    QAction *renameTarget = new QAction(tr("Rename"), this);
    QAction *saveTarget = new QAction(tr("Export"), this);
    QAction *sortTargetsPing = new QAction(tr("Sort"), this);
//    QAction *sortTargetsName = new QAction(tr("By Name"), this);

    //QAction *goToFlag = new QAction(tr("Go To Flag"),this);
    if (tools->selectedTargets().count() == 1)
        popMenu->addAction(renameTarget);
    popMenu->addAction(saveTarget);
    popMenu->addAction(deleteFlag);
//    subMenu->addAction(sortTargetsName);

    connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedItems()));
    connect(deleteFlag,SIGNAL(triggered()),this,SLOT(measuringTape_DeleteSelectedItems()));
    connect(renameTarget, SIGNAL(triggered()),this,SLOT(target_renameTarget()));
    connect(saveTarget, SIGNAL(triggered()),this,SLOT(target_exportTarget()));
    connect(sortTargetsPing, SIGNAL(triggered()),this,SLOT(target_sortByPing()));
//    connect(sortTargetsName, SIGNAL(triggered()),this,SLOT(target_sortByName()));
    //connect(goToFlag,SIGNAL(triggered()),this,SLOT(goToFlagSlot()));
    popMenu->exec(QCursor::pos());
    delete popMenu;
    delete deleteFlag;
    delete renameTarget;
    delete saveTarget;
    delete sortTargetsPing;
}

void MainWindow :: on_waypointListWidget_itemSelectionChanged() {
    if (flag.targetIgnoreSelectionChange)       return;
    flag.targetIgnoreSelectionChange = true;
    target_removeSnipBoxes();
    QList<QListWidgetItem*> selectedItems = ui->waypointListWidget->selectedItems();

    // If no flagList items are selected, then deselect everything
    if (selectedItems.length() == 0) {
        tools->tracker.resetRoute();
        target_selectAllWaypoints(false);
        ui->waypointListWidget->setCurrentRow(-1);
        flag.targetIgnoreSelectionChange = false;
        plot_replotAll();
        return;
    } else if (selectedItems.length() == 1) {
        target_selectAllWaypoints(false);
        flag.targetIgnoreSelectionChange = true;
        int currentIndex = ui->waypointListWidget->row(selectedItems.first());
        ui->waypointListWidget->setCurrentRow(currentIndex);
        for (int i = 0; i < tools->waypointList.length(); i++) {
            // Go through targets to select currentItem and deselect all others
            if (i == currentIndex) {
                target_selectWaypoint(&tools->waypointList[i], true, false);
                tools->tracker.anchorWaypoint(&tools->waypointList[i]);
                break;
            } else
                target_selectWaypoint(&tools->waypointList[i], false);
        }
        ui->waypointListWidget->setCurrentRow(currentIndex);
        target_ShowImageAndBox();
    } else {
        tools->tracker.resetRoute();
        // Multiple Items selected
        for (int i = 0; i < tools->waypointList.length(); i++) {
            // Go through targets to select currentItem and deselect all others
            if (tools->isTargetInList(tools->waypointList[i], selectedItems)) {
                target_selectWaypoint(&tools->waypointList[i], true, true);
                tools->tracker.appendRoute(&tools->waypointList[i]);
            } else
                target_selectWaypoint(&tools->waypointList[i], false);
        }
        ui->waypointListWidget->setCurrentItem(selectedItems.last());
        tools->tracker.anchorWaypoint(tools->tracker.route.first());
        tools->tracker.displayRoute();
    }

    plot_replotAll();
    ui->waypointListWidget->setFocus(Qt::NoFocusReason);
    ui->customPlot->setFocus(Qt::ActiveWindowFocusReason);
    ui->actionSaveTarget->setEnabled(true); // set false for now
    flag.targetIgnoreSelectionChange = false;
}

void MainWindow :: on_waypointListWidget_customContextMenuRequested(const QPoint &pos) {
    if (ui->waypointListWidget->count() == 0)     return;

    //create a popup menu, and add in an action(delete)
    QMenu *popMenu = new QMenu(this);
//    QMenu *subMenu = popMenu->addMenu("Sort");
    QAction *deleteFlag = new QAction(tr("Delete"),this);
    QAction *renameTarget = new QAction(tr("Rename"), this);
    QAction *saveTarget = new QAction(tr("Export"), this);
    QAction *sortWaypoints = new QAction(tr("Sort"), this);
    QAction *setStartPoint = new QAction(tr("Set to 1"),this);
//    QAction *sortTargetsName = new QAction(tr("By Name"), this);

    //QAction *goToFlag = new QAction(tr("Go To Flag"),this);
//    popMenu->addAction(saveTarget);
    if (tools->selectedWaypoints().count() == 1) {
        popMenu->addAction(renameTarget);
        popMenu->addAction(setStartPoint);
    }
//    popMenu->addAction(sortWaypoints);
    popMenu->addAction(deleteFlag);
    if (!gnssController->validCoords)    sortWaypoints->setEnabled(false);

    connect(deleteFlag,SIGNAL(triggered()),this,SLOT(target_DeleteSelectedWaypoints()));
    connect(saveTarget, SIGNAL(triggered()),this,SLOT(target_exportTarget()));
    connect(setStartPoint, SIGNAL(triggered()),this,SLOT(target_setStartWaypoint()));
    connect(renameTarget, SIGNAL(triggered()),this,SLOT(target_renameTarget()));
    connect(sortWaypoints, SIGNAL(triggered()),this,SLOT(target_sortWaypoints()));
    popMenu->exec(QCursor::pos());
    delete popMenu;
    delete setStartPoint;
    delete deleteFlag;
    delete renameTarget;
    delete saveTarget;
    delete sortWaypoints;
}

/**************** Other Tools ****************/

void MainWindow :: measuringTape_SelectItem(int itemRow) {
    tools->measuringTapeList[itemRow].selected = true;

    // Deselect everything on the other plots
    target_selectAllTargets(false);  // Clear selection on flagList
    target_removeSnipBoxes();
    plot_replotAll();
    customPlotLF->deselectAll();
    customPlotHF->deselectAll();
    ui->gpsPlot->deselectAll();

    tools->measuringTapeList[itemRow].distanceText->setSelected(true);
    tools->measuringTapeList[itemRow].distanceText->setSelectedFont(QFont("sans", 10));

    for (int k = 0; k < tools->measuringTapeList[itemRow].line_back.length(); k++){
        // Set each of the line segments to selected
        tools->measuringTapeList[itemRow].line_back.at(k)->setSelected(true);
        tools->measuringTapeList[itemRow].line_top.at(k)->setSelected(true);
    }

    return;
}

void MainWindow :: measuringTape_DeleteSelectedItems() {
    // Only delete selected measuring tape items
    for (int i = 0; i < tools->measuringTapeList.length(); i++) {
        if (tools->measuringTapeList.at(i).selected) {
            customPlotLF->removeItem(tools->measuringTapeList.at(i).distanceText);
            customPlotHF->removeItem(tools->measuringTapeList.at(i).distanceText);
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).distanceText);
            customPlotLF->removeItem(tools->measuringTapeList.at(i).areaText);
            customPlotHF->removeItem(tools->measuringTapeList.at(i).areaText);
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).areaText);
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).textGps);
            for (int j = 0; j < tools->measuringTapeList.at(i).line_back.length(); j++) {
                if (tools->measuringTapeList.at(i).lineGps.length() > j)
                    ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).lineGps.at(j));
                customPlotLF->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
                customPlotLF->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
                customPlotHF->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
                customPlotHF->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
                ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
                ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
            }
            tools->measuringTapeList.removeAt(i);
            plot_replotAll();

            return;
        }
    }
}

void MainWindow :: measuringTape_DeleteAllItems() {
    // Only delete selected measuring tape items
    for (int i = tools->measuringTapeList.length() - 1; i >= 0; i--) {
        customPlotLF->removeItem(tools->measuringTapeList.at(i).distanceText);
        customPlotHF->removeItem(tools->measuringTapeList.at(i).distanceText);
        ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).distanceText);
        customPlotLF->removeItem(tools->measuringTapeList.at(i).areaText);
        customPlotHF->removeItem(tools->measuringTapeList.at(i).areaText);
        ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).areaText);
        ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).textGps);
        for (int j = 0; j < tools->measuringTapeList.at(i).line_back.length(); j++) {
            if (tools->measuringTapeList.at(i).lineGps.length() > j)
                ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).lineGps.at(j));
            customPlotLF->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
            customPlotLF->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
            customPlotHF->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
            customPlotHF->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
        }
        tools->measuringTapeList.removeAt(i);
        plot_replotAll();
    }
}

void MainWindow :: measuringTape_mouseClick(QCustomPlot *plotPtr, QMouseEvent* event) {
    if (state.PLAY_MODE != 4)   player_playData(false);
    static double x1, x2, y1, y2;
    static double totalDist = 0, totalDistGps = 0;
    double xPosition = plotPtr->xAxis->pixelToCoord(event->pos().x());
    double yPosition = plotPtr->yAxis->pixelToCoord(event->pos().y());
    static double firstLat, firstLon;
    int inside = 0;

    // if the mouse is outside of the sonar data on the plot, then do not allow the user to make a measurement in that area
    for (int i = 0; i < colorMapNum_queue.length(); i++) {
        if (colorMapNum_queue.at(i)->data()->data(xPosition, yPosition) > 0)
            inside++;
    }
    if (inside == 0)     return;

    //findGpsCoordinate
    double angle_coefficient=0;
    if(ping->angleOrientation==70){
        // Angle scan
        angle_coefficient = 1/qTan(ping->degree * M_PI/180);
    }
    double yCursorCorrected = yPosition-abs(xPosition*angle_coefficient);
    PingMapItem mapItem = dataMap.findItemWithY(yCursorCorrected);
    if (!mapItem.isValid)        return;
    double GpsLattitude = mapItem.lattitude;
    double GpsLongitude = mapItem.longitude;
    double GpsHeading = mapItem.heading;

    double ratio; //ratio = d / 6378137.0  radius of the earth
    ratio = abs(xPosition) / 6378137.0;

    double sonarAngle = ping->angleOrientation;
    if (xPosition < 0)  sonarAngle *= -1;

    double sampleLat = (180 / M_PI) * qAsin(qSin(GpsLattitude * M_PI/180) * qCos(ratio)
                            + qCos(GpsLattitude * M_PI/180) * qSin(ratio)
                            * qCos((sonarAngle + GpsHeading) * M_PI/180));
    double sampleLon = (180 / M_PI) * (GpsLongitude * M_PI/180
                            + qAtan((qSin((sonarAngle + GpsHeading) * M_PI/180) * qSin(ratio)
                            * qCos(GpsLattitude * M_PI/180))/(qCos(ratio)
                            - qSin(GpsLattitude * M_PI/180) * qSin(sampleLat * M_PI/180))));
    if (GpsLattitude < -400) {
        sampleLat = -500;
        sampleLon = -500;
    }

    if (!tools->activeMeasuringTapeObject.start_measuring && (flag.measuringTapeActive == true || flag.shadowToolActive == true)){
        // First click of measuring tape. Find the first mouse position
        if (flag.shadowToolActive)          tools->activeMeasuringTapeObject.shadowTool = true;
        else                                tools->activeMeasuringTapeObject.shadowTool = false;
        tools->activeMeasuringTapeObject.shadowAngle = ping->degree;
        tools->activeMeasuringTapeObject.setStartPoint(xPosition, yPosition, plotPtr);

    } else if (tools->activeMeasuringTapeObject.start_measuring && flag.measuringTapeActive == true && flag.ctrlKeyPressed == true){
        tools->activeMeasuringTapeObject.addPoint(xPosition, yPosition, plotPtr);

        if (plotPtr == customPlotLF)    flag.mousePressedFlagLf = 1;
        else                            flag.mousePressedFlagLf = 0;
        if (plotPtr == customPlotHF)    flag.mousePressedFlagHf = 1;
        else                            flag.mousePressedFlagHf = 0;

        x1=x2;
        y1=y2;
    } else if (tools->activeMeasuringTapeObject.start_measuring && flag.measuringTapeActive == true && flag.ctrlKeyPressed != true){
        // Set position on mouse position
        tools->activeMeasuringTapeObject.setEndPoint(xPosition, yPosition, plotPtr);
        tools->addNewTapeMeasurement();

        // Set text position to middle of the last line
        //x2 = (activeMeasuringTapeObject.start_point.x() + x2) / 2;
        //y2 = (activeMeasuringTapeObject.start_point.y() + y2) / 2;

        flag.mousePressedFlagLf = 0;
        flag.mousePressedFlagHf = 0;

        target_selectAllTargets(false);
        plot_replotAll();
        tools->notifyUser();
    } else if (tools->activeMeasuringTapeObject.start_measuring && flag.shadowToolActive) {
        // If the shadow tool is active

        // Calculate object height
        // D2 = D1/R1*R2
        // D1 = Sonar altitude
        // R1 = Distance from start point to bottom tracking
        // R2 = Length of line
        double angle_coefficient=0;
        if(ping->angleOrientation==90)   angle_coefficient=0;    // Side scan
        else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
        double yCursorCorrected = yPosition - abs(xPosition*angle_coefficient);
        PingMapItem item = dataMap.findItemWithY(yCursorCorrected);
        double R1 = abs(tools->activeMeasuringTapeObject.start_point.x());
        if (flag.showGap)   R1 -= item.altitude;//ping->lastTraceAlt;
        double depthFactor = sin(qDegreesToRadians(ping->degree/* - item.pitch*/));
        double D1 = item.altitude / depthFactor;  // Must correct actual received distance

        bool done = false;
        done = tools->activeMeasuringTapeObject.setShadowEndPoint(xPosition, yPosition, plotPtr, D1/R1);
        if (!done)  return;
        tools->addNewTapeMeasurement();

        flag.mousePressedFlagLf = 0;
        flag.mousePressedFlagHf = 0;

        target_selectAllTargets(false);
        plot_replotAll();
        tools->notifyUser();
    }
    plotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: measuringTape_UpdatePlot(QCustomPlot *plotPtr, QMouseEvent* event) {
    // Used in the onMouseMove functions
    if (!flag.measuringTapeActive && !flag.shadowToolActive) {
        return;
    }

//    plotPtr->setCursor(Qt::CrossCursor);
    if (tools->activeMeasuringTapeObject.line_back.length() > 0) {
        if (!plotPtr->hasItem(tools->activeMeasuringTapeObject.line_back.last()))     return;
    }

    double x = 0, y = 0;

    if(tools->activeMeasuringTapeObject.start_measuring ==true){
        x = plotPtr->xAxis->pixelToCoord(event->pos().x());
        y = plotPtr->yAxis->pixelToCoord(event->pos().y());

        if (tools->activeMeasuringTapeObject.line_back.count() > 1 &&
                abs(event->pos().x() - plotPtr->xAxis->coordToPixel(tools->activeMeasuringTapeObject.line_back.at(0)->start->key())) < 10 &&
                abs(event->pos().y() - plotPtr->yAxis->coordToPixel(tools->activeMeasuringTapeObject.line_back.at(0)->start->value())) < 10) {
            // If the first end is within 10 pixels of the mouse, then snap tape
            x = tools->activeMeasuringTapeObject.line_back.at(0)->start->key();
            y = tools->activeMeasuringTapeObject.line_back.at(0)->start->value();
        } else if (flag.shadowToolActive) {
            // Set coords of end of measuring tape - it should line up with the ping
            // Restrict the user to only move inwards from the start point
            double tanDegree = 0;
            if (ping->degree < 88)   tanDegree = qCos(ping->degree * M_PI / 180);
            double xStart = tools->activeMeasuringTapeObject.start_point.x();
            double yStart = tools->activeMeasuringTapeObject.start_point.y();
            double xCoord1 = x - xStart;
            double yCoord1 = yStart;
            y = -abs(xCoord1*tanDegree) + yCoord1;

            if (abs(x) > abs(xStart)) {
                x = xStart+0.001;
                y = yStart+0.001;
            }
            else if ((x < 0 && xStart > 0) || (x > 0 && xStart < 0)) {
                x = 0;
                y = -abs(xStart*tanDegree) + yCoord1;
            }
        }

        tools->activeMeasuringTapeObject.setLine(tools->activeMeasuringTapeObject.start_point.x(),
                                                 tools->activeMeasuringTapeObject.start_point.y(), x, y);
        plotPtr->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: measuringTape_RemoveItemsFromPlot(double traceYCoord) {
    for(int i=0;i<tools->measuringTapeList.length();i++){
        if ((tools->measuringTapeList[i].start_point.y() <= traceYCoord || tools->measuringTapeList[i].end_point.y() <= traceYCoord)
                && tools->measuringTapeList[i].distanceText !=nullptr && tools->measuringTapeList[i].line_back.size()!=0 &&
                tools->measuringTapeList[i].line_top.size()!=0){
            customPlotLF->removeItem(tools->measuringTapeList[i].distanceText);
            customPlotHF->removeItem(tools->measuringTapeList[i].distanceText);
            customPlotLF->removeItem(tools->measuringTapeList.at(i).areaText);
            customPlotHF->removeItem(tools->measuringTapeList.at(i).areaText);
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).areaText);
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).textGps);
            for(int j=0;j<tools->measuringTapeList[i].line_back.length();j++){
                if (tools->measuringTapeList.at(i).lineGps.length() > j)
                    ui->gpsPlot->removeItem(tools->measuringTapeList[i].lineGps.at(j));
                customPlotLF->removeItem(tools->measuringTapeList[i].line_back.at(j));
                customPlotLF->removeItem(tools->measuringTapeList[i].line_top.at(j));
                customPlotHF->removeItem(tools->measuringTapeList[i].line_back.at(j));
                customPlotHF->removeItem(tools->measuringTapeList[i].line_top.at(j));
            }
            tools->measuringTapeList[i].distanceText = nullptr;
            tools->measuringTapeList[i].line_back.clear();
            tools->measuringTapeList[i].line_top.clear();
        }
    }
}

void MainWindow :: measuringTape_UndoLastMeasure() {
    if (tools->activeMeasuringTapeObject.start_measuring) {
        tools->activeMeasuringTapeObject.start_measuring = false;
        for (int j = 0; j < tools->activeMeasuringTapeObject.line_back.length(); j++) {
            customPlotLF->removeItem(tools->activeMeasuringTapeObject.line_back.at(j));
            customPlotLF->removeItem(tools->activeMeasuringTapeObject.line_top.at(j));
            customPlotHF->removeItem(tools->activeMeasuringTapeObject.line_back.at(j));
            customPlotHF->removeItem(tools->activeMeasuringTapeObject.line_top.at(j));
            ui->gpsPlot->removeItem(tools->activeMeasuringTapeObject.line_back.at(j));
            ui->gpsPlot->removeItem(tools->activeMeasuringTapeObject.line_top.at(j));

            tools->activeMeasuringTapeObject.line_back.clear();
            tools->activeMeasuringTapeObject.line_top.clear();
        }
        plot_replotAll();
        return;
    }

    int i = tools->measuringTapeList.size()-1;
    if (i < 0)      return;
    customPlotLF->removeItem(tools->measuringTapeList.at(i).distanceText);
    customPlotHF->removeItem(tools->measuringTapeList.at(i).distanceText);
    ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).distanceText);
    customPlotLF->removeItem(tools->measuringTapeList.at(i).areaText);
    customPlotHF->removeItem(tools->measuringTapeList.at(i).areaText);
    ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).areaText);
    ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).textGps);
    for (int j = 0; j < tools->measuringTapeList.at(i).line_back.length(); j++) {
        if (tools->measuringTapeList.at(i).lineGps.length() > j)
            ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).lineGps.at(j));
        customPlotLF->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
        customPlotLF->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
        customPlotHF->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
        customPlotHF->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
        ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).line_back.at(j));
        ui->gpsPlot->removeItem(tools->measuringTapeList.at(i).line_top.at(j));
    }
    tools->measuringTapeList.removeAt(i);
    plot_replotAll();
}

void MainWindow :: crop_MouseClick(QCustomPlot *plotPtr, QMouseEvent* event) {
    if (!cropTool->toolActive)      return;
    if (state.PLAY_MODE != 4)   player_playData(false);

    cropTool->ping_rangeBegin = ping->pingNumFirst;
    cropTool->ping_rangeEnd = ping->pingNumEnd;

    double xPosition = plotPtr->xAxis->pixelToCoord(event->pos().x());
    double yPosition = plotPtr->yAxis->pixelToCoord(event->pos().y());

    double range = ping->rangeLf_double;
    if (plotPtr == customPlotHF) {
        range = ping->rangeHf_double;
    }
    double angle_coefficient=0;
    if(ping->angleOrientation==90)   angle_coefficient=0;    // Side scan
    else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
    double yCursorCorrected = yPosition-abs(xPosition*angle_coefficient);

    // Determine ping number and store it in the cropTool object
    if (cropTool->startSelected) {
        PingMapItem item = dataMap.findItemWithY(yCursorCorrected);
        if (!item.isValid)      return;
        cropTool->ping_start = item.pingNumber;
        cropTool->rtc_start = item.rtcTime;
        cropTool->setCropLine(plotPtr, yCursorCorrected, range, ping->degree);
        cropTool->addCursorText(plotPtr, event);
    }
    else if (cropTool->endSelected) {
        int currentPing = dataMap.findItemWithY(yCursorCorrected).pingNumber;
        if (currentPing < cropTool->ping_start)  return;
        cropTool->ping_end = currentPing;
        cropTool->setCropLine(plotPtr, yCursorCorrected, range, ping->degree);
    }
}

void MainWindow :: crop_UpdateMarkers(double x, double y) {
    // Called when mouse hovers over plot and the snipping tool is active
    if (!cropTool->toolActive)   return;

    if (cropTool->ping_start > 0) {
        if (cropTool->start_port == nullptr)    return;
        double xPosition = x;
        double yPosition = y;

        double angle_coefficient=0;
        if(ping->angleOrientation==90)   angle_coefficient=0;    // Side scan
        else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
        double yCursorCorrected = yPosition-abs(xPosition*angle_coefficient);
        double range = ping->rangeLf_double;

        QString text = dataMap.findItemWithY(yCursorCorrected).rtcTime;
        int currentPing = dataMap.findItemWithY(yCursorCorrected).pingNumber;

        if (currentPing > cropTool->ping_start) {
            cropTool->ping_end = currentPing;
            cropTool->moveCropLine(cropTool->start_port->parentPlot(), yCursorCorrected, range, ping->degree);
            cropTool->updateCursorText(text);
        }
    }
}

void MainWindow :: crop_UpdatePlot(QCustomPlot *plotPtr, QMouseEvent* event) {
    // Called when mouse hovers over plot and the snipping tool is active
    if (!cropTool->toolActive)   return;

    if (cropTool->ping_start > 0) {
        double xPosition = plotPtr->xAxis->pixelToCoord(event->pos().x());
        double yPosition = plotPtr->yAxis->pixelToCoord(event->pos().y());

        double angle_coefficient=0;
        if(ping->angleOrientation==90)   angle_coefficient=0;    // Side scan
        else        angle_coefficient = 1/qTan(ping->degree * M_PI/180); // Angle scan
        double yCursorCorrected = yPosition-abs(xPosition*angle_coefficient);
        double range = ping->rangeLf_double;

        double xBound = ping->rangeLf_double;
        if (plotPtr == customPlotLF)        xBound = ping->rangeHf_double;
        if ((yCursorCorrected > ping->y_UpperBound) || (yCursorCorrected < ping->y_LowerBound) ||
                xPosition > xBound || xPosition < -xBound)
            yCursorCorrected = ping->lastTracePos - ping->res_fwd;

        PingMapItem item = dataMap.findItemWithY(yCursorCorrected);
        QString text = item.rtcTime;
        int currentPing = item.pingNumber;

        if (currentPing > cropTool->ping_start) {
            cropTool->ping_end = currentPing;
            cropTool->moveCropLine(plotPtr, yCursorCorrected, range, ping->degree);
            cropTool->updateCursorText(/*event, */text);
            plot_replotAll();
        } else {
            cropTool->ping_end = cropTool->ping_start + 1;
            cropTool->moveCropLine(plotPtr, dataMap.findItem(cropTool->ping_end).yCoord, range, ping->degree);
            cropTool->updateCursorText(cropTool->rtc_start);
            plot_replotAll();
        }
    }
}

void MainWindow :: crop_saveFile() {

    // Copy name and open browser to save cropped file
    QString filter = "Jetasonic Scan Data (*.JAS)";
    QString fileName = player.openedFileName.left(player.openedFileName.lastIndexOf(".")) + "_C";
    int counter = 0;
    do {
        QString counter_text = QString::number(counter);
        if (counter < 1000000)     counter_text = counter_text.rightJustified(3,'0');
        fileName = player.openedFileName.left(player.openedFileName.lastIndexOf(".")) + "_C" + counter_text;
        counter++;
    } while (QFileInfo(fileName + ".JAS").exists());
//    if (cropTool->ping_end > cropTool->ping_start)
//        fileName += QString::number(cropTool->ping_start - ping->ping_change) + "_" + QString::number(cropTool->ping_end - ping->ping_change);
//    else
//        fileName += QString::number(cropTool->ping_end - ping->ping_change) + "_" + QString::number(cropTool->ping_start - ping->ping_change);
    cropTool->filePath  = QFileDialog::getSaveFileName(this, tr("Save Cropped Sonar File"),
                                             fileName, filter, &filter);
    if (cropTool->filePath == "") {
        // if user presses cancel
        cropTool->resetSlider();
        if (cropTool->toolActive)   on_actionCrop_File_triggered();
        return;
    }
    cropTool->fileOut.setFileName(cropTool->filePath);
    cropTool->fileOut.open(QIODevice::WriteOnly);

    // Take file from player, copy data from player to buffer for croppedFile
    player.sonarFile.close();  // close the file in the player, so it can be opened by cropping tool
    cropTool->fileIn.setFileName(player.openedFileName);
    cropTool->fileIn.open(QIODevice::ReadOnly);
    cropTool->fileInSize = player.fileSize;
    cropTool->fileInBufferPos = 0;
    cropTool->fileInPos = 0;
    cropTool->fileInBuffer.clear();
    cropTool->loadBuffer();

    if (cropTool->ping_end < cropTool->ping_start) {
        // If start marker is less than end marker, then switch them
        int tmp = cropTool->ping_end;
        cropTool->ping_end = cropTool->ping_start;
        cropTool->ping_start = tmp;
    }

    // Crop data for sonarfile
    // Search file for start ping number
    int readFileBytes = 0;
    while (cropTool->fileInPos < cropTool->fileInSize - 50) {
        readFileBytes = 0;

        quint32 syncBytes, packetID, dataSize;
        //For the very first ping
        //Packet Header
        cropTool->fileInPtr += data_copy(&syncBytes, cropTool->fileInPtr, 4, &readFileBytes);      //Sync Word 1&2
        if (syncBytes != 0x8000FFFF) {
            qDebug() << "Misaligned";
        }
        cropTool->fileInPtr += data_copy(&packetID, cropTool->fileInPtr, 4, &readFileBytes);        //Message Type is 0 for the very first ping
        cropTool->fileInPtr += data_copy(&dataSize, cropTool->fileInPtr, 4, &readFileBytes);                 //Packet Size

        cropTool->fileInPtr += Jas_readPingHeader(cropTool->fileInPtr, packetID, &readFileBytes);

        // Go back to start of ping
        cropTool->fileInPtr -= ping->pingHeaderSize + 12;
        readFileBytes -= ping->pingHeaderSize + 12;
        if (ping->pingNumCurrent >= cropTool->ping_start) {
            // Stop here
            cropTool->fileInStartPos = cropTool->fileInPos;
            break;
        } else {
            // go to next ping
            cropTool->fileInPtr += dataSize + 4;
            readFileBytes += dataSize + 4;
            cropTool->fileInPos += readFileBytes;
            cropTool->fileInBufferPos += readFileBytes;

            cropTool->loadBuffer();
        }
    }

    // Search file for end ping number
    while (cropTool->fileInPos < cropTool->fileInSize - 50) {
        readFileBytes = 0;

        quint32 syncBytes, packetID, dataSize;
        //For the very first ping
        //Packet Header
        cropTool->fileInPtr += data_copy(&syncBytes, cropTool->fileInPtr, 4, &readFileBytes);      //Sync Word 1&2
        if (syncBytes != 0x8000FFFF) {
            qDebug() << "Misaligned";
        }
        cropTool->fileInPtr += data_copy(&packetID, cropTool->fileInPtr, 4, &readFileBytes);        //Message Type is 0 for the very first ping
        cropTool->fileInPtr += data_copy(&dataSize, cropTool->fileInPtr, 4, &readFileBytes);                 //Packet Size

        cropTool->fileInPtr += Jas_readPingHeader(cropTool->fileInPtr, packetID, &readFileBytes);

        // Go back to start of ping
        cropTool->fileInPtr -= ping->pingHeaderSize + 12;
        readFileBytes -= ping->pingHeaderSize + 12;
        if (ping->pingNumCurrent >= cropTool->ping_end) {
            cropTool->fileInEndPos = cropTool->fileInPos;
            break;
        } else {
            // go to next ping
            cropTool->fileInPtr += dataSize + 4;
            readFileBytes += dataSize + 4;
            cropTool->fileInPos += readFileBytes;
            cropTool->fileInBufferPos += readFileBytes;

            cropTool->loadBuffer();
        }
    }

    // Crop data for Gnss file
    cropTool->fileOutGps.setFileName(cropTool->filePath + "LOG");
    cropTool->fileOutGps.open(QIODevice::WriteOnly);
    player.GPSFile.close();  // close the file in the player, so it can be opened by cropping tool
    cropTool->fileInGps.setFileName(player.openedFileName + "LOG");
    cropTool->fileInGps.open(QIODevice::ReadOnly);

    // Crop data for flag file
    cropTool->fileOutFlag.setFileName(cropTool->filePath + "flag");
    cropTool->fileOutFlag.open(QIODevice::WriteOnly);
    cropTool->fileInFlag.setFileName(player.openedFileName + "flag");
    cropTool->fileInFlag.open(QIODevice::ReadOnly);

    // Copy data to buffer until the end ping number
    cropTool->cropData();

    // Load sonar and gps files and resume from last position
    player.sonarFile.open(QIODevice::ReadOnly);
    player.sonarFile.seek(player.filePos - player.bufferPos);
    player.GPSFile.open(QIODevice::ReadOnly);
    player.fileBuffer.clear();
    player_loadToBuffer();
    cropTool->resetSlider();
    if (cropTool->toolActive)   on_actionCrop_File_triggered();
}

/****************Actions,Buttons and sliders****************/

void MainWindow :: status_resetStatusBar() {
    tem_label->setText(/*"T: "+*/"0.0°C");
    ping_label->setText("0000000");
    time_label->setText("00:00:00");
    depth_label->setText("0.0 m");
    rollValue->setText("Roll: 0.0°");
    pitchValue->setText("Pitch: 0.0°");
    yawValue->setText("Yaw: 0.0°");
//    avg_label->setText("Alt: " + QString::number(Altitude, 'd', 1));
    ui->degree_lbl->setText("0.0º");
}

void MainWindow :: status_updateStatusBar (int Ping, QString Rtc, double Depth, double Temperature, double Roll, double Pitch, double Yaw, double Altitude, double heading) {
    // Depth is read from the pressure sensor, and is the distance from the sea surface to the sonar
    // Altitude readings come from the bottom tracking algorithm, and is the diustance between sonar to sea floor - hidden from status bar

    QString ping_format = QString::number(Ping);
    if (Ping < 1000000)     ping_format = ping_format.rightJustified(7,'0');
    if (Ping < 0)           ping_format = "0000000";
    if (Rtc == "")          Rtc = "00:00:00";

    // Altimeter readings displayed on DepthPlot
    double AltRange = Altitude;//*ping->depthFactor;//sin(qDegreesToRadians(ping->degree-Pitch));
    static double AltRangePrev = AltRange;
    if (round(AltRange*10) < round(AltRangePrev*10))    ui->label_alt3->setText("  ↑  ");
    else if (round(AltRange*10) > round(AltRangePrev*10))   ui->label_alt3->setText("  ↓  ");
    AltRangePrev = AltRange;

    if (setting.showAltDepth && Depth > 0)   AltRange += Depth;
    QString altimeter1 = QString::number(AltRange,'d',1);
//    depthLegend->setText("");// D:"+altimeter1+" ");
//    QString altimeter2 = QString::number(Altitude*cos(qDegreesToRadians(ping->degree-Pitch)),'d',1);
    depthLegend2->setText("");// R:"+altimeter2+" ");
    if (setting.showAltDepth && Depth > 0)      ui->label_alt4->setText("   ᴰ ");
    else                                        ui->label_alt4->setText("     ");
    ui->label_alt->setText(""+altimeter1.split('.').first());
    ui->label_alt2->setText(" "+altimeter1.split('.').last());

    if ((state.PLAY_MODE==4 || state.PLAY_MODE == 5 || state.PLAY_MODE == 6) && flag.PlotAngleScan && Format->isOpen_DepthPlot) {
        depthPlot->replot(QCustomPlot::rpQueuedReplot);
    }

    tem_label->setText(/*"T: "+*/QString::number(Temperature, 'd', 1)+"°C");
    ping_label->setText(ping_format);
    time_label->setText(Rtc);
    if (Depth < 0)  depth_label->setText(/*"Depth: " + */QString::number(0, 'd', 1) + " m");
    else            depth_label->setText(/*"Depth: " + */QString::number(Depth, 'd', 1) + " m");
    if (Roll < -360)    rollValue->setText("Roll: " + QString::number(0, 'd', 1)+"°");
    else                rollValue->setText("Roll: " + QString::number(Roll, 'd', 1)+"°");
    if (Pitch < -360)   pitchValue->setText("Pitch: " + QString::number(0, 'd', 1)+"°");
    else                pitchValue->setText("Pitch: " + QString::number(Pitch, 'd', 1)+"°");
    if (Yaw < -360)     yawValue->setText("Yaw: " + QString::number(0, 'd', 1)+"°");
    else                yawValue->setText("Yaw: " + QString::number(Yaw, 'd', 1)+"°");
    avg_label->setText("Alt: " + QString::number(Altitude, 'd', 1));
    ui->degree_lbl->setText(/*"AZT: "+*/QString::number(heading, 'd', 1) + "º");
}

void MainWindow :: status_updateAltLabel() {

    double y = line->point1->coords().y();
    double alt_trace = 0, yaw = 0, roll = 0, pitch = 0, azimuth = 0, temperature = 0;
    float status_depth = 0;
    QString status_rtc = "";
    double ping_Num = 0;
    GpsPoint readGpsPoint;

    PingMapItem item = dataMap.findItemWithY(y);
    readGpsPoint.Lat = item.lattitude;
    readGpsPoint.Lon = item.longitude;
    readGpsPoint.Dir = item.heading;
    alt_trace = item.altitude;
    yaw = item.yaw;
    roll = item.roll;
    pitch = item.pitch;
    azimuth = item.azimuth;
    status_depth = item.depth;
    status_rtc = item.rtcTime;
    ping_Num = item.pingNumber;
    temperature = item.temperature;

    status_updateStatusBar(ping_Num-ping->pingNumFirst, status_rtc, status_depth, temperature, roll,
                           pitch, yaw, alt_trace, azimuth);

}

void MainWindow :: depthAlarm_start() {
    if (state.GUI_MODE < 2 && state.PLAY_MODE < 3 && state.RT_MODE < 2)     return;
    if (!depthAlarm->activated)     return;
    // Start the alarm if it was previously off
    if (!depthAlarm->onState) {
        depthAlarm->onState = true;
        depthAlarm->timer.start(500);
    }
}

void MainWindow :: depthAlarm_run() {
    // If alarm state was on, set to off
    // Else, set to true
    if (depthAlarm->flashState) {
        ui->label_depthAlarm->setPixmap(QPixmap(":/icons/icons/alarm_bw.png"));
        depthAlarm->flashState = false;
        depthAlarm->count++;
        if (depthAlarm->activated && !depthAlarm->audio->state() && state.PLAY_MODE != 4) {
            depthAlarm->audio->play();
        }
    } else {
        ui->label_depthAlarm->setPixmap(QPixmap(":/icons/icons/alarm_red.png"));
        depthAlarm->flashState = true;
        depthAlarm->count++;
    }
    if (depthAlarm->count > depthAlarm->audio->duration()/500) {
        depthAlarm->onState = false;
        depthAlarm->timer.stop();
        ui->label_depthAlarm->setPixmap(QPixmap(":/icons/icons/alarm_bw.png"));
        depthAlarm->flashState = false;
        depthAlarm->count = 0;
    }
}

void MainWindow :: depthAlarm_hide() {
    depthAlarm->hide();
    depthAlarm->hideTimer.stop();
}

void MainWindow :: depthAlarm_updateHandlePosition(int xPos) {
    if (!depthAlarm->activated)     return;
    depthAlarm->handle->setVisible(depthAlarm->activated);
//    DepthAlarmHandle->setVisible(depthAlarm->alarmOn);
    bool found;
    double yStart = 0;
    if (depthMapNum_queue.size() > 0) {
        yStart = depthMapNum_queue.first()->getValueRange(found).lower;
    }
    depthAlarm->level = round(xPos);
    depthAlarm->handle->start->setCoords(xPos, yStart);
    depthAlarm->handle->end->setCoords(xPos, ping->lastTracePos);
    if (state.PLAY_MODE == 4)      depthPlot->replot(QCustomPlot::rpQueuedReplot);

//    double widthDepth = (depthPlot->xAxis->range().upper - depthPlot->xAxis->range().lower)/(depthPlot->width()*1.0);
//    double heightDepth = (depthPlot->yAxis->range().upper - depthPlot->yAxis->range().lower)/(depthPlot->height()*1.0);
//    double xIncDepth = widthDepth * 20.0;
//    double yIncDepth = heightDepth * 20.0;
//    DepthAlarmHandle->topLeft->setCoords(xPos-xIncDepth/2,ping->lastTracePos + yIncDepth);
//    DepthAlarmHandle->bottomRight->setCoords(xPos+xIncDepth/2,ping->lastTracePos);

}

void MainWindow :: depthAlarm_selectHandle(QMouseEvent *e) {
    QList<QCPAbstractItem*> itemList = depthPlot->selectedItems();
    if (e->button()==Qt::LeftButton && itemList.length() > 0) {
        if (itemList.first() == depthAlarm->handle/* || itemList.first() == DepthAlarmHandle*/) {
            flag.depthAlarmItemSelected = true;
            flag.depthAlarmItemActive = true;
            depthPlot->setCursor(Qt::SplitHCursor);
        } else {
            flag.depthAlarmItemSelected = false;
            flag.depthAlarmItemActive = false;
        }
    } else {
        flag.depthAlarmItemSelected = false;
        flag.depthAlarmItemActive = false;
    }
}
void MainWindow :: depthAlarm_selectHandle(bool selected) {
//    QList<QCPAbstractItem*> itemList = depthPlot->selectedItems();
    if (selected == true/* && itemList.length() > 0*/) {
        flag.depthAlarmItemSelected = true;
        flag.depthAlarmItemActive = true;
        depthPlot->setCursor(Qt::SplitHCursor);
    } else {
        flag.depthAlarmItemSelected = false;
        flag.depthAlarmItemActive = false;
    }
}

void MainWindow :: depthAlarm_moveHandle(QMouseEvent *e) {
    QList<QCPAbstractItem*> itemList = depthPlot->selectedItems();

    double x = depthPlot->xAxis->pixelToCoord(e->pos().x());
    double y = depthPlot->yAxis->pixelToCoord(e->pos().y());

    double widthDepth = (depthPlot->xAxis->range().upper - depthPlot->xAxis->range().lower)/(depthPlot->width()*1.0);
    double heightDepth = (depthPlot->yAxis->range().upper - depthPlot->yAxis->range().lower)/(depthPlot->height()*1.0);
    double xIncDepth = widthDepth * 20.0;
    double yIncDepth = heightDepth * 20.0;
    double yStart = 0;
    if (depthMapNum_queue.size() > 0) {
        bool found;
        yStart = depthMapNum_queue.first()->getValueRange(found).lower;
    }

//    if (x < depthAlarm->level + 2 && x > depthAlarm->level - 2 && y > ping->lastTracePos && y < ping->lastTracePos + yIncDepth) {
//        if (flag.depthAlarmItemSelected)
//            depthPlot->setCursor(Qt::SplitHCursor);
//        else
//            depthPlot->setCursor(Qt::PointingHandCursor);
//    } else {
//        depthPlot->setCursor(Qt::ArrowCursor);
//    }
    if (flag.depthAlarmItemActive) {
        depthPlot->setInteractions(QCP::iSelectItems);
        double xRound = round(x);
        if (xRound > 10) {
            xRound = 10;
        } else if (xRound < 5) {
            xRound = 5;
        }
        depthAlarm->level = xRound;
        depthAlarm->handle->start->setCoords(xRound, yStart);
        depthAlarm->handle->end->setCoords(xRound, ping->lastTracePos);
//        DepthAlarmHandle->topLeft->setCoords(xRound-xIncDepth/2,ping->lastTracePos + yIncDepth);
//        DepthAlarmHandle->bottomRight->setCoords(xRound+xIncDepth/2,ping->lastTracePos);
    }
}

void MainWindow :: depthAlarm_deselectHandle(QMouseEvent *e) {
    if (flag.depthAlarmItemActive) {
        depthPlot->setInteractions(QCP::iRangeDrag | QCP::iSelectItems);
        flag.depthAlarmItemActive = false;
        updateAlarmSettings();
    }
}

void MainWindow :: depthAlarm_updateButtonState() {
    if (depthAlarm->activated)      ui->btn_depth->setIcon(QIcon(":/icons/icons/alarm_red.png"));
    else                            ui->btn_depth->setIcon(QIcon(":/icons/icons/alarm_bw.png"));
}

void MainWindow :: on_btn_depth_clicked() {
    if (depthAlarm->isHidden()) {
        depthAlarm->show();
        depthAlarm->move(350, 60);
        depthAlarm->hideTimer.start(30000);
    } else {
        depthAlarm->hide();
    }
}

/**
 * @brief MainWindow::keyPressEvent
 * @param e
 * When measuring tape is already open, press Esc will clear
 * all the measuring tape on the customPlotLF
 */
void MainWindow :: keyPressEvent(QKeyEvent * e) {
    QMouseEvent* event=nullptr;
    int key = e->key();

    if (e->key() == Qt::Key_Enter) {
        if (depthAlarm->isHidden())
            depthAlarm->hide();
    }
    if (e->key() == Qt::Key_Escape) {
        depthAlarm->hide();
        tools->removeAllWaypointScanAreas();
        if (flag.targetToolActive)   on_actionFlag_triggered();
        if (flag.measuringTapeActive)    {
            if (tools->activeMeasuringTapeObject.start_measuring == true)      measuringTape_UndoLastMeasure();
            else            on_actionMeasuring_Tape_triggered();
        }
        if (flag.shadowToolActive)    {
            if (tools->activeMeasuringTapeObject.start_measuring == true)      measuringTape_UndoLastMeasure();
            else            on_actionShadow_triggered();
        }
        if (Format->isZoomBoxAlwaysOn)       format_showZoomBox(false);
        if (cropTool->toolActive)       on_actionCrop_File_triggered();
        if (flag.waypointToolActive)    on_actionWaypoint_triggered();
    }

    if (e->key() == Qt::Key_Delete){
        target_DeleteSelectedItems();
        target_DeleteSelectedWaypoints();
        measuringTape_DeleteSelectedItems();
    }

    // Shortcuts with Down button
    if (e->key() == Qt::Key_Down && state.PLAY_MODE == 5 && state.GUI_MODE == 2){ // File Playback
        player_addColorMap();
        ui->quick_playpause->setEnabled(true);
        //statusBar()->removeWidget(status_msg);
        progressBar->show();
        ui->quick_playpause->setChecked(true);
        ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));
    }
    else if(e->modifiers()==(Qt::ControlModifier) && e->key() == Qt::Key_Down){
        plot_offset_verticalShiftDown();
    }
    else if (e->modifiers()==(Qt::ShiftModifier) && e->key() == Qt::Key_Down) {
        target_keyPressEvent(e);
    }
    else if (e->key() == Qt::Key_Down) {
        // Select next item down, or select top item
        target_keyPressEvent(e);
    }

    // Shortcuts with Up button
    if (e->modifiers()==(Qt::ShiftModifier) && e->key() == Qt::Key_Up) {
        target_keyPressEvent(e);
    }
    else if(e->modifiers()==(Qt::ControlModifier) && e->key() == Qt::Key_Up){
        plot_offset_verticalShiftUp();
    }
    else if (e->key() == Qt::Key_Up) {
        // Select next item up, or select bottom item
        target_keyPressEvent(e);
    }

    if (e->key() == Qt::Key_Right && state.PLAY_MODE > 2 && state.GUI_MODE == 2) {
        player_skipPings(500);
    }
    else if (e->key() == Qt::Key_Left && state.PLAY_MODE > 2 && state.GUI_MODE == 2){
        player_playFromPing(((int)ping->pingNumCurrent) - 500);
    }

    if(e->modifiers()==(Qt::ControlModifier) && e->key() == Qt::Key_A){
//        ui->flagList->setSelectionMode(QAbstractItemView::ExtendedSelection);
        if (state.GUI_MODE == 2 && !DEVELOPER_TESTING) {
            for (int i = 0; i < tools->targetList.length(); i++) {
                target_selectTarget(&tools->targetList[i], true);
            }
        } else {
            if (tools->waypointList.length() == tools->selectedWaypoints().length()) {
                // Deselect all waypoints if they are selected
                tools->tracker.resetRoute();
                target_selectAllWaypoints(false);
            } else {
                tools->tracker.resetRoute();
                for (int i = 0; i < tools->waypointList.length(); i++) {
                    target_selectWaypoint(&tools->waypointList[i], true, true);
                    tools->tracker.appendRoute(&tools->waypointList[i]);
                }
                if (tools->waypointList.length() > 0)
                    tools->tracker.anchorWaypoint(&tools->waypointList[0]);
                tools->tracker.displayRoute();
            }
            ui->gpsPlot->replot(QCustomPlot::rpQueuedReplot);
        }
    }
    // Recording shortcut
    if((e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_R)){
        // qDebug() << "ctrl + S";
        if (((state.GUI_MODE == 0 && state.SIM_MODE == 1) ||
                (state.GUI_MODE == 1 && state.RT_MODE == 2))) {
            if (!recorder_startStop(true)) {
                return;
            }
            elTime.start();
            timer->start(500);
            setup_buttonsAndTools();
            plot_updateLfHfLegend(" R ", true);
        }
    }

    if(e->modifiers()==(Qt::ControlModifier)){
        flag.ctrlKeyPressed = true;
    }
    if(e->modifiers()==(Qt::ShiftModifier)){
        flag.shiftKeyPressed = true;
    }

    // Play/Pause
    if(e->key() == Qt::Key_Space){
        ui->quick_playpause->setChecked(!ui->quick_playpause->isChecked());
        on_quick_playpause_clicked();
        return;

        if (state.GUI_MODE == 2) {
            if (state.PLAY_MODE == 4) {
                // Resume playback
                player_playData(true);
                setPlaySpeed(setting.plotRate);
            }
            else if (state.PLAY_MODE == 3) {
                // Pause playback
                player_playData(false);
                plot_updateLfHfLegend(" ll ", true);
            }
            else if (state.PLAY_MODE == 5) {
                // Continue to next color map
                player_addColorMap();
                ui->quick_playpause->setEnabled(true);
                //statusBar()->removeWidget(status_msg);
                progressBar->show();
                ui->quick_playpause->setChecked(true);
                ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));
            }
            else if (state.PLAY_MODE == 6) {
                state.PLAY_MODE = 3;
                player_selectFile();
                player_addColorMap();
                player_playData(true);
            }
        }
        else if (state.GUI_MODE == 1) {
            if (state.RT_MODE == 1) {
                // Real Time Mode and connected to board
                quint8 cmdType = 0;
                quint32 cmd_data = 1;
                Tcp_sendCommand(cmdType,cmd_data,0);
                resetPlotLists();
                state.RT_MODE = 2;

                replotTimer.start(10);//start replot timer
            }
            else if (state.RT_MODE == 2) {
                quint8 cmdType = 0;
                quint32 cmd_data = 0;
                Tcp_sendCommand(cmdType,cmd_data,0); //send
                resetPlotLists();
                replotTimer.stop();
                state.RT_MODE = 1;

                //changing state for quick record button
                timer->stop();
                recorder_startStop(false);
            }
            setup_buttonsAndTools();
        }

        else if (state.GUI_MODE == 0 && (legendText->text().compare("")==0)) {
            state.SIM_MODE = 0;
            simulationTimer.stop();
            //GUI_MODE = 1;
            plot_updateLfHfLegend(" ll ", false);

        }
        else if (state.GUI_MODE == 0 && (legendText->text().compare(" ll ")==0)) {
            // Simulation Mode
            state.SIM_MODE = 1;
            simulationTimer.start(300);//simulationTimer calls plottrace on timeout
            statusBar()->show();

            ui->quick_playpause->setChecked(true);
            ui->quick_playpause->setIcon(QIcon(":/icons/icons/pause.png"));
            plot_updateLfHfLegend("", false);
            replotTimer.start(10);//start replot timer
        }
        setup_buttonsAndTools();
        // ui->customPlot->setFocusPolicy(Qt::StrongFocus);
    }
    // Player Buttons
    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_O){
        flag.ctrlKeyPressed = false;
        on_actionOpen_triggered();
    }
    else if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_1) {
        if (state.GUI_MODE == 1 && state.RT_MODE > 0) {
            on_btn_range1_clicked();
        }
    }
    else if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_2) {
        if (state.GUI_MODE == 1 && state.RT_MODE > 0) {
            on_btn_range2_clicked();
        }
    }
    else if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_3) {
        if (state.GUI_MODE == 1 && state.RT_MODE > 0) {
            on_btn_range3_clicked();
        }
    }
    else if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_4) {
        if (state.GUI_MODE == 1 && state.RT_MODE > 0) {
            on_btn_range4_clicked();
        }
    }

    // System Settings Shortcut
    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_J){devWindowInstance->show();}
    // Scope Window
    if(e->modifiers() == (Qt::AltModifier) && e->key() == Qt::Key_S){on_scope_btn_clicked();}

    if(e->key() == Qt::Key_P){
        //gpsPlotReplot();
    }

    // Measuring Tape
    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_Z){
        measuringTape_UndoLastMeasure();
    }
    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_U){
        measuringTape_DeleteAllItems();
    }
    if(e->key() == Qt::Key_M){
        on_actionMeasuring_Tape_triggered();
    }
    if(e->key() == Qt::Key_H){
        on_actionShadow_triggered();
    }
    if(e->key() == Qt::Key_C){
        on_actionCrop_File_triggered();
    }
    if (e->key() == Qt::Key_T){
        on_actionFlag_triggered();
    }
    if(e->key() == Qt::Key_W){
        PingMapItem mapItem = dataMap.last();
        if (!mapItem.isValid)        return;
        flag.waypointToolActive = true;
        target_NewWaypoint(mapItem.lattitude, mapItem.longitude);
        flag.waypointToolActive = false;
    }
    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_E){
        on_actionSaveTarget_triggered();
    }
    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_X){
        on_actionConverter_triggered();
    }

    if(e->modifiers() == (Qt::ControlModifier) && e->key() == Qt::Key_G){ // Gain Button
        on_gain_btn_clicked();
    }

}

void MainWindow :: keyReleaseEvent(QKeyEvent *event){
    /*This function keeps track of when the ctrl key was released
     *Sets the flag to false
    */

    if(event->key()==(Qt::Key_Control) ){
        flag.ctrlKeyPressed = false;
    }
    if(event->key()==(Qt::Key_Shift) ){
        flag.shiftKeyPressed = false;
    }
}

void MainWindow :: mouseMoveEvent(QMouseEvent *event) {
//    bool paused = ((state.PLAY_MODE==4 || state.PLAY_MODE == 5 || state.PLAY_MODE == 6) && state.GUI_MODE == 2)
//            || (state.RT_MODE > 0 && state.GUI_MODE == 1) || state.GUI_MODE == 0;
//    if (!paused)    return;

//    if (crosshair_lf.isVisible() || crosshair_hf.isVisible()) {
//        if (customPlotLF->rect().contains(customPlotLF->mapFromGlobal(QCursor::pos())) && Format->isOpenLf) {}
//        else if (customPlotHF->rect().contains(customPlotHF->mapFromGlobal(QCursor::pos())) && Format->isOpenHf){}
//        else if (ui->sensor_graph->rect().contains(ui->sensor_graph->mapFromGlobal(QCursor::pos()))  && Format->isOpen_sensorPlot) {}
//        else if (depthPlot->rect().contains(depthPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_DepthPlot){}
//        else if (ui->gpsPlot->rect().contains(ui->gpsPlot->mapFromGlobal(QCursor::pos())) && Format->isOpen_gpsPlot){}
//        else {
//            crosshair_lf.setVisible(false);
//            crosshair_hf.setVisible(false);
//            plot_replotAll();
//        }
//    }
}

void MainWindow :: mousePressEvent(QMouseEvent *event) {
    if (!depthAlarm->isHidden()) {
        depthAlarm->hide();
    }
}

void MainWindow :: on_about_btn_clicked(int IDnum, quint16 LFrequency, quint16 HFrequency, int Angle) {
    static int packetID2 = 0;
    int modelNo = 92;
    if (ping->Oct31ASTest)                        modelNo = 70;
    else if (ping->firmwareVer / 1000000000 == 2)      modelNo = 70;
    else if (ping->firmwareVer / 1000000000 == 1) modelNo = 90;

    if (ping->lowFrequency > 100 && ping->lowFrequency < 200)       modelNo += 1;
    else if (ping->lowFrequency > 200 && ping->lowFrequency < 300)  modelNo += 2;
    else                                                modelNo += 3;

    if (ping->firmwareVer == NULL) {//if there is no valid firmware version detected (No board connected)
        QMessageBox::information(this,
                                 tr("Pathway Help"),
                                 QString(developmentTitle + " \nSonar Information Unavailable\nPlease connect sonar or open a recorded file"));
    }
    else {
        int firm = ping->firmwareVer;
        QString firmString = "";
        int counter = 1;

        //this while loop turns the integer firmware version into a string
        while (firm != 0) {
            firmString += QString::number(firm%10);
            firm = firm/10;
            if (counter == 3 || counter == 6 || counter == 9) {
                firmString += ".";
            }
            counter ++;
        }

        QString firmwareString = "";
        for(int i=firmString.length()-1; i>=0; i--){
            firmwareString += firmString.at(i);
        }

        if(packetID2 == 0){
            QMessageBox::information(this,
                                     tr("About"),
                                     QString(developmentTitle + " \nModel " + QString::number(modelNo) +
                                             " \nFirmware: " + firmwareString +
                                             " \nLow Frequency: "+QString::number(ping->lowFrequency)+
                                             " kHz\nHigh Frequency: "+QString::number(ping->highFrequency)+" kHz"));
        } else if(packetID2 == 1) {
            QMessageBox::information(this,
                                     tr("About"),
                                     QString(developmentTitle + " \nModel " + QString::number(modelNo) +
                                             " \nFirmware: " + firmwareString +
                                             " \nLow Frequency: " + QString::number(ping->lowFrequency) +
                                             " kHz\nHigh Frequency: " + QString::number(ping->highFrequency)+" kHz"));
        } else {
            QMessageBox::information(this,
                                     tr("About"),
                                     QString(developmentTitle + " \nModel " + QString::number(modelNo) +
                                             " \nFirmware: " + firmwareString +
                                             " \nLow Frequency: " + QString::number(ping->lowFrequency) +
                                             " kHz\nHigh Frequency: " + QString::number(ping->highFrequency)+" kHz"));
        }
    }
}

void MainWindow :: on_add_gain_clicked() {

    if (flag.readyToTransmit == 1) {
        Tcp_sendCommand(7, 1, 0);
    }
}

void MainWindow :: on_sub_gain_clicked() {

    if (flag.readyToTransmit == 1) {
        Tcp_sendCommand(7, 0, 0);
    }
}

/****************GNSS Toolbar****************/

void MainWindow :: on_gnssBtn_clicked() {
    navSettingInstance->on_btn_refresh_clicked();
    navSettingInstance->setWindowTitle(" Navigation Settings");//->grab();
    navSettingInstance->show();
}

void MainWindow :: on_zoomIn_clicked() {
    ChartWindowInstance->zoomScale *= 0.5;
    ChartWindowInstance->mapPlotPtr->xAxis->scaleRange(0.5);
    ChartWindowInstance->mapPlotPtr->yAxis->scaleRange(0.5);
    ChartWindowInstance->mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: on_zoomOut_clicked() {
    ChartWindowInstance->zoomScale /= 0.5;
    ChartWindowInstance->mapPlotPtr->xAxis->scaleRange(2);
    ChartWindowInstance->mapPlotPtr->yAxis->scaleRange(2);
    ChartWindowInstance->mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: on_flagBtn_clicked() {
    //    QCursor cursor;
    //    QPixmap pixmap(":/icons/icons/pin.png");
    //    QPixmap pixmap2 = pixmap.scaled(40,40);

    //    //0,0 is the coords of hotspot
    //    cursor = QCursor(pixmap2,-20,40);
    //    setCursor(cursor);

    on_actionFlag_triggered();

    if (flag.measuringTapeActive)    on_actionMeasuring_Tape_triggered();

}

void MainWindow :: on_trackpointsBtn_clicked() {
    if (ChartWindowInstance->newCurve->data()->size() > 0) {
        //int ch = QMessageBox::warning(this, "Warning",
        //                              "Are you sure to delete all trackpoints?",
        //                              QMessageBox::Yes|QMessageBox::No);
        //if(ch!=QMessageBox::Yes)    return;

        ChartWindowInstance->newCurve->data()->clear();
        ChartWindowInstance->newCurve2->data()->clear();
        ChartWindowInstance->newCurve3->data()->clear();
        ChartWindowInstance->trackpoints->data()->clear();
        ChartWindowInstance->portLine->setVisible(false);
        ChartWindowInstance->stbdLine->setVisible(false);
        ChartWindowInstance->mapPlotPtr->replot();
    }
}

/****************Hidden Buttons****************/

/**
 * @brief MainWindow::on_scope_btn_clicked
 * Opens the "scope" menu when the Scope button is clicked
 */
void MainWindow :: on_scope_btn_clicked() {
    if(!flag.graphDialogActive){
        graphWindowInstance->show();    //Opens the scope window
        ui->scope_btn->setStyleSheet("background-color: rgb(255, 255, 255);");
        flag.graphDialogActive = true;
    }
    else{
        //scope = 0;
        graphWindowInstance->hide();
        ui->scope_btn->setStyleSheet("background-color: rgb(255, 255, 255);");
        //ui->scope_btn->setStyleSheet("QToolTip{border: 0px solid black; background:white;}");
        flag.graphDialogActive = false;
    }
}

/****************Menu Bar****************/

/** When open option is triggered from the file menu**/
void MainWindow :: on_actionOpen_triggered(){

    //on_quick_stop_btn_clicked();
    if(ui->quick_replay_btn->isChecked()){
        ui->quick_replay_btn->setChecked(false);
    }
    plot_updateLfHfLegend("", false);
    setup_buttonsAndTools();

    if (state.GUI_MODE == 0 && state.SIM_MODE > 0) {
        Tcp_sendCommand(0,0,0); // pause sonar
        resetPlotLists();
        replotTimer.stop();
        simulationTimer.stop();

        //changing state for quick record button
        timer->stop();
        recorder_startStop(false);
    } else if (state.GUI_MODE == 1 && state.RT_MODE > 0) {
        Tcp_sendCommand(0,0,0); // pause sonar
        resetPlotLists();
        replotTimer.stop();

        //changing state for quick record button
        timer->stop();
        recorder_startStop(false);

        if (state.RT_MODE == 2) {
            state.RT_MODE = 1;
            QMessageBox::StandardButton checkMsg = QMessageBox::question(this, "Disconnect Sonar?",
                                                                         "Are you sure you want to disconnect sonar?");
            setup_buttonsAndTools();
            if (checkMsg != QMessageBox::Yes)    return;
        }
    } else if (state.GUI_MODE == 2 && state.PLAY_MODE == 3) {
        player_playData(false);
        setup_buttonsAndTools();
    }

    state.GUI_MODE = 2;
    state.PLAY_MODE = 1;

    player_selectFile();

    if (state.PLAY_MODE == 4) {
        player_playData(true);
        statusBar()->show();
        emit formatStatusBar(true);
    }
    if(ui->quick_replay_btn->isChecked()){
        ui->quick_replay_btn->setChecked(false);
    }
    setup_buttonsAndTools();
}

/** When option save is triggered from the file menu**/
void MainWindow :: on_actionSave_triggered(){

    if((state.GUI_MODE == 1 && state.RT_MODE == 2) ||
            (state.GUI_MODE == 0 && state.RT_MODE == 1)) {
        if (!recorder_startStop(true)) {
            return;
        }
        setup_buttonsAndTools();
        elTime.start();
        timer->start(500);

        plot_updateLfHfLegend(" R ", true);
    }
}

void MainWindow :: on_actionSaveAs_triggered() {
    if (state.GUI_MODE != 2)    return;

    // Open dialog window and get new filename
    QString fileName = player.openedFileName.mid(0, player.openedFileName.length()-4) + "_Copy.JAS";
    QString filter = "Jetasonic Scan Data (*.JAS)";
    fileName  = QFileDialog::getSaveFileName(this, tr("Save Recorded Sonar File"),
                                             fileName, filter, &filter, QFileDialog::DontConfirmOverwrite);
    if (fileName == "")     return;  // User clicked cancel
    if (QFile(fileName).exists()) {
        int i = 2;
        while (QFile(fileName.mid(0, fileName.length() - 4) + " (" + QString::number(i) + ").JAS").exists()) {
            i++;
        }
        fileName = fileName.mid(0, fileName.length() - 4) + " (" + QString::number(i) + ").JAS";
        QString message = "Do you want to save as " + fileName + " instead?";
        int ret = QMessageBox::warning(this, tr("File Already Exists"), message, QMessageBox::Yes | QMessageBox::No);
        switch(ret){
        case QMessageBox::Yes:
            break;
        case QMessageBox::No:
            return;
        }
    }

    // Copy sonar file to new filepath
    player.sonarFile.copy(fileName);

    // Copy flag file to new path
    player.targetFile.setFileName(player.openedFileName + "FLAG");
    player.targetFile.open(QIODevice::ReadOnly);
    player.targetFile.copy(fileName.mid(0, fileName.length()-4) + ".JASFLAG");

    // Copy gps file to new path
    player.GPSFile.copy(fileName.mid(0, fileName.length()-4) + ".JASLOG");

    // Copy target images to new paths
    tools->copyTargetImages(fileName.mid(0, fileName.length()-4));

    // Load sonar and gps files and resume from last position
    player.sonarFile.open(QIODevice::ReadOnly);
    player.sonarFile.seek(player.filePos - player.bufferPos);
    player.GPSFile.open(QIODevice::ReadOnly);
    player.fileBuffer.clear();
    player_loadToBuffer();
}

void MainWindow :: on_actionClose_triggered() {
    state.GUI_MODE = 1;
    state.PLAY_MODE = 0;
    plot_updateLfHfLegend("", false);
    resetGui();
    player.fileBuffer = nullptr;
    if (ui->quick_replay_btn->isChecked())
        ui->quick_replay_btn->setChecked(false);
    setup_buttonsAndTools();
}

/**
 * @brief MainWindow::on_actionQuit_triggered
 * Quits application
 */
void MainWindow :: on_actionQuit_triggered() {
    close();
}

/**
 * @brief MainWindow::on_actionSettings_triggered
 * To launch System Settings window.
 */
void MainWindow :: on_actionSettings_triggered() {
    systemSettingInstance->show();
}

/**
 * @brief MainWindow::on_actionAbout_triggered
 * Shows the information about the Database creation
 * date, the current version of the GUI and the
 * databases.
 */
void MainWindow :: on_actionAbout_3_triggered() {
    on_about_btn_clicked();
}

/** When Help option is triggered**/
void MainWindow :: on_actionHelp_2_triggered(){

    QString rootPath = QDir().currentPath();
    QDesktopServices::openUrl(QUrl("file:///"+rootPath+"/Jaimi UI Manual 1.03.pdf", QUrl::TolerantMode));

    //    emit createHelpWindow();
}

void MainWindow :: on_actionMeasuring_Tape_triggered(){
    if(flag.measuringTapeActive == false){
        if (state.GUI_MODE != 2)        return;
        // Turn off all other tools
        if (flag.targetToolActive)         on_actionFlag_triggered();
        if (flag.shadowToolActive)      on_actionShadow_triggered();
        if (cropTool->toolActive)       on_actionCrop_File_triggered();
        if (flag.waypointToolActive)    on_actionWaypoint_triggered();

        // Set measuring tape tool active
        ui->actionMeasuring_Tape->setText("Measuring tape ✓");
        flag.measuringTapeActive = true;
    } else{
        if (tools->activeMeasuringTapeObject.start_measuring == true){
            measuringTape_UndoLastMeasure();
        }
        measuringTape_DeleteAllItems();
        ui->actionMeasuring_Tape->setText("Measuring tape  ");
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
        flag.measuringTapeActive = false;
        tools->activeMeasuringTapeObject.start_measuring =false;
    }
    plot_setCursor();
    plot_updateLfHfLegend(legendTxt, 1);
}

void MainWindow :: on_actionShadow_triggered(){
    if (flag.shadowToolActive == false) {
        if (state.GUI_MODE != 2)        return;
        // Turn off all other tools
        if (flag.measuringTapeActive)   on_actionMeasuring_Tape_triggered();
        if (flag.targetToolActive)         on_actionFlag_triggered();
        if (cropTool->toolActive)      on_actionCrop_File_triggered();
        if (flag.waypointToolActive)    on_actionWaypoint_triggered();
        ui->actionShadow->setText("Height ✓");

        flag.shadowToolActive = true;
    } else {
        if (tools->activeMeasuringTapeObject.start_measuring == true){
            measuringTape_UndoLastMeasure();
        }
        measuringTape_DeleteAllItems();
        customPlotLF->replot(QCustomPlot::rpQueuedReplot);
        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
        ui->actionShadow->setText("Height  ");
        flag.shadowToolActive = false;
        tools->activeMeasuringTapeObject.start_measuring =false;
    }
    plot_setCursor();
    plot_updateLfHfLegend(legendTxt, 1);
}

void MainWindow :: on_actionCrop_File_triggered() {
    if (cropTool->isHidden() && !cropTool->toolActive) {
        if (state.GUI_MODE != 2)        return;
        cropTool->show();
        if (flag.measuringTapeActive)   on_actionMeasuring_Tape_triggered();
        if (flag.targetToolActive)         on_actionFlag_triggered();
        if (flag.shadowToolActive)         on_actionShadow_triggered();
        if (flag.waypointToolActive)    on_actionWaypoint_triggered();
        cropTool->toolActive = true;
        ui->actionCrop_File->setText("Crop ✓");
        cropTool->setCropStart();
        cropTool->updatePlayerPosition(progressBar->value()/(progressBar->maximum()*1.0));
    } else {
        cropTool->UndoLastClick();
        cropTool->hide();
        cropTool->toolActive = false;
        ui->actionCrop_File->setText("Crop  ");
    }
    plot_setCursor();
    format_updatePlotSizes();
    plot_updateLfHfPlots();
    plot_replotAll();
}

/**
 * @brief MainWindow::on_actionActivate_triggered
 * Activates the simulation mode.
 */
void MainWindow :: on_actionActivate_triggered() {
    if (PLAYER_DEMO)     return;
    simulate_Plot(0);
}

/**
 * @brief MainWindow::on_actionDeactivate_triggered
 * Deactivates the simulation mode.
 */
void MainWindow :: on_actionDeactivate_triggered() {
    if (PLAYER_DEMO)     return;
    simulate_Plot(1);
}

void MainWindow :: on_actionSystem_triggered() {
    devWindowInstance->show();
}

/**
 * @brief MainWindow::on_actionUserMenu_triggered
 * To launch the user settings window.
 */
void MainWindow :: on_actionUserMenu_triggered() {
    userWindowInstance->setWindowTitle(" Settings");
    if (userWindowInstance->isHidden()) {
        userWindowInstance->show();//->open();
        if (!Format->isHidden()) {
            Format->hide();
        }
    } else
        userWindowInstance->hide();
}

void MainWindow :: closeTvg() {
    ui->tvg_btn->setChecked(false);
}

void MainWindow :: on_tvg_btn_clicked() {
    if (ui->tvg_btn->isChecked()) {
        tvgWindowInstance->show();
//        ui->LF_HF_lLayout->addWidget(testformui);
//        testformui->raise();
//        this->updateGeometry();
//        testformui->show();

        tvgWindowInstance->move(QApplication::desktop()->screenGeometry(this).width()-tvgWindowInstance->width(), QApplication::desktop()->screenGeometry(this).height()/2-tvgWindowInstance->height()/2);
    } else {
        tvgWindowInstance->hide();
    }
}

void MainWindow :: on_gain_btn_clicked() {
    if (!ui->gainSlider->isVisible()) {
        ui->gain_btn->setChecked(true);
        emit showFormatGainSlider(true);
        ui->actionShow_gain->setText("Show Gain Slider ✓");
        ui->gainSlider->show();
        ui->mainGain_btn->show();
        ui->mainGain_txt->show();
        ui->hfGainSlider->show();
        ui->hfGain_btn->show();
        ui->hfGain_txt->show();
        ui->slider_gainDepth->show();
        ui->label_gainDepth->show();
        ui->label_gainDepthTxt->show();
        gainSliderTimer.start(60000);
    }
    else {
        ui->gain_btn->setChecked(false);
        emit showFormatGainSlider(false);
        ui->actionShow_gain->setText("Show Gain Slider");
        ui->gainSlider->hide();
        ui->mainGain_btn->hide();
        ui->mainGain_txt->hide();
        ui->hfGainSlider->hide();
        ui->hfGain_btn->hide();
        ui->hfGain_txt->hide();
        ui->slider_gainDepth->hide();
        ui->label_gainDepth->hide();
        ui->label_gainDepthTxt->hide();
        gainSliderTimer.stop();
    }
//    format_splitterHandleChanged(); // Call this to correct any issues caused by minimum size for depth gain slider
}

void MainWindow :: hideGainSliders() {
    if (ui->gainSlider->isVisible())       on_gain_btn_clicked();
}

/****************Settings - Altimeter Tab****************/

void MainWindow :: showLabelOnAltGraph(double y){
    // check if we should add a label to plot
    // if true
    // find the point on the graph (depthPlot->graph(2)) that has that yCoord
    //double x = depthPlot->graph(2)->data()->at(y)->value;
    // add new label - similar to the onMousePressed function
    // add new QCPCircleItem to depthPlot on graph(2)
    // if false, then return
    static double counter = 20;
    if (y < 1)  counter = 20;
    //QCPItemText *altimeterText1 = new QCPItemText(depthPlot);
    if(counter < y){
        QFont altFont(font().family(),12);
        altFont.setFamily("Arial");
        altFont.setBold(1);
        double xCoordinates = ping->lastTraceAlt;//(kalmanDepth+kalmanDepth2)/2;

        QCPItemText *altimeterText = new QCPItemText(depthPlot);

        altimeterText->setPositionAlignment(Qt::AlignRight);
        altimeterText->setText(QString::number(xCoordinates, 'd', 1));
        //altimeterText->setColor(Qt::green);
        altimeterText->setColor(Qt::white);
        altimeterText->setTextAlignment(Qt::AlignRight);
        altimeterText->setFont(altFont);
        altimeterText->position->setCoords((xCoordinates-0.5),y);
        altimeterText->setLayer("boatLayer");

        altimeterText->setVisible(flag.showDepthText);
        depthPlot->replot(QCustomPlot::rpQueuedReplot);
        counter=counter+20;
        text_struct tmp;
        tmp.yCoord = y;
        tmp.item = altimeterText;
        item_text.append(tmp);
    }
}

void MainWindow :: depthLabelHide(bool show){
    flag.showDepthText = show;
    for(int i=0;i<item_text.length();i++){
        item_text.at(i).item->setVisible(show);
    }
    depthPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: tractionStripHide(bool show){
    if(show){
        depthPlot->graph(3)->setVisible(show);
        depthPlot->graph(4)->setVisible(show);
    }
    else{
        depthPlot->graph(3)->setVisible(show);
        depthPlot->graph(4)->setVisible(show);
    }
    depthPlot->replot(QCustomPlot::rpQueuedReplot);
}

void MainWindow :: setTransparency(int value, int i){

    int newValue = (value*256)/100;
    QColor GradientColor1 =  QColor(176, 224, 230);
    GradientColor1.setAlpha(0);

    QColor gradientColor1;
    QColor gradientColor2;

    switch(i){
    case 1:
        gradientColor1 = QColor(238, 192, 198);
        gradientColor2 = QColor(126, 232, 250);
        break;
    case 2:
        gradientColor1 = QColor(102, 102, 238);
        gradientColor2 = QColor(187, 187, 255);
        break;
    case 3:
        gradientColor1 = QColor(41, 82, 74);
        gradientColor2 = QColor(233, 188, 183);
        break;
    case 4:
        gradientColor1 = QColor(158, 118, 143);
        gradientColor2 = QColor(159, 164, 196);
        break;
    case 5:
        gradientColor1 = QColor(134, 22, 87);
        gradientColor2 = QColor(255, 166, 158);
        break;
    default:
        return;
    }
    gradientColor1.setAlpha(newValue);
    gradientColor2.setAlpha(newValue);

    //    QLinearGradient depthPlotBackground;
    //    //depthPlotBackground.setStart(350, 0);
    //    depthPlotBackground.setStart(500, 0);
    //    depthPlotBackground.setFinalStop(0, 0);
    //    //depthPlotBackground.setColorAt(0.5,GradientColor2);
    //    depthPlotBackground.setColorAt(0, gradientColor1);
    //    depthPlotBackground.setColorAt(0.5, gradientColor2);
    //    depthPlotBackground.setColorAt(1, gradientColor2);
    //    ui->depthCPlot->graph(2)->setBrush(QBrush(depthPlotBackground));
    //    ui->depthCPlot->replot();
    //    gradientColor1.setAlpha(newValue);
    //    gradientColor2.setAlpha(newValue);

    QLinearGradient depthPlotBackground;
    // qDebug()<<"gradin: "<<nValue;
    //depthPlotBackground.setStart(350, 0);
    depthPlotBackground.setStart(setting.nValue, 0);
    depthPlotBackground.setFinalStop(0, 0);
    //depthPlotBackground.setColorAt(0.5,GradientColor2);
    depthPlotBackground.setColorAt(0, gradientColor1);
    depthPlotBackground.setColorAt(0.5, gradientColor2);
    depthPlotBackground.setColorAt(1, gradientColor2);
    ui->depthCPlot->graph(2)->setBrush(QBrush(depthPlotBackground));
    ui->depthCPlot->replot(QCustomPlot::rpQueuedReplot);

}

void MainWindow :: setAltGradient(int value){
    setting.nValue = value;
}

/****************Format Window****************/

void MainWindow :: format_actionTriggered(){
    Format->setWindowTitle(" Format");
    if (Format->isHidden()) {
        Format->show(); // use ->open() to make it modal
        if (!userWindowInstance->isHidden()) {
            userWindowInstance->hide();
        }
    } else
        Format->hide();
}

void MainWindow :: format_enableGnssFeatures(bool enable) {
//    emit showGnssClickable(enable);
    if (enable) {
//        ui->splitter_sonarGnss->setStyleSheet("QSplitter::handle:horizontal {\nbackground: #aaa;\nborder: 2px solid #777;"
//                                      "\nwidth: 4px;\nmargin-left: 10px;\nmargin-right: 10px;\nborder-radius: 4px;\n}");
//        ui->splitter_sonarGnss->setHandleWidth(5);
//        ui->flagBtn->setEnabled(true);
//        ui->actionFlag->setEnabled(true);
    } else {
//        ui->splitter_sonarGnss->setStyleSheet("QSplitter::handle:horizontal {\nbackground: #aaa;\nborder: 0px solid #777;"
//                                      "\nwidth: 0px;\nmargin-left: 0px;\nmargin-right: 0px;\nborder-radius: 0px;\n}");
//        ui->splitter_sonarGnss->setHandleWidth(0);
//        ui->splitter_sonarGnss->setSizes({ui->splitter_sonarGnss->width(), 0});
//        ui->flagBtn->setEnabled(false);
//        ui->actionFlag->setEnabled(false);
//        format_showGpsPlot(0);
    }
}

void MainWindow :: format_setPlotSizes() {
    ui->splitter_LfHf->setSizes({Format->heightLf, Format->heightHf});
    ui->splitter_depth->setSizes({Format->widthDepth, Format->widthSensor});
    ui->splitter_sonar->setSizes({Format->widthSensor + Format->widthDepth, Format->widthLfHf});
     ui->splitter_sonarGnss->setSizes({Format->widthLfHf + Format->widthSensor + Format->widthDepth, Format->widthGnss});
     ui->splitter_Gnss->setSizes({Format->heightGnss, Format->heightTools});
     ui->splitter_tools->setSizes({Format->widthTargets, Format->widthImage});

     if (Format->widthWindow > 0 && Format->heightWindow > 0)   this->resize(Format->widthWindow, Format->heightWindow);
     if (Format->windowMaximized)       this->showMaximized();//->setWindowState(Qt::WindowMaximized);

     Format->updateCheckBoxes();

     // Update LF/HF labels
     format_updatePlotSizes(); // Keep this at end of function but before ConfigureXLabels
     plot_updateLfHfPlots();
     plot_replotAll();
}

void MainWindow :: format_updateOtherSettings() {
    // Update format settings for signal plots, grid, status bar, etc.
    format_showGainSlider(Format->isGainSliderOn);
    format_showStatusBar(Format->isStatusBarShown);
    format_showSignalPlot(Format->isSignalPlotOpen);
    format_showGridOnPlot(Format->isGridShown);
    format_showZoomBox(Format->isZoomBoxAlwaysOn);
    format_showCompass(Format->isDisplayCompass);

    Format->updateCheckBoxes();
}

void MainWindow :: format_updatePlotSizes() {
    // Updates the variables on the format window
    // and updates the check boxes on the format dialog
    qDebug() << "Window visible" << this->isVisible() << this->isHidden();
    if (this->isVisible()) {
        Format->widthDepth = ui->splitter_depth->sizes().at(0);
        Format->widthSensor = ui->splitter_depth->sizes().at(1);
        Format->widthLfHf = ui->splitter_sonar->sizes().at(1);
        Format->widthGnss = ui->splitter_sonarGnss->sizes().at(1);
        Format->widthTargets = ui->splitter_tools->sizes().at(0);
        Format->widthImage = ui->splitter_tools->sizes().at(1);

        Format->heightGnss = ui->splitter_Gnss->sizes().at(0);
        Format->heightTools = ui->splitter_Gnss->sizes().at(1);
        Format->heightLf = ui->splitter_LfHf->sizes().at(0);
        Format->heightHf = ui->splitter_LfHf->sizes().at(1);

        Format->widthSonar = Format->widthLfHf + Format->widthDepth + Format->widthSensor;
    }
    Format->updateCheckBoxes();
    if (customPlotLF != nullptr) {
        if (Format->isOpenLf) {
            Format->plotWidth_LfHf = Format->widthLfHf - customPlotLF->axisRect()->margins().left() - customPlotLF->axisRect()->margins().right();
            Format->plotHeight_Lf = Format->heightLf - customPlotLF->axisRect()->margins().top() - customPlotLF->axisRect()->margins().bottom();
        }
        if (Format->isOpenHf) {
            Format->plotWidth_LfHf = Format->widthLfHf - customPlotHF->axisRect()->margins().left() - customPlotHF->axisRect()->margins().right();
            Format->plotHeight_Hf = Format->heightHf - customPlotHF->axisRect()->margins().top() - customPlotHF->axisRect()->margins().bottom();
        }
        if (Format->isOpen_DepthPlot)
            Format->plotHeight_depth = depthPlot->height() - depthPlot->axisRect()->margins().top() - depthPlot->axisRect()->margins().bottom();
        if (Format->isOpen_sensorPlot)
            Format->plotHeight_sensor = ui->sensor_graph->height() - ui->sensor_graph->axisRect()->margins().top() - ui->sensor_graph->axisRect()->margins().bottom();
    }

    // If all plots are closed, then open the LF plot
    // This should never occur, but just in case it does
    if (!Format->isOpenLf && !Format->isOpenHf && !Format->isOpen_DepthPlot && !Format->isOpen_sensorPlot
            && !Format->isOpen_gpsPlot && !Format->isOpen_TargetWidget && !Format->isOpen_imagePlot) {

    }
}

/**
 * @brief MainWindow::showDepthPlot
 * @param showing
 * Either reveals or hides depth plot
 */
void MainWindow :: format_showDepthPlot(bool showing) {
    int sensorGraphWidth = ui->sensor_graph->minimumWidth()+200;
    int depthGraphWidth = ui->depthCPlot->minimumWidth()+200;
    if (showing) {//if reveal depth plot
        if (Format->isOpen_sensorPlot) { // If sensor plot was open
            if(Format->isOpen_DepthPlot){
                // do nothing if depth plot was already open
            }else{
                // if depth plot was closed
                ui->splitter_depth->setSizes({depthGraphWidth,sensorGraphWidth});
            }
        } else { // if sensor plot was closed
            if (Format->isOpen_DepthPlot) {
                // do nothing
            } else {
                ui->splitter_depth->setSizes({depthGraphWidth, 0});
                ui->splitter_sonar->setSizes({depthGraphWidth, (ui->splitter_sonar->width()-depthGraphWidth)});
            }
            depthPlot->xAxis->ticker()->setTickCount(5);
        }
        ui->splitter_depth->setStyleSheet("QSplitter::handle:horizontal {\nbackground: #aaa;\nborder: 2px solid #777;"
                                      "\nwidth: 4px;\nmargin-left: 10px;\nmargin-right: 10px;\nborder-radius: 4px;\n}");
        ui->splitter_depth->setHandleWidth(5);
    }
    else { // closing depth plot
        if (Format->isOpen_sensorPlot) {
            ui->splitter_depth->setSizes({0, ui->splitter_depth->width()});
        } else {
            ui->splitter_sonar->setSizes({0, ui->splitter_sonar->width()});
       }
    }

    if(depthPlot->width()>=210)     depthPlot->xAxis2->ticker()->setTickCount(5);
    else                            depthPlot->xAxis2->ticker()->setTickCount(2);

    format_updatePlotSizes();    // Keep this at end of function
    plot_updateLfHfPlots();    // Call this after format_updatePlotSizes()
    plot_replotAll();
}

void MainWindow :: format_showSensorGraph(bool open) {
    int sensorGraphWidth = ui->sensor_graph->minimumWidth()+200;
    int depthGraphWidth = ui->depthCPlot->minimumWidth()+200;

    if (open) {
        if (Format->isOpen_DepthPlot) {
            if (Format->isOpen_sensorPlot){
                //do nothing
            } else {
                ui->splitter_depth->setSizes({depthGraphWidth, sensorGraphWidth});
            }
        } else {
            if (Format->isOpen_sensorPlot) {
                //if previously true, do nothing
            } else {
                ui->splitter_sonar->setSizes({sensorGraphWidth, (ui->splitter_sonar->width()-sensorGraphWidth)});
                ui->splitter_depth->setSizes({0, sensorGraphWidth});
            }
        }
        ui->sensor_graph->replot(QCustomPlot::rpQueuedReplot);
        Format->isOpen_sensorPlot = true;
    }
    else {
        if (Format->isOpen_DepthPlot) {
            ui->splitter_depth->setSizes({depthGraphWidth, 0});
        } else {
            ui->splitter_sonar->setSizes({0, ui->splitter_sonar->width()});
        }
        Format->isOpen_sensorPlot = false;
    }

    format_updatePlotSizes();  // Keep this at end of function
    plot_updateLfHfPlots();    // Call this after format_updatePlotSizes()
    plot_replotAll();
}

void MainWindow :: format_showLfPlot(bool open) {
    if (open) {
        ui->splitter_LfHf->setSizes({ui->splitter_LfHf->height()/2, ui->splitter_LfHf->height()/2});//customPlotLF->show();

        if (!Format->isOpenLf && !Format->isOpenHf) {
            // If LF/HF side of splitter was collapsed, then open it again
            ui->splitter_sonar->setSizes({ui->sensor_graph->minimumWidth() + ui->depthCPlot->minimumWidth(), ui->splitter_sonar->width()});
        }
    } else {
        if (flag.PlotHfSidescan)
            ui->splitter_LfHf->setSizes({0, ui->splitter_LfHf->height()});//customPlotLF->hide();
        else
            ui->splitter_sonar->setSizes({500, 0});

        // Configure zoom boxes
        if (customPlotHF->hasItem(HfPlotViewBox)) {
            customPlotHF->removeItem(HfPlotViewBox);
            HfPlotViewBox = nullptr;
        }
        if (customPlotLF->hasItem(LfPlotViewBox)) {
            customPlotLF->removeItem(LfPlotViewBox);
            LfPlotViewBox = nullptr;
        }
    }
    plot_replotAll();
    format_updatePlotSizes(); // Keep this at end of function but before ConfigureXLabels
    plot_updateLfHfPlots();

    if (state.GUI_MODE == 1 && state.PLAY_MODE == 0)    return;
    plot_replotAll(1);
}

void MainWindow :: format_showHfPlot(bool open) {
    if (open) {
        // Open the HF Plot
//        customPlotHF->replot(QCustomPlot::rpQueuedReplot);
        ui->splitter_LfHf->setSizes({ui->splitter_LfHf->height()/2, ui->splitter_LfHf->height()/2});

        if (!Format->isOpenLf && !Format->isOpenHf) {
            ui->splitter_sonar->setSizes({ui->sensor_graph->minimumWidth() + ui->depthCPlot->minimumWidth(),
                                          ui->splitter_sonar->width()});
        }
    } else{
        ui->splitter_LfHf->setSizes({ui->splitter_LfHf->height(), 0});

        if (customPlotHF->hasItem(HfPlotViewBox)) {
            customPlotHF->removeItem(HfPlotViewBox);
            HfPlotViewBox = nullptr;
        }
        if (customPlotLF->hasItem(LfPlotViewBox)) {
            customPlotLF->removeItem(LfPlotViewBox);
            LfPlotViewBox = nullptr;
        }
    }

    plot_replotAll();
    format_updatePlotSizes(); // Keep this at end of function but before ConfigureXLabels
    plot_updateLfHfPlots();

    if (state.GUI_MODE == 1 && state.PLAY_MODE == 0)    return;
    plot_replotAll(1);
}

void MainWindow :: format_showGpsPlot(bool show){
    if (!show) {
        ui->splitter_sonarGnss->setSizes({1,0});
    } else {
        if (Format->isOpen_TargetWidget || Format->isOpen_imagePlot) {
            ui->splitter_Gnss->setSizes({ui->splitter_Gnss->height()*2/3, ui->splitter_Gnss->height()/3});
        } else {
            ui->splitter_sonarGnss->setSizes({ui->splitter_sonarGnss->width()*2/3, ui->splitter_sonarGnss->width()/3});
        }
        Chart_UnstretchPlot();
        ChartWindowInstance->target_resizeAllTargets();
//        if (state.GNSS_MODE)     ui->splitter_sonarGnss->setSizes({ui->splitter_sonarGnss->width()*2/3, ui->splitter_sonarGnss->width()/3});
    }
    format_updatePlotSizes(); // Keep this at end of function
    plot_updateLfHfPlots();   // Call this after format_updatePlotSizes();
    plot_replotAll();
}

void MainWindow :: format_showSignalPlot(bool show){

    if(show==true){
        ui->portSide_graph->show();
        ui->horizontalWidget->show();
        ui->altPlot->show();
    }
    else{
        ui->portSide_graph->hide();
        ui->horizontalWidget->hide();
        ui->altPlot->hide();
    }
    Format->isSignalPlotOpen = show;
    plot_updateLfHfPlots();
}

void MainWindow :: format_showStatusBar(bool show){

    if(show == true){
        ui->statusBar->setVisible(true);
        flag.showStatusBar = true;

    }
    else{
        ui->statusBar->setVisible(false);
        flag.showStatusBar = false;
    }
}

void MainWindow :: format_showColorScale(bool show){

    if (ui->actionShowColorScale->text().compare("Show Color Scale ✓")==0 && !show){
        plot_colorScale_hide(true);
        ui->actionShowColorScale->setText("Show Color Scale ");
        ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
        ui->customPlot2->replot(QCustomPlot::rpQueuedReplot);
    }
    else{
        plot_colorScale_hide(false);
        ui->actionShowColorScale->setText("Show Color Scale ✓");
        ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
        ui->customPlot2->replot(QCustomPlot::rpQueuedReplot);
    }
}

void MainWindow :: format_showGridOnPlot(bool show){
    if(show){
        ui->depthCPlot->xAxis2->grid()->setVisible(true);
        ui->depthCPlot->yAxis->grid()->setVisible(true);
        ui->sensor_graph->xAxis2->grid()->setVisible(true);
        ui->sensor_graph->yAxis->grid()->setVisible(true);
        ui->customPlot->xAxis2->grid()->setVisible(true);
        ui->customPlot->yAxis->grid()->setVisible(true);
        ui->customPlot2->xAxis2->grid()->setVisible(true);
        ui->customPlot2->yAxis->grid()->setVisible(true);
        ui->gpsPlot->xAxis->grid()->setVisible(true);
        ui->gpsPlot->yAxis->grid()->setVisible(true);
    }
    else{
        ui->depthCPlot->xAxis2->grid()->setVisible(false);
        ui->depthCPlot->yAxis->grid()->setVisible(false);
        ui->sensor_graph->xAxis2->grid()->setVisible(false);
        ui->sensor_graph->yAxis->grid()->setVisible(false);
        ui->customPlot->xAxis2->grid()->setVisible(false);
        ui->customPlot->yAxis->grid()->setVisible(false);
        ui->customPlot2->xAxis2->grid()->setVisible(false);
        ui->customPlot2->xAxis->grid()->setVisible(false);
        ui->customPlot2->yAxis->grid()->setVisible(false);
        ui->gpsPlot->xAxis->grid()->setVisible(false);
        ui->gpsPlot->yAxis->grid()->setVisible(false);
    }
    plot_replotAll();
}

void MainWindow :: format_showZoomBox(bool show){
    if (!show) {
        Format->isZoomBoxAlwaysOn = false;
        zoomRatioTimer.start(1000);
    } else {
        Format->isZoomBoxAlwaysOn = true;

        plot_showUpdatedZoomLegends();
        plot_zoomBox_updatePosition(true);
        plot_replotAll();
    }
}

void MainWindow :: format_showCompass(bool show) {
    Format->isDisplayCompass = show;
    ChartWindowInstance->compassIcon->setVisible(show);
    plot_icons_updateCompassIcon();
    plot_replotAll();
}

void MainWindow :: format_showCrosshair(bool show) {
    Format->isCrosshairShown = show;
    bool paused = ((state.PLAY_MODE==4 || state.PLAY_MODE == 5 || state.PLAY_MODE == 6) && state.GUI_MODE == 2)
            || (state.RT_MODE < 2 && state.GUI_MODE == 1) || (state.GUI_MODE == 0 && state.SIM_MODE == 0);
    if (crosshair_hf.isValid() && crosshair_lf.isValid() &&
            paused && !show) {
        crosshair_hf.setVisible(show);
        crosshair_lf.setVisible(show);
        plot_replotAll();
    }
}

void MainWindow :: format_showPanel(bool show) {
    tools->updateIconPanel(!Format->isOpenLf, flag.targetToolActive, flag.measuringTapeActive, flag.shadowToolActive, cropTool->toolActive);
    plot_replotAll();
}

void MainWindow :: format_showGainSlider(bool show){
    if (show) {
        ui->actionShow_gain->setText("Show Gain Slider ✓");
        ui->gain_btn->setChecked(true);
        emit showFormatGainSlider(true);
        ui->gainSlider->show();
        ui->mainGain_btn->show();
        ui->mainGain_txt->show();
        ui->hfGainSlider->show();
        ui->hfGain_btn->show();
        ui->hfGain_txt->show();
        ui->slider_gainDepth->show();
        ui->label_gainDepth->show();
        ui->label_gainDepthTxt->show();
        gainSliderTimer.start(60000);
    }
    else {
        ui->actionShow_gain->setText("Show Gain Slider");
        ui->gainSlider->hide();
        ui->gain_btn->setChecked(false);
        emit showFormatGainSlider(false);
        ui->mainGain_btn->hide();
        ui->mainGain_txt->hide();
        ui->hfGainSlider->hide();
        ui->hfGain_btn->hide();
        ui->hfGain_txt->hide();
        ui->slider_gainDepth->hide();
        ui->label_gainDepth->hide();
        ui->label_gainDepthTxt->hide();
        gainSliderTimer.stop();
    }
}

void MainWindow :: format_showBalance(bool show){
    if (show) {
//        ui->actionShow_balance->setText("Show Balance Slider ✓");
//        ui->actionShowBalanceSlider->setText("Show Balance Slider ✓");
//        ui->digitalGainBalance->show();
//        ui->digitalGainLabel->show();
//        ui->digitalGainTxt->show();
    }
    else {
        ui->actionShow_balance->setText("Show Balance Slider");
        ui->actionShowBalanceSlider->setText("Show Balance Slider");
        ui->digitalGainBalance->hide();
        ui->digitalGainLabel->hide();
        ui->digitalGainTxt->hide();
    }
}

void MainWindow :: format_showGap(bool show){
    // If GUI is not in Angle scan mode, then do not do anything to show/hide gap
    if (!flag.PlotAngleScan)    return;
    flag.showGap = show;
    plot_data_showGap2(show);
}

void MainWindow :: format_splitterHandleChanged () {
    format_updatePlotSizes();

    if (Format->widthGnss == 0) {
    } else {
        resizeEvent(nullptr);
        ChartWindowInstance->target_resizeAllTargets();
    }

    if (Format->widthSonar != 0 && Format->widthDepth == 0 && Format->widthSensor == 0) {
        ui->splitter_depth->setSizes({0, ui->sensor_graph->minimumWidth()});
    }
    if (Format->widthLfHf != 0) {
        if (abs(Format->heightLf - Format->heightHf) < 50) {
            ui->splitter_LfHf->setSizes({ui->splitter_LfHf->height()/2, ui->splitter_LfHf->height()/2});
        }
        format_updatePlotSizes();
        plot_replotAll();
    }
    if (depthPlot->width()>=210) {
        depthPlot->xAxis2->ticker()->setTickCount(5);
    } else {
        depthPlot->xAxis2->ticker()->setTickCount(2);
    }
    plot_updateLfHfPlots();
    plot_replotAll(1);
}

/****************sliders****************/

/**
 * @brief MainWindow::on_att_slider_valueChanged
 * @param value
 * To assign the value for attenuation according to the slider position
 * and sends to the board side.
 */
void MainWindow :: on_att_slider_valueChanged(int value)
{

    if (flag.readyToTransmit == 1) {
        Tcp_sendCommand(9, value, 0);
    }
}

void MainWindow :: gainSlider_setValueLf(int value) {
    // Change slider programmatically from user menu
    if (state.GUI_MODE == 2)    plot_data_changeGainOnLf((value*0.1) - gain->C_lfOffset,
                                                         (value*0.1) - gain->C_lfOffset);
    if (round(gain->tvg_CLfP*10) == round(10*gain->tvg_CLfS))
        ui->mainGain_btn->setText("  "+QString::number(value*0.1/* + gain->tvg_CLfP*/)+" dB");
    else
        ui->mainGain_btn->setText("  "+QString::number(value*0.1)+" dB");
    ui->gainSlider->setValue(value);
    gain->C_lfOffset = value*0.1;
    gain->gainSliderIndexLf = value;
}

void MainWindow :: gainSlider_setValueHf(int value) {
    // Change slider programmatically from user menu
    if (state.GUI_MODE == 2)    plot_data_changeGainOnHf((value*0.1) - gain->C_hfOffset,
                                                         (value*0.1) - gain->C_hfOffset);
    if (round(gain->tvg_CHfP*10) == round(10*gain->tvg_CHfS))
        ui->hfGain_btn->setText("  "+QString::number(value*0.1/* + gain->tvg_CHfP*/)+" dB");
    else
        ui->hfGain_btn->setText("  "+QString::number(value*0.1)+" dB");
    ui->hfGainSlider->setValue(value);
    gain->C_hfOffset = value*0.1;
    gain->gainSliderIndexHf = value;
}

void MainWindow :: gainSlider_setValueDepth(int value) {
    // Change slider programmatically from user menu
    if (state.GUI_MODE == 2)    plot_data_changeGainOnDepth((value*0.1) - gain->C_depthOffset);
    ui->slider_gainDepth->setValue(value);
    gain->C_depthOffset = value*0.1;
    ui->label_gainDepth->setText(QString::number(gain->C_depthOffset, 'f', 1) + " dB  ");
    gain->gainSliderIndexDepth = value;
}

/**
 * @brief MainWindow::on_gainSlider_valueChanged
 * @param value
 * Sets mainGain to value of the slider plus 50
 */
void MainWindow :: gainSlider_userMovedSliderLf(int value) {
    // Slider was changed through user interaction
    gainSliderTimer.start(60000);
    gainSlider_setValueLf(value);
    emit GainsToUsermenu(value, gain->DigitalGainIndex, ui->hfGainSlider->value());
}

void MainWindow :: gainSlider_userMovedSliderHf(int value) {
    gainSliderTimer.start(60000);
    gainSlider_setValueHf(value);
    emit GainsToUsermenu(ui->gainSlider->value(), gain->DigitalGainIndex, value);
}

void MainWindow :: gainSlider_userMovedSliderDepth(int value) {
    gainSliderTimer.start(60000);
    gainSlider_setValueDepth(value);
    emit GainsToUsermenu(ui->gainSlider->value(), gain->DigitalGainIndex, ui->hfGainSlider->value());
}

void MainWindow :: on_mainGain_btn_clicked() {
    double newGainOffset = ui->gainSlider->value()*0.1;
    gain->tvg_CLfP += newGainOffset;
    gain->tvg_CLfS += newGainOffset;
    gain->C_lfOffset = 0;
    emit GainsToUsermenu(0, gain->DigitalGainIndex, ui->hfGainSlider->value());
    ui->gainSlider->setValue(0);
}

void MainWindow :: on_hfGain_btn_clicked() {
    double newGainOffset = ui->hfGainSlider->value()*0.1;
    gain->tvg_CHfP += newGainOffset;
    gain->tvg_CHfS += newGainOffset;
    gain->C_hfOffset = 0;
    emit GainsToUsermenu(ui->gainSlider->value(), gain->DigitalGainIndex, 0);
    ui->hfGainSlider->setValue(0);
}

/****************XTF Files****************/

void MainWindow :: xtf_recordPingHeader() {
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.MagicNumber, sizeof(pingHeaderObject.MagicNumber));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.HeaderType, sizeof(pingHeaderObject.HeaderType));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SubChannelNumber, sizeof(pingHeaderObject.SubChannelNumber));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.NumChansToFollow, sizeof(pingHeaderObject.NumChansToFollow));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Reserved1[0], sizeof(pingHeaderObject.Reserved1[0]));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Reserved1[1], sizeof(pingHeaderObject.Reserved1[1]));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.NumBytesThisRecord, sizeof(pingHeaderObject.NumBytesThisRecord));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Year, sizeof(pingHeaderObject.Year));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Month, sizeof(pingHeaderObject.Month));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Day, sizeof(pingHeaderObject.Day));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Hour, sizeof(pingHeaderObject.Hour));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Minute, sizeof(pingHeaderObject.Minute));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Second, sizeof(pingHeaderObject.Second));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.HSeconds, sizeof(pingHeaderObject.HSeconds));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.JulianDay, sizeof(pingHeaderObject.JulianDay));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.EventNumber, sizeof(pingHeaderObject.EventNumber));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.PingNumber, sizeof(pingHeaderObject.PingNumber));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SoundVelocity, sizeof(pingHeaderObject.SoundVelocity));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.OceanTide, sizeof(pingHeaderObject.OceanTide));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Reserved2, sizeof(pingHeaderObject.Reserved2));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ConductivityFreq, sizeof(pingHeaderObject.ConductivityFreq));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.TemperatureFreq, sizeof(pingHeaderObject.TemperatureFreq));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.PressureFreq, sizeof(pingHeaderObject.PressureFreq));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.PressureTemp, sizeof(pingHeaderObject.PressureTemp));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Conductivity, sizeof(pingHeaderObject.Conductivity));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.WaterTemperature, sizeof(pingHeaderObject.WaterTemperature));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Pressure, sizeof(pingHeaderObject.Pressure));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ComputedSoundVelocity, sizeof(pingHeaderObject.ComputedSoundVelocity));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.MagX, sizeof(pingHeaderObject.MagX));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.MagY, sizeof(pingHeaderObject.MagY));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.MagZ, sizeof(pingHeaderObject.MagZ));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AuxVal1, sizeof(pingHeaderObject.AuxVal1));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AuxVal2, sizeof(pingHeaderObject.AuxVal2));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AuxVal3, sizeof(pingHeaderObject.AuxVal3));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AuxVal4, sizeof(pingHeaderObject.AuxVal4));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AuxVal5, sizeof(pingHeaderObject.AuxVal5));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AuxVal6, sizeof(pingHeaderObject.AuxVal6));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SpeedLog, sizeof(pingHeaderObject.SpeedLog));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Turbidity, sizeof(pingHeaderObject.Turbidity));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ShipSpeed, sizeof(pingHeaderObject.ShipSpeed));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ShipGyro, sizeof(pingHeaderObject.ShipGyro));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ShipYcoordinate, sizeof(pingHeaderObject.ShipYcoordinate));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ShipXcoordinate, sizeof(pingHeaderObject.ShipXcoordinate));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ShipAltitude, sizeof(pingHeaderObject.ShipAltitude));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ShipDepth, sizeof(pingHeaderObject.ShipDepth));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FixTimeHour, sizeof(pingHeaderObject.FixTimeHour));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FixTimeMinute, sizeof(pingHeaderObject.FixTimeMinute));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FixTimeSecond, sizeof(pingHeaderObject.FixTimeSecond));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FixTimeHsecond, sizeof(pingHeaderObject.FixTimeHsecond));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorSpeed, sizeof(pingHeaderObject.SensorSpeed));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.KP, sizeof(pingHeaderObject.KP));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorYcoordinate, sizeof(pingHeaderObject.SensorYcoordinate));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorXcoordinate, sizeof(pingHeaderObject.SensorXcoordinate));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SonarStatus, sizeof(pingHeaderObject.SonarStatus));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.RangeToFish, sizeof(pingHeaderObject.RangeToFish));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.BearingToFish, sizeof(pingHeaderObject.BearingToFish));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.CableOut, sizeof(pingHeaderObject.CableOut));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Layback, sizeof(pingHeaderObject.Layback));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.CableTension, sizeof(pingHeaderObject.CableTension));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorDepth, sizeof(pingHeaderObject.SensorDepth));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorPrimaryAltitude, sizeof(pingHeaderObject.SensorPrimaryAltitude));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorAuxAltitude, sizeof(pingHeaderObject.SensorAuxAltitude));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorPitch, sizeof(pingHeaderObject.SensorPitch));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorRoll, sizeof(pingHeaderObject.SensorRoll));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.SensorHeading, sizeof(pingHeaderObject.SensorHeading));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Heave, sizeof(pingHeaderObject.Heave));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.Yaw, sizeof(pingHeaderObject.Yaw));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.AttitudeTimeTag, sizeof(pingHeaderObject.AttitudeTimeTag));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.DOT, sizeof(pingHeaderObject.DOT));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.NavFixMilliseconds, sizeof(pingHeaderObject.NavFixMilliseconds));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ComputerClockHour, sizeof(pingHeaderObject.ComputerClockHour));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ComputerClockMinute, sizeof(pingHeaderObject.ComputerClockMinute));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ComputerClockSecond, sizeof(pingHeaderObject.ComputerClockSecond));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ComputerClockHsec, sizeof(pingHeaderObject.ComputerClockHsec));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FishPositionDeltaX, sizeof(pingHeaderObject.FishPositionDeltaX));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FishPositionDeltaY, sizeof(pingHeaderObject.FishPositionDeltaY));
    recorder.XtfBuffer.append((const char *)&pingHeaderObject.FishPositionErrorCode, sizeof(pingHeaderObject.FishPositionErrorCode));

    recorder.XtfBuffer.append((const char *)&pingHeaderObject.ReservedSpace2, sizeof(pingHeaderObject.ReservedSpace2));

}

void MainWindow :: xtf_recordPingChHeader() {
    //channel ping channel header
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ChannelNumber,       2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.DownsampleMethod,    2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.SlantRange,          4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.GroundRange,         4);

    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.TimeDelay,           4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.TimeDuration,        4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.SecondsPerPing,      4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ProcessingFlags,     2);

    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.Frequency,           2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.InitialGainCode,     2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.GainCode,            2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.BandWidth,           2);

    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ContactNumber,       4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ContactClassification,2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ContactSubNumber,    1);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ContactType,         1);

    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.NumSamples,          4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.MillivoltScale,      2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ContactTimeOffTrack, 4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ContactCloseNumber,  1);

    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.Reserved2,           1);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.FixedVSOP,           4);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.Weight,              2);
    recorder.XtfBuffer.append((const char *)&pingChHeaderObject.ReservedSpace,       4);

}

void MainWindow :: xtf_recordFileHeader() {

    recorder.xtfFileData.append((const char *)&fileHeaderObject.FileFormat, sizeof(fileHeaderObject.FileFormat));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.SystemType, sizeof(fileHeaderObject.SystemType));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.RecordingProgramName, sizeof(fileHeaderObject.RecordingProgramName));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.RecordingProgramVersion, sizeof(fileHeaderObject.RecordingProgramVersion));

    recorder.xtfFileData.append((const char *)&fileHeaderObject.SonarName, sizeof(fileHeaderObject.SonarName));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.SonarType, sizeof(fileHeaderObject.SonarType));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NoteString, sizeof(fileHeaderObject.NoteString));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.ThisFileName, sizeof(fileHeaderObject.ThisFileName));

    recorder.xtfFileData.append((const char *)&fileHeaderObject.NavUnits, sizeof(fileHeaderObject.NavUnits));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NumberOfSonarChannels, sizeof(fileHeaderObject.NumberOfSonarChannels));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NumberOfBathymetryChannels, sizeof(fileHeaderObject.NumberOfBathymetryChannels));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NumberOfSnippetChannels, sizeof(fileHeaderObject.NumberOfSnippetChannels));

    recorder.xtfFileData.append((const char *)&fileHeaderObject.NumberOfForwardLookArrays, sizeof(fileHeaderObject.NumberOfForwardLookArrays));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NumberOfEchoStrengthChannels, sizeof(fileHeaderObject.NumberOfEchoStrengthChannels));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NumberOfInterferometryChannels, sizeof(fileHeaderObject.NumberOfInterferometryChannels));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.Reserved1, sizeof(fileHeaderObject.Reserved1));

    recorder.xtfFileData.append((const char *)&fileHeaderObject.Reserved2, sizeof(fileHeaderObject.Reserved2));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.ReferencePointHeight, sizeof(fileHeaderObject.ReferencePointHeight));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.ProjectionType,sizeof(fileHeaderObject.ProjectionType));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.SpheriodType, sizeof(fileHeaderObject.SpheriodType));

    recorder.xtfFileData.append((const char *)&fileHeaderObject.NavigationLatency, 4);
    recorder.xtfFileData.append((const char *)&fileHeaderObject.OriginY, sizeof(fileHeaderObject.OriginY));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.OriginX, sizeof(fileHeaderObject.OriginX));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NavOffsetY, sizeof(fileHeaderObject.NavOffsetY));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NavOffsetX, sizeof(fileHeaderObject.NavOffsetX));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.NavOffsetZ, sizeof(fileHeaderObject.NavOffsetZ));

    recorder.xtfFileData.append((const char *)&fileHeaderObject.NavOffsetYaw, sizeof(fileHeaderObject.NavOffsetYaw));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.MRUOffsetY, sizeof(fileHeaderObject.MRUOffsetY));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.MRUOffsetX, sizeof(fileHeaderObject.MRUOffsetX));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.MRUOffsetZ, sizeof(fileHeaderObject.MRUOffsetZ));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.MRUOffsetYaw, sizeof(fileHeaderObject.MRUOffsetYaw));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.MRUOffsetPitch, sizeof(fileHeaderObject.MRUOffsetPitch));
    recorder.xtfFileData.append((const char *)&fileHeaderObject.MRUOffsetRoll, sizeof(fileHeaderObject.MRUOffsetRoll));
}

void MainWindow :: xtf_recordChHeader() {

    recorder.xtfFileData.append((const char *)&chanInfoObject.TypeOfChannel, sizeof(chanInfoObject.TypeOfChannel));
    recorder.xtfFileData.append((const char *)&chanInfoObject.SubChannelNumber, sizeof(chanInfoObject.SubChannelNumber));
    recorder.xtfFileData.append((const char *)&chanInfoObject.CorrectionFlags, sizeof(chanInfoObject.CorrectionFlags));
    recorder.xtfFileData.append((const char *)&chanInfoObject.UniPolar, sizeof(chanInfoObject.UniPolar));

    recorder.xtfFileData.append((const char *)&chanInfoObject.BytesPerSample, sizeof(chanInfoObject.BytesPerSample));
    recorder.xtfFileData.append((const char *)&chanInfoObject.Reserved, 4);
    recorder.xtfFileData.append((const char *)&chanInfoObject.ChannelName, sizeof(chanInfoObject.ChannelName));
    recorder.xtfFileData.append((const char *)&chanInfoObject.VoltScale, sizeof(chanInfoObject.VoltScale));

    recorder.xtfFileData.append((const char *)&chanInfoObject.Frequency, sizeof(chanInfoObject.Frequency));
    recorder.xtfFileData.append((const char *)&chanInfoObject.HorizBeamAngle, sizeof(chanInfoObject.HorizBeamAngle));
    recorder.xtfFileData.append((const char *)&chanInfoObject.TiltAngle, sizeof(chanInfoObject.TiltAngle));
    recorder.xtfFileData.append((const char *)&chanInfoObject.BeamWidth, sizeof(chanInfoObject.BeamWidth));

    recorder.xtfFileData.append((const char *)&chanInfoObject.OffsetX, sizeof(chanInfoObject.OffsetX));
    recorder.xtfFileData.append((const char *)&chanInfoObject.OffsetY, sizeof(chanInfoObject.OffsetY));
    recorder.xtfFileData.append((const char *)&chanInfoObject.OffsetZ, sizeof(chanInfoObject.OffsetZ));
    recorder.xtfFileData.append((const char *)&chanInfoObject.OffsetYaw, sizeof(chanInfoObject.OffsetYaw));

    recorder.xtfFileData.append((const char *)&chanInfoObject.OffsetPitch, sizeof(chanInfoObject.OffsetPitch));
    recorder.xtfFileData.append((const char *)&chanInfoObject.OffsetRoll, sizeof(chanInfoObject.OffsetRoll));
    recorder.xtfFileData.append((const char *)&chanInfoObject.BeamsPerArray, sizeof(chanInfoObject.BeamsPerArray));
    recorder.xtfFileData.append((const char *)&chanInfoObject.SampleFormat, sizeof(chanInfoObject.SampleFormat));

    recorder.xtfFileData.append((const char *)&chanInfoObject.ReservedArea2, sizeof(chanInfoObject.ReservedArea2));
}

void MainWindow :: xtf_writePing() {
    // Write Ping Header
    pingHeaderObject.MagicNumber = 0xFACE;
    pingHeaderObject.HeaderType = 0;
    pingHeaderObject.NumChansToFollow = 0;
    if (ping->samplesToPlotLF) {
        pingHeaderObject.NumChansToFollow += 2;
    }
    if (ping->samplesToPlotHF) {
        pingHeaderObject.NumChansToFollow += 2;
    }

    pingHeaderObject.NumBytesThisRecord = ping->samplesToPlotLF*4 + ping->samplesToPlotHF*4
            + 256 + pingHeaderObject.NumChansToFollow*64;

    if (pingHeaderObject.NumBytesThisRecord > 300) {
        currentDate = QDate::currentDate();
        pingHeaderObject.Year = currentRtcTime.year;
        pingHeaderObject.Month = currentRtcTime.month;
        pingHeaderObject.Day = currentRtcTime.day;

        currentTime = QTime::currentTime();
        pingHeaderObject.Hour = currentRtcTime.hour;
        pingHeaderObject.Minute = currentRtcTime.minute;
        pingHeaderObject.Second = currentRtcTime.second;
        pingHeaderObject.HSeconds = currentRtcTime.millisecond/10;

        pingHeaderObject.PingNumber = ping->pingNumCurrent;
        pingHeaderObject.SoundVelocity = 1500;
        pingHeaderObject.ShipSpeed = setting.boatSpeed;
        pingHeaderObject.SensorRoll = ping->lastTraceRoll;
        pingHeaderObject.SensorPitch = ping->lastTracePitch;
        pingHeaderObject.SensorHeading = ping->heading;

        // Following 3 fields are required for HYPACK to view XTF files
        if (gnssController->validCoords) {
            pingHeaderObject.ShipXcoordinate = gnssController->Longitude();
            pingHeaderObject.ShipYcoordinate = gnssController->Latitude();
            pingHeaderObject.SensorXcoordinate = gnssController->Longitude();
            pingHeaderObject.SensorYcoordinate = gnssController->Latitude();
        } else {
            pingHeaderObject.ShipXcoordinate = 63.5 + ping->pingNumCurrent*0.0000001;
            pingHeaderObject.ShipYcoordinate = 43.7;
            pingHeaderObject.SensorXcoordinate = 63.5 + ping->pingNumCurrent*0.0000001;
            pingHeaderObject.SensorYcoordinate = 43.7;
        }
        pingHeaderObject.ShipGyro = 0;

        // Append Ping Header
        xtf_recordPingHeader();
        //                recordedXtfDataBuffer.append((const char *)&pingHeaderObject,sizeof(pingHeaderObject));

        if (ping->samplesToPlotLF) {
            // Write LF Port Side Channel Header
            pingChHeaderObject.ChannelNumber = 0;
            pingChHeaderObject.SlantRange = (float) ping->rangeLf_double;
            pingChHeaderObject.NumSamples = (DWORD) ping->samplesToPlotLF;
            pingChHeaderObject.Frequency = ping->lowFrequency;
            //                recordedXtfDataBuffer.append((const char *)&pingChHeaderObject,sizeof(pingChHeaderObject));

            // Append LF Port Channel Ping Header
            xtf_recordPingChHeader();

            // Append data, but first reverse data bytes
            QByteArray b;
            quint16 x, y;
            for(int i = 0; i < ping->samplesToPlotLF; i++) {
                x = ping->data_portLf.at(i);
                y = ping->data_stbdLf.at(i);
                b.append(x);
                b.resize(2);
                portSide1_bytes.prepend(b);
                b.clear();
                b.append(y);
                b.resize(2);
                starboardSide1_bytes.append(b);
                b.clear();
            }
            recorder.XtfBuffer.append(portSide1_bytes);

            // Write LF Starboard Side Channel Header
            pingChHeaderObject.ChannelNumber = 1; // starBoard low freq
            pingChHeaderObject.SlantRange = (float) ping->rangeLf_double;
            pingChHeaderObject.NumSamples = (DWORD) ping->samplesToPlotLF;
            pingChHeaderObject.Frequency = ping->lowFrequency;

            // Append LF Starboard Channel Ping Header
            xtf_recordPingChHeader();
            recorder.XtfBuffer.append(starboardSide1_bytes);
        }

        if (ping->samplesToPlotHF) {
            // Write HF Port Side Channel Header
            pingChHeaderObject.ChannelNumber = 2; // port high freq
            pingChHeaderObject.SlantRange = (float) ping->rangeHf_double;
            pingChHeaderObject.NumSamples = (DWORD) ping->samplesToPlotHF;
            pingChHeaderObject.Frequency = ping->highFrequency;

            // Append HF Port Channel Ping Header
            xtf_recordPingChHeader();

            QByteArray b;
            quint16 x, y;
            for(int i = 0; i < ping->samplesToPlotHF; i++) {
                x = ping->data_portLf.at(i);
                y = ping->data_stbdLf.at(i);
                b.append(x);
                b.resize(2);
                portSide2_bytes.prepend(b);
                b.clear();
                b.append(y);
                b.resize(2);
                starboardSide2_bytes.append(b);
                b.clear();
            }
            recorder.XtfBuffer.append(portSide2_bytes);

            // Write HF Starboard Side Channel Header
            pingChHeaderObject.ChannelNumber = 3; // starboard high freq
            pingChHeaderObject.SlantRange = (float) ping->rangeHf_double;
            pingChHeaderObject.NumSamples = (DWORD) ping->samplesToPlotHF;
            pingChHeaderObject.Frequency = ping->highFrequency;

            // Append HF Starboard Channel Ping Header
            xtf_recordPingChHeader();
            recorder.XtfBuffer.append(starboardSide2_bytes);
        }

        pingCounter++;
        recorder_saveData(recorder.XtfBuffer);
        recorder.XtfBuffer.clear();
    }
    //clearing the buffers
    portSide1_bytes.clear();
    starboardSide1_bytes.clear();
    portSide2_bytes.clear();
    starboardSide2_bytes.clear();
}

void MainWindow :: xtf_writeFileHeader() {
    flag.recordXtfFormat = 1;
    fileHeaderObject.FileFormat = 0x7B;
    fileHeaderObject.SystemType = 1;
    fileHeaderObject.SonarType = 0;
    char *baseName = QFileInfo(recorder.recPath).baseName().toLatin1().data();
    memcpy(&fileHeaderObject.ThisFileName, &baseName, 64);

    fileHeaderObject.NavUnits = 3;
    fileHeaderObject.SystemType = 1;
    if (flag.PlotHfSidescan)    fileHeaderObject.NumberOfSonarChannels = 4;
    else                        fileHeaderObject.NumberOfSonarChannels = 2;
    fileHeaderObject.NumberOfBathymetryChannels = 0;

    fileHeaderObject.NumberOfSnippetChannels = 0;
    fileHeaderObject.NumberOfForwardLookArrays = 0;
    fileHeaderObject.NumberOfEchoStrengthChannels = 0;
    fileHeaderObject.NumberOfInterferometryChannels = 0;
    xtf_recordFileHeader();

    chanInfoObject.TypeOfChannel = 1; // port
    chanInfoObject.BytesPerSample = 2; // 16bit
    chanInfoObject.Frequency = ping->lowFrequency;
    xtf_recordChHeader();

    chanInfoObject.TypeOfChannel = 2; // starboard
    chanInfoObject.BytesPerSample = 2; // 16bit
    xtf_recordChHeader();

    if (flag.useDualFrequency == 1) {
        chanInfoObject.TypeOfChannel = 1; // port
        chanInfoObject.BytesPerSample = 2; // 16bit
        chanInfoObject.Frequency = ping->highFrequency;
    } else {
        chanInfoObject.TypeOfChannel = 0; // port
        chanInfoObject.BytesPerSample = 0; // 16bit
        chanInfoObject.Frequency = 0;
    }
    xtf_recordChHeader();

    if (flag.useDualFrequency == 1) {
        chanInfoObject.TypeOfChannel = 2; // starboard
        chanInfoObject.BytesPerSample = 2; // 16bit
    }
    xtf_recordChHeader();

    chanInfoObject.TypeOfChannel = 0;
    chanInfoObject.BytesPerSample = 0;
    chanInfoObject.Frequency = 0;
    xtf_recordChHeader();
    xtf_recordChHeader();

    recorder.sonarFile.write(recorder.xtfFileData);
}

int MainWindow :: xtf_readFile(char *data_pointer, int dataType, int *BytesPointer) {
    // dataType = 1 for XTFFILEHEADER, = 2 for XTFPINGHEADER, 3 for XTFPINGCHANHEADER, 4 for CHANINFO
    if (dataType == 1) {
        data_pointer += data_copy(&fileHeaderObject.FileFormat, data_pointer, sizeof(fileHeaderObject.FileFormat), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.SystemType, data_pointer, sizeof(fileHeaderObject.SystemType), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.RecordingProgramName, data_pointer, sizeof(fileHeaderObject.RecordingProgramName), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.RecordingProgramVersion, data_pointer, sizeof(fileHeaderObject.RecordingProgramVersion), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.SonarName, data_pointer, sizeof(fileHeaderObject.SonarName), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.SonarType, data_pointer, sizeof(fileHeaderObject.SonarType), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NoteString, data_pointer, sizeof(fileHeaderObject.NoteString), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.ThisFileName, data_pointer, sizeof(fileHeaderObject.ThisFileName), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.NavUnits, data_pointer, sizeof(fileHeaderObject.NavUnits), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NumberOfSonarChannels, data_pointer, sizeof(fileHeaderObject.NumberOfSonarChannels), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NumberOfBathymetryChannels, data_pointer, sizeof(fileHeaderObject.NumberOfBathymetryChannels), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NumberOfSnippetChannels, data_pointer, sizeof(fileHeaderObject.NumberOfSnippetChannels), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.NumberOfForwardLookArrays, data_pointer, sizeof(fileHeaderObject.NumberOfForwardLookArrays), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NumberOfEchoStrengthChannels, data_pointer, sizeof(fileHeaderObject.NumberOfEchoStrengthChannels), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NumberOfInterferometryChannels, data_pointer, sizeof(fileHeaderObject.NumberOfInterferometryChannels), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.Reserved1, data_pointer, sizeof(fileHeaderObject.Reserved1), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.Reserved2, data_pointer, sizeof(fileHeaderObject.Reserved2), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.ReferencePointHeight, data_pointer, sizeof(fileHeaderObject.ReferencePointHeight), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.ProjectionType, data_pointer, sizeof(fileHeaderObject.ProjectionType), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.SpheriodType, data_pointer, sizeof(fileHeaderObject.SpheriodType), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.NavigationLatency, data_pointer, sizeof(fileHeaderObject.NavigationLatency), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.OriginY, data_pointer, sizeof(fileHeaderObject.OriginY), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.OriginX, data_pointer, sizeof(fileHeaderObject.OriginX), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NavOffsetY, data_pointer, sizeof(fileHeaderObject.NavOffsetY), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.NavOffsetX, data_pointer, sizeof(fileHeaderObject.NavOffsetX), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NavOffsetZ, data_pointer, sizeof(fileHeaderObject.NavOffsetZ), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.NavOffsetYaw, data_pointer, sizeof(fileHeaderObject.NavOffsetYaw), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.MRUOffsetY, data_pointer, sizeof(fileHeaderObject.MRUOffsetY), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.MRUOffsetX, data_pointer, sizeof(fileHeaderObject.MRUOffsetX), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.MRUOffsetZ, data_pointer, sizeof(fileHeaderObject.MRUOffsetZ), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.MRUOffsetYaw, data_pointer, sizeof(fileHeaderObject.MRUOffsetYaw), BytesPointer);
        data_pointer += data_copy(&fileHeaderObject.MRUOffsetPitch, data_pointer, sizeof(fileHeaderObject.MRUOffsetPitch), BytesPointer);

        data_pointer += data_copy(&fileHeaderObject.MRUOffsetRoll, data_pointer, sizeof(fileHeaderObject.MRUOffsetRoll), BytesPointer);
        return 256;
    }
    else if (dataType == 2) {
        data_pointer += data_copy(&pingHeaderObject.MagicNumber, data_pointer, sizeof(pingHeaderObject.MagicNumber), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.HeaderType, data_pointer, sizeof(pingHeaderObject.HeaderType), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SubChannelNumber, data_pointer, sizeof(pingHeaderObject.SubChannelNumber), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.NumChansToFollow, data_pointer, sizeof(pingHeaderObject.NumChansToFollow), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.Reserved1, data_pointer, 4, BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.NumBytesThisRecord, data_pointer, sizeof(pingHeaderObject.NumBytesThisRecord), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Year, data_pointer, sizeof(pingHeaderObject.Year), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Month, data_pointer, sizeof(pingHeaderObject.Month), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.Day, data_pointer, sizeof(pingHeaderObject.Day), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Hour, data_pointer, sizeof(pingHeaderObject.Hour), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Minute, data_pointer, sizeof(pingHeaderObject.Minute), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Second, data_pointer, sizeof(pingHeaderObject.Second), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.HSeconds, data_pointer, sizeof(pingHeaderObject.HSeconds), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.JulianDay, data_pointer, sizeof(pingHeaderObject.JulianDay), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.EventNumber, data_pointer, sizeof(pingHeaderObject.EventNumber), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.PingNumber, data_pointer, sizeof(pingHeaderObject.PingNumber), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.SoundVelocity, data_pointer, sizeof(pingHeaderObject.SoundVelocity), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.OceanTide, data_pointer, sizeof(pingHeaderObject.OceanTide), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Reserved2, data_pointer, sizeof(pingHeaderObject.Reserved2), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ConductivityFreq, data_pointer, sizeof(pingHeaderObject.ConductivityFreq), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.TemperatureFreq, data_pointer, sizeof(pingHeaderObject.TemperatureFreq), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.PressureFreq, data_pointer, sizeof(pingHeaderObject.PressureFreq), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.PressureTemp, data_pointer, sizeof(pingHeaderObject.PressureTemp), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Conductivity, data_pointer, sizeof(pingHeaderObject.Conductivity), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.WaterTemperature, data_pointer, sizeof(pingHeaderObject.WaterTemperature), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Pressure, data_pointer, sizeof(pingHeaderObject.Pressure), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ComputedSoundVelocity, data_pointer, sizeof(pingHeaderObject.ComputedSoundVelocity), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.MagX, data_pointer, sizeof(pingHeaderObject.MagX), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.MagY, data_pointer, sizeof(pingHeaderObject.MagY), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.MagZ, data_pointer, sizeof(pingHeaderObject.MagZ), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.AuxVal1, data_pointer, sizeof(pingHeaderObject.AuxVal1), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.AuxVal2, data_pointer, sizeof(pingHeaderObject.AuxVal2), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.AuxVal3, data_pointer, sizeof(pingHeaderObject.AuxVal3), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.AuxVal4, data_pointer, sizeof(pingHeaderObject.AuxVal4), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.AuxVal5, data_pointer, sizeof(pingHeaderObject.AuxVal5), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.AuxVal6, data_pointer, sizeof(pingHeaderObject.AuxVal6), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.SpeedLog, data_pointer, sizeof(pingHeaderObject.SpeedLog), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Turbidity, data_pointer, sizeof(pingHeaderObject.Turbidity), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ShipSpeed, data_pointer, sizeof(pingHeaderObject.ShipSpeed), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ShipGyro, data_pointer, sizeof(pingHeaderObject.ShipGyro), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.ShipYcoordinate, data_pointer, sizeof(pingHeaderObject.ShipYcoordinate), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ShipXcoordinate, data_pointer, sizeof(pingHeaderObject.ShipXcoordinate), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ShipAltitude, data_pointer, sizeof(pingHeaderObject.ShipAltitude), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ShipDepth, data_pointer, sizeof(pingHeaderObject.ShipDepth), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.FixTimeHour, data_pointer, sizeof(pingHeaderObject.FixTimeHour), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.FixTimeMinute, data_pointer, sizeof(pingHeaderObject.FixTimeMinute), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.FixTimeSecond, data_pointer, sizeof(pingHeaderObject.FixTimeSecond), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.FixTimeHsecond, data_pointer, sizeof(pingHeaderObject.FixTimeHsecond), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.SensorSpeed, data_pointer, sizeof(pingHeaderObject.SensorSpeed), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.KP, data_pointer, sizeof(pingHeaderObject.KP), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorYcoordinate, data_pointer, sizeof(pingHeaderObject.SensorYcoordinate), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorXcoordinate, data_pointer, sizeof(pingHeaderObject.SensorXcoordinate), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.SonarStatus, data_pointer, sizeof(pingHeaderObject.SonarStatus), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.RangeToFish, data_pointer, sizeof(pingHeaderObject.RangeToFish), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.BearingToFish, data_pointer, sizeof(pingHeaderObject.BearingToFish), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.CableOut, data_pointer, sizeof(pingHeaderObject.CableOut), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.Layback, data_pointer, sizeof(pingHeaderObject.Layback), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.CableTension, data_pointer, sizeof(pingHeaderObject.CableTension), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorDepth, data_pointer, sizeof(pingHeaderObject.SensorDepth), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorPrimaryAltitude, data_pointer, sizeof(pingHeaderObject.SensorPrimaryAltitude), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.SensorAuxAltitude, data_pointer, sizeof(pingHeaderObject.SensorAuxAltitude), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorPitch, data_pointer, sizeof(pingHeaderObject.SensorPitch), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorRoll, data_pointer, sizeof(pingHeaderObject.SensorRoll), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.SensorHeading, data_pointer, sizeof(pingHeaderObject.SensorHeading), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.Heave, data_pointer, sizeof(pingHeaderObject.Heave), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.Yaw, data_pointer, sizeof(pingHeaderObject.Yaw), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.AttitudeTimeTag, data_pointer, sizeof(pingHeaderObject.AttitudeTimeTag), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.DOT, data_pointer, sizeof(pingHeaderObject.DOT), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.NavFixMilliseconds, data_pointer, sizeof(pingHeaderObject.NavFixMilliseconds), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ComputerClockHour, data_pointer, sizeof(pingHeaderObject.ComputerClockHour), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ComputerClockMinute, data_pointer, sizeof(pingHeaderObject.ComputerClockMinute), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.ComputerClockSecond, data_pointer, sizeof(pingHeaderObject.ComputerClockSecond), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.ComputerClockHsec, data_pointer, sizeof(pingHeaderObject.ComputerClockHsec), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.FishPositionDeltaX, data_pointer, sizeof(pingHeaderObject.FishPositionDeltaX), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.FishPositionDeltaY, data_pointer, sizeof(pingHeaderObject.FishPositionDeltaY), BytesPointer);
        data_pointer += data_copy(&pingHeaderObject.FishPositionErrorCode, data_pointer, sizeof(pingHeaderObject.FishPositionErrorCode), BytesPointer);

        data_pointer += data_copy(&pingHeaderObject.ReservedSpace2, data_pointer, 11, BytesPointer);
        return 256;
    }
    else if (dataType == 3) {
        data_pointer += data_copy(&pingChHeaderObject.ChannelNumber, data_pointer, sizeof(pingChHeaderObject.ChannelNumber), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.DownsampleMethod, data_pointer, sizeof(pingChHeaderObject.DownsampleMethod), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.SlantRange, data_pointer, sizeof(pingChHeaderObject.SlantRange), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.GroundRange, data_pointer, sizeof(pingChHeaderObject.GroundRange), BytesPointer);

        data_pointer += data_copy(&pingChHeaderObject.TimeDelay, data_pointer, sizeof(pingChHeaderObject.TimeDelay), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.TimeDuration, data_pointer, sizeof(pingChHeaderObject.TimeDuration), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.SecondsPerPing, data_pointer, sizeof(pingChHeaderObject.SecondsPerPing), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ProcessingFlags, data_pointer, sizeof(pingChHeaderObject.ProcessingFlags), BytesPointer);

        data_pointer += data_copy(&pingChHeaderObject.Frequency, data_pointer, sizeof(pingChHeaderObject.Frequency), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.InitialGainCode, data_pointer, sizeof(pingChHeaderObject.InitialGainCode), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.GainCode, data_pointer, sizeof(pingChHeaderObject.GainCode), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.BandWidth, data_pointer, sizeof(pingChHeaderObject.BandWidth), BytesPointer);

        data_pointer += data_copy(&pingChHeaderObject.ContactNumber, data_pointer, sizeof(pingChHeaderObject.ContactNumber), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ContactClassification, data_pointer, sizeof(pingChHeaderObject.ContactClassification), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ContactSubNumber, data_pointer, sizeof(pingChHeaderObject.ContactSubNumber), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ContactType, data_pointer, sizeof(pingChHeaderObject.ContactType), BytesPointer);

        data_pointer += data_copy(&pingChHeaderObject.NumSamples, data_pointer, sizeof(pingChHeaderObject.NumSamples), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.MillivoltScale, data_pointer, sizeof(pingChHeaderObject.MillivoltScale), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ContactTimeOffTrack, data_pointer, sizeof(pingChHeaderObject.ContactTimeOffTrack), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ContactCloseNumber, data_pointer, sizeof(pingChHeaderObject.ContactCloseNumber), BytesPointer);

        data_pointer += data_copy(&pingChHeaderObject.Reserved2, data_pointer, sizeof(pingChHeaderObject.Reserved2), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.FixedVSOP, data_pointer, sizeof(pingChHeaderObject.FixedVSOP), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.Weight, data_pointer, sizeof(pingChHeaderObject.Weight), BytesPointer);
        data_pointer += data_copy(&pingChHeaderObject.ReservedSpace, data_pointer, sizeof(pingChHeaderObject.ReservedSpace), BytesPointer);
        return 64;
    }
    else if (dataType == 4) {
        data_pointer += data_copy(&chanInfoObject.TypeOfChannel, data_pointer, sizeof(chanInfoObject.TypeOfChannel), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.SubChannelNumber, data_pointer, sizeof(chanInfoObject.SubChannelNumber), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.CorrectionFlags, data_pointer, sizeof(chanInfoObject.CorrectionFlags), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.UniPolar, data_pointer, sizeof(chanInfoObject.UniPolar), BytesPointer);

        data_pointer += data_copy(&chanInfoObject.BytesPerSample, data_pointer, sizeof(chanInfoObject.BytesPerSample), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.Reserved, data_pointer, sizeof(chanInfoObject.Reserved), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.ChannelName, data_pointer, sizeof(chanInfoObject.ChannelName), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.VoltScale, data_pointer, sizeof(chanInfoObject.VoltScale), BytesPointer);

        data_pointer += data_copy(&chanInfoObject.Frequency, data_pointer, sizeof(chanInfoObject.Frequency), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.HorizBeamAngle, data_pointer, sizeof(chanInfoObject.HorizBeamAngle), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.TiltAngle, data_pointer, sizeof(chanInfoObject.TiltAngle), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.BeamWidth, data_pointer, sizeof(chanInfoObject.BeamWidth), BytesPointer);

        data_pointer += data_copy(&chanInfoObject.OffsetX, data_pointer, sizeof(chanInfoObject.OffsetX), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.OffsetY, data_pointer, sizeof(chanInfoObject.OffsetY), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.OffsetZ, data_pointer, sizeof(chanInfoObject.OffsetZ), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.OffsetYaw, data_pointer, sizeof(chanInfoObject.OffsetYaw), BytesPointer);

        data_pointer += data_copy(&chanInfoObject.OffsetPitch, data_pointer, sizeof(chanInfoObject.OffsetPitch), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.OffsetRoll, data_pointer, sizeof(chanInfoObject.OffsetRoll), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.BeamsPerArray, data_pointer, sizeof(chanInfoObject.BeamsPerArray), BytesPointer);
        data_pointer += data_copy(&chanInfoObject.SampleFormat, data_pointer, sizeof(chanInfoObject.SampleFormat), BytesPointer);

        data_pointer += data_copy(&chanInfoObject.ReservedArea2, data_pointer, sizeof(chanInfoObject.ReservedArea2), BytesPointer);
        return 128;
    }
    return 0;
}

int MainWindow :: xtf_dataProcess() {
    QList<quint16> listA1, listB1, listC1, listA2, listB2;
    static bool xtfMisalignment = false;
    int readFileBytes = 0, fileFormat = 0;

    // Read XTFPINGHEADER
    player.dataPtr += xtf_readFile(player.dataPtr, 2, &readFileBytes);
    qDebug() << "XTFPINGHEADER" << pingHeaderObject.MagicNumber << pingHeaderObject.HeaderType << pingHeaderObject.NumChansToFollow <<
                pingHeaderObject.NumBytesThisRecord << pingHeaderObject.PingNumber << "Time:" <<
                pingHeaderObject.Year << pingHeaderObject.Month << pingHeaderObject.Day <<
                pingHeaderObject.Hour << pingHeaderObject.Minute << pingHeaderObject.Second << pingHeaderObject.JulianDay <<
                "NumberOfBytes" << readFileBytes;

    if (pingHeaderObject.MagicNumber != 0xFACE) {
        xtfMisalignment = true;
    }
    else {
        xtfMisalignment = false;
    }
    if (pingHeaderObject.HeaderType != 0 || xtfMisalignment) {
        readFileTimer.stop();
        replotTimer.stop();

        return readFileBytes;
    }

    if (pingHeaderObject.NumChansToFollow > 0) {
        // Read XTFPINGCHANHEADER
        player.dataPtr += xtf_readFile(player.dataPtr, 3, &readFileBytes);
        qDebug() << "XTFPINGCHANHEADER" << pingChHeaderObject.ChannelNumber << pingChHeaderObject.SlantRange <<
                    pingChHeaderObject.Frequency << pingChHeaderObject.NumSamples << "NumberOfBytes" << readFileBytes;

        ping->rangeLf_tmp = (quint16) pingChHeaderObject.SlantRange;
        ping->rangeLf_double = ping->rangeLf_tmp;
        ping->samplesToPlotLF = pingChHeaderObject.NumSamples;
        ping->roll = pingHeaderObject.SensorRoll;
        ping->pitch = pingHeaderObject.SensorPitch;
        ping->heading = pingHeaderObject.SensorHeading;
        ping->lastTracedyaw = pingHeaderObject.Yaw;
        ping->pingNumCurrent = pingHeaderObject.PingNumber;
        currentRtcTime.year = pingHeaderObject.Year;
        currentRtcTime.month = pingHeaderObject.Month;
        currentRtcTime.day = pingHeaderObject.Day;
        currentRtcTime.hour = pingHeaderObject.Hour;
        currentRtcTime.minute = pingHeaderObject.Minute;
        currentRtcTime.second = pingHeaderObject.Second;
        currentRtcTime.millisecond = pingHeaderObject.HSeconds;

        // Copy LF port samples
        for(int i = 0; i< ping->samplesToPlotLF; i++) {
            quint16 x;
            player.dataPtr += data_copy(&x, player.dataPtr, 2, &readFileBytes);
            listA1.append(x);
        }

        player.dataPtr += xtf_readFile(player.dataPtr, 3, &readFileBytes);
        qDebug() << "XTFPINGCHANHEADER2" << pingChHeaderObject.ChannelNumber << pingChHeaderObject.SlantRange <<
                    pingChHeaderObject.Frequency << pingChHeaderObject.NumSamples << "NumberOfBytes" << readFileBytes;

        // Copy LF starboard samples
        for(int i = 0; i< ping->samplesToPlotLF; i++) {
            quint16 y;
            player.dataPtr += data_copy(&y, player.dataPtr, 2, &readFileBytes);
            listB1.append(y);
        }

        WORD chanNum = 0;
        memcpy(&chanNum, player.dataPtr, 2);
        qDebug() << "chanNum" << chanNum;

        // Read HF channels if available
        if ((chanNum == 2) || (chanNum == 3) || pingHeaderObject.NumChansToFollow > 2) {
            flag.useDualFrequency = true;
            flag.PlotHfSidescan = 1;

            player.dataPtr += xtf_readFile(player.dataPtr, 3, &readFileBytes);
            qDebug() << "XTFPINGCHANHEADER3" << pingChHeaderObject.ChannelNumber << pingChHeaderObject.SlantRange <<
                        pingChHeaderObject.Frequency << pingChHeaderObject.NumSamples << "NumberOfBytes" << readFileBytes;
            ping->rangeHf_tmp = (quint16) pingChHeaderObject.SlantRange;
            ping->rangeHf_double = ping->rangeHf_tmp;
            ping->samplesToPlotHF = pingChHeaderObject.NumSamples;

            for(int i = 0; i< ping->samplesToPlotHF; i++) {//get LF port samples
                quint16 x;
                player.dataPtr += data_copy(&x, player.dataPtr, 2, &readFileBytes);
                listA2.append(x);
            }

            player.dataPtr += xtf_readFile(player.dataPtr, 3, &readFileBytes);
            qDebug() << "XTFPINGCHANHEADER4" << pingChHeaderObject.ChannelNumber << pingChHeaderObject.SlantRange <<
                        pingChHeaderObject.Frequency << pingChHeaderObject.NumSamples << "NumberOfBytes" << readFileBytes;

            for(int i = 0; i< ping->samplesToPlotHF; i++) {//get LF port samples
                quint16 y;
                player.dataPtr += data_copy(&y, player.dataPtr, 2, &readFileBytes);
                listB2.append(y);
            }
        }

        if(ping->rangeLf_tmp != ping->rangeLf_int) {//if range recieved from the board is not the current GUI range, set GUI range to board range
            if (ping->rangeLf_tmp != 0) {
                setRecordedRange(ping->rangeLf_tmp);
                flag.highRangeChange = true;
            }
            ping->rangeLf_int = ping->rangeLf_tmp;
        }
        if(ping->rangeHf_tmp != ping->rangeHf_int) {//same as above
            if (ping->rangeHf_tmp > 0 && !flag.highRangeChange) {
                setRecordedRange(ping->rangeHf_tmp);
            }
            flag.highRangeChange = false;
            ping->rangeHf_int = ping->rangeHf_tmp; // Only use this after setRecordedRange is called
        }
        flag.highRangeChange = false;
        qDebug() << "How many bytes?" << readFileBytes;
        if (player.typeXtf) {
            for(int k=0, s=listA1.size(), max=(s/2); k<max; k++) listA1.swap(k,s-(1+k));
            for(int k=0, s=listA2.size(), max=(s/2); k<max; k++) listA2.swap(k,s-(1+k));
        }

        ping->data_portLf = listA1;
        ping->data_stbdLf = listB1;
        ping->data_portHf = listA2;
        ping->data_stbdHf = listB2;
    }
    return readFileBytes;
}

/****************Gain  functions ****************/

/**
 * @brief MainWindow::setTransmitter
 * @param trans
 * Changes which transmitters are on
 */
void MainWindow :: setTransmitter(int trans) {
    qDebug() << "setTransmitter" << trans;
    if (flag.readyToTransmit == 1) {
        switch(trans) {
        case 0:
            Tcp_sendCommand(3, 0, 0);//both HF and LF off
            break;
        case 1:
            Tcp_sendCommand(3, 1, 0);//LF on
            break;
        case 2:
            Tcp_sendCommand(3, 2, 0);//HF on
            break;
        case 3:
            Tcp_sendCommand(3, 3, 0);//Both on
            break;
        }
    }
}

void MainWindow :: setEdgeAlign(bool state) {
    flag.EdgeAlignActive = state;
}

/**
 * @brief MainWindow::setGainSliderVal
 * @param value
 * DOES NOT SET THE VALUE FOR THE MAIN WINDOW SLIDER
 * Takes value from gain slider in system settings (default 2) and applies it to the board on connection
 */
void MainWindow :: setGainSliderVal(int value) {
    on_gain_mult_slider_valueChanged(value);
}

/**
 * @brief MainWindow::setHFGains
 * @param starHF
 * @param portHF
 * Used to apply saved gains
 */
void MainWindow :: setHFGains(double starHF, double portHF) {
    gain->starGainHF = starHF;
    gain->portGainHF = portHF;
}

/**
 * @brief MainWindow::applyGains
 * @param upper
 * @param lower
 * @param bal
 * Used to apply saved colorscale gains from the user database
 */
void MainWindow :: applyGains(double upper, double lower, int bal) {
    colorScale->axis()->setRangeUpper(upper);
    colorScale->axis()->setRangeLower(lower);
    ui->digitalGainBalance->setValue(bal);
    on_digitalGainBalance_valueChanged(bal);
}

void MainWindow :: gainSlider_setValueDigital(int value) {
    ui->digitalGainLabel->setText("  "+QString::number(value) + "%");
    ui->digitalGainBalance->setValue(value);
    if (value == -1 || value == 1 || value == 0) {//if slider value 1, -1, or 0
        gain->starGain = 1;
        gain->portGain = 1;
    }
    else if (value < 0) {//if slider value negative, apply gain to port side
        gain->starGain = 1;
        gain->portGain = abs(value);//takes the absolute value to get rid of the negative
    }
    else {//if slider value positive, apply gain to starboard side
        gain->starGain = value;
        gain->portGain = 1;
    }
    emit sendLFBalance(value);//send balance value to user database for saving
    gain->DigitalGainIndex = value;
}

/**
 * @brief MainWindow::on_digitalGainBalance_valueChanged
 * @param value
 * Gets the gain value from the balance slider and applies to to either port or starboard
 */
void MainWindow :: on_digitalGainBalance_valueChanged(int value) {
    gainSlider_setValueDigital(value);
    emit GainsToUsermenu(ui->gainSlider->value(), value, ui->hfGainSlider->value());
}

void MainWindow :: on_gain_mult_slider_valueChanged(int value) {
    if (flag.readyToTransmit == 1) {
        Tcp_sendCommand(8, value, 0);
    }
}

/****************TVG****************/

/**
 * @brief MainWindow::onColorScaleRangeChanged
 * If colorscale dragged
 */
void MainWindow :: setTvg(bool tvgSet) {
    flag.tvgActive = tvgSet;
}

void MainWindow :: GainsFromUsermenu(bool tvg,  QString unit, int Gain, int Balance, int Gain_HF) {
    flag.tvgActive = gain->tvg_on;

    if (!tvg) {
//        gain->tvg_CLfP = 0;
//        gain->tvg_CLfS = 0;
//        gain->tvg_CHfP = 0;
//        gain->tvg_CHfS = 0;
    }

    // Update Sliders
    gainSlider_setValueLf(gain->gainSliderIndexLf);
    gainSlider_setValueHf(gain->gainSliderIndexHf);
    gainSlider_setValueDepth(gain->gainSliderIndexDepth);
    gainSlider_setValueDigital(Balance);

    if(unit.compare("Yards")==0){setting.convert_unit = 1.094;}
    //the unit is in meters//
    else{setting.convert_unit =1;}
}


/*******************Simulation Functions********************/

void MainWindow :: simulate_Plot(bool action){
    if (action == 0){
        state.GUI_MODE = 0;
        state.PLAY_MODE = 0;
        flag.PlotHfSidescan = 1;
        flag.PlotAngleScan = 0;
        format_preparePlots();
        ui->targetListWidget->hide();
        ui->waypointListWidget->show();
        //Run btn should be enabled
        ui->btn_range1->setEnabled(true);
        ui->btn_range2->setEnabled(true);
        ui->btn_range3->setEnabled(true);
        ui->btn_range4->setEnabled(true);
        on_range_btn_clicked(30);

        //        map_x1_config1 = -30;  // Sonar Range
        //        map_x2_config1 = 30;  // Sonar Range
        //        samplesToPlot_config1 = 1250;    // Number of Samples per Ping
        //        f_rep_config1 = 5;            // Repittion Frequency of Sonar
        //        res_fwd_config1 = 0.2;  // Forward resolution based on boat speed
    } else{
        //Run btn should be disabled
        //        GUI_MODE = 1;
    }
    setup_buttonsAndTools();
}

/***************To/From User Menu******************************/

/**
 * @brief MainWindow::updateCableLength
 * @param length
 * sets cable length to length and sets the text in the toolbar to match
 */
void MainWindow :: updateCableLength(double length) {
    //ui->status_cl->setText(QString::number(length));
    setting.cableLength = length;
}

void MainWindow :: receiveBoatSpeed(int i) {
    switch (i)
    {
    case 0:
        setting.boatSpeed = 1;
        break;
    case 1:
        setting.boatSpeed = 1.5;
        break;
    case 2:
        setting.boatSpeed = 2;
        break;
    case 3:
        setting.boatSpeed = 2.5;
        break;
    case 4:
        setting.boatSpeed = 3;
        break;
    case 5:
        setting.boatSpeed = 3.5;
        break;
    case 6:
        setting.boatSpeed = 4;
        break;
    case 7:
        setting.boatSpeed = 4.5;
        break;
    case 8:
        setting.boatSpeed = 5;
        break;
    }
    if (colorMapNum_queue.size() > 0 || colorMapHFNum_queue.size() > 0)
        plot_data_addColorMap();
}

void MainWindow :: receiveDepthRange(int i){

    //    qDebug()<<i;
    switch (i)
    {
    case 0:
        setting.globalDepth = 10;
        break;
    case 1:
        setting.globalDepth = 20;
        break;
    case 2:
        setting.globalDepth = 30;
        break;
    case 3:
        setting.globalDepth = 40;
        break;
    case 4:
        setting.globalDepth = 50;
        break;

    }

}

void MainWindow :: receive_alt_length(int i) {
    switch (i)
    {
    case 0:
        setting.averageDepthQueueLength = 1;
        break;
    case 1:
        setting.averageDepthQueueLength = 2;
        break;
    case 2:
        setting.averageDepthQueueLength = 3;
        break;
    case 3:
        setting.averageDepthQueueLength =  4;
        break;
    case 4:
        setting.averageDepthQueueLength = 5;
        break;
    case 5:
        setting.averageDepthQueueLength = 10;
        break;
    case 6:
        setting.averageDepthQueueLength = 15;
        break;
    case 7:
        setting.averageDepthQueueLength = 20;
        break;
    }
}

void MainWindow :: receive_yaw_length(int i){
    switch (i) {
    case 0:
        setting.yawRunningLength = 1;
        break;
    case 1:
        setting.yawRunningLength = 2;
        break;
    case 2:
        setting.yawRunningLength = 3;
        break;
    case 3:
        setting.yawRunningLength =  4;
        break;
    case 4:
        setting.yawRunningLength = 5;
        break;
    case 5:
        setting.yawRunningLength = 6;
        break;
    case 6:
        setting.yawRunningLength = 7;
        break;
    case 7:
        setting.yawRunningLength= 8;
        break;
    case 8:
        setting.yawRunningLength = 9;
        break;
    case 9:
        setting.yawRunningLength = 10;
        break;
    case 10:
        setting.yawRunningLength = 15;
        break;
    case 11:
        setting.yawRunningLength = 20;
        break;
    }
}

int MainWindow :: getBoadSpeedIndex(float speed) {

    int index;
    if(speed == 2 || speed == 2.5 || speed == 3 || speed == 3.5 || speed == 4 || speed == 4.5 || speed == 5 ){
        index = speed;
    }
    else{
        index = 3;
    }
    return index;
}

void MainWindow :: receiveEthernetActive(bool b) {
    if (b)
        flag.ethernetActive = true;
    else
        flag.ethernetActive = false;

}

/**
 * @brief MainWindow::updateSpeedOfSound
 * @param ss
 * Sets speed of sound to ss and sets the text in the toolbar to match
 */
void MainWindow :: updateSpeedOfSound(double ss) {
    setting.speed_of_sound = ss;
}

void MainWindow :: on_actionSaveSettings_triggered () {
    // Open a windows dialog to pick location of saved file
    QString fileName = player.openPath + "SettingsConfiguration.ini";
    QString filter = "Configuration File (*.ini)";
    fileName  = QFileDialog::getSaveFileName(this, tr("Save the Settings File"),
                                             player.openPath + "/Settings", filter, &filter);
    // Execute function in settings window to save settings
    userWindowInstance->SaveIniFile(fileName);
}

void MainWindow :: on_actionLoadSettings_triggered () {
    bool pause = false;
    if (state.GUI_MODE == 2 && state.PLAY_MODE == 3) {
        pause = true;
        player_playData(false);
    }
    // Open a windows dialog to open file
    QString data_filePath = QFileDialog::getOpenFileName(this,"Select the settings file",player.openPath,"Configuration File (*.ini)");
    if (data_filePath == "")    return;

    // remove all colormaps from plot
    plot_clearSonarPlots();
    dataMap.clearAll();
    // Execute function in settings window
    userWindowInstance->ReadIniFile(data_filePath);
    Chart_UnstretchPlot();
    plot_range_unstretchLfHfPlots();

    if (pause)
        player_playData(true);
}

/****************To/From System Settings***************************************/

/**
 * @brief MainWindow::openSystemSettings
 * Opens system settings, hides password prompt
 */
void MainWindow :: openSystemSettings() {
    systemSettingInstance->show();
    devWindowInstance->hide();
}

/**
 * @brief MainWindow::showTime
 * Displays elapsed time since application has been opened
 */
void MainWindow :: showTime() {
    int sec = elTime.elapsed()/1000;
    int min = (sec / 60) % 60;
    int hr = (sec / 3600);
    sec = sec % 60;
    ui->quick_timerLabel->setText(QString("%1:%2:%3")
                                  .arg(hr, 2, 10, QLatin1Char('0'))
                                  .arg(min, 2, 10, QLatin1Char('0'))
                                  .arg(sec, 2, 10, QLatin1Char('0')) );
}

/**************X**********/

/**
 * @brief MainWindow::changeNoiseThresh
 * @param newThresh
 * Changes the threshold value for noise filtering
 */
void MainWindow :: changeNoiseThresh(double newThresh) {
    noiseThresh = newThresh;
    emit sendNoiseThresh(noiseThresh);
}

/**
 * @brief MainWindow::setFilterSize
 * @param size
 * Sets the size of the filter in processData
 * Called from the system settings menu
 */
void MainWindow :: setFilterSize(int size) {
    realSize = size;
    emit sendFilterSize(realSize);
    filterStartTrace = traceCounter1;
}

/**
 * @brief MainWindow::setFilterEnabled
 * @param enabled
 * Enables and disables the filter in processData
 */
void MainWindow :: setFilterEnabled(bool enabled) {
    flag.filterOn = enabled;
}

void MainWindow :: setHFWindowOpen(bool open) {                                        //SLOT:set the hf_window_btn
    Format->isOpenHf = open;
}

void MainWindow :: setDepthWindowOpen(bool depthOpen) {
    Format->isOpen_DepthPlot = depthOpen;
}

void MainWindow :: setScopeWindowOpen(bool ScopeOpen) {                                //SLOT:set the status of scope_btn
    flag.graphDialogActive = ScopeOpen;
    ui->scope_btn->setStyleSheet("background-color: rgb(255, 255, 255);");
    // ui->scope_btn->setStyleSheet("QToolTip{border: 0px solid black; background:white;}");
}

void MainWindow :: receiveDataToSend(int data) {
    Tcp_sendCommand(21, 0, data);
}

/**
 * @brief MainWindow::setCompressionFactor
 * @param factor
 * Used to update the compression factor from the system settings menu
 * the factor is the number of samples we find the maximum of during each pass of compression
 */
void MainWindow :: setCompressionFactor(int factor) {
    compressionFactor = factor;
}

/**
 * @brief MainWindow::callCompressList
 * @param list      list you want to compress
 * Compresses data from board by compression factor
 */
QList<quint16> MainWindow :: compressList(QList<quint16> list) {
    //This function operates by splitting list into a number of smaller qlists whose size is determined by compression factor
    //The greatest values contained in each qlist are then added to a new list, and returned

    if(ping->rangeLf_double==360){
        compressionFactor = compressionFactor360;
    }
    if(ping->rangeLf_double==480){
        compressionFactor = compressionFactor480;
    }
    QList<quint16> list2;
    quint16 max = 0;
    while (!list.isEmpty()) {
        for (int i = 0; i < compressionFactor; i++) {
            quint16 temp = list.takeFirst();
            if (temp >= max)
                max = temp;
        }
        list2.append(max);
        max = 0;
    }
    return list2;
}

void MainWindow :: setCompressionRate360(double compRate360){
    compressionFactor360 = compRate360;
}

void MainWindow :: setCompressionRate480(double compRate480){
    compressionFactor480 = compRate480;
}

/*****************Chart Dialog****************************************/

void MainWindow :: on_openFileButton_clicked(){
    ui->gpsPlot->removeItem(gpsChart.chartPixmap);
    if (ui->gpsPlot->width() < 300){
        ChartWindowInstance->mapPlotPtr->xAxis->setRange(gpsChart.minLongitude, gpsChart.maxLongitude);
    }
    Chart_selectFile();
    setup_buttonsAndTools();
}

void MainWindow :: Chart_Values(double w, double x, double y, double z){
    gpsChart.minLongitude = w; // min X
    gpsChart.maxLongitude = x; // Max X
    gpsChart.minLatitude = y; // Min Y
    gpsChart.maxLatitude = z; // Max Y
    gpsChart.chartPixmap->setScaled(true,Qt::KeepAspectRatio);

    ChartWindowInstance->mapPlotPtr->xAxis->setRange(w, x);
    ChartWindowInstance->mapPlotPtr->yAxis->setRange(y, z);

    gpsChart.chartPixmap->topLeft->setCoords(w, z);
    gpsChart.chartPixmap->bottomRight->setCoords(x, y);

    resizeEvent(nullptr);

    ChartWindowInstance->target_resizeAllTargets();
    ChartWindowInstance->plot_replot();
    ChartWindowInstance->target_resizeAllTargets();
}

void MainWindow :: Chart_GpsPlotReplot() {
    double xCoord=0, yCoord=0;
    int xCell, yCell;

    if (ChartWindowInstance->mapPlotPtr == nullptr)  return;

    ChartWindowInstance->mapPlotPtr->clearPlottables();

    double x1 = ChartWindowInstance->mapPlotPtr->xAxis->range().lower;
    double x2 = ChartWindowInstance->mapPlotPtr->xAxis->range().upper;
    double y1 = ChartWindowInstance->mapPlotPtr->yAxis->range().lower;
    double y2 = ChartWindowInstance->mapPlotPtr->yAxis->range().upper;

    ChartWindowInstance->setPlotRange(x1, x2, y1, y2);
    ChartWindowInstance->setPlotColorMap(x1, x2, y1, y2, height_r, wdth_r);

    QCPColorMap* RncMap = ChartWindowInstance->getColorMapPointer();

    for (int i = 0; i < (height_r-1)*5/6; i++)
    {
        for (int j = 0; j < (wdth_r); j++)
        {
            // qDebug()<<"maxLongitude: "<<maxLongitude;
            xCoord = (j*(gpsChart.maxLongitude-gpsChart.minLongitude))/(wdth_r*1.0) + gpsChart.minLongitude;
            //qDebug()<<"xCoord: "<<xCoord;
            yCoord = -(i*(gpsChart.maxLatitude-gpsChart.minLatitude))/(height_r*1.0) + gpsChart.maxLatitude;
            if((xCoord<x1||xCoord>x2) || (yCoord<y1||yCoord>y2)){
                continue;
            }
            RncMap->data()->coordToCell(xCoord,yCoord,&xCell,&yCell);

            RncMap->data()->setAlpha(xCell,yCell,255);
        }
    }
    ChartWindowInstance->plot_replot();
}

void MainWindow :: Chart_ReadTiffFile(){

    gpsChart.chartPixmap = new QCPItemPixmap(ui->gpsPlot);
    gpsChart.chartPixmap->setParent(ui->gpsPlot);
    qDebug()<<"tiffname: "<<gpsChart.tiffFile.fileName();
    QPixmap pixmap(gpsChart.tiffFile.fileName());

    gpsChart.chartPixmap->setPixmap(pixmap);
    gpsChart.chartPixmap->setScaled(true);
    gpsChart.chartPixmap->topLeft->setCoords(0, 5);
    gpsChart.chartPixmap->bottomRight->setCoords(5,0);
    gpsChart.minLongitude = 0; // min X
    gpsChart.maxLongitude = 5; // Max X
    gpsChart.minLatitude = 0; // Min Y
    gpsChart.maxLatitude = 5; // Max Y
    gpsChart.originalChart = 1;

    ChartWindowInstance->mapPlotPtr->xAxis->setRange(0, 5);
    ChartWindowInstance->mapPlotPtr->yAxis->setRange(0, 5);
    ChartWindowInstance->mapPlotPtr->xAxis->setVisible(false);
    ChartWindowInstance->mapPlotPtr->yAxis->setVisible(false);
    ChartWindowInstance->plot_replot();
    emit tiffFileName(gpsChart.tiffFile.fileName());

    if (PLAYER_DEMO == 0) {
        chartDialogInstance->setWindowTitle("Chart Dialog");
        chartDialogInstance->setWindowFlag(Qt::WindowContextHelpButtonHint,false);
        chartDialogInstance->open();
    }
}

int MainWindow :: Chart_ReadTiff(char* tiffDataPointer, int bytesPointer){

    TIFF_LEADER tiffLeader;

    int valueInt=0,byteOrderInt=0,checkTiffFileInt=0,offsetInt=0,tagTypeInt=0,fieldTypeInt=0,countInt=0,valueOffsetInt=0,noOfTagsInt=0;

    tiffDataPointer += data_copy(&tiffLeader.byteOrder, tiffDataPointer, 2, &bytesPointer);
    tiffDataPointer += data_copy(&tiffLeader.checkTiffFile, tiffDataPointer, 2, &bytesPointer);
    tiffDataPointer += data_copy(&tiffLeader.offSet, tiffDataPointer, 4, &bytesPointer);

    offsetInt = Chart_ConvertToInt2(tiffLeader.offSet, 4, tiffLeader.byteOrder[0]);

    tiffDataPointer = tiffDataPointer - 8;
    bytesPointer += offsetInt-8;
    tiffDataPointer += offsetInt;

    tiffDataPointer += data_copy(&tiffLeader.noOfTags, tiffDataPointer, 2, &bytesPointer);
    noOfTagsInt = Chart_ConvertToInt2(tiffLeader.noOfTags,2,tiffLeader.byteOrder[0]);

    for(int i=0;i<noOfTagsInt;i++){

        tiffDataPointer += data_copy(&tiffLeader.tagType, tiffDataPointer, 2, &bytesPointer);
        tagTypeInt = Chart_ConvertToInt2(tiffLeader.tagType,2,tiffLeader.byteOrder[0]);
        //  qDebug()<<"tags: "<<tagTypeInt;
        tiffLeader.tagTypeList.append(tagTypeInt);

        tiffDataPointer += data_copy(&tiffLeader.fieldType, tiffDataPointer, 2, &bytesPointer);
        fieldTypeInt = Chart_ConvertToInt2(tiffLeader.fieldType,2,tiffLeader.byteOrder[0]);
        tiffLeader.fieldTypeList.append(fieldTypeInt);
        qDebug()<<"fieldType: "<<fieldTypeInt;

        tiffDataPointer += data_copy(&tiffLeader.count, tiffDataPointer, 4, &bytesPointer);
        countInt = Chart_ConvertToInt2(tiffLeader.count,4
                                 ,tiffLeader.byteOrder[0]);
        qDebug()<<"count: "<<countInt;
        tiffLeader.countList.append(countInt);


        tiffDataPointer += data_copy(&tiffLeader.valueOffset, tiffDataPointer, 4, &bytesPointer);
        valueOffsetInt = Chart_ConvertToInt2(tiffLeader.valueOffset,4,tiffLeader.byteOrder[0]);
        //            qDebug()<<"offset: "<<valueOffsetInt;
        tiffLeader.valueOffSetList.append(valueOffsetInt);

    }

    int offSetStore=0;
    int range = 0 ;
    for(int i=0;i<tiffLeader.tagTypeList.size();i++){
        if(tiffLeader.tagTypeList.at(i)>10000){
            range = tiffLeader.countList.at(i) * tiffLeader.fieldTypeList.at(i);
            qDebug()<<"range:"<<range;
            offSetStore = tiffLeader.valueOffSetList.at(i);
            qDebug()<<"ofset for tags > 10000: "<<offSetStore;
            tiffLeader.geoTiffOfset.append(offSetStore);
            tiffLeader.rangeList.append(range);
        }
    }

    for(int i=0;i<tiffLeader.geoTiffOfset.size();i++){

        tiffDataPointer -= bytesPointer;//tiffFile.readAll().data();
        bytesPointer = 0;
        tiffDataPointer+= tiffLeader.geoTiffOfset.at(i);
        bytesPointer += tiffLeader.geoTiffOfset.at(i);

        char value[tiffLeader.rangeList.at(i)];

        tiffDataPointer+= data_copy(value,tiffDataPointer,tiffLeader.rangeList.at(i),&bytesPointer);

        valueInt = Chart_ConvertToInt2(value,tiffLeader.rangeList.at(i),tiffLeader.byteOrder[0]);

        tiffLeader.valueList.append(valueInt);
        qDebug()<<"prach: "<<valueInt;

    }

    byteOrderInt = Chart_ConvertToInt2(tiffLeader.byteOrder,2,tiffLeader.byteOrder[0]);
    checkTiffFileInt = Chart_ConvertToInt2(tiffLeader.checkTiffFile,2,tiffLeader.byteOrder[0]);
    qDebug()<<byteOrderInt<<", "<<checkTiffFileInt<<", "<<offsetInt<<", "<<noOfTagsInt<<", "<<tagTypeInt<<
              ", "<<fieldTypeInt<<", "<<countInt<<", "<<valueOffsetInt<<", "<<valueInt;
    return bytesPointer;
}

int MainWindow :: Chart_ConvertToInt2 (char *dataPointer, int size, char tp) {
    QByteArray strArray;
    for (int i = 0; i < size; i++) {
        strArray.append(dataPointer[i]);
    }
    int value = 0;

    if (tp =='I') {
        for (int i = size; i >= 0; i--) {
            char b;
            if (i < strArray.length()) {
                b = strArray.at(i);
            } else {
                b = 0;
            }
            //        int tmp = strArray.toInt();
            value = (value << 8) | quint8(b);
        }
        return value;

    } else {
        for (int i = 0; i <= size; i++) {
            char b;
            if (i < strArray.length()) {
                b = strArray.at(i);
            } else {
                b = 0;
            }
            value = (value << 8) | quint8(b);
        }
        return value;
    }
}

void MainWindow :: Chart_selectFile(){

    //measuring tape should be switched to false when changing files
    //can cause crashes
    if (flag.measuringTapeActive)    on_actionMeasuring_Tape_triggered();

    QString data_filePath;
    if (PLAYER_DEMO == 0) {
        data_filePath = QFileDialog::getOpenFileName(this,"Select a chart file",recorder.recPath,
                                                     "Electronic Nautical Chart S-57 (*.000)\nGeoTIFF File (*.tiff *.png *.tif)");
    } else {
        data_filePath = QCoreApplication::applicationDirPath() + "\\Glen Haven.png";
    }
    if (data_filePath.size() == 0) {
        emit updateFileSelected(false);//sets file selected to false in recorder
        emit updateFileName("");//sets file name to blank text
        if (player.fileBuffer == nullptr) {//if no file selected
            state.PLAY_MODE = 0;
            state.GUI_MODE = 1;
            setup_buttonsAndTools();
            return;
        }
        return;
    }

    QFileInfo fi(data_filePath);
    QString data_fileName = fi.filePath();
    QString extension = fi.suffix();

    if (!QFile(data_filePath).exists()) return;
    else if (extension.compare("tif")==0 || extension.compare("tiff")==0 || extension.compare("png")==0){
        gpsChart.filePath = data_filePath;
        gpsChart.tiffFile.setFileName(data_filePath);
        gpsChart.tiffFile.open(QIODevice::ReadWrite);

        ui->openFileButton->setIcon(QIcon(":/icons/icons/world.png"));
        ui->openFileButton->setChecked(true);

        Chart_ReadTiffFile();
    }
    else if (extension.compare("000") == 0) {
        EncFile.close();
        // Read S-57 file to plot map on chart windowf
        EncFile.setFileName(data_filePath);
        EncFile.open(QIODevice::ReadWrite);


        //ChartWindowInstance->show();
        progressBar->setVisible(true);
        progressBar->show();
        emit updateChartProgress(0);

        QThreadPool pool;
        encChartThreadProgress = QtConcurrent::run(this, &MainWindow::Enc_readFile); //Runs function in a thread
        EncTimer.start(100);
        connect(&EncTimer, SIGNAL(timeout()), this, SLOT(Enc_checkToPlot()));
        qDebug() << "Done ENC";
//        encChartThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on
        //Enc_readFile();

//        // Plot all spatial Features
//        double xEncMin, xEncMax, yEncMin, yEncMax;
//        Enc_FindPlotRange(&xEncMin, &xEncMax, &yEncMin, &yEncMax);
//        ChartWindowInstance->setPlotRange(xEncMin, xEncMax, yEncMin, yEncMax);
//        ChartWindowInstance->setPlotColorMap(xEncMin, xEncMax, yEncMin, yEncMax);
//        Enc_PlotRecordObjects();
//        ChartWindowInstance->replot();
//        setprogressBarValue(100);

    //    //    ChartWindowInstance->paintMap();//determineAreaFill();
    //    ChartWindowInstance->replot();
    }
}

void MainWindow :: Chart_Zoom() {
    // This function is used to fix the memory issue that occurs with the chart pixmap
    // When the user zooms in, it crops the image to the viewing area. When the user
    // zooms out, the chart was show the original image
    if (!ui->gpsPlot->hasItem(gpsChart.chartPixmap))     return;

    // Find coordinates of both the map and plot
    double xMap1 = gpsChart.chartPixmap->topLeft->coords().x(), xMap2 = gpsChart.chartPixmap->bottomRight->coords().x();
    double yMap1 = gpsChart.chartPixmap->bottomRight->coords().y(), yMap2 = gpsChart.chartPixmap->topLeft->coords().y();
    double xPlot1 = ui->gpsPlot->xAxis->range().lower, xPlot2 = ui->gpsPlot->xAxis->range().upper;
    double yPlot1 = ui->gpsPlot->yAxis->range().lower, yPlot2 = ui->gpsPlot->yAxis->range().upper;

    double xMapDiff = xMap2 - xMap1, yMapDiff = yMap2 - yMap1;
    double xPlotDiff = xPlot2 - xPlot1, yPlotDiff = yPlot2 - yPlot1;

    // Expand map area to extend a little beyond the current plotting area
    xPlot1 -= 0.5*xPlotDiff;
    yPlot1 -= 0.5*yPlotDiff;
    xPlot2 += 0.5*xPlotDiff;
    yPlot2 += 0.5*yPlotDiff;
    xPlotDiff *= 2;
    yPlotDiff *= 2;

    double x1Diff = xPlot1 - xMap1, y2Diff = -yPlot2 + yMap2;

    if (xPlot1 > xMap1 || xPlot2 < xMap2 || yPlot1 > yMap1 || yPlot2 < yMap2) {
        if (xPlotDiff*yPlotDiff < 0.1*xMapDiff*yMapDiff) {
            // If the area of the plot is less than 10% the area of the map, then crop pixmap

            // Get details of pixmap
            int height = gpsChart.chartPixmap->pixmap().height(), width = gpsChart.chartPixmap->pixmap().width();
            double x1_pixel = (x1Diff / xMapDiff) * width, y1_pixel = (y2Diff / yMapDiff) * height;
            double x2_pixel = (xPlotDiff / xMapDiff) * width, y2_pixel = (yPlotDiff / yMapDiff) * height;

            // Crop the pixmap
            QPixmap pixmap = gpsChart.chartPixmap->pixmap().copy(x1_pixel, y1_pixel, x2_pixel, y2_pixel);
            gpsChart.chartPixmap->setPixmap(pixmap);
            gpsChart.chartPixmap->topLeft->setCoords(xPlot1, yPlot2);
            gpsChart.chartPixmap->bottomRight->setCoords(xPlot2, yPlot1);
            gpsChart.originalChart = 0;
        }
    }
    else if (gpsChart.originalChart == 0 && (ui->gpsPlot->xAxis->range().lower < xMap1 || ui->gpsPlot->xAxis->range().upper > xMap2
                    || ui->gpsPlot->yAxis->range().lower < yMap1 || ui->gpsPlot->yAxis->range().upper > yMap2)) {
        // If the plot ranges are outside of the map ranges, and inside the original map area
        QPixmap pixmap(gpsChart.tiffFile.fileName());

        gpsChart.chartPixmap->setPixmap(pixmap);
        gpsChart.chartPixmap->setScaled(true);
        gpsChart.chartPixmap->topLeft->setCoords(gpsChart.minLongitude, gpsChart.maxLatitude);
        gpsChart.chartPixmap->bottomRight->setCoords(gpsChart.maxLongitude, gpsChart.minLatitude);
        gpsChart.originalChart = 1;
    }
}

void MainWindow :: Chart_UnstretchPlot() {
    QSize gpsPlotSz = ui->gpsPlot->axisRect()->size();

    int gpsHeight = gpsPlotSz.rheight();
    int gpsWidth = gpsPlotSz.rwidth();

    double updatedView1 = (gpsHeight * 1.0) / (gpsWidth * 1.0);

    qDebug()<<"updated view: "<<updatedView1;
    //double updatedView1 = gpsHeight/gpsWidth;

    // Find the current distance (in degrees) of x/y of the gpsPlot
    double xGpsView2 = ui->gpsPlot->xAxis->range().upper;
    double xGpsView1 = ui->gpsPlot->xAxis->range().lower;
    double yGpsView2 = ui->gpsPlot->yAxis->range().upper;
    double yGpsView1 = ui->gpsPlot->yAxis->range().lower;
    double distanceX = 0, distanceY = 0, bearing;

    // Find the distance (in meters) of a 0.01 degree difference of x/y
    Gnss_calcDistanceAndBearing(yGpsView2, xGpsView1, yGpsView2, xGpsView1+0.01, &distanceX, &bearing);
    Gnss_calcDistanceAndBearing(yGpsView2, xGpsView1, yGpsView2+0.01, xGpsView1, &distanceY, &bearing);

    // Keep the boat in the center of the plot
    double yCenter = (yGpsView1 + yGpsView2)/2;
    // Determine the correct upper y range to give a 1m:1m ratio for x:y
    yGpsView1 = yGpsView2 - updatedView1 * (ui->gpsPlot->xAxis->range().upper - ui->gpsPlot->xAxis->range().lower)
            * distanceX/distanceY;
    double yRange = yGpsView2 - yGpsView1;

    // set the new range values
    ui->gpsPlot->xAxis->setRange(xGpsView1,xGpsView2);
    ui->gpsPlot->yAxis->setRange(yCenter - yRange/2.0, yCenter + yRange/2.0);

    ChartWindowInstance->plot_replot();
}

/**************** XTF Converter Dialog ****************************************/

//opens the converter dialog window
void MainWindow :: on_actionConverter_triggered(){
    converterDialogInstance->setWindowTitle(" XTF Converter");
    converterDialogInstance->setWindowFlag(Qt::WindowContextHelpButtonHint,false);
    converterDialogInstance->show();
}

void MainWindow :: xtfConverter_changeDataFilePath(QList<QString>browserPath, QList<QString>saveBrowser){

    xtfList.clear();
    jasList.clear();
    //saves the file names passed fom the converter window as QLists
    //the qlists are global
    for(int i=0;i<browserPath.size();i++){
        jasList.append(browserPath.at(i));
        xtfList.append(saveBrowser.at(i));
    }
}

void MainWindow :: xtfConverter_FileCounter(int num){
    //sets the counter to read basically the file size eg num=4
    fileCounter = num;
}

bool MainWindow :: xtfConverter_DialogSelectFile(QString browserPath, QString saveBrowser){
    //set the global variable so that can be used as an argument for the converterFileName function.
    //goal for this was when files finish converting they a replaced as .xtf files with a check mark on the QListWidget

    //close files if open
    recorder.sonarFile.close();
    int readFileBytes = 0;

    readFileTimer.stop();

    //decrement counter inorder to read next file eg 4->3->2->1-0, in descending order
    fileCounter--;

    QFile dataFile(browserPath);

    //read the initial ping, so the percentage on progress bar is 0.
    emit converterDialogProgressBar(0);

    player.fileSize = dataFile.size();
    dataFile.open(QIODevice::ReadWrite);
    player.fileBuffer = dataFile.readAll();

    if (player.fileBuffer.data() == nullptr) //if no file selected
        return 0;
    char *data_pointer = player.fileBuffer.data();//set data pointer to beginning of file

    track.sampleNum_port = 300;
    //header data
    quint32 syncBytes, packetID, dataSize;
    //For the very first ping
    //Packet Header
    data_pointer += data_copy(&syncBytes, data_pointer, 4, &readFileBytes);   //Sync Word 1&2
    data_pointer += data_copy(&packetID, data_pointer, 4, &readFileBytes);    //Message Type is 0 for the very first ping
    data_pointer += data_copy(&dataSize, data_pointer, 4, &readFileBytes);    //Packet Size

    QThreadPool pool;
    QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess,
                                                                data_pointer, packetID, &readFileBytes); //Runs processDataToPlot in a thread
    while (!processDataThreadProgress.isFinished()) //During the function's execution
    {
        if (flag.thread_middlePoint == true) //When it reaches this point
        {
            processDataThreadProgress.togglePaused(); //Pause the function
            flag.thread_middlePoint = false;
            break;
        }
    }

    if (ping->highFrequency) {
        flag.PlotHfSidescan = 1;
        flag.useDualFrequency = 1;
    } else {
        flag.PlotHfSidescan = 0;
        flag.useDualFrequency = 0;
    }
    if (ping->recieveDepth) {
        flag.PlotAngleScan = 1;
        flag.convertToXtf = false;
    } else {
        flag.PlotAngleScan = 0;
        flag.convertToXtf=true;
    }

    if (ping->Oct31ASTest) {
        flag.PlotAngleScan = 1;
        flag.PlotHfSidescan = 0;
        flag.useDualFrequency = 1;
        flag.convertToXtf=false;
    }

    flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue
    processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data
    data_hashMap_update();

    player.dataPtr = player.fileBuffer.data();
    player.filePos = readFileBytes;


    //message when angle scan file is opened
    //flag.convertToXtf = 1;
    if (flag.convertToXtf == 0){
        int ret = QMessageBox::critical(this, tr("Conversion Error"), tr("The file selected cannot be converted to XTF"), QMessageBox::Ok);

        switch(ret){
        case QMessageBox::Ok:
            emit angleFileDetected(true);
            break;
        }
        return 0;
    }

    //sets the name of the xtf file with the new filepath
    QString xtfPath = saveBrowser;

    if (QFileInfo(browserPath).suffix().toUpper() == "JAS")
        xtfPath = QFileInfo(saveBrowser).filePath().replace(".JAS", ".XTF");
//    else

    emit converterFileName(xtfPath);
    emit fileNameToXtfConverterWindow(xtfPath);
    recorder.fileName = xtfPath;
    recorder.sonarFile.setFileName(xtfPath);
    recorder.sonarFile.open(QIODevice::ReadWrite);

    xtf_writeFileHeader();
    emit progressBarRange(dataFile.size());
    return flag.convertToXtf;
}

bool MainWindow :: xtfConverter_SortFileData(){
    quint32 check;
    quint32 packetID, syncword, packetsize;

    int readFileBytes = 0;
    memcpy(&check, player.dataPtr, 4);//checks for footer value used to stop playback at end of file

    if (check == 0xFAAF && !player.typeXtf) {//if footer detected on .jss file, end playback

        emit converterDialogProgressBar(player.fileBuffer.size());
        flag.conversionFinished = 1;

        //continue reading the next file until the last file is read, close the current file and stop timer
        if(fileCounter!=0){
            //if not last file continue to the next filerecorder.sonarFile.write(recorder.buffer);//write rec buffer to file
            recorder.buffer.clear();//clear buffer to save memory
            recorder.sonarFile.close();
            flag.convertToXtf = false;
            emit isConversionOver(false);
            sortFileTimer.stop();
            xtfConverter_StartBtnClicked(true);

        }else{
            //when the last file is read,stop the timer, close the file and clear the list
            sortFileTimer.stop();
            recorder.sonarFile.write(recorder.buffer);//write rec buffer to file
            recorder.buffer.clear();//clear buffer to save memory
            recorder.sonarFile.close();
            flag.convertToXtf = false;
            emit isConversionOver(true);
            fileConverterIndex = 0;
            xtfList.clear();
            jasList.clear();

        }
        return 1;
    }

    //sync word in packet header
    player.dataPtr += data_copy(&syncword, player.dataPtr, 4, &readFileBytes);      //Sync Word 1&2
    player.dataPtr += data_copy(&packetID, player.dataPtr, 4, &readFileBytes);
    player.dataPtr += data_copy(&packetsize, player.dataPtr, 4, &readFileBytes);

    if (packetID > 0 && packetID < 20) {//packetID = 3 represents the presence of a additional depth sounder on the board
        ping->recieveDepth = true;
    } else {
        ping->recieveDepth = false;
    }

    QThreadPool pool;
    QFuture<void> processDataThreadProgress = QtConcurrent::run(&pool, this, &MainWindow::Jas_dataProcess, player.dataPtr, packetID, &readFileBytes); //Runs processDataToPlot in a thread
    while (!processDataThreadProgress.isFinished()) //During the function's execution
    {
        if (flag.thread_middlePoint == true) //When it reaches this point
        {
            processDataThreadProgress.togglePaused(); //Pause the function
            flag.thread_middlePoint = false;
            break;
        }
    }

    //While the function is paused, do the code below
    if(ping->rangeLf_tmp != ping->rangeLf_int) {//if range recieved from the board is not the current GUI range, set GUI range to board range
        ping->rangeLf_int = ping->rangeLf_tmp;
        //setRecordedRange_low(currentRange_low);
        //setRecordedRange(rangeLf_int);
        if (ping->rangeLf_tmp >= 180 && flag.useDualFrequency) {//this is a small hack, fix the issue so we don't need this anymore
            // The GUI expects the HF ranges to be able to go above 180, however at ranges above 180 the board still sends over 180 as the range value for HF
            // * This if statement detects if the board range has been changed to a range above or equal to 180 and then causes the below if statement to be triggered
            // * This causes the GUI to act as if the HF range has been changed and add a new 180 range colormap, avoiding an out of bounds error
            //  * This is really a hack and should be changed to a more elegant solution when we have more time
            flag.highRangeChange = true;
        }
    }
    if(ping->rangeHf_tmp != ping->rangeHf_int || flag.highRangeChange) {//same as above
        ping->rangeHf_int = ping->rangeHf_tmp;
        //            setRecordedRange(currentRange_high,"high");
        if (flag.highRangeChange)
            flag.highRangeChange = false;
    }

    flag.thread_rangeDone = true; //Tells the thread the code is done, so it can continue

    processDataThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on to plot the data
    data_hashMap_update();

    player.dataPtr += readFileBytes - 12;
    player.filePos += readFileBytes;

    xtf_writePing();

    emit updateConvertedFileSize(player.filePos);
    emit converterDialogProgressBar(player.filePos);

    return 0;
}

void MainWindow :: xtfConverter_StartBtnClicked(bool show){

    if(show){

        ping->recieveDepth =0;
        flag.useDualFrequency=0;
        ping->Oct31ASTest=0;

        //the file names are passed from the converter window and stored in qlists on the main window
        //each file name is passed as an argument to the converterDialogSelectFile
        flag.conversionFinished = xtfConverter_DialogSelectFile(jasList.at(fileConverterIndex), xtfList.at(fileConverterIndex));
        //increment index inorder to read next file name from the qlists
        fileConverterIndex++;

        //if .xtf file created write to it
        if (flag.conversionFinished == 1) {
            flag.conversionFinished = 0;
            //start the timer when reading new file name and stop the timer in the convertSortFileData
            sortFileTimer.start(0);
        }
        else{
            return;
        }
    }
}

void MainWindow :: xtfConverter_CancelBtnClicked(bool show){
    //stops the timer when the cancel button is hit and deletes the file being converted
    if(show){
        sortFileTimer.stop();
        //removes the file created and sets progress bar to zero
        recorder.sonarFile.remove();
        emit converterDialogProgressBar(0);
    }
}

/**************** ENC Files ****************/

int  MainWindow :: Enc_ReadString (char *variable, char *data_pointer, int *NumChar2) {
    int NumChar = 0;
    char *pointer2 = data_pointer;
    char checkTerminator;
    do {
        memcpy(&checkTerminator, pointer2, 1);
        pointer2++;
        NumChar++;
    } while ((checkTerminator & 0x1E) != 0x1E);
    char *Array;
    char nullChar = '\0';
    if (NumChar == 1) {
        memcpy(variable, &nullChar, 1);
    } else {
        memcpy(variable, data_pointer, NumChar-1);
    }
    //    strcat(Array, &nullChar);
    //    memcpy(&Array[NumChar], &nullChar, NumChar);

    //    memcpy(&variable, &Array, NumChar);

    //    variable = QString(Array);
    *NumChar2 += NumChar - 1;
    return NumChar - 1;
}

int  MainWindow :: Enc_ReadString2 (char *variable, char *data_pointer, int *NumChar2) {
    int NumChar = 0;
    char *pointer2 = data_pointer;
    char checkTerminator;
    do {
        memcpy(&checkTerminator, pointer2, 1);
        pointer2++;
        NumChar++;
    } while ((checkTerminator & 0x1F) != 0x1F);
    char *Array;
    char nullChar = '\0';
    //    if (NumChar == 1) {
    //        memcpy(variable, &nullChar, 1);
    //    } else {
    memcpy(variable, data_pointer, NumChar);
    //    }
    //    strcat(Array, &nullChar);
    //    memcpy(&Array[NumChar], &nullChar, NumChar);

    //    memcpy(&variable, &Array, NumChar);

    //    variable = QString(Array);
    *NumChar2 = NumChar;
    return NumChar;
}

int  MainWindow :: Enc_ReadDataRecord (char *data_pointer, int BytesPointer) {
    int checkFT = 0;
    int sizeFieldLength = 0;
    int sizeFieldPosition = 0;
    int sizeFieldTag = 0;
    DR_LEADER DrLeader;

    // Read the DR Leader
    data_pointer += data_copy(&DrLeader.Record_Length,          data_pointer, 5, &BytesPointer);
    data_pointer += data_copy(&DrLeader.Indentifiers,           data_pointer, 7, &BytesPointer);
    data_pointer += data_copy(&DrLeader.FieldAreaBaseAddress,   data_pointer, 5, &BytesPointer);
    data_pointer += data_copy(&DrLeader.Indicator,              data_pointer, 3, &BytesPointer);
    data_pointer += data_copy(&DrLeader.sizeFieldLength,        data_pointer, 1, &BytesPointer);
    data_pointer += data_copy(&DrLeader.sizeFieldPosition,      data_pointer, 1, &BytesPointer);
    data_pointer += data_copy(&DrLeader.Reserved,               data_pointer, 1, &BytesPointer);
    data_pointer += data_copy(&DrLeader.sizeFieldTag,           data_pointer, 1, &BytesPointer);
    //        qDebug() << "DR Leader" << DrLeader.Record_Length << "ID" << DrLeader.Indentifiers << "Addr" << DrLeader.FieldAreaBaseAddress <<
    //                    "ID" << DrLeader.Indicator << "Length" << DrLeader.sizeFieldLength << DrLeader.sizeFieldPosition << DrLeader.Reserved <<
    //                    DrLeader.sizeFieldTag;

    // Find parameters of each field
    sizeFieldLength =   (int) DrLeader.sizeFieldLength - 48;
    sizeFieldPosition = (int) DrLeader.sizeFieldPosition - 48;
    sizeFieldTag =      (int) DrLeader.sizeFieldTag - 48;

    // Read DR Directory
    DR_DIRECTORY DrDirectory;
    QVector <DR_DIRECTORY> DrDirectory_List;
    do {
        // Find all fields in DR entry
        data_pointer += data_copy(&DrDirectory.FieldTag,        data_pointer, sizeFieldTag, &BytesPointer);
        data_pointer += data_copy(&DrDirectory.FieldLength,     data_pointer, sizeFieldLength, &BytesPointer);
        data_pointer += data_copy(&DrDirectory.FieldPosition,   data_pointer, sizeFieldPosition, &BytesPointer);

        // Save the DR directory info
        DrDirectory_List.append(DrDirectory);
        //            qDebug() << "DR Directory:" << DrDirectory.FieldTag <<  DrDirectory.FieldLength << DrDirectory.FieldPosition;
        memcpy(&checkFT, data_pointer, 1);
    } while (checkFT != 0x1E);
    data_pointer += data_copy(&checkFT, data_pointer, 1, &BytesPointer);

    // Read DR Field Area
    for (int i = 0; i < DrDirectory_List.length(); i++) {
        char FieldArea[2000] = {'\0'};
        QVector<QString> DrDataEntries;
        int ReadBytes = 0;
        int dummyCounter = 0;

        // Find the number of bytes to read of the field area
        int NumBytes = QString(DrDirectory_List[i].FieldLength).toInt();

        do {
            // Limit the number of bytes read
            if (NumBytes < 1400)    data_copy(&FieldArea, data_pointer, NumBytes, &dummyCounter);
            else                    data_copy(&FieldArea, data_pointer, 1500, &dummyCounter);
            ReadBytes = NumBytes;   // Set number of bytes left to read
            /*DrDataEntries = */Enc_ReadDescriptor(DrDataEntries, DrDirectory_List, /*DdrFields_List,*/ i, FieldArea, &ReadBytes);
            NumBytes -= ReadBytes;  // Deduct the Number of Bytes read from the function
            data_pointer += ReadBytes;  // Increment char pointer
            BytesPointer += ReadBytes;  // Increment int pointer
        } while (NumBytes > 0);

        // Read data in descriptor and save
        DrDirectory_List[i].Data = DrDataEntries;
    }

    //    qDebug() << "DR Directory:" << DrDirectory_List[0].Data.FieldAreaEntries[0] << "Record ID:"
    //             << DrDirectory_List[1].Data.FieldAreaEntries[1] << DrDirectory_List[1].FieldTag << "Last Subfield"
    //             << DrDirectory_List[DrDirectory_List.length()-1].FieldTag <<  DrDirectory_List[DrDirectory_List.length()-1].FieldLength
    //             << DrDirectory_List[DrDirectory_List.length()-1].Data.FieldArea << DrDirectory_List[DrDirectory_List.length()-2].FieldTag
    //             << BytesPointer;

    DrEntry_List.append(DrDirectory_List);
    return BytesPointer;
}

int  MainWindow :: Enc_ReadDdr (char *data_pointer, int BytesPointer) {
    int BytesPointer2 = 0;
    DDR_LEADER DdrLeader;

    // Read the DDR Leader
    data_pointer += data_copy(&DdrLeader.Record_Length,          data_pointer, 5, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.Indentifiers,           data_pointer, 7, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.FieldAreaBaseAddress,   data_pointer, 5, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.Indicator,              data_pointer, 3, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.sizeFieldLength,        data_pointer, 1, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.sizeFieldPosition,      data_pointer, 1, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.Reserved,               data_pointer, 1, &BytesPointer);
    data_pointer += data_copy(&DdrLeader.sizeFieldTag,           data_pointer, 1, &BytesPointer);

    qDebug() << "DDR Leader" << DdrLeader.Record_Length << "ID" << DdrLeader.Indentifiers << "Addr" << DdrLeader.FieldAreaBaseAddress <<
                "ID" << DdrLeader.Indicator << "Length" << DdrLeader.sizeFieldLength << DdrLeader.sizeFieldPosition << DdrLeader.Reserved <<
                DdrLeader.sizeFieldTag;

    // Find parameters of DDR fields
    int checkFT = 0;
    int sizeFieldLength =   (int) DdrLeader.sizeFieldLength - 48;
    int sizeFieldPosition = (int) DdrLeader.sizeFieldPosition - 48;
    int sizeFieldTag =      (int) DdrLeader.sizeFieldTag - 48;

    // Read the DDR Directory
    DDR_DIRECTORY DdrDirectory;
    do {
        // Read all DDR Fields
        data_pointer += data_copy(&DdrDirectory.FieldTag,        data_pointer, sizeFieldTag, &BytesPointer);
        data_pointer += data_copy(&DdrDirectory.FieldLength,     data_pointer, sizeFieldLength, &BytesPointer);
        data_pointer += data_copy(&DdrDirectory.FieldPosition,   data_pointer, sizeFieldPosition, &BytesPointer);

        // Save all DDR Fields
        DdrFields_List.append(DdrDirectory);
        qDebug() << "Directory:" << DdrDirectory.FieldTag <<  DdrDirectory.FieldLength << DdrDirectory.FieldPosition;
        memcpy(&checkFT, data_pointer, 1);
    } while (checkFT != 0x1E);
    data_pointer += data_copy(&checkFT, data_pointer, 1, &BytesPointer);

    // Read DDR Field Control
    DDR_FIELD_CONTROL DdrFieldControl;
    QList <DDR_FIELD_CONTROL> DdrFieldCtrl_List;
    data_pointer += data_copy(&DdrFieldControl.Controls, data_pointer, 9, &BytesPointer);
    data_pointer += data_copy(&DdrFieldControl.UT,       data_pointer, 1, &BytesPointer);
    qDebug() << "Field Control:" << DdrFieldControl.Controls <<  DdrFieldControl.UT;

    // Read Field Pairs and link field pairs to the DDR Field List
    for (int i = 2; i < DdrFields_List.length(); i++) {
        data_pointer += data_copy(&DdrFieldControl.FieldTagPair1,   data_pointer, 4, &BytesPointer);
        data_pointer += data_copy(&DdrFieldControl.FieldTagPair2,   data_pointer, 4, &BytesPointer);

        // Link the DDR field to its parent field name
        DdrFields_List[i].parent = QString::fromLatin1(DdrFieldControl.FieldTagPair1);

        qDebug() << "FieldTagPairs:" << DdrFieldControl.FieldTagPair1 << DdrFields_List[i].parent << DdrFields_List[i].FieldTag << DdrFieldControl.FieldTagPair2;
        memcpy(&checkFT, data_pointer, 1);
    }
    // Read the field terminator
    data_pointer += data_copy(&DdrFieldControl.FT,       data_pointer, 1, &BytesPointer);

    // Read DDR Descriptive Area
    for (int i = 1; i < DdrFields_List.length(); i++) {
        // For each field tag, go through and findeach subfield name and format
        DDR_DATA_DESCRIPTION DdrDataDesc;

        data_pointer += data_copy(&DdrDataDesc.DataStructure,        data_pointer, 1, &BytesPointer);
        data_pointer += data_copy(&DdrDataDesc.DataType,             data_pointer, 1, &BytesPointer);
        data_pointer += data_copy(&DdrDataDesc.AuxiliaryControls,    data_pointer, 2, &BytesPointer);
        data_pointer += data_copy(&DdrDataDesc.PrintableGraphics,    data_pointer, 2, &BytesPointer);
        data_pointer += data_copy(&DdrDataDesc.EscapeSequence,       data_pointer, 1, &BytesPointer);
        BytesPointer2 = 0;
        data_pointer += Enc_ReadString(DdrDataDesc.FieldName,           data_pointer, &BytesPointer2);
        DdrDataDesc.FieldName[BytesPointer2] = '\0';
        BytesPointer += BytesPointer2;
        data_pointer += data_copy(&DdrDataDesc.UT1,                  data_pointer, 1, &BytesPointer);
        BytesPointer2 = 0;
        data_pointer += Enc_ReadString(DdrDataDesc.ArrayDescriptor,     data_pointer, &BytesPointer2);
        DdrDataDesc.ArrayDescriptor[BytesPointer2] = '\0';
        BytesPointer += BytesPointer2;
        data_pointer += data_copy(&DdrDataDesc.UT2,                  data_pointer, 1, &BytesPointer);
        BytesPointer2 = 0;
        data_pointer += Enc_ReadString(DdrDataDesc.FormatControls,      data_pointer, &BytesPointer2);
        DdrDataDesc.FormatControls[BytesPointer2] = '\0';
        BytesPointer += BytesPointer2;
        data_pointer += data_copy(&DdrDataDesc.FT,                   data_pointer, 1, &BytesPointer);
        qDebug() << "DdrDataDesc:" << DdrDataDesc.DataStructure << DdrDataDesc.DataType << DdrDataDesc.AuxiliaryControls <<
                    DdrDataDesc.PrintableGraphics << DdrDataDesc.EscapeSequence << DdrDataDesc.FieldName << DdrDataDesc.UT1 <<
                    DdrDataDesc.ArrayDescriptor << DdrDataDesc.UT2 << DdrDataDesc.FormatControls << DdrDataDesc.FT;

        // Split each array descriptor into seperate descriptors
        DdrDataDesc.ArrayDesc = QString::fromLatin1(DdrDataDesc.ArrayDescriptor).split("!");

        // Split FormatControls to find length of each array descriptor (subfield)
        DdrDataDesc.FormatChar = QString::fromLatin1(DdrDataDesc.FormatControls).split(",");
        for (int j = DdrDataDesc.FormatChar.length() - 1; j >= 0; j--) {
            if (j == 0) {
                // remove first bracket
                QString tmp = DdrDataDesc.FormatChar.at(0);
                tmp.remove(0,1);
                DdrDataDesc.FormatChar.replace(0, tmp);
            }
            if (j == DdrDataDesc.FormatChar.length() - 1) {
                // Remove end bracket
                QString tmp = DdrDataDesc.FormatChar.at(j).mid(0, DdrDataDesc.FormatChar.at(j).size() - 1);// .split(")");
                DdrDataDesc.FormatChar.replace(j, tmp);
            }
            if (DdrDataDesc.FormatChar.at(j).mid(0,1) < 58 && DdrDataDesc.FormatChar.at(j).mid(0,1) > 48) {
                // if the first character is a number, then repeat the binary number n times
                int RepeatChar = DdrDataDesc.FormatChar.at(j).mid(0,1).toInt();
                QString InsertChar = DdrDataDesc.FormatChar.at(j).mid(1);
                DdrDataDesc.FormatChar.removeAt(j);
                for (int k = 0; k < RepeatChar; k++) {
                    DdrDataDesc.FormatChar.insert(j, InsertChar);
                }
            }
        }

        // Save the DDR Data to the field object
        DdrFields_List[i].Data = DdrDataDesc;
    }
    return BytesPointer;
}

void MainWindow :: Enc_CreateSpatialObjectList() {
    // TO DO: Try to make this function simplier
    int counter = 0;
    int iLast = 0;
    double COMF, SOMF;
    double xCoord, yCoord, zCoord;
    QString PointsStr;

    // Read X/Y cordinates from all SG2D fields
    for (int k = 0; k < DrEntry_List.length(); k++) {
        QStringList Points_List;
        iLast = DrEntry_List[k].length() - 1;
        if (QString::fromLatin1(DrEntry_List[k][iLast-1].FieldTag, 4) == "VRPT" && QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4) == "SG2D") {
            // Find all VRPT entries that also contain a list of coordinates
            SPATIAL_FEATURES FeatureEntry;
            FeatureEntry.TagName = QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4); // SG2D
            FeatureEntry.parent1 = QString::fromLatin1(DrEntry_List[k][iLast-1].FieldTag, 4); // VRPT
            FeatureEntry.parent2 = QString::fromLatin1(DrEntry_List[k][iLast-2].FieldTag, 4); // VRID / ATTV

            FeatureEntry.TagPointer = DrEntry_List[k][1].Data[0] + DrEntry_List[k][1].Data[1]; // In VRID field

            // Add start and end nodes from the VRPT entry
            if (DrEntry_List[k][iLast-1].Data.length() > 7) {
                FeatureEntry.VrptTags1 = DrEntry_List[k][iLast-1].Data[1];
                FeatureEntry.VrptTags1B = DrEntry_List[k][iLast-1].Data[2];
                FeatureEntry.VrptTags2 = DrEntry_List[k][iLast-1].Data[6];
                FeatureEntry.VrptTags2B = DrEntry_List[k][iLast-1].Data[7];
                FeatureEntry.Node1Dtn = DrEntry_List[k][iLast-1].Data[3];
                FeatureEntry.Node2Dtn = DrEntry_List[k][iLast-1].Data[8];
                FeatureEntry.Node1Ptr = DrEntry_List[k][iLast-1].Data[0];
                FeatureEntry.Node2Ptr = DrEntry_List[k][iLast-1].Data[5];
                if (FeatureEntry.Node1Dtn == 1)         FeatureEntry.StartNodePointer = DrEntry_List[k][iLast-1].Data[0];
                else if (FeatureEntry.Node2Dtn == 1)    FeatureEntry.StartNodePointer = DrEntry_List[k][iLast-1].Data[5];
                if (FeatureEntry.Node1Dtn == 2)         FeatureEntry.EndNodePointer = DrEntry_List[k][iLast-1].Data[0];
                else if (FeatureEntry.Node2Dtn == 2)    FeatureEntry.EndNodePointer = DrEntry_List[k][iLast-1].Data[5];
            }

            // Copy all 2D points from field area
            FeatureEntry.Points = DrEntry_List[k][iLast].Data;

            for (int i = 0; i < FeatureEntry.Points.length() / 2; i++) {
                PointsStr = QString::number(Enc_convertToInt(FeatureEntry.Points[2*i])) + ", " +
                        QString::number(Enc_convertToInt(FeatureEntry.Points[2*i + 1]));
                Points_List.append(PointsStr);
            }
            qDebug() << "VRPT / SG2D" << FeatureEntry.TagPointer << Feature_List.length() << DrEntry_List[k][2].Data.length();
            if (Enc_convertToInt(DrEntry_List[k][1].Data[0]) == 140) {
                qDebug() << "Face: " << k << Feature_List.length();
            }

            // Add to the spatial feature list
            Feature_List.append(FeatureEntry);
        }
        else if (QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4) == "FSPT") {
            // Read the FSPT feature records
            FEATURE_RECORD Record;
            Record.ObjectCode = Enc_convertToInt(DrEntry_List[k][1].Data[4]);
            if (Record.ObjectCode == 17)
                qDebug() << "Here";

            Record.TagName = QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4);
            Record.parent1 = QString::fromLatin1(DrEntry_List[k][iLast-1].FieldTag, 4);
            Record.parent2 = QString::fromLatin1(DrEntry_List[k][iLast-2].FieldTag, 4);
            Record.TagPointer = DrEntry_List[k][1].Data[0] + DrEntry_List[k][1].Data[1];
            Record.Primitive = Enc_convertToInt(DrEntry_List[k][1].Data[2]);
            if (Record.parent1 == "NATF" || Record.parent1 == "ATTF") // Save Attributes to record
                Record.AttributeList = DrEntry_List[k][iLast-1].Data;
            for (int l = 0; l < Record.AttributeList.length(); l+=2) {
                if ((Enc_convertToInt(Record.AttributeList[l]) == 123 || Enc_convertToInt(Record.AttributeList[l]) == 113) && Record.ObjectCode != 121) {
                    qDebug() << "Sediment";
                }
            }

            for (int l = 0; l < DrEntry_List[k][iLast].Data.length()/4; l++) {
                Record.Pointers.append(DrEntry_List[k][iLast].Data[l*4]);
                Record.AreaFR.append(Enc_convertToInt(DrEntry_List[k][iLast].Data[l*4+1]));
                Record.AreaEI.append(Enc_convertToInt(DrEntry_List[k][iLast].Data[l*4+2]));
            }
            Record_List.append(Record);
        }
        else if (QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4) == "SG3D") {
            // Find all sounding points
            SG3D_RECORD FeatureEntry;
            FeatureEntry.TagName = QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4); // SG3D
            FeatureEntry.parent1 = QString::fromLatin1(DrEntry_List[k][iLast-1].FieldTag, 4); // VRID
            FeatureEntry.parent2 = QString::fromLatin1(DrEntry_List[k][iLast-2].FieldTag, 4); // 0001

            FeatureEntry.TagPointer = DrEntry_List[k][1].Data[0] + DrEntry_List[k][1].Data[1];

            for (int i = 0; i < DrEntry_List[k][iLast].Data.length() / 3; i++) {
                FeatureEntry.X.append(Enc_convertToInt(DrEntry_List[k][iLast].Data[3*i+1]));
                FeatureEntry.Y.append(Enc_convertToInt(DrEntry_List[k][iLast].Data[3*i]));
                FeatureEntry.Z.append(Enc_convertToInt(DrEntry_List[k][iLast].Data[3*i+2]));
            }

            if (Enc_convertToInt(DrEntry_List[k][1].Data[0]) == 140) {
                qDebug() << "Face: " << k;
            }

            // Add to the spatial feature list
            SG3D_List.append(FeatureEntry);
        }
        else if (QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4) == "SG2D") {
            // Find all entries that only have 2D points
            SPATIAL_FEATURES FeatureEntry;
            FeatureEntry.TagName = QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4); // SG2D
            FeatureEntry.parent1 = QString::fromLatin1(DrEntry_List[k][iLast-1].FieldTag, 4); // VRID
            FeatureEntry.parent2 = QString::fromLatin1(DrEntry_List[k][iLast-2].FieldTag, 4); // 0001

            FeatureEntry.TagPointer = DrEntry_List[k][1].Data[0] + DrEntry_List[k][1].Data[1];

            // Copy all 2D points from field area
            FeatureEntry.Points = DrEntry_List[k][iLast].Data;
            qDebug() << "SG2D" << FeatureEntry.TagPointer << Feature_List.length() << DrEntry_List[k][2].Data.length();

            if (Enc_convertToInt(DrEntry_List[k][1].Data[0]) == 140) {
                qDebug() << "Face: " << k << Feature_List.length();
            }

            // Add to the spatial feature list
            SG2D_List.append(FeatureEntry);
        }
        else if (QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4) == "VRPT") {
            // Find all VRPT entries that do not contain additional SG2D points
            SPATIAL_FEATURES FeatureEntry;
            FeatureEntry.TagName = QString::fromLatin1(DrEntry_List[k][iLast].FieldTag, 4); // VRPT
            FeatureEntry.parent1 = QString::fromLatin1(DrEntry_List[k][iLast-1].FieldTag, 4); // VRID / ATTV
            FeatureEntry.parent2 = QString::fromLatin1(DrEntry_List[k][iLast-2].FieldTag, 4); // 0001 / VRID

            FeatureEntry.TagPointer = DrEntry_List[k][1].Data[0] + DrEntry_List[k][1].Data[1];

            // Add start and end nodes from the VRPT entry
            if (DrEntry_List[k][iLast].Data.length() > 7) {
                FeatureEntry.VrptTags1 = DrEntry_List[k][iLast].Data[1];
                FeatureEntry.VrptTags1B = DrEntry_List[k][iLast].Data[2];
                FeatureEntry.VrptTags2 = DrEntry_List[k][iLast].Data[6];
                FeatureEntry.VrptTags2B = DrEntry_List[k][iLast].Data[7];
                FeatureEntry.Node1Dtn = DrEntry_List[k][iLast].Data[3];
                FeatureEntry.Node2Dtn = DrEntry_List[k][iLast].Data[8];
                FeatureEntry.Node1Ptr = DrEntry_List[k][iLast].Data[0];
                FeatureEntry.Node2Ptr = DrEntry_List[k][iLast].Data[5];
                if (FeatureEntry.Node1Dtn == 1)         FeatureEntry.StartNodePointer = DrEntry_List[k][iLast].Data[0];
                else if (FeatureEntry.Node2Dtn == 1)    FeatureEntry.StartNodePointer = DrEntry_List[k][iLast].Data[5];
                if (FeatureEntry.Node1Dtn == 2)         FeatureEntry.EndNodePointer = DrEntry_List[k][iLast].Data[0];
                else if (FeatureEntry.Node2Dtn == 2)    FeatureEntry.EndNodePointer = DrEntry_List[k][iLast].Data[5];
            }
            qDebug() << "VRPT " << FeatureEntry.TagPointer << Feature_List.length() << DrEntry_List[k][iLast].Data.length();
            if (Enc_convertToInt(DrEntry_List[k][1].Data[0]) == 140) {
                qDebug() << "Face: " << k << Feature_List.length();
            }

            // Add to the spatial feature list
            Feature_List.append(FeatureEntry);
        }
    }
}

void MainWindow :: Enc_FindPlotRange(double *xEncMin, double *xEncMax, double *yEncMin, double *yEncMax) {
    *xEncMin = SG3D_List[0].X[0] / COMF;
    *xEncMax = SG3D_List[0].X[0] / COMF;
    *yEncMin = SG3D_List[0].Y[0] / COMF;
    *yEncMax = SG3D_List[0].Y[0] / COMF;

    double xCoord, yCoord;

    for (int i = 0; i < Feature_List.length(); i++) {
        if (Feature_List[i].Points.length() > 2 && Feature_List[i].TagName == "SG2D") {
            for (int j = 0; j < Feature_List[i].Points.length()/2; j++) {
                xCoord = Enc_convertToInt(Feature_List[i].Points[2*j+1]) / COMF;
                yCoord = Enc_convertToInt(Feature_List[i].Points[2*j]) / COMF;
                if (xCoord < *xEncMin)          *xEncMin = xCoord;
                else if (xCoord > *xEncMax)     *xEncMax = xCoord;
                if (yCoord < *yEncMin)          *yEncMin = yCoord;
                else if (yCoord > *yEncMax)     *yEncMax = yCoord;
            }
        }
    }
}

void MainWindow :: Enc_PlotRecordObjects() {
    double xCoord, yCoord, zCoord;

    // Go through each item in the record list to sort data into the spatial features list
    for (int i = 0; i < Record_List.length(); i++) {
        FEATURE_POINTS FeatureItem;
        // Creck Object Code to determine how to plot data
        int pointerCode = Record_List[i].ObjectCode;

        // Lines and Areas are converted to the feature point objects
        // Points are converted to Single Point Objects
        if (Record_List[i].Primitive == 1)    {
            if (pointerCode == 14 || pointerCode == 15 || pointerCode == 16 || pointerCode == 17 || pointerCode == 18 || pointerCode == 19) {}
            else    continue;

            double x = 0, y = 0;

            // Search SG2D_List to save the coordinates to the singlePointObjects list
            for (int j = 0; j < SG2D_List.length(); j++) {
                if (SG2D_List[j].TagName != "SG2D")  continue;
                if (SG2D_List[j].TagPointer != Record_List[i].Pointers[0])   continue;

                x = Enc_convertToInt(SG2D_List[j].Points[1]) / COMF;  // XCOO
                y = Enc_convertToInt(SG2D_List[j].Points[0]) / COMF;  // XCOO
            }
            OBJECT_POINT object;
            object.xCoord = x;
            object.yCoord = y;
            for (int l = 0; l < Record_List[i].AttributeList.length(); l+=2) {
                if (Record_List[i].AttributeList.length() % 2 != 0)
                    break;
                QString AttributeVal = Record_List[i].AttributeList[l+1];
                object.AttributeCodes.append(Enc_convertToInt(Record_List[i].AttributeList[l]));
                object.AttributeValues.append(AttributeVal);
            }
            object.ObjectCode = pointerCode;
            SinglePointObjects_List.append(object);
            continue;
        }
        else if (Record_List[i].Primitive < 2)      continue;

        int lineType = 0;
        if      (pointerCode == 12)        lineType = 2;   // Building - BUISGL
        else if (pointerCode == 13)        lineType = 2;   // Building Area - BUAARE
        else if (pointerCode == 42)        lineType = 8;   // Depth Area - DEPARE
        else if (pointerCode == 43)        lineType = 3;   // Water Contour - DEPCNT
        else if (pointerCode == 46)        lineType = 8;   // Dredged Area - DRGARE
        else if (pointerCode == 69)        lineType = 10;   // Lake - LAKARE
        else if (pointerCode == 71)        lineType = 4;   // Land Area - LNDARE
        else if (pointerCode == 119)       lineType = 6;   // Sea Area - SEAARE
        else if (pointerCode == 122)       lineType = 4;   // Shoreline construction - SLCONS
        else if (pointerCode == 302)       lineType = 1;   // Coverage/Border - M_COVR

        int counter = 0;
        QString firstPointer;
        QString Pointer2;

//        if (pointerCode == 42) {
            // Search attributes to copy water depth
            for (int l = 0; l < Record_List[i].AttributeList.length(); l+=2) {
                int AttributeCode = Enc_convertToInt(Record_List[i].AttributeList[l]);
                if (Record_List[i].AttributeList.length() % 2 != 0)
                    break;
                QString AttributeVal = Record_List[i].AttributeList[l+1];
                FeatureItem.AttributeCodes.append(Enc_convertToInt(Record_List[i].AttributeList[l]));
                FeatureItem.AttributeValues.append(AttributeVal);
                if (AttributeCode == 87 && pointerCode == 42)
                    FeatureItem.depth1 = AttributeVal.mid(0, AttributeVal.length()-1).toDouble();
                if (AttributeCode == 88 && pointerCode == 42)
                    FeatureItem.depth2 = AttributeVal.mid(0, AttributeVal.length()-1).toDouble();
            }
//        }

        // Search through the list of pointers inside the record to find points
        for (int k = 0; k < Record_List[i].Pointers.length(); k++) {
            for (int j = 0; j < vrptRecord_List.length(); j++) {
                // If the pointers do not match, continue onto the next
                if (vrptRecord_List[j].TagPointer != Record_List[i].Pointers[k])    continue;

                double Xstart = 0, Ystart = 0, Xend = 0, Yend = 0;
                bool Forward;

                // Record it as an interior or exterior boundary
                if (Record_List[i].AreaEI[k] < 3)
                    FeatureItem.Exterior = Record_List[i].AreaEI[k];
                FeatureItem.ObjectCode = pointerCode;
                if (Record_List[i].Primitive == 3)      FeatureItem.Area = 1;

                // Plot the line provided by the VRPT record
                // Sort points so data line is drawn clockwise
                if (Record_List[i].AreaFR[k] == 1) {
                    if (vrptRecord_List[j].Node1.TOPI == 1) {
                        Xstart = vrptRecord_List[j].Node1.X;
                        Ystart = vrptRecord_List[j].Node1.Y;
                        if (counter == 0) firstPointer = vrptRecord_List[j].Node1.Pointer;
                    }
                    else if (vrptRecord_List[j].Node2.TOPI == 1) {
                        Xstart = vrptRecord_List[j].Node2.X;
                        Ystart = vrptRecord_List[j].Node2.Y;
                        if (counter == 0) firstPointer = vrptRecord_List[j].Node2.Pointer;
                    }
                    if (vrptRecord_List[j].Node1.TOPI == 2) {
                        Xend = vrptRecord_List[j].Node1.X;
                        Yend = vrptRecord_List[j].Node1.Y;
                        Pointer2 = vrptRecord_List[j].Node1.Pointer;
                    }
                    else if (vrptRecord_List[j].Node2.TOPI == 2) {
                        Xend = vrptRecord_List[j].Node2.X;
                        Yend = vrptRecord_List[j].Node2.Y;
                        Pointer2 = vrptRecord_List[j].Node2.Pointer;
                    }
                }
                else if (Record_List[i].AreaFR[k] == 2) {
                    if (vrptRecord_List[j].Node1.TOPI == 2) {
                        Xstart = vrptRecord_List[j].Node1.X;
                        Ystart = vrptRecord_List[j].Node1.Y;
                        if (counter == 0) firstPointer = vrptRecord_List[j].Node1.Pointer;
                    }
                    else if (vrptRecord_List[j].Node2.TOPI == 2) {
                        Xstart = vrptRecord_List[j].Node2.X;
                        Ystart = vrptRecord_List[j].Node2.Y;
                        if (counter == 0) firstPointer = vrptRecord_List[j].Node2.Pointer;
                    }
                    if (vrptRecord_List[j].Node1.TOPI == 1) {
                        Xend = vrptRecord_List[j].Node1.X;
                        Yend = vrptRecord_List[j].Node1.Y;
                        Pointer2 = vrptRecord_List[j].Node1.Pointer;
                    }
                    else if (vrptRecord_List[j].Node2.TOPI == 1) {
                        Xend = vrptRecord_List[j].Node2.X;
                        Yend = vrptRecord_List[j].Node2.Y;
                        Pointer2 = vrptRecord_List[j].Node2.Pointer;
                    }
                }

                //ChartWindowInstance->addMapVector2(Xstart, Ystart, counter, lineType);
                counter++;

                FeatureItem.x.append(Xstart);
                FeatureItem.y.append(Ystart);

                // Draw the curve
                if (Record_List[i].AreaFR[k] == 1) {
                    for (int l = 0; l < vrptRecord_List[j].X.length(); l++) {
                        //ChartWindowInstance->addMapVector2(vrptRecord_List[j].X[l], vrptRecord_List[j].Y[l], counter, lineType);
                        FeatureItem.x.append(vrptRecord_List[j].X[l]);
                        FeatureItem.y.append(vrptRecord_List[j].Y[l]);
                        counter++;
                    }
                }
                else if (Record_List[i].AreaFR[k] == 2) {
                    for (int l = vrptRecord_List[j].X.length() - 1; l >= 0; l--) {
                        //ChartWindowInstance->addMapVector2(vrptRecord_List[j].X[l], vrptRecord_List[j].Y[l], counter, lineType);
                        FeatureItem.x.append(vrptRecord_List[j].X[l]);
                        FeatureItem.y.append(vrptRecord_List[j].Y[l]);
                        counter++;
                    }
                }

                //ChartWindowInstance->addMapVector2(Xend, Yend, counter, lineType);
                FeatureItem.x.append(Xend);
                FeatureItem.y.append(Yend);

                if (Pointer2 == firstPointer) {
                    // Make a complete loop and add to the spatial features list
                    counter = 0;
                    SpatialFeatures_List.append(FeatureItem);

                    if      (pointerCode == 12)        SpatialObjectTypes_List.BuildingArea.append(FeatureItem);   // Building - BUISGL
                    else if (pointerCode == 13)        SpatialObjectTypes_List.BuildingArea.append(FeatureItem);   // Building Area - BUAARE
//                    else if (pointerCode == 121)        SpatialObjectTypes_List.BuildingArea.append(FeatureItem);   // Building Area - BUAARE
                    else if (pointerCode == 42)        SpatialObjectTypes_List.WaterArea.append(FeatureItem);   // Depth Area - DEPARE
                    else if (pointerCode == 43)        SpatialObjectTypes_List.WaterContour.append(FeatureItem);   // Water Contour - DEPCNT
                    else if (pointerCode == 46)        lineType = 8;   // Dredged Area - DRGARE
                    else if (pointerCode == 69)        SpatialObjectTypes_List.LakeArea.append(FeatureItem);   // Lake - LAKARE
                    else if (pointerCode == 71)        SpatialObjectTypes_List.LandArea.append(FeatureItem);   // Land Area - LNDARE
                    else if (pointerCode == 119)       SpatialObjectTypes_List.SeaArea.append(FeatureItem);   // Sea Area - SEAARE
                    else if (pointerCode == 122)       SpatialObjectTypes_List.LandContour.append(FeatureItem);   // Shoreline construction - SLCONS
                    else if (pointerCode == 302)       SpatialObjectTypes_List.MapOutline.append(FeatureItem);   // Coverage/Border - M_COVR

                    FeatureItem.x.clear();
                    FeatureItem.y.clear();
                }

                break;
            }
        }

        if (FeatureItem.x.length() > 0)  {
            SpatialFeatures_List.append(FeatureItem);

            if      (pointerCode == 12)        SpatialObjectTypes_List.BuildingArea.append(FeatureItem);   // Building - BUISGL
            else if (pointerCode == 13)        SpatialObjectTypes_List.BuildingArea.append(FeatureItem);   // Building Area - BUAARE
//            else if (pointerCode == 121)        SpatialObjectTypes_List.BuildingArea.append(FeatureItem);   // Building Area - BUAARE
            else if (pointerCode == 42)        SpatialObjectTypes_List.WaterArea.append(FeatureItem);   // Depth Area - DEPARE
            else if (pointerCode == 43)        SpatialObjectTypes_List.WaterContour.append(FeatureItem);   // Water Contour - DEPCNT
            else if (pointerCode == 46)        lineType = 8;   // Dredged Area - DRGARE
            else if (pointerCode == 69)        SpatialObjectTypes_List.LakeArea.append(FeatureItem);   // Lake - LAKARE
            else if (pointerCode == 71)        SpatialObjectTypes_List.LandArea.append(FeatureItem);   // Land Area - LNDARE
            else if (pointerCode == 119)       SpatialObjectTypes_List.SeaArea.append(FeatureItem);   // Sea Area - SEAARE
            else if (pointerCode == 122)       SpatialObjectTypes_List.LandContour.append(FeatureItem);   // Shoreline construction - SLCONS
            else if (pointerCode == 302)       SpatialObjectTypes_List.MapOutline.append(FeatureItem);   // Coverage/Border - M_COVR
        }
    }
}

void MainWindow :: Enc_CreateVrptList () {
    for (int i = 0; i < Feature_List.length(); i++) {
        int counter = 0;
        if (Feature_List[i].TagName != "VRPT" && Feature_List[i].parent1 != "VRPT")     continue;

        VRPT_RECORD Record;
        Record.TagPointer = Feature_List[i].TagPointer;
        Record.Node1.Pointer = Feature_List[i].Node1Ptr;
        Record.Node1.ORNT = Enc_convertToInt(Feature_List[i].VrptTags1);
        Record.Node1.USAG = Enc_convertToInt(Feature_List[i].VrptTags1B);
        Record.Node1.TOPI = Enc_convertToInt(Feature_List[i].Node1Dtn);
        Record.Node2.Pointer = Feature_List[i].Node2Ptr;
        Record.Node2.ORNT = Enc_convertToInt(Feature_List[i].VrptTags2);
        Record.Node2.USAG = Enc_convertToInt(Feature_List[i].VrptTags2B);
        Record.Node2.TOPI = Enc_convertToInt(Feature_List[i].Node2Dtn);

        // Find the x/y coordinates of the start and end node pointers
        for (int j = 0; j < SG2D_List.length(); j++) {
            if (SG2D_List[j].TagName != "SG2D")  continue;
            if (SG2D_List[j].TagPointer != Record.Node1.Pointer && SG2D_List[j].TagPointer != Record.Node2.Pointer)   continue;

            double xCoord = Enc_convertToInt(SG2D_List[j].Points[1]) / COMF;  // XCOO
            double yCoord = Enc_convertToInt(SG2D_List[j].Points[0]) / COMF;  // XCOO

            if (SG2D_List[j].TagPointer == Record.Node1.Pointer) {
                Record.Node1.X = xCoord;
                Record.Node1.Y = yCoord;
            }
            if (SG2D_List[j].TagPointer == Record.Node2.Pointer) {
                Record.Node2.X = xCoord;
                Record.Node2.Y = yCoord;
            }

            counter++;
            if (counter > 1)        break;
        }

        // Save all x/y coordinates of SG2D points
        for (int k = 0; k < Feature_List[i].Points.length()/2; k++) {
            Record.X.append(Enc_convertToInt(Feature_List[i].Points[2*k+1]) / COMF);
            Record.Y.append(Enc_convertToInt(Feature_List[i].Points[2*k]) / COMF);
        }
        //if (Record.X.length() > 0) qDebug() << "Length > 0" << Record.X.length() << vrptRecord_List.length();

        vrptRecord_List.append(Record);
    }
}

void MainWindow :: Enc_plotFsptList () {
    for (int i = 0; i < SpatialFeatures_List.length(); i++) {
        int pointerCode = SpatialFeatures_List[i].ObjectCode;
        int lineType = 0;
        if      (pointerCode == 12)        lineType = 2;   // Building - BUISGL
        else if (pointerCode == 13)        lineType = 2;   // Building Area - BUAARE
        else if (pointerCode == 42)        lineType = 8;   // Depth Area - DEPARE
        else if (pointerCode == 43)        lineType = 3;   // Water Contour - DEPCNT
        else if (pointerCode == 46)        lineType = 8;   // Dredged Area - DRGARE
        else if (pointerCode == 69)        lineType = 10;   // Lake - LAKARE
        else if (pointerCode == 71)        lineType = 4;   // Land Area - LNDARE
        else if (pointerCode == 119)       lineType = 6;   // Sea Area - SEAARE
        else if (pointerCode == 122)       lineType = 2;   // Shoreline construction - SLCONS
        else if (pointerCode == 302)       lineType = 1;   // Coverage/Border - M_COVR

        // Plot all spatial objects from the existing list
        if (pointerCode == 42 && SpatialFeatures_List[i].depth2 < 20) {
            for (int j = 0; j < SpatialFeatures_List[i].x.length(); j++) {
                ChartWindowInstance->addMapVector2(SpatialFeatures_List[i].x[j], SpatialFeatures_List[i].y[j], j, lineType);
            }
        }

        if (SpatialFeatures_List[i].ObjectCode == 71)   SpatialObjectTypes_List.LandArea.append(SpatialFeatures_List[i]);
        if (SpatialFeatures_List[i].ObjectCode == 6)   SpatialObjectTypes_List.WaterArea.append(SpatialFeatures_List[i]);
        //ui->gpsPlot->replot();
    }

    for (int i = 0; i < SG3D_List.length(); i++) {
        for (int j = 0; j < SG3D_List[i].X.length(); j++) {
            if (SG3D_List[i].convertedToDouble == 0) {
                SG3D_List[i].X[j] = SG3D_List[i].X[j] / COMF;
                SG3D_List[i].Y[j] = SG3D_List[i].Y[j] / COMF;
                SG3D_List[i].Z[j] = SG3D_List[i].Z[j] / SOMF;
            }
            ChartWindowInstance->addSoundingPoint(SG3D_List[i].X[j], SG3D_List[i].Y[j], SG3D_List[i].Z[j]);
        }
        SG3D_List[i].convertedToDouble = 1;
    }
}

void MainWindow :: Enc_plotAllSoundPoints () {
    for (int i = 0; i < SG3D_List.length(); i++) {
        for (int j = 0; j < SG3D_List[i].X.length(); j++) {
            if (SG3D_List[i].convertedToDouble == 0) {
                SG3D_List[i].X[j] = SG3D_List[i].X[j] / COMF;
                SG3D_List[i].Y[j] = SG3D_List[i].Y[j] / COMF;
                SG3D_List[i].Z[j] = SG3D_List[i].Z[j] / SOMF;
            }
            if (SG3D_List[i].textItem.length() < j + 1)     SG3D_List[i].textItem.append(nullptr);
            SG3D_List[i].textItem[j] = ChartWindowInstance->addSoundingPoint(SG3D_List[i].X[j], SG3D_List[i].Y[j], SG3D_List[i].Z[j]);
        }
        SG3D_List[i].convertedToDouble = 1;
    }
}

void MainWindow :: Enc_plotAll2dObjects () {
    for (int i = 0; i < SpatialObjectTypes_List.SeaArea.length(); i++) {
        FEATURE_POINTS SeaObject = SpatialObjectTypes_List.SeaArea[i];
        //SpatialObjectTypes_List.SeaArea[i].curvePtr = ChartWindowInstance->addCurves(SeaObject.x, SeaObject.y, 7);
    }
    for (int i = 0; i < SpatialObjectTypes_List.WaterArea.length(); i++) {
        FEATURE_POINTS WaterObject = SpatialObjectTypes_List.WaterArea[i];
        if (WaterObject.ObjectCode == 42 && WaterObject.depth2 < 4)
            SpatialObjectTypes_List.WaterArea[i].curvePtr = ChartWindowInstance->addCurves(WaterObject.x, WaterObject.y, 9);
        else if (WaterObject.ObjectCode == 42 && WaterObject.depth2 < 10)
            SpatialObjectTypes_List.WaterArea[i].curvePtr = ChartWindowInstance->addCurves(WaterObject.x, WaterObject.y, 8);
        else
            SpatialObjectTypes_List.WaterArea[i].curvePtr = ChartWindowInstance->addCurves(WaterObject.x, WaterObject.y, 6);
    }
    for (int i = 0; i < SpatialObjectTypes_List.LandArea.length(); i++) {
        FEATURE_POINTS LandObject = SpatialObjectTypes_List.LandArea[i];
        SpatialObjectTypes_List.LandArea[i].curvePtr = ChartWindowInstance->addCurves(LandObject.x, LandObject.y, 4);
    }
    for (int i = 0; i < SpatialObjectTypes_List.BuildingArea.length(); i++) {
        FEATURE_POINTS BuildingObject = SpatialObjectTypes_List.BuildingArea[i];
        SpatialObjectTypes_List.BuildingArea[i].curvePtr = ChartWindowInstance->addCurves(BuildingObject.x, BuildingObject.y, 5);
//        for (int j = 0; j < SpatialObjectTypes_List.BuildingArea[i].AttributeCodes.length(); j++) {
//            if (SpatialObjectTypes_List.BuildingArea[i].AttributeCodes[j] == 113) {
//                QString Attribute = SpatialObjectTypes_List.BuildingArea[i].AttributeValues[j].mid(0,1);
//                if (Attribute == "1")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Mud")->setVisible(true);
//                if (Attribute == "2")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Clay")->setVisible(true);
//                if (Attribute == "3")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Silt")->setVisible(true);
//                if (Attribute == "4")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Sand")->setVisible(true);
//                if (Attribute == "5")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Stone")->setVisible(true);
//                if (Attribute == "6")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Gravel")->setVisible(true);
//                if (Attribute == "7")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Pebbles")->setVisible(true);
//                if (Attribute == "8")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Cobbles")->setVisible(true);
//                if (Attribute == "9")    ChartWindowInstance->addPointObject(SpatialObjectTypes_List.BuildingArea[i].x[0],
//                                                                            SpatialObjectTypes_List.BuildingArea[i].y[0], "Rock")->setVisible(true);
//            }
//        }
    }
    for (int i = 0; i < SpatialObjectTypes_List.LakeArea.length(); i++) {
        FEATURE_POINTS LakeObject = SpatialObjectTypes_List.LakeArea[i];
        SpatialObjectTypes_List.LakeArea[i].curvePtr = ChartWindowInstance->addCurves(LakeObject.x, LakeObject.y, 8);
    }
    for (int i = 0; i < SpatialObjectTypes_List.MapOutline.length(); i++) {
        FEATURE_POINTS MapObject = SpatialObjectTypes_List.MapOutline[i];
        SpatialObjectTypes_List.MapOutline[i].curvePtr = ChartWindowInstance->addCurves(MapObject.x, MapObject.y, 1);
    }

    for (int i = 0; i < SinglePointObjects_List.length(); i++) {
        for (int j = 0; j < SinglePointObjects_List[i].AttributeCodes.length(); j++) {
            if (SinglePointObjects_List[i].AttributeCodes[j] == 116)
                SinglePointObjects_List[i].text = ChartWindowInstance->addPointObject(SinglePointObjects_List[i].xCoord,
                              SinglePointObjects_List[i].yCoord, SinglePointObjects_List[i].AttributeValues[j]);
        }
    }
//    /* 1. Plot Outline of Water, land and map   */
//    for (int i = 0; i < SpatialObjectTypes_List.MapOutline.length(); i++) {
//        FEATURE_POINTS MapObject = SpatialObjectTypes_List.MapOutline[i];
//        for (int j = 0; j < MapObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector4(MapObject.x[j], MapObject.y[j], j, 255, 1, 0);
//        }
//    }
//    for (int i = 0; i < SpatialObjectTypes_List.LandArea.length(); i++) {
//        FEATURE_POINTS LandObject = SpatialObjectTypes_List.LandArea[i];
//        for (int j = 0; j < LandObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector4(LandObject.x[j], LandObject.y[j], j, 50, 1, 0);
//        }
//    }
//    for (int i = 0; i < SpatialObjectTypes_List.SeaArea.length(); i++) {
//        FEATURE_POINTS SeaObject = SpatialObjectTypes_List.SeaArea[i];
//        for (int j = 0; j < SeaObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector4(SeaObject.x[j], SeaObject.y[j], j, 150, 1, 0);
//        }
//    }
//    /* 2. Color everything outside map outline black   */
////    ChartWindowInstance->fillMapBorder();
//    /* 3. Plot thick lines of land and water when alpha is 0   */
//    for (int i = 0; i < SpatialObjectTypes_List.MapOutline.length(); i++) {
//        FEATURE_POINTS MapObject = SpatialObjectTypes_List.MapOutline[i];
//        for (int j = 0; j < MapObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector4(MapObject.x[j], MapObject.y[j], j, 255, 1, 0);
//        }
//    }
//    for (int i = 0; i < SpatialObjectTypes_List.LandArea.length(); i++) {
//        FEATURE_POINTS LandObject = SpatialObjectTypes_List.LandArea[i];
//        for (int j = 0; j < LandObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector4(LandObject.x[j], LandObject.y[j], j, 50, 3, 0);
//        }
//    }
//    for (int i = 0; i < SpatialObjectTypes_List.SeaArea.length(); i++) {
//        FEATURE_POINTS SeaObject = SpatialObjectTypes_List.SeaArea[i];
//        for (int j = 0; j < SeaObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector4(SeaObject.x[j], SeaObject.y[j], j, 150, 3, 0);
//        }
//    }
//    ui->gpsPlot->replot();
//    /* 4. Fill land and water areas  */
//    ChartWindowInstance->paintMap();
//    /* 5. Fill other areas   */
//    /*   a. Draw outline on top colorMap   */
//    for (int i = 0; i < SpatialObjectTypes_List.WaterArea.length(); i++) {
//        FEATURE_POINTS WaterObject = SpatialObjectTypes_List.WaterArea[i];
//        if (WaterObject.depth2 < 5)
//        for (int j = 0; j < WaterObject.x.length(); j++) {
//            ChartWindowInstance->addMapVector5(WaterObject.x[j], WaterObject.y[j], j, 170, 3, 1);
//        }
//    }
//    ui->gpsPlot->replot();
//    /*   b. Fill the area - only if there are pixels not along the edges   */
//    ChartWindowInstance->paintTopMap(150);
//    /*   c. Copy pixels from top map to bottom map if the color meets criteria   */
//    ChartWindowInstance->copyAreaToLandMap(170, 150);
//    ui->gpsPlot->replot();
//    /* 6. Plot outline of remaining objects   */



//    for (int i = 0; i < SpatialFeatures_List.length(); i++) {
//        int pointerCode = SpatialFeatures_List[i].ObjectCode;
//        int lineType = 0;
//        if      (pointerCode == 12)        lineType = 2;   // Building - BUISGL
//        else if (pointerCode == 13)        lineType = 2;   // Building Area - BUAARE
//        else if (pointerCode == 42)        lineType = 8;   // Depth Area - DEPARE
//        else if (pointerCode == 43)        lineType = 3;   // Water Contour - DEPCNT
//        else if (pointerCode == 46)        lineType = 8;   // Dredged Area - DRGARE
//        else if (pointerCode == 69)        lineType = 10;   // Lake - LAKARE
//        else if (pointerCode == 71)        lineType = 4;   // Land Area - LNDARE
//        else if (pointerCode == 119)       lineType = 6;   // Sea Area - SEAARE
//        else if (pointerCode == 122)       lineType = 2;   // Shoreline construction - SLCONS
//        else if (pointerCode == 302)       lineType = 1;   // Coverage/Border - M_COVR

//        // Plot all spatial objects from the existing list
//        if (pointerCode == 42 && SpatialFeatures_List[i].depth2 < 20) {
//            for (int j = 0; j < SpatialFeatures_List[i].x.length(); j++) {
//                ChartWindowInstance->addMapVector2(SpatialFeatures_List[i].x[j], SpatialFeatures_List[i].y[j], j, lineType);
//            }
//        }

//        if (SpatialFeatures_List[i].ObjectCode == 71)   SpatialObjectTypes_List.LandArea.append(SpatialFeatures_List[i]);
//        //ui->gpsPlot->replot();
//    }
}

uint32_t MainWindow :: Enc_blowfish(BLOWFISH_CTX *ctx, uint32_t xIn) {
    uint8_t xA = (xIn >> 24) & 0x000000FF;
    uint8_t xB = (xIn >> 16) & 0x000000FF;
    uint8_t xC = (xIn >> 8) & 0x000000FF;
    uint8_t xD = (xIn >> 0) & 0x000000FF;
    uint32_t ans;
    ans = (ctx->S[0][xA] + ctx->S[1][xB]) % 4294967296;
    ans = ans ^ ctx->S[2][xC];
    ans = (ans + ctx->S[3][xD]) % 4294967296;

    return ans;
}

void MainWindow :: Enc_Encrypt (BLOWFISH_CTX *ctx, unsigned long *xl, unsigned long *xr) {
    uint32_t XR = *xr;
    uint32_t XL = *xl;
    uint32_t tmp;

    for (int i = 0; i < 16; i++) {
        XL = XL ^ ctx->P[i];
        XR = Enc_blowfish(ctx, XL) ^ XR;

        tmp = XL;
        XL = XR;
        XR = tmp;
    }
    tmp = XR;
    XR = XL ^ (ctx->P[16]);
    XL = tmp ^ (ctx->P[17]);

    *xl = (unsigned long) XL;
    *xr = (unsigned long) XR;
}

void MainWindow :: Enc_readFile () {
    uint32_t SL = 0x31323334;
    uint32_t SR = 0x38030303;
    //    SL = 0xC1CB518E;//
    //    SR = 0x9C030303;
    uint32_t PL = 0x39383736;
    uint32_t PR = 0x35030303;
    unsigned long DR = 0;
    unsigned long DL = 0;
    BLOWFISH_CTX X_ctx;
    uint32_t tmp, tmpP;
    char key[6] = "98765";
    int a = 0;
    unsigned long data;
    for (int i = 0; i < 18; i++) {
        //        if (i % 8 == 0) {
        //            X_ctx.P[i] = X_ctx.P[i] ^ PL;
        //        } else if (i % 8 == 1) {
        //            X_ctx.P[i] = X_ctx.P[i] ^ PR;
        //        }

        data = 0;
        for (int k = 0; k < 4; k++) {
            data = (data << 8) | key[a];
            a++;
            if (a >= 5) a = 0;
        }
        X_ctx.P[i] = X_ctx.P[i] ^ data;
    }

    for (int i = 0; i < 18; i+=2) {
        Enc_Encrypt(&X_ctx, &DL, &DR);
        X_ctx.P[i] = DL;
        X_ctx.P[i+1] = DR;
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 256; j += 2) {
            Enc_Encrypt(&X_ctx, &DL, &DR);
            X_ctx.S[i][j] = DL;
            X_ctx.S[i][j+1] = DR;
        }
    }
    DL = SL;    DR = SR;
    Enc_Encrypt(&X_ctx, &DL, &DR);
    char s1[9] = {'\0'};
    char s2[9] = {'\0'};
    char s[17] = {'\0'};
    sprintf(s1, "%08X", DL);
    sprintf(s2, "%08X", DR);
    memcpy(s, s1, 8);
    memcpy(&s[8], s2, 8);
    uint64_t sInt = (((uint64_t) DL) << 32) | DR;

    qDebug() << "Blowfish Algorithm Output:" << DL << DR << s1 << s2 << s << sInt;

    uint32_t crc=0xFFFFFFFF;
    char str[10] = "123456789";

    for(int i = 0; i < 9; i++) {
        char ch = str[i];//1;//s[i];//(sInt >> (8*i)) & 0x000000FF;//s[i];
        //        crc = crc ^ ch;
        for(int j = 0; j < 8; j++) {
            uint32_t b = (ch ^ crc) & 1;
            crc <<= 1;
            if (b)      crc = crc ^ 0x04C11DB7; //0xEDB88320 //0x04C11DB7
            ch <<= 1;

            //            uint32_t b = -(crc & 1);
            //            crc = (crc >> 1) ^ (/*0xEDB88320*/0x04C11DB7 & b);
        }
    }
    qDebug() << "CRC:" << crc;
    // TO DO: Fix the CRC32 calculation

    // .swap() is required to clear the lists. The .clear() is too slow
    QVector <QVector <DR_DIRECTORY>> DrSwap;
    DrEntry_List.swap(DrSwap);
    QVector <DDR_DIRECTORY> DdrSwap;
    DdrFields_List.swap(DdrSwap);
    QVector <SPATIAL_FEATURES> FeatureSwap;
    Feature_List.swap(FeatureSwap);
    QList <QStringList> FeatureStrSwap;
    Feature_StringList.swap(FeatureStrSwap);
    QVector <FEATURE_RECORD> RecordSwap;
    Record_List.swap(RecordSwap);
    QVector <SG3D_RECORD> SG3DSwap;
    SG3D_List.swap(SG3DSwap);
    QVector <SPATIAL_FEATURES> SG2DSwap;
    SG2D_List.swap(SG2DSwap);
    QVector <FEATURE_POINTS> SpatialFeaturesSwap;
    SpatialFeatures_List.swap(SpatialFeaturesSwap);
    QVector <SPATIAL_FEATURES> SoundingFeaturesSwap;
    SoundingFeatures_List.swap(SoundingFeaturesSwap);
    QVector <VRPT_RECORD> vrptRecordSwap;
    vrptRecord_List.swap(vrptRecordSwap);
    QVector <FEATURE_POINTS> AreaSwap;
    SpatialObjectTypes_List.SeaArea.swap(AreaSwap);
    QVector <FEATURE_POINTS> AreaSwap2;
    SpatialObjectTypes_List.DeepArea.swap(AreaSwap2);
    QVector <FEATURE_POINTS> AreaSwap3;
    SpatialObjectTypes_List.WaterArea.swap(AreaSwap3);
    QVector <FEATURE_POINTS> AreaSwap4;
    SpatialObjectTypes_List.ShallowArea.swap(AreaSwap4);
    QVector <FEATURE_POINTS> AreaSwap5;
    SpatialObjectTypes_List.LandArea.swap(AreaSwap5);
    QVector <FEATURE_POINTS> AreaSwap6;
    SpatialObjectTypes_List.LakeArea.swap(AreaSwap6);
    QVector <FEATURE_POINTS> AreaSwap7;
    SpatialObjectTypes_List.BuildingArea.swap(AreaSwap7);
    QVector <FEATURE_POINTS> AreaSwap8;
    SpatialObjectTypes_List.WaterContour.swap(AreaSwap8);
    QVector <FEATURE_POINTS> AreaSwap9;
    SpatialObjectTypes_List.LandContour.swap(AreaSwap9);
    QVector <FEATURE_POINTS> AreaSwap10;
    SpatialObjectTypes_List.MapOutline.swap(AreaSwap10);
    QVector <OBJECT_POINT> SinglePointObjectsSwap;
    SinglePointObjects_List.swap(SinglePointObjectsSwap);

    QByteArray EncBuffer = EncFile.readAll();

    char *data_pointer = EncBuffer.data();
    int fileLength = EncFile.size();
    int BytesPointer = 0, BytesPointer2 = 0;

    /*   Read ENC File   */
    // Read the DDR
    BytesPointer2 = Enc_ReadDdr(data_pointer, BytesPointer);
    BytesPointer += BytesPointer2;
    data_pointer += BytesPointer2;
    //qDebug() << "BytesPointer:" << BytesPointer;
    emit updateChartProgress(10);

    // Read all DR entries
    do {
        BytesPointer2 = Enc_ReadDataRecord(data_pointer, 0);
        BytesPointer += BytesPointer2;
        data_pointer += BytesPointer2;
        qDebug() << "BytesPointer:" << BytesPointer;
        if (BytesPointer == 1499189)
            qDebug() << "1499189";
    } while(fileLength - 10 > BytesPointer);

    emit sendSBarSize(100);
    status_setProgressBarValue(25);

    // Read COMF value
    int i = 0, j = 0, index = 0;
    for (i = 0; i < DdrFields_List.length(); i++) {
        if (QString::fromLatin1(DdrFields_List[i].FieldTag) == "DSPM") {
            index = i;
            break;
        }
    }
    for (j = 0; j < DdrFields_List[i].Data.ArrayDesc.length(); j++) {
        if (DdrFields_List[i].Data.ArrayDesc[j] == "COMF") {
            break;
        }
    }
    for (int k = 0; k < DrEntry_List.length(); k++) {
        if (QString::fromLatin1(DrEntry_List[k][1].FieldTag) == "DSPM") {
            COMF = (double) Enc_convertToInt(DrEntry_List[k][1].Data[j]);
            SOMF = (double) Enc_convertToInt(DrEntry_List[k][1].Data[j+1]);
            break;
        }
    }

    emit updateChartProgress(20);

    // Create a list of spatial points with all SG2D/SG3D/VRPT entries
    Enc_CreateSpatialObjectList();
    emit updateChartProgress(40);
    Enc_CreateVrptList();
    emit updateChartProgress(60);
    Enc_PlotRecordObjects();
    emit updateChartProgress(80);
    // Leave thread and go to Enc_checkToPlot()
}

void MainWindow :: Enc_checkToPlot() {
    if (!encChartThreadProgress.isFinished())    {
        EncTimer.start(100);
        return;
    }
    EncTimer.stop();
//    ChartWindowInstance->clearEncChart();
    //encChartThreadProgress.waitForFinished(); //Waits for the thread to be finished before moving on
    //Enc_readFile();

    // Plot all spatial Features
    double xEncMin = 0, xEncMax = 0, yEncMin = 0, yEncMax = 0;
    Enc_plotAll2dObjects();
    Enc_plotAllSoundPoints();
    if (SpatialObjectTypes_List.MapOutline.length() > 0) {
        if (SpatialObjectTypes_List.MapOutline[0].x.length() > 0) {
            xEncMin = SpatialObjectTypes_List.MapOutline[0].x[0];
            xEncMax = SpatialObjectTypes_List.MapOutline[0].x[0];
            yEncMin = SpatialObjectTypes_List.MapOutline[0].y[0];
            yEncMax = SpatialObjectTypes_List.MapOutline[0].y[0];
        }
    }
    for (int i = 0; i < SpatialObjectTypes_List.MapOutline.length(); i++) {
        for (int j = 0; j < SpatialObjectTypes_List.MapOutline[i].x.length(); j++) {
            if (xEncMin > SpatialObjectTypes_List.MapOutline[i].x[j])
                xEncMin = SpatialObjectTypes_List.MapOutline[i].x[j];
            if (xEncMax < SpatialObjectTypes_List.MapOutline[i].x[j])
                xEncMax = SpatialObjectTypes_List.MapOutline[i].x[j];
            if (yEncMin > SpatialObjectTypes_List.MapOutline[i].y[j])
                yEncMin = SpatialObjectTypes_List.MapOutline[i].y[j];
            if (yEncMax < SpatialObjectTypes_List.MapOutline[i].y[j])
                yEncMax = SpatialObjectTypes_List.MapOutline[i].y[j];
        }
    }
    ChartWindowInstance->setPlotRange(xEncMin, xEncMax, yEncMin, yEncMax);
    emit updateChartProgress(100);
    ui->gpsPlot->xAxis->setTickLabels(true);
    ui->gpsPlot->yAxis->setTickLabels(true);
    ui->gpsPlot->xAxis->grid()->setVisible(true);
    ui->gpsPlot->yAxis->grid()->setVisible(true);
    ChartWindowInstance->plot_replot();
    Chart_UnstretchPlot();
    emit updateChartProgress(110);
}

int  MainWindow :: Enc_convertToInt (QString &str) {
    QByteArray strArray;
    int value = 0;
    strArray = (str.mid(0,4).toLatin1());//.append(str);
    //    for (auto b : strArray)
    //        value = (value << 8) | quint8(b);
    for (int i = 3; i >= 0; i--) {
        char b;
        if (i < strArray.length()) {
            b = strArray.at(i);
        } else {
            b = 0;
        }
        //        int tmp = strArray.toInt();
        value = (value << 8) | quint8(b);
    }

    return value;
}

void MainWindow :: Enc_ReadDescriptor (QVector<QString> &DrDataEntries, QVector <DR_DIRECTORY> DrFields_List, /*QVector<DDR_DIRECTORY> DdrFields_List,*/ int i, char *FieldArea, int *BytesRead) {
    // Find DR field name in DDR Field List
    char *pointer = FieldArea;
    int bitPointer = 0;
    int countPointer = 0;
    int j = 0;
    if (QString::fromLatin1(DrFields_List[i].FieldTag, 4) == "0000")
        qDebug() << "0000";
    for (j = 0; j < DdrFields_List.length(); j++) {
        if (QString::fromLatin1(DrFields_List[i].FieldTag, 4) == QString::fromLatin1(DdrFields_List[j].FieldTag, 4)) {
            break;
        }
    }

    //    if (DdrFields_List.length())    return DrDataDesc.FieldAreaEntries;
    //    if (DrFields_List[i].FieldLength.toInt() < DdrFields_List[j].Data.FormatChar.length())    return;
    //    if (QString::fromLatin1(DrFields_List[i].FieldTag) == "0001")    return DrDataDesc.FieldAreaEntries;

    do {
        // Start reading DR field based on provided data
        for (int k = 0; k < DdrFields_List[j].Data.FormatChar.length(); k++) {
            bitPointer = 0;
            if (DdrFields_List[j].Data.FormatChar[k] == "b11") {
                char tmp[2] = " ";
                // Read one byte
                memcpy(&tmp, pointer, 1);
                QString tmpstr = tmp;
                pointer += 1;
                countPointer += 1;
                DrDataEntries.append(QString::fromLatin1(tmp, 1));
            }
            else if (DdrFields_List[j].Data.FormatChar[k] == "b12") {
                char tmp[3] = "  ";
                // Read 2 bytes
                memcpy(&tmp, pointer, 2);
                pointer += 2;
                countPointer += 2;
                DrDataEntries.append(QString::fromLatin1(tmp, 2));
            }
            else if (DdrFields_List[j].Data.FormatChar[k] == "b14") {
                char tmp[5] = "    ";
                // Read 4 bytes
                memcpy(&tmp, pointer, 4);
                pointer += 4;
                countPointer += 4;
                DrDataEntries.append(QString::fromLatin1(tmp, 4));
            }
            else if (DdrFields_List[j].Data.FormatChar[k] == "b24") {
                char tmp[5] = "    ";
                // Read 4 bytes
                memcpy(&tmp, pointer, 4);
                pointer += 4;
                countPointer += 4;
                DrDataEntries.append(QString::fromLatin1(tmp, 4));
            }
            else if (DdrFields_List[j].Data.FormatChar[k].mid(0,1) == "B") {
                uint length = 0;
                if (DdrFields_List[j].Data.FormatChar[k].length() == 4) {
                    // One digit + )
                    length = DdrFields_List[j].Data.FormatChar[k].mid(2,1).toUInt();
                }
                else if (DdrFields_List[j].Data.FormatChar[k].length() == 5) {
                    // Two digits + )
                    length = DdrFields_List[j].Data.FormatChar[k].mid(2,2).toUInt();
                }
                char tmp[10] = {'\0'};
                length /= 8;
                memcpy(&tmp, pointer, length);
                if (length > *BytesRead) {
                    qDebug() << "BytesRead";
                    countPointer = *BytesRead;
                    DrDataEntries.append("N/A");
                    return;
                }
                pointer += length;
                countPointer += length;
                DrDataEntries.append(QString::fromLatin1(tmp, (int) length));
            }
            else {
                // Is 'A', 'B', 'R', or 'I'
                if (DdrFields_List[j].Data.FormatChar[k].length() > 2) {
                    uint length = 0;
                    if (DdrFields_List[j].Data.FormatChar[k].length() == 4) {
                        // One digit + )
                        length = DdrFields_List[j].Data.FormatChar[k].mid(2,1).toUInt();
                    }
                    else if (DdrFields_List[j].Data.FormatChar[k].length() == 5) {
                        // Two digits + )
                        length = DdrFields_List[j].Data.FormatChar[k].mid(2,2).toUInt();
                    }
                    char tmp[100] = {'\0'};
                    memcpy(&tmp, pointer, length);
                    if (length > *BytesRead) {
                        qDebug() << "BytesRead";
                        countPointer = *BytesRead;
                        DrDataEntries.append("N/A");
                        return;
                    }
                    pointer += length;
                    countPointer += length;
                    DrDataEntries.append(QString::fromLatin1(tmp, (int) length));
                } else {
                    // Read at an unknown length
                    char tmp[1000] = {'\0'};
                    int length = 0;
                    length += Enc_ReadString2(tmp, pointer, &bitPointer);
                    if (length > *BytesRead) {
                        qDebug() << "BytesRead";
                        countPointer = *BytesRead;
                        DrDataEntries.append("N/A");
                        return;
                    }
                    pointer += length;
                    countPointer += length;
                    DrDataEntries.append(QString::fromLatin1(tmp, length));
                }
            }/*
            else if (DdrFields_List[j].Data.FormatChar[k] == "A(8)") {
                char tmp[9] = "        ";
                // Read 8 bytes
                memcpy(&tmp, pointer, 8);
                pointer += 8;
                countPointer += 8;
                DrDataDesc.FieldAreaEntries.append(QString::fromLatin1(tmp));
            }
            else if (DdrFields_List[j].Data.FormatChar[k] == "R(4)") {
                char tmp[5] = "    ";
                // Read 4 bytes
                memcpy(&tmp, pointer, 4);
                pointer += 4;
                countPointer += 4;
                DrDataDesc.FieldAreaEntries.append(QString::fromLatin1(tmp));
            }
            else if (DdrFields_List[j].Data.FormatChar[k] == "A") {
            }*/
        }
    } while((/*QString::fromLatin1(DrFields_List[i].FieldLength).toInt() - 1*/*BytesRead-1 > countPointer) && countPointer < 1400);
    char checkFT;
    memcpy(&checkFT, pointer, 1);
    if (checkFT == 0x1E)    countPointer++;
    *BytesRead = countPointer;
    //    return DrDataEntries;
}
