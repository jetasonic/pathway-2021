#include "navigationsettings.h"
#include "ui_navigationsettings.h"
#include "mainwindow.h"


MainWindow* nMainWindow;

NavigationSettings::NavigationSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NavigationSettings)
{
    nMainWindow = qobject_cast<MainWindow*>(this->parent());
    ui->setupUi(this);
    this->grab();
    this->setWindowFlags(Qt::Window);
    ui->radio_data8->setChecked(true);
    ui->radio_stop1->setChecked(true);
    ui->radio_parityNone->setChecked(true);
    ui->list_baud->setCurrentText("115200");
    reloadPortInfo();
    loadSettings();
//    timerUpdatePortInfo.start(100);
//    connect(&timerUpdatePortInfo, SIGNAL(timeout()), this, SLOT(reloadPortInfo()));
    connect(ui->list_settingNames, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(on_btn_load_clicked()));
}

NavigationSettings::~NavigationSettings()
{
    delete ui;
}

void NavigationSettings :: closeEvent(QCloseEvent *) {
    //disconnect(ComPort, SIGNAL(readyRead()), this, SLOT(readSerial()));
    ui->text_stream->clear();
    windowOpen = false;
}

void NavigationSettings :: mousePressEvent(QMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        if (e->x() > ui->list_port->x()/* && e->x() < ui->list_port->x() + ui->list_port->width()*/ &&
                e->y() > ui->list_port->y()/* && e->x() < ui->list_port->y() + ui->list_port->height()*/)  {

        }
    }
}

bool NavigationSettings :: eventFilter(QObject *obj, QEvent *e) {
//    if (obj == ui->list_port && e->type() == QEvent::MouseButtonPress) {
////        updateComboBox = 1;
////        reloadPortInfo();
////        updateComboBox = 0;
////        ui->list_port->showPopup();
////        ui->list_port->showPopup();
//        return 1;
//    }
//    return 0;
}

void NavigationSettings :: keyPressEvent(QKeyEvent *e) {
    if (e->modifiers() == Qt::ControlModifier && e->key() == Qt::Key_Q) {//        QByteArray sendArr;
        // Modifies settings for U-Blox Neo 9M - Configure settings with UBX protocol
        QByteArray sendArr;
        quint8 sendChar[50];
        quint8 header1 = 0xb5;
        quint8 header2 = 0x62;
        quint8 ckA = 0, ckB = 0;

        sendChar[0] = 0x06;  // CFG class
        sendChar[1] = 0x8a;
        sendChar[2] = 0x04 + 0x28; // Length & 0x00FF
        sendChar[3] = 0x00; // Length & 0xFF00
        sendChar[4] = 0x00;
        sendChar[5] = 0x01;
        sendChar[6] = 0x00;
        sendChar[7] = 0x00;

        // Output Rate of ZDA via USB - Turn off ZDA
        sendChar[8] = 0xdb; // keyID & 0x000000FF
        sendChar[9] = 0x00; // keyID & 0x0000FF00
        sendChar[10] = 0x91; // keyID & 0x00FF0000
        sendChar[11] = 0x20; // keyID & 0xFF000000
        sendChar[12] = 0x00;//0x01;
        // Turn off GSA via USB
        sendChar[13] = 0xc2; // keyID & 0x000000FF
        sendChar[14] = 0x00; // keyID & 0x0000FF00
        sendChar[15] = 0x91; // keyID & 0x00FF0000
        sendChar[16] = 0x20; // keyID & 0xFF000000
        sendChar[17] = 0x00;
        // Turn off GSV
        sendChar[18] = 0xc7; // keyID & 0x000000FF
        sendChar[19] = 0x00; // keyID & 0x0000FF00
        sendChar[20] = 0x91; // keyID & 0x00FF0000
        sendChar[21] = 0x20; // keyID & 0xFF000000
        sendChar[22] = 0x00;
        // Set CFG-TP-USE_LOCKED_TP1
        sendChar[23] = 0x09; // keyID & 0x000000FF
        sendChar[24] = 0x00; // keyID & 0x0000FF00
        sendChar[25] = 0x05; // keyID & 0x00FF0000
        sendChar[26] = 0x10; // keyID & 0xFF000000
        sendChar[27] = 0x00;
        // Set timepulse length
        sendChar[28] = 0x04; // keyID & 0x000000FF
        sendChar[29] = 0x00; // keyID & 0x0000FF00
        sendChar[30] = 0x05; // keyID & 0x00FF0000
        sendChar[31] = 0x40; // keyID & 0xFF000000
        sendChar[32] = 0xA0;
        sendChar[33] = 0x86;
        sendChar[34] = 0x01;
        sendChar[35] = 0x00;
        // Set time pulse duty cycle
        sendChar[36] = 0x2a; // keyID & 0x000000FF
        sendChar[37] = 0x00; // keyID & 0x0000FF00
        sendChar[38] = 0x05; // keyID & 0x00FF0000
        sendChar[39] = 0x50; // keyID & 0xFF000000
        sendChar[40] = 0x00;
        sendChar[41] = 0x00;
        sendChar[42] = 0x00;
        sendChar[43] = 0x00;
        sendChar[44] = 0x00;
        sendChar[45] = 0x00;
        sendChar[46] = 0x24;
        sendChar[47] = 0x40;

        for (int k = 0; k < 48; k++) {
            ckA = ckA + sendChar[k];
            ckB = ckB + ckA;
        }

        sendArr.append((const char*)&header1, sizeof(header1));
        sendArr.append((const char*)&header2, sizeof(header2));
        sendArr.append((const char*)&sendChar, 48);
        //        sendArr.append((const char*)&class1, sizeof(class1));
        //        sendArr.append((const char*)&id, sizeof(id));
        //        sendArr.append((const char*)&len1, sizeof(len1));
        //        sendArr.append((const char*)&payload, sizeof(payload));
        //        sendArr.append((const char*)&keyId, sizeof(keyId));
        //        sendArr.append((const char*)&setting, sizeof(setting));
        sendArr.append((const char*)&ckA, sizeof(ckA));
        sendArr.append((const char*)&ckB, sizeof(ckB));
        //sendArr.append(0x209100db);
        //sendArr.append(0x01);

        int num = 0;
        num = ComPort->write(sendArr, 52);

    }
}

void NavigationSettings :: reloadPortInfo() {
    QList<QSerialPortInfo> portInfo = QSerialPortInfo::availablePorts();
    QList<QString> portList;

    // Clear entire list and repopulate with "Disabled" and all available COM ports
    ui->list_port->clear();
    ui->list_port->addItem("Disabled");

    QString currentPortName = "NULL";
    if (ComPort != nullptr) {
        currentPortName = ComPort->portName();
    }

    // Make a list of active COM ports
    foreach (const QSerialPortInfo &serialPortInfo, portInfo){
        if (!serialPortInfo.isBusy() || serialPortInfo.portName() == currentPortName)
            ui->list_port->addItem(serialPortInfo.portName());
    }
    if (ComPort != nullptr) {
        if (ui->list_port->findText(ComPort->portName()) > -1) {
            ui->list_port->setCurrentText(ComPort->portName());
        }
    } else {
        updateComboBox = 1;
        ui->list_port->setCurrentIndex(0);
    }
}

void NavigationSettings :: on_list_port_currentTextChanged() {
    if (updateComboBox)     return;

    QString portName = ui->list_port->currentText();
    if (ComPort != nullptr) {
        disconnect(ComPort, SIGNAL(readyRead()), this, SLOT(readSerial()));
        ComPort->close();
        delete ComPort;
        ComPort = nullptr;
        nMainWindow->state.GNSS_MODE = 0;
        nMainWindow->Gnss_updateButtonState(0);
        ui->text_stream->clear();
    }
    if (portName == "Disabled") {
        ui->label_location->setText("Device Disconnected");
        return;
    } else if (portName == "")     return;

    QSerialPortInfo obj(portName);
    if (obj.isNull()) {
        ui->label_location->setText("Device Disconnected");
        ui->list_port->setCurrentIndex(0);
        on_btn_refresh_clicked();
        return;
    }
    ComPort = new QSerialPort(this);
    ComPort->setPortName(portName);
    ComPort->open(QSerialPort::ReadWrite);
    ComPort->setBaudRate(baudRate);
    ComPort->setDataBits(dataBits);
    ComPort->setParity(parity);
    ComPort->setStopBits(stopBits);
    ComPort->setFlowControl(QSerialPort::NoFlowControl);
    nMainWindow->state.GNSS_MODE = 1;  // Begin Validation Process
    nMainWindow->Gnss_updateButtonState(1);
    validationTimer.start(10000);
    invalidMessageCount = 0;
    gnssController->reset();
    ui->label_location->setText("Device connected, waiting for signal...");
    ui->label_location->setStyleSheet("color: white");

    connect(ComPort, SIGNAL(readyRead()), this, SLOT(readSerial()));
    connect(ComPort, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(closeSerial(QSerialPort::SerialPortError)));
    connect(&validationTimer, SIGNAL(timeout()), this, SLOT(invalidateTime()));
}

void NavigationSettings :: on_radio_data7_clicked() {
    dataBits = QSerialPort::Data7;
    if (ComPort != nullptr)     ComPort->setDataBits(dataBits);
}

void NavigationSettings :: on_radio_data8_clicked() {
    dataBits = QSerialPort::Data8;
    if (ComPort != nullptr)     ComPort->setDataBits(dataBits);
}

void NavigationSettings :: on_radio_stop1_clicked() {
    stopBits = QSerialPort::OneStop;
    if (ComPort != nullptr)     ComPort->setStopBits(stopBits);
}

void NavigationSettings :: on_radio_stop2_clicked() {
    stopBits = QSerialPort::TwoStop;
    if (ComPort != nullptr)     ComPort->setStopBits(stopBits);
}

void NavigationSettings :: on_radio_parityNone_clicked() {
    parity = QSerialPort::NoParity;
    if (ComPort != nullptr)     ComPort->setParity(parity);
}

void NavigationSettings :: on_radio_parityOdd_clicked() {
    parity = QSerialPort::OddParity;
    if (ComPort != nullptr)     ComPort->setParity(parity);
}

void NavigationSettings :: on_radio_parityEven_clicked() {
    parity = QSerialPort::EvenParity;
    if (ComPort != nullptr)     ComPort->setParity(parity);
}

void NavigationSettings :: on_list_baud_currentTextChanged() {
    switch (ui->list_baud->currentText().toInt()) {
    case 2400:
        baudRate = QSerialPort::Baud2400;
        break;
    case 4800:
        baudRate = QSerialPort::Baud4800;
        break;
    case 9600:
        baudRate = QSerialPort::Baud9600;
        break;
    case 19200:
        baudRate = QSerialPort::Baud19200;
        break;
    case 38400:
        baudRate = QSerialPort::Baud38400;
        break;
    case 57600:
        baudRate = QSerialPort::Baud57600;
        break;
    case 115200:
        baudRate = QSerialPort::Baud115200;
        break;
    }
    if (ComPort != nullptr)     ComPort->setBaudRate(baudRate);
}

void NavigationSettings :: on_btn_add_clicked() {
    if (ui->text_settingName->text() == "")     return;
    if (ComPort == nullptr)                     return;

    // Save COM port settings to a list
    SERIALSETTINGS currentSettings;
    currentSettings.SettingName = ui->text_settingName->text();
    currentSettings.PortName = ui->list_port->currentText();
    currentSettings.baudRate = baudRate;
    currentSettings.parity = parity;
    currentSettings.dataBits = dataBits;
    currentSettings.stopBits = stopBits;

    // Add the item to the list of settings (in window and QList<struct>)
    settings_list.append(currentSettings);
    ui->list_settingNames->addItem(currentSettings.SettingName);
    ui->text_settingName->setText("");

    // Save the settings to a config file
    saveSettings();
}

void NavigationSettings :: on_btn_delete_clicked() {
    int selectedRow = ui->list_settingNames->currentRow();
    if (selectedRow < 0)    return;
    else if (selectedRow > settings_list.length())    return;

    ui->text_settingName->setText("");
    settings_list.removeAt(selectedRow);
    ui->list_settingNames->takeItem(selectedRow);

    // Save the settings to a config file
    saveSettings();
}

void NavigationSettings :: on_btn_load_clicked() {
    int selectedRow = ui->list_settingNames->currentRow();
    if (selectedRow < 0)    return;
    else if (selectedRow > settings_list.length())    return;

    int PortNameMatch = 0;
    for (int i = 0; i < ui->list_port->count(); i++) {
        if (ui->list_port->itemText(i) == settings_list.at(selectedRow).PortName) {
            PortNameMatch++;
            break;
        }
    }
    if (PortNameMatch == 0) {
        // Return a warning
        return;
    }

    // Update setting variables and UI buttons
    updateComboBox = 0;
    ui->text_settingName->setText(settings_list.at(selectedRow).SettingName);
    ui->list_port->setCurrentText(settings_list.at(selectedRow).PortName);
    baudRate = settings_list.at(selectedRow).baudRate;
    ui->list_baud->setCurrentText(QString::number(convertBaudRateToInt(settings_list.at(selectedRow).baudRate)));
    parity = settings_list.at(selectedRow).parity;
    dataBits = settings_list.at(selectedRow).dataBits;
    stopBits = settings_list.at(selectedRow).stopBits;
    switch (convertParityBitsToInt(parity)) {
    case 3:     ui->radio_parityOdd->setChecked(true);   break;
    case 2:     ui->radio_parityEven->setChecked(true);  break;
    default:    ui->radio_parityNone->setChecked(true);  break;
    }
    switch (convertDataBitsToInt(dataBits)) {
    case 7:     ui->radio_data7->setChecked(true);   break;
    case 8:     ui->radio_data8->setChecked(true);  break;
    default:    ui->radio_data8->setChecked(true);  break;
    }
    switch (convertStopBitsToInt(stopBits)) {
    case 1:     ui->radio_stop1->setChecked(true);   break;
    case 2:     ui->radio_stop2->setChecked(true);  break;
    default:    ui->radio_stop1->setChecked(true);  break;
    }
    updateComboBox = 1;

    on_list_port_currentTextChanged();
}

void NavigationSettings :: on_btn_save_clicked() {
    if (ComPort == nullptr)    return;
    if (ui->text_settingName->text() == "") {
        ui->text_settingName->setStyleSheet("QLineEdit{border : 2px solid orange;}");
        return;
    }

    ui->text_settingName->setStyleSheet("QLineEdit{border : 2px black;}");
    ui->text_settingName->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");

    // Save COM port settings to a list
    SERIALSETTINGS currentSettings;
    currentSettings.SettingName = ui->text_settingName->text();
    currentSettings.PortName = ui->list_port->currentText();
    currentSettings.baudRate = baudRate;
    currentSettings.parity = parity;
    currentSettings.dataBits = dataBits;
    currentSettings.stopBits = stopBits;

    // Search the list of setting names to find if it already exists
    int itemPosition = -1;
    for (int i = 0; i < ui->list_settingNames->count(); i++) {
        if (ui->list_settingNames->item(i)->text() == ui->text_settingName->text()) {
            itemPosition = i;
            break;
        }
    }

    // Add the item to the list of settings (in window and QList<struct>)
    if (itemPosition != -1)  settings_list.replace(itemPosition, currentSettings);
    else  {
        settings_list.append(currentSettings);
        ui->list_settingNames->addItem(currentSettings.SettingName);
    }
    ui->text_settingName->setText("");

    // Save the settings to a config file
    saveSettings();
}

void NavigationSettings :: on_btn_ok_clicked() {
    closeEvent(nullptr);
    this->hide();
}

void NavigationSettings :: on_btn_refresh_clicked() {
    updateComboBox = 1;
    reloadPortInfo();
    updateComboBox = 0;
}

void NavigationSettings :: saveSettings() {
    // Save all settings in the QList to a config (.ini) file
    QFile configFile;
    configFile.setFileName("./ComPortSettings.ini");
    configFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QByteArray configTxt;
    for (int i = 0; i < settings_list.length(); i++) {
        configTxt.append("[" + settings_list.at(i).SettingName + "]\n");
        configTxt.append("Port=" + settings_list.at(i).PortName + "\n"); //double
        configTxt.append("BaudRate=" + QString::number(convertBaudRateToInt(settings_list.at(i).baudRate)) + "\n");
        configTxt.append("DataBits=" + QString::number(convertDataBitsToInt(settings_list.at(i).dataBits)) + "\n");
        configTxt.append("ParityBits=" + QString::number(convertParityBitsToInt(settings_list.at(i).parity)) + "\n");
        configTxt.append("StopBits=" + QString::number(convertStopBitsToInt(settings_list.at(i).stopBits)) + "\n\n");
    }

    configFile.write(configTxt);
    configFile.close();
}

void NavigationSettings :: loadSettings() {
    QFile IniFile;
    IniFile.setFileName("./ComPortSettings.ini");
    IniFile.open(QIODevice::ReadWrite | QIODevice::Text);

    IniFile.seek(0);
    QByteArray IniTxt = IniFile.readAll();
    QString IniStr = IniTxt;
    QStringList IniList = IniStr.split("\n");

    settings_list.clear();
    SERIALSETTINGS settingItem;

    // Copy settings from .ini file to the QList
    for (int i = 0; i < IniList.length(); i++) {
        if (IniList.at(i).length() < 1)     continue;
        QStringList IniItem = IniList.at(i).split("=");

        if (IniList.at(i).at(0) == '[')     settingItem.SettingName = IniList.at(i).split("[").last().split("]").first();
        else if (IniItem.length() < 2)   continue;
        else if (IniItem.at(0).contains("Port"))        settingItem.PortName = IniItem.last();
        else if (IniItem.at(0).contains("BaudRate"))    settingItem.baudRate = convertIntToBaudRate(IniItem.last().toInt());
        else if (IniItem.at(0).contains("DataBits"))    settingItem.dataBits = convertIntToDataBits(IniItem.last().toInt());
        else if (IniItem.at(0).contains("ParityBits"))  settingItem.parity =   convertIntToParityBits(IniItem.last().toInt());
        else if (IniItem.at(0).contains("StopBits")) {
            settingItem.stopBits = convertIntToStopBits(IniItem.last().toInt());
            settings_list.append(settingItem);
        }
    }

    // Update the UI list based on the QList
    ui->list_settingNames->clear();
    for (int i = 0; i < settings_list.length(); i++) {
        ui->list_settingNames->addItem(settings_list.at(i).SettingName);
    }
}

int NavigationSettings :: convertBaudRateToInt(QSerialPort::BaudRate baud) {
    switch  (baud) {
    case QSerialPort::Baud1200:
        return 1200;
    case QSerialPort::Baud2400:
        return 2400;
    case QSerialPort::Baud4800:
        return 4800;
    case QSerialPort::Baud9600:
        return 9600;
    case QSerialPort::Baud19200:
        return 19200;
    case QSerialPort::Baud38400:
        return 38400;
    case QSerialPort::Baud57600:
        return 57600;
    case QSerialPort::Baud115200:
        return 115200;
    case QSerialPort::UnknownBaud:
        return 0;
    }
    return 0;
}

QSerialPort::BaudRate NavigationSettings :: convertIntToBaudRate(int baud) {
    switch  (baud) {
    case 1200:
        return QSerialPort::Baud1200;
    case 2400:
        return QSerialPort::Baud2400;
    case 4800:
        return QSerialPort::Baud4800;
    case 9600:
        return QSerialPort::Baud9600;
    case 19200:
        return QSerialPort::Baud19200;
    case 38400:
        return QSerialPort::Baud38400;
    case 57600:
        return QSerialPort::Baud57600;
    case 115200:
        return QSerialPort::Baud115200;
    case 0:
        return QSerialPort::UnknownBaud;
    }
    return QSerialPort::Baud115200;
}

int NavigationSettings :: convertDataBitsToInt(QSerialPort::DataBits databits) {
    switch  (databits) {
    case QSerialPort::Data5:
        return 5;
    case QSerialPort::Data6:
        return 6;
    case QSerialPort::Data7:
        return 7;
    case QSerialPort::Data8:
        return 8;
    case QSerialPort::UnknownDataBits:
        return -1;
    }
    return 0;
}

QSerialPort::DataBits NavigationSettings :: convertIntToDataBits(int databits) {
    switch  (databits) {
    case 5:
        return QSerialPort::Data5;
    case 6:
        return QSerialPort::Data6;
    case 7:
        return QSerialPort::Data7;
    case 8:
        return QSerialPort::Data8;
    case -1:
        return QSerialPort::UnknownDataBits;
    }
    return QSerialPort::Data8;
}

int NavigationSettings :: convertParityBitsToInt(QSerialPort::Parity paritybits) {
    switch  (paritybits) {
    case QSerialPort::NoParity:
        return 0;
    case QSerialPort::EvenParity:
        return 2;
    case QSerialPort::OddParity:
        return 3;
    case QSerialPort::SpaceParity:
        return 4;
    case QSerialPort::MarkParity:
        return 5;
    case QSerialPort::UnknownParity:
        return -1;
    }
    return 0;
}

QSerialPort::Parity NavigationSettings :: convertIntToParityBits(int paritybits) {
    switch  (paritybits) {
    case 0:
        return QSerialPort::NoParity;
    case 2:
        return QSerialPort::EvenParity;
    case 3:
        return QSerialPort::OddParity;
    case 4:
        return QSerialPort::SpaceParity;
    case 5:
        return QSerialPort::MarkParity;
    case -1:
        return QSerialPort::UnknownParity;
    }
    return QSerialPort::NoParity;
}

int NavigationSettings :: convertStopBitsToInt(QSerialPort::StopBits stopbits) {
    switch  (stopbits) {
    case QSerialPort::OneStop:
        return 1;
    case QSerialPort::OneAndHalfStop:
        return 3;
    case QSerialPort::TwoStop:
        return 2;
    case QSerialPort::UnknownStopBits:
        return -1;
    }
    return 0;
}

QSerialPort::StopBits NavigationSettings :: convertIntToStopBits(int stopbits) {
    switch  (stopbits) {
    case 1:
        return QSerialPort::OneStop;
    case 3:
        return QSerialPort::OneAndHalfStop;
    case 2:
        return QSerialPort::TwoStop;
    case -1:
        return QSerialPort::UnknownStopBits;
    }
    return QSerialPort::OneStop;
}

void NavigationSettings :: invalidateTime() {
    validationTimer.stop();
    if (nMainWindow->state.GNSS_MODE == 2)      return;

//    nMainWindow->state.GNSS_MODE = 0;
//    ComPort->disconnect();
//    nMainWindow->Gnss_updateButtonState(0);
//    ui->label_location->setStyleSheet("color: red");
//    ui->text_stream->clear();
    closeSerial(QSerialPort::TimeoutError);
}

void NavigationSettings :: closeSerial(QSerialPort::SerialPortError error) {
    if (ComPort == nullptr)   return;
    ComPort->close();
    delete ComPort;
    ComPort = nullptr;
    validationTimer.stop();
    updateComboBox = 0;
    ui->list_port->setCurrentIndex(0);
    updateComboBox = 1;
//    if (nMainWindow->state.GNSS_MODE != 2)  return; // if it did not pass the validation process, then do not show message
    nMainWindow->state.GNSS_MODE = 0;
    nMainWindow->Gnss_updateButtonState(0);
    // Return a wearning message
    ui->label_location->setText("Device disconnected");
    ui->text_stream->clear();
    gnssController->reset();
}

void NavigationSettings :: readSerial() {
    static QString serialBuffer;
    QStringList serialBufferList;
    QStringList tmpBufferList;

    // Read all incoming data from the COM port
    QByteArray serialData = ComPort->readAll();
    if (nMainWindow->state.GUI_MODE == 2)   return;

    // Only show the data on the text box if the window is open
    if (NavigationSettings::isVisible())
        ui->text_stream->append(serialData);

    // Add data to a static buffer and split the data into its separate lines
    serialBuffer += QString::fromStdString(serialData.toStdString());
    tmpBufferList = serialBuffer.split("\r\n");
    // If no complete new lines were added, then leave the function
    if (tmpBufferList.length() < 2)      return;

    // Otherwise, append all list items, except for the last item
    for (int i = 0; i < tmpBufferList.length() - 1; i++) {
        serialBufferList.append(tmpBufferList.at(i));

        // and remove the text from the buffers
        int lineSize = tmpBufferList.at(i).length();
        serialBuffer.remove(0, lineSize+2);
    }
    qDebug() << "***" << serialBufferList;

    // Decrypt the NMEA-0183 messages into coordinates
    readNmea(serialBufferList);

}

void NavigationSettings :: readNmea(QStringList DataList) {
    GPSINFO GpsTmp;
    MSINFO MotionTmp;
    for (int i = 0; i < DataList.length(); i++) {
        QStringList NmeaList = DataList.at(i).split(",");

        if (NmeaList.at(0).mid(0,2) != "$G")  {
            invalidMessageCount++;
            if (invalidMessageCount > 10 && nMainWindow->state.GNSS_MODE == 1) {
                // Not a valid message
                // If we receive more than 10 invalid messages, then we tell the user the device is invalid
                closeSerial(QSerialPort::UnsupportedOperationError);
                nMainWindow->state.GNSS_MODE = 0;
//                ComPort->close();
//                delete ComPort;
//                ComPort = nullptr;
//                validationTimer.stop();
//                updateComboBox = 0;
//                ui->list_port->setCurrentIndex(0);
//                updateComboBox = 1;
//                nMainWindow->Gnss_updateButtonState(0);
                ui->label_location->setText("Invalid device");
            }
        }

        // Verify checksum
        QString checksumStr = DataList.at(i).split('$').last().split('*').first();
        int checksumNum = 0;
        for (int j = 0; j < checksumStr.length(); j++) {
            int charByte = checksumStr.at(j).toLatin1();
            checksumNum ^= charByte;
        }
        int NmeaChecksum = DataList.at(i).split('*').last().split('\n\r').first().toUInt(nullptr, 16);
        if (checksumNum != NmeaChecksum)    continue;

        if (NmeaList.at(0).mid(3,3) == "GGA") {
            if (NmeaList.length() < 6)  continue;
            if (NmeaList.at(2).length() < 4 || NmeaList.at(4).length() < 4)    continue;

            if (NmeaList.at(1).length() >= 6)
                gnssController->updateTime(NmeaList.at(1).mid(0,2).toShort(),
                                           NmeaList.at(1).mid(2,2).toShort(),
                                           NmeaList.at(1).mid(4,2).toShort());

            // Get latitude degrees and minutes
            double Lat = -500, Lon = -500;
            int LatDeg = NmeaList.at(2).mid(0,2).toInt();
            double LatMin = NmeaList.at(2).mid(2).toDouble();
            if (NmeaList.at(2) != "")       Lat = LatDeg + LatMin/60;

            // Get N/S Field
            if (NmeaList.at(3) == 'S')      Lat *= -1.0;

            // Get longitude degrees and minutes
            int LonDeg = NmeaList.at(4).mid(0,3).toInt();
            double LonMin = NmeaList.at(4).mid(3).toDouble();
            if (NmeaList.at(4) != "")       Lon = LonDeg + LonMin/60;

            // Get E/W Field
            if (NmeaList.at(5) == 'W')      Lon *= -1.0;

            gnssController->updateCoords(Lat, Lon, ping->pingNumCurrent);

            if (gnssController->validCoords)
                ui->label_location->setText("Device connected. Current Location: " + QString::number(gnssController->Latitude(), 'g', 9) + ", "
                                            + QString::number(gnssController->Longitude(), 'g', 9));
            nMainWindow->state.GNSS_MODE = 2;
            if (nMainWindow->state.RT_MODE == 0 && nMainWindow->state.GUI_MODE < 2)     nMainWindow->Gnss_Update();
        }
        else if (NmeaList.at(0).mid(3,3) == "GLL") {
            if (NmeaList.length() < 6)  continue;
            if (NmeaList.at(1).length() < 4 || NmeaList.at(3).length() < 4)    continue;

            // Get latitude degrees and minutes
            double Lat = -500, Lon = -500;
            int LatDeg = NmeaList.at(1).mid(0,2).toInt();
            double LatMin = NmeaList.at(1).mid(2).toDouble();
            if (NmeaList.at(1) != "")       Lat = LatDeg + LatMin/60;

            // Get N/S Field
            if (NmeaList.at(2) == 'S')      Lat *= -1.0;

            // Get longitude degrees and minutes
            int LonDeg = NmeaList.at(3).mid(0,3).toInt();
            double LonMin = NmeaList.at(3).mid(3).toDouble();
            if (NmeaList.at(3) != "")       Lon = LonDeg + LonMin/60;

            // Get E/W Field
            if (NmeaList.at(4) == 'W')      Lon *= -1.0;

            gnssController->updateCoords(Lat, Lon, ping->pingNumCurrent);
            if (NmeaList.at(5).length() >= 6)
                gnssController->updateTime(NmeaList.at(5).mid(0,2).toShort(),
                                           NmeaList.at(5).mid(2,2).toShort(),
                                           NmeaList.at(5).mid(4,2).toShort());

            if (gnssController->validCoords)
                ui->label_location->setText("Device connected. Current Location: " + QString::number(gnssController->Latitude(), 'g', 9) + ", "
                                            + QString::number(gnssController->Longitude(), 'g', 9));
            nMainWindow->state.GNSS_MODE = 2;
            if (nMainWindow->state.RT_MODE == 0 && nMainWindow->state.GUI_MODE < 2)     nMainWindow->Gnss_Update();
        }
        else if (NmeaList.at(0).mid(3,3) == "ZDA") {
            if (NmeaList.length() < 6)  continue;
            if (NmeaList.at(1).length() < 7)    continue;

            gnssController->updateDate(NmeaList.at(2).mid(0,2).toShort(),
                                       NmeaList.at(3).mid(2,2).toShort(),
                                       NmeaList.at(4).mid(4,2).toShort() + 2000);

            //Retrieve TIME
            gnssController->updateTime(NmeaList.at(1).mid(0,2).toShort(),
                                       NmeaList.at(1).mid(2,2).toShort(),
                                       NmeaList.at(1).mid(4,2).toShort());

            nMainWindow->state.GNSS_MODE = 2;
        }
        else if (NmeaList.at(0).mid(3,3) == "RMC") {
            if (NmeaList.length() < 9)  continue;
            if (NmeaList.at(3).length() < 4 || NmeaList.at(5).length() < 4)    continue;

            if (NmeaList.at(1).length() >= 6) {
                gnssController->updateTime(NmeaList.at(1).mid(0,2).toShort(),
                                           NmeaList.at(1).mid(2,2).toShort(),
                                           NmeaList.at(1).mid(4,2).toShort());
            }

            // Get latitude degrees and minutes
            double Lat = -500, Lon = -500;
            int LatDeg = NmeaList.at(3).mid(0,2).toInt();
            double LatMin = NmeaList.at(3).mid(2).toDouble();
            if (NmeaList.at(3) != "")       Lat = LatDeg + LatMin/60;

            // Get N/S Field
            if (NmeaList.at(4) == 'S')      Lat *= -1.0;

            // Get longitude degrees and minutes
            int LonDeg = NmeaList.at(5).mid(0,3).toInt();
            double LonMin = NmeaList.at(5).mid(3).toDouble();
            if (NmeaList.at(5) != "")       Lon = LonDeg + LonMin/60;

            // Get E/W Field
            if (NmeaList.at(6) == 'W')      Lon *= -1.0;
            gnssController->updateCoords(Lat, Lon, ping->pingNumCurrent);

            if (NmeaList.at(7) != "")       gnssController->updateSpeed(NmeaList.at(7).toDouble());
            if (NmeaList.at(8) != "")       gnssController->updateHeading(NmeaList.at(8).toDouble());

            if (NmeaList.at(9).length() >= 6)
                gnssController->updateDate(NmeaList.at(9).mid(0,2).toShort(),
                                           NmeaList.at(9).mid(2,2).toShort(),
                                           NmeaList.at(9).mid(4,2).toShort() + 2000);

            if (gnssController->validCoords)
                ui->label_location->setText("Device connected. Current Location: " + QString::number(gnssController->Latitude(), 'g', 9) + ", "
                                            + QString::number(gnssController->Longitude(), 'g', 9));
            if (gnssController->validHeading)       gnssController->HeadingType = 1;
            if (gnssController->validSpeed)         gnssController->SpeedType = 1;
            nMainWindow->state.GNSS_MODE = 2;
            if (nMainWindow->state.RT_MODE == 0 && nMainWindow->state.GUI_MODE < 2)     nMainWindow->Gnss_Update();
        }
        else if (NmeaList.at(0).mid(3,3) == "HDT") {
            if (NmeaList.length() < 3)  continue;

            if (NmeaList.at(1) != "")       gnssController->updateHeading(NmeaList.at(1).toDouble());
            if (gnssController->validHeading)       gnssController->HeadingType = 1;
            nMainWindow->state.GNSS_MODE = 2;
            if (nMainWindow->state.RT_MODE == 0 && nMainWindow->state.GUI_MODE < 2)     nMainWindow->Gnss_Update();
        }
        else if (NmeaList.at(0).mid(3,3) == "VHW") {
            if (NmeaList.length() < 6)  continue;

            if (NmeaList.at(1) != "")       gnssController->updateHeading(NmeaList.at(1).toDouble());
            if (NmeaList.at(5) != "")       gnssController->updateSpeed(NmeaList.at(5).toDouble());
            if (gnssController->validHeading)       gnssController->HeadingType = 1;
            if (gnssController->validSpeed)         gnssController->SpeedType = 1;
            nMainWindow->state.GNSS_MODE = 2;
            if (nMainWindow->state.RT_MODE == 0 && nMainWindow->state.GUI_MODE < 2)     nMainWindow->Gnss_Update();
        }
//        else if (NmeaList.at(0).mid(3,3) == "HDG") {
//            if (NmeaList.at(0).length() < 8)  continue;
//            nMainWindow->GpsItems.Heading = NmeaList.at(1).toDouble();
//            nMainWindow->GpsItems.PingNum = (int) ping->pingNumCurrent;
//        }
        else if (NmeaList.at(0).mid(3,3) == "VTG") {
            if (NmeaList.length() < 6)  continue;

            if (NmeaList.at(1) != "")       gnssController->updateHeading(NmeaList.at(1).toDouble());
            if (NmeaList.at(5) != "")       gnssController->updateSpeed(NmeaList.at(5).toDouble());
            if (gnssController->validHeading)       gnssController->HeadingType = 1;
            if (gnssController->validSpeed)         gnssController->SpeedType = 1;
            nMainWindow->state.GNSS_MODE = 2;
            if (nMainWindow->state.RT_MODE == 0 && nMainWindow->state.GUI_MODE < 2)     nMainWindow->Gnss_Update();
        }
        else {
//            if (NmeaList.at(0).mid(0,2) != "$G") {

//            }
        }

        continue;
        /* Legacy custom format - GPS with Arduino */
        QStringList msSeparator, gpsSeparator;
        gpsSeparator = DataList.at(i).split(" ");
        if (gpsSeparator.at(0) == "DATE:") {
            //Retrieve DATE
            GpsTmp.Year =   gpsSeparator.at(1).mid(0,4).toInt();  // Year
            GpsTmp.Month =  gpsSeparator.at(1).mid(5,2).toInt(); // Month
            GpsTmp.Day =    gpsSeparator.at(1).mid(8,2).toInt(); // Day
        }
        else if (gpsSeparator.at(0) == "TIME:") {
            //Retrieve TIME
            GpsTmp.Hour =       gpsSeparator.at(1).mid(0,2).toInt();  // Hour
            GpsTmp.Minute =     gpsSeparator.at(1).mid(3,2).toInt();  // Minute
            GpsTmp.Second =     gpsSeparator.at(1).mid(6,2).toInt(); // Second
            GpsTmp.Millisecond = gpsSeparator.at(1).mid(9,3).toInt(); // Millisecond
        }
        else if (gpsSeparator.at(0) == "LOCATION:") {
            //Retrieve LOCATION
            if (gpsSeparator.length() > 4) {
                GpsTmp.Lattitude_DMS = gpsSeparator.at(1).toDouble(); // Lattitude
                GpsTmp.Lattitude_NS = (gpsSeparator.at(2));           // Lattitude Direction (North/South)
                GpsTmp.Longitude_DMS = gpsSeparator.at(3).toDouble(); // Longitude
                GpsTmp.Longitude_EW = (gpsSeparator.at(4));           // Longitude (East/West)
            }
        }
        else if (gpsSeparator.at(0) == "LOCATION" && gpsSeparator.at(1) == "(DEG):") {
            // Retrieve LOCATION (DEG)
//            GpsTmp.Lattitude_Deg = gpsSeparator.at(2).toDouble(); // Lattitude
//            GpsTmp.Longitude_Deg = gpsSeparator.at(3).toDouble(); // Longitude
//            nMainWindow->GpsItems = GpsTmp;
//            nMainWindow->GpsItems.PingNum = (int) ping->pingNumCurrent;
        }
        gpsSeparator.clear();

        //Retrieve data from COMPASS
        msSeparator = DataList.at(i).split(" "); //Separates at the 4th position (which is compass X, Y, Z) to easily retrieve values from the String
        if (msSeparator.at(0) == "COMPASS:") {
            MotionTmp.CompassX = msSeparator.at(2).toInt() - (-175);   //Compass X
            MotionTmp.CompassY = msSeparator.at(4).toInt() - (-100);   //Compass Y
            MotionTmp.CompassZ = msSeparator.at(6).toInt();   //Compass Z
            double boatHeadingCompass = 180 + (180*qAtan2(MotionTmp.CompassY, MotionTmp.CompassX))/M_PI;// - 20;
            if (boatHeadingCompass < 0) boatHeadingCompass = boatHeadingCompass + 360;
        }
        else if (msSeparator.at(0) == "ACCELEROMETER:") {
            MotionTmp.Pitch = msSeparator.at(2).toDouble(); //Pitch
            MotionTmp.Roll = msSeparator.at(4).toDouble(); //Roll
        }
        else if (msSeparator.at(0) == "GYRO:") {
            MotionTmp.GyroX = msSeparator.at(2).toInt();   //Gyro X
            MotionTmp.GyroY = msSeparator.at(4).toInt();   //Gyro Y
            MotionTmp.GyroZ = msSeparator.at(6).toInt();   //Gyro Z
        }
        else if (msSeparator.at(0) == "TIME:") {
            //Retrieve data from TIME
            MotionTmp.Hour =        msSeparator.at(0).mid(0, 2).toInt();  //Hour
            MotionTmp.Minute  =     msSeparator.at(0).mid(3, 2).toInt();  //Minute
            MotionTmp.Second  =     msSeparator.at(0).mid(6, 2).toInt(); //Second
            MotionTmp.Millisecond = msSeparator.at(0).mid(9, 3).toInt(); //Millisecond
            MotionItems = MotionTmp;
        }
        msSeparator.clear();
    }
}

