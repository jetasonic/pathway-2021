#ifndef FORMAT_H
#define FORMAT_H

#include <QDialog>
#include "qcustomplot.h"
#include<cmath>
#include <QAbstractButton>
#include<QtSql>
#include<QFileInfo>
//#include<QMESSAGEBOX>
namespace Ui {
class format;
}

class format : public QDialog
{
    Q_OBJECT



public:
    explicit format(QWidget *parent = 0);
    ~format();

    void saveFormatSettings();
    void userFormatSettings();
    void resetFormatSettings();
    void help(QHelpEvent *help);
    void updateCheckBoxes();
    //void help(QWhatsThis *whatsThis);
    //void help(QWhatsThisClickedEvent *hi);
    QSqlDatabase formatSettings;
    QString fSettings = "formatSettings.db";



    bool isOpenLf, isOpenHf, isOpen_sensorPlot, isOpen_DepthPlot, isOpen_gpsPlot;
    bool sliderBox, isStatusBarShown, isSignalPlotOpen, isGainSliderOn,
        isGridShown, isZoomBoxAlwaysOn, isDisplayCompass = 1, isCrosshairShown = 0;
    bool isOpen_TargetWidget, isOpen_imagePlot;

    double splitter1Ratio = 2.5, splitter2Ratio = 3.0, splitterToolsRatio = 0.6666, splitterSonarRatio = 0.4, splitter3Ratio = 100;
    bool splittersUpdated = 0;

    int widthLfHf, widthDepth, widthSensor, widthGnss;
    int heightTools, heightGnss, widthImage, widthTargets;
    int heightLf, heightHf;
    int widthSonar;
    int widthWindow, heightWindow;
    bool windowMaximized = false;

    int plotWidth_LfHf = 0, plotHeight_Lf = 0, plotHeight_Hf = 0;
    int plotHeight_sensor = 0, plotHeight_depth = 0;

    enum plotType {
        lf = 0x01,
        hf = 0x02,
        depth = 0x04,
        sensor = 0x08,
        gnss = 0x10,
        target = 0x20,
        image = 0x40
    };

    bool connOpenFormatSettings(){
        formatSettings = QSqlDatabase::addDatabase("QSQLITE","userConnection");
        formatSettings.setDatabaseName(fSettings);

        if(!formatSettings.open()){
            qDebug()<<("Could not Connect to Database");
            QMessageBox::critical(this,tr("Database Info"),tr("FORMAT SETTINGS DATABASE NOT FOUND!!"));
            return false;
        }
        else{
            return true;
        }
        formatSettings = QSqlDatabase::database("userConnection",true);
    }

    void connCloseFormatSettings(){
        QString con = formatSettings.connectionName();
        formatSettings.close();
        QSqlDatabase::removeDatabase(con);
    }

    void closeEvent(QCloseEvent *event);

signals:
    void showBalance(bool show);
    void showGap(bool show);
    void showGain(bool show);
    void showBottom(bool show);
    void showStatus(bool show);
    void showColorScale(bool show);
    void showSignal(bool show);
    void showAltimeter(bool show);
    void showGrid(bool show);
    void showZoomBox(bool show);
    void showCompass(bool show);
    void showCrosshair(bool show);

    void bottomTrackingZoom(bool val);
    void userMenuAccess();


private slots:

    void bottomTrackingClicked(bool val);
    void on_LFBox_clicked();
    void on_HFBox_clicked();
    void on_GraphBox_clicked();
    void on_altiBox_clicked();
    void on_GapBox_clicked();
    void on_StatusBox_clicked();
    void on_GpsBox_clicked();
    void on_colorScaleBox_clicked();
    void on_SiganlPlotBox_clicked();
    void on_gainSliderBox_clicked();
    void on_GridBox_clicked();
    void on_ZoomBox_clicked();
    void on_CompassBox_clicked();
    void on_CrosshairBox_clicked();

    void on_saveButton_clicked();
    void selectStatusBar(bool show);
    void on_resetButton_clicked();

    void showGnssClickable(bool show);
    void showHfClickable(bool show);
    void showAltimeterClickable(bool show);
    void showFormatGainSlider(bool show);
    void updateSplitterSizeInfo(double ratio1, double ratio2, double ratioTools,
                                double ratioSonar, double ratio3);

private:
    Ui::format *ui;
};



#endif // FORMAT_H
