/* systemsettings.cpp, Header comment written August 9th, 2019
 * The systemsettings object controls various variables used for processing data by the GUI
 * It can also send commands to the board that are useful for testing and development
 * It is not meant to be accessed by a normal user, and is protected by a password, and a hidden button that must be pressed to reveal the password input window
 */
#include "systemsettings.h"
#include "ui_systemsettings.h"
#include <QDebug>
#include <QKeyEvent>
#include <qmessagebox.h>

SystemSettings::SystemSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SystemSettings)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window);

    setUpWidgetsAndVisuals();

    this->setWindowFlags(this->windowFlags() &~Qt::WindowContextHelpButtonHint|Qt::WindowCloseButtonHint|Qt::MSWindowsFixedSizeDialogHint);
    this->setWindowTitle("System Config");
    //window()->setFixedSize(window()->sizeHint());
  // this->setFixedSize(this->sizeHint());

    //ui->tabWidget->setFixedSize(this->width(),this->height());
//    ui->config_range->setText("60");
//    ui->config_range_HF->setText("20");

    //Connections to check if any changes are made in the Settings
    //The GUI will consider clicking a button that is already checked/clicked a change
    //This may be fixed when more time is available
        //Config
    connect(ui->skipSamples_box, SIGNAL(textEdited(const QString &)), this, SLOT(configTabChangeDetected()));
    connect(ui->sonarPositionBox, SIGNAL(textEdited(const QString &)), this, SLOT(configTabChangeDetected()));
    connect(ui->thresholdSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(configTabChangeDetected()));
    connect(ui->thresholdValBox, SIGNAL(textEdited(const QString &)), this, SLOT(configTabChangeDetected()));
    connect(ui->noiseTEdit, SIGNAL(textEdited(const QString &)), this, SLOT(configTabChangeDetected()));
    connect(ui->color_resolution_box, SIGNAL(currentIndexChanged(int)), this, SLOT(configTabChangeDetected()));
    connect(ui->crossPt_avg_No, SIGNAL(clicked()), this, SLOT(configTabChangeDetected()));
    connect(ui->crossPt_avg_yes, SIGNAL(clicked()), this, SLOT(configTabChangeDetected()));
    connect(ui->systemAngle, SIGNAL(textEdited(const QString &)), this, SLOT(configTabChangeDetected()));
    connect(ui->systemAngle_2, SIGNAL(textEdited(const QString &)), this, SLOT(configTabChangeDetected()));
    connect(ui->Alt_running_length, SIGNAL(currentIndexChanged(int)), this, SLOT (send_alt_length_limit(int)));
    connect(ui->Yaw_length, SIGNAL(currentIndexChanged(int)), this, SLOT (send_yaw_limit(int)));
      //Receiver
    connect(ui->gain_mult_slider, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->add_gain, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->sub_gain, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_8, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_9, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_10, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->adc1, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_15, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_16, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_17, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->adc2, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_22, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_23, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_24, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->adc3, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_29, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_30, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_31, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->adc4, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->commonBox, SIGNAL(stateChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_32, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_33, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_34, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->adc, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_36, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->radioButton_37, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->att_slider, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->LFOn_btn, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->HFOn_btn, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->transOn_btn, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->transOff_btn, SIGNAL(clicked()), this, SLOT(receiverTabChangeDetected()));
    connect(ui->depthBox, SIGNAL(stateChanged(int)), this, SLOT(receiverTabChangeDetected()));
    connect(ui->gainAddBox, SIGNAL(valueChanged(int)), this, SLOT(receiverTabChangeDetected()));
        //Compression
    connect(ui->ping30,  SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
    connect(ui->ping60,  SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
    connect(ui->ping120, SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
    connect(ui->ping180, SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
    connect(ui->ping240, SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
    connect(ui->ping360, SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
    connect(ui->ping480, SIGNAL(textEdited(const QString &)), this, SLOT(compressionTabChangeDetected()));
        //Filter
    connect(ui->filterBox, SIGNAL(stateChanged(int)), this, SLOT(filterTabChangeDetected()));
    connect(ui->filterSizeBox, SIGNAL(valueChanged(int)), this, SLOT(filterTabChangeDetected()));

    //dynamic range
    //emit send_dynamic_range((ui->config_range->text()).toInt(),(ui->config_range_HF->text()).toInt());

//    startConfigDatabase();
}

SystemSettings::~SystemSettings()
{
    delete ui;
}

void SystemSettings :: setUpWidgetsAndVisuals() {

    ui->thresholdSelection->setCurrentIndex(0);
    ui->thresholdValBox->setEnabled(false);
    on_color_resolution_box_currentIndexChanged(6);
    ui->att_slider->setPageStep(1);
    ui->att_slider->setRange(0,7);

    ui->radioButton->hide();
    ui->radioButton_2->hide();
    ui->adc1->setRange(0,30);
    ui->adc2->setRange(0,30);
    ui->adc3->setRange(0,30);
    ui->adc4->setRange(0,30);
    ui->adc->setRange(0,30);

    ui->gain_mult_slider->setPageStep(1);
    ui->gain_mult_slider->setRange(0, 7);
    emit sendGainVal(2);
    ui->noiseTEdit->setValidator(new QDoubleValidator(-99999.0, 99999.0, 4, ui->noiseTEdit));

    QColor white;
    white.setRgb(255,255,255);
    ui->groupBox_7->setStyleSheet("color: white");
    ui->tab_6->setStyleSheet("color:white");
    ui->tab_2->setStyleSheet("color:white");
    ui->tab_3->setStyleSheet("color:white");

    QColor gray;
    gray.setRgb(128,128,128);
    ui->groupBox_13->setStyleSheet("color:gray");

    ui->radioButton_32->setEnabled(false);
    ui->radioButton_33->setEnabled(false);
    ui->radioButton_34->setEnabled(false);
    ui->adc->setEnabled(false);

    ui->systemAngle->setValidator(new QIntValidator(20,90,this));
    ui->systemAngle_2->setValidator(new QIntValidator(0,90,this));

    ui->systemOk->hide();
    ui->systemAngle->setEnabled(false);
    ui->systemAngle_2->setEnabled(false);

    ui->calComplete->hide();
    ui->northDir->hide();
    ui->southDir->hide();
    ui->eastDir->hide();
    ui->westDir->hide();
    ui->xCoord->hide();
    ui->yCoord->hide();
    ui->n1->hide();
    ui->n2->hide();
    ui->n3->hide();
    ui->n4->hide();
    ui->n5->hide();
    ui->n6->hide();
    ui->n7->hide();
    ui->n8->hide();
}

void SystemSettings :: on_closeBtn_clicked() {
        this->close();
}

/**
 * @brief SystemSettings::closeEvent
 * @param event
 * Closes SystemSetting window when the
 * user clicks the Close button or the X.
 * If the user has not saved their values
 * or if the user has not finished the
 * calibration a warning box will show up.
 */
void SystemSettings :: closeEvent(QCloseEvent *event) {

    //This is used to tell the user what tabs they made changes in
    QString changes = "Do you want to save changes? \n\n   Changes made in:";
    for (int i = 0; i < tabChanges.size(); i++)
    {
        if (tabChanges[i] != NULL)
        {
            changes += "\n        -" + tabChanges[i];
        }
    }

   /* if (calibrateActive)
    {
        event->ignore();
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this, "Warning" , "Calibration still active. Close anyway?", QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes)
        {
            event->accept();
        }
    }

    else*/ if (changeDetected)
    {
        event->ignore();
        QMessageBox::StandardButton reply;
           reply = QMessageBox::warning(this, "Warning", changes, QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::No)
        {
            event->accept();
            retrieveConfigDatabaseValues();
        }
        else
        {
            on_applyAllBtn_clicked();
            event->accept();
        }
        changeDetected = false;
        tabChanges.clear();
    }
}

/**
 * @brief SystemSettings::startConfigDatabase
 * Starts up the entire config database. It
 * is called through a SIGNAL/SLOT from the
 * MainWindow upon startup.
 */
void SystemSettings :: startConfigDatabase() {
    configData = QSqlDatabase::addDatabase("QSQLITE", "userConfigConnection"); //Sets up connection
    configData.setDatabaseName("configSettings.db");                                        //Sets the database 'configData' to have the name 'configSettings.db', as 'configSettings.db' is the database name
    configData = QSqlDatabase::database("userConfigConnection", true);                      //Connection is now open

    if (QSqlDatabase::contains("userConfigConnection"))                                     //Outputs if it the connection is open or not
    {
        qDebug() << "Config Database open";
        retrieveConfigDatabaseValues(); //This retrieves the user's saved values from the database
    }
    else
    {
        qDebug() << "Config Database connection error";
    }
}

/**
 * @brief SystemSettings::setConfigTabDefaults
 * Changes the values in the config tab to the
 * original defaults that are stored in the
 * database. It is called when the resetConfig
 * button is clicked.
 */
void SystemSettings :: resetConfigTab() {

    QSqlQueryModel *configTabModel = new QSqlQueryModel();                                  //Creates a new query model for the configTab table
    QSqlQuery *configTabQuery = new QSqlQuery(configData);                                               //Create a new query for the database
    configTabQuery->prepare("SELECT * FROM configTab ");  //'configTab' is name of table in database     //Prepare to select data from it
    configTabQuery->exec();                                                                              //Begin
    configTabModel->setQuery(*configTabQuery);                                                           //Set the query model to the query

    if(configTabModel->query().exec())
    {                                                                  //If the configTabModel is successful, it can start reading values from the configTab table
        //Sample skip
        QString sampleSkip = configTabModel->record(0).value("SampToSkip").toString();
        receiveSampleSkipFromDB(sampleSkip);

        //Distance to sea surface
        QString seaSurfDist = configTabModel->record(0).value("SeaSurfDis").toString();
        receiveSeaSurfDistFromDB(seaSurfDist);

        //Nx calculation //Since the thresholdValBox's value in SystemSettings is dependent on the Nx calculation, thresholdValBox's value does not need to be retrieved from the database
        QString indexLabel = configTabModel->record(0).value("NxT").toString();
        if (indexLabel == "Maximum")
        {
            ui->thresholdSelection->setCurrentIndex(0);
            on_thresholdSelection_currentIndexChanged(0);
        }
        else if (indexLabel == "Cross Point")
        {
            ui->thresholdSelection->setCurrentIndex(1);
            on_thresholdSelection_currentIndexChanged(1);
        }
        else if (indexLabel == "Threshold")
        {
            ui->thresholdSelection->setCurrentIndex(2);
            on_thresholdSelection_currentIndexChanged(2);
        }

        //Noise threshold
        double r = configTabModel->record(0).value("NoiseThresh").toInt();
        changeNoiseThresh(r);

        //Resolution
        int ind = configTabModel->record(0).value("ResolutionInd").toInt();
        on_color_resolution_box_currentIndexChanged(ind);

        //Averaging //Since it's a boolean and it always starts with off, we don't need to retrieve from database
       // int averagingValue = model->record(0).value("Averaging").toInt();
        on_crossPt_avg_No_clicked();

        //Compression factor
        int compFactor = configTabModel->record(0).value("CompFactor").toInt();
        on_compBox_valueChanged(compFactor);

        ui->Alt_running_length->setCurrentIndex(configTabModel->record(0).value("Alt_Avg_Length").toInt());
        ui->Yaw_length->setCurrentIndex(configTabModel->record(0).value("Yaw_Avg_Length").toInt());
        emit send_alt_length(ui->Alt_running_length->currentIndex());
        emit send_yaw_length(ui->Yaw_length->currentIndex());

        //Horizontal angle
        //(the part of the config menu doesn't seem finished, postpone reading from database for now)

        //Vertical angle
        //(the part of the config menu doesn't seem finished, postpone reading from database for now)

        delete configTabQuery;
    }
    else
    {
        qDebug() << "ConfigTab error: " <<  configTabQuery->lastError();
    }

    delete configTabModel;
}

/**
 * @brief SystemSettings::setReceiverTabDefaults
 * Changes the values in the receiver tab to
 * the original defaults that are stored in the
 * database. It is called when the resetReceiver
 * button is clicked.
 */
void SystemSettings :: resetReceiverTab() {

    QSqlQueryModel *receiverTabModel = new QSqlQueryModel();
    QSqlQuery *receiverTabQuery = new QSqlQuery(configData);
    receiverTabQuery->prepare("SELECT * FROM receiverTab ");
    receiverTabQuery->exec();
    receiverTabModel->setQuery(*receiverTabQuery);

    if (receiverTabModel->query().exec())
    {
        //Demodulator force
        int fValue = receiverTabModel->record(0).value("Demodulator").toInt();
        receiveDemoForce(fValue);

        //Demodulator scale
        QString scaleBtn = receiverTabModel->record(0).value("Scale").toString(); //The String in this field must be either "+ 12db" or "0 dB" because the slot checks for one of these two
        receiveScaleButton(scaleBtn);

        //Ch1 button and slider
        QString ch1Btn = receiverTabModel->record(0).value("Ch1_Radio").toString(); //The String in this field must be "15dB", "18dB", or "24dB" because the slot checks for only these three
        receiveCh1Button(ch1Btn);
        int ch1Slider = receiverTabModel->record(0).value("Ch1_Slider").toInt();
        receiveCh1Slider(ch1Slider);

        //Ch2 button and slider
        QString ch2Btn = receiverTabModel->record(0).value("Ch2_Radio").toString(); //The String in this field must be "15dB", "18dB", or "24dB" because the slot checks for only these three
        receiveCh2Button(ch2Btn);
        int ch2Slider = receiverTabModel->record(0).value("Ch2_Slider").toInt();
         receiveCh2Slider(ch2Slider);

        //Ch3 button and slider
        QString ch3Btn = receiverTabModel->record(0).value("Ch3_Radio").toString(); //The String in this field must be "15dB", "18dB", or "24dB" because the slot checks for only these three
        receiveCh3Button(ch3Btn);
        int ch3Slider = receiverTabModel->record(0).value("Ch3_Slider").toInt();
        receiveCh3Slider(ch3Slider);

        //Ch4 button and slider
        QString ch4Btn = receiverTabModel->record(0).value("Ch4_Radio").toString(); //The String in this field must be "15dB", "18dB", or "24dB" because the slot checks for only these three
        receiveCh4Button(ch4Btn);
        int ch4Slider = receiverTabModel->record(0).value("Ch4_Slider").toInt();
        receiveCh4Slider(ch4Slider);

        //Use common checkbox
        QString check = receiverTabModel->record(0).value("UseCommon").toString(); //The String in this field must be either "Yes" or "No" because the slot checks for one of these two
        receiveCheckOnOff(check);

        //Common button and slider
        QString cBtn = receiverTabModel->record(0).value("C_Radio").toString();     //The String in this field must be "15dB", "18dB", or "24dB" because the slot checks for only these three
        receiveCBtn(cBtn);
        int cSlider = receiverTabModel->record(0).value("C_Slider").toInt();
        receiveCSlider(cSlider);

        //PGA button
        int pButton = receiverTabModel->record(0).value("PGA").toInt(); //The int in this field must be 24 or 30 because the slot checks for one of these two
        receivePButton(pButton);

        //Attenutation
        int att = receiverTabModel->record(0).value("Attenuation").toInt();
        receiveAtt(att);

        //Transmitter buttons
        QString transm = receiverTabModel->record(0).value("Transmitter").toString(); //The String in this field must be "LF", "HF", "Both On", or "Both Off" because the slot checks for one of these four
        receiveTransm(transm);

        //Depth sounder checkbox
        QString depSounder = receiverTabModel->record(0).value("DepthSounder").toString(); //The String in this field must be either "Yes" or "No" because the slot checks for one of these two
        receiveDepSounder(depSounder);

        //Gain additive
        int gain = receiverTabModel->record(0).value("GainAdditive").toInt();
        receiveGain(gain);

        delete receiverTabQuery;
    }
    else
    {
        qDebug() << "ReceiverTab error: " << receiverTabQuery->lastError();
    }

    delete receiverTabModel;
}

/**
 * @brief SystemSettings::setCompressionTabDefaults
 * Changes the values in the compression tab to
 * the original defaults that are stored in the
 * database. It is called when the resetReceiver
 * button is clicked.
 */
void SystemSettings :: resetCompressionTab() {

    QSqlQueryModel *compressionTabModel = new QSqlQueryModel();
    QSqlQuery *compressionTabQuery = new QSqlQuery(configData);
    compressionTabQuery->prepare("SELECT * FROM compressionTab ");
    compressionTabQuery->exec();
    compressionTabModel->setQuery(*compressionTabQuery);

    if(compressionTabModel->query().exec())
    {
        QList<int> rangesFromTable;
        QList<double> EPIsFromTable;
        QList<int> compressionsFromTable;

        for (int i = 0; i < 7; i++)
        {
            rangesFromTable.append(compressionTabModel->record(i).value("Range").toInt());
            EPIsFromTable.append(compressionTabModel->record(i).value("EPI").toInt());
            compressionsFromTable.append(compressionTabModel->record(i).value("Compression").toInt());
        }

        receiveListsFromTable(rangesFromTable, EPIsFromTable, compressionsFromTable);

        delete compressionTabQuery;
    }
    else
    {
        qDebug() << "compressionTab error: " <<  compressionTabQuery->lastError();
    }

    delete compressionTabModel;
}

/**
 * @brief SystemSettings::setFilterTabDefaults
 * Changes the values in the filter tab to the
 * original defaults that are stored in the
 * database. It is called when the resetFilter
 * button is clicked.
 */
void SystemSettings :: resetFilterTab() {

    QSqlQueryModel *filterTabModel = new QSqlQueryModel();
    QSqlQuery *filterTabQuery = new QSqlQuery(configData);
    filterTabQuery->prepare("SELECT * FROM filterTab");
    filterTabQuery->exec();
    filterTabModel->setQuery(*filterTabQuery);

    if (filterTabModel->query().exec())
    {
        //Filter off
        receiveFilterOff();

        //Filter size
        int filterSize = filterTabModel->record(0).value("FilterSize").toInt();
        receiveFilterSizeFromDB(filterSize);

        delete filterTabQuery;
    }
    else
    {
        qDebug() << "filterTab error: " << filterTabQuery->lastError();
    }

    delete filterTabModel;
}

/**
 * @brief SystemSettings::on_applyAllBtn_clicked
 * Saves the current values across all the tabs
 * to their corresponding tables in the database.
 * It is called when the Apply button is clicked.
 */
void SystemSettings :: on_applyAllBtn_clicked() {

    QSqlQuery insConfigQuery(configData);
    QSqlQuery insReceiverQuery(configData);
    QSqlQuery insCompressionQuery(configData);
    QSqlQuery insFilterQuery(configData);

    //Store current Config values into the database
//    insConfigQuery.prepare("DELETE FROM configTab WHERE Type='UserValues'"); //This deletes the record holding the user's previous saved values if they're present in the database
    insConfigQuery.prepare(("DROP TABLE IF EXISTS configTab"));
    insConfigQuery.exec();

    insConfigQuery.prepare("CREATE TABLE IF NOT EXISTS configTab (Type TEXT, SampToSkip INTEGER, SeaSurfDis INTEGER, NxT TEXT, "
                           "Nx TEXT, NoiseThresh INTEGER, ResolutionInd INTEGER, Averaging TEXT, CompFactor INTEGER, "
                           "HControl INTEGER, VControl INTEGER, Alt_Avg_Length INTEGER, Yaw_Avg_Length INTEGER)");
    insConfigQuery.exec();
    insConfigQuery.prepare("INSERT INTO configTab(Type, SampToSkip, SeaSurfDis, NxT, Nx, NoiseThresh, ResolutionInd, "
                           "Averaging, CompFactor, HControl, VControl, Alt_Avg_Length, Yaw_Avg_Length)"
                           " VALUES(:t, :ss, :ssd, :nxt, :nx, :nt, :ri, :a, :cf, :hc, :vc, :Alt_Length, :Yaw_Length)");
    insConfigQuery.bindValue(":t", "DefaultValues");
    insConfigQuery.bindValue(":ss", 200);
    insConfigQuery.bindValue(":ssd", 0);
    insConfigQuery.bindValue(":nxt", "Maximum");
    insConfigQuery.bindValue(":nx", 0); //May result in error if text() == "Default"
    insConfigQuery.bindValue(":nt", 4);
    insConfigQuery.bindValue(":ri", 6);
    insConfigQuery.bindValue(":a", "No");
    insConfigQuery.bindValue(":cf", 1);
    insConfigQuery.bindValue(":hc", 0);
    insConfigQuery.bindValue(":vc", 0);
    insConfigQuery.bindValue(":Alt_Length", 5);
    insConfigQuery.bindValue(":Yaw_Length", 4);
    insConfigQuery.exec();

    insConfigQuery.bindValue(":t", "UserValues"); //This is used to keep track of which record to replace - we only want to replace the user's previous values, not the default
    insConfigQuery.bindValue(":ss", ui->skipSamples_box->text().toInt());
    insConfigQuery.bindValue(":ssd", ui->sonarPositionBox->text().toInt());
    insConfigQuery.bindValue(":nxt", ui->thresholdSelection->currentText());
    insConfigQuery.bindValue(":nx", ui->thresholdValBox->text().toInt()); //May result in error if text() == "Default"
    insConfigQuery.bindValue(":nt", ui->noiseTEdit->text().toInt());
    insConfigQuery.bindValue(":ri", ui->color_resolution_box->currentIndex());
    if (ui->crossPt_avg_No->isChecked())
        insConfigQuery.bindValue(":a", "No");
    else
        insConfigQuery.bindValue(":a", "Yes");
    insConfigQuery.bindValue(":cf", ui->compBox->value());
    insConfigQuery.bindValue(":hc", ui->systemAngle->text());
    insConfigQuery.bindValue(":vc", ui->systemAngle_2->text());
    insConfigQuery.bindValue(":Alt_Length", ui->Alt_running_length->currentIndex());
    insConfigQuery.bindValue(":Yaw_Length", ui->Yaw_length->currentIndex());

    //Since .exec() both returns a true or false AND performs the exec(), the if statement below both runs it and checks if it's true or false
    //If you want the if statment gone, replace it with: insConfigQuery.exec();
    if (insConfigQuery.exec())
      qDebug() << "E- 1Worked";
    else
      qDebug() << "E- 1Failed: " << insConfigQuery.lastError();

    //Store current Receiver values into database
//    insReceiverQuery.prepare("DELETE FROM receiverTab WHERE Type='UserValues'");
    insReceiverQuery.prepare(("DROP TABLE IF EXISTS receiverTab"));
    insReceiverQuery.exec();

    QString scaleBtn, ch1Btn, ch2Btn, ch3Btn, ch4Btn, cb, cBtn, tran, depth;
    int pgav;

    insReceiverQuery.prepare("CREATE TABLE IF NOT EXISTS receiverTab (Type TEXT, Demodulator INTEGER, Scale TEXT, "
                             "Ch1_Radio TEXT, Ch1_Slider INTEGER, Ch2_Radio TEXT, Ch2_Slider INTEGER, "
                             "Ch3_Radio TEXT, Ch3_Slider INTEGER, Ch4_Radio TEXT, Ch4_Slider INTEGER, "
                             "UseCommon TEXT, C_Radio TEXT, C_Slider INTEGER, PGA INTEGER, Attenuation INTEGER, "
                             "Transmitter TEXT, DepthSounder TEXT, GainAdditive INTEGER)");
    insReceiverQuery.exec();
    insReceiverQuery.prepare("INSERT INTO receiverTab(Type, Demodulator, Scale, Ch1_Radio, Ch1_Slider, Ch2_Radio, Ch2_Slider, "
                             "Ch3_Radio, Ch3_Slider, Ch4_Radio, Ch4_Slider, UseCommon, C_Radio, C_Slider, PGA, Attenuation, "
                             "Transmitter, DepthSounder, GainAdditive) "
                             "VALUES(:t, :d, :s, :c1r, :c1s, :c2r, :c2s, :c3r, :c3s, :c4r, "
                             ":c4s, :uc, :cr, :cs, :pga, :a, :tr, :ds, :g)");
    insReceiverQuery.bindValue(":t", "DefaultValues");

    insReceiverQuery.bindValue(":d", 0);
    insReceiverQuery.bindValue(":s", "+ 12db");
    insReceiverQuery.bindValue(":c1r", "18dB");
    insReceiverQuery.bindValue(":c1s", 0);
    insReceiverQuery.bindValue(":c2r", "18dB");
    insReceiverQuery.bindValue(":c2s", 0);
    insReceiverQuery.bindValue(":c3r", "18dB");
    insReceiverQuery.bindValue(":c3s", 0);
    insReceiverQuery.bindValue(":c4r", "18dB");
    insReceiverQuery.bindValue(":c4s", 0);
    insReceiverQuery.bindValue(":uc", "No");
    insReceiverQuery.bindValue(":cr", "18dB");
    insReceiverQuery.bindValue(":cs", 0);
    insReceiverQuery.bindValue(":pga", 24);
    insReceiverQuery.bindValue(":a", 0);
    insReceiverQuery.bindValue(":tr", "Both On");
    insReceiverQuery.bindValue(":ds", "No");
    insReceiverQuery.bindValue(":g", 0);
    insReceiverQuery.exec();

    insReceiverQuery.bindValue(":t", "UserValues");

    insReceiverQuery.bindValue(":d", ui->gain_mult_slider->value());
    if (ui->add_gain->isChecked())
        scaleBtn = "+ 12db";
    else if (ui->sub_gain->isChecked())
        scaleBtn = "0 dB";
    insReceiverQuery.bindValue(":s", scaleBtn);

    if (ui->radioButton_8->isChecked())
        ch1Btn = "15dB";
    else if (ui->radioButton_9->isChecked())
        ch1Btn = "18dB";
    else if (ui->radioButton_10->isChecked())
        ch1Btn = "24dB";
    insReceiverQuery.bindValue(":c1r", ch1Btn);
    insReceiverQuery.bindValue(":c1s", ui->adc1->value());

    if (ui->radioButton_15->isChecked())
        ch2Btn = "15dB";
    else if (ui->radioButton_16->isChecked())
        ch2Btn = "18dB";
    else if (ui->radioButton_17->isChecked())
        ch2Btn = "24dB";
    insReceiverQuery.bindValue(":c2r", ch2Btn);
    insReceiverQuery.bindValue(":c2s", ui->adc2->value());

    if (ui->radioButton_22->isChecked())
        ch3Btn = "15dB";
    else if (ui->radioButton_23->isChecked())
        ch3Btn = "18dB";
    else if (ui->radioButton_24->isChecked())
        ch3Btn = "24dB";
    insReceiverQuery.bindValue(":c3r", ch3Btn);
    insReceiverQuery.bindValue(":c3s", ui->adc3->value());

    if (ui->radioButton_29->isChecked())
        ch4Btn = "15dB";
    else if (ui->radioButton_30->isChecked())
        ch4Btn = "18dB";
    else if (ui->radioButton_31->isChecked())
        ch4Btn = "24dB";
    insReceiverQuery.bindValue(":c4r", ch4Btn);
    insReceiverQuery.bindValue(":c4s", ui->adc4->value());

    if (ui->commonBox->isChecked())
        cb = "Yes";
    else
        cb = "No";
    insReceiverQuery.bindValue(":uc", cb);

    if (ui->radioButton_32->isChecked())
        cBtn = "15dB";
    else if (ui->radioButton_33->isChecked())
        cBtn = "18dB";
    else if (ui->radioButton_34->isChecked())
        cBtn = "24dB";
    insReceiverQuery.bindValue(":cr", cBtn);
    insReceiverQuery.bindValue(":cs", ui->adc->value());

    if (ui->radioButton_36->isChecked())
        pgav = 24;
    else
        pgav = 30;
    insReceiverQuery.bindValue(":pga", pgav);

    insReceiverQuery.bindValue(":a", ui->att_slider->value());

    tran = "Both On"; //Hardcoded for testing //The commented out code above is the default
    insReceiverQuery.bindValue(":tr", tran);

    if (ui->depthBox->isChecked())
        depth = "Yes";
    else
        depth = "No";
    insReceiverQuery.bindValue(":ds", depth);

    insReceiverQuery.bindValue(":g", ui->gainAddBox->value());

    if (insReceiverQuery.exec())
       qDebug() << "E- 2Worked";
    else
       qDebug() << "E- 2Failed: " << insConfigQuery.lastError();


    //Store current Compression values into database
    insCompressionQuery.prepare("DELETE FROM compressionTab WHERE Range IS NULL OR trim(Range) = ''");
    insCompressionQuery.exec();

    insCompressionQuery.prepare("INSERT INTO compressionTab (EPI) VALUES (:e)");
    insCompressionQuery.bindValue(":e", ui->ping30->text().toInt());
        insCompressionQuery.next();
        insCompressionQuery.exec();
    insCompressionQuery.bindValue(":e", ui->ping60->text().toInt());
        insCompressionQuery.next();
        insCompressionQuery.exec();
    insCompressionQuery.bindValue(":e", ui->ping120->text().toInt());
        insCompressionQuery.next();
        insCompressionQuery.exec();
    insCompressionQuery.bindValue(":e", ui->ping180->text().toInt());
        insCompressionQuery.next();
        insCompressionQuery.exec();
    insCompressionQuery.bindValue(":e", ui->ping240->text().toInt());
        insCompressionQuery.next();
        insCompressionQuery.exec();
    insCompressionQuery.bindValue(":e", ui->ping360->text().toInt());
        insCompressionQuery.next();
        insCompressionQuery.exec();
    insCompressionQuery.bindValue(":e", ui->ping480->text().toInt());
        insCompressionQuery.exec();

    if (insCompressionQuery.exec())
       qDebug() << "E- 3Worked";
    else
       qDebug() << "E- 3Failed: " << insConfigQuery.lastError();



    //Store current Filter values into database
    insFilterQuery.prepare("DELETE FROM filterTab WHERE Type='UserValues'");
    insFilterQuery.exec();

    insFilterQuery.prepare("INSERT INTO filterTab (Type, FilterEnabled, FilterSize) values (:t, :e, :s)");

    insFilterQuery.bindValue(":t", "UserValues");
    if (ui->filterBox->isChecked())
        insFilterQuery.bindValue(":e", "Yes");
    else
        insFilterQuery.bindValue(":e", "No");

    insFilterQuery.bindValue(":s", ui->filterSizeBox->value());

    if (insFilterQuery.exec())
       qDebug() << "E- 4Worked";
    else
       qDebug() << "E- 4Failed: " << insConfigQuery.lastError();

    if(ui->sim_activate->isChecked()){
        emit set_simulation(0);
    }
    else{
        emit set_simulation(1);
    }

    changeDetected = false;
    this->hide();
  //this->close();

    emit send_dynamic_range((ui->config_range->text()).toInt(),(ui->config_range_HF->text()).toInt());

}

/**
 * @brief SystemSettings::on_saveButton_clicked
 * The apply button on the system settings UI
 * Applies changes made to controls
 */
void SystemSettings :: on_saveButton_clicked()
{
//    if(ui->systemAngle->text().toDouble() < 20){
//        QMessageBox::critical(this, "Invalid Input", "enter angle between 20° and 90°");
//    }
   // else{
        double degree = ui->systemAngle->text().toDouble();
        emit degreeChanged(degree);

    QString num  = ui->thresholdValBox->text();
    int val = num.toInt();
    QString s = ui->skipSamples_box->text();
    int skipSamples = s.toInt();
    QString s1 = ui->sonarPositionBox->text();
    int position = s1.toInt();
    //get expected ping interval from compression table
    double ping30 = ui->ping30->text().toDouble();
    double ping60 = ui->ping60->text().toDouble();
    double ping120 = ui->ping120->text().toDouble();
    double ping180 = ui->ping180->text().toDouble();
    double ping240 = ui->ping240->text().toDouble();
    double ping360 = ui->ping360->text().toDouble();
    double ping480 = ui->ping480->text().toDouble();

    //get compression rate from the compression table
    double compression30 = ui->compression30->text().toDouble();
    double compression60 = ui->compression60->text().toDouble();
    double compression120 = ui->compression120->text().toDouble();
    double compression180 = ui->compression180->text().toDouble();
    double compression240 = ui->compression240->text().toDouble();
    double compression360 = ui->compression360->text().toDouble();
    double compression480 = ui->compression480->text().toDouble();

    //send the expected compression rate to the mainWindow
    emit compressionRate30(compression30);
    emit compressionRate60(compression60);
    emit compressionRate120(compression120);
    emit compressionRate180(compression180);
    emit compressionRate240(compression240);
    emit compressionRate360(compression360);
    emit compressionRate480(compression480);

    //send the expected ping interval to the mainWindow
    emit expectPing30(ping30);
    emit expectPing60(ping60);
    emit expectPing120(ping120);
    emit expectPing180(ping180);
    emit expectPing240(ping240);
    emit expectPing360(ping360);
    emit expectPing480(ping480);

    emit setSkipSamples(skipSamples);
    emit setSonarPosition(position);
    emit useThreshold(val);
    emit sendNoiseThresh(ui->noiseTEdit->text().toDouble());
   // }
}

/**
 * @brief SystemSettings::retrieveConfigDatabaseValues
 * Retrieves the user's saved values from the
 * corresponding tables in the database and
 * populates the tabs with those values. It is
 * called in the startConfigDatabase function
 * and when the user chooses not to save their
 * changes.
 */
void SystemSettings :: retrieveConfigDatabaseValues() {

    QSqlQuery fillConfig(configData);
    QSqlQuery fillReceiver(configData);
    QSqlQuery fillCompression(configData);
    QSqlQuery fillFilter(configData);

    //Fill config tab with user values from database
    fillConfig.prepare("SELECT * FROM configTab ");
    fillConfig.exec();
    QSqlQueryModel configModel;
    configModel.setQuery(fillConfig);

    if (configModel.query().exec())
    {
        //Sample skip
        receiveSampleSkipFromDB(configModel.record(1).value("SampToSkip").toString());

        //Distance to sea surface
        receiveSeaSurfDistFromDB(configModel.record(1).value("SeaSurfDis").toString());
        //ui->sonarPositionBox->setText(configModel.record(1).value("SeaSurfDis").toString());

        //Nx calculation
        if (configModel.record(1).value("NxT").toString() == "Maximum")
        {
            ui->thresholdSelection->blockSignals(true); //This prevents the signal from being called, which is only supposed to be called on a 'user' change
            ui->thresholdSelection->setCurrentIndex(0);
            on_thresholdSelection_currentIndexChanged(0);
            ui->thresholdSelection->blockSignals(false);
        }
        else if (configModel.record(1).value("NxT").toString() == "Cross Point")
        {
            ui->thresholdSelection->blockSignals(true);
            ui->thresholdSelection->setCurrentIndex(1);
            on_thresholdSelection_currentIndexChanged(1);
            ui->thresholdValBox->setText(configModel.record(1).value("Nx").toString());
            ui->thresholdSelection->blockSignals(false);
        }
        else if (configModel.record(1).value("NxT").toString() == "Threshold")
        {
            ui->thresholdSelection->blockSignals(true);
            ui->thresholdSelection->setCurrentIndex(2);
            on_thresholdSelection_currentIndexChanged(2);
            ui->thresholdValBox->setText(configModel.record(1).value("Nx").toString());
            ui->thresholdSelection->blockSignals(false);
        }

        //Noise threshold
       // ui->noiseTEdit->setText(configModel.record(1).value("NoiseThresh").toString());
        changeNoiseThresh(configModel.record(1).value("NoiseThresh").toDouble());

        //Resolution
        switch(configModel.record(1).value("ResolutionInd").toInt())
        {
            case 0:
                ui->color_resolution_box->blockSignals(true); //This prevents the signal from being called, which is only supposed to be called on a 'user' change
                ui->color_resolution_box->setCurrentIndex(0);
                on_color_resolution_box_currentIndexChanged(0);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 1:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(1);
                on_color_resolution_box_currentIndexChanged(1);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 2:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(2);
                on_color_resolution_box_currentIndexChanged(2);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 3:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(3);
                on_color_resolution_box_currentIndexChanged(3);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 4:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(4);
                on_color_resolution_box_currentIndexChanged(4);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 5:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(5);
                on_color_resolution_box_currentIndexChanged(5);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 6:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(6);
                on_color_resolution_box_currentIndexChanged(6);
                ui->color_resolution_box->blockSignals(false);
                break;
            case 7:
                ui->color_resolution_box->blockSignals(true);
                ui->color_resolution_box->setCurrentIndex(7);
                on_color_resolution_box_currentIndexChanged(7);
                ui->color_resolution_box->blockSignals(false);
                break;
        }

        //Averaging
        if (configModel.record(1).value("Averaging").toString() == "No")
            on_crossPt_avg_No_clicked();
        else if (configModel.record(1).value("Averaging").toString() == "Yes")
            on_crossPt_avg_yes_clicked();

        //Compression factor
        //ui->compBox->setValue(configModel.record(1).value("CompFactor").toInt());
        on_compBox_valueChanged(configModel.record(1).value("CompFactor").toInt());

        //Horizontal angle
        ui->systemAngle->setText(configModel.record(1).value("HControl").toString());
        //Vertical angle
        ui->systemAngle_2->setText(configModel.record(1).value("VControl").toString());

        if (!configModel.record(1).value("Alt_Avg_Length").isNull())
            ui->Alt_running_length->setCurrentIndex(configModel.record(1).value("Alt_Avg_Length").toInt());
        if (!configModel.record(1).value("Yaw_Avg_Length").isNull())
            ui->Yaw_length->setCurrentIndex(configModel.record(1).value("Yaw_Avg_Length").toInt());
        emit send_alt_length(ui->Alt_running_length->currentIndex());
        emit send_yaw_length(ui->Yaw_length->currentIndex());

    }
    else
    {
        qDebug() << "E- R1Error: " << fillConfig.lastError();
    }

    //Fill receiver tab with user values from database
    fillReceiver.prepare("SELECT * FROM receiverTab ");
    fillReceiver.exec();
    QSqlQueryModel receiverModel;
    receiverModel.setQuery(fillReceiver);

    if (receiverModel.query().exec())
    {
        //Demodulator force
        ui->gain_mult_slider->blockSignals(true); //This prevents the signal from being called, which is only supposed to be called on a 'user' change
        receiveDemoForce(receiverModel.record(1).value("Demodulator").toInt());
        ui->gain_mult_slider->blockSignals(false);

        //Demodulator scale
        receiveScaleButton(receiverModel.record(1).value("Scale").toString());

        //Ch1 button and slider
        receiveCh1Button(receiverModel.record(1).value("Ch1_Radio").toString());
        ui->adc1->blockSignals(true);
        receiveCh1Slider(receiverModel.record(1).value("Ch1_Slider").toInt());
        ui->adc1->blockSignals(false);

        //Ch2 button and slider
        receiveCh2Button(receiverModel.record(1).value("Ch2_Radio").toString());
        ui->adc2->blockSignals(true);
        receiveCh2Slider(receiverModel.record(1).value("Ch2_Slider").toInt());
        ui->adc2->blockSignals(false);

        //Ch3 button and slider
        receiveCh3Button(receiverModel.record(1).value("Ch3_Radio").toString());
        ui->adc3->blockSignals(true);
        receiveCh3Slider(receiverModel.record(1).value("Ch3_Slider").toInt());
        ui->adc3->blockSignals(false);

        //Ch4 button and slider
        receiveCh4Button(receiverModel.record(1).value("Ch4_Radio").toString());
        ui->adc4->blockSignals(true);
        receiveCh4Slider(receiverModel.record(1).value("Ch4_Slider").toInt());
        ui->adc4->blockSignals(false);

        //Use common checkbox
        ui->commonBox->blockSignals(true);
        receiveCheckOnOff(receiverModel.record(1).value("UseCommon").toString());
        ui->commonBox->blockSignals(false);

        //Common button and slider
        receiveCBtn(receiverModel.record(1).value("C_Radio").toString());
        ui->adc->blockSignals(true);
        receiveCSlider(receiverModel.record(1).value("C_Slider").toInt());
        ui->adc->blockSignals(false);

        //PGA button
        receivePButton(receiverModel.record(1).value("PGA").toInt());

        //Attenutation
        ui->att_slider->blockSignals(true);
        receiveAtt(receiverModel.record(1).value("Attenuation").toInt());
        ui->att_slider->blockSignals(false);

        //Transmitter buttons
        receiveTransm(receiverModel.record(0).value("Transmitter").toString());

        //Depth sounder checkbox
        ui->depthBox->blockSignals(true);
        receiveDepSounder(receiverModel.record(1).value("DepthSounder").toString());
        ui->depthBox->blockSignals(false);

        //Gain additive
        receiveGain(receiverModel.record(1).value("GainAdditive").toInt());

    }
    else
    {
        qDebug() << "E- R2Error: " << fillReceiver.lastError();
    }

    //Fill compression tab with user values from database
    fillCompression.prepare("SELECT * FROM compressionTab ");
    fillCompression.exec();
    QSqlQueryModel compModel;
    compModel.setQuery(fillCompression);

    if(compModel.query().exec())
    {
        ui->range30->setText(compModel.record(0).value("Range").toString());
        ui->range60->setText(compModel.record(1).value("Range").toString());
        ui->range120->setText(compModel.record(2).value("Range").toString());
        ui->range180->setText(compModel.record(3).value("Range").toString());
        ui->range240->setText(compModel.record(4).value("Range").toString());
        ui->range360->setText(compModel.record(5).value("Range").toString());
        ui->range480->setText(compModel.record(6).value("Range").toString());
        ui->range30->setEnabled(false);
        ui->range60->setEnabled(false);
        ui->range120->setEnabled(false);
        ui->range180->setEnabled(false);
        ui->range240->setEnabled(false);
        ui->range360->setEnabled(false);
        ui->range480->setEnabled(false);

        ui->ping30->setText(compModel.record(7).value("EPI").toString());
        ui->ping60->setText(compModel.record(8).value("EPI").toString());
        ui->ping120->setText(compModel.record(9).value("EPI").toString());
        ui->ping180->setText(compModel.record(10).value("EPI").toString());
        ui->ping240->setText(compModel.record(11).value("EPI").toString());
        ui->ping360->setText(compModel.record(12).value("EPI").toString());
        ui->ping480->setText(compModel.record(13).value("EPI").toString());

        QVector<double> values(7); //Send these EPIs to the mainWindow for EPI testing
        for (int i = 0; i < 7; i++)
        {
            values[i] = compModel.record(i + 7).value("EPI").toDouble();
        }
        emit sendEPIs(values);

        //set default values for compression rate
        ui->compression30->setText(compModel.record(0).value("Compression").toString());
        ui->compression60->setText(compModel.record(1).value("Compression").toString());
        ui->compression120->setText(compModel.record(2).value("Compression").toString());
        ui->compression180->setText(compModel.record(3).value("Compression").toString());
        ui->compression240->setText(compModel.record(4).value("Compression").toString());
        ui->compression360->setText(compModel.record(5).value("Compression").toString());
        ui->compression480->setText(compModel.record(6).value("Compression").toString());
        //grayed out the compression box
        ui->compression30->setEnabled(false);
        ui->compression60->setEnabled(false);
        ui->compression120->setEnabled(false);
        ui->compression180->setEnabled(false);
        ui->compression240->setEnabled(false);
        ui->compression360->setEnabled(false);
        ui->compression480->setEnabled(false);

    }
    else
    {
        qDebug() << "E- R3Error: " << fillCompression.lastError();
    }

    //Fill filter tab with user values from database
    fillFilter.prepare("SELECT * FROM filterTab ");
    fillFilter.exec();
    QSqlQueryModel filterModel;
    filterModel.setQuery(fillFilter);

    if (filterModel.query().exec())
    {
        //Filter on/off
        ui->filterBox->blockSignals(true);
        if (filterModel.record(1).value("FilterEnabled").toString() == "No")
        {
            receiveFilterOff();
        }
        else if (filterModel.record(1).value("FilterEnabled").toString() == "Yes")
        {
            ui->filterBox->setChecked(true);
            on_filterBox_clicked();
        }
        ui->filterBox->blockSignals(false);

        //Filter size
        ui->filterSizeBox->blockSignals(true);
        on_filterSizeBox_valueChanged(filterModel.record(1).value("FilterSize").toInt());
        ui->filterSizeBox->blockSignals(false);

    }
    else
    {
        qDebug() << "E- R4Error: " << fillFilter.lastError();
    }
}

void SystemSettings :: receive_default_range(int range, int range_HF){

    ui->config_range->setText(QString::number(range));
    ui->config_range_HF->setText(QString::number(range_HF));
}

/**
 * @brief SystemSettings::closeConfigDatabase
 * Closes the database. It is called through
 * a SIGNAL/SLOT from the MainWindow upon
 * closing the GUI.
 */
void SystemSettings :: closeConfigDatabase() {
    configData.close();
    configData.removeDatabase("configSettings.db");
}

void SystemSettings :: on_resetConfigBtn_clicked() {
    resetConfigTab();
}

void SystemSettings :: on_resetReceiverBtn_clicked() {
    resetReceiverTab();
}

void SystemSettings :: on_resetCompressionBtn_clicked() {
    resetCompressionTab();
}

void SystemSettings :: on_resetFitlerBtn_clicked() {
    resetFilterTab();
}

/**
 * @brief SystemSettings::configTabChangeDetected
 * Slot for the Widget connection Signals. When a
 * change is made in any Widget in the config tab
 * this slot is called.
 */
void SystemSettings :: configTabChangeDetected() {

    //Still need to account for when reset is clicked
    //  or maybe not, seeing how the user may want to
    //  revert back to their old changes after hitting
    //  reset

    //This also occurs because of the changes that are made on startup

        if (!tabChanges.contains("Config "))
            tabChanges.append("Config ");

    qDebug() << "Change detected in Config";
    changeDetected = true;
}

/**
 * @brief SystemSettings::receiverTabChangeDetected
 * Slot for the Widget connection Signals. When a
 * change is made in any Widget in the receiver tab
 * this slot is called.
 */
void SystemSettings :: receiverTabChangeDetected() {

        if (!tabChanges.contains("Receiver "))
            tabChanges.append("Receiver ");

    qDebug() << "Change detected in Receiver";
    changeDetected = true;
}

/**
 * @brief SystemSettings::compressionTabChangeDetected
 * Slot for the Widget connection Signals. When a
 * change is made in any Widget in the compression tab
 * this slot is called.
 */
void SystemSettings :: compressionTabChangeDetected() {

        if (!tabChanges.contains("Compression "))
            tabChanges.append("Compression ");

    qDebug() << "Change detected in Compression";
    changeDetected = true;
}

/**
 * @brief SystemSettings::filterTabChangeDetected
 * Slot for the Widget connection Signals. When a
 * change is made in any Widget in the filter tab
 * this slot is called.
 */
void SystemSettings :: filterTabChangeDetected() {

        if (!tabChanges.contains("Filter "))
            tabChanges.append("Filter ");

    qDebug() << "Change detected in Filter";
    changeDetected = true;
}

/**
 * @brief SystemSettings::keyPressEvent
 * @param e     Key thats pressed
 * If the "a" key is pressed on the keyboard, act as if the apply button has been pressed
 */
void SystemSettings :: keyPressEvent(QKeyEvent * e) {
    if (e->key() == Qt::Key_A) {
        on_useThreshold_btn_clicked();
    }
}

/************************** Config Tab *******************************/

void SystemSettings::on_resetBoard_clicked()
{
    emit resetBoard();
}

/**
 * @brief SystemSettings::on_thresholdSelection_currentIndexChanged
 * @param index
 */
void SystemSettings::on_thresholdSelection_currentIndexChanged(int index)
{
    emit changeThresholdMode(index);
    if(index == 0) {

        ui->thresholdValBox->setEnabled(false);
        ui->thresholdValBox->setText("Default");

    }
    //use the sample number
    else if(index == 1) {
        ui->thresholdValBox->setEnabled(true);
        ui->thresholdValBox->setText("0");
    }
    //use threshold value
    else if(index == 2) {
        ui->thresholdValBox->setEnabled(true);
        ui->thresholdValBox->setText("0");
    }
}

/**
 * @brief SystemSettings::on_useThreshold_btn_clicked
 * The apply button on the system settings UI
 * Applies changes made to controls
 */
void SystemSettings::on_useThreshold_btn_clicked()
{
   // if(ui->systemAngle->text().toDouble() < 20){
   //     //QMessageBox::critical(this, "Invalid Input", "enter angle between 20° and 90°");
   //    qDebug() << "Enter";
    //}
    //else{
        double degree = ui->systemAngle->text().toDouble();
        emit degreeChanged(degree);

    QString num  = ui->thresholdValBox->text();
    int val = num.toInt();
    QString s = ui->skipSamples_box->text();
    int skipSamples = s.toInt();
    QString s1 = ui->sonarPositionBox->text();
    int position = s1.toInt();
    //get expected ping interval from compression table
//    double ping30 = ui->ping30->text().toDouble();
//    double ping60 = ui->ping60->text().toDouble();
//    double ping120 = ui->ping120->text().toDouble();
//    double ping180 = ui->ping180->text().toDouble();
//    double ping240 = ui->ping240->text().toDouble();
//    double ping360 = ui->ping360->text().toDouble();
//    double ping480 = ui->ping480->text().toDouble();

//    //get compression rate from the compression table
//    double compression30 = ui->compression30->text().toDouble();
//    double compression60 = ui->compression60->text().toDouble();
//    double compression120 = ui->compression120->text().toDouble();
//    double compression180 = ui->compression180->text().toDouble();
//    double compression240 = ui->compression240->text().toDouble();
//    double compression360 = ui->compression360->text().toDouble();
//    double compression480 = ui->compression480->text().toDouble();

//    //send the expected compression rate to the mainWindow
//    emit compressionRate30(compression30);
//    emit compressionRate60(compression60);
//    emit compressionRate120(compression120);
//    emit compressionRate180(compression180);
//    emit compressionRate240(compression240);
//    emit compressionRate360(compression360);
//    emit compressionRate480(compression480);

//    //send the expected ping interval to the mainWindow
//    emit expectPing30(ping30);
//    emit expectPing60(ping60);
//    emit expectPing120(ping120);
//    emit expectPing180(ping180);
//    emit expectPing240(ping240);
//    emit expectPing360(ping360);
//    emit expectPing480(ping480);

    emit setSkipSamples(skipSamples);
    emit setSonarPosition(position);
    emit useThreshold(val);
    emit sendNoiseThresh(ui->noiseTEdit->text().toDouble());
    this->hide();
   // }
}

void SystemSettings :: displayThresholdValue(QString value) {
    ui->thresholdValBox->setText(value);
}

void SystemSettings::on_boardSimulation_enable_clicked()
{
    emit enableBoardSimul();
}

void SystemSettings::on_boardSimulation_disabled_clicked()
{
    emit disableBoardSimul();
}

void SystemSettings::on_color_resolution_box_currentIndexChanged(int index)
{
    ui->color_resolution_box->setCurrentIndex(index);
    emit setResolution(index);
    emit sendResLabel(index);
}

int SystemSettings :: returnSwitchingDepth(int currentRange, bool switchUp) {
    QString depthSwitchDown, depthSwitchUp;
    int switchDepth;
    switch (currentRange) {
    case 5:
//        depthSwitchDown = ui->switchDown_5->text();
//        depthSwitchUp = ui->switchDown_5->text();
        break;
    case 10:
//        depthSwitchDown = ui->switchDown_10->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 15:
//        depthSwitchDown = ui->switchDown_15->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 20:
//        depthSwitchDown = ui->switchDown_20->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 25:
//        depthSwitchDown = ui->switchDown_25->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 30:
//        depthSwitchDown = ui->switchDown_30->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 35:
//        depthSwitchDown = ui->switchDown_35->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 40:
//        depthSwitchDown = ui->switchDown_40->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 45:
//        depthSwitchDown = ui->switchDown_45->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 50:
//        depthSwitchDown = ui->switchDown_50->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 55:
//        depthSwitchDown = ui->switchDown_55->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 60:
//        depthSwitchDown = ui->switchDown_60->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 65:
//        depthSwitchDown = ui->switchDown_65->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    case 70:
//        depthSwitchDown = ui->switchDown_70->text();
//        depthSwitchUp = ui->switchDown_10->text();
        break;
    default:
        return 70;
    }
    if(switchUp) {
        switchDepth = depthSwitchUp.toInt();
    }
    else {
        switchDepth = depthSwitchDown.toInt();
    }
    qDebug() << "string" << depthSwitchDown;
    qDebug() << "system switching depth" << switchDepth;
    return switchDepth;
}

void SystemSettings::on_afeReset_btn_clicked()
{
    emit resetAFE();
}

void SystemSettings::on_sim_activate_clicked(){
    emit set_simulation(0);
    ui->sim_activate->setChecked(true);
    ui->sim_deactivate->setChecked(false);
}

void SystemSettings::on_sim_deactivate_clicked(){
    emit set_simulation(1);
    ui->sim_deactivate->setChecked(true);
    ui->sim_activate->setChecked(false);
}

void SystemSettings :: setSimulation(bool set) {
    ui->sim_activate->setChecked(set);
    ui->sim_deactivate->setChecked(!set);
}

void SystemSettings :: on_ethernetBtn_clicked(){
    emit ethernetActive(true);
}

void SystemSettings :: send_alt_length_limit(int i){
    emit send_alt_length(i);
}

void SystemSettings :: send_yaw_limit(int i){
    emit send_yaw_length(i);
}

/************************** Receiver Tab *******************************/

void SystemSettings :: on_commonBox_clicked() {
    QColor white,gray;
    white.setRgb(255,255,255);
    gray.setRgb(128,128,128);
    if (ui->commonBox->isChecked()) {
        ui->groupBox_13->setStyleSheet("color:white");
        ui->radioButton_32->setEnabled(true);
        ui->radioButton_33->setEnabled(true);
        ui->radioButton_34->setEnabled(true);
        ui->adc->setEnabled(true);

        ui->groupBox_8->setStyleSheet("color:gray");
        ui->groupBox_9->setStyleSheet("color:gray");
        ui->groupBox_10->setStyleSheet("color:gray");
        ui->groupBox_11->setStyleSheet("color:gray");

        ui->adc1->setEnabled(false);
        ui->adc2->setEnabled(false);
        ui->adc3->setEnabled(false);
        ui->adc4->setEnabled(false);

        ui->radioButton_8->setEnabled(false);
        ui->radioButton_9->setEnabled(false);
        ui->radioButton_10->setEnabled(false);
        ui->radioButton_15->setEnabled(false);
        ui->radioButton_16->setEnabled(false);
        ui->radioButton_17->setEnabled(false);
        ui->radioButton_22->setEnabled(false);
        ui->radioButton_23->setEnabled(false);
        ui->radioButton_24->setEnabled(false);
        ui->radioButton_29->setEnabled(false);
        ui->radioButton_30->setEnabled(false);
        ui->radioButton_31->setEnabled(false);
    }
    else {
        ui->groupBox_13->setStyleSheet("color:gray");
        ui->radioButton_32->setEnabled(false);
        ui->radioButton_33->setEnabled(false);
        ui->radioButton_34->setEnabled(false);
        //ui->radioButton_32->setChecked(false);
        //ui->radioButton_34->setChecked(false);
        ui->adc->setEnabled(false);

        ui->groupBox_8->setStyleSheet("color:white");
        ui->groupBox_9->setStyleSheet("color:white");
        ui->groupBox_10->setStyleSheet("color:white");
        ui->groupBox_11->setStyleSheet("color:white");

        ui->adc1->setStyleSheet("color:white");
        ui->adc2->setStyleSheet("color:white");
        ui->adc3->setStyleSheet("color:white");
        ui->adc4->setStyleSheet("color:white");

        ui->adc1->setEnabled(true);
        ui->adc2->setEnabled(true);
        ui->adc3->setEnabled(true);
        ui->adc4->setEnabled(true);

        ui->radioButton_8->setEnabled(true);
        ui->radioButton_9->setEnabled(true);
        ui->radioButton_10->setEnabled(true);
        ui->radioButton_15->setEnabled(true);
        ui->radioButton_16->setEnabled(true);
        ui->radioButton_17->setEnabled(true);
        ui->radioButton_22->setEnabled(true);
        ui->radioButton_23->setEnabled(true);
        ui->radioButton_24->setEnabled(true);
        ui->radioButton_29->setEnabled(true);
        ui->radioButton_30->setEnabled(true);
        ui->radioButton_31->setEnabled(true);
    }
}

/**
 * @brief SystemSettings::on_att_slider_valueChanged
 * @param value     the value of the slider position
 * Changes the value of the attenuation label next to the slider
 */
void SystemSettings::on_att_slider_valueChanged(int value)
{
    int att_display_value;
    switch (value) {
    case 0:
        att_display_value = 0;
        break;
    case 1:
        att_display_value = 6;
        break;
    case 2:
        att_display_value = 12;
        break;
    case 3:
        att_display_value = 18;

        break;
    case 4:
        att_display_value = 24;
        break;
    case 5:
        att_display_value = 30;
        break;
    case 6:
        att_display_value = 36;
        break;
    case 7:
        att_display_value = 42;
        break;
    default:
        break;
    }
    ui->att_label->setText(QString ::number(att_display_value * -1));
}

/**
 * @brief SystemSettings::on_att_slider_sliderReleased
 * Applies the attenuation value of the sldier to the board when the slider is released
 */
void SystemSettings::on_att_slider_sliderReleased()
{
    int indexVal = ui->att_slider->value();
    emit attenuationVal(indexVal);
}

void SystemSettings::on_gain_mult_slider_valueChanged(int value) {
    int gain_display_value;
    switch (value) {
    case 0:
        gain_display_value = 0;
        break;
    case 1:
        gain_display_value = 2;
        break;
    case 2:
        gain_display_value = 4;
        break;
    case 3:
        gain_display_value = 8;
        break;
    case 4:
        gain_display_value = 16;
        break;
    case 5:
        gain_display_value = 32;
        break;
    case 6:
        gain_display_value = 64;
        break;
    case 7:
        gain_display_value = 128;
        break;
    default:
        break;
    }
    ui->gain_label->setText(QString ::number(gain_display_value));
}

void SystemSettings::on_adc1_valueChanged(int adc1) {
    double adcValue1;
    switch (adc1) {
    case 0:
        adcValue1 = 0;
        break;
    case 1:
        adcValue1 = 0.2;
        break;
    case 2:
        adcValue1 = 0.4;
        break;
    case 3:
        adcValue1 = 0.6;
        break;
    case 4:
        adcValue1 = 0.8;
        break;
    case 5:
        adcValue1 = 1.0;
        break;
    case 6:
        adcValue1 = 1.2;
        break;
    case 7:
        adcValue1 = 1.4;
        break;
    case 8:
        adcValue1 = 1.6;
        break;
    case 9:
        adcValue1 = 1.8;
        break;
    case 10:
        adcValue1 = 2.0;
        break;
    case 11:
        adcValue1 = 2.2;
        break;
    case 12:
        adcValue1 = 2.4;
        break;
    case 13:
        adcValue1 = 2.6;
        break;
    case 14:
        adcValue1 = 2.8;
        break;
    case 15:
        adcValue1 = 3.0;
        break;
    case 16:
        adcValue1 = 3.2;
        break;
    case 17:
        adcValue1 = 3.4;
        break;
    case 18:
        adcValue1 = 3.6;
        break;
    case 19:
        adcValue1 = 3.8;
        break;
    case 20:
        adcValue1 = 4.0;
        break;
    case 21:
        adcValue1 = 4.2;
        break;
    case 22:
        adcValue1 = 4.4;
        break;
    case 23:
        adcValue1 = 4.6;
        break;
    case 24:
        adcValue1 = 4.8;
        break;
    case 25:
        adcValue1 = 5.0;
        break;
    case 26:
        adcValue1 = 5.2;
        break;
    case 27:
        adcValue1 = 5.4;
        break;
    case 28:
        adcValue1 = 5.6;
        break;
    case 29:
        adcValue1 = 5.8;
        break;
    case 30:
        adcValue1 = 6.0;
        break;
    default:
        break;
    }
    ui->adc_label1->setText(QString ::number(adcValue1)+" dB");
}

void SystemSettings::on_adc2_valueChanged(int adc2) {
    double adcValue2;
    switch (adc2) {
    case 0:
        adcValue2 = 0;
        break;
    case 1:
        adcValue2 = 0.2;
        break;
    case 2:
        adcValue2 = 0.4;
        break;
    case 3:
        adcValue2 = 0.6;
        break;
    case 4:
        adcValue2 = 0.8;
        break;
    case 5:
        adcValue2 = 1.0;
        break;
    case 6:
        adcValue2 = 1.2;
        break;
    case 7:
        adcValue2 = 1.4;
        break;
    case 8:
        adcValue2 = 1.6;
        break;
    case 9:
        adcValue2 = 1.8;
        break;
    case 10:
        adcValue2 = 2.0;
        break;
    case 11:
        adcValue2 = 2.2;
        break;
    case 12:
        adcValue2 = 2.4;
        break;
    case 13:
        adcValue2 = 2.6;
        break;
    case 14:
        adcValue2 = 2.8;
        break;
    case 15:
        adcValue2 = 3.0;
        break;
    case 16:
        adcValue2 = 3.2;
        break;
    case 17:
        adcValue2 = 3.4;
        break;
    case 18:
        adcValue2 = 3.6;
        break;
    case 19:
        adcValue2 = 3.8;
        break;
    case 20:
        adcValue2 = 4.0;
        break;
    case 21:
        adcValue2 = 4.2;
        break;
    case 22:
        adcValue2 = 4.4;
        break;
    case 23:
        adcValue2 = 4.6;
        break;
    case 24:
        adcValue2 = 4.8;
        break;
    case 25:
        adcValue2 = 5.0;
        break;
    case 26:
        adcValue2 = 5.2;
        break;
    case 27:
        adcValue2 = 5.4;
        break;
    case 28:
        adcValue2 = 5.6;
        break;
    case 29:
        adcValue2 = 5.8;
        break;
    case 30:
        adcValue2 = 6.0;
        break;
    default:
        break;
    }
    ui->adc_label2->setText(QString ::number(adcValue2)+" dB");
}

void SystemSettings::on_adc3_valueChanged(int adc2) {
    double adcValue2;
    switch (adc2) {
    case 0:
        adcValue2 = 0;
        break;
    case 1:
        adcValue2 = 0.2;
        break;
    case 2:
        adcValue2 = 0.4;
        break;
    case 3:
        adcValue2 = 0.6;
        break;
    case 4:
        adcValue2 = 0.8;
        break;
    case 5:
        adcValue2 = 1.0;
        break;
    case 6:
        adcValue2 = 1.2;
        break;
    case 7:
        adcValue2 = 1.4;
        break;
    case 8:
        adcValue2 = 1.6;
        break;
    case 9:
        adcValue2 = 1.8;
        break;
    case 10:
        adcValue2 = 2.0;
        break;
    case 11:
        adcValue2 = 2.2;
        break;
    case 12:
        adcValue2 = 2.4;
        break;
    case 13:
        adcValue2 = 2.6;
        break;
    case 14:
        adcValue2 = 2.8;
        break;
    case 15:
        adcValue2 = 3.0;
        break;
    case 16:
        adcValue2 = 3.2;
        break;
    case 17:
        adcValue2 = 3.4;
        break;
    case 18:
        adcValue2 = 3.6;
        break;
    case 19:
        adcValue2 = 3.8;
        break;
    case 20:
        adcValue2 = 4.0;
        break;
    case 21:
        adcValue2 = 4.2;
        break;
    case 22:
        adcValue2 = 4.4;
        break;
    case 23:
        adcValue2 = 4.6;
        break;
    case 24:
        adcValue2 = 4.8;
        break;
    case 25:
        adcValue2 = 5.0;
        break;
    case 26:
        adcValue2 = 5.2;
        break;
    case 27:
        adcValue2 = 5.4;
        break;
    case 28:
        adcValue2 = 5.6;
        break;
    case 29:
        adcValue2 = 5.8;
        break;
    case 30:
        adcValue2 = 6.0;
        break;
    default:
        break;
    }
    ui->adc_label3->setText(QString ::number(adcValue2)+" dB");
}

void SystemSettings::on_adc4_valueChanged(int adc2) {
    double adcValue2;
    switch (adc2) {
    case 0:
        adcValue2 = 0;
        break;
    case 1:
        adcValue2 = 0.2;
        break;
    case 2:
        adcValue2 = 0.4;
        break;
    case 3:
        adcValue2 = 0.6;
        break;
    case 4:
        adcValue2 = 0.8;
        break;
    case 5:
        adcValue2 = 1.0;
        break;
    case 6:
        adcValue2 = 1.2;
        break;
    case 7:
        adcValue2 = 1.4;
        break;
    case 8:
        adcValue2 = 1.6;
        break;
    case 9:
        adcValue2 = 1.8;
        break;
    case 10:
        adcValue2 = 2.0;
        break;
    case 11:
        adcValue2 = 2.2;
        break;
    case 12:
        adcValue2 = 2.4;
        break;
    case 13:
        adcValue2 = 2.6;
        break;
    case 14:
        adcValue2 = 2.8;
        break;
    case 15:
        adcValue2 = 3.0;
        break;
    case 16:
        adcValue2 = 3.2;
        break;
    case 17:
        adcValue2 = 3.4;
        break;
    case 18:
        adcValue2 = 3.6;
        break;
    case 19:
        adcValue2 = 3.8;
        break;
    case 20:
        adcValue2 = 4.0;
        break;
    case 21:
        adcValue2 = 4.2;
        break;
    case 22:
        adcValue2 = 4.4;
        break;
    case 23:
        adcValue2 = 4.6;
        break;
    case 24:
        adcValue2 = 4.8;
        break;
    case 25:
        adcValue2 = 5.0;
        break;
    case 26:
        adcValue2 = 5.2;
        break;
    case 27:
        adcValue2 = 5.4;
        break;
    case 28:
        adcValue2 = 5.6;
        break;
    case 29:
        adcValue2 = 5.8;
        break;
    case 30:
        adcValue2 = 6.0;
        break;
    default:
        break;
    }
    ui->adc_label4->setText(QString ::number(adcValue2)+" dB");
}

void SystemSettings::on_adc_valueChanged(int adc1) {
    double adcValue1;
    switch (adc1) {
    case 0:
        adcValue1 = 0;
        break;
    case 1:
        adcValue1 = 0.2;
        break;
    case 2:
        adcValue1 = 0.4;
        break;
    case 3:
        adcValue1 = 0.6;
        break;
    case 4:
        adcValue1 = 0.8;
        break;
    case 5:
        adcValue1 = 1.0;
        break;
    case 6:
        adcValue1 = 1.2;
        break;
    case 7:
        adcValue1 = 1.4;
        break;
    case 8:
        adcValue1 = 1.6;
        break;
    case 9:
        adcValue1 = 1.8;
        break;
    case 10:
        adcValue1 = 2.0;
        break;
    case 11:
        adcValue1 = 2.2;
        break;
    case 12:
        adcValue1 = 2.4;
        break;
    case 13:
        adcValue1 = 2.6;
        break;
    case 14:
        adcValue1 = 2.8;
        break;
    case 15:
        adcValue1 = 3.0;
        break;
    case 16:
        adcValue1 = 3.2;
        break;
    case 17:
        adcValue1 = 3.4;
        break;
    case 18:
        adcValue1 = 3.6;
        break;
    case 19:
        adcValue1 = 3.8;
        break;
    case 20:
        adcValue1 = 4.0;
        break;
    case 21:
        adcValue1 = 4.2;
        break;
    case 22:
        adcValue1 = 4.4;
        break;
    case 23:
        adcValue1 = 4.6;
        break;
    case 24:
        adcValue1 = 4.8;
        break;
    case 25:
        adcValue1 = 5.0;
        break;
    case 26:
        adcValue1 = 5.2;
        break;
    case 27:
        adcValue1 = 5.4;
        break;
    case 28:
        adcValue1 = 5.6;
        break;
    case 29:
        adcValue1 = 5.8;
        break;
    case 30:
        adcValue1 = 6.0;
        break;
    default:
        break;
    }
    ui->adc_label->setText(QString ::number(adcValue1)+" dB");
}
//
void SystemSettings::on_gain_mult_slider_sliderReleased() {
    int indexVal = ui->gain_mult_slider->value();
    emit sendGainVal(indexVal);
    emit gainMultVal(indexVal);

}

/**
 * @brief SystemSettings::on_add_gain_clicked
 * Calls checkGain, which sends a command to add 12 db of gain to the board
 */
void SystemSettings::on_add_gain_clicked() {
    checkGain();
}

/**
 * @brief SystemSettings::on_sub_gain_clicked
 * Calls checkGain, which sends a command to subtract 12 db of gain to the board
 */
void SystemSettings::on_sub_gain_clicked() {
    checkGain();
}

/**
 * @brief SystemSettings::checkGain
 * Sends a command to either add or subtract 12 db of gain to the board
 * Dependent on what button is checked on the UI
 */
void SystemSettings::checkGain() {
    if(ui->add_gain->isChecked()) {//if add gain button is checked
        emit addGain();
    }
    else if (ui->sub_gain->isChecked()) {//if sub gain button is checked
        emit subGain();
    }
}

/**
 * @brief SystemSettings::on_transOff_btn_clicked
 * Sends command to turn both LF and HF transmitter off
 */
void SystemSettings :: on_transOff_btn_clicked() {
    int TXval = 0;
    emit transConfig(TXval);
}

/**
 * @brief SystemSettings::on_LFOn_btn_clicked
 * Sends command to turn on LF transmitter, and turn off HF
 */
void SystemSettings :: on_LFOn_btn_clicked() {
    int TXval = 1;
    emit transConfig(TXval);
}

/**
 * @brief SystemSettings::on_HFOn_btn_clicked
 * Sends command to turn on HF transmitter, and turn off LF
 */
void SystemSettings :: on_HFOn_btn_clicked() {
    int TXval = 2;
    emit transConfig(TXval);
}

/**
 * @brief SystemSettings::on_transOn_btn_clicked
 * Sends command to turn on both HF and LF transmitters
 */
void SystemSettings :: on_transOn_btn_clicked() {
    int TXval = 3;
    emit transConfig(TXval);
}

void SystemSettings :: receiveSampleSkipFromDB(QString sampleSkip) {
    ui->skipSamples_box->setText(sampleSkip);
    emit setSkipSamples(sampleSkip.toInt());
}

void SystemSettings :: receiveSeaSurfDistFromDB(QString dist) {
    ui->sonarPositionBox->setText(dist);
}

void SystemSettings :: receiveDemoForce(int v) {
    ui->gain_mult_slider->setValue(v);
    on_gain_mult_slider_sliderReleased();
}

void SystemSettings :: receiveScaleButton(QString s) {

    if (s == "+ 12db")
    {
        ui->add_gain->setChecked(true);
        ui->sub_gain->setChecked(false);
        on_add_gain_clicked();
    }
    else if (s == "0 dB")
    {
        ui->add_gain->setChecked(false);
        ui->sub_gain->setChecked(true);
        on_add_gain_clicked();
    }
}

void SystemSettings :: receiveCh1Button(QString s) {

    if (s == "15dB")
    {
        ui->radioButton_8->setChecked(true);
        ui->radioButton_9->setChecked(false);
        ui->radioButton_10->setChecked(false);
    }
    else if (s == "18dB")
    {
        ui->radioButton_8->setChecked(false);
        ui->radioButton_9->setChecked(true);
            qDebug()<<"radioButton9 is checked.";
            emit coarseGainToMain(0);
        ui->radioButton_10->setChecked(false);

    }
    else if (s == "24dB")
    {
        ui->radioButton_8->setChecked(false);
        ui->radioButton_9->setChecked(false);
        ui->radioButton_10->setChecked(true);
    }
}

void SystemSettings :: receiveCh2Button(QString s) {

    if (s == "15dB")
    {
        ui->radioButton_15->setChecked(true);
        ui->radioButton_16->setChecked(false);
        ui->radioButton_17->setChecked(false);
    }
    else if (s == "18dB")
    {
        ui->radioButton_15->setChecked(false);
        ui->radioButton_16->setChecked(true);
        ui->radioButton_17->setChecked(false);

    }
    else if (s == "24dB")
    {
        ui->radioButton_15->setChecked(false);
        ui->radioButton_16->setChecked(false);
        ui->radioButton_17->setChecked(true);
    }
}

void SystemSettings :: receiveCh3Button(QString s) {

    if (s == "15dB")
    {
        ui->radioButton_22->setChecked(true);
        ui->radioButton_23->setChecked(false);
        ui->radioButton_24->setChecked(false);
    }
    else if (s == "18dB")
    {
        ui->radioButton_22->setChecked(false);
        ui->radioButton_23->setChecked(true);
        ui->radioButton_24->setChecked(false);

    }
    else if (s == "24dB")
    {
        ui->radioButton_22->setChecked(false);
        ui->radioButton_23->setChecked(false);
        ui->radioButton_24->setChecked(true);
    }
}

void SystemSettings :: receiveCh4Button(QString s) {

    if (s == "15dB")
    {
        ui->radioButton_29->setChecked(true);
        ui->radioButton_30->setChecked(false);
        ui->radioButton_31->setChecked(false);
    }
    else if (s == "18dB")
    {
        ui->radioButton_29->setChecked(false);
        ui->radioButton_30->setChecked(true);
        ui->radioButton_31->setChecked(false);

    }
    else if (s == "24dB")
    {
        ui->radioButton_29->setChecked(false);
        ui->radioButton_30->setChecked(false);
        ui->radioButton_31->setChecked(true);
    }
}

void SystemSettings :: receiveCh1Slider(int n) {
    ui->adc1->setValue(n);
    ui->adc_label1->setText("0 dB");
    on_adc1_valueChanged(n);
}

void SystemSettings :: receiveCh2Slider(int n) {
    ui->adc2->setValue(n);
    ui->adc_label2->setText("0 dB");
    on_adc2_valueChanged(n);
}

void SystemSettings :: receiveCh3Slider(int n) {
    ui->adc3->setValue(n);
    ui->adc_label3->setText("0 dB");
    on_adc3_valueChanged(n);
}

void SystemSettings :: receiveCh4Slider(int n) {
    ui->adc4->setValue(n);
    ui->adc_label4->setText("0 dB");
    on_adc4_valueChanged(n);
}

void SystemSettings :: receiveCheckOnOff(QString s) {

    if (s == "No")
    {
        ui->commonBox->setChecked(false);
        on_commonBox_clicked();
    }
    else if (s == "Yes")
    {
        ui->commonBox->setChecked(true);
        on_commonBox_clicked();
    }
}

void SystemSettings :: receiveCBtn(QString s) {

    if (s == "15dB")
    {
        ui->radioButton_32->setChecked(true);
        ui->radioButton_33->setChecked(false);
        ui->radioButton_34->setChecked(false);
    }
    else if (s == "18dB")
    {
        ui->radioButton_32->setChecked(false);
        ui->radioButton_33->setChecked(true);
        ui->radioButton_34->setChecked(false);

    }
    else if (s == "24dB")
    {
        ui->radioButton_32->setChecked(false);
        ui->radioButton_33->setChecked(false);
        ui->radioButton_34->setChecked(true);
    }
}

void SystemSettings :: receivePButton(int n) {

    if (n == 24)
    {
        ui->radioButton_36->setChecked(true);
        ui->radioButton_37->setChecked(false);
    }
    else if (n == 30)
    {
        ui->radioButton_36->setChecked(false);
        ui->radioButton_37->setChecked(true);
    }
}
void SystemSettings :: receiveCSlider(int n) {

    ui->adc->setValue(n);
    ui->adc_label->setText("0 dB");
    on_adc_valueChanged(n);
}

void SystemSettings :: receiveAtt(int n) {

    ui->att_slider->setValue(n);
    on_att_slider_sliderReleased();
    on_att_slider_valueChanged(n);

}

void SystemSettings :: receiveTransm(QString s) {

    if (s == "LF")
    {
        ui->LFOn_btn->setChecked(true);
            on_LFOn_btn_clicked();
        ui->HFOn_btn->setChecked(false);
        ui->transOn_btn->setChecked(false);
        ui->transOff_btn->setChecked(false);
    }

    else if (s == "HF")
    {
        ui->LFOn_btn->setChecked(false);
        ui->HFOn_btn->setChecked(true);
            on_HFOn_btn_clicked();
        ui->transOn_btn->setChecked(false);
        ui->transOff_btn->setChecked(false);
    }

    else if (s == "Both On")
    {
        ui->LFOn_btn->setChecked(false);
        ui->HFOn_btn->setChecked(false);
        ui->transOn_btn->setChecked(true);
            on_transOn_btn_clicked();
        ui->transOff_btn->setChecked(false);
    }

    else if (s == "Both Off")
    {
        ui->LFOn_btn->setChecked(false);
        ui->HFOn_btn->setChecked(false);
        ui->transOn_btn->setChecked(false);
        ui->transOff_btn->setChecked(true);
            on_transOff_btn_clicked();
    }
}

void SystemSettings :: receiveDepSounder(QString s) {

    if (s == "No")
    {
        ui->depthBox->setChecked(false);
    }
    else if (s == "Yes")
    {
        ui->depthBox->setChecked(true);
        on_depthBox_clicked();
    }
}

void SystemSettings :: receiveGain(int n) {
    on_gainAddBox_valueChanged(n);
}
void SystemSettings :: receiveListsFromTable(QList<int> range, QList<double> EPI, QList<int> comp) {

    //set a default value for each range
    ui->range30->setText(QString::number(range[0]));
    ui->range60->setText(QString::number(range[1]));
    ui->range120->setText(QString::number(range[2]));
    ui->range180->setText(QString::number(range[3]));
    ui->range240->setText(QString::number(range[4]));
    ui->range360->setText(QString::number(range[5]));
    ui->range480->setText(QString::number(range[6]));
    //grayed out the range box
    ui->range30->setEnabled(false);
    ui->range60->setEnabled(false);
    ui->range120->setEnabled(false);
    ui->range180->setEnabled(false);
    ui->range240->setEnabled(false);
    ui->range360->setEnabled(false);
    ui->range480->setEnabled(false);

    //set a default value for expected ping interval
    ui->ping30->setText(QString::number(EPI[0]));
    ui->ping60->setText(QString::number(EPI[1]));
    ui->ping120->setText(QString::number(EPI[2]));
    ui->ping180->setText(QString::number(EPI[3]));
    ui->ping240->setText(QString::number(EPI[4]));
    ui->ping360->setText(QString::number(EPI[5]));
    ui->ping480->setText(QString::number(EPI[6]));

    //set default values for compression rate
    ui->compression30->setText(QString::number(comp[0]));
    ui->compression60->setText(QString::number(comp[1]));
    ui->compression120->setText(QString::number(comp[2]));
    ui->compression180->setText(QString::number(comp[3]));
    ui->compression240->setText(QString::number(comp[4]));
    ui->compression360->setText(QString::number(comp[5]));
    ui->compression480->setText(QString::number(comp[6]));
    //grayed out the compression box
    ui->compression30->setEnabled(false);
    ui->compression60->setEnabled(false);
    ui->compression120->setEnabled(false);
    ui->compression180->setEnabled(false);
    ui->compression240->setEnabled(false);
    ui->compression360->setEnabled(false);
    ui->compression480->setEnabled(false);
}

void SystemSettings :: receiveFilterOff() {
    ui->filterBox->setChecked(false);
    on_filterBox_clicked();
}

void SystemSettings :: receiveFilterSizeFromDB(int i) {
    on_filterSizeBox_valueChanged(i);
}

/************************** Filter Tab *******************************/

void SystemSettings :: on_filterRstBtn_clicked() {
    ui->filterSizeBox->blockSignals(true); //This prevents the signal from being called, which is only supposed to be called on a 'user' change
    emit sendFilterSize(ui->filterSizeBox->value());
    ui->filterSizeBox->setValue(ui->filterSizeBox->value());
    ui->filterSizeBox->blockSignals(false);
}

void SystemSettings :: on_filterBox_clicked() {
    emit sendFilterEnabled(ui->filterBox->isChecked());
}

void SystemSettings :: on_filterSizeBox_valueChanged(int value) {
    ui->filterSizeBox->blockSignals(true); //This prevents the signal from being called, which is only supposed to be called on a 'user' change
    emit sendFilterSize(value);
    ui->filterSizeBox->setValue(value);
    ui->filterSizeBox->blockSignals(false);
}

/************************** Compass Tab *******************************/

/*//void SystemSettings :: receiveSys(float x, float y) {  //Compass tab

//    ui->xCoord->setText(QString::number(x));
//    ui->yCoord->setText(QString::number(y));
//}

//void SystemSettings :: on_beginCalibration_clicked() { //Compass tab

//    calibrateRestart = true;
//    calibrateActive = true;
//    ui->nextBtn->setEnabled(true);
//    ui->nextBtn->setText("Next");

//    ui->calComplete->hide();
//    ui->northDir->show();
//    ui->southDir->hide();
//    ui->eastDir->hide();
//    ui->westDir->hide();

//    ui->n1->hide(); ui->n2->hide();
//    ui->n3->hide(); ui->n4->hide();
//    ui->n5->hide(); ui->n6->hide();
//    ui->n7->hide(); ui->n8->hide();

//    ui->xCoord->move(120, 420);
//    ui->yCoord->move(230, 420);
//    ui->xCoord->show();
//    ui->yCoord->show();

//    emit sendCompass('0');
//}

//void SystemSettings :: on_nextBtn_clicked() {         //Compass tab

//    static int i = 0;
//    if (calibrateRestart)
//    {
//        i = 0;
//        calibrateRestart = false;
//    }

//    if (i == 0)
//    {
//        ui->northDir->hide();
//        ui->westDir->show();
//        QString nx = ui->xCoord->text();
//        QString ny = ui->yCoord->text();
//        ui->n1->setText(nx); ui->n1->show();
//        ui->n2->setText(ny); ui->n2->show();
//        emit sendCompass('N');
//        ui->xCoord->move(120, 460);
//        ui->yCoord->move(230, 460);
//        i++;
//    }
//    else if (i == 1)
//    {
//       ui->westDir->hide();
//       ui->southDir->show();
//       QString wx = ui->xCoord->text();
//       QString wy = ui->yCoord->text();
//       ui->n3->setText(wx); ui->n3->show();
//       ui->n4->setText(wy); ui->n4->show();
//       emit sendCompass('W');
//       ui->xCoord->move(120, 500);
//       ui->yCoord->move(230, 500);
//       i++;
//    }
//    else if (i == 2)
//    {
//        ui->southDir->hide();
//        ui->eastDir->show();
//        QString sx = ui->xCoord->text();
//        QString sy = ui->yCoord->text();
//        ui->n5->setText(sx); ui->n5->show();
//        ui->n6->setText(sy); ui->n6->show();
//        emit sendCompass('S');
//        ui->xCoord->move(120, 540);
//        ui->yCoord->move(230, 540);
//        ui->nextBtn->setText("Done");
//        i++;
//    }
//    else if (i == 3)
//    {
//        ui->eastDir->hide();
//        ui->calComplete->show();
//        QString ex = ui->xCoord->text();
//        QString ey = ui->yCoord->text();
//        ui->n7->setText(ex); ui->n7->show();
//        ui->n8->setText(ey); ui->n8->show();
//        emit sendCompass('E');
//        ui->xCoord->hide();
//        ui->yCoord->hide();
//        i++;
//        ui->nextBtn->setEnabled(false);
//        calibrateActive = false;
//    }
//}

//void SystemSettings :: on_rBtn_clicked() {
//    emit sendDateData(1);
//}

//void SystemSettings :: receiveMessage(QString message) {
//   ui->dateLabel->setText(message);
//   ui->dateLabel->show();
//}*/




void SystemSettings::on_skipYes_clicked()
{
    activateSkipSamples = 1;
    emit skipSampleMode(activateSkipSamples);
}

void SystemSettings::on_skipNo_clicked()
{
    activateSkipSamples = 0;
    emit skipSampleMode(activateSkipSamples);
}

void SystemSettings::on_crossPt_avg_No_clicked()
{
    ui->crossPt_avg_No->setChecked(true); //Even though clicking the button does this by default, when calling this slot from main the checking does not occur
    ui->crossPt_avg_yes->setChecked(false);
    emit setAverageMode(0);
}

void SystemSettings::on_crossPt_avg_yes_clicked()
{
    ui->crossPt_avg_No->setChecked(false);
    ui->crossPt_avg_yes->setChecked(true);
    emit setAverageMode(1);
}

void SystemSettings :: on_compBox_valueChanged(int i) {
    ui->compBox->setValue(i);
    emit changeCompression(i);
}

void SystemSettings :: on_depthBox_clicked() {
    emit enableDepth(ui->depthBox->isChecked());
}


void SystemSettings :: on_gainAddBox_valueChanged(int value) {
    ui->gainAddBox->blockSignals(true); //This prevents the signal from being called, which is only supposed to be called on a 'user' change
    emit setAddGain(value);
    ui->gainAddBox->setValue(value);
    ui->gainAddBox->blockSignals(false);
}

void SystemSettings :: changeNoiseThresh(double thresh) {
    ui->noiseTEdit->setText(QString::number(thresh));
}

void SystemSettings :: defaultAngle(double angle){
    ui->systemAngle->setText(QString::number(angle));
    ui->systemAngle_2->setText(QString::number(angle));
}

void SystemSettings :: noAngleControl(){
    ui->systemAngle->setEnabled(false);
    ui->systemAngle_2->setEnabled(false);
    ui->systemOk->setEnabled(false);
}

void SystemSettings :: openControls(bool open){
    ui->systemAngle->setEnabled(true);
    ui->systemAngle_2->setEnabled(true);
}
