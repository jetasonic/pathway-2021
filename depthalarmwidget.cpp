#include "depthalarmwidget.h"
#include "ui_depthalarmwidget.h"

DepthAlarmWidget::DepthAlarmWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DepthAlarmWidget)
{
    ui->setupUi(this);
    audio = new QMediaPlayer;
    connect(ui->slider_volume, SIGNAL(valueChanged(int)), this, SLOT(controlAudioVolume(int)));
    connect(audio, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(checkSoundTest(QMediaPlayer::State)));
    ui->slider_volume->setFocus();
}

DepthAlarmWidget::~DepthAlarmWidget()
{
    delete ui;
}

void DepthAlarmWidget :: mousePressEvent(QMouseEvent *event) {
    hideTimer.start(15000);
}

void DepthAlarmWidget :: mouseMoveEvent(QMouseEvent *event) {
    hideTimer.start(15000);
}

void DepthAlarmWidget :: on_btn_close_clicked() {
    hideTimer.stop();
    this->hide();
}

void DepthAlarmWidget :: on_check_alarmOn_clicked() {
    activated = ui->check_alarmOn->isChecked();
    if (!activated) {
        audio->stop();
        handle->setVisible(false);
    } else {
        if (volume == 0)    controlAudioVolume(5);
        handle->setVisible(true);
    }
    handle->parentPlot()->replot(QCustomPlot::rpQueuedReplot);
    hideTimer.start(15000);
    updateButtonIcon();
}

void DepthAlarmWidget :: on_box_alarmLevel_currentTextChanged() {
    updateHandlePosition(ui->box_alarmLevel->currentText().toInt());
    hideTimer.start(15000);
}

void DepthAlarmWidget :: on_box_select_currentIndexChanged(int index) {
    audioIndex = index;
    audio->stop();
    loadAudio(index);
    hideTimer.start(15000);
}

void DepthAlarmWidget :: on_btn_testSound_clicked() {
    if (ui->btn_testSound->isChecked()) {
        if (volume == 0)    controlAudioVolume(20);
        audio->play();
    } else {
        audio->stop();
    }
    hideTimer.start(15000);
}

void DepthAlarmWidget :: checkSoundTest(QMediaPlayer::State state) {
    if (ui->btn_testSound->isChecked() && state == QMediaPlayer::StoppedState)     ui->btn_testSound->setChecked(false);
    if (state == QMediaPlayer::StoppedState) {
        // Stop the alarm from the mainWindow (to reset the icon)
        count = audio->duration()/500 + 10;
        emit stopAlarm();
    }
}

void DepthAlarmWidget :: updateAlarmSettings() {
    if (level < 3)      level = 10;
    ui->box_alarmLevel->setCurrentText(QString::number(level));
    ui->check_alarmOn->setChecked(activated);
    controlAudioVolume(volume);
    loadAudio(audioIndex);
    updateButtonIcon();
}

void DepthAlarmWidget :: controlAudioVolume(int value) {
    volume = value;
    ui->slider_volume->setValue(volume);
    ui->label_volPercent->setText(QString::number(volume) + " %");
    audio->setVolume(volume);
}

void DepthAlarmWidget :: loadAudio(int index) {
    ui->box_select->setCurrentIndex(audioIndex);
    QString link;
    switch(index) {
    case 0:
        link = QFileInfo("audio/Alarm-Fast-High-Pitch-B1.mp3").absoluteFilePath();
        break;
    case 1:
        link = QFileInfo("audio/street-public-alarm.wav").absoluteFilePath();
        break;
    case 2:
        link = QFileInfo("audio/Alarm-Fast-A1.mp3").absoluteFilePath();
        break;
    case 3:
        link = QFileInfo("audio/Alarm-Slow-B1.mp3").absoluteFilePath();
        break;
    case 4:
        link = QFileInfo("audio/game-notification-wave-alarm.wav").absoluteFilePath();
        break;
    default:
        link = QFileInfo("audio/Alarm-Slow-B1.mp3").absoluteFilePath();
        break;
    }
    audio->setMedia(QUrl::fromLocalFile(link));
}

