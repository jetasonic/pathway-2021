#ifndef CREATEFLAG_H
#define CREATEFLAG_H

#include <QDialog>
#include "qcustomplot.h"
#include "modules.h"

namespace Ui {
class CreateFlag;
}

class CreateFlag : public QDialog
{
    Q_OBJECT

public:
    explicit CreateFlag(QWidget *parent = 0);
    ~CreateFlag();
    void getParametersToFlag(targetItem*, bool);

    bool okButtonClicked = false;
    bool cancelButtonClicked = false;

public slots:

    void on_cancel_clicked();
    void on_ok_clicked();
    void closeEvent(QCloseEvent *event);

signals:
     void renameTargetImageFile();
    void cancelFlag(bool);
    void flagInfo(QString, targetItem*, bool);
    void flagCreateOkButton(bool okButtonClicked);
    void flagCreateCancelButton(bool cancelButtonClicked);
private:
    Ui :: CreateFlag *flag_ui;
    QString FlagHeading;
    int CurrentRow;
    targetItem *CurrentItem;
    bool NewItem;
};

#endif // CREATEFLAG_H
