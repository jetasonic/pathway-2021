/* usermenu.cpp, Header comment written August 9th 2019
 * This object controls the settings set by the user
 * This includes things like what palette they use, what directory they save recorded files to, etc..
 * The user can interact with this object to change their settings
 * Some settings are duplicated on the toolbar in the main window for convenience
 */
#include "usermenu.h"
#include "ui_usermenu.h"
#include "QCloseEvent"
#include<QMessageBox>
#include<cmath>
#include<QGradient>
#include<QPalette>
#include<QSlider>
#include<QWidget>
#include "mainwindow.h"

MainWindow* uMainWindow;
QCPItemEllipse *itemPortTvg = nullptr, *itemStbdTvg = nullptr, *itemTvgC = nullptr;
QCPItemText *itemMaxTvg = nullptr;
QCPItemLine *itemTvgDr = nullptr;
bool dragItemB = false, dragItemC = false;

userMenu::userMenu(QWidget *parent) :
    QDialog(parent),
    usermenu_ui(new Ui::userMenu)
{
    uMainWindow = qobject_cast<MainWindow*>(this->parent());
    gain = new GainSettings();
    usermenu_ui->setupUi(this);
    setUpWidgetsAndVisuals();
    //this->setWindowFlags(Qt::Window);
    this->setWindowFlags(Qt::Window| /*Qt::WindowContextHelpButtonHint|*/ Qt::WindowCloseButtonHint);
    //
    //setFixedSize(this->width(),this->height());

    //usermenu_ui->paletteBox->setCurrentText("Ocean");
    setCustomPalette("Rainbow");
    setDepthPalette("Ocean");

    usermenu_ui->ss_line->setValidator(new QDoubleValidator(-99999.0, 99999.0, 4, usermenu_ui->ss_line));
    if (usermenu_ui->ss_line->text() == "0") {
        usermenu_ui->ss_line->setText("1500");
    }

    usermenu_ui->clBox->setValidator(new QDoubleValidator(-99999.0, 99999.0, 4, usermenu_ui->clBox));

    //usermenu_ui->tab->setA
    usermenu_ui->tabBox->removeTab(2); //temporarily disable tow control tab//
    usermenu_ui->tabBox->removeTab(2); // Remove "Default Gain Settings" Tab
    usermenu_ui->tabBox->removeTab(2); // Remove Copmass Tab
    usermenu_ui->tabBox->removeTab(2);
    usermenu_ui->tabBox->removeTab(2);
    usermenu_ui->tabBox->removeTab(2);

    usermenu_ui->groupBox_15->setVisible(false);

    usermenu_ui->dirPathBox->setText(qApp->applicationDirPath());
    //Altimeter tab initialization
    usermenu_ui->transparencySlider->setValue(50);
    setTransparency(50,1);
    usermenu_ui->pixelSlider->setValue(500);
    usermenu_ui->tStripBtn->setChecked(true);
    usermenu_ui->dLabelBtn->setChecked(true);
    usermenu_ui->gradCheckBox1->setChecked(true);
    usermenu_ui->depthSpacing->setCurrentText("20");
    usermenu_ui->clBox->setText("10");

    usermenu_ui->acceptButton->setStyleSheet("font: 10pt;");
    connClose();
    connOpen();

    Bvalue_list<<usermenu_ui->bVal1<<usermenu_ui->bVal2<<usermenu_ui->bVal3<<usermenu_ui->bVal4<<usermenu_ui->bVal5<<usermenu_ui->bVal6<<usermenu_ui->bVal7<<usermenu_ui->bVal8<<usermenu_ui->bVal9<<usermenu_ui->bVal10<<usermenu_ui->bVal11<<usermenu_ui->bVal12<<usermenu_ui->bVal13<<usermenu_ui->bVal14;

    setUpComboBox();
    plotLogGraph();

    usermenu_ui->box_lfZoom->setValidator(new QIntValidator(10, 500, usermenu_ui->box_lfZoom));
    usermenu_ui->box_hfZoom->setValidator(new QIntValidator(10, 500, usermenu_ui->box_hfZoom));
    connect(usermenu_ui->box_lfZoom, SIGNAL(currentTextChanged(QString)), this, SLOT(updateZoomSettingLf(QString)));
    connect(usermenu_ui->box_hfZoom, SIGNAL(currentTextChanged(QString)), this, SLOT(updateZoomSettingHf(QString)));

    //   this->setWindowFlags(this->windowFlags() &~Qt::WindowContextHelpButtonHint|Qt::WindowCloseButtonHint);//|Qt::MSWindowsFixedSizeDialogHint);

    connect(usermenu_ui->paletteBox, SIGNAL(currentTextChanged(QString)), this,  SLOT(mainPalletteChose(QString)));
    connect(usermenu_ui->depthPalBox, SIGNAL(currentTextChanged(QString)), this, SLOT(depthPaletteChoose(QString)));

    connect(usermenu_ui->threshSlider, SIGNAL(valueChanged(int)), this, SLOT(receiveThreshValue(int)));

    connect(usermenu_ui->listSizeSlider, SIGNAL(valueChanged(int)), this, SLOT(receiveListSize(int)));
    connect(usermenu_ui->boatSpeedBox, SIGNAL(currentIndexChanged(int)), this, SLOT(boatSpeedChanged(int)));
    connect(usermenu_ui->tabBox, SIGNAL(currentChanged(int)), this, SLOT(checkCurrentTab(int)));
    connect(&updateTvgTimer, SIGNAL(timeout()), this, SLOT(updateTvgSettingsOnMainWindowTimer()));

    //    connect(usermenu_ui->transparencySlider, SIGNAL(valueChanged(int)),this,SLOT(on_transparencySlider_valueChanged()(int)));

    usermenu_ui->tabBox->setVisible(true);

    connect(usermenu_ui->globalDepthBox, SIGNAL(currentIndexChanged(int)),this, SLOT(depthRangeChanged(int)));

    connect(usermenu_ui->radio_target_red, SIGNAL(clicked()),this, SLOT(onNewTargetIconSelected()));
    connect(usermenu_ui->radio_target_yellow, SIGNAL(clicked()),this, SLOT(onNewTargetIconSelected()));
    connect(usermenu_ui->radio_target_blue, SIGNAL(clicked()),this, SLOT(onNewTargetIconSelected()));
    connect(usermenu_ui->radio_target_green, SIGNAL(clicked()),this, SLOT(onNewTargetIconSelected()));
}

userMenu::~userMenu()
{
    delete usermenu_ui;

}

void userMenu :: setLineIntValidator(QLineEdit *lineItem, int minimum, int maximum) {
    lineItem->setValidator(new QIntValidator(minimum, maximum, lineItem));
}
void userMenu :: setLineDoubleValidator(QLineEdit *lineItem, double minimum, double maximum) {
    lineItem->setValidator(new QDoubleValidator(minimum, maximum, 1, lineItem));
}

void userMenu :: setUpWidgetsAndVisuals() {

    QColor white;
    white.setRgb(255,255,255);

    QFont btn_bold;
    bool enable = true;
    btn_bold.setBold(enable);
    usermenu_ui->tab->setStyleSheet("color:white");

//    usermenu_ui->B_LfPort->setEnabled(false);
//    usermenu_ui->B_LfStbd->setEnabled(false);
//    usermenu_ui->B_HfPort->setEnabled(false);
//    usermenu_ui->B_HfStbd->setEnabled(false);

    QString customSliderColor1= "QSlider {border: 1px solid #bbb; border-radius:4px; height: 10px} QSlider::groove:vertical {background: qlineargradient( x1:0 y1:0, x2:0 y2:1, stop: 0 #eec0c6, stop: 1 #7ee8fa)}";//sabbir
    usermenu_ui->gradientSlider1->setStyleSheet(customSliderColor1);

    QString customSliderColor2= "QSlider {border: 1px solid #bbb; border-radius:4px; height: 10px} QSlider::groove:vertical {background: qlineargradient( x1:0 y1:0, x2:0 y2:1, stop: 0 #66e, stop: 1 #bbf)}";//sabbir
    usermenu_ui->gradientSlider2->setStyleSheet(customSliderColor2);

    QString customSliderColor3= "QSlider {border: 1px solid #bbb; border-radius:4px; height: 10px} QSlider::groove:vertical {background: qlineargradient( x1:0 y1:0, x2:0 y2:1, stop: 0 #29524a, stop: 1 #e9bcb7)}";//sabbir
    usermenu_ui->gradientSlider3->setStyleSheet(customSliderColor3);

    QString customSliderColor4= "QSlider {border: 1px solid #bbb; border-radius:4px; height: 10px} QSlider::groove:vertical {background: qlineargradient( x1:0 y1:0, x2:0 y2:1, stop: 0 #9e768f, stop: 1 #9fa4c4)}";//sabbir
    usermenu_ui->gradientSlider4->setStyleSheet(customSliderColor4);

    QString customSliderColor5= "QSlider {border: 1px solid #bbb; border-radius:4px; height: 10px} QSlider::groove:vertical {background: qlineargradient( x1:0 y1:0, x2:0 y2:1, stop: 0 #861657, stop: 1 #ffa69e)}";//sabbir
    usermenu_ui->gradientSlider5->setStyleSheet(customSliderColor5);

    // usermenu_ui->pixelSlider->setValue(0);
    usermenu_ui->displayForwardRange->setVisible(false);


}

void userMenu :: closeEvent(QCloseEvent *e){
    //QMessageBox.setWindowFlags(Qt::WindowTitleHint | Qt::FramelessWindowHint);

    if (calibrateActive)
    {
        e->ignore();
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this, "Warning" , "Calibration still active. Close anyway?", QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes)
        {
            e->accept();
        }
    }

    else if(!usermenu_ui->acceptButton->isChecked()){          //does not click the apply button to close the user menu
        this->hide();
        e->setAccepted(false);
    }

    else{                                                   //click the apply button to close the user menu
        this->hide();
        usermenu_ui->acceptButton->setChecked(false);

    }
}

void userMenu :: on_acceptButton_clicked()
{
    on_saveButton_clicked();
    // this->hide(); //this is actually close the usermenu window
}

void userMenu :: on_saveButton_clicked()
{
    bool emptyFields = false;
    emptyFields |= emptyFieldWarning(usermenu_ui->ss_line);
    emptyFields |= emptyFieldWarning(usermenu_ui->clBox);
    emptyFields |= emptyFieldWarning(usermenu_ui->box_lfZoom);
    emptyFields |= emptyFieldWarning(usermenu_ui->box_hfZoom);

    if (emptyFields) {
        //  int ret = QMessageBox::warning(this, tr("Warning"), tr("Do you want to reset to default settings? ");
        QMessageBox msgBox;

        msgBox.critical(0,"Error","Required fields cannot be empty!");
        // setStyleSheet("{border : 2px solid orange;}");
        return;
        //resetUserSettings();
    }

    cl = usermenu_ui->clBox->text().toDouble();
    ss = usermenu_ui->ss_line->text().toDouble();
    cableLength = usermenu_ui->clBox->text();
    speedofSound = usermenu_ui->ss_line->text();
    attnuation = usermenu_ui->attenuationBox->currentText().toDouble();
    customPal = usermenu_ui->paletteBox->currentText();
    depthPal = usermenu_ui->depthPalBox->currentText();
    setCustomPalette(customPal);
    setDepthPalette(depthPal);

    connCloseUser();
    saveUserData();
    saveGains();
    emit updateCL(cl);
    emit updateSS(ss);
    qDebug()<<usermenu_ui->swathBar->value();
    on_dirPathBox_textChanged();

    //this->hide(); //this is actually close the usermenu window

    /*emit signalToMain(usermenu_ui->signalPlots->isChecked(),
                      usermenu_ui->PlotExternal->isChecked(),
                      usermenu_ui->RotateAltimeter->isChecked());*/

    recordPath = usermenu_ui->dirPathBox->text();
    //    emit sendRecDir(recordPath);
    uMainWindow->recorder.recPath = recordPath;

    this->hide();
}

bool userMenu :: emptyFieldWarning(QLineEdit *lineItem) {
    if(lineItem->text().isEmpty()){
        lineItem->setStyleSheet("QLineEdit{border : 2px solid orange;}");
        return 1;
    } else {
        lineItem->setStyleSheet("QLineEdit{border : 2px black;}");
        lineItem->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");
        return 0;
    }
}
bool userMenu :: emptyFieldWarning(QComboBox *lineItem) {
    if(lineItem->currentText().isEmpty()){
        lineItem->setStyleSheet("QComboBox{border : 2px solid orange;}");
        return 1;
    } else {
        lineItem->setStyleSheet("QComboBox{border : 2px black;}");
        lineItem->setStyleSheet("QComboBox::hover""{""border:2px solid orange;""}");
        return 0;
    }
}

void userMenu :: get_Dynamic_range(int maxRange, int maxRange_HF){

    gain->tvg_maxLf = maxRange;
    gain->tvg_maxHf = maxRange_HF;

//    updateTvgSettingsOnMainWindow();
}

void userMenu :: saveUserData(){
    connOpenUser();

    if(QSqlDatabase::contains("userConnection")) {
        //format().saveFormatSettings();
        myUserDb = QSqlDatabase::database("userConnection");
        QSqlQuery userqry(myUserDb);

        // Save Data in Table "UserDB"
        userqry.prepare(("DROP TABLE IF EXISTS UserDB"));//Table is recreated with new updated datas everytime save is clicked
        userqry.exec();
        if(userqry.exec()){
            //   qDebug()<<"User Data Updated";
        }
        else
        {       QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());

        }
        userqry.prepare("CREATE TABLE IF NOT EXISTS UserDB "
                        "(ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, B REAL, Pallette VARCHAR(30), DepthPalette VARCHAR(30), TargetIcon INTEGER, TargetAutoplay INTEGER, View_Factor INTEGER,"
                        "Rec_Path VARCHAR(8000), Open_Path VARCHAR(8000), Cable_Length FLOAT, Invert INTEGER, InvertDepth INTEGER, HFBal INTEGER,HFDGUpper FLOAT,"
                        "HFDGLower FLOAT,LFBal INTEGER,LFDGUpper FLOAT,LFDGLower FLOAT, "
                        "Resolution INTEGER, Gain_Add INTEGER, Noise_Thresh FLOAT, Filter_Size INTEGER,"
                        "Units VARCHAR(30), Boat_Speed INTEGER, Sound_Speed INTEGER, Alt_Avg_Length INTEGER, Yaw_Avg_Length INTEGER, "
                        "ZoomLf INTEGER, ZoomHf INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << userqry.lastError();

        }
        //First row/record
        userqry.prepare("INSERT INTO UserDB(B,Pallette,DepthPalette,TargetIcon,TargetAutoplay,View_Factor,"
                        "Rec_Path,Open_Path,Cable_Length,Invert,InvertDepth,HFBal,HFDGUpper,"
                        "HFDGLower,LFBal,LFDGUpper,LFDGLower,"
                        "Gain_Add,Filter_Size,"
                        "Units,Boat_Speed,Sound_Speed,Alt_Avg_Length,Yaw_Avg_Length,"
                        "ZoomLf,ZoomHf) "
                        "values(:bval1, :pallett, :depthPal, :flag, :flagAuto, :vF, :rec_path, :open_path, :cL, :inv, :invD, :hbal, :hupper, :hlower, "
                        ":lbal, :lupper, :llower, :add, :fsize, :Unit, :Boat, :Sound, :Alt_Length, :Yaw_Length, :ZoomLf, :ZoomHf)");

        userqry.bindValue(":bval1", bval1);
        userqry.bindValue(":pallett", usermenu_ui->paletteBox->currentText());
        userqry.bindValue(":depthPal", usermenu_ui->depthPalBox->currentText());
        userqry.bindValue(":flag", uMainWindow->flag.targetIconType);
        userqry.bindValue(":flagAuto", uMainWindow->flag.targetAutoplay);
        userqry.bindValue(":vF", viewFactor);
        userqry.bindValue(":rec_path", recordPath);
        userqry.bindValue(":open_path", openPath);
        userqry.bindValue(":cL", cableLength);
        userqry.bindValue(":inv", usermenu_ui->invertBox->isChecked());
        userqry.bindValue(":invD", usermenu_ui->invertBoxDepth->isChecked());
        userqry.bindValue(":hbal", hfBalanceIndex);
        userqry.bindValue(":hupper", dGainUpperHF);
        userqry.bindValue(":hlower", dGainLowerHF);
        userqry.bindValue(":lbal", lfBalanceIndex);
        userqry.bindValue(":lupper", dGainUpperLF);
        userqry.bindValue(":llower", dGainLowerLF);
        //userqry.bindValue(":res", resLabel);
        userqry.bindValue(":add", gainAdd);
        //userqry.bindValue(":noise", noiseThreshold);
        userqry.bindValue(":fsize", filterSize);
        userqry.bindValue(":Unit", usermenu_ui->unitBox->currentText());
        userqry.bindValue(":Boat", usermenu_ui->boatSpeedBox->currentIndex());
        userqry.bindValue(":Sound", usermenu_ui->ss_line->text().toInt());
        userqry.bindValue(":Alt_Length", 2);
        userqry.bindValue(":Yaw_Length", 4);
        userqry.bindValue(":ZoomLf", zoomLf_index);
        userqry.bindValue(":ZoomHf", zoomHf_index);

        userqry.exec(); //Executes first row, so now the second row can be made

        //Second row/record
        //This record is filled with the default values that a user will receive upon clicking 'apply'
        userqry.prepare("INSERT INTO UserDB(B,Pallette,DepthPalette,TargetIcon,TargetAutoplay,View_Factor,Rec_Path,Open_Path,Cable_Length,Invert,InvertDepth,HFBal,HFDGUpper,HFDGLower,LFBal,LFDGUpper,"
                        "LFDGLower,Gain_Add,Filter_Size,Units,Boat_Speed,Sound_Speed,Alt_Avg_Length,Yaw_Avg_Length,ZoomLf,ZoomHf) "
                        "values(:bval1, :pallett, :depthPal, :flag, :flagAuto, :vF, :rec_path, :open_path, :cL, :inv, :invD, :hbal, :hupper, :hlower, "
                        ":lbal, :lupper, :llower, :add, :fsize, :Unit, :Boat, :Sound, :Alt_Length, :Yaw_Length, :ZoomLf, :ZoomHf)");

        userqry.bindValue(":bval1", bval2);
        userqry.bindValue(":pallett", "Rainbow");
        userqry.bindValue(":depthPal", "Ocean");
        userqry.bindValue(":flag", 0);
        userqry.bindValue(":flagAuto", 1);
        userqry.bindValue(":vF", 0);
        userqry.bindValue(":rec_path", "C:/");
        userqry.bindValue(":open_path", "C:/");
        userqry.bindValue(":cL", 0.0);
        userqry.bindValue(":inv", 0);
        userqry.bindValue(":invD", 0);
        userqry.bindValue(":hbal", 0);
        userqry.bindValue(":hupper", 8191);
        userqry.bindValue(":hlower", 0);
        userqry.bindValue(":lbal", 0);
        userqry.bindValue(":lupper", 8191);
        userqry.bindValue(":llower", 0);
        userqry.bindValue(":res", NULL);
        userqry.bindValue(":add", NULL);
        userqry.bindValue(":noise", NULL);
        userqry.bindValue(":fsize", NULL);
        userqry.bindValue(":Unit", usermenu_ui->unitBox->itemText(0));
        userqry.bindValue(":Boat", 4);
        userqry.bindValue(":Sound", 1500);
        userqry.bindValue(":Alt_Length", 2);
        userqry.bindValue(":Yaw_Length", 4);
        userqry.bindValue(":ZoomLf", 0);
        userqry.bindValue(":ZoomHf", 0);
        userqry.exec();


        // Save data into table "GainSettings"
        userqry.prepare(("DROP TABLE IF EXISTS GainSettings"));//Table is recreated with new updated datas everytime save is clicked
        userqry.exec();
        if(userqry.exec()){        }
        else
        {       QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());

        }

        userqry.prepare("CREATE TABLE IF NOT EXISTS GainSettings ( ID INTEGER, TVG_Enable INTEGER, Sync INTEGER,"
                        "A_LfPortInd INTEGER, A_LfStbdInd INTEGER, A_HfPortInd INTEGER, A_HfStbdInd INTEGER,"
                        "C_LfPort FLOAT, C_LfStbd FLOAT, C_HfPort FLOAT, C_HfStbd FLOAT,"
                        "T INTEGER,"
                        "B_Lf FLOAT, B_Hf FLOAT,"
                        "Gain_Slider_Value INTEGER, Balance_Slider_Value INTEGER,"
                        "Gain_Slider_HF_Value INTEGER, Balance_Slider_HF_Value INTEGER,"
                        "Gain_Slider_Depth_Value INTEGER, Max_range_value INTEGER, Max_range_HF_value INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << "userqry.lastError()" << userqry.lastError();

        }

        userqry.prepare("INSERT INTO GainSettings(TVG_Enable, Sync,"
                        "A_LfPortInd, A_LfStbdInd, A_HfPortInd, A_HfStbdInd,"
                        "C_LfPort, C_LfStbd, C_HfPort, C_HfStbd,"
                        "T,"
                        "B_Lf, B_Hf,"
                        "Gain_Slider_Value, Balance_Slider_Value,"
                        "Gain_Slider_HF_Value, Balance_Slider_HF_Value,"
                        "Gain_Slider_Depth_Value, Max_range_value, Max_range_HF_value)"
                        "values(:tvg, :sync, :aLfP, :aLfS, :aHfP, :aHfS, :cLfP, :cLfS, :cHfP, :cHfS, :t, :bLf, :bHf, :gain, :balance, :gain_HF, :balance_HF, :gain_depth, :Max_range, :Max_range_HF)");

        userqry.bindValue(":tvg", gain->tvg_on);
        userqry.bindValue(":sync", gain->tvg_syncOn);
        userqry.bindValue(":aLfP", gain->tvg_ALfPInd);
        userqry.bindValue(":aLfS", gain->tvg_ALfSInd);
        userqry.bindValue(":aHfP", gain->tvg_AHfPInd);
        userqry.bindValue(":aHfS", gain->tvg_AHfSInd);
        userqry.bindValue(":cLfP", gain->tvg_CLfP);
        userqry.bindValue(":cLfS", gain->tvg_CLfS);
        userqry.bindValue(":cHfP", gain->tvg_CHfP);
        userqry.bindValue(":cHfS", gain->tvg_CHfS);
        userqry.bindValue(":t", gain->tvg_T);
        if (!gain->calculateBLf)    userqry.bindValue(":bLf", gain->tvg_BLf);
        else                        userqry.bindValue(":bLf", QVariant(QVariant::Double));
        if (!gain->calculateBHf)    userqry.bindValue(":bHf", gain->tvg_BHf);
        else                        userqry.bindValue(":bHf", QVariant(QVariant::Double));
        userqry.bindValue(":gain", gain->gainSliderIndexLf);
        userqry.bindValue(":balance", gain->balanceSliderIndex);
        userqry.bindValue(":gain_HF", gain->gainSliderIndexHf);
        userqry.bindValue(":balance_HF", gain->balanceSliderIndex_HF);
        userqry.bindValue(":gain_depth", gain->gainSliderIndexDepth);
        userqry.bindValue(":Max_range",gain->tvg_maxLf);
        userqry.bindValue(":Max_range_HF",gain->tvg_maxHf);

        userqry.exec(); //Executes first row, so now the second row can be made

        //Second row/record
        userqry.prepare("INSERT INTO GainSettings(TVG_Enable, Sync,"
                        "A_LfPortInd, A_LfStbdInd, A_HfPortInd, A_HfStbdInd,"
                        "C_LfPort, C_LfStbd, C_HfPort, C_HfStbd,"
                        "T,"
                        "B_Lf, B_Hf,"
                        "Gain_Slider_Value, Balance_Slider_Value,"
                        "Gain_Slider_HF_Value, Balance_Slider_HF_Value,"
                        "Gain_Slider_Depth_Value, Max_range_value, Max_range_HF_value)"
                        "values(:tvg, :sync, :aLfP, :aLfS, :aHfP, :aHfS, :cLfP, :cLfS, :cHfP, :cHfS, :t, :bLf, :bHf, :gain, :balance, :gain_HF, :balance_HF, :gain_depth, :Max_range, :Max_range_HF)");

        userqry.bindValue(":tvg", 1);
        userqry.bindValue(":sync", 1);
        userqry.bindValue(":aLfP", 2);
        userqry.bindValue(":aLfS", 2);
        userqry.bindValue(":aHfP", 2);
        userqry.bindValue(":aHfS", 2);
        userqry.bindValue(":cLfP", 0);
        userqry.bindValue(":cLfS", 0);
        userqry.bindValue(":cHfP", 0);
        userqry.bindValue(":cHfS", 0);
        userqry.bindValue(":t", 10);
        userqry.bindValue(":bLf", QVariant(QVariant::Double)); // set to NULL
        userqry.bindValue(":bHf", QVariant(QVariant::Double));
        userqry.bindValue(":gain", 0);
        userqry.bindValue(":balance", 0);
        userqry.bindValue(":gain_HF", 0);
        userqry.bindValue(":balance_HF", 0);
        userqry.bindValue(":gain_depth", 0);
        userqry.bindValue(":Max_range", 80);
        userqry.bindValue(":Max_range_HF", 80);

        userqry.exec();

        userqry.prepare(("DROP TABLE IF EXISTS AltimeterSettings"));
        userqry.exec();
        if (!userqry.exec()) {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        }
        userqry.prepare("CREATE TABLE IF NOT EXISTS AltimeterSettings (DepthLabel INTEGER, TractionStrip INTEGER, TransparencyPercentage INTEGER, "
                        "DepthRange INTEGER, Gradient INTEGER, GradientPixel INTEGER, bottomTracking INTEGER, showGap INTEGER, showAltDepth INTEGER, "
                        "projectTarget INTEGER, "
                        "alarmOn INTEGER, alarmLevel INTEGER, alarmVolume INTEGER, alarmIndex INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << "userqry.lastError()" << userqry.lastError();
        }

        userqry.prepare("INSERT INTO AltimeterSettings(DepthLabel,TractionStrip,TransparencyPercentage,DepthRange,Gradient,GradientPixel, bottomTracking, "
                        "showGap, showAltDepth, projectTarget, alarmOn, alarmLevel, alarmVolume, alarmIndex)"
                        " values(:DepthLabel, :TractionStrip, :TransparencyPercentage, :DepthRange, :Gradient, :GradientPixel, :bottomTracking, "
                        ":showGap, :showAltDepth, :projectTarget, :alarm, :alarmLev, :alarmVol, :alarmInd)");

        gapBox = usermenu_ui->GapBox->isChecked();
        userqry.bindValue(":DepthLabel", usermenu_ui->dLabelBtn->isChecked());
        userqry.bindValue(":TractionStrip", usermenu_ui->tStripBtn->isChecked());
        userqry.bindValue(":TransparencyPercentage", usermenu_ui->transparencySlider->value());
        userqry.bindValue(":DepthRange", usermenu_ui->globalDepthBox->currentIndex());
        userqry.bindValue(":Gradient", tempGradient);
        userqry.bindValue(":GradientPixel", usermenu_ui->pixelSlider->value());
        userqry.bindValue(":bottomTracking", usermenu_ui->displayForwardRange->isChecked());
        userqry.bindValue(":showGap", int(gapBox));
        userqry.bindValue(":showAltDepth", uMainWindow->setting.showAltDepth);
        userqry.bindValue(":projectTarget", uMainWindow->setting.projectTargets);
        userqry.bindValue(":alarm", uMainWindow->depthAlarm->activated);
        userqry.bindValue(":alarmLev", uMainWindow->depthAlarm->level);
        userqry.bindValue(":alarmVol", uMainWindow->depthAlarm->volume);
        userqry.bindValue(":alarmInd", uMainWindow->depthAlarm->audioIndex);


        userqry.exec();

        userqry.prepare("INSERT INTO AltimeterSettings(DepthLabel,TractionStrip,TransparencyPercentage,DepthRange,Gradient,GradientPixel, bottomTracking, "
                        "showGap, showAltDepth, projectTarget, alarmOn, alarmLevel, alarmVolume, alarmIndex)"
                        " values(:DepthLabel, :TractionStrip, :TransparencyPercentage, :DepthRange, :Gradient, :GradientPixel, :bottomTracking, "
                        ":showGap, :showAltDepth, :projectTarget, :alarm, :alarmLev, :alarmVol, :alarmInd)");
        userqry.bindValue(":DepthLabel",1);
        userqry.bindValue(":TractionStrip",1);
        userqry.bindValue(":TransparencyPercentage",50);
        userqry.bindValue(":DepthRange",2);
        userqry.bindValue(":Gradient", 1);
        userqry.bindValue(":GradientPixel", 500);
        userqry.bindValue(":bottomTracking", 1);
        userqry.bindValue(":showGap", 0);
        userqry.bindValue(":showAltDepth", 0);
        userqry.bindValue(":projectTarget", 0);
        userqry.bindValue(":alarm", 0);
        userqry.bindValue(":alarmLev", 10);
        userqry.bindValue(":alarmVol", 50);
        userqry.bindValue(":alarmInd", 0);

        if(userqry.exec()){
            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));
            qDebug() << "data Saved";
        } else {
            //userqry.finish();
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";
            connCloseUser();//closing database connection after saving
        }

        userqry.prepare("INSERT INTO PlayerSettings(PauseMode) values(:Pause)");
        userqry.bindValue(":Pause", 1);

        if(userqry.exec()){
            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));
            qDebug() << "data Saved";
        } else {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";
            connCloseUser();//closing database connection after saving
        }

        userqry.prepare(("DROP TABLE IF EXISTS zoom"));
        userqry.exec();
        if (!userqry.exec()) {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        }
        userqry.prepare("CREATE TABLE IF NOT EXISTS zoom (tieLFHF INTEGER, tieDepthSensor INTEGER, verticalShift INTREGER, "
                        "ShiftCountLf INTEGER, ShiftCountHf INTEGER, ShiftDepthSensor INTEGER,  numberOfPings INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << "userqry.lastError()" << userqry.lastError();
        }
        userqry.prepare("INSERT INTO zoom(tieLFHF, tieDepthSensor, verticalShift, ShiftCountLf, ShiftCountHf, ShiftDepthSensor, numberOfPings)"
                        "values(:tieLFHF, :tieDepthSensor, :verticalShift, :ShiftCountLf, :ShiftCountHf, :ShiftDepthSensor, :numberOfPings)");
        userqry.bindValue(":tieLFHF", 1);
        userqry.bindValue(":tieDepthSensor", 1);
        userqry.bindValue(":verticalShift", 4);
        userqry.bindValue(":ShiftCountLf", 0);
        userqry.bindValue(":ShiftCountHf", 0);
        userqry.bindValue(":ShiftDepthSensor", 0);
        userqry.bindValue(":numberOfPings", 2);

        // userqry.bindValue(":zoomStep",zoomValue);
        userqry.exec();
        userqry.prepare("INSERT INTO zoom(tieLFHF, tieDepthSensor, verticalShift, ShiftCountLf, ShiftCountHf, ShiftDepthSensor, numberOfPings)"
                        "values(:tieLFHF, :tieDepthSensor, :verticalShift, :ShiftCountLf, :ShiftCountHf, :ShiftDepthSensor, :numberOfPings)");
        //userqry.bindValue(":untieLFHF", 0);
        userqry.bindValue(":tieLFHF", 1);

        //  userqry.bindValue(":untieLFHF", int(usermenu_ui->untieLFHF->isChecked()));
        userqry.bindValue(":tieDepthSensor", 1);
        userqry.bindValue(":verticalShift", 4);
        userqry.bindValue(":ShiftCountLf", uMainWindow->setting.shiftCounterLf);
        userqry.bindValue(":ShiftCountHf", uMainWindow->setting.shiftCounterHf);
        userqry.bindValue(":ShiftDepthSensor", uMainWindow->setting.shiftCounterDepthSensor);
        userqry.bindValue(":numberOfPings", usermenu_ui->numberOfPings->currentIndex());

        //  userqry.bindValue(":zoomStep",0);
        if(userqry.exec()){
            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));
            qDebug() << "data Saved";
        } else {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";
        }
    }
    else{qDebug()<<"not available"; }
    connCloseUser();

    if(verticalShiftChanged==false){

    }else{
        qDebug()<<"changed";
    }
}

void userMenu :: saveGainSettings() {

    connOpenUser();

    if(QSqlDatabase::contains("userConnection")) {
        //format().saveFormatSettings();
        myUserDb = QSqlDatabase::database("userConnection");
        QSqlQuery userqry(myUserDb);

        // Save data into table "GainSettings"
        userqry.prepare(("DROP TABLE IF EXISTS GainSettings"));//Table is recreated with new updated datas everytime save is clicked
        userqry.exec();
        if(userqry.exec()){        }
        else
        {       QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());

        }

        userqry.prepare("CREATE TABLE IF NOT EXISTS GainSettings ( ID INTEGER, TVG_Enable INTEGER, Sync INTEGER,"
                        "A_LfPortInd INTEGER, A_LfStbdInd INTEGER, A_HfPortInd INTEGER, A_HfStbdInd INTEGER,"
                        "C_LfPort FLOAT, C_LfStbd FLOAT, C_HfPort FLOAT, C_HfStbd FLOAT,"
                        "T INTEGER,"
                        "B_Lf FLOAT, B_Hf FLOAT,"
                        "Gain_Slider_Value INTEGER, Balance_Slider_Value INTEGER,"
                        "Gain_Slider_HF_Value INTEGER, Balance_Slider_HF_Value INTEGER,"
                        "Gain_Slider_Depth_Value INTEGER, Max_range_value INTEGER, Max_range_HF_value INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << "userqry.lastError()" << userqry.lastError();

        }

        userqry.prepare("INSERT INTO GainSettings(TVG_Enable, Sync,"
                        "A_LfPortInd, A_LfStbdInd, A_HfPortInd, A_HfStbdInd,"
                        "C_LfPort, C_LfStbd, C_HfPort, C_HfStbd,"
                        "T,"
                        "B_Lf, B_Hf,"
                        "Gain_Slider_Value, Balance_Slider_Value,"
                        "Gain_Slider_HF_Value, Balance_Slider_HF_Value,"
                        "Gain_Slider_Depth_Value, Max_range_value, Max_range_HF_value)"
                        "values(:tvg, :sync, :aLfP, :aLfS, :aHfP, :aHfS, :cLfP, :cLfS, :cHfP, :cHfS, :t, :bLf, :bHf, :gain, :balance, :gain_HF, :balance_HF, :gain_depth, :Max_range, :Max_range_HF)");

        userqry.bindValue(":tvg", gain->tvg_on);
        userqry.bindValue(":sync", gain->tvg_syncOn);
        userqry.bindValue(":aLfP", gain->tvg_ALfPInd);
        userqry.bindValue(":aLfS", gain->tvg_ALfSInd);
        userqry.bindValue(":aHfP", gain->tvg_AHfPInd);
        userqry.bindValue(":aHfS", gain->tvg_AHfSInd);
        userqry.bindValue(":cLfP", gain->tvg_CLfP);
        userqry.bindValue(":cLfS", gain->tvg_CLfS);
        userqry.bindValue(":cHfP", gain->tvg_CHfP);
        userqry.bindValue(":cHfS", gain->tvg_CHfS);
        userqry.bindValue(":t", gain->tvg_T);
        if (!gain->calculateBLf)    userqry.bindValue(":bLf", gain->tvg_BLf);
        else                        userqry.bindValue(":bLf", QVariant(QVariant::Double));
        if (!gain->calculateBHf)    userqry.bindValue(":bHf", gain->tvg_BHf);
        else                        userqry.bindValue(":bHf", QVariant(QVariant::Double));
        userqry.bindValue(":gain", gain->gainSliderIndexLf);
        userqry.bindValue(":balance", gain->balanceSliderIndex);
        userqry.bindValue(":gain_HF", gain->gainSliderIndexHf);
        userqry.bindValue(":balance_HF", gain->balanceSliderIndex_HF);
        userqry.bindValue(":gain_depth", gain->gainSliderIndexDepth);
        userqry.bindValue(":Max_range",gain->tvg_maxLf);
        userqry.bindValue(":Max_range_HF",gain->tvg_maxHf);

        userqry.exec(); //Executes first row, so now the second row can be made

        //Second row/record
        userqry.prepare("INSERT INTO GainSettings(TVG_Enable, Sync,"
                        "A_LfPortInd, A_LfStbdInd, A_HfPortInd, A_HfStbdInd,"
                        "C_LfPort, C_LfStbd, C_HfPort, C_HfStbd,"
                        "T,"
                        "B_Lf, B_Hf,"
                        "Gain_Slider_Value, Balance_Slider_Value,"
                        "Gain_Slider_HF_Value, Balance_Slider_HF_Value,"
                        "Gain_Slider_Depth_Value, Max_range_value, Max_range_HF_value)"
                        "values(:tvg, :sync, :aLfP, :aLfS, :aHfP, :aHfS, :cLfP, :cLfS, :cHfP, :cHfS, :t, :bLf, :bHf, :gain, :balance, :gain_HF, :balance_HF, :gain_depth, :Max_range, :Max_range_HF)");

        userqry.bindValue(":tvg", 1);
        userqry.bindValue(":sync", 1);
        userqry.bindValue(":aLfP", 2);
        userqry.bindValue(":aLfS", 2);
        userqry.bindValue(":aHfP", 2);
        userqry.bindValue(":aHfS", 2);
        userqry.bindValue(":cLfP", 0);
        userqry.bindValue(":cLfS", 0);
        userqry.bindValue(":cHfP", 0);
        userqry.bindValue(":cHfS", 0);
        userqry.bindValue(":t", 10);
        userqry.bindValue(":bLf", QVariant(QVariant::Double)); // set to NULL
        userqry.bindValue(":bHf", QVariant(QVariant::Double));
        userqry.bindValue(":gain", 0);
        userqry.bindValue(":balance", 0);
        userqry.bindValue(":gain_HF", 0);
        userqry.bindValue(":balance_HF", 0);
        userqry.bindValue(":gain_depth", 0);
        userqry.bindValue(":Max_range", 80);
        userqry.bindValue(":Max_range_HF", 80);

        userqry.exec();
    }
    connCloseUser();
}

void userMenu :: readUserSettings(){
    //    myUserDb = QSqlDatabase::database("userConnection");
    connOpenUser();
    if(!QSqlDatabase::contains("userConnection")) {
        qDebug()<<"not available";
        return;
    }
    QSqlQuery userqry(myUserDb);
    userqry.prepare("SELECT * FROM UserDB ");
    userqry.exec();

    QSqlQueryModel model;
    model.setQuery(userqry);
    if(model.query().exec()){
        /* Main Tab */
        // Palette Box
        customPal = model.record(0).value("Pallette").toString();
        setCustomPalette(customPal);
        setDepthPalette(model.record(0).value("DepthPalette").toString());
        uMainWindow->flag.targetIconType = model.record(0).value("TargetIcon").toInt();
//        uMainWindow->flag.targetAutoplay = model.record(0).value("TargetAutoplay").toInt();
//        usermenu_ui->box_autoplay->setChecked(uMainWindow->flag.targetAutoplay);
        setTargetIcon();
        usermenu_ui->invertBox->setChecked(model.record(0).value("Invert").toInt());
        usermenu_ui->invertBoxDepth->setChecked(model.record(0).value("InvertDepth").toInt());
//        usermenu_ui->paletteBox->setVisible(true);
//        usermenu_ui->paletteBox->setCurrentText(customPal);

        // File Path
        recordPath = verifyFilePath(model.record(0).value("Rec_Path").toString());
        uMainWindow->recorder.recPath = recordPath;
        usermenu_ui->dirPathSaveBox->setText(recordPath);
        openPath = verifyFilePath(model.record(0).value("Open_Path").toString());
        uMainWindow->player.openPath = openPath;
        usermenu_ui->dirPathBox->setText(openPath);

        // "Factors" tab
        cableLength = model.record(0).value("Cable_Length").toString();
        emit updateCL(cableLength.toDouble());
        usermenu_ui->clBox->setText(cableLength);
        hfBalanceIndex = model.record(0).value("HFBal").toInt();
        dGainUpperHF = model.record(0).value("HFDGUpper").toDouble();
        dGainLowerHF = model.record(0).value("HFDGLower").toDouble();
        lfBalanceIndex = model.record(0).value("LFBal").toInt();
        dGainUpperLF = model.record(0).value("LFDGUpper").toDouble();
        dGainLowerLF = model.record(0).value("LFDGLower").toDouble();
        compGPSValue = model.record(0).value("Comp_GPS").toInt();
        gainAdd = model.record(0).value("Gain_Add").toInt();
        filterSize = model.record(0).value("Filter_Size").toInt();

        zoomLf_index = model.record(0).value("ZoomLf").toInt();
        zoomHf_index = model.record(0).value("ZoomHf").toInt();

        usermenu_ui->ss_line->setText(model.record(0).value("Sound_Speed").toString());
        if (usermenu_ui->ss_line->text() == "0") {
            usermenu_ui->ss_line->setText("1500");
        }
        usermenu_ui->unitBox->setCurrentIndex(0);
        usermenu_ui->boatSpeedBox->setCurrentIndex(model.record(0).value("Boat_Speed").toInt());
        emit sendBoatSpeed(usermenu_ui->boatSpeedBox->currentIndex());

        /*new updates*/
        model.clear();

        if (compGPSValue == 0)
        {
            emit compassClicked(true);
            //usermenu_ui->compass_btn->setChecked(true);
            //usermenu_ui->gps_btn->setChecked(false);
        }
        else
        {
            emit compassClicked(false);
            // usermenu_ui->compass_btn->setChecked(false);
            //usermenu_ui->gps_btn->setChecked(true);
        }
        emit sendLFGains(dGainUpperLF,dGainLowerLF,lfBalanceIndex);
        emit sendHFGains(dGainUpperHF,dGainLowerHF,hfBalanceIndex);
        emit sendGainAdd(gainAdd);
        emit sendFSize(filterSize);
        mainPalletteChose(usermenu_ui->paletteBox->currentText());
        depthPaletteChoose(usermenu_ui->depthPalBox->currentText());

        usermenu_ui->box_lfZoom->setCurrentIndex(zoomLf_index);
//        updateZoomSettingLf(usermenu_ui->box_lfZoom->currentText());
        usermenu_ui->box_hfZoom->setCurrentIndex(zoomHf_index);
//        updateZoomSettingHf(usermenu_ui->box_hfZoom->currentText());
    }
    else{
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM GainSettings ");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        gain->tvg_on = model.record(0).value("TVG_Enable").toBool();
        gain->tvg_syncOn = model.record(0).value("Sync").toBool();
        gain->tvg_ALfPInd = model.record(0).value("A_LfPortInd").toInt();
        gain->tvg_ALfSInd = model.record(0).value("A_LfStbdInd").toInt();
        gain->tvg_AHfPInd = model.record(0).value("A_HfPortInd").toInt();
        gain->tvg_AHfSInd = model.record(0).value("A_HfStbdInd").toInt();
        gain->tvg_CLfP = model.record(0).value("C_LfPort").toDouble();
        gain->tvg_CLfS = model.record(0).value("C_LfStbd").toDouble();
        gain->tvg_CHfP = model.record(0).value("C_HfPort").toDouble();
        gain->tvg_CHfS = model.record(0).value("C_HfStbd").toDouble();
        gain->tvg_T = model.record(0).value("T").toInt();

        gain->gainSliderIndexLf = model.record(0).value("Gain_Slider_Value").toInt();
        gain->balanceSliderIndex = model.record(0).value("Balance_Slider_Value").toInt();
        gain->gainSliderIndexHf = model.record(0).value("Gain_Slider_HF_Value").toInt();
        gain->balanceSliderIndex_HF = model.record(0).value("Balance_Slider_HF_Value").toInt();
        gain->gainSliderIndexDepth = model.record(0).value("Gain_Slider_Depth_Value").toInt();

        gain->tvg_maxLf = model.record(0).value("Max_range_value").toInt();
        gain->tvg_maxHf = model.record(0).value("Max_range_HF_value").toInt();
        if (!model.record(0).value("B_Lf").isNull())
            gain->tvg_BLf = model.record(0).value("B_Lf").toDouble();
        else
            gain->tvg_BLf = gain->calculateBValue(GainSettings::LfPort, ping->rangeLf_double);
        if (!model.record(0).value("B_Hf").isNull())
            gain->tvg_BHf = model.record(0).value("B_Hf").toDouble();
        else
            gain->tvg_BHf = gain->calculateBValue(GainSettings::HfPort, ping->rangeHf_double);

        emit updateTvgWindow();
        emit send_default_range(gain->tvg_maxLf,gain->tvg_maxHf);

        on_dirPathBox_textChanged();

    }
    else{
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM AltimeterSettings");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        readDepthLabel = model.record(0).value("DepthLabel").toBool();
        usermenu_ui->dLabelBtn->setChecked(readDepthLabel);
        emit depthLabelHide(readDepthLabel);

        readTractionStrip = model.record(0).value("TractionStrip").toBool();
        usermenu_ui->tStripBtn->setChecked(readTractionStrip);
        emit tractionStripHide(readTractionStrip);

        readTransparencyPercentage = model.record(0).value("TransparencyPercentage").toInt();
        usermenu_ui->transparencySlider->setValue(readTransparencyPercentage);

        readDepthRange = model.record(0).value("DepthRange").toInt();
        usermenu_ui->globalDepthBox->setCurrentIndex(readDepthRange);
        emit sendDepthRange(readDepthRange);

        readGradient = model.record(0).value("Gradient").toInt();
        setGradientChecked(readGradient);

        readPixel = model.record(0).value("GradientPixel").toInt();
        usermenu_ui->pixelSlider->setValue(readPixel);
        emit setAltGradient(readPixel);
        emit setTransparency(readTransparencyPercentage,readGradient);

        displayForwardRange = model.record(0).value("bottomTracking").toBool();
        usermenu_ui->displayForwardRange->setChecked(displayForwardRange);
        emit showForwardRange(displayForwardRange);

        gapBox = model.record(0).value("showGap").toBool();
        usermenu_ui->GapBox->setChecked(gapBox);
        emit showGap(gapBox);

        uMainWindow->setting.showAltDepth = model.record(0).value("showAltDepth").toBool();
        setAltitudeSetting();
        uMainWindow->setting.projectTargets = model.record(0).value("projectTarget").toBool();
        setProjectSetting();

        uMainWindow->depthAlarm->activated = model.record(0).value("alarmOn").toBool();
        uMainWindow->depthAlarm->level = model.record(0).value("alarmLevel").toInt();
        uMainWindow->depthAlarm->volume = model.record(0).value("alarmVolume").toInt();
        uMainWindow->depthAlarm->audioIndex = model.record(0).value("alarmIndex").toInt();
        updateAlarmSettings();
    }
    else {
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM PlayerSettings");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
//        PauseMode = model.record(0).value("PauseMode").toInt();
//        setRadio_pauseMode(PauseMode);
    }
    else {
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM WayPoints");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        targetButton = model.record(0).value("WayPoints").toInt();
    }
    else {
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM zoom");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        uMainWindow->setting.shiftCounterLf = model.record(1).value("ShiftCountLf").toInt();
        uMainWindow->setting.shiftCounterHf = model.record(1).value("ShiftCountHf").toInt();
        uMainWindow->setting.shiftCounterDepthSensor = model.record(1).value("ShiftDepthSensor").toInt();

        numberOfPings = model.record(1).value("numberOfPings").toInt();
        emit numberOfPingsToPlot(numberOfPings);
        usermenu_ui->numberOfPings->setCurrentIndex(numberOfPings);

    }
    else {
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    connCloseUser();

}

void userMenu :: resetGainDefaults() {
    connOpenUser();
    if(!QSqlDatabase::contains("userConnection")) {
        qDebug()<<"not available";
        return;
    }
    QSqlQuery userqry(myUserDb);
    QSqlQueryModel model;

    userqry.prepare("SELECT * FROM GainSettings ");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        gain->tvg_on = model.record(1).value("TVG_Enable").toBool();
        gain->tvg_syncOn = model.record(1).value("Sync").toBool();
        gain->tvg_ALfPInd = model.record(1).value("A_LfPortInd").toInt();
        gain->tvg_ALfSInd = model.record(1).value("A_LfStbdInd").toInt();
        gain->tvg_AHfPInd = model.record(1).value("A_HfPortInd").toInt();
        gain->tvg_AHfSInd = model.record(1).value("A_HfStbdInd").toInt();
        gain->tvg_CLfP = model.record(1).value("C_LfPort").toDouble();
        gain->tvg_CLfS = model.record(1).value("C_LfStbd").toDouble();
        gain->tvg_CHfP = model.record(1).value("C_HfPort").toDouble();
        gain->tvg_CHfS = model.record(1).value("C_HfStbd").toDouble();
        gain->tvg_T = model.record(1).value("T").toInt();

        gain->gainSliderIndexLf = model.record(1).value("Gain_Slider_Value").toInt();
        gain->balanceSliderIndex = model.record(1).value("Balance_Slider_Value").toInt();
        gain->gainSliderIndexHf = model.record(1).value("Gain_Slider_HF_Value").toInt();
        gain->balanceSliderIndex_HF = model.record(1).value("Balance_Slider_HF_Value").toInt();
        gain->gainSliderIndexDepth = model.record(1).value("Gain_Slider_Depth_Value").toInt();

        gain->tvg_frequencyLow = ping->lowFrequency;
        gain->tvg_frequencyHigh = ping->highFrequency;
        gain->tvg_maxLf = model.record(1).value("Max_range_value").toInt();
        gain->tvg_maxHf = model.record(1).value("Max_range_HF_value").toInt();
        gain->tvg_BLf = gain->calculateBValue(GainSettings::LfPort, ping->rangeLf_double);
        gain->tvg_BHf = gain->calculateBValue(GainSettings::HfPort, ping->rangeHf_double);

        emit updateTvgWindow();
        emit send_default_range(gain->tvg_maxLf,gain->tvg_maxHf);

        on_dirPathBox_textChanged();

    } else {
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }
    connCloseUser();

}

void userMenu :: resetDefaults() {
    //    myUserDb = QSqlDatabase::database("userConnection");
    connOpenUser();
    if(!QSqlDatabase::contains("userConnection")) {
        qDebug()<<"not available";
        return;
    }
    QSqlQuery userqry(myUserDb);
    userqry.prepare("SELECT * FROM UserDB ");
    userqry.exec();

    QSqlQueryModel model;
    model.setQuery(userqry);
    if(model.query().exec()){
        //        for (int i = 0; i < 14; ++i) {
        //            Btemp.append(model.record(i).value("B").toDouble());

        //        }
        //        for (int i = 0; i < 14; ++i) {
        //            Bvalue_list[i]->setValue(Btemp[i]);

        //        }
        /*new Updates*/
        setCustomPalette(model.record(1).value("Pallette").toString());
//        usermenu_ui->paletteBox->setCurrentText(customPal);
        setDepthPalette(model.record(1).value("DepthPalette").toString());
        uMainWindow->flag.targetIconType = model.record(1).value("TargetIcon").toInt();
//        uMainWindow->flag.targetAutoplay = model.record(1).value("TargetAutoplay").toInt();
//        usermenu_ui->box_autoplay->setChecked(uMainWindow->flag.targetAutoplay);
        setTargetIcon();

        recordPath = verifyFilePath(model.record(1).value("Rec_Path").toString());
        uMainWindow->recorder.recPath = recordPath;
        usermenu_ui->dirPathSaveBox->setText(recordPath);
        openPath = verifyFilePath(model.record(1).value("Open_Path").toString());
        uMainWindow->player.openPath = openPath;
        usermenu_ui->dirPathBox->setText(openPath);

        cableLength = model.record(1).value("Cable_Length").toString();
        usermenu_ui->clBox->setText(cableLength);
        usermenu_ui->invertBox->setChecked(model.record(1).value("Invert").toInt());
        usermenu_ui->invertBoxDepth->setChecked(model.record(1).value("InvertDepth").toInt());
        hfBalanceIndex = model.record(1).value("HFBal").toInt();
        dGainUpperHF = model.record(1).value("HFDGUpper").toDouble();
        dGainLowerHF = model.record(1).value("HFDGLower").toDouble();
        lfBalanceIndex = model.record(1).value("LFBal").toInt();
        dGainUpperLF = model.record(1).value("LFDGUpper").toDouble();
        dGainLowerLF = model.record(1).value("LFDGLower").toDouble();
        compGPSValue = model.record(1).value("Comp_GPS").toInt();

        usermenu_ui->ss_line->setText(model.record(1).value("Sound_Speed").toString());
        usermenu_ui->unitBox->setCurrentIndex(0);
        usermenu_ui->boatSpeedBox->setCurrentIndex(model.record(1).value("Boat_Speed").toInt());
        emit sendBoatSpeed(usermenu_ui->boatSpeedBox->currentIndex());

        zoomLf_index = model.record(1).value("ZoomLf").toInt();
        zoomHf_index = model.record(1).value("ZoomHf").toInt();

        //qDebug() << "Va: " << compGPSValue;
        /*new updates*/
        model.clear();

        if (compGPSValue == 0)
        {
            emit compassClicked(true);
            //usermenu_ui->compass_btn->setChecked(true);
            //usermenu_ui->gps_btn->setChecked(false);
        }
        else
        {
            emit compassClicked(false);
            // usermenu_ui->compass_btn->setChecked(false);
            //usermenu_ui->gps_btn->setChecked(true);
        }
        emit sendLFGains(dGainUpperLF,dGainLowerLF,lfBalanceIndex);
        emit sendHFGains(dGainUpperHF,dGainLowerHF,hfBalanceIndex);
        mainPalletteChose(usermenu_ui->paletteBox->currentText());
        depthPaletteChoose(usermenu_ui->depthPalBox->currentText());

        usermenu_ui->box_lfZoom->setCurrentIndex(zoomLf_index);
//        updateZoomSettingLf(usermenu_ui->box_lfZoom->currentText());
        usermenu_ui->box_hfZoom->setCurrentIndex(zoomHf_index);
//        updateZoomSettingHf(usermenu_ui->box_hfZoom->currentText());
    }
    else{
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }
/*
    userqry.prepare("SELECT * FROM GainSettings ");
    userqry.exec();

    model.setQuery(userqry);
    if(model.query().exec()){
        gain->tvg_ALfPInd = model.record(1).value("A_LfPortInd").toInt();
        gain->tvg_ALfSInd = model.record(1).value("A_LfStbdInd").toInt();
        gain->tvg_AHfPInd = model.record(1).value("A_HfPortInd").toInt();
        gain->tvg_AHfSInd = model.record(1).value("A_HfStbdInd").toInt();
        gain->tvg_CLfP = model.record(1).value("C_LfPort").toDouble();
        gain->tvg_CLfS = model.record(1).value("C_LfStbd").toDouble();
        gain->tvg_CHfP = model.record(1).value("C_HfPort").toDouble();
        gain->tvg_CHfS = model.record(1).value("C_HfStbd").toDouble();
        gain->tvg_T = model.record(1).value("T").toInt();
        gain->tvg_BLf = gain->calculateBValue(GainSettings::LfPort);
        gain->tvg_BHf = gain->calculateBValue(GainSettings::HfPort);

        gain->gainSliderIndexLf = model.record(1).value("Gain_Slider_Value").toInt();
        gain->balanceSliderIndex = model.record(1).value("Balance_Slider_Value").toInt();
        gain->gainSliderIndexHf = model.record(1).value("Gain_Slider_HF_Value").toInt();
        gain->balanceSliderIndex_HF = model.record(1).value("Balance_Slider_HF_Value").toInt();

        gain->tvg_frequencyLow = ping->lowFrequency;
        gain->tvg_frequencyHigh = ping->highFrequency;

        gain->tvg_DR_Lf = model.record(1).value("Max_range_value").toInt();
        gain->tvg_DR_Hf = model.record(1).value("Max_range_HF_value").toInt();
        emit send_default_range(gain->tvg_DR_Lf,gain->tvg_DR_Hf);

        on_dirPathBox_textChanged();
    }

    else{
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }
*/
    userqry.prepare("SELECT * FROM AltimeterSettings");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        dDepthLabel = model.record(1).value("DepthLabel").toBool();
        usermenu_ui->dLabelBtn->setChecked(dDepthLabel);
        emit depthLabelHide(dDepthLabel);

        dTractionStrip = model.record(1).value("TractionStrip").toBool();
        usermenu_ui->tStripBtn->setChecked(dTractionStrip);
        emit tractionStripHide(dTractionStrip);

        dTransparencyPercentage = model.record(1).value("TransparencyPercentage").toInt();
        usermenu_ui->transparencySlider->setValue(dTransparencyPercentage);

        dDepthRange = model.record(1).value("DepthRange").toInt();
        usermenu_ui->globalDepthBox->setCurrentIndex(2);
        emit depthRangeChanged(dDepthRange);

        dGradient = model.record(1).value("Gradient").toInt();
        setGradientChecked(dGradient);

        dPixel = model.record(1).value("GradientPixel").toInt();
        usermenu_ui->pixelSlider->setValue(dPixel);
        emit setAltGradient(dPixel);
        emit setTransparency(dTransparencyPercentage,dGradient);

        displayForwardRange = model.record(1).value("bottomTracking").toBool();
        usermenu_ui->displayForwardRange->setChecked(displayForwardRange);
        emit showForwardRange(displayForwardRange);

        gapBox = model.record(1).value("showGap").toBool();
        usermenu_ui->GapBox->setChecked(gapBox);
        if (usermenu_ui->GapBox->isEnabled()) {
            emit showGap(gapBox);
        }

        uMainWindow->setting.showAltDepth = model.record(1).value("showAltDepth").toBool();
        setAltitudeSetting();
        uMainWindow->setting.projectTargets = model.record(1).value("projectTarget").toBool();
        setProjectSetting();

        uMainWindow->depthAlarm->activated = model.record(1).value("alarmOn").toBool();
        uMainWindow->depthAlarm->level = model.record(1).value("alarmLevel").toInt();
        uMainWindow->depthAlarm->volume = model.record(1).value("alarmVolume").toInt();
        uMainWindow->depthAlarm->audioIndex = model.record(1).value("alarmIndex").toInt();
        updateAlarmSettings();
    }
    else {
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM PlayerSettings");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
//        PauseMode = model.record(1).value("PauseMode").toInt();
//        setRadio_pauseMode(PauseMode);
    }
    else {
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM WayPoints");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        targetButton = model.record(1).value("WayPoints").toInt();
    }
    else {
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

    userqry.prepare("SELECT * FROM zoom");
    userqry.exec();
    model.setQuery(userqry);
    if(model.query().exec()){
        uMainWindow->setting.shiftCounterLf = model.record(0).value("ShiftCountLf").toInt();
        uMainWindow->setting.shiftCounterHf = model.record(0).value("ShiftCountHf").toInt();
        uMainWindow->setting.shiftCounterDepthSensor = model.record(0).value("ShiftDepthSensor").toInt();

        numberOfPings = model.record(0).value("numberOfPings").toInt();
        emit numberOfPingsToPlot(numberOfPings);
        usermenu_ui->numberOfPings->setCurrentIndex(numberOfPings);

    }
    else {
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }
    connCloseUser();
}

void userMenu :: on_resetSettings_btn_clicked() {
    QMessageBox msgBox;
    // msgBox.warning(0,"Warning","Do you want to reset to default settings? ");
    //msgBox.setInformativeText("Do you want to reset to default settings? ");
    //  msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int ret = QMessageBox::warning(this, tr("Warning"), tr("Do you want to reset to default settings? "),QMessageBox::Yes | QMessageBox::No);
    switch(ret){
    case QMessageBox::Yes:
        resetDefaults();
        break;
    case QMessageBox::No:
        //this->hide();
        break;
    }
}

void userMenu :: setGradientChecked(int value){
    switch (value) {
    case 1:
        usermenu_ui->gradCheckBox1->setChecked(true);
        break;
    case 2:
        usermenu_ui->gradCheckBox2->setChecked(true);
        break;
    case 3:
        usermenu_ui->gradCheckBox3->setChecked(true);
        break;
    case 4:
        usermenu_ui->gradCheckBox4->setChecked(true);
        break;
    case 5:
        usermenu_ui->gradCheckBox5->setChecked(true);
        break;

    }

}

void userMenu :: checkCurrentTab(int i) {

    if (i == 3) //If on the Compass tab
    {
        //        usermenu_ui->acceptButton->hide();
        //        usermenu_ui->reset_btn->hide();
        // usermenu_ui->saveButton->hide();

        //to resolve an issue the buttons for compass tab have been disabled//
        //the switch will be enabled only after the connection is made to the board//

        // usermenu_ui->rBtn->setEnabled(false);
        // usermenu_ui->beginCalibration->setEnabled(false);
    }
    else
    {
        usermenu_ui->acceptButton->show();
        usermenu_ui->resetSettings_btn->show();
        //usermenu_ui->saveButton->show();
    }
}

QString userMenu :: verifyFilePath(QString filePath) {

    if (filePath == NULL || filePath.compare("") == 0 || !QDir(filePath).exists() || filePath == "C:/") {
        filePath = QDir::currentPath();//QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
        if (QFile::exists(QCoreApplication::applicationDirPath() + "\\datapath.txt")) {
            QFile readPath;
            readPath.setFileName(QCoreApplication::applicationDirPath() + "\\datapath.txt");
            readPath.open(QIODevice::ReadWrite);
            QByteArray readPathTxt = readPath.readLine();
            readPath.close();
            filePath = readPathTxt;//.readAll();
        }
        //            recordPath += "/PathwayRecordedFiles";
        //            if (!QDir(recordPath).exists()) {
        //                QDir().mkdir(recordPath);
        //            }
    }
    return filePath;
}

void userMenu :: SaveIniFile (QString filePath) {
    QFile configFile;
    configFile.setFileName(filePath);
    configFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QByteArray configTxt;
    configTxt.append("[User]\n");
    configTxt.append("Pallette_Main=" + customPal + "\n");
    configTxt.append("Pallette_Depth=" + depthPal + "\n");
    configTxt.append("Invert_Main=" + QString::number(usermenu_ui->invertBox->isChecked()) + "\n");
    configTxt.append("Invert_Depth=" + QString::number(usermenu_ui->invertBoxDepth->isChecked()) + "\n");
    configTxt.append("TargetIconType=" + QString::number(uMainWindow->flag.targetIconType) + "\n");
    configTxt.append("TargetAutoplay=" + QString::number(uMainWindow->flag.targetAutoplay) + "\n");
    configTxt.append("Rec_Path=" + recordPath + "\n");
    configTxt.append("Open_Path=" + openPath + "\n");
    configTxt.append("Cable_Length=" + cableLength + "\n");
    configTxt.append("HFBal=" + QString::number(hfBalanceIndex) + "\n");
    configTxt.append("HFDGUpper=" + QString::number(dGainUpperHF) + "\n");
    configTxt.append("HFDGLower=" + QString::number(dGainLowerHF) + "\n");
    configTxt.append("LFBal=" + QString::number(lfBalanceIndex) + "\n");
    configTxt.append("LFDGUpper=" + QString::number(dGainUpperLF) + "\n");
    configTxt.append("LFDGLower=" + QString::number(dGainLowerLF) + "\n");
    configTxt.append("Gain_Add=" + QString::number(gainAdd) + "\n");
    configTxt.append("Filter_Size=" + QString::number(filterSize) + "\n");
    configTxt.append("Units=" + usermenu_ui->unitBox->currentText() + "\n");
    configTxt.append("Boat_Speed=" + QString::number(usermenu_ui->boatSpeedBox->currentIndex()) + "\n");
    configTxt.append("Sound_Speed=" + QString::number(usermenu_ui->ss_line->text().toInt()) + "\n");
    configTxt.append("ZoomLf=" + QString::number(zoomLf_index) + "\n");
    configTxt.append("ZoomHf=" + QString::number(zoomHf_index) + "\n");

    configTxt.append("\n[Tracking]\n");
    configTxt.append("DepthLabel=" + QString::number(usermenu_ui->dLabelBtn->isChecked()) + "\n");
    configTxt.append("TractionStrip=" + QString::number(usermenu_ui->tStripBtn->isChecked()) + "\n");
    configTxt.append("TransparencyPercentage=" + QString::number(usermenu_ui->transparencySlider->value()) + "\n");
    configTxt.append("DepthRange=" + QString::number(usermenu_ui->globalDepthBox->currentIndex()) + "\n");
    configTxt.append("Gradient=" + QString::number(tempGradient) + "\n");
    configTxt.append("GradientPixel=" + QString::number(usermenu_ui->pixelSlider->value()) + "\n");
    configTxt.append("BottomTracking=" + QString::number(usermenu_ui->displayForwardRange->isChecked()) + "\n");
    configTxt.append("showGap=" + QString::number(int(gapBox)) + "\n");
    configTxt.append("showAltDepth=" + QString::number(int(uMainWindow->setting.showAltDepth)) + "\n");
    configTxt.append("projectTarget=" + QString::number(int(uMainWindow->setting.projectTargets)) + "\n");

    configTxt.append("\n[Depth Alarm]\n");
    configTxt.append("AlarmActivate=" + QString::number(uMainWindow->depthAlarm->activated) + "\n");
    configTxt.append("AlarmLevel=" + QString::number(uMainWindow->depthAlarm->level) + "\n");
    configTxt.append("AlarmVolume=" + QString::number(uMainWindow->depthAlarm->volume) + "\n");
    configTxt.append("AlarmTrackIndex=" + QString::number(uMainWindow->depthAlarm->audioIndex) + "\n");

    configTxt.append("\n[TVG]\n");
    configTxt.append("tvgEnable=" +    QString::number(gain->tvg_on) + "\n");
    configTxt.append("tvgSync=" +    QString::number(gain->tvg_syncOn) + "\n");
    configTxt.append("tvgALP=" +   QString::number(gain->tvg_ALfPInd) + "\n");
    configTxt.append("tvgALS=" +   QString::number(gain->tvg_ALfSInd) + "\n");
    configTxt.append("tvgAHP=" +   QString::number(gain->tvg_AHfPInd) + "\n");
    configTxt.append("tvgAHS=" +   QString::number(gain->tvg_AHfSInd) + "\n");
    configTxt.append("tvgCLP=" +      QString::number(gain->tvg_CLfP) + "\n");
    configTxt.append("tvgCLS=" +      QString::number(gain->tvg_CLfS) + "\n");
    configTxt.append("tvgCHP=" +      QString::number(gain->tvg_CHfP) + "\n");
    configTxt.append("tvgCHS=" +      QString::number(gain->tvg_CHfS) + "\n");
    configTxt.append("tvgT=" +      QString::number(gain->tvg_T) + "\n");
    if (!gain->calculateBLf)    configTxt.append("tvgBLf=" +      QString::number(gain->tvg_BLf) + "\n");
    else                        configTxt.append("tvgBLf=-10\n");
    if (!gain->calculateBHf)    configTxt.append("tvgBHf=" +      QString::number(gain->tvg_BHf) + "\n");
    else                        configTxt.append("tvgBHf=-10\n");
    configTxt.append("GainSlider_LF=" +    QString::number(gain->gainSliderIndexLf) + "\n");
    configTxt.append("GainSlider_HF=" + QString::number(gain->gainSliderIndexHf) + "\n");
    configTxt.append("GainSlider_Depth=" +    QString::number(gain->gainSliderIndexDepth) + "\n");
    configTxt.append("Max_range_LF=" +     QString::number(gain->tvg_maxLf) + "\n");
    configTxt.append("Max_range_HF=" +  QString::number(gain->tvg_maxHf) + "\n");

    configTxt.append("\n[Player]\n");
    configTxt.append("PlaybackSpeed=" + QString::number(usermenu_ui->numberOfPings->currentIndex()) + "\n");

    configTxt.append("\n[Zoom]\n");
    configTxt.append("verticalShift=" + QString::number(4) + "\n");
    configTxt.append("ShiftCountLf=" + QString::number(uMainWindow->setting.shiftCounterLf) + "\n");
    configTxt.append("ShiftCountHf=" + QString::number(uMainWindow->setting.shiftCounterHf) + "\n");
    configTxt.append("ShiftDepthSensor=" + QString::number(uMainWindow->setting.shiftCounterDepthSensor) + "\n");

    configTxt.append("\n[Format]\n");
    configTxt.append("showAltimeter=" + QString::number(uMainWindow->Format->isOpen_DepthPlot) + "\n");
    configTxt.append("showSensorGraph=" + QString::number(uMainWindow->Format->isOpen_sensorPlot) + "\n");
    configTxt.append("showSignalPlot=" + QString::number(uMainWindow->Format->isSignalPlotOpen) + "\n");
    configTxt.append("showLF=" + QString::number(uMainWindow->Format->isOpenLf) + "\n");
    configTxt.append("showHF=" + QString::number(uMainWindow->Format->isOpenHf) + "\n");
    configTxt.append("showGPSPlot=" + QString::number(uMainWindow->Format->isOpen_gpsPlot) + "\n");
    configTxt.append("showGainSlider=" + QString::number(uMainWindow->Format->isGainSliderOn) + "\n");
    configTxt.append("showStatusBar=" + QString::number(uMainWindow->Format->isStatusBarShown) + "\n");
    configTxt.append("showGrid=" + QString::number(uMainWindow->Format->isGridShown) + "\n");
    configTxt.append("showZoomBox=" + QString::number(uMainWindow->Format->isZoomBoxAlwaysOn) + "\n");
    configTxt.append("showCompassBox=" + QString::number(uMainWindow->Format->isDisplayCompass) + "\n");
    configTxt.append("showPanelBox=" + QString::number(uMainWindow->tools->panel.separateIcons) + "\n");
    configTxt.append("width_LfHf=" + QString::number(uMainWindow->Format->widthLfHf) + "\n");
    configTxt.append("width_Depth=" + QString::number(uMainWindow->Format->widthDepth) + "\n");
    configTxt.append("width_Sensor=" + QString::number(uMainWindow->Format->widthSensor) + "\n");
    configTxt.append("width_Gnss=" + QString::number(uMainWindow->Format->widthGnss) + "\n");
    configTxt.append("width_Image=" + QString::number(uMainWindow->Format->widthImage) + "\n");
    configTxt.append("width_Targets=" + QString::number(uMainWindow->Format->widthTargets) + "\n");
    configTxt.append("height_Tools=" + QString::number(uMainWindow->Format->heightTools) + "\n");
    configTxt.append("height_Gnss=" + QString::number(uMainWindow->Format->heightGnss) + "\n");
    configTxt.append("height_Lf=" + QString::number(uMainWindow->Format->heightLf) + "\n");
    configTxt.append("height_Hf=" + QString::number(uMainWindow->Format->heightHf) + "\n");
    configTxt.append("width_Window=" + QString::number(uMainWindow->Format->widthWindow) + "\n");
    configTxt.append("height_Window=" + QString::number(uMainWindow->Format->heightWindow) + "\n");
    configTxt.append("window_Maximized=" + QString::number(uMainWindow->Format->windowMaximized) + "\n");

    configFile.write(configTxt);
    configFile.close();
}

void userMenu :: ReadIniFile (QString filePath) {
    QFile IniFile;
    IniFile.setFileName(filePath);
    IniFile.open(QIODevice::ReadWrite | QIODevice::Text);

    IniFile.seek(0);
    QByteArray IniTxt = IniFile.readAll();
    QString IniStr = IniTxt;
    QStringList IniList = IniStr.split("\n");

    for (int i = 0; i < IniList.length(); i++) {
        QStringList IniItem = IniList.at(i).split("=");

        if (IniItem.length() < 2)   continue;
        else if (IniItem.at(0).contains("Pallette_Main")) {
            setCustomPalette(IniItem.at(1));
            mainPalletteChose(usermenu_ui->paletteBox->currentText());
        }
        else if (IniItem.at(0).contains("Pallette_Depth")) {
            setDepthPalette(IniItem.at(1));
            depthPaletteChoose(usermenu_ui->depthPalBox->currentText());
        }
        else if (IniItem.at(0).contains("Invert_Main"))         usermenu_ui->invertBox->setChecked(IniItem.at(1).toInt());
        else if (IniItem.at(0).contains("Invert_Depth"))        usermenu_ui->invertBoxDepth->setChecked(IniItem.at(1).toInt());
        else if (IniItem.at(0).contains("TargetIconType")) {
            uMainWindow->flag.targetIconType = IniItem.at(1).toInt();
            setTargetIcon();
        }
        else if (IniItem.at(0).contains("TargetAutoplay"))  uMainWindow->flag.targetAutoplay = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("Rec_Path")) {
            recordPath = verifyFilePath(IniItem.at(1));
            uMainWindow->recorder.recPath = recordPath;
            usermenu_ui->dirPathSaveBox->setText(recordPath);
        }
        else if (IniItem.at(0).contains("Open_Path")) {
            openPath = verifyFilePath(IniItem.at(1));
            uMainWindow->player.openPath = openPath;
            usermenu_ui->dirPathBox->setText(openPath);
        }
        else if (IniItem.at(0).contains("Cable_Length")) {
            cableLength = IniItem.at(1);
            emit updateCL(cableLength.toDouble());
            usermenu_ui->clBox->setText(cableLength);
        }
        else if (IniItem.at(0).contains("HFBal"))       hfBalanceIndex = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("HFDGUpper"))   dGainUpperHF = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("HFDGLower"))   dGainLowerHF = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("LFBal"))       lfBalanceIndex = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("LFDGUpper"))   dGainUpperLF = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("LFDGLower"))   dGainLowerLF = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("Gain_Add"))    gainAdd = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("Filter_Size")) filterSize = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("Units"))       usermenu_ui->unitBox->setCurrentIndex(0);
        else if (IniItem.at(0).contains("Boat_Speed")) {
            usermenu_ui->boatSpeedBox->setCurrentIndex(IniItem.at(1).toInt());
            emit sendBoatSpeed(usermenu_ui->boatSpeedBox->currentIndex());
        }
        else if (IniItem.at(0).contains("Sound_Speed")) {
            usermenu_ui->ss_line->setText(IniItem.at(1));
            if (usermenu_ui->ss_line->text() == "0") {
                usermenu_ui->ss_line->setText("1500");
            }
        }
        else if (IniItem.at(0).contains("ZoomLf")) {
            usermenu_ui->box_lfZoom->setCurrentIndex(zoomLf_index);
        }
        else if (IniItem.at(0).contains("ZoomHf")) {
            usermenu_ui->box_hfZoom->setCurrentIndex(zoomHf_index);
        }

        else if (IniItem.at(0).contains("DepthLabel")) {
            readDepthLabel = IniItem.at(1).toInt();
            usermenu_ui->dLabelBtn->setChecked(readDepthLabel);
            emit depthLabelHide(readDepthLabel);
        }
        else if (IniItem.at(0).contains("TractionStrip")) {
            readTractionStrip = IniItem.at(1).toInt();
            usermenu_ui->tStripBtn->setChecked(readTractionStrip);
            emit tractionStripHide(readTractionStrip);
        }
        else if (IniItem.at(0).contains("TransparencyPercentage")) {
            readTransparencyPercentage = IniItem.at(1).toInt();
            usermenu_ui->transparencySlider->setValue(readTransparencyPercentage);
            emit setTransparency(readTransparencyPercentage,readGradient);
        }
        else if (IniItem.at(0).contains("DepthRange")) {
            readDepthRange = IniItem.at(1).toInt();
            usermenu_ui->globalDepthBox->setCurrentIndex(readDepthRange);
            emit sendDepthRange(readDepthRange);
        }
        else if (IniItem.at(0).contains("Gradient")) {
            readGradient = IniItem.at(1).toInt();
            setGradientChecked(readGradient);
            emit setTransparency(readTransparencyPercentage,readGradient);
        }
        else if (IniItem.at(0).contains("GradientPixel")) {
            readPixel = IniItem.at(1).toInt();
            usermenu_ui->pixelSlider->setValue(readPixel);
            emit setAltGradient(readPixel);
        }
        else if (IniItem.at(0).contains("BottomTracking")) {
            displayForwardRange = IniItem.at(1).toInt();
            usermenu_ui->displayForwardRange->setChecked(displayForwardRange);
            emit showForwardRange(displayForwardRange);
        }
        else if (IniItem.at(0).contains("showGap")) {
            gapBox = IniItem.at(1).toInt();
            usermenu_ui->GapBox->setChecked(gapBox);
            emit showGap(gapBox);
        }
        else if (IniItem.at(0).contains("showAltDepth")) {
            uMainWindow->setting.showAltDepth = IniItem.at(1).toInt();
            setAltitudeSetting();
        }
        else if (IniItem.at(0).contains("projectTarget")) {
            uMainWindow->setting.projectTargets = IniItem.at(1).toInt();
            setProjectSetting();
        }

        else if (IniItem.at(0).contains("AlarmActivate"))       uMainWindow->depthAlarm->activated = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("AlarmLevel"))          uMainWindow->depthAlarm->level = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("AlarmVolume"))         uMainWindow->depthAlarm->volume = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("AlarmTrackIndex"))     uMainWindow->depthAlarm->audioIndex = IniItem.at(1).toInt();

        else if (IniItem.at(0).contains("tvgEnable"))        gain->tvg_on = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgSync"))        gain->tvg_syncOn = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgT"))        gain->tvg_T = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgALP"))     gain->tvg_ALfPInd = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgALS"))     gain->tvg_ALfSInd = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgAHP"))     gain->tvg_AHfPInd = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgAHS"))     gain->tvg_AHfSInd = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("tvgCLP"))        gain->tvg_CLfP = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("tvgCLS"))        gain->tvg_CLfS = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("tvgCHP"))        gain->tvg_CHfP = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("tvgCHS"))        gain->tvg_CHfS = IniItem.at(1).toDouble();
        else if (IniItem.at(0).contains("tvgBLf")) {
            gain->tvg_BLf = IniItem.at(1).toDouble();
            if (gain->tvg_BLf < 0)      gain->tvg_BLf = gain->calculateBValue(GainSettings::LfPort, ping->rangeLf_double);
        }
        else if (IniItem.at(0).contains("tvgBHf")) {
            gain->tvg_BHf = IniItem.at(1).toDouble();
            if (gain->tvg_BHf < 0)      gain->tvg_BHf = gain->calculateBValue(GainSettings::HfPort, ping->rangeHf_double);
        }
        else if (IniItem.at(0).contains("GainSlider_LF"))      /*uMainWindow->gainSlider_setValueLf(IniItem.at(1).toInt());//*/
            gain->gainSliderIndexLf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("GainSlider_HF"))   /*uMainWindow->gainSlider_setValueHf(IniItem.at(1).toInt());//*/
            gain->gainSliderIndexHf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("GainSlider_Depth"))   /*uMainWindow->gainSlider_setValueDepth(IniItem.at(1).toInt());//*/gain->gainSliderIndexDepth = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("Max_range_LF"))       gain->tvg_maxLf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("Max_range_HF"))    gain->tvg_maxHf = IniItem.at(1).toInt();

        else if (IniItem.at(0).contains("PlaybackSpeed"))           numberOfPings = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("ShiftCountLf")) {
            uMainWindow->setting.shiftCounterLf = IniItem.at(1).toInt();
            uMainWindow->plot_offset_verticalShiftCalculator();
        }
        else if (IniItem.at(0).contains("ShiftCountHf")) {
            uMainWindow->setting.shiftCounterHf = IniItem.at(1).toInt();
            uMainWindow->plot_offset_verticalShiftCalculator();
        }
        else if (IniItem.at(0).contains("ShiftDepthSensor")) {
            uMainWindow->setting.shiftCounterDepthSensor = IniItem.at(1).toInt();
            uMainWindow->plot_offset_verticalShiftCalculator();
        }

        else if (IniItem.at(0).contains("showAltimeter"))        uMainWindow->Format->isOpen_DepthPlot = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showSensorGraph"))        uMainWindow->Format->isOpen_sensorPlot = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showSignalPlot"))         uMainWindow->Format->isSignalPlotOpen = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showLF"))        uMainWindow->Format->isOpenLf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showHF"))        uMainWindow->Format->isOpenHf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showGPSPlot"))        uMainWindow->Format->isOpen_gpsPlot = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showGainSlider"))        uMainWindow->Format->isGainSliderOn = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showStatusBar"))        uMainWindow->Format->isStatusBarShown = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showGrid"))        uMainWindow->Format->isGridShown = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showZoomBox"))        uMainWindow->Format->isZoomBoxAlwaysOn = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("showCompassBox"))        uMainWindow->Format->isDisplayCompass = IniItem.at(1).toInt();
//        else if (IniItem.at(0).contains("showPanelBox"))        uMainWindow->tools->panel.separateIcons = IniItem.at(1).toInt(); //Unused
        else if (IniItem.at(0).contains("width_LfHf"))        uMainWindow->Format->widthLfHf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("width_Depth"))        uMainWindow->Format->widthDepth = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("width_Sensor"))        uMainWindow->Format->widthSensor = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("width_Gnss"))        uMainWindow->Format->widthGnss = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("width_Image"))        uMainWindow->Format->widthImage = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("width_Targets"))        uMainWindow->Format->widthTargets = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("height_Tools"))        uMainWindow->Format->heightTools = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("height_Gnss"))        uMainWindow->Format->heightGnss = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("height_Lf"))        uMainWindow->Format->heightLf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("height_Hf"))        uMainWindow->Format->heightHf = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("width_Window"))        uMainWindow->Format->widthWindow = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("height_Window"))        uMainWindow->Format->heightWindow = IniItem.at(1).toInt();
        else if (IniItem.at(0).contains("window_Maximized"))        uMainWindow->Format->windowMaximized = IniItem.at(1).toInt();
    }

    if (compGPSValue == 0)  emit compassClicked(true);
    else                    emit compassClicked(false);
    emit sendLFGains(dGainUpperLF,dGainLowerLF,lfBalanceIndex);
    emit sendHFGains(dGainUpperHF,dGainLowerHF,hfBalanceIndex);
    emit sendGainAdd(gainAdd);
    emit sendFSize(filterSize);
    emit send_default_range(gain->tvg_maxLf,gain->tvg_maxHf);

    on_dirPathBox_textChanged();

    emit numberOfPingsToPlot(numberOfPings);
    usermenu_ui->numberOfPings->setCurrentIndex(numberOfPings);

    emit updateTvgWindow();
    emit send_default_range(gain->tvg_maxLf,gain->tvg_maxHf);
    updateAlarmSettings();
//    saveUserData();

    uMainWindow->format_updateOtherSettings();
    uMainWindow->format_setPlotSizes();
    uMainWindow->plot_offset_zoomResetPlots();

//    usermenu_ui->box_autoplay->setChecked(uMainWindow->flag.targetAutoplay);
//    uMainWindow->Format->saveFormatSettings();
}

/************************** First Tab *******************************/

void userMenu::setUpComboBox(){
    usermenu_ui->acceptButton->setCheckable(true);
    //usermenu_ui->SwathRadio->setCheckable(true);
    usermenu_ui->attenuationBox->addItem("10");
    usermenu_ui->attenuationBox->addItem("20");
    usermenu_ui->attenuationBox->addItem("40");
    usermenu_ui->paletteBox->addItem("Ocean");
    usermenu_ui->paletteBox->addItem("Rainbow");
    usermenu_ui->paletteBox->addItem("Volcano");
    usermenu_ui->paletteBox->addItem("Iron");
    usermenu_ui->paletteBox->addItem("Sepia");
    usermenu_ui->paletteBox->addItem("Mud");
    usermenu_ui->paletteBox->addItem("Gold");
    usermenu_ui->paletteBox->addItem("Golden Sepia");
    usermenu_ui->paletteBox->addItem("Steel");
    usermenu_ui->depthPalBox->addItem("Ocean");
    usermenu_ui->depthPalBox->addItem("Rainbow");
    usermenu_ui->depthPalBox->addItem("Volcano");
    usermenu_ui->depthPalBox->addItem("Iron");
    usermenu_ui->depthPalBox->addItem("Sepia");
    usermenu_ui->depthPalBox->addItem("Mud");
    usermenu_ui->depthPalBox->addItem("Gold");
    usermenu_ui->depthPalBox->addItem("Golden Sepia");
    usermenu_ui->depthPalBox->addItem("Steel");
    //usermenu_ui->paletteBox->addItem("Custom 1");
    //usermenu_ui->paletteBox->addItem("Custom 2");
    //usermenu_ui->paletteBox->addItem("Custom 3");
    //usermenu_ui->paletteBox->addItem("Custom 4");
    usermenu_ui->unitBox->addItem("Meters");
    usermenu_ui->unitBox->addItem("Yards");
    //new updates
    /*
    if(usermenu_ui->SwathRadio->isChecked()){
        usermenu_ui->swathBar->setEnabled(true);
    }
    else{
        usermenu_ui->swathBar->setEnabled(false);
    }
    */
    usermenu_ui->depthSpacing->addItem("20");
    usermenu_ui->depthSpacing->addItem("40");
    usermenu_ui->depthSpacing->addItem("60");
    usermenu_ui->depthSpacing->addItem("80");

    usermenu_ui->transparencySlider->setRange(0,100);
    usermenu_ui->pixelSlider->setRange(100,1000);
    //
}

void userMenu :: mainPalletteChose(QString paletteVal){
    choosePalette(paletteVal, usermenu_ui->swathBar, usermenu_ui->invertBox->isChecked());
    customPal = paletteVal;
    Custom1 = showPalette(paletteVal, usermenu_ui->invertBox->isChecked());
    emit emitCustomGradient(Custom1);
}

void userMenu :: depthPaletteChoose(QString paletteVal){
    choosePalette(paletteVal, usermenu_ui->swathBar_depth, usermenu_ui->invertBoxDepth->isChecked());
    QCPColorGradient depthCustomColor = showPalette(paletteVal, usermenu_ui->invertBoxDepth->isChecked());
    emit emitDepthGradient(depthCustomColor);
}

void userMenu :: choosePalette(QString paletteVal, QSlider *slider, bool invert){
    // display the palette on the given slider
    if (slider == usermenu_ui->swathBar)
        customPal = paletteVal;
//    else if (slider == usermenu_ui->swathBar_depth)
    QString customSliderColor = "QSlider {border: 1px solid #bbb; border-radius:4px; height: 10px} QSlider::groove:horizontal {background: qlineargradient( ";
    if (!invert)
        customSliderColor += "x1:0 y1:0, x2:1 y2:0,";
    else
        customSliderColor += "x1:1 y1:0, x2:0 y2:0,";
    if(paletteVal=="Ocean"){
        customSliderColor += " stop:0 white,stop:0.3 cyan,stop:0.6 blue stop:1 black) }";
    }
    else if(paletteVal == "Rainbow"){
        customSliderColor += " stop:0 white,stop:0.1 yellow,stop:0.4 orange stop:0.7 purple stop:1 blue)}";
    }
    else if(paletteVal == "Volcano") {
        customSliderColor += " stop:0 white,stop:0.2 yellow,stop:0.4 rgb(255,150,10) stop:0.6 rgb(245,50,0) stop:1 rgb(50,0,0))}";
    }
    else if(paletteVal == "Iron") {
        customSliderColor += " stop:0 white,stop:1 black)}";
    }
    else if(paletteVal == "Sepia") {
        customSliderColor += " stop:0 white stop:0.1 rgb(227,193,165) stop:0.2 rgb(218,174,138) stop:0.3 rgb(219,155,110)"
                             " stop:0.4 rgb(201,136,83) stop:0.5 rgb(190,118,59) stop:0.6 rgb(163,101,50) stop:0.7 rgb(136,84,42)"
                             " stop:0.8 rgb(108,67,33) stop:0.9 rgb(81,50,25) stop:1 rgb(54,33,16))}";
    }
    else if(paletteVal == "Mud") {
        customSliderColor += " stop:0 rgb(243,223,188) stop:0.25 rgb(219,164,86) stop:0.5 rgb(161,95,28)"
                             " stop:0.75 rgb(85,38,5) stop:1 rgb(0,0,0))}";
    }
    else if(paletteVal == "Gold") {
        customSliderColor += " stop:0 rgb(255,255,255) stop:0.25 rgb(244,187,0) stop:0.5 rgb(181,116,0)"
                             " stop:0.75 rgb(107,53,0) stop:1 rgb(0,0,0))}";
    }
    else if(paletteVal == "Golden Sepia") {
        customSliderColor += " stop:0 rgb(255,255,255) stop:0.25 rgb(239,199,121) stop:0.5 rgb(185,129,52)"
                             " stop:0.75 rgb(106,63,19) stop:1 rgb(0,0,0))}";
    }
    else if(paletteVal == "Steel") {
        customSliderColor += " stop:0 rgb(255,255,255) stop:0.25 rgb(181,181,200) stop:0.5 rgb(117,117,139)"
                             " stop:0.75 rgb(59,59,74) stop:1 rgb(0,0,0))}";
    }
    else if(paletteVal == "Custom 1"){
        customSliderColor += " stop:0 rgb(255,20,147),stop:0.1 blue,stop:0.4 yellow stop:0.7 red stop:1 black) }";//sabbir
    }
    else if(paletteVal == "Custom 2"){
        customSliderColor += " stop:0 black,stop:0.1 red,stop:0.5 indigo stop:0.94 rgb(63,42,20) stop:0.98 rgb(204,204,0) stop:1 white)}";
    }
    else if(paletteVal == "Custom 3"){
        customSliderColor += " stop:0 rgb(59,0,229),stop:0.3 rgb(255,0,130),stop:0.9 rgb(0,100,0) stop:0.95 rgb(255,215,0) stop:1 rgb(255,255,255))}";
    }
    else if(paletteVal == "Custom 4") {
        customSliderColor += " stop:0 rgb(100,0,0),stop:0.15 rgb(255,30,0),stop:0.35 rgb(255,255,0) stop:0.65 rgb(0,255,255) stop:85 rgb(0,50,255) stop:1 rgb(0,0,100))}";
    }
    else {
        customSliderColor += " stop:0 white,stop:0.3 cyan,stop:0.6 blue stop:1 black) }";
    }
    slider->setStyleSheet(customSliderColor);
}

void userMenu :: setCustomPalette(QString customPalette){
    customPal = customPalette;
    usermenu_ui->paletteBox->setCurrentText(customPal);
    mainPalletteChose(customPalette);
}

void userMenu :: setDepthPalette(QString customPalette) {
    usermenu_ui->depthPalBox->setCurrentText(customPalette);
    depthPaletteChoose(customPalette);
}

QCPColorGradient userMenu :: showPalette(QString customPalette, bool invert) {
    // return the color gradient for the custom plots
    QCPColorGradient customColor = QCPColorGradient::gpCold;
    if(customPalette == "Ocean"){
        customColor = QCPColorGradient::gpCold;
    }
    else if (customPalette == "Rainbow"){
        customColor = QCPColorGradient::gpThermal;
    }
    else if (customPalette == "Volcano") {
        customColor = QCPColorGradient::gpHot;
    }
    else if (customPalette == "Iron") {
        customColor = QCPColorGradient::gpGrayscale;
    }
    else if(customPalette == "Sepia"){
        customColor.clearColorStops();
        customColor.setColorStopAt(1.0,QColor(255,255,255));
        customColor.setColorStopAt(0.9,QColor(227,193,165));
        customColor.setColorStopAt(0.8,QColor(218,174,138));
        customColor.setColorStopAt(0.7,QColor(219,155,110));
        customColor.setColorStopAt(0.6,QColor(201,136,83));
        customColor.setColorStopAt(0.5,QColor(190,118,59));
        customColor.setColorStopAt(0.4,QColor(163,101,50));
        customColor.setColorStopAt(0.3,QColor(136,84,42));
        customColor.setColorStopAt(0.2,QColor(108,67,33));
        customColor.setColorStopAt(0.1,QColor(81,50,25));
        customColor.setColorStopAt(0.0,QColor(54,33,16));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if(customPalette == "Mud"){
        customColor.clearColorStops();
        customColor.setColorStopAt(1.0,QColor(243,223,188));
        customColor.setColorStopAt(0.75,QColor(219,164,86));
        customColor.setColorStopAt(0.5,QColor(161,95,28));
        customColor.setColorStopAt(0.25,QColor(85,38,5));
        customColor.setColorStopAt(0.0,QColor(0,0,0));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if(customPalette == "Gold"){
        customColor.clearColorStops();
        customColor.setColorStopAt(1.0,QColor(255,255,255));
        customColor.setColorStopAt(0.75,QColor(244,187,0));
        customColor.setColorStopAt(0.5,QColor(181,116,0));
        customColor.setColorStopAt(0.25,QColor(107,53,0));
        customColor.setColorStopAt(0.0,QColor(0,0,0));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if(customPalette == "Golden Sepia"){
        customColor.clearColorStops();
        customColor.setColorStopAt(1.0,QColor(255,255,255));
        customColor.setColorStopAt(0.75,QColor(239,199,121));
        customColor.setColorStopAt(0.5,QColor(185,129,52));
        customColor.setColorStopAt(0.25,QColor(106,63,19));
        customColor.setColorStopAt(0.0,QColor(0,0,0));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if(customPalette == "Steel"){
        customColor.clearColorStops();
        customColor.setColorStopAt(1.0,QColor(255,255,255));
        customColor.setColorStopAt(0.75,QColor(181,181,200));
        customColor.setColorStopAt(0.5,QColor(117,117,139));
        customColor.setColorStopAt(0.25,QColor(59,59,74));
        customColor.setColorStopAt(0.0,QColor(0,0,0));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if(customPalette == "Custom 1"){
        customColor.clearColorStops();
        customColor.setColorStopAt(0,QColor(0,0,0));
        customColor.setColorStopAt(0.3,QColor(255,0,0));
        customColor.setColorStopAt(0.6,QColor(255,255,51));
        customColor.setColorStopAt(0.9,QColor(0,0,255));
        customColor.setColorStopAt(1.0,QColor(255,20,147));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if (customPalette == "Custom 2") {
        customColor.clearColorStops();
        customColor.setColorStopAt(0,QColor(255,255,255));
        customColor.setColorStopAt(0.02,QColor(204,204,0));
        customColor.setColorStopAt(0.06,QColor(63,42,20));
        customColor.setColorStopAt(0.5,QColor(75,0,130));
        customColor.setColorStopAt(0.9,QColor(255,0,0));
        customColor.setColorStopAt(1.0,QColor(0,0,0));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if (customPalette == "Custom 3") {
        customColor.clearColorStops();
        customColor.setColorStopAt(0,QColor(255,255,255));
        customColor.setColorStopAt(0.05,QColor(255,215,0));
        customColor.setColorStopAt(0.1,QColor(0,100,0));
        customColor.setColorStopAt(0.7,QColor(255,0,130));
        customColor.setColorStopAt(1.0,QColor(59,0,229));
        customColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);
    }
    else if (customPalette == "Custom 4") {
        customColor = QCPColorGradient::gpJet;
    }
    if (invert)     customColor = customColor.inverted();
    Custom1 = customColor;
    return customColor;
}

void userMenu :: on_invertBox_clicked() {
    mainPalletteChose(usermenu_ui->paletteBox->currentText());
}

void userMenu :: on_invertBoxDepth_clicked() {
    depthPaletteChoose(usermenu_ui->depthPalBox->currentText());
}

void userMenu :: on_colorScaleBox_clicked() {

    //hide_color_scale = usermenu_ui->colorScaleBox->isChecked();
    if(hide_color_scale){
        emit hideColorScale(true);
    }
    else{
        emit hideColorScale(false);
    }

}

void userMenu :: on_browseFile_btn_clicked() {
    //    QString data_filepath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
    //                                                              "C:/Users/Le/Documents/Pathway 2019-11-28 T2.1c/data",

    QString data_filepath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                              //                                                          "C:/",
                                                              usermenu_ui->dirPathBox->text(),//qApp->applicationDirPath(),
                                                              QFileDialog::ShowDirsOnly
                                                              | QFileDialog::DontResolveSymlinks);
    //add an iff statement to check if the user does not enter a file name.
    openPath = verifyFilePath(data_filepath);
    uMainWindow->player.openPath = openPath;
    usermenu_ui->dirPathBox->setText(openPath);

    qDebug()<<"application dir Path"<<qApp->applicationDirPath();
    qDebug()<<"application file path"<<qApp->applicationFilePath();
    qDebug()<<"root path"<<QDir::rootPath();
    qDebug()<<"home path"<<QDir::homePath();
}

void userMenu :: on_browseSave_btn_clicked() {
    //    QString data_filepath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
    //                                                              "C:/Users/Le/Documents/Pathway 2019-11-28 T2.1c/data",

    QString data_filepath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                              //                                                          "C:/",
                                                              usermenu_ui->dirPathSaveBox->text(),//qApp->applicationDirPath(),
                                                              QFileDialog::ShowDirsOnly
                                                              | QFileDialog::DontResolveSymlinks);
    //add an iff statement to check if the user does not enter a file name.

    recordPath = verifyFilePath(data_filepath);
    uMainWindow->recorder.recPath = recordPath;
    usermenu_ui->dirPathSaveBox->setText(recordPath);

    qDebug()<<"application dir Path"<<qApp->applicationDirPath();
    qDebug()<<"application file path"<<qApp->applicationFilePath();
    qDebug()<<"root path"<<QDir::rootPath();
    qDebug()<<"home path"<<QDir::homePath();
}

void userMenu :: on_gps_btn_clicked() {
    emit compassClicked(false);
}

void userMenu :: on_compass_btn_clicked(){

    emit compassClicked(true);
}

void userMenu :: on_low_freq_radio_btn_clicked() {
    if(lfB == 0){
        lfB = 1;
        hfB = 0;
        // emit lfChanged(true);
        emit setToLF(true);
    }
    else{
        lfB = 1;
        hfB = 0;
    }
}

void userMenu :: on_high_freq_radio_btn_clicked() {
    if(hfB == 0){
        hfB = 1;
        lfB = 0;
        //emit hfChanged(true);
        emit setToLF(false);
    }
    else{
        hfB = 1;
        lfB = 0;
    }
}

void userMenu :: updateDirPath(QString path) {
    recordPath = path;
    usermenu_ui->dirPathBox->setText(path);
}

void userMenu :: changeSoundSpeed(double ss) {
    usermenu_ui->ss_line->setText(QString::number(ss));
    speedofSound = usermenu_ui->ss_line->text();
}

void userMenu :: changeCableLength(double cl) {
    usermenu_ui->clBox->setText(QString::number(cl));
    cableLength = usermenu_ui->clBox->text();
}

void userMenu :: on_ethernetBtn_clicked(){
    emit ethernetActive(true);
}

void userMenu :: on_USBBtn_clicked() {
    emit ethernetActive(false);
}

void userMenu :: boatSpeedChanged(int i) {
    emit sendBoatSpeed(i);
}

void userMenu :: depthRangeChanged(int i){
    emit sendDepthRange(i);
}

void userMenu :: receiveDisableBoatSpeed(bool b) {
    if (b)
    {
        usermenu_ui->boatSpeedBox->setEnabled(false);
    }
    else
    {
        usermenu_ui->boatSpeedBox->setEnabled(true);
    }
}

void userMenu :: on_dirPathBox_textChanged(){

    //    if (recordPath == NULL || recordPath.compare("") == 0 || !QDir(recordPath).exists()) {
    //        recordPath = QDir::currentPath();
    //        recordPath += "/recorded_files";
    //        if (!QDir(recordPath).exists()) {
    //            QDir().mkdir(recordPath);
    //        }
    //    }
    //    recordPath = usermenu_ui->dirPathBox->text();
    //    emit sendRecPath(recordPath);

}

void userMenu :: onNewTargetIconSelected() {
    if (usermenu_ui->radio_target_red->isChecked())     uMainWindow->flag.targetIconType = 0;
    else if (usermenu_ui->radio_target_yellow->isChecked())     uMainWindow->flag.targetIconType = 1;
    else if (usermenu_ui->radio_target_blue->isChecked())     uMainWindow->flag.targetIconType = 2;
    else if (usermenu_ui->radio_target_green->isChecked())     uMainWindow->flag.targetIconType = 3;
    uMainWindow->target_UpdateIconType();
}

void userMenu :: setTargetIcon() {
    if (uMainWindow->flag.targetIconType == 0)       usermenu_ui->radio_target_red->setChecked(true);
    else if (uMainWindow->flag.targetIconType == 1)  usermenu_ui->radio_target_yellow->setChecked(true);
    else if (uMainWindow->flag.targetIconType == 2)  usermenu_ui->radio_target_blue->setChecked(true);
    else if (uMainWindow->flag.targetIconType == 3)  usermenu_ui->radio_target_green->setChecked(true);
    uMainWindow->target_UpdateIconType();
}

void userMenu :: setCustomLfZoom() {
    if (usermenu_ui->box_lfZoom->itemText(8) != "--")
        usermenu_ui->box_lfZoom->insertItem(8, "--");
    usermenu_ui->box_lfZoom->setCurrentText("--");
    uMainWindow->flag.zoomLfFixed = false;
}

void userMenu :: setCustomHfZoom() {
    if (usermenu_ui->box_hfZoom->itemText(8) != "--")
        usermenu_ui->box_hfZoom->insertItem(8, "--");
    usermenu_ui->box_hfZoom->setCurrentText("--");
    uMainWindow->flag.zoomHfFixed = false;
}

void userMenu :: resetLfZoom() {
    usermenu_ui->box_lfZoom->setCurrentIndex(zoomLf_index);
    uMainWindow->flag.zoomLfFixed = true;
}

void userMenu :: resetHfZoom() {
    usermenu_ui->box_hfZoom->setCurrentIndex(zoomHf_index);
    uMainWindow->flag.zoomHfFixed = true;
}

void userMenu :: updateZoomSettingLf(QString str) {
    if (str != "--") {
        uMainWindow->setting.zoomMagnitude_lf = str.toInt() / 100.0;
        zoomLf_index = usermenu_ui->box_lfZoom->currentIndex();
        uMainWindow->flag.zoomLfFixed = true;
        uMainWindow->plot_offset_zoomResetLf();
        if (usermenu_ui->box_lfZoom->itemText(8) == "--")
            usermenu_ui->box_lfZoom->removeItem(8);
    }
    uMainWindow->plot_showUpdatedZoomLegends();
}

void userMenu :: updateZoomSettingHf(QString str) {
    if (str != "--") {
        uMainWindow->setting.zoomMagnitude_hf = str.toInt() / 100.0;
        zoomHf_index = usermenu_ui->box_hfZoom->currentIndex();
        uMainWindow->flag.zoomHfFixed = true;
        uMainWindow->plot_offset_zoomResetHf();
        if (usermenu_ui->box_hfZoom->itemText(8) == "--")
            usermenu_ui->box_hfZoom->removeItem(8);
    }
    uMainWindow->plot_showUpdatedZoomLegends();
}

/************************** TVG Tab *******************************/

/************************** Tow Control Tab *******************************/

void userMenu :: on_enable_btn_clicked() {
    towfishEnabled = true;
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
    usermenu_ui->groupBox_41->setEnabled(true);
    usermenu_ui->groupBox_42->setEnabled(true);
    usermenu_ui->groupBox_45->setEnabled(true);
    usermenu_ui->groupBox_46->setEnabled(true);
}

void userMenu :: on_disable_btn_clicked(){
    towfishEnabled = false;
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
    usermenu_ui->groupBox_41->setEnabled(false);
    usermenu_ui->groupBox_42->setEnabled(false);
    usermenu_ui->groupBox_45->setEnabled(false);
    usermenu_ui->groupBox_46->setEnabled(false);
}

void userMenu :: on_global_btn_clicked() {
    globalEnabled = true;
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
}

void userMenu :: on_relative_btn_clicked() {
    globalEnabled = false;
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
}

void userMenu :: on_small_btn_clicked() {
    lineLength = (1);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
}

void userMenu :: on_med_btn_clicked() {
    lineLength = (2);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
}

void userMenu :: on_large_btn_clicked() {
    lineLength = (3);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
}

void userMenu :: on_av_enable_btn_clicked() {
    avEnabled = (true);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
    usermenu_ui->listSizeSlider->setEnabled(true);
    usermenu_ui->listSizeLabel->show();
}

void userMenu :: on_av_disable_btn_clicked() {
    avEnabled = (false);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
    usermenu_ui->listSizeSlider->setEnabled(false);
    usermenu_ui->listSizeLabel->hide();
}

void userMenu :: receiveThreshValue(int i) {
    threshValue = (i);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
    QString valueDisplay = QString("%1°").arg(i);
    usermenu_ui->sliderLabel->setText(valueDisplay);
}

void userMenu :: receiveListSize(int i) {
    listSizeValue = (i);
    emit sendTowfishParameters(towfishEnabled, globalEnabled, lineLength, avEnabled, threshValue, listSizeValue);
    usermenu_ui->listSizeLabel->setNum(i);
}

/************************** Compass Tab *******************************/

void userMenu :: receiveSys(float x, float y) {  //Compass tab

    usermenu_ui->xCoord->setText(QString::number(x));
    usermenu_ui->yCoord->setText(QString::number(y));
}

void userMenu :: on_beginCalibration_clicked() { //Compass tab

    calibrateRestart = true;
    calibrateActive = true;
    usermenu_ui->nextBtn->setEnabled(true);
    usermenu_ui->nextBtn->setText("Next");

    usermenu_ui->calComplete->hide();
    usermenu_ui->northDir->show();
    usermenu_ui->southDir->hide();
    usermenu_ui->eastDir->hide();
    usermenu_ui->westDir->hide();

    usermenu_ui->n1->hide(); usermenu_ui->n2->hide();
    usermenu_ui->n3->hide(); usermenu_ui->n4->hide();
    usermenu_ui->n5->hide(); usermenu_ui->n6->hide();
    usermenu_ui->n7->hide(); usermenu_ui->n8->hide();

    usermenu_ui->xCoord->move(120, 420);
    usermenu_ui->yCoord->move(230, 420);
    usermenu_ui->xCoord->show();
    usermenu_ui->yCoord->show();

    emit sendCompass('0');
    receiveMessage("Point the sonar North and press 'Next'");
}

void userMenu :: on_nextBtn_clicked() {         //Compass tab

    static int i = 0;
    if (calibrateRestart)
    {
        i = 0;
        calibrateRestart = false;
        receiveMessage("Point the sonar North and press 'Next'");
    }

    if (i == 0)
    {
        usermenu_ui->northDir->hide();
        usermenu_ui->westDir->show();
        QString nx = usermenu_ui->xCoord->text();
        QString ny = usermenu_ui->yCoord->text();
        usermenu_ui->n1->setText(nx); usermenu_ui->n1->show();
        usermenu_ui->n2->setText(ny); usermenu_ui->n2->show();
        emit sendCompass('N');
        usermenu_ui->xCoord->move(120, 460);
        usermenu_ui->yCoord->move(230, 460);
        i++;
        receiveMessage("Point the sonar East and press 'Next'");
    }
    else if (i == 1)
    {
        usermenu_ui->westDir->hide();
        usermenu_ui->southDir->show();
        QString wx = usermenu_ui->xCoord->text();
        QString wy = usermenu_ui->yCoord->text();
        usermenu_ui->n3->setText(wx); usermenu_ui->n3->show();
        usermenu_ui->n4->setText(wy); usermenu_ui->n4->show();
        emit sendCompass('W');
        usermenu_ui->xCoord->move(120, 500);
        usermenu_ui->yCoord->move(230, 500);
        i++;
        receiveMessage("Point the sonar South and press 'Next'");
    }
    else if (i == 2)
    {
        usermenu_ui->southDir->hide();
        usermenu_ui->eastDir->show();
        QString sx = usermenu_ui->xCoord->text();
        QString sy = usermenu_ui->yCoord->text();
        usermenu_ui->n5->setText(sx); usermenu_ui->n5->show();
        usermenu_ui->n6->setText(sy); usermenu_ui->n6->show();
        emit sendCompass('S');
        usermenu_ui->xCoord->move(120, 540);
        usermenu_ui->yCoord->move(230, 540);
        usermenu_ui->nextBtn->setText("Done");
        i++;
        receiveMessage("Point the sonar South and press 'Next'");
    }
    else if (i == 3)
    {
        usermenu_ui->eastDir->hide();
        usermenu_ui->calComplete->show();
        QString ex = usermenu_ui->xCoord->text();
        QString ey = usermenu_ui->yCoord->text();
        usermenu_ui->n7->setText(ex); usermenu_ui->n7->show();
        usermenu_ui->n8->setText(ey); usermenu_ui->n8->show();
        emit sendCompass('E');
        usermenu_ui->xCoord->hide();
        usermenu_ui->yCoord->hide();
        i++;
        usermenu_ui->nextBtn->setEnabled(false);
        calibrateActive = false;
        receiveMessage("Calibration complete");
    }
}

void userMenu :: on_rBtn_clicked() {            //Compass tab
    emit sendDateData(1);
}

void userMenu :: receiveMessage(QString message) {
    usermenu_ui->dataLabel->setText(message);
    usermenu_ui->dataLabel->show();
}

/**************************Altimeter Settings Tab**************************/

void userMenu::on_dLabelBtn_clicked(){
    if(usermenu_ui->dLabelBtn->isChecked()){
        emit depthLabelHide(true);
    }else{
        emit depthLabelHide(false );
    }

}

void userMenu::on_tStripBtn_clicked(){
    if(usermenu_ui->tStripBtn->isChecked()){
        emit tractionStripHide(true);
    }else{
        emit tractionStripHide(false);
    }
}

void userMenu::on_gradCheckBox1_clicked(){
    //emit receiveGradients(1);
    tempGradient=1;
    emit setTransparency(transparencySliderValue,tempGradient);
}

void userMenu::on_gradCheckBox2_clicked(){
    tempGradient=2;
    emit setTransparency(transparencySliderValue,tempGradient);
}

void userMenu::on_gradCheckBox3_clicked(){
    tempGradient=3;
    emit setTransparency(transparencySliderValue,tempGradient);
}

void userMenu::on_gradCheckBox4_clicked(){
    tempGradient=4;
    emit setTransparency(transparencySliderValue,tempGradient);
}

void userMenu::on_gradCheckBox5_clicked(){
    tempGradient=5;
    emit setTransparency(transparencySliderValue,tempGradient);
}

void userMenu :: on_transparencySlider_valueChanged(int value){
    QString valDisplay = QString("%1%").arg(value);
    usermenu_ui->tSliderLabel->setText(valDisplay);
    transparencySliderValue = value;
    emit setTransparency(transparencySliderValue,tempGradient);
}

void userMenu :: on_pixelSlider_valueChanged(int value){
    QString valDisplay = QString("%1 px").arg(value);
    usermenu_ui->pSliderLabel->setText(valDisplay);
    emit setAltGradient(value);
    emit setTransparency(transparencySliderValue,tempGradient);

}

void userMenu :: on_btn_Altitude_clicked() {
    usermenu_ui->btn_Altitude->setChecked(true);
    uMainWindow->setting.showAltDepth = false;
    usermenu_ui->btn_AltDepth->setChecked(false);
    uMainWindow->status_updateAltLabel();
}

void userMenu :: on_btn_AltDepth_clicked() {
    usermenu_ui->btn_AltDepth->setChecked(true);
    uMainWindow->setting.showAltDepth = true;
    usermenu_ui->btn_Altitude->setChecked(false);
    uMainWindow->status_updateAltLabel();
}

void userMenu :: setAltitudeSetting() {
    if (uMainWindow->setting.showAltDepth) {
        usermenu_ui->btn_Altitude->setChecked(false);
        usermenu_ui->btn_AltDepth->setChecked(true);
    } else {
        usermenu_ui->btn_Altitude->setChecked(true);
        usermenu_ui->btn_AltDepth->setChecked(false);
    }
}

void userMenu :: on_btn_project_clicked() {
    uMainWindow->setting.projectTargets = usermenu_ui->btn_project->isChecked();
    uMainWindow->target_updateAltPlotPosition();
}

void userMenu :: setProjectSetting() {
    usermenu_ui->btn_project->setChecked(uMainWindow->setting.projectTargets);
    uMainWindow->target_updateAltPlotPosition();
}

bool userMenu :: enableGap(bool state) {
    usermenu_ui->GapBox->setEnabled(state);
    if (state)
        return gapBox;
    else
        return true;
}

/**************************File Player Tab**************************/

void userMenu :: on_numberOfPings_currentIndexChanged() {
    emit numberOfPingsToPlot(usermenu_ui->numberOfPings->currentIndex());
}

/************************** System Settings *******************************/

void userMenu :: setLFDGain(double upper, double lower) {
    dGainUpperLF = upper;
    dGainLowerLF = lower;
}

void userMenu :: setHFDGain(double upper, double lower) {
    dGainUpperHF = upper;
    dGainLowerHF = lower;
}

void userMenu :: setLFBalance(int value) {
    lfBalanceIndex = value;
}

void userMenu :: setHFBalance(int value) {
    hfBalanceIndex = value;
}

void userMenu :: setGainAdd(int add) {
    // qDebug() << "Usermenu Add gain set";
    gainAdd = add;
}

void userMenu :: setNoiseThreshold(double thresh) {
    noiseThreshold = thresh;
}

void userMenu :: setFSize(int size) {
    filterSize = size;
    //qDebug() << "Database filter size set";
}

/************************** Unused *******************************/

void userMenu::plotLogGraph(){
    const int dataCount = 5000;
    QVector<QCPGraphData> dataLinear(dataCount);
    for (int i=0; i<dataCount; ++i)
    {
        double a= usermenu_ui->attenuationBox->currentText().toDouble();
        dataLinear[i].key = (i*500);
        dataLinear[i].value = dataLinear[i].key;}


    QVector <double> x(5000),y(5000);
    for(int i = 0 ; i<2000;i++){
        x[i] = i+50;
        y[i] = 50*log(x[i])+20;


    }
    //    usermenu_ui->logGraph->graph(0)->setData(x, y);
    //    usermenu_ui->logGraph->xAxis->setLabel("x");
    //    usermenu_ui->logGraph->yAxis->setLabel("y");
    //    usermenu_ui->logGraph->yAxis->setScaleType(QCPAxis::stLogarithmic);
    //    usermenu_ui->logGraph->xAxis->setRange(0,5000);
    //    usermenu_ui->logGraph->yAxis->setRange(1e-2, 1e10);
    //    usermenu_ui->logGraph->replot();
}

void userMenu::setBValue(QList<double> customBValues){
    for (int i = 0 ; i<14;i++){
        Bvals.append(customBValues[i]);
    }
    emit sendBValues(customBValues);

}

void userMenu::saveGains(){
    bval1 = usermenu_ui->bVal1->text();
    bval2 = usermenu_ui->bVal2->text();
    bval3 = usermenu_ui->bVal3->text();
    bval4 = usermenu_ui->bVal4->text();
    bval5 = usermenu_ui->bVal5->text();
    bval6 = usermenu_ui->bVal6->text();
    bval7 = usermenu_ui->bVal7->text();
    bval8 = usermenu_ui->bVal8->text();
    bval9 = usermenu_ui->bVal9->text();
    bval10 = usermenu_ui->bVal10->text();
    bval11= usermenu_ui->bVal11->text();
    bval12= usermenu_ui->bVal12->text();
    bval13= usermenu_ui->bVal13->text();
    bval14= usermenu_ui->bVal14->text();

    for(int i = 0 ; i<14;i++){
        Bvals.append(Bvalue_list[i]->text().toDouble());
    }

    setBValue(Bvals);
}

void userMenu :: clickCounter(int countLF, int countHF, int countDepthSensor){
    uMainWindow->setting.shiftCounterLf = countLF;
    uMainWindow->setting.shiftCounterHf = countHF;
    uMainWindow->setting.shiftCounterDepthSensor = countDepthSensor;
    if(!myUserDb.open())
        saveZoomClicks();
}

void userMenu :: saveZoomClicks(){
    //    connCloseUser();
    connOpenUser();

    if(QSqlDatabase::contains("userConnection")) {

        myUserDb = QSqlDatabase::database("userConnection");
        QSqlQuery userqry(myUserDb);
        userqry.prepare(("DROP TABLE IF EXISTS zoom"));
        userqry.exec();
        if (!userqry.exec()) {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        }
        userqry.prepare("CREATE TABLE IF NOT EXISTS zoom (tieLFHF INTEGER, tieDepthSensor INTEGER, verticalShift INTREGER, "
                        "ShiftCountLf INTEGER, ShiftCountHf INTEGER, ShiftDepthSensor INTEGER,  numberOfPings INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << "userqry.lastError()" << userqry.lastError();
        }
        userqry.prepare("INSERT INTO zoom(tieLFHF, tieDepthSensor, verticalShift, ShiftCountLf, ShiftCountHf, ShiftDepthSensor, numberOfPings)"
                        "values(:tieLFHF, :tieDepthSensor, :verticalShift, :ShiftCountLf, :ShiftCountHf, :ShiftDepthSensor, :numberOfPings)");
        userqry.bindValue(":tieLFHF", 1);
        userqry.bindValue(":tieDepthSensor", 1);
        userqry.bindValue(":verticalShift", 4);
        userqry.bindValue(":ShiftCountLf", 0);
        userqry.bindValue(":ShiftCountHf", 0);
        userqry.bindValue(":ShiftDepthSensor", 0);
        userqry.bindValue(":numberOfPings", 2);

        // userqry.bindValue(":zoomStep",zoomValue);
        userqry.exec();
        userqry.prepare("INSERT INTO zoom(tieLFHF, tieDepthSensor, verticalShift, ShiftCountLf, ShiftCountHf, ShiftDepthSensor, numberOfPings)"
                        "values(:tieLFHF, :tieDepthSensor, :verticalShift, :ShiftCountLf, :ShiftCountHf, :ShiftDepthSensor, :numberOfPings)");
        //userqry.bindValue(":untieLFHF", 0);
        userqry.bindValue(":tieLFHF", 1);

        //  userqry.bindValue(":untieLFHF", int(usermenu_ui->untieLFHF->isChecked()));
        userqry.bindValue(":tieDepthSensor", 1);
        userqry.bindValue(":verticalShift", 4);
        userqry.bindValue(":ShiftCountLf", uMainWindow->setting.shiftCounterLf);
        userqry.bindValue(":ShiftCountHf", uMainWindow->setting.shiftCounterHf);
        userqry.bindValue(":ShiftDepthSensor", uMainWindow->setting.shiftCounterDepthSensor);
        userqry.bindValue(":numberOfPings", usermenu_ui->numberOfPings->currentIndex());
        if(userqry.exec()){
            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));
            qDebug() << "data Saved";
        } else {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";
            //connCloseUser();//closing database connection after saving
            //            QSqlDatabase::removeDatabase("userConnection");
        }


    }

    connCloseUser();

//    if(verticalShiftChanged == true){
//        emit resetZoomOnAllPlots(true);
//    }
    //    saveUserData();

}

void userMenu :: on_displayForwardRange_clicked(){

    //     emit bottomTrackingClicked(usermenu_ui->enableBottomTracking->isChecked());
    //    if(usermenu_ui->enableBottomTracking->isChecked()){

    //    }
    emit showForwardRange(usermenu_ui->displayForwardRange->isChecked());
}

void userMenu :: on_GapBox_clicked(){
    emit showGap(usermenu_ui->GapBox->isChecked());
    gapBox = usermenu_ui->GapBox->isChecked();

}

void userMenu :: showGapClickable(bool show){
    if(show){
        usermenu_ui->GapBox->setEnabled(true);
        usermenu_ui->GapBox->setStyleSheet("color:white");
        usermenu_ui->altimeter->setEnabled(true);
        usermenu_ui->altimeter->setStyleSheet("color:white");


    }else{

        //        ui->HFBox->setEnabled(false);
        usermenu_ui->GapBox->setEnabled(false);
        usermenu_ui->GapBox->setStyleSheet("color:grey");
        usermenu_ui->altimeter->setEnabled(false);
        usermenu_ui->altimeter->setStyleSheet("color:grey");
    }
}

void userMenu :: untiePlots(bool val){

    if(!myUserDb.open())
        saveZoomClicks();
}

void userMenu :: uncheckShowForwardRng(bool val){

    usermenu_ui->displayForwardRange->setChecked(val);
    saveAltimeterSettings();

}

void userMenu :: saveAltimeterSettings(){
    connOpenUser();

    if(QSqlDatabase::contains("userConnection")) {

        myUserDb = QSqlDatabase::database("userConnection");
        QSqlQuery userqry(myUserDb);
        userqry.prepare(("DROP TABLE IF EXISTS AltimeterSettings"));
        userqry.exec();
        if (!userqry.exec()) {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        }
        userqry.prepare("CREATE TABLE IF NOT EXISTS AltimeterSettings (DepthLabel INTEGER, TractionStrip INTEGER, TransparencyPercentage INTEGER,"
                        "DepthRange INTEGER, Gradient INTEGER, GradientPixel INTEGER, bottomTracking INTEGER, showGap INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << "userqry.lastError()" << userqry.lastError();
        }

        userqry.prepare("INSERT INTO AltimeterSettings(DepthLabel,TractionStrip,TransparencyPercentage,DepthRange,Gradient,GradientPixel, bottomTracking, showGap)"
                        " values(:DepthLabel, :TractionStrip, :TransparencyPercentage, :DepthRange, :Gradient, :GradientPixel, :bottomTracking, :showGap)");

        gapBox = usermenu_ui->GapBox->isChecked();
        userqry.bindValue(":DepthLabel", usermenu_ui->dLabelBtn->isChecked());
        userqry.bindValue(":TractionStrip", usermenu_ui->tStripBtn->isChecked());
        userqry.bindValue(":TransparencyPercentage", usermenu_ui->transparencySlider->value());
        userqry.bindValue(":DepthRange", usermenu_ui->globalDepthBox->currentIndex());
        userqry.bindValue(":Gradient", tempGradient);
        userqry.bindValue(":GradientPixel", usermenu_ui->pixelSlider->value());
        userqry.bindValue(":bottomTracking", usermenu_ui->displayForwardRange->isChecked());
        userqry.bindValue(":showGap", int(gapBox));


        userqry.exec();

        userqry.prepare("INSERT INTO AltimeterSettings(DepthLabel,TractionStrip,TransparencyPercentage,DepthRange,Gradient,GradientPixel, bottomTracking, showGap)"
                        " values(:DepthLabel, :TractionStrip, :TransparencyPercentage, :DepthRange, :Gradient, :GradientPixel, :bottomTracking, :showGap)");
        userqry.bindValue(":DepthLabel",1);
        userqry.bindValue(":TractionStrip",1);
        userqry.bindValue(":TransparencyPercentage",50);
        userqry.bindValue(":DepthRange",2);
        userqry.bindValue(":Gradient", 1);
        userqry.bindValue(":GradientPixel", 500);
        userqry.bindValue(":bottomTracking", 1);
        userqry.bindValue(":showGap", 0);

        if(userqry.exec()){
            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));
            qDebug() << "data Saved";
        } else {
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";

        }

    }

    connCloseUser();//closing database connection after saving


}
