#include "helpdialog.h"
#include "ui_helpdialog.h"
#include "mainwindow.h"

helpDialog::helpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::helpDialog)
{
    ui->setupUi(this);


}

helpDialog::~helpDialog()
{
    delete ui;
}

void helpDialog :: createHelpWindow(){

    QHelpEngine *helpEngine = new QHelpEngine(
                QApplication::applicationDirPath() +
                "/pathway/HTML Help (CHM).qch");
    helpEngine->setupData();
    QTabWidget* tWidget = new QTabWidget;
    tWidget->addTab(helpEngine->contentWidget(), "Contents");
    tWidget->addTab(helpEngine->indexWidget(), "Index");

    qDebug()<<tWidget->size();
    //tWidget->setMinimumSize()

    helpbrowser *textViewer = new helpbrowser(helpEngine);
//    textViewer->setSource(
//                QUrl("qthelp://walletfox.qt.helpexample/doc/index.html"));
    connect(helpEngine->contentWidget(),
            SIGNAL(linkActivated(QUrl)),
            textViewer, SLOT(setSource(QUrl)));

    connect(helpEngine->indexWidget(),
            SIGNAL(linkActivated(QUrl, QString)),
            textViewer, SLOT(setSource(QUrl)));

    QSplitter *horizSplitter = new QSplitter(this);
    horizSplitter->insertWidget(0, tWidget);
    horizSplitter->insertWidget(1, textViewer);

    ui->dockWidget->setWidget(horizSplitter);


//    QWidget::nativeEvent()

}
