#include "createFlag.h"
#include "ui_createFlag.h"
#include "mainwindow.h"
#include <QDebug>
#include <QCloseEvent>

MainWindow* cMainWindow;

CreateFlag::CreateFlag(QWidget *parent):
    QDialog(parent),
    flag_ui(new Ui::CreateFlag)
{
    flag_ui->setupUi(this);
    cMainWindow = qobject_cast<MainWindow*>(this->parent());
//    this->move(QApplication::desktop()->width()/2,50);
    QColor white;
    white.setRgb(225, 225, 225);
    CreateFlag :: setWindowTitle("Target");
    this->setWindowFlag(Qt::WindowContextHelpButtonHint,false);
    CreateFlag::setWindowIcon(QIcon(":/icons/icons/pin_solid_red.png"));

//    flag_ui->label_2->setStyleSheet("color: white");
//    flag_ui->label_3->setStyleSheet("color: white");
//    flag_ui->label_4->setStyleSheet("color: white");
    flag_ui->label_5->hide();
    flag_ui->label_6->hide();
    flag_ui->line_ping->hide();
    flag_ui->line_sample->hide();

//    flag_ui->lineEdit->setStyleSheet("color: white");
//    flag_ui->lineEdit_2->setStyleSheet("color: white");
//    flag_ui->lineEdit_3->setStyleSheet("color: white");
//    flag_ui->lineEdit_4->setStyleSheet("color: white");
//    flag_ui->lineEdit_5->setStyleSheet("color: white");

    flag_ui->line_lattitude->setEnabled(false);
    flag_ui->line_longitude->setEnabled(false);
    flag_ui->line_ping->setEnabled(false);
    flag_ui->line_sample->setEnabled(false);
    flag_ui->line_area->setEnabled(false);
    flag_ui->line_heading->setEnabled(false);


    flag_ui->line_name->setText("Target");
    flag_ui->line_name->setFocus();
}

CreateFlag::~CreateFlag(){
    delete flag_ui;
}

void CreateFlag :: getParametersToFlag(targetItem *item, bool newItem) {
//    targetItem *item;
//    item = &cMainWindow->tools->targetList[row];
    if (item->importedWaypoint)
        this->setWindowTitle("Waypoint " + QString::number(item->listIndex+1) + " of "
                               + QString::number(cMainWindow->tools->waypointList.length()));
    else
        this->setWindowTitle("Target " + QString::number(item->listIndex+1) + " of "
                               + QString::number(cMainWindow->tools->targetList.length()));

    flag_ui->line_name->setText(item->Name);
    flag_ui->line_ping->setText(QString::number(item->ping));
    flag_ui->line_sample->setText(QString::number(item->sample));
    flag_ui->line_area->setText(QString::number(item->area, 'f', 1) + " m²");
    flag_ui->line_heading->setText(QString::number(item->heading, 'f', 1) + "º");

    QString Lattitude = QString::number(abs(item->Lattitude), 'd', 4);
    if (item->Lattitude > 0)   Lattitude.append(" N");
    else                      Lattitude.append(" S");

    QString Longitude = QString::number(abs(item->Longitude), 'd', 4);
    if (item->Longitude > 0)   Longitude.append(" E");
    else                      Longitude.append(" W");

    if (item->Lattitude < -400) {
        Lattitude = "N/A";
        Longitude = "N/A";
    }
    flag_ui->line_lattitude->setText(Lattitude);
    flag_ui->line_longitude->setText(Longitude);
    CurrentItem = item;
    NewItem = newItem;

    flag_ui->ok->setFocusPolicy(Qt::StrongFocus);
    flag_ui->line_name->selectAll();
    flag_ui->line_name->setFocus();
}

void CreateFlag :: on_ok_clicked(){
    //Only click ok button will save the flag
    //qDebug()<<"Ok";

    if (cMainWindow->tools->checkIfTargetNameExists(CurrentRow, flag_ui->line_name->text())) {
        QMessageBox msgBox;
        msgBox.critical(0,"Error","Target name is taken!");
        flag_ui->line_name->setStyleSheet("border : 2px solid orange;");
        flag_ui->line_name->setText(cMainWindow->tools->targetList[CurrentRow].Name);
        return;
    }

    flag_ui->line_name->setStyleSheet("");
    okButtonClicked=true;
    emit flagCreateOkButton(okButtonClicked);

    //the flag string information below will be sent to the mainwindow
    emit flagInfo(flag_ui->line_name->text(), CurrentItem, NewItem);
    this->hide();
}

void CreateFlag :: on_cancel_clicked(){
    //If click on cancel button, it will remove the flag if the flagh is a new item
    cancelButtonClicked = true;
    emit flagCreateCancelButton(cancelButtonClicked);
    emit cancelFlag(NewItem);
    this->hide();
    emit renameTargetImageFile();
}

void CreateFlag :: closeEvent(QCloseEvent *event){
    emit cancelFlag(NewItem);
   // qDebug()<<"Close";
}





