#ifndef USERMENU_H
#define USERMENU_H

#include <QDialog>
#include "qcustomplot.h"
#include<cmath>
#include<QtSql>
#include<QFileInfo>
#include "modules.h"

namespace Ui {
class userMenu;
}

class userMenu : public QDialog
{
    Q_OBJECT

public:

    explicit userMenu(QWidget *parent = 0);
    ~userMenu();

    void setUpWidgetsAndVisuals();
    GainSettings* gain;
    SonarPing *ping;
    QSqlDatabase configDB1,configDB2, myUserDb, defaultsDB, configSettingsDB /*formatSettings*/;
    QString configConnection,userConnection;
    QString userfilenameLinux = "userDB.db";
    QString fileName_config1 = "configuration_lf.db";
    QString fileName_config2 = "configuration_hf.db";

    //QString fSettings = "formatSettings.db";

    // QString fileName_usrDefaults = "usrDefault.db";
   // int wayPointBtnState = 1;

    bool connOpenUser(){
        myUserDb = QSqlDatabase::addDatabase("QSQLITE","userConnection");
        myUserDb.setDatabaseName("userDB.db");

        if(!myUserDb.open()){
            qDebug()<<("Could not Connect to Database");
            QMessageBox::critical(this,tr("Database Info"),tr("USER DATABASE NOT FOUND!!"));
            return false;
        }

        else {
            // qDebug()<<( "Connected to Database..... User Menu");
            return true;
        }

        myUserDb = QSqlDatabase::database("userConnection",true);
    }
    void connCloseUser() {
        QString con =myUserDb.connectionName();
        QSqlDatabase::removeDatabase(con);
        myUserDb.close();

    }




    //   bool connOpenDefault() {
    //       defaultsDB = QSqlDatabase::addDatabase("QSQLITE","default");
    //       defaultsDB.setDatabaseName(fileName_usrDefaults);

    //       if (!defaultsDB.open()) {
    //           qDebug()<<("Could not connect to Default Database");
    //           QMessageBox::critical(this,tr("Database Info"),tr("DEFAULT DATABASE NOT FOUND!!"));
    //           return false;
    //       }
    //       else {
    //           qDebug() << "Connected to Default settings Database";
    //           return true;
    //       }
    //   }

    void connCloseDefault() {
        QString con = defaultsDB.connectionName();
        defaultsDB.close();
        QSqlDatabase::removeDatabase(con);
    }
    bool  connOpen() {
        configDB1 = QSqlDatabase::addDatabase("QSQLITE","config1");
        configDB1.setDatabaseName(fileName_config1);

        if(!configDB1.open()){
            qDebug()<<("Couldnot Connect to Database");
            QMessageBox::critical(this,tr("Database Info"),tr("CONFIG1 DATABASE NOT FOUND!!"));
            return false;
        }
        else {
            qDebug()<<( "Connected to Database..... Config DB 1");
        }

        //configuration 2
        configDB2 = QSqlDatabase::addDatabase("QSQLITE","config2");
        configDB2.setDatabaseName(fileName_config2);

        if(!configDB2.open()){
            qDebug()<<("Couldnot Connect to Database");
            QMessageBox::critical(this,tr("Database Info"),tr("CONFIG2 DATABASE NOT FOUND!!"));
            return false;
        }
        else {
            qDebug()<<( "Connected to Database..... Config DB 2");
            return true;
        }
    }

    void connClose(){
        configDB1.close();
        configDB2.close();
        QSqlDatabase::removeDatabase("config1");
        QSqlDatabase::removeDatabase("config2");

    }

    //   bool configOpen() {
    //    configSettingsDB = QSqlDatabase::addDatabase("QSQLITE", "userConfigConnection");
    //   }
    double attnuation;
    double noiseThreshold;
    QString customPal;
    QString recordPath, openPath;
    QList<double>Bvals;
    QString bval1,bval2,bval3,bval4,bval5,bval6,bval7,bval8,bval9,bval10,bval11,bval12,bval13,bval14;
    QList <QDoubleSpinBox*> Bvalue_list;
    QList<double> Btemp;
    QCPColorGradient Custom1;
    int swathValue;//new
    int invert = 0;
    int hfBalanceIndex = 0;
    int lfBalanceIndex = 0;
    QString depthPal;
    int resLabel = 7;
    int gainAdd;
    int filterSize;
    double dGainUpperLF = 8191;
    double dGainLowerLF = 0;
    double dGainUpperHF = 8191;
    double dGainLowerHF = 0;
    QString viewFactor;
    bool track_port = 1, track_starboard = 1;
    int lfB = 1, hfB = 0;
    bool inverted = false, hide_color_scale=false;
    bool signalplot = true;
    double cl, ss;
    QString cableLength, speedofSound;
    int compGPSValue = 0;
    //Compass tab
    bool calibrateRestart = false;
    bool calibrateActive = false;
//    int A_LfPortInd, T_LfPort, F_Lf = 0;
//    int A_LfStbdInd, T_LfStbd;
//    int A_HfPortInd, T_HfPort, F_Hf = 0;
//    int A_HfStbdInd, T_HfStbd;
//    double C_LfPort, C_LfStbd, C_HfPort, C_HfStbd;
    int r_val,max_range=0,max_range_HF=0;

    bool towfishEnabled = 1, globalEnabled = 1, avEnabled = 1;
    int lineLength = 1, threshValue = 10, listSizeValue = 10;
    bool alti;
    bool readDepthLabel,readTractionStrip,dDepthLabel, dTractionStrip, dVerticalZoom;
    int readTransparencyPercentage,readDepthRange,readGradient,readPixel,dPixel,dGradient,dTransparencyPercentage,dDepthRange;
    int transparencySliderValue,tempGradient=1;
    int numberOfPings=0;
    int wayPointButtonState = 0, targetButton;
    int zoomButtonState = 0;

    bool verticalShiftChanged = false;
    double zoomStepValue = 0;

    bool indexChanged = false;
    int number=0;
    bool displayForwardRange;

    int zoomLf_index = 0, zoomHf_index = 0;

    bool emptyFieldWarning(QLineEdit *lineItem);
    bool emptyFieldWarning(QComboBox *lineItem);
    void setLineIntValidator(QLineEdit *lineItem, int minimum, int maximum);
    void setLineDoubleValidator(QLineEdit *lineItem, double minimum, double maximum);
    void saveUserData();
    void saveZoomClicks();
    void saveAltimeterSettings();
    void closeEvent(QCloseEvent *e);
    void setUpComboBox();
    void saveGains();
    void resetDefaults();
    void readUserSettings();
    void choosePalette(QString paletteVal, QSlider *slider, bool invert);
    QCPColorGradient showPalette(QString customPalette, bool invert);

    void setGradientChecked(int value);
//    void setZoomOnPlot(int state);
    QString verifyFilePath(QString filePath);
    void setTargetIcon();

    void setCustomLfZoom();
    void setCustomHfZoom();
    void resetLfZoom();
    void resetHfZoom();
    void setAltitudeSetting();
    void setProjectSetting();
    bool enableGap(bool state);

    double zoomValueLF=0, zoomValueHF=0, zoomValueDepthSense=0;
    bool gapBox;
public slots:
    void resetGainDefaults();
    void saveGainSettings();

    void uncheckShowForwardRng(bool val);
    void untiePlots(bool val);
    void mainPalletteChose(QString paletteVal);
    void depthPaletteChoose(QString paletteVal);
    void setCustomPalette(QString customPalette);
    void setDepthPalette(QString customPalette);
    void setBValue(QList<double> customBValues);

    //Compass tab
    void on_beginCalibration_clicked();
    void on_nextBtn_clicked();
    void on_rBtn_clicked();
    void receiveMessage(QString message);
    void get_Dynamic_range(int maxRange, int maxRange_HF);

    //altimeter tab
    void on_dLabelBtn_clicked();
    void on_tStripBtn_clicked();

    void on_gradCheckBox1_clicked();
    void on_gradCheckBox2_clicked();
    void on_gradCheckBox3_clicked();
    void on_gradCheckBox4_clicked();
    void on_gradCheckBox5_clicked();

    void clickCounter(int countLF, int countHF, int countDepthSensor);

    void on_displayForwardRange_clicked();

    void SaveIniFile (QString filePath);
    void ReadIniFile (QString filePath);

signals:
    void updateTvgWindow();

    void resetVerticalShift();
    void showGap(bool show);
    void showForwardRange(bool val);
    void numberOfPingsToPlot(int numberOfPings);
    void resetZoomOnAllPlots(bool val);
    void depthLabelHide(bool);
    void tractionStripHide(bool);
    //void setTransparency(int);
    void setTransparency(int, int);
    void setAltGradient(int);
   // void receiveGradients(int);
    void receiveDepthRange(int);


    void send_default_range(int range,int range_HF);
    void sendLogGainA(double newAttenuation);
    void sendCustomPalette(QString newPalette);
    void sendBValues(QList<double> newBValues);
    void emitOnExit();
    void emitCustomGradient(QCPColorGradient custom1);
    void updateCL(double length);
    void updateStatusFreq(bool mainFreq);
    void updateSS(double speed);
    void toggleTVG(bool tvg);
    void gradInvert(bool inverted);
    void hideColorScale(bool hide_color_scale);
    void sendHFGains(double,double,int);
    void sendLFGains(double,double,int);
    void sendColorMaxMin(int,int);
    void sendColorMax(int);
    void sendRecDir(QString);
    void emitDepthGradient(QCPColorGradient custom);
    void setResolutionLabel(int i);
    void sendGainAdd(int);
    void sendNThresh(double);
    void sendFSize(int);
    //    void changeRecPath(QString data_filepath);
    void GainsToMainwindow(bool tvg,  QString unit, int Gain, int Balance, int Gain_HF);
    void AvaltoMainwindow(double Aval);
    void KvaltoMainwindow(double Kval);
    void BvaltoMainwindow(double Bval);
    //void lfChanged(bool);
    //void hfChanged(bool);
    void setToLF(bool);
    //void gpsClicked(bool);
    void compassClicked(bool);
    void signalToMain(bool, bool, bool);
    void ethernetActive(bool);
    void sendBoatSpeed(int i);

    void sendDepthRange(int i);

    //Towfish control tab signals
    void sendTowfishParameters(bool, bool, int, bool, int, int);

    //Compass tab
    void sendCompass(int);
    void sendDateData(int);

    //altimeter tab

    // Player Tab
    void displayWayPoint(int);
    void verticalShiftPercentage(int num);
    void updateAlarmSettings(void);


private slots:

    void showGapClickable(bool show);
    void on_GapBox_clicked();

    void plotLogGraph();
    void on_acceptButton_clicked();
    void on_saveButton_clicked();

    //Tow control tab slots
    void on_enable_btn_clicked();
    void on_disable_btn_clicked();
    void on_small_btn_clicked();
    void on_med_btn_clicked();
    void on_large_btn_clicked();
    void on_av_enable_btn_clicked();
    void on_av_disable_btn_clicked();
    void on_global_btn_clicked();
    void on_relative_btn_clicked();
    void receiveThreshValue(int i);
    void on_transparencySlider_valueChanged(int value);
    void on_pixelSlider_valueChanged(int value);

    void receiveListSize(int i);
    void on_numberOfPings_currentIndexChanged();

    void on_browseFile_btn_clicked();
    void on_browseSave_btn_clicked();

    void on_low_freq_radio_btn_clicked();

    void on_high_freq_radio_btn_clicked();

    void on_gps_btn_clicked();

    void on_compass_btn_clicked();

    void updateDirPath(QString path);

    void changeSoundSpeed(double ss);

    void changeCableLength(double cl);

    void on_invertBox_clicked();
    void on_invertBoxDepth_clicked();

    void on_colorScaleBox_clicked();

    void setLFDGain(double upper, double lower);

    void setLFBalance(int value);

    void setHFDGain(double upper, double lower);

    void setHFBalance(int value);

    void on_resetSettings_btn_clicked();

    //    void on_C_value_textChanged();

    //    void on_C_value_HF_textChanged();

    void on_dirPathBox_textChanged();

    void setGainAdd(int add);

    void setNoiseThreshold(double thresh);

    void setFSize(int size);

    //void on_lf_tvg_clicked();

    //void on_hf_tvg_clicked();

    void on_ethernetBtn_clicked();

    void on_USBBtn_clicked();

    void boatSpeedChanged(int i);

    void depthRangeChanged(int i);

    void receiveDisableBoatSpeed(bool b);

    void receiveSys(float x, float y);

    void checkCurrentTab(int i);
    void onNewTargetIconSelected();
    void updateZoomSettingLf(QString str);
    void updateZoomSettingHf(QString str);
    void on_btn_AltDepth_clicked();
    void on_btn_Altitude_clicked();
    void on_btn_project_clicked();

private:
    Ui::userMenu *usermenu_ui;
    QTimer updateTvgTimer;

};

#endif // USERMENU_H

