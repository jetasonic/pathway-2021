#ifndef SYSTEMSETTINGS_H
#define SYSTEMSETTINGS_H

#include <QDialog>
#include <QtSql>

namespace Ui {
class SystemSettings;
}

class SystemSettings : public QDialog
{
    Q_OBJECT

public:
    explicit SystemSettings(QWidget *parent = 0);
    ~SystemSettings();

    void setUpWidgetsAndVisuals();
    int returnSwitchingDepth(int currentRange, bool switchUp);
    void setSimulation(bool set);
    bool activateSkipSamples = 0;
    //bool calibrateRestart = false; //Compass tab
   // bool calibrateActive = false;  //Compass tab
    QSqlDatabase configData;
    bool changeDetected = false;
    QList<QString> tabChanges;
    //QString nx = NULL, ny = NULL, `

private slots:

    //void receiveSys(float x, float y);
//    void on_compressOn_clicked();
//    void on_compressOff_clicked();
    void on_resetBoard_clicked();
    void on_thresholdSelection_currentIndexChanged(int index);
    void on_useThreshold_btn_clicked();
    void on_saveButton_clicked();
    void displayThresholdValue(QString value);
    void on_boardSimulation_enable_clicked();
    void on_boardSimulation_disabled_clicked();
//    void on_rampUp_btn_clicked();
//    void on_rampDown_btn_clicked();
    void on_color_resolution_box_currentIndexChanged(int index);
    void on_afeReset_btn_clicked();
    void on_skipYes_clicked();
    void on_skipNo_clicked();
    void on_crossPt_avg_No_clicked();
    void on_crossPt_avg_yes_clicked();
    void on_commonBox_clicked();
    void on_att_slider_valueChanged(int value);
    void on_att_slider_sliderReleased();
    //void on_portSide_gainSlider_valueChanged(int value);
    //void on_starBoardSide_gainSlider_valueChanged(int value);
    //void on_portSide_gainSlider_sliderReleased();
    //void on_starBoardSide_gainSlider_sliderReleased();
    void on_gain_mult_slider_valueChanged(int value);
    void on_adc1_valueChanged(int adc1);
    void on_adc2_valueChanged(int adc2);
    void on_adc3_valueChanged(int adc3);
    void on_adc4_valueChanged(int adc4);
    void on_adc_valueChanged(int adc);
    void on_gain_mult_slider_sliderReleased();
    void on_add_gain_clicked();
    void on_sub_gain_clicked();
    void checkGain();
    void on_transOff_btn_clicked();
    void on_LFOn_btn_clicked();
    void on_HFOn_btn_clicked();
    void on_transOn_btn_clicked();
    void keyPressEvent(QKeyEvent * e);
    void on_compBox_valueChanged(int i);
    void on_depthBox_clicked();
    void on_filterRstBtn_clicked();
    void on_filterBox_clicked();
    void on_filterSizeBox_valueChanged(int value);
    void on_gainAddBox_valueChanged(int value);
    void changeNoiseThresh(double thresh);
    void defaultAngle(double);
    void noAngleControl();
    void openControls(bool);
   // void on_beginCalibration_clicked(); //Compass tab
   // void on_nextBtn_clicked();          //Compass tab
   // void on_rBtn_clicked();             //Compass tab
   // void receiveMessage(QString message); //Compass tab
    void on_closeBtn_clicked();
    void closeEvent(QCloseEvent *event);
    void on_resetConfigBtn_clicked();
    void on_resetReceiverBtn_clicked();
    void on_resetCompressionBtn_clicked();
    void on_resetFitlerBtn_clicked();
    void on_applyAllBtn_clicked();

    //Config change detection slots
    void configTabChangeDetected();
    void receiverTabChangeDetected();
    void compressionTabChangeDetected();
    void filterTabChangeDetected();

    //Config settings database value slots
    void startConfigDatabase();
    void resetConfigTab();
    void resetReceiverTab();
    void resetCompressionTab();
    void resetFilterTab();
    void retrieveConfigDatabaseValues();
    void closeConfigDatabase();

        //configTab
    void receiveSampleSkipFromDB(QString sampleSkip);
    void receiveSeaSurfDistFromDB(QString dist);
    void on_sim_activate_clicked();
    void on_sim_deactivate_clicked();
    void  send_alt_length_limit(int i);
    void  send_yaw_limit(int i);
        //receiveTab
    void receiveDemoForce(int v);
    void receiveScaleButton(QString s);
    void receiveCh1Button(QString s);
    void receiveCh2Button(QString s);
    void receiveCh3Button(QString s);
    void receiveCh4Button(QString s);
    void receiveCh1Slider(int n);
    void receiveCh2Slider(int n);
    void receiveCh3Slider(int n);
    void receiveCh4Slider(int n);
    void receiveCheckOnOff(QString s);
    void receiveCBtn(QString s);
    void receiveCSlider(int n);
    void receivePButton(int n);
    void receiveAtt(int n);
    void receiveTransm(QString s);
    void receiveDepSounder(QString s);
    void receiveGain(int n);
        //compressionTab
    void receiveListsFromTable(QList<int> range, QList<double> EPI, QList<int> comp);
        //fitlerTab
    void receiveFilterOff();
    void receiveFilterSizeFromDB(int i);
    void receive_default_range(int range, int range_HF);
    void on_ethernetBtn_clicked();

signals:

//    void compressOn();
//    void compressOff();
    void resetBoard();
    void changeThresholdMode(int index);
    void useThreshold(int num);
    void enableBoardSimul();
    void disableBoardSimul();
    void sendNStart(quint32 num);
    void sendNStop(quint32 num);
    void sendNStep(quint32 num);
//    void rampUp();
//    void rampDown();
    void setResolution(int index);
    void setAverageDepthLength(int index);
    void resetAFE();
    void expectPing30(double ping30);
    void expectPing60(double ping60);
    void expectPing120(double ping120);
    void expectPing180(double ping180);
    void expectPing240(double ping240);
    void expectPing360(double ping360);
    void expectPing480(double ping480);
    void compressionRate30(double compression30);
    void compressionRate60(double compression60);
    void compressionRate120(double compression120);
    void compressionRate180(double compression180);
    void compressionRate240(double compression240);
    void compressionRate360(double compression360);
    void compressionRate480(double compression480);
    void setSkipSamples(int skipSamples);
    void skipSampleMode(bool activateSkipSamples);
    void setAverageMode(bool activate);
    void setSonarPosition(double value);
    void attenuationVal(int index);
    void setPortSideGain(int gainCh1, int gainCh2);
    void setStarBoardSideGain(int gainCh1, int gainCh2);
    void gainMultVal(int index);
    void addGain();
    void subGain();
    void transConfig(int);
    void sendGainVal(int);
    void sendNoiseThresh(double);
    void changeCompression(int);
    void enableDepth(bool);
    void sendFilterEnabled(bool);
    void sendFilterSize(int);
    void sendResLabel(int);
    void setAddGain(int);
    void coarseGainToMain(int);
    void degreeChanged(double);
   // void sendCompass(int);  //Compass tab
   // void sendDateData(int); //Compass tab
    void sendEPIs(QVector<double> values);
    //signal for activating/deactivating simulation from the config window//
    void set_simulation(bool activate);
    void send_dynamic_range(int range, int range_HF);
   void ethernetActive(bool);
    //Config database
   void setEdgeAlign(bool);
   void send_alt_length(int i);
   void send_yaw_length(int i);

private:
    Ui::SystemSettings *ui;
};

#endif // SYSTEMSETTINGS_H
