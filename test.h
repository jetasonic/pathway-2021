#ifndef TEST_H
#define TEST_H
#include "qcustomplot.h"
#include <QDialog>

namespace Ui {
class Test;
}

class Test : public QDialog
{
    Q_OBJECT

public:
    explicit Test(QWidget *parent = 0);
    ~Test();
    void setUpPlottingArea();
    QCPColorScale *colorScale2;
    QCustomPlot* getCustomPlotHF();
    int colorScaleMin = 0;
    int colorScaleMax = 8191;


private:
    Ui::Test *ui_test;
};

#endif // TEST_H
