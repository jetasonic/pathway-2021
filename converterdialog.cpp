#include "converterdialog.h"
#include "ui_converterdialog.h"
#include<QWidget>
#include "QCloseEvent"
#include<QMessageBox>
#include "mainwindow.h"
MainWindow* uMainWindow1;
converterdialog::converterdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::converterdialog)
{
    ui->setupUi(this);

    converterDialogProgressBar(0);

    //initial start of GUI the button is close is browser button is not clicked
    button = 1;
    ui->converterButton->setText("Close");

    ui->fileSize->setText("");
    ui->fileNameLabel->setText("");


    ui->listWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->listWidget,SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(listWidgetCustomContextMenuRequested(QPoint)));

    //sets color of button grey
    ui->converterBrowseButton->setStyleSheet("background-color:#666666");
    ui->converterButton->setStyleSheet("background-color:#666666");
    ui->saveFileBrowser->setStyleSheet("background-color:#666666");
    ui->saveDirPath->setStyleSheet("background-color:#666666");
    ui->dirPathBox->setStyleSheet("background-color:#666666");

}

converterdialog::~converterdialog()
{
    delete ui;
}

//when the browser button is clicked
void converterdialog::on_converterBrowseButton_clicked(){

    //enables hover and QAction delete
    ui->listWidget->setDisabled(false);
    ui->listWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    //clear
    ui->listWidget->clear();
    filePathName.clear();
    xtfPathName.clear();
    saveFileList.clear();
    ui->fileNameLabel->setText("");
    ui->fileSize->setText("");
    //sets the row index of the QListWidget to 0 every time the browse button is clicked
    listWidgetIndex=0;

    int backSlashLastIndex;

    QString openPath;
    //QFileDialog::getOpenFileNames:- allows to select multiple files
    data_filePath = QFileDialog::getOpenFileNames(this,"Select the data file",openPath, "Jetasonic Data File (*.jss *.JAS)");

    //if no file is selected set the button to close
    if(data_filePath.isEmpty()){
        button = 1;
        ui->converterButton->setText("Close");
        return;
    }

    //gets the index of the last backslash from the file Path string
    backSlashLastIndex = data_filePath.at(0).lastIndexOf("/");

    //saves path as "C:/Users/Parth/Downloads/SidescanImagingFiles 16102020/" leaving out the file name
    browsePath = data_filePath.at(0).mid(0,(backSlashLastIndex+1));

    //sets the directory text
    ui->dirPathBox->setText(browsePath);
    ui->saveDirPath->setText(browsePath);

    //loops through the QStringList and stores the file paths in a QList.
    for(int i=0;i<data_filePath.size();i++){
        //if file path empty do nothing
        if(data_filePath.at(i).isNull()){
            button=1;
            return;
        }

        QString name =  data_filePath.at(i);
        QFileInfo fi1(data_filePath.at(i));
        QString data_fileName1 = fi1.filePath();
        QString extension = fi1.suffix();
        QFile dataFile(data_filePath.at(i));

        if (extension == "JAS")
            name.replace(".JAS", ".XTF");

        ui->progressBar->setValue(0);

        //set button to start when files are selected
        button=2;
        ui->converterButton->setText("Start");


        //append the file path and renamed xtf path into lists.
        filePathName.append(data_filePath.at(i));
        xtfPathName.append(name);
    }

    //adds file names onto the QListWidget
    for(int i=0;i<filePathName.size();i++){
        ui->listWidget->insertItem(i,filePathName.at(i).mid(filePathName.at(i).lastIndexOf("/")+1));
    }
}

//this function is a slot to when their is a right click on the QListWidget
void converterdialog :: listWidgetCustomContextMenuRequested(const QPoint &pos){

    QListWidgetItem *curItem = ui->listWidget->itemAt(pos);

    if(curItem == NULL)
        return;

    QMenu *popMenu = new QMenu(this);
    QAction *deleteFile = new QAction(tr("Delete"),this);

    popMenu->addAction(deleteFile);

    //a signal-slot to connect to the delete function
    connect(deleteFile,SIGNAL(triggered()),this,SLOT(deleteFileSlot()));

    popMenu->exec(QCursor::pos());

}




void converterdialog :: deleteFileSlot(){

    QString fileName;
    QListWidgetItem *item = ui->listWidget->currentItem();

    if(item == NULL)
        return;

    //gets the current index of the item
    int curIndex = ui->listWidget->row(item);

    //pops dialog
    int ch = QMessageBox::question(this, " "+item->text(),
                                   "Are you sure to delete this file?",
                                   QMessageBox::Yes|QMessageBox::No);
    if(ch!=QMessageBox::Yes)
        return;

    //removes the item from the QlistWidget as well as from the lists.
    ui->listWidget->takeItem(curIndex);
    filePathName.removeAt(curIndex);
    xtfPathName.removeAt(curIndex);

}


void converterdialog :: on_saveFileBrowser_clicked(){

    QString saveFilePath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                             //                                                          "C:/",
                                                             ui->saveDirPath->text(),//qApp->applicationDirPath(),
                                                             QFileDialog::ShowDirsOnly
                                                             | QFileDialog::DontResolveSymlinks);
    //if the no path is selected or cancel button is pressed
    //sets the path same as from where the file was selected
    if(saveFilePath.isEmpty()){
        saveFilePath=browsePath;
        return;
    }

    ui->saveDirPath->setText(saveFilePath);

    QString saveFileName;
    QString name;
    QString newFilePath;
    QString data_fileName1;
    QString extension;


    //loops through the list
    for(int i=0;i<filePathName.size();i++){
        //splits the file path string to get only the file name
        saveFileName=(filePathName.at(i).mid(data_filePath.at(i).lastIndexOf("/")));
        //concates the file name gotten previously with the directory path
        //stores the name into a new list
        newFilePath = saveFilePath + saveFileName;
        saveFileList.append(newFilePath);
    }

    //clear the list before appending new filenames
    xtfPathName.clear();

    for(int i=0;i<saveFileList.size();i++){

        name =  saveFileList.at(i);
        QFileInfo fi1(saveFileList.at(i));
        data_fileName1 = fi1.filePath();
        extension = fi1.suffix();
        QFile dataFile(saveFileList.at(i));
        if (extension == "JAS")
            name.replace(".JAS", ".XTF");

        //the list now has new xtf file paths with new directory
        xtfPathName.append(name);
    }
}


void converterdialog :: converterDialogProgressBar(double val){

    if(val>=ui->progressBar->maximum()){
        ui->progressBar->setValue(ui->progressBar->maximum());
    }else{
        ui->progressBar->setValue(val);
    }

}


void converterdialog :: converterFileName(QString name){

    //int i=0;
    filePath = name.mid(name.lastIndexOf("/")+1);
    QFileInfo fi(name);
    QString xtfPath;
    if(fi.suffix()=="JAS"){
        xtfPath = name.replace(".JAS", ".XTF");
    }

//    ui->listWidget->setCurrentRow(i);
//    ui->listWidget->takeItem(i);
    ui->listWidget->item(listWidgetIndex)->setForeground(Qt::red);
//    ui->listWidget->insertItem(i,filePath);

//    i++;
    ui->fileNameLabel->setText(filePath);


}

void converterdialog :: progressBarRange(int fileSize){
    ui->progressBar->setRange(0, fileSize-4);
}



void converterdialog::on_converterButton_clicked(){
    //if close button is clicked reset everything
    if(button==1){
        this->hide();
        ui->progressBar->setValue(0);
        ui->fileSize->setText("");
        ui->fileNameLabel->setText("");
        ui->saveDirPath->setText("");
        ui->dirPathBox->setText("");
        ui->listWidget->clear();
        ui->converterButton->setText("Close");
    }

    //if start button is clicked
    if(button==2){
        //disables the browse buttons
        ui->saveFileBrowser->setEnabled(false);
        ui->converterBrowseButton->setEnabled(false);

        //disables the delete action on the qlistwidget when converting
        ui->listWidget->setContextMenuPolicy(Qt::NoContextMenu);
        //purpose is to disables mouse hover action when converting and set white font color
        ui->listWidget->setDisabled(true);
        ui->listWidget->setStyleSheet("color: white;");

        emit convertFileCounter(filePathName.size());
        emit changeDataFilePath(filePathName,xtfPathName);

        emit converterStartBtnClicked(true);

        //if angle scan file detected change button to close
        if(isAngleScanFile == true){
            button=1;
            ui->converterButton->setText("Close");
            isAngleScanFile=false;
            return;
        }
        //while converting change button to cancel
        else{
            button=3;
            ui->converterButton->setText("Cancel");
            return;
        }
        //when done change it to close
        button=1;
        ui->converterButton->setText("close");
    }

    //if cancel button is clicked
    if(button==3){
        ui->converterButton->setText("Cancel");
        //enable the browse buttons
        ui->saveFileBrowser->setEnabled(true);
        ui->converterBrowseButton->setEnabled(true);

        emit converterCancelBtnClicked(true);

        //change button to close
        button=1;
        ui->converterButton->setText("Close");
        return;
    }
}

void converterdialog :: isConversionOver(bool show){
    //if all files finished converting
    if(show){
        button = 1;
        ui->converterButton->setText("Done");
        //enable browse buttons
        ui->saveFileBrowser->setEnabled(true);
        ui->converterBrowseButton->setEnabled(true);
        //removes the name from qlist widget and replaces it with checkmark file name
        ui->listWidget->setCurrentRow(listWidgetIndex);
        ui->listWidget->takeItem(listWidgetIndex);
        ui->listWidget->insertItem(listWidgetIndex,filePath+" ✓");
        //increment index to go to next row
        listWidgetIndex++;

    }
    //if there are files to convert next
    else{
        button = 3;
        ui->converterButton->setText("Cancel");
        //removes the name from qlist widget and replaces it with checkmark file name
        ui->listWidget->setCurrentRow(listWidgetIndex);
        ui->listWidget->takeItem(listWidgetIndex);
        ui->listWidget->insertItem(listWidgetIndex,filePath+" ✓");
        //increment index to go to next row
        listWidgetIndex++;
    }
}

void converterdialog :: angleFileDetected(bool show){
    //sets flag to true
    //used for the button state
    if(show){
        isAngleScanFile = true;
    }
}

void converterdialog :: closeEvent(QCloseEvent *event) {

    this->hide();

}

void converterdialog :: updateConvertedFileSize(int num){

    double fSize = double(num)/1000000;
    ui->fileSize->setText(QString::number(fSize, 'd', 0) +" MB");
}


