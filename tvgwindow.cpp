#include "tvgwindow.h"
#include "ui_tvgwindow.h"
#include "modules.h"
#include "mainwindow.h"

TvgWindow::TvgWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TvgWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window| /*Qt::WindowContextHelpButtonHint|*/ Qt::WindowCloseButtonHint | Qt::MSWindowsFixedSizeDialogHint);

    setLineDoubleValidator(ui->C_LfPort, -100, 100);
//    setLineIntValidator(ui->T_LfPort, -100, 100);
//    setLineIntValidator(ui->F_LfPort, 10, 1000);

    setLineDoubleValidator(ui->C_LfStbd, -100, 100);
//    setLineIntValidator(ui->T_LfStbd, -100, 100);
//    setLineIntValidator(ui->F_LfStbd, 10, 1000);

    setLineDoubleValidator(ui->C_HfPort, -100, 100);
//    setLineIntValidator(ui->T_HfPort, -100, 100);
//    setLineIntValidator(ui->F_HfPort, 10, 1000);

    setLineDoubleValidator(ui->C_HfStbd, -100, 100);
//    setLineIntValidator(ui->T_HfStbd, -100, 100);
//    setLineIntValidator(ui->F_HfStbd, 10, 1000);

    connect(ui->A_LfPort, SIGNAL(activated(QString)), this, SLOT(updateAValueLineLF(QString)));
//    connect(ui->B_LfPort, SIGNAL(textEdited(QString)), this, SLOT(updateTvgSettingsOnMainWindow()));
//    connect(ui->C_LfPort, SIGNAL(textEdited(QString)), this, SLOT(updateCValueLineLF(QString)));
//    connect(ui->F_LfPort, SIGNAL(textEdited(QString)), this, SLOT(updateFreqLfLine(QString)));
//    connect(ui->T_LfPort, SIGNAL(textEdited(QString)), this, SLOT(updateTemperatureLine(QString)));

    connect(ui->A_LfStbd, SIGNAL(activated(QString)), this, SLOT(updateAValueLineLF(QString)));
//    connect(ui->B_LfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateTvgSettingsOnMainWindow()));
//    connect(ui->C_LfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateCValueLineLF(QString)));
//    connect(ui->F_LfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateFreqLfLine(QString)));
//    connect(ui->T_LfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateTemperatureLine(QString)));

    connect(ui->A_HfPort, SIGNAL(activated(QString)), this, SLOT(updateAValueLineHF(QString)));
//    connect(ui->B_HfPort, SIGNAL(textEdited(QString)), this, SLOT(updateTvgSettingsOnMainWindow()));
//    connect(ui->C_HfPort, SIGNAL(textEdited(QString)), this, SLOT(updateCValueLineHF(QString)));
//    connect(ui->F_HfPort, SIGNAL(textEdited(QString)), this, SLOT(updateFreqHfLine(QString)));
//    connect(ui->T_HfPort, SIGNAL(textEdited(QString)), this, SLOT(updateTemperatureLine(QString)));

    connect(ui->A_HfStbd, SIGNAL(activated(QString)), this, SLOT(updateAValueLineHF(QString)));
//    connect(ui->B_HfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateTvgSettingsOnMainWindow()));
//    connect(ui->C_HfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateCValueLineHF(QString)));
//    connect(ui->F_HfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateFreqHfLine(QString)));
//    connect(ui->T_HfStbd, SIGNAL(textEdited(QString)), this, SLOT(updateTemperatureLine(QString)));

    ui->B_LfPort->setEnabled(false);
    ui->B_LfStbd->setEnabled(false);
    ui->B_HfPort->setEnabled(false);
    ui->B_HfStbd->setEnabled(false);
    ui->C_LfPort->setEnabled(false);
    ui->C_LfStbd->setEnabled(false);
    ui->C_HfPort->setEnabled(false);
    ui->C_HfStbd->setEnabled(false);

    ui->plot_tvgCurve->yAxis->setRange(-100, 100);
    ui->plot_tvgCurve->setInteractions(QCP::iSelectItems);//QCP::iRangeDrag|QCP::iRangeZoom);
    ui->plot_tvgCurve->xAxis->setRange(0, 1000);
    ui->plot_tvgCurve->addGraph(ui->plot_tvgCurve->xAxis, ui->plot_tvgCurve->yAxis);
    ui->plot_tvgCurve->addGraph(ui->plot_tvgCurve->xAxis, ui->plot_tvgCurve->yAxis);
    QPen redPen;
    redPen.setColor(QColor(150,0,0));
    redPen.setWidth(3);
    QPen greenPen;
    greenPen.setColor(QColor(0,150,0));
    greenPen.setWidth(3);
    ui->plot_tvgCurve->graph(0)->setPen(redPen);
    ui->plot_tvgCurve->graph(0)->setName("Port");
    ui->plot_tvgCurve->graph(1)->setPen(greenPen);
    ui->plot_tvgCurve->graph(1)->setName("Starboard");
    ui->plot_tvgCurve->legend->setVisible(true);
    ui->plot_tvgCurve->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop);
    ui->plot_tvgCurve->xAxis->setLabel("Range (m)");
    ui->plot_tvgCurve->yAxis->setLabel("Gain (dB)");
    QColor grey;
    grey.setRgb(230, 230, 230,255);//(50, 50, 50,255);
    QColor white;
    white.setRgb(255, 255, 255);
    ui->plot_tvgCurve->setBackground(QBrush(grey));
    QFont secondSize;
    secondSize.setPointSize(10);
    QPen linePen1;
    linePen1.setColor(Qt::black);
    linePen1.setWidth(1);
    linePen1.setStyle(Qt::DashLine);
    QPen linePen2 = linePen1;
    linePen2.setColor(Qt::blue);

    itemLineTvgMax = new QCPItemLine(ui->plot_tvgCurve);
    itemLineTvgMax->setPen(linePen1);
    itemLineTvgMax->setSelectedPen(linePen2);
    itemLineTvgMax->start->setCoords(0, 10);
    itemLineTvgMax->end->setCoords(10, 0);

    itemLineTvgMin = new QCPItemLine(ui->plot_tvgCurve);
    itemLineTvgMin->setPen(linePen1);
    itemLineTvgMin->setSelectedPen(linePen2);
    itemLineTvgMin->start->setCoords(0, 10);
    itemLineTvgMin->end->setCoords(10, 0);
//    itemLineTvgMin->setVisible(false);

    // Port B and C Handles
    itemBHandle = new QCPItemEllipse(ui->plot_tvgCurve);
    itemBHandle->topLeft->setCoords(10,10);
    itemBHandle->bottomRight->setCoords(20,0);
    itemBHandle->setBrush(QBrush(QColor(Qt::black)));
    itemCHandle = new QCPItemEllipse(ui->plot_tvgCurve);
    itemCHandle->topLeft->setCoords(10,10);
    itemCHandle->bottomRight->setCoords(20,0);
    itemCHandle->setBrush(QBrush(QColor(Qt::black)));

    itemLabelCurveMax = new QCPItemText(ui->plot_tvgCurve);
    itemLabelCurveMax->setColor(Qt::black);
    itemLabelCurveMax->setBrush(QBrush(Qt::NoBrush));
    itemLabelCurveMax->setPositionAlignment(Qt::AlignRight | Qt::AlignBottom);
    itemLabelCurveMax->setSelectedBrush(QBrush(Qt::NoBrush));
    itemLabelCurveMax->setFont(QFont(font().family(),9));
    itemLabelCurveMax->position->setCoords(15,15);
    itemLabelCurveMax->setSelectedColor(Qt::black);
    itemLabelCurveMax->setSelectable(false);
    itemLabelTvgMax = new QCPItemText(ui->plot_tvgCurve);
    itemLabelTvgMax->setColor(Qt::black);
    itemLabelTvgMax->setBrush(QBrush(Qt::NoBrush));
    itemLabelTvgMax->setPositionAlignment(Qt::AlignHCenter | Qt::AlignBottom);
    itemLabelTvgMax->setSelectedBrush(QBrush(Qt::NoBrush));
    itemLabelTvgMax->setFont(QFont(font().family(),9));
    itemLabelTvgMax->position->setCoords(15,15);
    itemLabelTvgMax->setSelectedColor(Qt::black);
    itemLabelTvgMax->setSelectable(false);
    itemLabelTvgMin = new QCPItemText(ui->plot_tvgCurve);
    itemLabelTvgMin->setColor(Qt::black);
    itemLabelTvgMin->setBrush(QBrush(Qt::NoBrush));
    itemLabelTvgMin->setPositionAlignment(Qt::AlignHCenter | Qt::AlignTop);
    itemLabelTvgMin->setSelectedBrush(QBrush(Qt::NoBrush));
    itemLabelTvgMin->setFont(QFont(font().family(),9));
    itemLabelTvgMin->position->setCoords(15,15);
    itemLabelTvgMin->setSelectedColor(Qt::black);
    itemLabelTvgMin->setSelectable(false);
//    itemLabelTvgMin->setVisible(false);
    connect(ui->plot_tvgCurve, &QCustomPlot::mousePress, this, &TvgWindow::onMouseClick);
    connect(ui->plot_tvgCurve, &QCustomPlot::mouseMove, this, &TvgWindow::onMouseMove);
    connect(ui->plot_tvgCurve, &QCustomPlot::mouseRelease, this, &TvgWindow::onMouseRelease);
    ui->groupBox_tvgLf->raise();
}

TvgWindow::~TvgWindow()
{
    delete ui;
}

void TvgWindow :: closeEvent(QCloseEvent *e) {
    this->hide();
    emit tvgWindowClosed();
}

void TvgWindow :: setLineIntValidator(QLineEdit *lineItem, int minimum, int maximum) {
    lineItem->setValidator(new QIntValidator(minimum, maximum, lineItem));
}
void TvgWindow :: setLineDoubleValidator(QLineEdit *lineItem, double minimum, double maximum) {
    lineItem->setValidator(new QDoubleValidator(minimum, maximum, 1, lineItem));
}

bool TvgWindow :: emptyFieldWarning(QLineEdit *lineItem) {
    // If the field is empty, warn the user by highlighting the LineEdit item with an orange border
    if(lineItem->text().isEmpty()){
        lineItem->setStyleSheet("QLineEdit{border : 2px solid orange;}");
        return 1;
    } else {
        lineItem->setStyleSheet("QLineEdit{border : 2px black;}");
        lineItem->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");
        return 0;
    }
}

void TvgWindow :: onMouseClick(QMouseEvent* event) {
    // Click on an item to set the drag mode
    QList<QCPAbstractItem*> itemList = ui->plot_tvgCurve->selectedItems();
    double x = ui->plot_tvgCurve->xAxis->pixelToCoord(event->pos().x());
    double y = ui->plot_tvgCurve->yAxis->pixelToCoord(event->pos().y());
    if (event->button()==Qt::LeftButton/* && itemList.length() > 0*/) {
        dragItemB = false;
        dragItemC = false;
        dragItemTvgMax = false;
        dragItemTvgMin = false;

        if (((x > itemBHandle->topLeft->coords().x() - 5) && (x < itemBHandle->bottomRight->coords().x() + 5))
                && ((y < itemBHandle->topLeft->coords().y() + 5) && (y > itemBHandle->bottomRight->coords().y() - 5))) {
            dragItemB = true;
        } else if (((x > itemCHandle->topLeft->coords().x() - 5) && (x < itemCHandle->bottomRight->coords().x() + 5))
                && ((y < itemCHandle->topLeft->coords().y() + 5) && (y > itemCHandle->bottomRight->coords().y() - 5))) {
            dragItemC = true;
        } else if (((y > itemLineTvgMin->start->coords().y() - 5) && (y < itemLineTvgMin->start->coords().y() + 5))) {
            dragItemTvgMin = true;
        } else if (((y > itemLineTvgMax->start->coords().y() - 5) && (y < itemLineTvgMax->start->coords().y() + 5))) {
            dragItemTvgMax = true;
        }
        itemLineTvgMin->setSelected(dragItemTvgMin);
        itemLineTvgMax->setSelected(dragItemTvgMax);
        itemCHandle->setSelected(dragItemC);
        itemBHandle->setSelected(dragItemB);

//        if (itemList.first() == itemBHandle)            dragItemB = true;
//        else if (itemList.first() == itemCHandle)       dragItemC = true;
//        else if (itemList.first() == itemLineTvgMax)             dragItemTvgMax = true;
//        else if (itemList.first() == itemLineTvgMin)             dragItemTvgMin = true;
//        else {
//            dragItemB = false;
//            dragItemC = false;
//        }
    } else {
        dragItemB = false;
        dragItemC = false;
    }
    if (!gain->tvg_on) {
        dragItemB = false;
        dragItemTvgMax = false;
        dragItemTvgMin = false;
    }
}

void TvgWindow :: onMouseMove(QMouseEvent* event) {
    // Drag selected item on plot
    QCustomPlot *plot = ui->plot_tvgCurve;
    if (dragItemC) {
        double x = (itemCHandle->topLeft->coords().x() + itemCHandle->bottomRight->coords().x())/2.0;//ui->plot_tvgCurve->xAxis->pixelToCoord(event->pos().x());
        double y = ui->plot_tvgCurve->yAxis->pixelToCoord(event->pos().y());
        double width = (plot->xAxis->range().upper - plot->xAxis->range().lower);
        double height = (plot->yAxis->range().upper - plot->yAxis->range().lower);
        double xInc =  10*width/(plot->width()*1.0);
        double yInc = 10*height/(plot->height()*1.0);

        itemCHandle->topLeft->setCoords(x-xInc, y+yInc);
        itemCHandle->bottomRight->setCoords(x+xInc, y-yInc);
        if (ui->btn_switchTvgFreq->isChecked())
            moveTvgCurve(GainSettings::HfPort);
        else
            moveTvgCurve(GainSettings::LfPort);
        ui->plot_tvgCurve->replot(QCustomPlot::rpQueuedReplot);
    }
    if (!gain->tvg_on)      return;
    // For each item, the y-coordinate of each item is changede to match the cursor position
    // The x-coordinate does not change
    if (dragItemB) {
        double x = (itemBHandle->topLeft->coords().x() + itemBHandle->bottomRight->coords().x())/2.0;//ui->plot_tvgCurve->xAxis->pixelToCoord(event->pos().x());
        double y = ui->plot_tvgCurve->yAxis->pixelToCoord(event->pos().y());
        double width = (plot->xAxis->range().upper - plot->xAxis->range().lower);
        double height = (plot->yAxis->range().upper - plot->yAxis->range().lower);
        double xInc =  10*width/(plot->width()*1.0);
        double yInc = 10*height/(plot->height()*1.0);

        itemBHandle->topLeft->setCoords(x-xInc, y+yInc);
        itemBHandle->bottomRight->setCoords(x+xInc, y-yInc);
        itemLabelCurveMax->position->setCoords(x+xInc, y+yInc);
        if (ui->btn_switchTvgFreq->isChecked())
            adjustTvgCurve(GainSettings::HfPort);
        else
            adjustTvgCurve(GainSettings::LfPort);
        ui->plot_tvgCurve->replot(QCustomPlot::rpQueuedReplot);
    }
    if (dragItemTvgMax) {
        double xStart = itemLineTvgMax->start->coords().x();
        double xEnd = itemLineTvgMax->end->coords().x();
        double y = round(ui->plot_tvgCurve->yAxis->pixelToCoord(event->pos().y()));

        if (ui->btn_switchTvgFreq->isChecked()) {
            if (y < gain->tvg_minHf)    y = gain->tvg_minHf;
        } else {
            if (y < gain->tvg_minLf)    y = gain->tvg_minLf;
        }

        itemLineTvgMax->start->setCoords(xStart, y);
        itemLineTvgMax->end->setCoords(xEnd, y);
        itemLabelTvgMax->position->setCoords(0.5*(xStart+xEnd),y);
        itemLabelTvgMax->setText(QString::number(y, 'f', 0));

        setNewTvg();
        GainSettings::channelType chType = GainSettings::LfPort;
        if (ui->btn_switchTvgFreq->isChecked()) {
            chType = GainSettings::HfPort;
            gain->tvg_maxHf = y;
        } else {
            gain->tvg_maxLf = y;
        }

        send_default_range(gain->tvg_maxLf, gain->tvg_maxHf);
        ui->plot_tvgCurve->replot(QCustomPlot::rpQueuedReplot);
        emit updateMainPlots(chType, gain_old, 4);
        setNewTvg();
    }
    if (dragItemTvgMin) {
        double xStart = itemLineTvgMin->start->coords().x();
        double xEnd = itemLineTvgMin->end->coords().x();
        double y = round(ui->plot_tvgCurve->yAxis->pixelToCoord(event->pos().y()));

        // Restrict Handle position to the bottom the the TVG curve and TVG max
        if (y < plot->graph(0)->data()->at(0)->value) {
            y = plot->graph(0)->data()->at(0)->value;
            updatedMinTvgHandle = false;
        } else {
            if (ui->btn_switchTvgFreq->isChecked()) {
                if (y > gain->tvg_maxHf)    y = gain->tvg_maxHf;
            } else {
                if (y > gain->tvg_maxLf)    y = gain->tvg_maxLf;
            }
            updatedMinTvgHandle = true;
        }

        itemLineTvgMin->start->setCoords(xStart, y);
        itemLineTvgMin->end->setCoords(xEnd, y);
        itemLabelTvgMin->position->setCoords(0.5*(xStart+xEnd),y);
        itemLabelTvgMin->setText(QString::number(y, 'f', 0));

        setNewTvg();
        GainSettings::channelType chType = GainSettings::LfPort;
        // Set constant variable to y position
        if (ui->btn_switchTvgFreq->isChecked())
            gain->tvg_minHf = y;
        else
            gain->tvg_minLf = y;
        ui->plot_tvgCurve->replot(QCustomPlot::rpQueuedReplot);
        emit updateMainPlots(chType, gain_old, 4);
        setNewTvg();
    }
}

void TvgWindow :: onMouseRelease(QMouseEvent* event) {
    // Turn off flags and redraw plot
    dragItemB = false;
    dragItemC = false;
    dragItemTvgMax = false;
    dragItemTvgMin = false;
    updateTvgCurve();
}

void TvgWindow :: plotTvgCurve() {
    ui->plot_tvgCurve->graph(0)->data()->clear();
    ui->plot_tvgCurve->graph(1)->data()->clear();
    double y = 0;
    int iMax = 0;
    if (ping->rangeLf_double > 0 && !ui->btn_switchTvgFreq->isChecked())
        iMax = ping->rangeLf_double*10;
    else if (ping->rangeHf_double > 0 && ui->btn_switchTvgFreq->isChecked())
        iMax = ping->rangeHf_double*10;
    else        iMax = 2000;

    if (ui->btn_switchTvgFreq->isChecked()) {
        // Draw the HF TVG curve
        for (int i = 10; i < iMax; i++) {
            double x = i*0.1;
            y = gain->calculateTvg(x, GainSettings::HfPort);
            ui->plot_tvgCurve->graph(0)->addData(x, y);
            y = gain->calculateTvg(x, GainSettings::HfStbd);
            ui->plot_tvgCurve->graph(1)->addData(x, y);
        }
    } else {
        // Draw the LF TVG curve
        for (int i = 10; i < iMax; i++) {
            double x = i*0.1;
            y = gain->calculateTvg(x, GainSettings::LfPort);
            ui->plot_tvgCurve->graph(0)->addData(x, y);
            y = gain->calculateTvg(x, GainSettings::LfStbd);
            ui->plot_tvgCurve->graph(1)->addData(x, y);
        }
    }
    bool found;
    itemLabelCurveMax->setText(QString::number(ui->plot_tvgCurve->graph(0)->data()->valueRange(found).upper, 'f', 1));
    if (ui->btn_switchTvgFreq->isChecked()) {
        itemLineTvgMax->start->setCoords(0, gain->tvg_maxHf);
        itemLineTvgMax->end->setCoords(iMax*0.1, gain->tvg_maxHf);
        itemLabelTvgMax->position->setCoords(0.05*iMax,gain->tvg_maxHf);
        itemLabelTvgMax->setText(QString::number(gain->tvg_maxHf));

        itemLineTvgMin->start->setCoords(0, gain->tvg_minHf);
        itemLineTvgMin->end->setCoords(iMax*0.1, gain->tvg_minHf);
        itemLabelTvgMin->position->setCoords(0.05*iMax,gain->tvg_minHf);
        itemLabelTvgMin->setText(QString::number(gain->tvg_minHf));
    } else {
        itemLineTvgMax->start->setCoords(0, gain->tvg_maxLf);
        itemLineTvgMax->end->setCoords(iMax*0.1, gain->tvg_maxLf);
        itemLabelTvgMax->position->setCoords(0.05*iMax,gain->tvg_maxLf);
        itemLabelTvgMax->setText(QString::number(gain->tvg_maxLf));

        itemLineTvgMin->start->setCoords(0, gain->tvg_minLf);
        itemLineTvgMin->end->setCoords(iMax*0.1, gain->tvg_minLf);
        itemLabelTvgMin->position->setCoords(0.05*iMax,gain->tvg_minLf);
        itemLabelTvgMin->setText(QString::number(gain->tvg_minLf));
    }
}

void TvgWindow :: updateHandlePositions(GainSettings::channelType chType, int handleType) {
    // Update all handles, except for the one given in handleType
    // handleType 1 is B, 2 is C, 0 is none
    QCustomPlot *plot = ui->plot_tvgCurve;
    double width = (plot->xAxis->range().upper - plot->xAxis->range().lower);
    double height = (plot->yAxis->range().upper - plot->yAxis->range().lower);
    double xInc =  10*width/(plot->width()*1.0);
    double yInc = 10*height/(plot->height()*1.0);

    bool found;
    int midIndex = plot->graph(0)->data()->size()/2;
    double xItem = plot->graph(0)->data()->keyRange(found).upper*0.97;
    double yItem = plot->graph(0)->data()->valueRange(found).upper;
    itemLabelCurveMax->position->setCoords(xItem+xInc, yItem+yInc);
    if (!((chType == GainSettings::LfPort || chType == GainSettings::HfPort) && handleType == 1)) {
        itemBHandle->topLeft->setCoords(xItem-xInc, yItem+yInc);
        itemBHandle->bottomRight->setCoords(xItem+xInc, yItem-yInc);
    }

    xItem = plot->graph(0)->data()->keyRange(found).upper*0.5;
    yItem = plot->graph(0)->data()->at(midIndex)->value;
    if (!((chType == GainSettings::LfPort || chType == GainSettings::HfPort) && handleType == 2)) {
        itemCHandle->topLeft->setCoords(xItem-xInc, yItem+yInc);
        itemCHandle->bottomRight->setCoords(xItem+xInc, yItem-yInc);
        if (!updatedMinTvgHandle) {
            if (ui->btn_switchTvgFreq->isChecked()) {
                itemLineTvgMin->start->setCoords(0, gain->tvg_minHf);
                itemLineTvgMin->end->setCoords(2*xItem, gain->tvg_minHf);
                itemLabelTvgMin->position->setCoords(xItem, gain->tvg_minHf);
                itemLabelTvgMin->setText(QString::number(gain->tvg_minHf));
            } else {
                itemLineTvgMin->start->setCoords(0, gain->tvg_minLf);
                itemLineTvgMin->end->setCoords(2*xItem, gain->tvg_minLf);
                itemLabelTvgMin->position->setCoords(xItem, gain->tvg_minLf);
                itemLabelTvgMin->setText(QString::number(gain->tvg_minLf));
            }
        }
    }
}

void TvgWindow :: moveTvgCurve(GainSettings::channelType chType) {
    // Move the curve up/down when C handle is dragged
    setNewTvg();
    double xItem = (itemCHandle->topLeft->coords().x() + itemCHandle->bottomRight->coords().x())/2.0;
    double yItem = (itemCHandle->topLeft->coords().y() + itemCHandle->bottomRight->coords().y())/2.0;

    // Find old C value at xItem position and update C
//    if (gain->tvg_on) {
        double gainAtX = gain->calculateTvg(xItem, chType);
        double CDiff = round(10*(yItem - gainAtX)/10.0);
        CDiff = yItem - gainAtX;
        if (chType == GainSettings::HfPort)
            gain->tvg_CHfP += CDiff;
        if ((chType == GainSettings::HfStbd) || (chType == GainSettings::HfPort/* && gain->tvg_syncOn*/))
            gain->tvg_CHfS += CDiff;
        if (chType == GainSettings::LfPort)
            gain->tvg_CLfP += CDiff;
        if ((chType == GainSettings::LfStbd) || (chType == GainSettings::LfPort/* && gain->tvg_syncOn*/))
            gain->tvg_CLfS += CDiff;
//    }
    ui->C_HfPort->setText(QString::number(gain->tvg_CHfP, 'f', 1));
    ui->C_HfStbd->setText(QString::number(gain->tvg_CHfS, 'f', 1));
    ui->C_LfPort->setText(QString::number(gain->tvg_CLfP, 'f', 1));
    ui->C_LfStbd->setText(QString::number(gain->tvg_CLfS, 'f', 1));

    if (!updatedMinTvgHandle) {
        gain->tvg_minLf = (gain->tvg_CLfP < gain->tvg_CLfS) ? gain->tvg_CLfP : gain->tvg_CLfS;
        gain->tvg_minHf = (gain->tvg_CHfP < gain->tvg_CHfS) ? gain->tvg_CHfP : gain->tvg_CHfS;
    }

    plotTvgCurve();
    updateHandlePositions(chType, 2);
    updateBValueLine();
    emit updateMainPlots(chType, gain_old, 3);
    setNewTvg();
}

void TvgWindow :: adjustTvgCurve(GainSettings::channelType chType) {
    // B handle dragged -> Adjust curve to fit with new B value
    setNewTvg();
    bool found;
    static bool constrainB = false;
    double xItem = (itemBHandle->topLeft->coords().x() + itemBHandle->bottomRight->coords().x())/2.0;
    double yItem = (itemBHandle->topLeft->coords().y() + itemBHandle->bottomRight->coords().y())/2.0;

    // Recalculate B
    if (chType == GainSettings::HfPort)
        gain->tvg_BHf = ((yItem - gain->tvg_AHfP*log(xItem) - gain->tvg_CHfP)/xItem);
    if ((chType == GainSettings::HfStbd) || (chType == GainSettings::HfPort && gain->tvg_syncOn))
        gain->tvg_BHf = ((yItem - gain->tvg_AHfS*log(xItem) - gain->tvg_CHfS)/xItem);
    if (chType == GainSettings::LfPort)
        gain->tvg_BLf = ((yItem - gain->tvg_ALfP*log(xItem) - gain->tvg_CLfP)/xItem);
    if ((chType == GainSettings::LfStbd) || (chType == GainSettings::LfPort && gain->tvg_syncOn))
        gain->tvg_BLf = ((yItem - gain->tvg_ALfS*log(xItem) - gain->tvg_CLfS)/xItem);
    if (ui->btn_switchTvgFreq->isChecked()) {
        if (gain->tvg_BHf < 0)  {
//            if (!constrainB)    QApplication::beep();
            gain->tvg_BHf = 0;
            constrainB = true;
        }
        else    constrainB = false;
        gain->calculateBHf = false;
    } else {
        if (gain->tvg_BLf < 0)  {
//            if (!constrainB)    QApplication::beep();
            gain->tvg_BLf = 0;
            constrainB = true;
        }
        else    constrainB = false;
        gain->calculateBLf = false;
    }
//    ui->B_HfPort->setText(QString::number(gain->tvg_BHf, 'f', 3));
//    ui->B_HfStbd->setText(QString::number(gain->tvg_BHf, 'f', 3));
//    ui->B_LfPort->setText(QString::number(gain->tvg_BLf, 'f', 3));
//    ui->B_LfStbd->setText(QString::number(gain->tvg_BLf, 'f', 3));

    plotTvgCurve();
    updateHandlePositions(chType, 1);
    updateBValueLine();
    emit updateMainPlots(chType, gain_old, 2);
    setNewTvg();
}

void TvgWindow :: updateTvgCurve() {
    // Redraw TVG curve with B ad C handles

    // Find the ratio of x to y to make the item a circle
    QCustomPlot *plot = ui->plot_tvgCurve;
//    double height = plot->height() * 1.0;
//    double width = plot->width() * 1.0;
    double xRange = plot->xAxis->range().upper - plot->xAxis->range().lower;
    double itemH = (plot->xAxis->range().upper - plot->xAxis->range().lower)
            * (plot->height() * 1.0) / (plot->width() * 1.0)
            * (10.0/(plot->height() * 1.0));
    double itemW = 10.0/(plot->width() * 1.0);

    plotTvgCurve();

    ui->plot_tvgCurve->yAxis->rescale();//->setRange(range_low,range_high);
    double range_low = plot->yAxis->range().lower;
    double range_high = plot->yAxis->range().upper;
    ui->plot_tvgCurve->yAxis->setRange(range_low-(range_high-range_low)*0.1, (range_high-range_low)*1.3+range_low);
    ui->plot_tvgCurve->xAxis->rescale();
    if (!gain->tvg_on)  ui->plot_tvgCurve->yAxis->setRange(ui->plot_tvgCurve->graph(0)->data()->at(0)->value - 40, ui->plot_tvgCurve->graph(0)->data()->at(0)->value + 40);
    ui->plot_tvgCurve->replot();

    updateHandlePositions(GainSettings::LfPort, 0);

    ui->plot_tvgCurve->replot();
    updateBValueLine();
}

void TvgWindow :: on_btn_switchTvgFreq_clicked() {
    if (ui->btn_switchTvgFreq->isChecked()) {
        ui->groupBox_tvgHf->raise();
        ui->btn_switchTvgFreq->setText("Show LF Settings");
    } else {
        ui->groupBox_tvgLf->raise();
        ui->btn_switchTvgFreq->setText("Show HF Settings");
    }
    updateTvgCurve();
}

void TvgWindow :: updateTvgVariables() {
    ui->tvgBox->setChecked(gain->tvg_on);
    ui->box_sync->setChecked(gain->tvg_syncOn);

    ui->A_LfPort->setCurrentIndex(gain->tvg_ALfPInd);
    ui->C_LfPort->setText(QString::number(gain->tvg_CLfP, 'f', 1));
//    ui->B_LfPort->setText(QString::number(gain->tvg_BLf, 'f', 3));
//    ui->T_LfPort->setText(QString::number(gain->tvg_T));
    gain->tvg_ALfP = ui->A_LfPort->currentText().toDouble();

    ui->A_LfStbd->setCurrentIndex(gain->tvg_ALfSInd);
    ui->C_LfStbd->setText(QString::number(gain->tvg_CLfS, 'f', 1));
//    ui->B_LfStbd->setText(QString::number(gain->tvg_BLf, 'f', 3));
//    ui->T_LfStbd->setText(QString::number(gain->tvg_T));
    gain->tvg_ALfS = ui->A_LfStbd->currentText().toDouble();

    ui->A_HfPort->setCurrentIndex(gain->tvg_AHfPInd);
    ui->C_HfPort->setText(QString::number(gain->tvg_CHfP, 'f', 1));
//    ui->B_HfPort->setText(QString::number(gain->tvg_BHf, 'f', 3));
//    ui->T_HfPort->setText(QString::number(gain->tvg_T));
    gain->tvg_AHfP = ui->A_HfPort->currentText().toDouble();

    ui->A_HfStbd->setCurrentIndex(gain->tvg_AHfSInd);
    ui->C_HfStbd->setText(QString::number(gain->tvg_CHfS, 'f', 1));
//    ui->B_HfStbd->setText(QString::number(gain->tvg_BHf, 'f', 3));
//    ui->T_HfStbd->setText(QString::number(gain->tvg_T));
    gain->tvg_AHfS = ui->A_HfStbd->currentText().toDouble();

    updateTvgCurve();
}

void TvgWindow :: updateTvgVariablesFromUserMenu() {
    ui->tvgBox->setChecked(gain->tvg_on);
    ui->box_sync->setChecked(gain->tvg_syncOn);

    ui->A_LfPort->setCurrentIndex(gain->tvg_ALfPInd);
    ui->C_LfPort->setText(QString::number(gain->tvg_CLfP, 'f', 1));
//    ui->B_LfPort->setText(QString::number(gain->tvg_BLf, 'f', 3));
//    ui->T_LfPort->setText(QString::number(gain->tvg_T));
    gain->tvg_ALfP = ui->A_LfPort->currentText().toDouble();

    ui->A_LfStbd->setCurrentIndex(gain->tvg_ALfSInd);
    ui->C_LfStbd->setText(QString::number(gain->tvg_CLfS, 'f', 1));
//    ui->B_LfStbd->setText(QString::number(gain->tvg_BLf, 'f', 3));
//    ui->T_LfStbd->setText(QString::number(gain->tvg_T));
    gain->tvg_ALfS = ui->A_LfStbd->currentText().toDouble();

    ui->A_HfPort->setCurrentIndex(gain->tvg_AHfPInd);
    ui->C_HfPort->setText(QString::number(gain->tvg_CHfP, 'f', 1));
//    ui->B_HfPort->setText(QString::number(gain->tvg_BHf, 'f', 3));
//    ui->T_HfPort->setText(QString::number(gain->tvg_T));
    gain->tvg_AHfP = ui->A_HfPort->currentText().toDouble();

    ui->A_HfStbd->setCurrentIndex(gain->tvg_AHfSInd);
    ui->C_HfStbd->setText(QString::number(gain->tvg_CHfS, 'f', 1));
//    ui->B_HfStbd->setText(QString::number(gain->tvg_BHf, 'f', 3));
//    ui->T_HfStbd->setText(QString::number(gain->tvg_T));
    gain->tvg_AHfS = ui->A_HfStbd->currentText().toDouble();

    updateTvgCurve();
    emit GainsToMainwindow(gain->tvg_on, "Meters",
                           gain->gainSliderIndexLf, gain->balanceSliderIndex,
                           gain->gainSliderIndexHf);
}

void TvgWindow :: on_tvgBox_clicked() {
    setNewTvg();
    gain->tvg_on = ui->tvgBox->isChecked();
    if (ui->tvgBox->isChecked()) {
        if (gain->calculateBLf)
            gain->tvg_BLf = 0.00017*gain->tvg_frequencyLow*gain->tvg_frequencyLow/(gain->tvg_T+18);
        ui->B_LfPort->setText(QString::number(gain->tvg_BLf,'f',3));
        ui->B_LfStbd->setText(QString::number(gain->tvg_BLf,'f',3));

        if (gain->calculateBHf)
            gain->tvg_BHf = 0.00017*gain->tvg_frequencyHigh*gain->tvg_frequencyHigh/(gain->tvg_T+18);
        ui->B_HfPort->setText(QString::number(gain->tvg_BHf,'f',3));
        ui->B_HfStbd->setText(QString::number(gain->tvg_BHf,'f',3));
    }

    ui->A_LfPort->setEnabled(gain->tvg_on);
//    ui->C_LfPort->setEnabled(gain->tvg_on);
//    ui->T_LfPort->setEnabled(gain->tvg_on);

    ui->A_LfStbd->setEnabled(gain->tvg_on);
//    ui->C_LfStbd->setEnabled(gain->tvg_on);
//    ui->T_LfStbd->setEnabled(gain->tvg_on);

    ui->A_HfPort->setEnabled(gain->tvg_on);
//    ui->C_HfPort->setEnabled(gain->tvg_on);
//    ui->T_HfPort->setEnabled(gain->tvg_on);

    ui->A_HfStbd->setEnabled(gain->tvg_on);
//    ui->C_HfStbd->setEnabled(gain->tvg_on);
//    ui->T_HfStbd->setEnabled(gain->tvg_on);
    updateBValueLine();
    emit updateMainPlots(GainSettings::LfPort, gain_old, 4);
    emit updateMainPlots(GainSettings::HfPort, gain_old, 4);
    setNewTvg();
    updateTvgCurve();
}

void TvgWindow :: on_box_sync_clicked() {
    gain->tvg_syncOn = ui->box_sync->isChecked();
    if (gain->tvg_syncOn) {
        if (gain->balanceSliderIndex >= 0) {
            gain->tvg_CLfS = gain->tvg_CLfP;
            gain->tvg_CHfS = gain->tvg_CHfP;
        } else {
            gain->tvg_CLfP = gain->tvg_CLfS;
            gain->tvg_CHfP = gain->tvg_CHfS;
        }
        gain->tvg_ALfSInd = gain->tvg_ALfPInd;
        gain->tvg_AHfSInd = gain->tvg_AHfPInd;
        updateTvgVariables();
        updateTvgCurve();
    }
}

void TvgWindow :: on_btn_ok_clicked() {
    // Save settings to userdb database
    bool emptyFields = false;
    emptyFields |= emptyFieldWarning(ui->C_LfPort);
//    emptyFields |= emptyFieldWarning(ui->F_LfPort);
//    emptyFields |= emptyFieldWarning(ui->T_LfPort);
    emptyFields |= emptyFieldWarning(ui->C_LfStbd);
//    emptyFields |= emptyFieldWarning(ui->F_LfStbd);
//    emptyFields |= emptyFieldWarning(ui->T_LfStbd);

    emptyFields |= emptyFieldWarning(ui->C_HfPort);
//    emptyFields |= emptyFieldWarning(ui->F_HfPort);
//    emptyFields |= emptyFieldWarning(ui->T_HfPort);
    emptyFields |= emptyFieldWarning(ui->C_HfStbd);
//    emptyFields |= emptyFieldWarning(ui->F_HfStbd);
//    emptyFields |= emptyFieldWarning(ui->T_HfStbd);
    if (emptyFields) {
        //  int ret = QMessageBox::warning(this, tr("Warning"), tr("Do you want to reset to default settings? ");
        QMessageBox msgBox;

        msgBox.critical(0,"Error","Required fields cannot be empty!");
        // setStyleSheet("{border : 2px solid orange;}");
        return;
        //resetUserSettings();
    }

    emit saveGainSettings();
    closeEvent(nullptr);
}

void TvgWindow :: on_btn_save_clicked() {
    // Save settings to userdb database
    bool emptyFields = false;
    emptyFields |= emptyFieldWarning(ui->C_LfPort);
//    emptyFields |= emptyFieldWarning(ui->F_LfPort);
//    emptyFields |= emptyFieldWarning(ui->T_LfPort);
    emptyFields |= emptyFieldWarning(ui->C_LfStbd);
//    emptyFields |= emptyFieldWarning(ui->F_LfStbd);
//    emptyFields |= emptyFieldWarning(ui->T_LfStbd);

    emptyFields |= emptyFieldWarning(ui->C_HfPort);
//    emptyFields |= emptyFieldWarning(ui->F_HfPort);
//    emptyFields |= emptyFieldWarning(ui->T_HfPort);
    emptyFields |= emptyFieldWarning(ui->C_HfStbd);
//    emptyFields |= emptyFieldWarning(ui->F_HfStbd);
//    emptyFields |= emptyFieldWarning(ui->T_HfStbd);
    if (emptyFields) {
        //  int ret = QMessageBox::warning(this, tr("Warning"), tr("Do you want to reset to default settings? ");
        QMessageBox msgBox;

        msgBox.critical(0,"Error","Required fields cannot be empty!");
        // setStyleSheet("{border : 2px solid orange;}");
        return;
        //resetUserSettings();
    }

    emit saveGainSettings();
}

void TvgWindow :: on_btn_reset_clicked() {
    // Reset all settings to user default
    emit resetGainSettings();
}

void TvgWindow :: on_btn_CLfP_Add_clicked() {
    addCValue(GainSettings::LfPort, 0.5);
}
void TvgWindow :: on_btn_CLfS_Add_clicked() {
    addCValue(GainSettings::LfStbd, 0.5);
}
void TvgWindow :: on_btn_CHfP_Add_clicked() {
    addCValue(GainSettings::HfPort, 0.5);
}
void TvgWindow :: on_btn_CHfS_Add_clicked() {
    addCValue(GainSettings::HfStbd, 0.5);
}
void TvgWindow :: on_btn_CLfP_Sub_clicked() {
    addCValue(GainSettings::LfPort, -0.5);
}
void TvgWindow :: on_btn_CLfS_Sub_clicked() {
    addCValue(GainSettings::LfStbd, -0.5);
}
void TvgWindow :: on_btn_CHfP_Sub_clicked() {
    addCValue(GainSettings::HfPort, -0.5);
}
void TvgWindow :: on_btn_CHfS_Sub_clicked() {
    addCValue(GainSettings::HfStbd, -0.5);
}

void TvgWindow :: addCValue(GainSettings::channelType chType, double val) {
    setNewTvg();
    switch(chType) {
    case GainSettings::LfPort:
        gain->tvg_CLfP += val;
        if (gain->tvg_syncOn)   gain->tvg_CLfS += val;
        break;
    case GainSettings::LfStbd:
        gain->tvg_CLfS += val;
        if (gain->tvg_syncOn)   gain->tvg_CLfP += val;
        break;
    case GainSettings::HfPort:
        gain->tvg_CHfP += val;
        if (gain->tvg_syncOn)   gain->tvg_CHfS += val;
        break;
    case GainSettings::HfStbd:
        gain->tvg_CHfS += val;
        if (gain->tvg_syncOn)   gain->tvg_CHfP += val;
        break;
    }
    ui->C_LfPort->setText(QString::number(gain->tvg_CLfP, 'f', 1));
    ui->C_LfStbd->setText(QString::number(gain->tvg_CLfS, 'f', 1));
    ui->C_HfPort->setText(QString::number(gain->tvg_CHfP, 'f', 1));
    ui->C_HfStbd->setText(QString::number(gain->tvg_CHfS, 'f', 1));
    updateTvgCurve();
    emit updateMainPlots(chType, gain_old, 3);
    setNewTvg();
}

void TvgWindow :: updateBValueLine() {
    //use QString::number method to transform a double to a string
    bool found = 0;
    double BLf = 0, BHf = 0;
    double maxRange = ui->plot_tvgCurve->graph(0)->data()->keyRange(found).upper;
    double maxTvg = ui->plot_tvgCurve->graph(0)->data()->valueRange(found).upper;
//    if (gain->calculateBLf)
        /*gain->tvg_BLf*/BLf = (maxTvg - (gain->tvg_ALfPInd*5) * log(maxRange)) / maxRange;//0.00017*gain->tvg_frequencyLow*gain->tvg_frequencyLow/(gain->tvg_T+18);
    ui->B_LfPort->setText(QString::number(/*gain->tvg_*/BLf,'f',3));
    ui->B_LfStbd->setText(QString::number(/*gain->tvg_*/BLf,'f',3));

//    if (gain->calculateBHf)
        /*gain->tvg_*/BHf = (maxTvg - (gain->tvg_ALfPInd*5) * log(maxRange)) / maxRange;//gain->tvg_BHf = 0.00017*gain->tvg_frequencyHigh*gain->tvg_frequencyHigh/(gain->tvg_T+18);
    ui->B_HfPort->setText(QString::number(/*gain->tvg_*/BHf,'f',3));
    ui->B_HfStbd->setText(QString::number(/*gain->tvg_*/BHf,'f',3));
    //updateTvgSettingsOnMainWindow();
}

void TvgWindow :: updateAValueLineLF(const QString &text) {
    // User changes index
    setNewTvg();
    if (gain->tvg_syncOn) {
        ui->A_LfPort->setCurrentText(text);
        ui->A_LfStbd->setCurrentText(text);
    }
    gain->tvg_ALfP = ui->A_LfPort->currentText().toDouble();
    gain->tvg_ALfS = ui->A_LfStbd->currentText().toDouble();
    gain->tvg_ALfPInd = ui->A_LfPort->currentIndex();
    gain->tvg_ALfSInd = ui->A_LfStbd->currentIndex();
    updateTvgCurve();
    emit updateMainPlots(GainSettings::LfPort, gain_old, 1);
    setNewTvg();
}
void TvgWindow :: updateAValueLineHF(const QString &text) {
    // User changes index
    setNewTvg();
    if (gain->tvg_syncOn) {
        ui->A_HfPort->setCurrentText(text);
        ui->A_HfStbd->setCurrentText(text);
    }
    gain->tvg_AHfP = ui->A_HfPort->currentText().toDouble();
    gain->tvg_AHfS = ui->A_HfStbd->currentText().toDouble();
    gain->tvg_AHfPInd = ui->A_HfPort->currentIndex();
    gain->tvg_AHfSInd = ui->A_HfStbd->currentIndex();
    updateTvgCurve();
    emit updateMainPlots(GainSettings::HfPort, gain_old, 1);
    setNewTvg();
}

void TvgWindow :: updateCValueLineLF(const QString &text) {
    // User changes text in field
    if (gain->tvg_syncOn) {
        ui->C_LfPort->setText(text);
        ui->C_LfStbd->setText(text);
    }
    gain->tvg_CLfP = ui->C_LfPort->text().toDouble();
    gain->tvg_CLfS = ui->C_LfStbd->text().toDouble();
    updateTvgCurve();
}

void TvgWindow :: updateCValueLineHF(const QString &text) {
    // User changes text in field
    if (gain->tvg_syncOn) {
        ui->C_HfPort->setText(text);
        ui->C_HfStbd->setText(text);
    }
    gain->tvg_CHfP = ui->C_HfPort->text().toDouble();
    gain->tvg_CHfS = ui->C_HfStbd->text().toDouble();
    updateTvgCurve();
}

void TvgWindow :: updateTemperatureLine(const QString &text) {
    // User edits temperature field
    // If user updates temperature field, then we recalculate B
    gain->calculateBLf = true;
    gain->calculateBHf = true;
//    ui->T_LfPort->setText(text);
//    ui->T_LfStbd->setText(text);
//    ui->T_HfPort->setText(text);
//    ui->T_HfStbd->setText(text);
    gain->tvg_T = text.toInt();
    updateBValueLine();
}

void TvgWindow :: GainsFromMainwindow(int GainSliderIndex, int BalanceSliderIndex, int GainSlinderIndex_HF) {
    // Update Slider Index values for database
    // Update UI fields
    updateTvgVariables();
}

void TvgWindow :: freqFromMainwindow(){
    // Frequency is updated from the main window, so we need to update TVG parameters
}

void TvgWindow :: get_Dynamic_range(int maxRange, int maxRange_HF){

    gain->tvg_maxLf = maxRange;
    gain->tvg_maxHf = maxRange_HF;
    double xStart = itemLineTvgMax->start->coords().x();
    double xEnd = itemLineTvgMax->end->coords().x();
    if (ui->btn_switchTvgFreq->isChecked()) {
        itemLineTvgMax->start->setCoords(xStart, maxRange_HF);
        itemLineTvgMax->end->setCoords(xEnd, maxRange_HF);
        itemLabelTvgMax->position->setCoords(0.5*(xStart+xEnd),maxRange_HF);
        itemLabelTvgMax->setText(QString::number(maxRange_HF));
    } else {
        itemLineTvgMax->start->setCoords(xStart, maxRange);
        itemLineTvgMax->end->setCoords(xEnd, maxRange);
        itemLabelTvgMax->position->setCoords(0.5*(xStart+xEnd),maxRange);
        itemLabelTvgMax->setText(QString::number(maxRange));
    }
    ui->plot_tvgCurve->replot(QCustomPlot::rpQueuedReplot);
}

//void TvgWindow ::

void TvgWindow :: setNewTvg() {
    gain_old.tvg_ALfP = gain->tvg_ALfP;
    gain_old.tvg_ALfS = gain->tvg_ALfS;
    gain_old.tvg_AHfP = gain->tvg_AHfP;
    gain_old.tvg_AHfS = gain->tvg_AHfS;
    gain_old.tvg_CLfP = gain->tvg_CLfP;
    gain_old.tvg_CLfS = gain->tvg_CLfS;
    gain_old.tvg_CHfP = gain->tvg_CHfP;
    gain_old.tvg_CHfS = gain->tvg_CHfS;
    gain_old.tvg_ALfPInd = gain->tvg_ALfPInd;
    gain_old.tvg_ALfSInd = gain->tvg_ALfSInd;
    gain_old.tvg_AHfPInd = gain->tvg_AHfPInd;
    gain_old.tvg_AHfSInd = gain->tvg_AHfSInd;
    gain_old.tvg_ratioLfP = gain->tvg_ratioLfP;
    gain_old.tvg_ratioHfP = gain->tvg_ratioHfP;
    gain_old.tvg_BLf = gain->tvg_BLf;
    gain_old.tvg_BHf = gain->tvg_BHf;
    gain_old.tvg_T = gain->tvg_T;
    gain_old.tvg_on = gain->tvg_on;
    gain_old.tvg_syncOn = gain->tvg_syncOn;
    gain_old.gainSliderIndexLf = gain->gainSliderIndexLf;
    gain_old.gainSliderIndexHf = gain->gainSliderIndexHf;
    gain_old.gainSliderIndexDepth = gain->gainSliderIndexDepth;
    gain_old.balanceSliderIndex = gain->balanceSliderIndex;
    gain_old.balanceSliderIndex_HF = gain->balanceSliderIndex_HF;
    gain_old.calculateBLf = gain->calculateBLf;
    gain_old.calculateBHf = gain->calculateBHf;
    gain_old.tvg_maxLf = gain->tvg_maxLf;
    gain_old.tvg_maxHf = gain->tvg_maxHf;
    gain_old.tvg_minLf = gain->tvg_minLf;
    gain_old.tvg_minHf = gain->tvg_minHf;
    gain_old.tvg_frequencyLow = gain->tvg_frequencyLow;
    gain_old.tvg_frequencyHigh = gain->tvg_frequencyHigh;
    gain_old.starGain = gain->starGain;
    gain_old.portGain = gain->portGain;
    gain_old.starGainHF = gain->starGainHF;
    gain_old.portGainHF = gain->portGainHF;
    gain_old.C_lfOffset = gain->C_lfOffset;
    gain_old.C_hfOffset = gain->C_hfOffset;
    gain_old.C_depthOffset = gain->C_depthOffset;
    gain_old.DigitalGainIndex = gain->DigitalGainIndex;
}
