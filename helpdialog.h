#ifndef HELPDIALOG_H
#define HELPDIALOG_H
#include <QMainWindow>
#include "qcustomplot.h"
#include "graphdialog.h"
#include "systemsettings.h"
#include "gpsmap.h"
#include "usermenu.h"
#include "createFlag.h"
#include "devwindow.h"
#include <QDataStream>
#include <QTcpSocket>
#include<QProgressBar>
#include<QGradient>
#include <QElapsedTimer>
#include <QSerialPort>
#include<QSerialPortInfo>
#include<QQueue>
#include "QCloseEvent"
#include "kfilter.h"
#include<QDesktopWidget>
#include<QList>
#include <QThreadPool> //THREAD
#include "libusb/libusb.h"
#include "format.h"
#include "chartdialog.h"
#include "helpbrowser.h"
#include <QHelpEngine>
#include <QHelpContentWidget>
#include <QHelpIndexWidget>
#include <QHelpSearchQueryWidget>
#include "converterdialog.h"
//#include <poppler/qt5/poppler-qt5.h>

//#include "helpdialog.h"
namespace Ui {
class helpDialog;
}

class helpDialog : public QDialog
{
    Q_OBJECT
    QDockWidget *helpWindow;
public:
    explicit helpDialog(QWidget *parent = nullptr);
    ~helpDialog();
public slots:
    void createHelpWindow();

    private:
        Ui::helpDialog *ui;
};

#endif // HELPDIALOG_H
