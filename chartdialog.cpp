#include "chartdialog.h"
#include "ui_chartdialog.h"
#include<QWidget>
#include "QCloseEvent"
#include<QMessageBox>
#include "mainwindow.h"

chartdialog::chartdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::chartdialog)
{
    ui->setupUi(this);
    ui->minLon->setValidator(new QDoubleValidator(-180,140,6,ui->minLon));
    ui->maxLon->setValidator(new QDoubleValidator(-180,140,6,ui->maxLon));
    ui->minLat->setValidator(new QDoubleValidator(-180,140,6,ui->minLat));
    ui->maxLat->setValidator(new QDoubleValidator(-180,140,6,ui->maxLat));

    ui->minLon->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");
    ui->maxLon->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");
    ui->minLat->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");
    ui->maxLat->setStyleSheet("QLineEdit::hover""{""border:2px solid orange;""}");
    //this->setWindowFlags(Qt::Window| Qt::WindowContextHelpButtonHint| Qt::WindowCloseButtonHint);

}

chartdialog::~chartdialog()
{
    delete ui;
}

void chartdialog :: on_cancelPushButtton_clicked(){
    this->hide();
}

void chartdialog :: on_okPushButton_clicked(){

    if(ui->minLon->text().toDouble()>ui->maxLon->text().toDouble()){
       int ret = QMessageBox::critical(this, tr("Chart Error"), tr("West Longitude must be greater than East Longitude"), QMessageBox::Ok);
      // setWindowFlags(Qt::Window| Qt::WindowContextHelpButtonHint| Qt::WindowCloseButtonHint);

        switch(ret){
        case QMessageBox::Ok:
            //this->hide();
            ui->minLon->setFocus();
            break;

        }


    }

   else if(ui->minLat->text().toDouble()>ui->maxLat->text().toDouble()){
        int ret = QMessageBox::critical(this, tr("Chart Error"), tr("North Latitude must be greater than South Latitude"), QMessageBox::Ok);

      // setWindowFlags(Qt::Window| Qt::WindowContextHelpButtonHint| Qt::WindowCloseButtonHint);

        switch(ret){
        case QMessageBox::Ok:
           // this->hide();
            ui->minLat->setFocus();
            break;

        }

    }

    else{
        saveCoordinates();
        emit chartValues(ui->minLon->text().toDouble(),ui->maxLon->text().toDouble(),ui->minLat->text().toDouble(),ui->maxLat->text().toDouble());

        this->hide();
    }




}

void chartdialog :: tiffFileName(QString fileName){
    name = fileName;
    if (PLAYER_DEMO) {
        ui->minLat->setText("44.617662");
        ui->maxLat->setText("44.638406");
        ui->minLon->setText("-63.961454");
        ui->maxLon->setText("-63.912805");
        on_okPushButton_clicked();
        this->hide();
        return;
    }

    readFromSavedCoords();
}

void chartdialog::saveCoordinates(){

    connOpenCoordinates();
    if(QSqlDatabase::contains("userConnection")){
        coordinates = QSqlDatabase::database("userConnection");
        QSqlQuery userqry(coordinates);
        QSqlQueryModel model;

        model.setQuery(userqry);

        userqry.prepare("CREATE TABLE IF NOT EXISTS coordinates(ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, fileName VARCHAR, minLongitude DOUBLE, maxLongitude DOUBLE, minLatitude DOUBLE, maxLatitude DOUBLE)");
        //userqry.exec();

        if(userqry.exec()){

        }else{
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        }

        //userqry.exec();
        if( !userqry.exec() ){
            qDebug() << userqry.lastError();

        }

        userqry.prepare("SELECT * FROM coordinates");
        userqry.exec();
        model.setQuery(userqry);
        qDebug()<<filenames.size();
        qDebug()<<model.rowCount();
        qDebug() << "data Saved";




        if(model.rowCount()==0){

            userqry.prepare("INSERT INTO coordinates(fileName, minLongitude, maxLongitude, minLatitude, maxLatitude) values(:fileName, :minLongitude, :maxLongitude, :minLatitude, :maxLatitude)");
            userqry.bindValue(":fileName", name);
            userqry.bindValue(":minLongitude", ui->minLon->text().toDouble());
            userqry.bindValue(":maxLongitude", ui->maxLon->text().toDouble());
            userqry.bindValue(":minLatitude", ui->minLat->text().toDouble());
            userqry.bindValue(":maxLatitude", ui->maxLat->text().toDouble());
        }


        for(int i=0;i<model.rowCount();i++){

            tiffName = model.record(i).value("fileName").toString();
            filenames.append(tiffName);

        }

        int counter=0;
        for(int i=0;i<filenames.size();i++){
            if(name==filenames.at(i)){
                counter++;

            }

        }

        if(counter>=1){

            userqry.prepare("UPDATE coordinates SET minLongitude = :minLongitude, maxLongitude = :maxLongitude, minLatitude= :minLatitude, maxLatitude = :maxLatitude WHERE fileName = :fileName");
            userqry.bindValue(":fileName", name);
            userqry.bindValue(":minLongitude", ui->minLon->text().toDouble());
            userqry.bindValue(":maxLongitude", ui->maxLon->text().toDouble());
            userqry.bindValue(":minLatitude", ui->minLat->text().toDouble());
            userqry.bindValue(":maxLatitude", ui->maxLat->text().toDouble());
        }else{

            userqry.prepare("INSERT INTO coordinates(fileName, minLongitude, maxLongitude, minLatitude, maxLatitude) values(:fileName, :minLongitude, :maxLongitude, :minLatitude, :maxLatitude)");
            userqry.bindValue(":fileName", name);
            userqry.bindValue(":minLongitude", ui->minLon->text().toDouble());
            userqry.bindValue(":maxLongitude", ui->maxLon->text().toDouble());
            userqry.bindValue(":minLatitude", ui->minLat->text().toDouble());
            userqry.bindValue(":maxLatitude", ui->maxLat->text().toDouble());

        }

        if(userqry.exec()){
            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));

        }
        else
        {
            //userqry.finish();
            //userqry.exec();
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";
            connCloseCoordinates();//closing database connection after saving
        }
    }
    else{qDebug()<<"not available"; }
}


void chartdialog :: readFromSavedCoords(){

    connOpenCoordinates();
    QSqlQuery userqry(coordinates);

    userqry.prepare("SELECT * FROM coordinates");
    userqry.exec();
    QSqlQueryModel model;
    model.setQuery(userqry);

    if(model.query().exec()){

        for (int i=0;i<model.rowCount();i++) {
            if(name == model.record(i).value("fileName").toString())   {
                qDebug()<<"tari mani";

                minLong = model.record(i).value("minLongitude").toDouble();
                ui->minLon->setText(QString::number(minLong,'d',6));
                qDebug()<<minLong;
                maxLong = model.record(i).value("maxLongitude").toDouble();
                ui->maxLon->setText(QString::number(maxLong,'d',6));

                minLat = model.record(i).value("minLatitude").toDouble();
                ui->minLat->setText(QString::number(minLat,'d',6));

                maxLat = model.record(i).value("maxLatitude").toDouble();
                ui->maxLat->setText(QString::number(maxLat,'d',6));
                break;

            }else{
                ui->minLon->setText(QString::number(0));
                ui->maxLon->setText(QString::number(0));
                ui->minLat->setText(QString::number(0));
                ui->maxLat->setText(QString::number(0));
            }
        }
    }else{
        QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }

}




