#ifndef DEVWINDOW_H
#define DEVWINDOW_H

#include <QDialog>

namespace Ui {
class DevWindow;
}

class DevWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DevWindow(QWidget *parent = 0);
    ~DevWindow();

private slots:
    void on_ok_btn_clicked();
signals:
    void openSys();

private:
    Ui::DevWindow *ui;
};

#endif // DEVWINDOW_H
