#ifndef GRAPHDIALOG_H
#define GRAPHDIALOG_H

#include <QDialog>

namespace Ui {
class graphDialog;
}

class graphDialog : public QDialog
{
    Q_OBJECT

public:
    
    explicit graphDialog(QWidget *parent = 0);
    ~graphDialog();
    void setupPlots();
    void addDataToPortSideGraph1(int sampleNum, int color);
    void addDataToStarBoardSideGraph1(int sampleNum, int color);
    void addDataToPortSideGraph2(int sampleNum, int color);
    void addDataToPortSideGraph3(int sampleNum, int color, int graphInt);
    void addDataToPortSideGraph4(int sampleNum, int color);
    void addDataToStarBoardSideGraph2(int sampleNum, int color);
    void addDataToCrossChannelGraph(int sample, int value);
    void setActivation(bool active);
    int numOfSamples = 1000;
    void removeData1();
    void removeData2();
    void refreshPlot1();
    void refreshPlot2();
    double lowerRange = 0;
    double upperRange = 0;
    bool customRange = 0;

private slots:
    
    void on_fullRange_clicked();
    void on_customRange_clicked();
    void on_setRange_btn_clicked();
    void closeEvent(QCloseEvent *);

signals:
    
    void getView();
    void graph_windowClosed(bool);
    void XYfromGraph(double,double);
    void XYfromGraphP(double,double);
    void LFRangesToMain(double,double);
    void rescaleToMain();
    void replotToMain();

private:
    Ui::graphDialog *graph_ui;
};

#endif // GRAPHDIALOG_H
