// This file is part of kfilter. 
// kfilter is a C++ variable-dimension extended kalman filter library. 
// 
// Copyright (C) 2004        Vincent Zalzal, Sylvain Marleau 
// Copyright (C) 2001, 2004  Richard Gourdeau 
// Copyright (C) 2004        GRPR and DGE's Automation sector 
//                           École Polytechnique de Montréal 
// 
// Code adapted from algorithms presented in : 
//      Bierman, G. J. "Factorization Methods for Discrete Sequential 
//      Estimation", Academic Press, 1977. 
// 
// This library is free software; you can redistribute it and/or 
// modify it under the terms of the GNU Lesser General Public 
// License as published by the Free Software Foundation; either 
// version 2.1 of the License, or (at your option) any later version. 
// 
// This library is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details. 
// 
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 
#ifndef KTYPES_HPP 
#define KTYPES_HPP 
 
 
#include <stdexcept> 
#include <string> 
 
 
namespace Kalman { 
   
  typedef short int K_INT_16;              
  typedef unsigned short int K_UINT_16;    
  typedef long int K_INT_32;               
  typedef unsigned long int K_UINT_32;     
  typedef float K_REAL_32;                 
  typedef double K_REAL_64;                
   
  struct KalmanError : public std::logic_error { 
 
 
    explicit KalmanError(const std::string& message)  
      : logic_error(message) {} 
  }; 
 
  struct OutOfBoundError : public KalmanError { 
 
    explicit OutOfBoundError(const std::string& message)  
      : KalmanError(message) {} 
  }; 
 
  namespace Util { 
     
     
    template <typename T> 
    inline void swap(T& a, T& b) { 
      T tmp = a; 
      a = b; 
      b = tmp; 
    } 
     
  } 
 
} 
 
#endif
