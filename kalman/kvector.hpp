// This file is part of kfilter. 
// kfilter is a C++ variable-dimension extended kalman filter library. 
// 
// Copyright (C) 2004        Vincent Zalzal, Sylvain Marleau 
// Copyright (C) 2001, 2004  Richard Gourdeau 
// Copyright (C) 2004        GRPR and DGE's Automation sector 
//                           École Polytechnique de Montréal 
// 
// Code adapted from algorithms presented in : 
//      Bierman, G. J. "Factorization Methods for Discrete Sequential 
//      Estimation", Academic Press, 1977. 
// 
// This library is free software; you can redistribute it and/or 
// modify it under the terms of the GNU Lesser General Public 
// License as published by the Free Software Foundation; either 
// version 2.1 of the License, or (at your option) any later version. 
// 
// This library is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details. 
// 
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 
#ifndef KVECTOR_HPP 
#define KVECTOR_HPP 
 
 
#include <vector> 
#include <string> 
#include <iostream> 
 
#include "kalman/ktypes.hpp" 
 
namespace Kalman { 
 
 
  template<typename T, K_UINT_32 BEG, bool DBG> 
  class KVector { 
  public: 
 
    typedef T type;              
 
    enum { beg = BEG             
    }; 
 
 
 
    inline KVector(); 
 
    inline explicit KVector(K_UINT_32 n); 
 
    inline KVector(K_UINT_32 n, const T& a); 
 
    inline KVector(K_UINT_32 n, const T* v); 
 
    inline KVector(const KVector& v); 
 
    inline ~KVector(); 
 
 
 
 
    inline T& operator()(K_UINT_32 i); 
 
    inline const T& operator()(K_UINT_32 i) const; 
 
    inline K_UINT_32 size() const; 
 
 
    inline void resize(K_UINT_32 n); 
 
    inline KVector& operator=(const T& a); 
 
    inline KVector& operator=(const KVector& v); 
 
    inline void assign(K_UINT_32 n, const T* v); 
 
    inline void swap(KVector& v); 
 
 
 
    inline void get(std::istream& is); 
 
    inline void put(std::ostream& os) const; 
 
 
  private: 
    std::vector<T> vimpl_;       
     
 
    T* v_; 
    K_UINT_32 n_;                
  }; 
 
  template<typename T, K_UINT_32 BEG, bool DBG> 
  inline std::istream& operator>>(std::istream& is,  
                                  KVector<T, BEG, DBG>& v); 
 
  template<typename T, K_UINT_32 BEG, bool DBG> 
  inline std::ostream& operator<<(std::ostream& os,  
                                  const KVector<T, BEG, DBG>& v); 
 
  typedef unsigned short KVectorContext; 
 
  extern KVectorContext DEFAULT_VECTOR_CONTEXT; 
 
  KVectorContext createKVectorContext(std::string elemDelim = " ",  
                                      std::string startDelim = "",  
                                      std::string endDelim = "",  
                                      unsigned prec = 4); 
 
  KVectorContext selectKVectorContext(KVectorContext c); 
} 
 
#include "kalman/kvector_impl.hpp" 
 
#endif
