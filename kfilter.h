#ifndef KFILTER_H
#define KFILTER_H

#include "kalman/ekfilter.hpp"

class cPlaneEKF : public Kalman::EKFilter<double,1> {
public:
        cPlaneEKF();

protected:

        void makeA();
        void makeH();
        void makeV();
        void makeR();
        void makeW();
        void makeQ();
        void makeProcess();
        void makeMeasure();

        double Period;
};

typedef cPlaneEKF::Vector Vector;
typedef cPlaneEKF::Matrix Matrix;

#endif // KFILTER_H
