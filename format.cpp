#include "format.h"
#include "ui_format.h"
#include "mainwindow.h"
MainWindow* uMainWindow2;

format :: format(QWidget *parent) :
    QDialog(parent),

    ui(new Ui::format)
{
    uMainWindow2 = qobject_cast<MainWindow*>(this->parent());
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window);

    //this->setWindowFlags(Qt::Window| Qt::WindowContextHelpButtonHint| Qt::WindowCloseButtonHint);
    showAltimeterClickable(false);
//    ui->altiBox->setVisible(false);
//    ui->GapBox->setVisible(false);
//    ui->colorScaleBox->setVisible(false);
    //if(uMainWindow2->PlotAngleScan){
    //  ui->altiBox->setEnabled(true);
    //}
    //else{
    // ui->altiBox->setEnabled(false);
    //}

}

format :: ~format()
{
    delete ui;
}

void format :: closeEvent(QCloseEvent *event) {
//    userFormatSettings();
    this->hide();
}

void format :: on_saveButton_clicked(){
    saveFormatSettings();
    this->close();
}

void format :: on_resetButton_clicked() {
    QMessageBox msgBox;
    // msgBox.warning(0,"Warning","Do you want to reset to default settings? ");
    //msgBox.setInformativeText("Do you want to reset to default settings? ");
    //  msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int ret = QMessageBox::warning(this, tr("Warning"), tr("Do you want to reset to a default format? "),QMessageBox::Yes | QMessageBox::No);
    switch(ret){
    case QMessageBox::Yes:
        resetFormatSettings();
        break;
    case QMessageBox::No:
        //this->hide();
        break;
    }
    saveFormatSettings();
    this->close();
}

void format :: updateCheckBoxes() {
    widthSonar = widthLfHf + widthDepth + widthSensor;
    isOpenLf = (widthLfHf > 0) && (heightLf > 0) && (widthSonar > 0);
    isOpenHf = (widthLfHf > 0) && (heightHf > 0) && (widthSonar > 0);
    isOpen_DepthPlot = (widthDepth > 0) && (widthSonar > 0);
    isOpen_sensorPlot = (widthSensor > 0) && (widthSonar > 0);
    isOpen_gpsPlot = (widthGnss > 0) && (heightGnss > 0);
    isOpen_TargetWidget = (widthGnss > 0) && (widthTargets > 0) && (heightTools > 0);
    isOpen_imagePlot = (widthGnss > 0) && (widthGnss > 0) && (heightTools > 0);
//    return;
    ui->LFBox->setChecked(isOpenLf);
    ui->HFBox->setChecked(isOpenHf);
    ui->GraphBox->setChecked(isOpen_sensorPlot);
    ui->altiBox->setChecked(isOpen_DepthPlot);
    ui->GpsBox->setChecked(isOpen_gpsPlot);

    ui->gainSliderBox->setChecked(isGainSliderOn);
    ui->StatusBox->setChecked(isStatusBarShown);
    ui->SiganlPlotBox->setChecked(isSignalPlotOpen);
    ui->GridBox->setChecked(isGridShown);
    ui->ZoomBox->setChecked(isZoomBoxAlwaysOn);
    ui->CompassBox->setChecked(isDisplayCompass);
    ui->CrosshairBox->setChecked(isCrosshairShown);
}

void format :: on_LFBox_clicked(){
    uMainWindow2->format_showLfPlot(ui->LFBox->isChecked());
}

void format :: on_HFBox_clicked(){
    uMainWindow2->format_showHfPlot(ui->HFBox->isChecked());
}

void format :: on_altiBox_clicked(){
//    if (ui->altiBox->isChecked()) {
//        // Split the depthSensor splitter in half
//        if (widthSensor > 0) {
//            widthDepth = widthSensor / 2;
//            widthSensor /= 2;
//        } else {
//            widthDepth = widthLfHf / 2;
//            widthLfHf /= 2;
//        }
//    } else {
//        if (widthSensor > 0) {
//            widthSensor += widthDepth;
//            widthDepth = 0;
//        } else {
//            widthLfHf += widthDepth;
//            widthDepth = 0;
//        }
//    }

//    uMainWindow2->format_setPlotSizes();
    uMainWindow2->format_showDepthPlot(ui->altiBox->isChecked());
    isOpen_DepthPlot=ui->altiBox->isChecked();
}

void format :: on_GpsBox_clicked(){

    uMainWindow2->format_showGpsPlot(ui->GpsBox->isChecked());
    isOpen_gpsPlot=ui->GpsBox->isChecked();
}

void format :: on_GraphBox_clicked(){
    //qDebug()<<"depthPlot: "<<uMainWindow2->depthPlot->width();
//    if (ui->GraphBox->isChecked()) {
//        // Split the depthSensor splitter in half
//        if (widthDepth > 0) {
//            widthSensor = widthDepth / 2;
//            widthDepth /= 2;
//        } else {
//            widthSensor = widthLfHf / 2;
//            widthLfHf /= 2;
//        }
//    } else {
//        if (widthDepth > 0) {
//            widthDepth += widthSensor;
//            widthSensor = 0;
//        } else {
//            widthLfHf += widthSensor;
//            widthSensor = 0;
//        }
//    }

    uMainWindow2->format_showSensorGraph(ui->GraphBox->isChecked());
    isOpen_sensorPlot = ui->GraphBox->isChecked();
    // qDebug()<<"depthPlot: "<<uMainWindow2->depthPlot->width();

}

void format :: on_GapBox_clicked(){
//    emit showGap(ui->GapBox->isChecked());
//    gapBox = ui->GapBox->isChecked();
}

void format :: on_StatusBox_clicked(){
    emit showStatus(ui->StatusBox->isChecked());
    isStatusBarShown=ui->StatusBox->isChecked();
    // qDebug()<<"altibox clicked"<<statusBox;
}

void format :: on_colorScaleBox_clicked(){

//    emit showColorScale(ui->colorScaleBox->isChecked());
//    colorScaleBox=ui->colorScaleBox->isChecked();
}

void format :: on_SiganlPlotBox_clicked(){

    emit showSignal(ui->SiganlPlotBox->isChecked());
    isSignalPlotOpen=ui->SiganlPlotBox->isChecked();

}

void format :: on_gainSliderBox_clicked(){

    emit showGain(ui->gainSliderBox->isChecked());

    isGainSliderOn=ui->gainSliderBox->isChecked();

}

void format :: showFormatGainSlider(bool show){

    ui->gainSliderBox->setChecked(show);

}

void format :: on_GridBox_clicked(){
    isGridShown=ui->GridBox->isChecked();
    emit showGrid(isGridShown);
}

void format :: on_ZoomBox_clicked(){
    isZoomBoxAlwaysOn=ui->ZoomBox->isChecked();
    emit showZoomBox(isZoomBoxAlwaysOn);
}

void format :: on_CompassBox_clicked() {
    isDisplayCompass = ui->CompassBox->isChecked();
    emit showCompass(isDisplayCompass);
}

void format :: on_CrosshairBox_clicked() {
    isCrosshairShown = ui->CrosshairBox->isChecked();
    emit showCrosshair(isCrosshairShown);
}

void format :: selectStatusBar(bool show){
    if(show){
        ui->StatusBox->setChecked(true);
    }
}

void format :: showGnssClickable (bool show){
    if(!show){
        ui->GpsBox->setDisabled(true);
        ui->GpsBox->setStyleSheet("color:grey");
    }else{
        ui->GpsBox->setEnabled(true);
        ui->GpsBox->setStyleSheet("color:white");
    }
}

void format :: showHfClickable(bool show){
    if(!show){
        //ui->HFBox->setEnabled(false);
        ui->HFBox->setDisabled(true);
        ui->HFBox->setChecked(false);
        ui->HFBox->setStyleSheet("color:grey");
    }else{
        ui->HFBox->setEnabled(true);
        ui->HFBox->setStyleSheet("color:white");
//        if (ui->HFBox->isChecked()) {
//            uMainWindow2->showHfPlot(ui->HFBox->isChecked());
//        }
    }
}

void format :: showAltimeterClickable(bool show){
    if (!show){
        //ui->HFBox->setEnabled(false);
        ui->altiBox->setDisabled(true);
        ui->altiBox->setStyleSheet("color:grey");
    } else {
        ui->altiBox->setEnabled(true);
        ui->altiBox->setStyleSheet("color:white");
//        if (ui->altiBox->isChecked()) {
//            uMainWindow2->showDepthPlot(ui->altiBox->isChecked());
//        }
    }
}

void format :: updateSplitterSizeInfo(double ratio1, double ratio2, double ratioTools,
                                      double ratioSonar, double ratio3) {
    if (ratio1 > 100)        ratio1 = 100;
    if (ratio2 > 100)       ratio2 = 100;
    if (ratioTools > 100)   ratioTools = 100;
    if (ratioSonar > 100)   ratioSonar = 100;
    if (ratio3 > 100)       ratio3 = 100;

    splitter1Ratio = ratio1;
    splitter2Ratio = ratio2;
    splitterToolsRatio = ratioTools;
    splitterSonarRatio = ratioSonar;
    splitter3Ratio = ratio3;
    splittersUpdated = 1;
}

void format :: saveFormatSettings(){
    //qDebug()<<"Lf value as it enters the function: "<<lF;
    connOpenFormatSettings();
    if(QSqlDatabase::contains("userConnection")){
        formatSettings = QSqlDatabase::database("userConnection");
        QSqlQuery userqry(formatSettings);

        // Save settings to show/hide plots
        userqry.prepare(("DROP TABLE IF EXISTS formatSettings"));//Table is recreated with new updated datas everytime save is clicked
        userqry.exec();
        if(userqry.exec()){
            //   qDebug()<<"User Data Updated";
        }
        else
        {       QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());

        }
        userqry.prepare("CREATE TABLE IF NOT EXISTS formatSettings (/*ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NULL, */"
                        "showAltimeter INTEGER, showSensorGraph INTEGER, showSignalPlot INTEGER, showLF INTEGER, showHF INTEGER, showColorScale INTEGER, showGPSPlot INTEGER, showBalanceSlider INTEGER, showGainSlider INTEGER, "
                        "showGap INTEGER, showStatusBar INTEGER, showGrid INTEGER, showZoomBox INTEGER, showCompassBox INTEGER, showCrosshairBox INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << userqry.lastError();
        }
        userqry.prepare("INSERT INTO formatSettings(showAltimeter, showSensorGraph, showSignalPlot, showLF, showHF, showColorScale, showGPSPlot, showBalanceSlider, showGainSlider, showGap, showStatusBar, showGrid, showZoomBox, showCompassBox, showCrosshairBox) "
                        "values(:showAltimeter, :showSensorGraph, :showSignalPlot, :showLF, :showHF, :showColorScale, :showGPSPlot, :showBalanceSlider, :showGainSlider, :showGap, :showStatusBar, :showGrid, :showZoomBox, :showCompassBox, :showCrosshairBox)");

        isOpen_DepthPlot=ui->altiBox->isChecked();
        isOpenLf = ui->LFBox->isChecked();
        isOpenHf = ui->HFBox->isChecked();
        isOpen_sensorPlot = ui->GraphBox->isChecked();
        isStatusBarShown=ui->StatusBox->isChecked();
        isOpen_gpsPlot=ui->GpsBox->isChecked();
        isSignalPlotOpen=ui->SiganlPlotBox->isChecked();
        isGainSliderOn=ui->gainSliderBox->isChecked();
        isGridShown=ui->GridBox->isChecked();
        isZoomBoxAlwaysOn=ui->ZoomBox->isChecked();
        isDisplayCompass = ui->CompassBox->isChecked();

        userqry.bindValue(":showAltimeter", int(isOpen_DepthPlot));
        userqry.bindValue(":showSensorGraph", int(isOpen_sensorPlot));
        userqry.bindValue(":showSignalPlot", int(isSignalPlotOpen));
        userqry.bindValue(":showLF", int(isOpenLf));
        userqry.bindValue(":showHF", int(isOpenHf));
        userqry.bindValue(":showColorScale", int(0)); // Todo: remove this entry
        userqry.bindValue(":showGPSPlot", int(isOpen_gpsPlot));
        userqry.bindValue(":showBalanceSlider", int(sliderBox));
        userqry.bindValue(":showGainSlider", int(isGainSliderOn));
        userqry.bindValue(":showStatusBar", (int)isStatusBarShown);
        userqry.bindValue(":showGrid", (int)isGridShown);
        userqry.bindValue(":showZoomBox", (int)isZoomBoxAlwaysOn);
        userqry.bindValue(":showCompassBox", (int)isDisplayCompass);
        userqry.bindValue(":showCrosshairBox", (int)isCrosshairShown);
        //qDebug()<<"lfvalue AFTER BIND: "<<lF;

        userqry.exec();

        qDebug() << userqry.lastError();

        userqry.prepare("INSERT INTO formatSettings(showAltimeter, showSensorGraph, showSignalPlot, showLF, showHF, showColorScale, showGPSPlot, showBalanceSlider, showGainSlider, showGap, showStatusBar, showGrid, showZoomBox, showCompassBox, showCrosshairBox) "
                        "values(:showAltimeter, :showSensorGraph, :showSignalPlot, :showLF, :showHF, :showColorScale, :showGPSPlot, :showBalanceSlider, :showGainSlider, :showGap, :showStatusBar, :showGrid, :showZoomBox, :showCompassBox, :showCrosshairBox)");
        userqry.bindValue(":showAltimeter", 1);
        userqry.bindValue(":showSensorGraph", 1);
        userqry.bindValue(":showSignalPlot", 1);
        userqry.bindValue(":showLF", 1);
        userqry.bindValue(":showHF", 1);
        userqry.bindValue(":showColorScale", 1);
        userqry.bindValue(":showGPSPlot", 1);
        userqry.bindValue(":showBalanceSlider", 0);
        userqry.bindValue(":showGainSlider", 0);
        userqry.bindValue(":showGap", 0);
        userqry.bindValue(":showStatusBar",1);
        userqry.bindValue(":showGrid",1);
        userqry.bindValue(":showZoomBox",0);
        userqry.bindValue(":showCompassBox", 1);
        userqry.bindValue(":showCrosshairBox", 0);

        userqry.exec();

        // Save the splitter sizes
        userqry.prepare(("DROP TABLE IF EXISTS formatSplitterSizes"));//Table is recreated with new updated datas everytime save is clicked
        userqry.exec();
        if(userqry.exec()){
            //   qDebug()<<"User Data Updated";
        }
        else
        {       QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());

        }
//        userqry.prepare("CREATE TABLE IF NOT EXISTS formatSplitterSizes (/*ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NULL, */splitter1Ratio FLOAT, splitter2Ratio FLOAT, splitterToolsRatio FLOAT, splitterSonarRatio FLOAT, splitter3Ratio FLOAT)");
        userqry.prepare("CREATE TABLE IF NOT EXISTS formatSplitterSizes ("
                        "width_LfHf INTEGER, width_Depth INTEGER, width_Sensor INTEGER, width_Gnss INTEGER, width_Image INTEGER, "
                        "width_Targets INTEGER, height_Tools INTEGER, height_Gnss INTEGER, height_Lf INTEGER, height_Hf INTEGER, "
                        "width_Window INTEGER, height_Window INTEGER, window_Maximized INTEGER)");
        userqry.exec();
        if( !userqry.exec() ){
            qDebug() << userqry.lastError();
        }
//        userqry.prepare("INSERT INTO formatSplitterSizes(splitter1Ratio, splitter2Ratio, splitterToolsRatio, splitterSonarRatio, splitter3Ratio) values(:splitter1Ratio, :splitter2Ratio, :splitterToolsRatio, :splitterSonarRatio, :splitter3Ratio)");
        userqry.prepare("INSERT INTO formatSplitterSizes(width_LfHf, width_Depth, width_Sensor, width_Gnss, width_Image,"
                        "width_Targets, height_Tools, height_Gnss, height_Lf, height_Hf, "
                        "width_Window, height_Window, window_Maximized) "
                        "values(:width_LfHf, :width_Depth, :width_Sensor, :width_Gnss, :width_Image,"
                        ":width_Targets, :height_Tools, :height_Gnss, :height_Lf, :height_Hf,"
                        ":width_Window, :height_Window, :window_Maximized)");

//        userqry.bindValue(":splitter1Ratio", splitter1Ratio);
//        userqry.bindValue(":splitter2Ratio", splitter2Ratio);
//        userqry.bindValue(":splitterToolsRatio", splitterToolsRatio);
//        userqry.bindValue(":splitterSonarRatio", splitterSonarRatio);
//        userqry.bindValue(":splitter3Ratio", splitter3Ratio);

        userqry.bindValue(":width_LfHf", widthLfHf);
        userqry.bindValue(":width_Depth", widthDepth);
        userqry.bindValue(":width_Sensor", widthSensor);
        userqry.bindValue(":width_Gnss", widthGnss);
        userqry.bindValue(":width_Image", widthImage);

        userqry.bindValue(":width_Targets", widthTargets);
        userqry.bindValue(":height_Tools", heightTools);
        userqry.bindValue(":height_Gnss", heightGnss);
        userqry.bindValue(":height_Lf", heightLf);
        userqry.bindValue(":height_Hf", heightHf);

        userqry.bindValue(":width_Window", widthWindow);
        userqry.bindValue(":height_Window", heightWindow);
        userqry.bindValue(":window_Maximized", windowMaximized);

        userqry.exec();

//        userqry.prepare("INSERT INTO formatSplitterSizes(splitter1Ratio, splitter2Ratio, splitterToolsRatio, splitterSonarRatio, splitter3Ratio) values(:splitter1Ratio, :splitter2Ratio, :splitterToolsRatio, :splitterSonarRatio, :splitter3Ratio)");
        userqry.prepare("INSERT INTO formatSplitterSizes(width_LfHf, width_Depth, width_Sensor, width_Gnss, width_Image,"
                        "width_Targets, height_Tools, height_Gnss, height_Lf, height_Hf, "
                        "width_Window, height_Window, window_Maximized) "
                        "values(:width_LfHf, :width_Depth, :width_Sensor, :width_Gnss, :width_Image,"
                        ":width_Targets, :height_Tools, :height_Gnss, :height_Lf, :height_Hf,"
                        ":width_Window, :height_Window, :window_Maximized)");
        userqry.bindValue(":width_LfHf", 500);
        userqry.bindValue(":width_Depth", 200);
        userqry.bindValue(":width_Sensor", 0);
        userqry.bindValue(":width_Gnss", 300);
        userqry.bindValue(":width_Image", 600);

        userqry.bindValue(":width_Targets", 400);
        userqry.bindValue(":height_Tools", 200);
        userqry.bindValue(":height_Gnss", 800);
        userqry.bindValue(":height_Lf", 500);
        userqry.bindValue(":height_Hf", 500);

        userqry.bindValue(":width_Window", 0);
        userqry.bindValue(":height_Window", 0);
        userqry.bindValue(":window_Maximized", 1);

        splittersUpdated = 0;

        if(userqry.exec()){

            // QMessageBox::information(this,tr("DATA SAVING"),tr("DATA SAVED!!"));

            qDebug() << "data Saved";
        }
        else
        {
            //userqry.finish();
            QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
            qDebug() << "Error1";
            connOpenFormatSettings();//closing database connection after saving
        }
    }
    else{qDebug()<<"not available"; }
}

void format :: userFormatSettings(){

    connOpenFormatSettings();
    if(!QSqlDatabase::contains("userConnection")) {
        qDebug()<<"not available";
        return;
    }
    QSqlQuery userqry(formatSettings);
    userqry.prepare("SELECT * FROM formatSettings ");
    userqry.exec();

    QSqlQueryModel model;
    model.setQuery(userqry);

    if (model.query().exec()){

//        dAlti = model.record(0).value("showAltimeter").toBool();
//        ui->altiBox->setChecked(dAlti);
////        uMainWindow2->format_showDepthPlot(dAlti);

//        dGraphBox = model.record(0).value("showSensorGraph").toBool();
//        ui->GraphBox->setChecked(dGraphBox);
////        uMainWindow2->format_showSensorGraph(dGraphBox);

//        dLf = model.record(0).value("showLF").toBool();
//        ui->LFBox->setChecked(dLf);
////        uMainWindow2->format_showLfPlot(dLf);

//        dHf = model.record(0).value("showHF").toBool();
//        ui->HFBox->setChecked(dHf);
////        uMainWindow2->format_showHfPlot(dHf);

//        dGpsBox = model.record(0).value("showGPSPlot").toBool();
//        ui->GpsBox->setChecked(dGpsBox);
////        emit showGps(dGpsBox);

        isSignalPlotOpen = model.record(0).value("showSignalPlot").toBool();
        ui->SiganlPlotBox->setChecked(isSignalPlotOpen);
        emit showSignal(isSignalPlotOpen);

        sliderBox = model.record(0).value("showBalanceSlider").toBool();
        //emit showBalance(sliderBox);

        isGainSliderOn = model.record(0).value("showGainSlider").toBool();
        ui->gainSliderBox->setChecked(isGainSliderOn);
        emit showGain(isGainSliderOn);

        isStatusBarShown = model.record(0).value("showStatusBar").toBool();
        ui->StatusBox->setChecked(isStatusBarShown);
        emit showStatus(isStatusBarShown);

        isGridShown = model.record(0).value("showGrid").toBool();
        ui->GridBox->setChecked(isGridShown);
        emit showGrid(isGridShown);

        isZoomBoxAlwaysOn = model.record(0).value("showZoomBox").toBool();
        ui->ZoomBox->setChecked(isZoomBoxAlwaysOn);
        emit showZoomBox(isZoomBoxAlwaysOn);

        isDisplayCompass = model.record(0).value("showCompassBox").toBool();
        ui->CompassBox->setChecked(isDisplayCompass);
        emit showCompass(isDisplayCompass);

        isCrosshairShown = model.record(0).value("showCrosshairBox").toBool();
        ui->CrosshairBox->setChecked(isCrosshairShown);
        emit showCrosshair(isCrosshairShown);
    }
    else{
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        //connCloseUser();
    }

    if (splittersUpdated) {
//        emit setSplitterRatios(splitter1Ratio, splitter2Ratio, splitterToolsRatio, splitterSonarRatio, splitter3Ratio);
        return;
    }
    userqry.prepare("SELECT * FROM formatSplitterSizes");
    userqry.exec();
    model.setQuery(userqry);
    if (model.query().exec()){
//        splitter1Ratio = model.record(0).value("splitter1Ratio").toDouble();
//        splitter2Ratio = model.record(0).value("splitter2Ratio").toDouble();
//        splitterToolsRatio = model.record(0).value("splitterToolsRatio").toDouble();
//        splitterSonarRatio = model.record(0).value("splitterSonarRatio").toDouble();
//        splitter3Ratio = model.record(0).value("splitter3Ratio").toDouble();

        widthLfHf = model.record(0).value("width_LfHf").toInt();
        widthDepth = model.record(0).value("width_Depth").toInt();
        widthSensor = model.record(0).value("width_Sensor").toInt();
        widthGnss = model.record(0).value("width_Gnss").toInt();
        widthImage = model.record(0).value("width_Image").toInt();

        widthTargets = model.record(0).value("width_Targets").toInt();
        heightTools = model.record(0).value("height_Tools").toInt();
        heightGnss = model.record(0).value("height_Gnss").toInt();
        heightLf = model.record(0).value("height_Lf").toInt();
        heightHf = model.record(0).value("height_Hf").toInt();
        //if (heightLf > 0 & heightHf > 0)    heightHf = heightLf;

        widthWindow = model.record(0).value("width_Window").toInt();
        heightWindow = model.record(0).value("height_Window").toInt();
        windowMaximized = model.record(0).value("window_Maximized").toInt();
        uMainWindow2->format_setPlotSizes();

//        emit setSplitterRatios(splitter1Ratio, splitter2Ratio, splitterToolsRatio, splitterSonarRatio, splitter3Ratio);
    }
    else {
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }
}

void format :: resetFormatSettings(){

    connOpenFormatSettings();
    if(!QSqlDatabase::contains("userConnection")) {
        qDebug()<<"not available";
        return;
    }
    QSqlQuery userqry(formatSettings);
    userqry.prepare("SELECT * FROM formatSettings ");
    userqry.exec();

    QSqlQueryModel model;
    model.setQuery(userqry);

    if (model.query().exec()){
        isSignalPlotOpen = model.record(1).value("showSignalPlot").toBool();
        ui->SiganlPlotBox->setChecked(isSignalPlotOpen);
        emit showSignal(isSignalPlotOpen);

        sliderBox = model.record(1).value("showBalanceSlider").toBool();
        //emit showBalance(sliderBox);

        isGainSliderOn = model.record(1).value("showGainSlider").toBool();
        ui->gainSliderBox->setChecked(isGainSliderOn);
        emit showGain(isGainSliderOn);

        isStatusBarShown = model.record(1).value("showStatusBar").toBool();
        ui->StatusBox->setChecked(isStatusBarShown);
        emit showStatus(isStatusBarShown);

        isGridShown = model.record(1).value("showGrid").toBool();
        ui->GridBox->setChecked(isGridShown);
        emit showGrid(isGridShown);

        isZoomBoxAlwaysOn = model.record(1).value("showZoomBox").toBool();
        ui->ZoomBox->setChecked(isZoomBoxAlwaysOn);
        emit showZoomBox(isZoomBoxAlwaysOn);

        isDisplayCompass = model.record(1).value("showCompassBox").toBool();
        ui->CompassBox->setChecked(isDisplayCompass);
        emit showCompass(isDisplayCompass);

        isCrosshairShown = model.record(1).value("showCrosshairBox").toBool();
        ui->CrosshairBox->setChecked(isCrosshairShown);
        emit showCrosshair(isCrosshairShown);
    }
    else{
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
        //connCloseUser();
    }

    if (splittersUpdated) {
//        emit setSplitterRatios(splitter1Ratio, splitter2Ratio, splitterToolsRatio, splitterSonarRatio, splitter3Ratio);
        return;
    }
    userqry.prepare("SELECT * FROM formatSplitterSizes");
    userqry.exec();
    model.setQuery(userqry);
    if (model.query().exec()){
        widthLfHf = model.record(1).value("width_LfHf").toInt();
        widthDepth = model.record(1).value("width_Depth").toInt();
        widthSensor = model.record(1).value("width_Sensor").toInt();
        widthGnss = model.record(1).value("width_Gnss").toInt();
        widthImage = model.record(1).value("width_Image").toInt();

        widthTargets = model.record(1).value("width_Targets").toInt();
        heightTools = model.record(1).value("height_Tools").toInt();
        heightGnss = model.record(1).value("height_Gnss").toInt();
        heightLf = model.record(1).value("height_Lf").toInt();
        heightHf = model.record(1).value("height_Hf").toInt();
        //if (heightLf > 0 & heightHf > 0)    heightHf = heightLf;

        widthWindow = model.record(1).value("width_Window").toInt();
        heightWindow = model.record(1).value("height_Window").toInt();
        windowMaximized = model.record(1).value("window_Maximized").toBool();
        uMainWindow2->format_setPlotSizes();

//        emit setSplitterRatios(splitter1Ratio, splitter2Ratio, splitterToolsRatio, splitterSonarRatio, splitter3Ratio);
    }
    else {
        //QMessageBox::critical(this,tr("ERROR"),userqry.lastError().text());
    }
}

void format :: bottomTrackingClicked(bool val){
    double steps = 13;
    if (val) {
        uMainWindow2->format_showDepthPlot(true);
        ui->altiBox->setChecked(true);
        emit showGap(false);
        //ui->GapBox->setChecked(false);
        emit bottomTrackingZoom(true);
    } else {
        uMainWindow2->format_showDepthPlot(false);
        ui->altiBox->setChecked(false);
        emit showGap(true);
        //ui->GapBox->setChecked(true);
        emit bottomTrackingZoom(false);
    }
}

