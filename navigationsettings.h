#ifndef NAVIGATIONSETTINGS_H
#define NAVIGATIONSETTINGS_H

#include <QDialog>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include "modules.h"

namespace Ui {
class NavigationSettings;
}

class NavigationSettings : public QDialog
{
    Q_OBJECT

public:
    explicit NavigationSettings(QWidget *parent = nullptr);
    ~NavigationSettings();
    void readNmea(QStringList DataList);
    void saveSettings();
    void loadSettings();
    int convertBaudRateToInt(QSerialPort::BaudRate baud);
    QSerialPort::BaudRate convertIntToBaudRate(int baud);
    int convertDataBitsToInt(QSerialPort::DataBits databits);
    QSerialPort::DataBits convertIntToDataBits(int databits);
    int convertParityBitsToInt(QSerialPort::Parity paritybits);
    QSerialPort::Parity convertIntToParityBits(int paritybits);
    int convertStopBitsToInt(QSerialPort::StopBits stopbits);
    QSerialPort::StopBits convertIntToStopBits(int stopbits);
    QSerialPort::BaudRate baudRate = QSerialPort::Baud115200;
    QSerialPort::DataBits dataBits = QSerialPort::Data8;
    QSerialPort::Parity parity = QSerialPort::NoParity;
    QSerialPort::StopBits stopBits = QSerialPort::OneStop;
    QSerialPort *ComPort = nullptr;
    bool windowOpen = false;
    SonarPing *ping;
    GnssController *gnssController = nullptr;

    struct GPSINFO {
        int Year = 0;
        int Month = 0;
        int Day = 0;

        int Hour = 0;
        int Minute = 0;
        int Second = 0;
        int Millisecond = 0;

        double Lattitude_DMS = 0;
        QString Lattitude_NS = "N";
        double Longitude_DMS = 0;
        QString Longitude_EW = "W";

        double Lattitude_Deg = -500;
        double Longitude_Deg = -500;

        int PingNum = 0;
        double Speed = -1;
        double Heading = -900;
        int HeadingType = -1; // 0 means GUI calculated, 1 means read from GNSS
    };
    GPSINFO GpsItems;

    struct MSINFO {
        int Hour = 0;
        int Minute = 0;
        int Second = 0;
        int Millisecond = 0;

        int CompassX = 0;
        int CompassY = 0;
        int CompassZ = 0;

        int AccelerometerX = 0;
        int AcceleromoterY = 0;
        int AccelerometerZ = 0;

        double Pitch = 0;
        double Roll = 0;

        int GyroX = 0;
        int GyroY = 0;
        int GyroZ = 0;
    };
    MSINFO MotionItems;

    struct SERIALSETTINGS {
        QString SettingName;
        QString PortName;
        QSerialPort::BaudRate baudRate = QSerialPort::Baud115200;
        QSerialPort::DataBits dataBits = QSerialPort::Data8;
        QSerialPort::Parity parity = QSerialPort::NoParity;
        QSerialPort::StopBits stopBits = QSerialPort::OneStop;
    };
    QList<SERIALSETTINGS> settings_list;

signals:
    void sendPortSettingsToMainWindow (QSerialPortInfo portInfo, int baudRate, int dataBits, int stopBits, int parity);
    void updateGpsPlot();

public slots:
    void on_list_port_currentTextChanged();
    void on_radio_data7_clicked();
    void on_radio_data8_clicked();
    void on_radio_stop1_clicked();
    void on_radio_stop2_clicked();
    void on_radio_parityNone_clicked();
    void on_radio_parityOdd_clicked();
    void on_radio_parityEven_clicked();
    void on_list_baud_currentTextChanged();
    void on_btn_add_clicked();
    void on_btn_delete_clicked();
    void on_btn_load_clicked();
    void on_btn_save_clicked();
    void on_btn_refresh_clicked();
    void readSerial();
    void closeSerial(QSerialPort::SerialPortError error);
    void reloadPortInfo();
    void closeEvent(QCloseEvent *);
    void mousePressEvent(QMouseEvent *e);
    bool eventFilter(QObject *obj, QEvent *e);
    void keyPressEvent(QKeyEvent *e);

private:
    Ui::NavigationSettings *ui;
    QTimer validationTimer;
    bool updateComboBox = 0;
    int invalidMessageCount = 0;

private slots:
    void invalidateTime();
    void on_btn_ok_clicked();
};

#endif // NAVIGATIONSETTINGS_H
