/*devwindow.cpp, Header comment written August 2019
 * The devwindow acts as password protection for the system settings menu
 * In order to access the system settings the user will have to input the password defined by the QString pass
 */
#include "devwindow.h"
#include "ui_devwindow.h"
#include <QDebug>

DevWindow::DevWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DevWindow)
{
    ui->setupUi(this);

}

DevWindow::~DevWindow()
{
    delete ui;
}

void DevWindow :: on_ok_btn_clicked() {
    QString pass = "Jeta2021";
//    QString pass = "";//for development purposes we changed the password to be blank for easy access, on release change it back to Jetasonic
    QString inpass;
    inpass = ui->pass_line->text();
    if (pass.compare(inpass) == 0) {//compares user input to pass
        inpass = "";
        ui->pass_line->setText("");//sets password field back to blank space
        emit openSys();//opens system settings menu
        //qDebug() << "Passwords match!!";
    }else{
        ui->pass_line->setText("");
    }
}
