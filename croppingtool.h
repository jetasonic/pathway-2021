#ifndef CROPPINGTOOL_H
#define CROPPINGTOOL_H

#include <QWidget>
#include "qcustomplot.h"

namespace Ui {
class CroppingTool;
}

class CroppingTool : public QWidget
{
    Q_OBJECT

public:
    explicit CroppingTool(QWidget *parent = nullptr);
    ~CroppingTool();

    void updatePlayerPosition(double position);
    void setCropLine(QCustomPlot *plotPtr, double yPos, double range, double angle);
    void moveCropLine(QCustomPlot *plotPtr, double yPos, double range, double angle);
    int loadBuffer();
    void cropData();
    void clearItems();
    void removeItemsBelowY(double yCoord);
    void updateCropPosition();
    void resetSlider();
    void setCropStart();
    void setCropEnd();
    void addCursorText(QCustomPlot *plot, QMouseEvent* event);
    void updateCursorText(/*QMouseEvent* event,*/ QString text);
    void UndoLastClick();

    bool toolActive = false;
    bool toolStarted = false;
    bool startSelected = false;
    bool endSelected = false;
    int ping_start = 0, ping_end = 0;
    int ping_rangeBegin = 0, ping_rangeEnd = 0;
    QString rtc_start = "";
    double slider_start = 0, slider_end = 1;

    QCPItemLine *start_port = nullptr, *start_stbd = nullptr;
    QCPItemLine *end_port = nullptr, *end_stbd = nullptr;
    QCPItemText *cursorText = nullptr;

    QString filePath;
    QFile fileOut;
    QByteArray fileOutBuffer;

    QByteArray fileInBuffer;
    QFile fileIn;
    char *fileInPtr = nullptr;
    int fileInPos = 0;
    int fileInSize = 0;
    int fileInBufferPos = 0;

    int fileInStartPos = 0, fileInEndPos = 0;

    QFile fileOutGps, fileInGps;
    QFile fileOutFlag, fileInFlag;

signals:
    void SaveCropping();

private slots:
    void on_btn_Start_clicked();
    void on_btn_End_clicked();
    void on_btn_Save_clicked();

private:
    Ui::CroppingTool *ui;
};

#endif // CROPPINGTOOL_H
