#ifndef CONFIG_DIALOG_H
#define CONFIG_DIALOG_H


#include <QMainWindow>
#include "qcustomplot.h"

namespace Ui {
class ui_Config_Dialog;
}

class ui_Config_Dialog : public QMainWindow
{
    Q_OBJECT

public:
    ui_Config_dialog(QWidget *parent1 = 0);
    ~ui_Config_Dialog();
private slots:
    //void realtimeDataSlot();
private:
    Ui::ui_Config_Dialog *ui2;
    //QTimer dataTimer;

};

#endif // CONFIG_DIALOG_H
