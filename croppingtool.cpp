#include "croppingtool.h"
#include "ui_croppingtool.h"

CroppingTool::CroppingTool(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CroppingTool)
{
    ui->setupUi(this);
    ui->btn_End->hide();
    ui->btn_Start->hide();
    updateCropPosition();

    QFont font = ui->label_duration->font();
    QFontMetrics fm(font);
    ui->label_duration->setMinimumWidth(fm.horizontalAdvance("_00:00:00_"));
}

CroppingTool::~CroppingTool()
{
    delete ui;
}

void CroppingTool :: on_btn_Start_clicked() {
    startSelected = ui->btn_Start->isChecked();
    setCropStart();
}

void CroppingTool :: on_btn_End_clicked() {
    endSelected = ui->btn_End->isChecked();
    setCropEnd();
}

void CroppingTool :: on_btn_Save_clicked() {
    // Send signal to main window to begin cropping
    emit SaveCropping();
}

void CroppingTool :: setCropStart() {
    startSelected = true;
    ui->btn_Start->setChecked(startSelected);
    if (endSelected) {
        endSelected = false;
        ui->btn_End->setChecked(endSelected);
    }

    // if checked, change cursor and allow user to place a start marker on the plot
    if (start_port != nullptr && start_stbd != nullptr) {
        QPen distancePen1;
        distancePen1.setColor(Qt::white);
        distancePen1.setWidth(5);
        distancePen1.setStyle(Qt::DashLine);

        start_port->setPen(distancePen1);
        start_port->setSelectedPen(distancePen1);
        start_stbd->setPen(distancePen1);
        start_stbd->setSelectedPen(distancePen1);
        start_stbd->parentPlot()->replot(QCustomPlot::rpQueuedReplot);
    }
}

void CroppingTool :: setCropEnd() {
    endSelected = true;
    ui->btn_End->setChecked(endSelected);
    if (startSelected) {
        startSelected = false;
        ui->btn_Start->setChecked(startSelected);
    }

    if (end_port != nullptr && end_stbd != nullptr) {
        QPen distancePen1;
        distancePen1.setColor(Qt::white);
        distancePen1.setWidth(3);
        distancePen1.setStyle(Qt::DashLine);

        end_port->setPen(distancePen1);
        end_port->setSelectedPen(distancePen1);
        end_stbd->setPen(distancePen1);
        end_stbd->setSelectedPen(distancePen1);
        end_stbd->parentPlot()->replot(QCustomPlot::rpQueuedReplot);
    }
}

void CroppingTool :: resetSlider() {
    ui->slider->setValue(0);
    slider_start = 0;
    slider_end = 1;
    updateCropPosition();
    ping_start = 0;
    ping_end = 0;
    ping_rangeBegin = 0;
    ping_rangeEnd = 0;
    ui->label_duration->setText("00:00");
    clearItems();
}

void CroppingTool :: updatePlayerPosition(double position) {
    ui->slider->setValue(position*1000);
}

void CroppingTool :: updateCropPosition() {
    QString style = "QSlider::groove:horizontal {"
                  "border: 1px solid #bbb;"
                  "background: qlineargradient(x1:0 y1:0, x2:1 y2:0, stop:0 black, stop:";
    if (slider_end - slider_start < 0.002)     slider_end = slider_start + 0.002;
    if (slider_start < slider_end)
        style += QString::number(slider_start, 'f', 3) +
                  " black, stop:" + QString::number(slider_start+0.001, 'f', 3) +
                  " orange, stop:" + QString::number(slider_end, 'f', 3) +
                  " orange, stop:" + QString::number(slider_end+0.001, 'f', 3);
    else
        style += QString::number(slider_end, 'f', 3) +
                  " black, stop:" + QString::number(slider_end+0.001, 'f', 3) +
                  " orange, stop:" + QString::number(slider_start, 'f', 3) +
                  " orange, stop:" + QString::number(slider_start+0.001, 'f', 3);
    style += " black); height: 10px; border-radius: 4px;}"

                  "QSlider::handle:horizontal {"
                  "background: qlineargradient(x1:0, y1:0, x2:1, y2:1,"
                  "    stop:0 #eee, stop:1 #ccc);"
                  "border: 1px solid #777;"
                  "width: 13px;"
                  "margin-top: -2px;"
                  "margin-bottom: -2px;"
                  "border-radius: 4px;}"

                  "QSlider::handle:horizontal:hover {"
                  "background: qlineargradient(x1:0, y1:0, x2:1, y2:1,"
                  "    stop:0 #fff, stop:1 #ddd);"
                  "border: 1px solid #444;"
                  "border-radius: 4px;}"

                  "QSlider::sub-page:horizontal:disabled {"
                  "background: #bbb;"
                  "border-color: #999;}"

                  "QSlider::add-page:horizontal:disabled {"
                  "background: #eee;"
                  "border-color: #999;}"

                  "QSlider::handle:horizontal:disabled {"
                  "background: #eee;"
                  "border: 1px solid #aaa;"
                  "border-radius: 4px;}";
    ui->slider->setStyleSheet(style);
}

void CroppingTool :: setCropLine(QCustomPlot *plotPtr, double yPos, double range, double angle) {
    if (startSelected && ping_start < 0)    return;
    if (endSelected && ping_end < 0)        return;

    double y1 = yPos, x1 = 0;
    double x2 = range, y2 = yPos + range*cos(angle * M_PI/180);

    QPen distancePen1;
    distancePen1.setColor(Qt::white);
    distancePen1.setWidth(3);
    distancePen1.setStyle(Qt::SolidLine);

    if (startSelected) {
        if (start_port == nullptr || start_stbd == nullptr) {
            start_port = new QCPItemLine(plotPtr);
            start_stbd = new QCPItemLine(plotPtr);
            start_stbd->setLayer("tools1");
            start_port->setLayer("tools1");
        }

        start_port->start->setCoords(x1, y1);
        start_port->end->setCoords(-x2, y2);
        start_port->setPen(distancePen1);
        start_port->setSelectedPen(distancePen1);

        start_stbd->start->setCoords(x1, y1);
        start_stbd->end->setCoords(x2, y2);
        start_stbd->setPen(distancePen1);
        start_stbd->setSelectedPen(distancePen1);

        slider_start = (ping_start - ping_rangeBegin)/((ping_rangeEnd - ping_rangeBegin)*1.0);
        updateCropPosition();

        plotPtr->replot(QCustomPlot::rpQueuedReplot);
        startSelected = false;
        ui->btn_Start->setChecked(startSelected);

        setCropEnd();
    }
    else if (endSelected) {
        if (end_port == nullptr || end_stbd == nullptr) {
            end_port = new QCPItemLine(plotPtr);
            end_stbd = new QCPItemLine(plotPtr);
            end_stbd->setLayer("tools1");
            end_port->setLayer("tools1");
        }

        end_port->start->setCoords(x1, y1);
        end_port->end->setCoords(-x2, y2);
        end_port->setPen(distancePen1);
        end_port->setSelectedPen(distancePen1);

        end_stbd->start->setCoords(x1, y1);
        end_stbd->end->setCoords(x2, y2);
        end_stbd->setPen(distancePen1);
        end_stbd->setSelectedPen(distancePen1);

        slider_end = (ping_end - ping_rangeBegin)/((ping_rangeEnd - ping_rangeBegin)*1.0);
        updateCropPosition();

        plotPtr->replot(QCustomPlot::rpQueuedReplot);
        endSelected = false;
        ui->btn_End->setChecked(endSelected);

        emit SaveCropping();
    }
}

void CroppingTool :: moveCropLine(QCustomPlot *plotPtr, double yPos, double range, double angle) {
    if (startSelected && ping_start < 0)    return;
    if (endSelected && ping_end < 0)        return;

    double y1 = yPos, x1 = 0;
    double x2 = range, y2 = yPos + range*cos(angle * M_PI/180);

    QPen distancePen1;
    distancePen1.setColor(Qt::white);
    distancePen1.setWidth(3);
    distancePen1.setStyle(Qt::DashLine);

    if (startSelected) {
        if (start_port == nullptr || start_stbd == nullptr) {
            start_port = new QCPItemLine(plotPtr);
            start_stbd = new QCPItemLine(plotPtr);
            start_stbd->setLayer("tools1");
            start_port->setLayer("tools1");
        }

        start_port->start->setCoords(x1, y1);
        start_port->end->setCoords(-x2, y2);
        start_port->setPen(distancePen1);
        start_port->setSelectedPen(distancePen1);

        start_stbd->start->setCoords(x1, y1);
        start_stbd->end->setCoords(x2, y2);
        start_stbd->setPen(distancePen1);
        start_stbd->setSelectedPen(distancePen1);

        slider_start = (ping_start - ping_rangeBegin)/((ping_rangeEnd - ping_rangeBegin)*1.0);
        updateCropPosition();
    }
    else if (endSelected) {
        if (end_port == nullptr || end_stbd == nullptr) {
            end_port = new QCPItemLine(plotPtr);
            end_stbd = new QCPItemLine(plotPtr);
            end_stbd->setLayer("tools1");
            end_port->setLayer("tools1");
        }

        end_port->start->setCoords(x1, y1);
        end_port->end->setCoords(-x2, y2);
        end_port->setPen(distancePen1);
        end_port->setSelectedPen(distancePen1);

        end_stbd->start->setCoords(x1, y1);
        end_stbd->end->setCoords(x2, y2);
        end_stbd->setPen(distancePen1);
        end_stbd->setSelectedPen(distancePen1);

        slider_end = (ping_end - ping_rangeBegin)/((ping_rangeEnd - ping_rangeBegin)*1.0);
        updateCropPosition();
    }
}

int CroppingTool :: loadBuffer() {
    if (fileInPos + 10000 > fileInSize) {
        // Stop loading and close file
        return -1;
    }
    if (fileInBufferPos + 100000 > fileInBuffer.size()) {
        fileInBuffer.remove(0, fileInBufferPos);
        fileInBufferPos = 0;
        fileInBuffer.append(fileIn.read(5000000));
        fileInPtr = fileInBuffer.data();
        return 1;
    }
    return 0;
}

void CroppingTool :: cropData() {
    QString fileOutPath = filePath.mid(0, filePath.length()-4);
    QString fileInPath = fileIn.fileName().mid(0, fileIn.fileName().length()-4);

    // Load sonar data into fileInBuffer
    fileIn.reset();
    fileInPos = fileIn.skip(fileInStartPos);

    // Copy sonar Data
    while(fileInEndPos - fileInPos > 0) {
        fileInBuffer.clear();
        int amountToRead = 5000000;
        if (fileInEndPos - fileInPos < amountToRead)
            amountToRead = fileInEndPos - fileInPos;
        fileInBuffer.append(fileIn.read(amountToRead));
        fileInPos += amountToRead;

        // Copy to fileOutBuffer
        fileOut.write(fileInBuffer);
    }
    fileOut.close();
    fileIn.close();
    fileInBuffer.clear();

    // Copy GPS data
    fileInGps.seek(0);
    qint64 gpsPos = fileInGps.pos();
    QString GpsFileLine;
    QStringList GpsFileItems;
    QByteArray gpsBuffer;
    // Find first ping
    while (1) {
        gpsPos = fileInGps.pos();
        GpsFileLine = fileInGps.readLine();
        GpsFileItems = GpsFileLine.split("\t");
        if (GpsFileLine == "") {
            // Break loop if the file has reached the end
            break;
        } else if (ping_start == GpsFileItems.at(0).mid(0,8).toInt()) {
            fileInGps.seek(gpsPos);
            break;
        } else if (ping_start < GpsFileItems.at(0).mid(0,8).toInt()) {
            break;
        }
    }
    // Copy data
    while (1) {
        gpsPos = fileInGps.pos();
        GpsFileLine = fileInGps.readLine();
        gpsBuffer.append(GpsFileLine);
        GpsFileItems = GpsFileLine.split("\t");
        if (GpsFileLine == "") {
            // Break loop if the file has reached the end
            break;
        } else if (ping_end == GpsFileItems.at(0).mid(0,8).toInt()) {
            break;
        } else if (ping_end < GpsFileItems.at(0).mid(0,8).toInt()) {
            break;
        }
    }
    fileOutGps.write(gpsBuffer);
    fileOutGps.close();
    fileInGps.close();

    // Copy target data
    QStringList list;
    QByteArray flagData;
    while (!fileInFlag.atEnd()) {
        QByteArray line = fileInFlag.readLine();
        QString str(line);
        list = str.split(",");
        int item = list.at(3).toInt(); // Read the ping number
        if ((item < ping_end - ping_rangeBegin) && (item > ping_start - ping_rangeBegin) && item > 0) {
            // Copy the targets that are within the two markers
            QByteArray before;
            before.append("," + list.at(3) + ",");
            QByteArray after;
            after.append(", " + QString::number(item - (ping_start - ping_rangeBegin)) + ",");
            line.replace(before, after);
            flagData.append(line);
            // Copy the target images and rename them
            QFile image(fileInPath + " " + list.at(0) + ".png");
            image.copy(fileOutPath + " " + list.at(0) + ".png");
        }
    }
    fileOutFlag.write(flagData);
    fileOutFlag.close();
    fileInFlag.close();

    clearItems();
}

void CroppingTool :: clearItems() {
    if (end_port != nullptr) {
        end_port->parentPlot()->removeItem(end_port);
        end_stbd->parentPlot()->removeItem(end_stbd);
    }
    if (start_port != nullptr) {
        start_port->parentPlot()->removeItem(start_port);
        start_stbd->parentPlot()->removeItem(start_stbd);
    }
    if (cursorText != nullptr) {
        cursorText->parentPlot()->removeItem(cursorText);
        cursorText = nullptr;
    }
    end_port = nullptr;
    end_stbd = nullptr;
    start_port = nullptr;
    start_stbd = nullptr;
}

void CroppingTool :: removeItemsBelowY(double yCoord) {
    if (end_port != nullptr) {
        if (end_port->start->coords().y() < yCoord) {
            end_port->parentPlot()->removeItem(end_port);
            end_stbd->parentPlot()->removeItem(end_stbd);
            end_port = nullptr;
            end_stbd = nullptr;
        }
    }
    if (start_port != nullptr) {
        if (start_port->start->coords().y() < yCoord) {
            start_port->parentPlot()->removeItem(start_port);
            start_stbd->parentPlot()->removeItem(start_stbd);
            start_port = nullptr;
            start_stbd = nullptr;
        }
    }
}

void CroppingTool :: addCursorText(QCustomPlot *plot, QMouseEvent* event) {
    if (ping_start < 0)    return;

//    cursorText = new QCPItemText(plot);
//    cursorText->setText("Duration: 00:00:00");
//    cursorText->setColor(Qt::yellow);
//    cursorText->setBrush(QBrush(Qt::black));
//    cursorText->setSelectedBrush(QBrush(Qt::black));
//    cursorText->setFont(QFont(font().family(),11));
//    cursorText->position->setPixelPosition(event->localPos());
//    cursorText->setLayer("tools1");
//    cursorText->setSelectedColor(Qt::yellow);
//    cursorText->setSelectedFont(QFont(font().family(),11));
//    cursorText->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
//    cursorText->setSelected(false);
}

void CroppingTool :: updateCursorText(/*QMouseEvent* event, */QString text) {
//    if (cursorText == nullptr)  return;
    if (rtc_start == "")        return;
    if (text == "") {
//        if (abs(event->y() - cursorText->position->pixelPosition().y()) > 20) {
//            if (cursorText != nullptr)
//                cursorText->setVisible(false);
//        }
        return;
    } else {
//        if (cursorText != nullptr)
//            cursorText->setVisible(true);
    }

    int hours_start = rtc_start.mid(0, 2).toInt();
    int minutes_start = rtc_start.mid(3, 2).toInt();
    int seconds_start = rtc_start.mid(6, 2).toInt();
    int totalStart = hours_start*3600 + minutes_start*60 + seconds_start;

    int hours_end = text.mid(0, 2).toInt();
    int minutes_end = text.mid(3, 2).toInt();
    int seconds_end = text.mid(6, 2).toInt();
    int totalEnd = hours_end*3600 + minutes_end*60 + seconds_end;

    int totalDiff = totalEnd - totalStart;
    int hours_diff = totalDiff / 3600;
    int minutes_diff = abs((totalDiff / 60) % 60);
    int seconds_diff = abs(totalDiff % 60);

    QString diffText = QString::number(minutes_diff).rightJustified(2,'0') + ":" + QString::number(seconds_diff).rightJustified(2,'0');
    if (hours_diff != 0)    diffText = QString::number(hours_diff).rightJustified(2,'0') + ":" + diffText;
    ui->label_duration->setText(diffText);
//    if (cursorText != nullptr) {
//        cursorText->setText(diffText);
//        cursorText->position->setPixelPosition(event->localPos());
//    }
}

void CroppingTool :: UndoLastClick() {
    clearItems();
    ping_start = 0;
    ping_end = 0;
    resetSlider();
}

