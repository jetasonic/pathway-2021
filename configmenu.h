#ifndef CONFIGMENU_H
#define CONFIGMENU_H

#include <QDialog>
#include<QtSql>
#include<QDebug>
#include<QFileInfo>
#include<QLabel>

# define PI           3.14159265358979323846




namespace Ui {
class configMenu;
}

class configMenu : public QDialog
{
    Q_OBJECT



public:
    explicit configMenu(QWidget *parent = 0);
    ~configMenu();




    QSqlDatabase myConfigDB;






    void connClose(){
        myConfigDB.close();
        myConfigDB.removeDatabase(QSqlDatabase::defaultConnection);

    }
    bool connOpen(){
        myConfigDB = QSqlDatabase::addDatabase("QSQLITE");

         myConfigDB.setDatabaseName(filename);

         if(!myConfigDB.open()){

             qDebug()<<("Couldnot Connect to Database");
             return false;
         }

         else{

             qDebug()<<( "Connected to Database.....");
             return true;
         }
    }

    QString filename;
    QString createFile;
    QList<QString> Rfwdtemp,depthTemp,CompTemp,propagationTemp,f_rep_temp,n_max_temp,n_comp_temp,n_plot_temp,swath_temp,l_plot_temp,res_lat_temp,res_fwd_temp;


    double setval = (15*3.14159265358979323846 )/180;

     QList <QLabel*> rfwd_list;
     QList <QLineEdit*> depthlist,compmx_list;
     QList <QLabel*> propagationtm_list;
     QList <QLabel*> f_rep_list;
     QList <QLabel*> n_max_list;
     QList <QLabel*> n_comp_list;
     QList <QLabel*> n_plot_list;
     QList <QLabel*> swath_list;
     QList <QLabel*> l_plot_list,res_lat_list,res_fwd_list;



    // QList<double> dep,propagation,freqn,nMax,comp,nComp,lplot,nplot,swath;
    // QList<QString> dlist,cmp;
    // QList<double> rfwd_max ;
    // QList<QString> rfwdout,ncompOut,lplotOut,swathOut,propagationout,freqout,maxOut,nplotOut;

     QString d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14;
     QString cmp1,cmp2,cmp3,cmp4,cmp5,cmp6,cmp7,cmp8,cmp9,cmp10,cmp11,cmp12,cmp13,cmp14;
     QString rfwdout1,rfwdout2,rfwdout3,rfwdout4,rfwdout5,rfwdout6,rfwdout7,rfwdout8,rfwdout9,rfwdout10,rfwdout11,rfwdout12,rfwdout13,rfwdout14;
     QString propagationout1,propagationout2,propagationout3,propagationout4,propagationout5,propagationout6,propagationout7,propagationout8,propagationout9,propagationout10,propagationout11,propagationout12,propagationout13,propagationout14;
     QString freqout1,freqout2,freqout3,freqout4,freqout5,freqout6,freqout7,freqout8,freqout9,freqout10,freqout11,freqout12,freqout13,freqout14;
     QString maxOut1,maxOut2,maxOut3,maxOut4,maxOut5,maxOut6,maxOut7,maxOut8,maxOut9,maxOut10,maxOut11,maxOut12,maxOut13,maxOut14;
     QString ncompOut1,ncompOut2,ncompOut3,ncompOut4,ncompOut5,ncompOut6,ncompOut7,ncompOut8,ncompOut9,ncompOut10,ncompOut11,ncompOut12,ncompOut13,ncompOut14;
     QString lplotOut1,lplotOut2,lplotOut3,lplotOut4,lplotOut5,lplotOut6,lplotOut7,lplotOut8,lplotOut9,lplotOut10,lplotOut11,lplotOut12,lplotOut13,lplotOut14;
     QString nplotOut1,nplotOut2,nplotOut3,nplotOut4,nplotOut5,nplotOut6,nplotOut7,nplotOut8,nplotOut9,nplotOut10,nplotOut11,nplotOut12,nplotOut13,nplotOut14;
     QString swathOut1 , swathOut2, swathOut3, swathOut4, swathOut5, swathOut6, swathOut7, swathOut8, swathOut9, swathOut10, swathOut11, swathOut12, swathOut13, swathOut14;
     QString reslatOut1,reslatOut2,reslatOut3,reslatOut4,reslatOut5,reslatOut6,reslatOut7,reslatOut8,reslatOut9,reslatOut10,reslatOut11,reslatOut12,reslatOut13,reslatOut14;
     QString rfwdOut1,rfwdOut2,rfwdOut3,rfwdOut4,rfwdOut5,rfwdOut6,rfwdOut7,rfwdOut8,rfwdOut9,rfwdOut10,rfwdOut11,rfwdOut12,rfwdOut13,rfwdOut14;
     QString resfwdout1,resfwdout2,resfwdout3,resfwdout4,resfwdout5,resfwdout6,resfwdout7,resfwdout8,resfwdout9,resfwdout10,resfwdout11,resfwdout12,resfwdout13,resfwdout14;

     QString confileName = "";
     QString dateUpdated = "";
     QString dateCreated = "";

     QString getConfigFile();
      QString getDateCreated();
       QString getDateUpdated();







   void assignFileName(QString dataFileName);

void showDefault();
private slots:
    //void row();
    void row1();
    void row2();
    void row3();
    void row4();
    void row5();
    void row6();
    void row7();
    void row8();
    void row9();
    void row10();
    void row11();
    void row12();
    void row13();
    void row14();
   void savedata();
   void writefile();
    void readfile();
   void date_time();
   bool traceFreqvald();

   void setConfigInfo(QString confile,QString dateCrt , QString dateUpd);

   void showConfig();


private:
    Ui::configMenu *config_ui;
    int boatpropagation = 3;
     bool validangle,validFrequency,validDepth,validCompFactor;
     double frequency_s ;
     double traceAngle;
};

#endif // CONFIGMENU_H
