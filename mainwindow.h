﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include "graphdialog.h"
#include "systemsettings.h"
#include "gpsmap.h"
#include "usermenu.h"
#include "createFlag.h"
#include "devwindow.h"
#include <QDataStream>
#include <QTcpSocket>
#include<QProgressBar>
#include<QGradient>
#include <QElapsedTimer>
#include <QSerialPort>
#include<QSerialPortInfo>
#include<QQueue>
#include "QCloseEvent"
#include "kfilter.h"
#include<QDesktopWidget>
#include<QList>
#include <QThreadPool> //THREAD
#include "libusb/libusb.h"
#include "format.h"
#include "chartdialog.h"
#include "helpbrowser.h"
#include <QHelpEngine>
#include <QHelpContentWidget>
#include <QHelpIndexWidget>
#include <QHelpSearchQueryWidget>
#include "converterdialog.h"
#include "helpdialog.h"
#include "navigationsettings.h"
#include "modules.h"
#include "tvgwindow.h"
#include "depthalarmwidget.h"
#include "croppingtool.h"
//#include <QWebPage>

#define PLAYER_DEMO 0
#define DEVELOPER_TESTING 0   // Intended to help test waypoint features during playback

namespace Ui {
class MainWindow;
}

QT_BEGIN_NAMESPACE
class QTcpSocket;
class QNetworkSession;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

#if PLAYER_DEMO == 1
    QString windowTitle = "Pathway Player 2021/09/10 C1.03";
#else
    QString windowTitle = "Jaimi 1.10";
    QString developmentTitle = "Jaimi C1.62";
#endif

    //Functions to be used in mainwindow.cpp
    void setupPlayerDemo(bool mode);
    void target_AddIconToPlot (double xCoord, double yCoord, double size, targetItem *item);
    void target_LoadFromFile();
    void target_RemoveAllIconsBelowMap(double traceYCoord);
    void target_ShowIconsOnPlot(bool playSound = false);
    bool target_NewTarget(double x, double y, QCustomPlot *plotPtr);
    void target_NewWaypoint(double Lattitude, double Longitude);
    void target_convertToTarget(waypointItem *item);
    void target_UpdateIconType();
    void target_closeFile();
    void target_MouseClick(QCustomPlot *plotPtr, QMouseEvent* event, bool doubleClick = false);
    void target_selectAllTargets(bool select);
    void target_selectAllWaypoints(bool select);
    void target_selectTarget(int index, bool select);
    void target_selectTarget(targetItem *item, bool select);
    void target_selectWaypoint(waypointItem *item, bool select, bool addToRoute = 1);
    void target_saveSnippedImage(QString filePath);
    void target_removeSnipBoxes();
    void target_keyPressEvent(QKeyEvent *e);
    void target_clearImagePlot();
    void target_updateAltPlotPosition();
    void setupInterface();
    void plot_configureUi(QCustomPlot *customPlot);
    void format_preparePlots();
    void format_showPanel(bool show);
    void plot_data_addColorMap();
    void Tcp_sendCommand(quint8 cmdType, quint32 on_off, quint32 range);
    void resetPlotLists();
    void connectSignals_Slots();
    void plot_data_setParameters();
    void xtf_recordPingHeader();
    void xtf_recordPingChHeader();
    void xtf_recordFileHeader();
    void xtf_recordChHeader();
    bool Tcp_collectData(int size);
    void ui_resetRangeButtons();
    void data_hashMap_update();
    void plot_updateLfHfLegend(QString text, bool replotRequired);
    void plot_updateLfHfZoomLegendPosition();
    void plot_showUpdatedZoomLegends();
    void plot_icons_updateCompassIcon();
    QByteArray data_simulateCreateBuffer();

    void gainSlider_setValueLf(int value);
    void gainSlider_setValueHf(int value);
    void gainSlider_setValueDepth(int value);
    void gainSlider_setValueDigital(int value);
    int xtf_dataProcess();
    int xtf_readFile(char *data_pointer, int dataType, int *BytesPointer);
    void xtf_writePing();
    void xtf_writeFileHeader();
    bool bottomTrack_addTrackedPoint(int mode);
    void bottomTrack_clearQueue();
    bool bottomTrack_calculate(QList<quint16> list, int PlotType = 0);
    void bottomTrack_filter(QList<quint16> *list, int size, int multiplier);
    void bottomTrack_differentiate(QList<quint16> *list);
    void bottomTrack_applyProbability(QList<quint16> *list, int kalmanInt);
    void bottomTrack_calibrate(QList<quint16> *list);
    void bottomTrack_plotCurve(QList<quint16> *list, int curveNum);
    QList<qint16> bottomTrack_convertToIntList(QList<quint16> *list);
    QList<quint16> bottomTrack_convertToUIntList(QList<qint16> *list);
    QList<quint16> bottomTrack_findPeaks(QList<quint16> data);
    void bottomTrack_reducePeaksList(QList<quint16> data, QList<quint16> *peaks);
    int  bottomTrack_findBestPeak(QList<quint16> *peaks, int lastPeakPosition);
    void Gnss_createTrackPointFile();
    void Gnss_writeTrackPoints();
    void Gnss_readGpxFile();
    void Gnss_saveGpsPoints();
    int Gnss_readGpsPointsFromFile();
    void Gnss_determineSpeed();
    void Gnss_calcDistanceAndBearing (double LatOld, double LonOld, double LatNew, double LonNew, double *distance, double *bearing);
    void readConfig();
    void fetchUser();
    void setRecordedRange(int range);
    int getBoadSpeedIndex(float speed);
    void format_setPlotSizes();
    void format_updateOtherSettings();
    void format_updatePlotSizes();
    void format_enableGnssFeatures(bool enable);
    void format_showDepthPlot(bool showing);
    void format_showHfPlot(bool open);
    void format_showLfPlot(bool open);
    void format_showSensorGraph(bool open);
    void format_showGpsPlot(bool show);
    void plot_configureYLabels();
    void plot_configureXLabels();
    void plot_setCursor(int state = -1);
    void player_nextColorMap();
    QList<quint16> compressList(QList<quint16> list);
    int data_copy(void * __restrict__ variable, char *data_pointer, int NumChar, int *NumChar2);
    int Jas_readPingHeader(char *data_pointer, quint32 packetID, int *BytesPointer);
    void plot_data_checkToAddNewMap(void);
    void plot_range_moveRanges();
    void player_playFromPing (int pingStart);
    void player_skipPings (int pingsToSkip);
    void player_showSegment (int pingStart, int pingEnd);
    int player_findLastPing ();
    void Chart_ReadTiffFile();
    int Enc_ReadString (char *variable, char *data_pointer, int *NumChar2);
    int Enc_ReadString2 (char *variable, char *data_pointer, int *NumChar2);
    void setup_buttonsAndTools();
    void status_updateStatusBar (int Ping, QString Rtc, double Depth, double Temperature, double Roll, double Pitch, double Yaw, double Altitude, double heading);
    void status_resetStatusBar();
    void status_updateAltLabel();
    void plotTiffImage();
    int Gnss_connectUsbNmea();
    double plot_range_determineYRange(double xRange, double width, double height);
    void plot_range_unstretchLfHfPlots();

    void plot_offset_verticalShiftUp();
    void plot_offset_verticalShiftDown();

    void plot_data_changeGainOnLf(double newMainGainPort, double newMainGainStbd);
    void plot_data_changeGainOnHf(double newMainGainPort, double newMainGainStbd);
    void plot_data_changeGainOnDepth(double newMainGain);
    void plot_data_showGap(bool show);
    void plot_data_showGap2(bool show);
    void plot_updateLfHfPlots();
    void plot_offset_zoomResetPlots();
    void plot_offset_zoomResetLf();
    void plot_offset_zoomResetHf();

    void target_ShowImageAndBox();
    void target_CopyImageBoxToPlot(QCustomPlot *plotPtr);
    void target_UpdatePlot(QCustomPlot *plotPtr, QMouseEvent* event);
    void target_ImportWaypoints(QString sonarFileName);

    void crop_MouseClick(QCustomPlot *plotPtr, QMouseEvent* event);
    void crop_UpdatePlot(QCustomPlot *plotPtr, QMouseEvent* event);
    void crop_UpdateMarkers(double x, double y);

    //Thread functions
    void Jas_dataProcess(char *data_pointer, quint32 packetID, int *BytesPointer);
    void Enc_readFile();

    void plot_icons_resizeBoatIcon();
    void measuringTape_SelectItem(int itemRow);
    void measuringTape_mouseClick(QCustomPlot *plotPtr, QMouseEvent* event);
    void measuringTape_UpdatePlot(QCustomPlot *plotPtr, QMouseEvent* event);
    void measuringTape_DeleteAllItems();
    void measuringTape_RemoveItemsFromPlot(double traceYCoord);
    void measuringTape_UndoLastMeasure();

    void Chart_Zoom();
    void Chart_UnstretchPlot();
    void Gnss_updateButtonState(bool buttonActive);

    void Gnss_Update(/*NavigationSettings::GPSINFO GpsData*/);

    void depthAlarm_start();
    bool recorder_checkState();

    struct chart_object {
        QCPItemPixmap *chartPixmap = nullptr;
        QString filePath;
        QFile tiffFile;

        double maxLatitude = 0; // Y_max, Y_topLeft
        double maxLongitude = 0; // X_Max, X_bottomRight
        double minLatitude = 0; // Y_Min, Y_bottomRight
        double minLongitude = 0; // X_Min, X_topLeft

        bool originalChart = 1;
    };

    struct time_object {
        short year = 0;
        short month = 0;
        short day = 0;
        short hour = 0;
        short minute = 0;
        short second = 0;
        int millisecond = 0;
    };
    time_object currentRtcTime;

    NavigationSettings::GPSINFO GpsItems, GpsTmp;

    struct MSINFO {
        int Hour = 0;
        int Minute = 0;
        int Second = 0;
        int Millisecond = 0;

        int CompassX = 0;
        int CompassY = 0;
        int CompassZ = 0;

        int AccelerometerX = 0;
        int AcceleromoterY = 0;
        int AccelerometerZ = 0;

        double Pitch = 0;
        double Roll = 0;

        int GyroX = 0;
        int GyroY = 0;
        int GyroZ = 0;
    };
    MSINFO MotionItems, MotionTmp;

    struct text_struct {
        double yCoord = 0;
        QCPItemText* item = nullptr;
    };

    int recentLfRange = 0, recentHfRange = 0;
    int lfRangeList[4] = {30, 60, 120, 240}, hfRangeList[4] = {30, 60, 120, 240};

    QFuture<void> encChartThreadProgress;
    QVector <text_struct> item_text;

    QCPItemRect *LfPlotViewBox = nullptr, *HfPlotViewBox = nullptr;

    QTimer *timer;
    QTime elTime;

    struct BOTTOM_TRACKING_INFO {
        QQueue<int> depth_queue_port, depth_queue_stbd;
        double depth_port = 1, depth_stbd = 1;
        quint16 sampleNum_port = 1, sampleNum_stbd = 1;
        int bottomTrackingCounter = 0;

        QQueue<int> signal_queue;
        QTimer signalTimer;
        int signalState = 0;
        int avg_signalStrength = 0;
    };
    BOTTOM_TRACKING_INFO track;

    QByteArray tcpBuffer = nullptr;

    //Creates instances of classes which will be used in the mainwindow
    //The 'this' keyword makes them children of the mainWindow, meaning
    //  they won't show up in the taskbar
    graphDialog *graphWindowInstance = new graphDialog(this);
    format *Format = new format(this);
    helpDialog *helpDialogInstance = new helpDialog(this);
    chartdialog *chartDialogInstance = new chartdialog(this);
    converterdialog *converterDialogInstance = new converterdialog(this);
    CreateFlag *flagInstance = new CreateFlag(this);
    userMenu *userWindowInstance = new userMenu(this);
    DevWindow *devWindowInstance = new DevWindow(this);
    gpsMap *ChartWindowInstance = new gpsMap(this);
    SystemSettings *systemSettingInstance = new SystemSettings(this);
    NavigationSettings *navSettingInstance = new NavigationSettings(this);
    TvgWindow *tvgWindowInstance = new TvgWindow(this);
    DepthAlarmWidget *depthAlarm = new DepthAlarmWidget();
    CroppingTool *cropTool = new CroppingTool();
    PlotTools *tools = new PlotTools();
//    GainSettings *gain = new GainSettings;

    //The windows will show up in the taskbar individually
    //     HighFreqWindow *hfWindowInstance = new HighFreqWindow();
    //     DepthPlotWindow *depthWindowInstance = new DepthPlotWindow();
    //     NavSensorWindow *navWindowInstance = new NavSensorWindow();
    //     TcpErrorDialog *tcperrWindowInstance = new TcpErrorDialog();
    //     ConnectError *conerrInstance = new ConnectError();
    //     graphDialog *graphWindowInstance = new graphDialog();
    //     dataRecorder *dataRecorderInstance = new dataRecorder();
    //     CreateFlag *flagInstance = new CreateFlag();
    //     userMenu *userWindowInstance = new userMenu();//sabbir
    //     DevWindow * devWindowInstance = new DevWindow();
    //     SystemSettings *systemSettingInstance =new SystemSettings();

    // Flags
    struct GUI_FLAGS {
        bool showDepthTextFlag = false;
        bool graphDialogActive = false;
        bool measuringTapeActive = false;
        bool shadowToolActive = false;
        bool EdgeAlignActive = true;
        bool TrackPositionToSonar = false;
        bool permanentZoomBox = false;
        bool compassIconActive = true;

        bool showDepthText = false;
        bool tmpPlotHF = false,         tmpPlotAngle = false;
        bool showStatusBar = false,     showTargetWidget = true;
        bool showBottomTracking = true; // Use to show/hide bottom tracking graph
        bool showGap = false; // Use Bottom Tracking to hide gap
        bool PlotAngleScanColorMap = true;
        bool PlotAngleScan = true; // Is the selected file/firmware for anglescan?
        bool PlotHfSidescan = true; // Is the selected file/firmware for dual frequency sidescan?

        int  mouseClick = 0; // Uses bits for indication: 1st bit for click, 2nd bit is LF, 3rd bit is HF, 4th bit is Depth, and 5th bit is Sensor Plot
        bool ethernetActive = true;
        bool tvgActive = true;
        bool setupFirstColorMap = false;
        bool highRangeChange = false;
        bool filterOn = false;
        bool compassClicked = true;
        bool readyToTransmit = false;
        bool toolsEnabled = false;
        bool targetToolActive = false, waypointToolActive = false;
        bool useDualFrequency = true;
        bool ctrlKeyPressed = false,    shiftKeyPressed = false;

        bool mousePressedFlagLf = false,    mousePressedFlagHf = false;
        bool zoomAroundBoatLf = true,       zoomAroundBoatHf = true;
        bool zoomLfFixed = true,            zoomHfFixed = true;

        bool recordData = false;
        bool recordXtfFormat = false;
        bool convertToXtf = false;
        bool conversionFinished = 0;

        bool depthAlarmItemSelected = false;
        bool depthAlarmItemActive = false;

        int targetIconType = 0; // 0 = red, 1 = yellow, 2 = blue
        bool targetAutoplay = true;
        bool targetIgnoreSelectionChange = false;
        bool cursorHidden = false;

        //Thread variables
        // 'Volatile' means the compiler will expect the variable to be changed elsewhere
        volatile bool thread_middlePoint = false;
        volatile bool thread_rangeDone = false;
    };
    GUI_FLAGS flag;

    struct GUI_STATE {
        int GUI_MODE = 1; // 0 is simulation mode, 1 is real-time mode, 2 is playback mode, 3 is covert to XTF mode
        int PLAY_MODE = 0; // 0 is not available, 1 is idle, 2 is loading, 3 is playing, 4 is paused, 5 is wait to continue, 6 is end of file, 7 is continuous mode
        int RT_MODE = 0; //used to keep track if connected to the board//
        int SIM_MODE = 0;
        int GNSS_MODE = 0; // 0 is not connected, 1 is validation stage, 2 is connected, 3 is playing a GPS file
    };
    GUI_STATE state;

    QFile EncFile;
    QFile trkptGPX;

    struct SONAR_RECORDER {
        QFile sonarFile;
        QFile targetFile;
        QFile GPSFile;

        QString recPath;
        QString fileName;
        QByteArray buffer, XtfBuffer;
        QByteArray xtfFileData;
    };
    SONAR_RECORDER recorder;

    struct SONAR_PLAYER {
        QFile sonarFile;
        QFile GPSFile;
        QFile targetFile;
        QString openPath;
        QString openedFileName;

        QByteArray fileBuffer = nullptr;
        char *dataPtr = nullptr;
        double fileSize;
        int filePos = 0;
        int bufferPos = 0;

        bool typeJeta;
        bool typeXtf;
    };
    SONAR_PLAYER player;

    chart_object gpsChart;

    QCPItemPixmap *towfishArrow, *boatArrow;
    QPixmap arrowRotatedT, arrowRotatedB;

    QSqlDatabase configDB1, configDB2;

    QSqlDatabase myUserDb;
    QByteArray starboardSide1_bytes, portSide1_bytes,starboardSide2_bytes, portSide2_bytes;

    QTime currentTime;
    QDate currentDate;

    int traceCounter1 = 0;
    int diffTime = 0;
    int diffMili = 0;
    int pingCounter = 0; //used to keep track of pings for recording
    int compressionFactor = 1;
    int compressionFactor360 = 1, compressionFactor480 = 1;
    int realSize = 60;
    int filterStartTrace;
    double noiseThresh = 0;

//    struct GAIN_SETTINGS {
//        double tvg_ALfP=15, tvg_BLfP=10, tvg_CLfP=0;
//        double tvg_ALfS=15, tvg_BLfS=10, tvg_CLfS=0;
//        double tvg_AHfP=15, tvg_BHfP=10, tvg_CHfP=0;
//        double tvg_AHfS=15, tvg_BHfS=10, tvg_CHfS=0;
//        double tvg_maxLf, tvg_maxHf;
//        double tvgLowFrequency = 0, tvgHighFrequency = 0;
//        double starGain = 1, portGain = 1;
//        double starGainHF = 1;
//        double portGainHF = 1;
//        double C_lfOffset = 50;
//        double C_hfOffset = 50;
//        int DigitalGainIndex = 0;
//    };
//    GAIN_SETTINGS gain;

    struct PLOT_SETTINGS {
        int plotRate = 40;
        int yawRunningLength = 5;
        double cableLength = 15;
        int averageDepthQueueLength = 20; //May not be used/needed
        double boatSpeed = 2;
        double speed_of_sound = 1500;
        double convert_unit = 1;
        int numOfTraces = 500;

        // Depth Plot
        int globalDepth = 0;
        int nValue;
        double maxAlt = 0;
        bool projectTargets = false;

        double zoomStepLF = 0;
        double zoomStepHF = 0;
        double zoomStepDepthSense = 0;
        int shiftCounterLf = 0;
        int shiftCounterHf = 0;
        int shiftCounterDepthSensor = 0;
        double zoomMagnitude_lf = 1;
        double zoomMagnitude_hf = 1;

        bool lfHfIndependent = 0;
        int wayPointBtnState = 3;
        bool showAltDepth = false; // true = Show Altitude and Depth, false = show Altitude only
//        int lfUpShiftCounter=0, lfDownShiftCounter=0, hfUpShiftCounter=0, hfDownShiftCounter=0, depthSensorUpShift=0, depthSensorDownShift=0;
    };
    PLOT_SETTINGS setting;

    // Plot items
    QCPItemStraightLine *line;
    QCPItemStraightLine *line_depthTrace;
    Crosshair crosshair_lf, crosshair_hf;
    QCPItemLine *line_depthBoat;
    QCPTextElement *legendText;
    QCPTextElement *legendTextHf;
    QCPItemText *depthLegend = nullptr, *depthLegend2, *customPlotLfLegend=nullptr, *customPlotHfLegend=nullptr;
    QCPItemPixmap *lfLegendIcon, *hfLegendIcon;
    QCPItemPixmap *lfBoatPixmap, *hfBoatPixmap, *depthBoatPixmap;
    QCPItemPixmap *lfCompassPixmap, *hfCompassPixmap;
    QCustomPlot *customPlotHF = nullptr;
    QCustomPlot *customPlotLF = nullptr;
    QCustomPlot *depthPlot = nullptr;
    QCPItemPixmap *Target;
    // Color Map Related
    QCPColorMap *colorMap = nullptr;
    QCPColorMap *depthMap;
    QCPColorMap *colorMapHF;
    QQueue <QCPColorMap*> colorMapNum_queue;
    QQueue <QCPColorMap*> depthMapNum_queue;
    QQueue <QCPColorMap*> colorMapHFNum_queue;
    QCPColorScale *colorScale;
    QCPColorScale *colorScaleHF;
    QCPColorScale *newDepthcolorScale;

    SonarPing *ping;
    GnssController *gnssController = nullptr;

    // Status bar
    QProgressBar *progressBar;
    QLabel *avg_label, *tem_label, *ping_label, *time_label, *depth_label;
    QLabel *fileSize_lbl, *file_Name, *rollValue, *pitchValue, *yawValue;

    struct GpsPoint {
        double Lat = -500;
        double Lon = -500;
        double Dir = 0;
    };
    PingMap dataMap;

    int wdth_r = 0, height_r = 0;
    QFile convertXTF;
    QList<QString> jasList, xtfList;
    int fileCounter, fileConverterIndex=0;

    QList<int> counterList;

    int targetCounter = 0;

    libusb_device_handle *dev_handle; //a device handle

    typedef struct {
        unsigned int SyncWord = 0x8000FFFF;
        unsigned int MessageType = 0x64;
        unsigned int PacketSize = 0x108;

        unsigned int EndSync = 0x0000FACE;

        unsigned int HeaderSize = 0x0080;
        unsigned int Time1 = 0;
        unsigned int Time2 = 0;
        unsigned int Firmware = 0xFFFFFFFF;
        unsigned short LfValue = 100;
        unsigned short HfValue = 200;

        unsigned char AngleOrientation = 90;
        unsigned char BytesPerSample = 2;
        unsigned short LfSamples = 1000;
        unsigned short HfSamples = 2000;

        unsigned short LfRange = 10;
        unsigned short HfRange = 10;

        unsigned int PingNumber = 10;

        float Roll = 0;
        float Pitch = 0;
        float Yaw = 0;
        float Heading = 0;
        float Altitude = 0;
        float Depth = 0;
        float Temperature = 0;
        char nullByte = 0;
        int nullInt = 0;
    } JAS_PINGHEADER;

    // S-57 file format
    struct DDR_LEADER { // 24 Bytes
        char Record_Length[6] = "";
        char Indentifiers[8] = "";  // Should be "3LE1 09"
        char FieldAreaBaseAddress[6] = ""; // Start address of field area (number of bytes inleader and directory)
        char Indicator[4] = ""; // Should be " ! "
        char sizeFieldLength = '0'; // Size of field length field
        char sizeFieldPosition = '0'; // Size of field position field
        char Reserved = '0'; // Should read '0'
        char sizeFieldTag = '4'; // Should be '4' for S-57
    };
    struct DR_LEADER { // 24 Bytes
        char Record_Length[6] = "";
        char Indentifiers[8] = "";  // Should be " D     "
        char FieldAreaBaseAddress[6] = ""; // Start address of field area (number of bytes inleader and directory)
        char Indicator[8] = ""; // Should be "   "
        char sizeFieldLength = '0'; // Size of field length field
        char sizeFieldPosition = '0'; // Size of field position field
        char Reserved = '0'; // Should read '0'
        char sizeFieldTag = '4'; // Should be '4' for S-57
    };
    struct TIFF_LEADER{
        char byteOrder[3];
        char checkTiffFile[3];
        char offSet[5];
        char noOfTags[3];
        char tagType[3];
        char fieldType[3];
        char count[5];
        char valueOffset[5];
        //char value[3];

        QList<int>tagTypeList;
        QList<int>fieldTypeList;
        QList<int>countList;
        QList<int>valueOffSetList;
        QList<int>geoTiffOfset;
        QList<int>rangeList;
        QList<int>valueList;
    };
    struct DDR_DATA_DESCRIPTION {
        char DataStructure; // "1"  - linear structure  "2"  - multi-dimensional structure
        char DataType; // "0"  - character string, "1"  - implicit point (integer), "5"  - binary form, "6"  - mixed data types
        char AuxiliaryControls[3] = ""; // Should be "00"
        char PrintableGraphics[3] = ""; // Should be ";&"
        char EscapeSequence[3] = ""; // lexical level 0- "   ", lexical level 1- "-A ", lexical level 2- "%/A"
        char FieldName[100];
        char UT1; // 0x1F
        char ArrayDescriptor[100];
        QStringList ArrayDesc;
        char UT2; // 0x1F
        char FormatControls[100];
        QStringList FormatChar;
        char FT; // 0x1E
        QString parent;
    };
    struct DDR_DIRECTORY {
        // Fill char arrays with null pointers
        char FieldTag[10] = "\0\0\0\0\0\0\0\0";
        char FieldLength[10] = "\0\0\0\0\0\0\0\0";
        char FieldPosition[10] = "\0\0\0\0\0\0\0\0";

        QString parent;
        DDR_DATA_DESCRIPTION Data;
    };
    struct DDR_FIELD_CONTROL {
        char Controls[10] = "\0\0\0\0\0\0\0\0"; // Should be "0000;&   "
        char UT; // 0x1F
        char FieldTagPair1[5] = "   "; // List of field tag pairs
        char FieldTagPair2[5] = "   "; // List of field tag pairs
        char FT; // 0x1E
    };
    struct DR_FIELD {
        //         char FieldArea[2000] = {'\0'};
        char FT; // 0x1E
        QVector<QString> FieldAreaEntries;
    };
    struct DR_DIRECTORY {
        // Fill char arrays with null pointers
        char FieldTag[10] = "\0\0\0\0\0\0\0\0";
        char FieldLength[10] = "\0\0\0\0\0\0\0\0";
        char FieldPosition[10] = "\0\0\0\0\0\0\0\0";

        QString parent;
        QVector<QString> Data;
        QVector <int> ObjectCode;
        bool LandArea = 0;
        bool WaterArea = 0;
        bool ChartArea = 0;
    };
    struct DR_ENTRY {
        QVector <DR_DIRECTORY> DrEntry;
    };
    struct DSID_PARAMETERS {
        // DSID
        QString RCNM;
        QString RCID;
        QString EXPP;
        QString INTU;
        QString DSNM;
        QString EDTN;
        QString UPDN;
        QString UADT;
        QString ISDT;
        QString STED;
        QString PRSP;
        QString PSDN;
        QString PRED;
        QString PROF;
        QString AGEN;
        QString COMT;

        // DSSI
        QString DSTR;
        QString AALL;
        QString NALL;
        QString NOMR;
        QString NOCR;
        QString NOGR;
        QString NOLR;
        QString NOIN;
        QString NOCN;
        QString NOED;
        QString NOFA;

    };
    struct DSPM_PARAMETERS {
        // DSPM
        QString RCNM;
        QString RCID;
        QString HDAT;
        QString VDAT;
        QString SDAT;
        QString CSCL;
        QString DUNI;
        QString HUNI;
        QString PUNI;
        QString COUN;
        QString COMF;
        QString SOMF;
        QString COMT;
    };
    struct SPATIAL_FEATURES {
        QString TagName;
        QString TagPointer;
        QString VrptTags1;
        QString VrptTags1B;
        QString VrptTags2;
        QString VrptTags2B;
        QString StartNodePointer;
        QString EndNodePointer;
        QList <int> ObjectCode;
        QString parent1;
        QString parent2;

        QString Node1Ptr;
        QString Node2Ptr;

        QString Node1Dtn;
        QString Node2Dtn;

        bool Land = 0;
        bool Water = 0;
        bool Chart = 0;

        QVector<QString> Points;
    };
    struct FEATURE_RECORD {
        QString TagName;
        QString TagPointer;
        int Primitive = 0;
        int ObjectCode;
        QString parent1;
        QString parent2;

        QVector<QString> Pointers;
        QVector <int> PointerIndexList;

        QVector<QString> AttributeList;

        QVector <int> AreaFR;
        QVector <int> AreaEI;
    };
    struct FEATURE_POINTS {
        QList <double> x;
        QList <double> y;
        int Exterior;
        int ObjectCode;
        bool Area = 0;

        QVector<QString> AttributeValues;
        QVector <int> AttributeCodes;
        double depth1 = 0;
        double depth2 = 0;
        QCPCurve *curvePtr = nullptr;
    };
    struct VRPT_NODE {
        QString Pointer;
        int ORNT;
        int USAG;
        int TOPI;

        double X;
        double Y;
    };
    struct VRPT_RECORD {
        QString TagName;
        QString TagPointer;
        VRPT_NODE Node1;
        VRPT_NODE Node2;

        QVector <double> X;
        QVector <double> Y;
    };
    struct SG3D_RECORD {
        QString TagName;
        QString TagPointer;
        QString parent1;
        QString parent2;
        bool convertedToDouble = 0;

        QVector <double> X;
        QVector <double> Y;
        QVector <double> Z;
        QVector <QCPItemText*> textItem;
    };
    struct LIST_SPATIAL_OBJECTS {
        QVector <FEATURE_POINTS> WaterArea;
        QVector <FEATURE_POINTS> ShallowArea;
        QVector <FEATURE_POINTS> DeepArea;
        QVector <FEATURE_POINTS> LandArea;
        QVector <FEATURE_POINTS> SeaArea;
        QVector <FEATURE_POINTS> LakeArea;
        QVector <FEATURE_POINTS> BuildingArea;

        QVector <FEATURE_POINTS> WaterContour;
        QVector <FEATURE_POINTS> LandContour;
        QVector <FEATURE_POINTS> MapOutline;
    };
    struct OBJECT_POINT {
        double xCoord;
        double yCoord;
        int ObjectCode;
        QVector<QString> AttributeValues;
        QVector <int> AttributeCodes;

        QCPAbstractItem *item = nullptr;
        QCPItemText *text = nullptr;
    };

    QVector <SG3D_RECORD> SG3D_List;
    QVector <SPATIAL_FEATURES> SG2D_List;
    QVector <FEATURE_POINTS> SpatialFeatures_List;
    QVector <SPATIAL_FEATURES> SoundingFeatures_List;
    QVector <VRPT_RECORD> vrptRecord_List;
    LIST_SPATIAL_OBJECTS SpatialObjectTypes_List;
    QVector <OBJECT_POINT> SinglePointObjects_List;
    void Enc_CreateVrptList();

    typedef struct {
        unsigned long P[16 + 2] = {
            0x243F6A88L, 0x85A308D3L, 0x13198A2EL, 0x03707344L,
            0xA4093822L, 0x299F31D0L, 0x082EFA98L, 0xEC4E6C89L,
            0x452821E6L, 0x38D01377L, 0xBE5466CFL, 0x34E90C6CL,
            0xC0AC29B7L, 0xC97C50DDL, 0x3F84D5B5L, 0xB5470917L,
            0x9216D5D9L, 0x8979FB1BL
        };
        unsigned long S[4][256]= { {
                                       0xd1310ba6l, 0x98dfb5acl, 0x2ffd72dbl, 0xd01adfb7l,
                                       0xb8e1afedl, 0x6a267e96l, 0xba7c9045l, 0xf12c7f99l,
                                       0x24a19947l, 0xb3916cf7l, 0x0801f2e2l, 0x858efc16l,
                                       0x636920d8l, 0x71574e69l, 0xa458fea3l, 0xf4933d7el,
                                       0x0d95748fl, 0x728eb658l, 0x718bcd58l, 0x82154aeel,
                                       0x7b54a41dl, 0xc25a59b5l, 0x9c30d539l, 0x2af26013l,
                                       0xc5d1b023l, 0x286085f0l, 0xca417918l, 0xb8db38efl,
                                       0x8e79dcb0l, 0x603a180el, 0x6c9e0e8bl, 0xb01e8a3el,
                                       0xd71577c1l, 0xbd314b27l, 0x78af2fdal, 0x55605c60l,
                                       0xe65525f3l, 0xaa55ab94l, 0x57489862l, 0x63e81440l,
                                       0x55ca396al, 0x2aab10b6l, 0xb4cc5c34l, 0x1141e8cel,
                                       0xa15486afl, 0x7c72e993l, 0xb3ee1411l, 0x636fbc2al,
                                       0x2ba9c55dl, 0x741831f6l, 0xce5c3e16l, 0x9b87931el,
                                       0xafd6ba33l, 0x6c24cf5cl, 0x7a325381l, 0x28958677l,
                                       0x3b8f4898l, 0x6b4bb9afl, 0xc4bfe81bl, 0x66282193l,
                                       0x61d809ccl, 0xfb21a991l, 0x487cac60l, 0x5dec8032l,
                                       0xef845d5dl, 0xe98575b1l, 0xdc262302l, 0xeb651b88l,
                                       0x23893e81l, 0xd396acc5l, 0x0f6d6ff3l, 0x83f44239l,
                                       0x2e0b4482l, 0xa4842004l, 0x69c8f04al, 0x9e1f9b5el,
                                       0x21c66842l, 0xf6e96c9al, 0x670c9c61l, 0xabd388f0l,
                                       0x6a51a0d2l, 0xd8542f68l, 0x960fa728l, 0xab5133a3l,
                                       0x6eef0b6cl, 0x137a3be4l, 0xba3bf050l, 0x7efb2a98l,
                                       0xa1f1651dl, 0x39af0176l, 0x66ca593el, 0x82430e88l,
                                       0x8cee8619l, 0x456f9fb4l, 0x7d84a5c3l, 0x3b8b5ebel,
                                       0xe06f75d8l, 0x85c12073l, 0x401a449fl, 0x56c16aa6l,
                                       0x4ed3aa62l, 0x363f7706l, 0x1bfedf72l, 0x429b023dl,
                                       0x37d0d724l, 0xd00a1248l, 0xdb0fead3l, 0x49f1c09bl,
                                       0x075372c9l, 0x80991b7bl, 0x25d479d8l, 0xf6e8def7l,
                                       0xe3fe501al, 0xb6794c3bl, 0x976ce0bdl, 0x04c006bal,
                                       0xc1a94fb6l, 0x409f60c4l, 0x5e5c9ec2l, 0x196a2463l,
                                       0x68fb6fafl, 0x3e6c53b5l, 0x1339b2ebl, 0x3b52ec6fl,
                                       0x6dfc511fl, 0x9b30952cl, 0xcc814544l, 0xaf5ebd09l,
                                       0xbee3d004l, 0xde334afdl, 0x660f2807l, 0x192e4bb3l,
                                       0xc0cba857l, 0x45c8740fl, 0xd20b5f39l, 0xb9d3fbdbl,
                                       0x5579c0bdl, 0x1a60320al, 0xd6a100c6l, 0x402c7279l,
                                       0x679f25fel, 0xfb1fa3ccl, 0x8ea5e9f8l, 0xdb3222f8l,
                                       0x3c7516dfl, 0xfd616b15l, 0x2f501ec8l, 0xad0552abl,
                                       0x323db5fal, 0xfd238760l, 0x53317b48l, 0x3e00df82l,
                                       0x9e5c57bbl, 0xca6f8ca0l, 0x1a87562el, 0xdf1769dbl,
                                       0xd542a8f6l, 0x287effc3l, 0xac6732c6l, 0x8c4f5573l,
                                       0x695b27b0l, 0xbbca58c8l, 0xe1ffa35dl, 0xb8f011a0l,
                                       0x10fa3d98l, 0xfd2183b8l, 0x4afcb56cl, 0x2dd1d35bl,
                                       0x9a53e479l, 0xb6f84565l, 0xd28e49bcl, 0x4bfb9790l,
                                       0xe1ddf2dal, 0xa4cb7e33l, 0x62fb1341l, 0xcee4c6e8l,
                                       0xef20cadal, 0x36774c01l, 0xd07e9efel, 0x2bf11fb4l,
                                       0x95dbda4dl, 0xae909198l, 0xeaad8e71l, 0x6b93d5a0l,
                                       0xd08ed1d0l, 0xafc725e0l, 0x8e3c5b2fl, 0x8e7594b7l,
                                       0x8ff6e2fbl, 0xf2122b64l, 0x8888b812l, 0x900df01cl,
                                       0x4fad5ea0l, 0x688fc31cl, 0xd1cff191l, 0xb3a8c1adl,
                                       0x2f2f2218l, 0xbe0e1777l, 0xea752dfel, 0x8b021fa1l,
                                       0xe5a0cc0fl, 0xb56f74e8l, 0x18acf3d6l, 0xce89e299l,
                                       0xb4a84fe0l, 0xfd13e0b7l, 0x7cc43b81l, 0xd2ada8d9l,
                                       0x165fa266l, 0x80957705l, 0x93cc7314l, 0x211a1477l,
                                       0xe6ad2065l, 0x77b5fa86l, 0xc75442f5l, 0xfb9d35cfl,
                                       0xebcdaf0cl, 0x7b3e89a0l, 0xd6411bd3l, 0xae1e7e49l,
                                       0x00250e2dl, 0x2071b35el, 0x226800bbl, 0x57b8e0afl,
                                       0x2464369bl, 0xf009b91el, 0x5563911dl, 0x59dfa6aal,
                                       0x78c14389l, 0xd95a537fl, 0x207d5ba2l, 0x02e5b9c5l,
                                       0x83260376l, 0x6295cfa9l, 0x11c81968l, 0x4e734a41l,
                                       0xb3472dcal, 0x7b14a94al, 0x1b510052l, 0x9a532915l,
                                       0xd60f573fl, 0xbc9bc6e4l, 0x2b60a476l, 0x81e67400l,
                                       0x08ba6fb5l, 0x571be91fl, 0xf296ec6bl, 0x2a0dd915l,
                                       0xb6636521l, 0xe7b9f9b6l, 0xff34052el, 0xc5855664l,
                                       0x53b02d5dl, 0xa99f8fa1l, 0x08ba4799l, 0x6e85076al
                                   },
                                   {
                                       0x4b7a70e9l, 0xb5b32944l, 0xdb75092el, 0xc4192623l,
                                       0xad6ea6b0l, 0x49a7df7dl, 0x9cee60b8l, 0x8fedb266l,
                                       0xecaa8c71l, 0x699a17ffl, 0x5664526cl, 0xc2b19ee1l,
                                       0x193602a5l, 0x75094c29l, 0xa0591340l, 0xe4183a3el,
                                       0x3f54989al, 0x5b429d65l, 0x6b8fe4d6l, 0x99f73fd6l,
                                       0xa1d29c07l, 0xefe830f5l, 0x4d2d38e6l, 0xf0255dc1l,
                                       0x4cdd2086l, 0x8470eb26l, 0x6382e9c6l, 0x021ecc5el,
                                       0x09686b3fl, 0x3ebaefc9l, 0x3c971814l, 0x6b6a70a1l,
                                       0x687f3584l, 0x52a0e286l, 0xb79c5305l, 0xaa500737l,
                                       0x3e07841cl, 0x7fdeae5cl, 0x8e7d44ecl, 0x5716f2b8l,
                                       0xb03ada37l, 0xf0500c0dl, 0xf01c1f04l, 0x0200b3ffl,
                                       0xae0cf51al, 0x3cb574b2l, 0x25837a58l, 0xdc0921bdl,
                                       0xd19113f9l, 0x7ca92ff6l, 0x94324773l, 0x22f54701l,
                                       0x3ae5e581l, 0x37c2dadcl, 0xc8b57634l, 0x9af3dda7l,
                                       0xa9446146l, 0x0fd0030el, 0xecc8c73el, 0xa4751e41l,
                                       0xe238cd99l, 0x3bea0e2fl, 0x3280bba1l, 0x183eb331l,
                                       0x4e548b38l, 0x4f6db908l, 0x6f420d03l, 0xf60a04bfl,
                                       0x2cb81290l, 0x24977c79l, 0x5679b072l, 0xbcaf89afl,
                                       0xde9a771fl, 0xd9930810l, 0xb38bae12l, 0xdccf3f2el,
                                       0x5512721fl, 0x2e6b7124l, 0x501adde6l, 0x9f84cd87l,
                                       0x7a584718l, 0x7408da17l, 0xbc9f9abcl, 0xe94b7d8cl,
                                       0xec7aec3al, 0xdb851dfal, 0x63094366l, 0xc464c3d2l,
                                       0xef1c1847l, 0x3215d908l, 0xdd433b37l, 0x24c2ba16l,
                                       0x12a14d43l, 0x2a65c451l, 0x50940002l, 0x133ae4ddl,
                                       0x71dff89el, 0x10314e55l, 0x81ac77d6l, 0x5f11199bl,
                                       0x043556f1l, 0xd7a3c76bl, 0x3c11183bl, 0x5924a509l,
                                       0xf28fe6edl, 0x97f1fbfal, 0x9ebabf2cl, 0x1e153c6el,
                                       0x86e34570l, 0xeae96fb1l, 0x860e5e0al, 0x5a3e2ab3l,
                                       0x771fe71cl, 0x4e3d06fal, 0x2965dcb9l, 0x99e71d0fl,
                                       0x803e89d6l, 0x5266c825l, 0x2e4cc978l, 0x9c10b36al,
                                       0xc6150ebal, 0x94e2ea78l, 0xa5fc3c53l, 0x1e0a2df4l,
                                       0xf2f74ea7l, 0x361d2b3dl, 0x1939260fl, 0x19c27960l,
                                       0x5223a708l, 0xf71312b6l, 0xebadfe6el, 0xeac31f66l,
                                       0xe3bc4595l, 0xa67bc883l, 0xb17f37d1l, 0x018cff28l,
                                       0xc332ddefl, 0xbe6c5aa5l, 0x65582185l, 0x68ab9802l,
                                       0xeecea50fl, 0xdb2f953bl, 0x2aef7dadl, 0x5b6e2f84l,
                                       0x1521b628l, 0x29076170l, 0xecdd4775l, 0x619f1510l,
                                       0x13cca830l, 0xeb61bd96l, 0x0334fe1el, 0xaa0363cfl,
                                       0xb5735c90l, 0x4c70a239l, 0xd59e9e0bl, 0xcbaade14l,
                                       0xeecc86bcl, 0x60622ca7l, 0x9cab5cabl, 0xb2f3846el,
                                       0x648b1eafl, 0x19bdf0cal, 0xa02369b9l, 0x655abb50l,
                                       0x40685a32l, 0x3c2ab4b3l, 0x319ee9d5l, 0xc021b8f7l,
                                       0x9b540b19l, 0x875fa099l, 0x95f7997el, 0x623d7da8l,
                                       0xf837889al, 0x97e32d77l, 0x11ed935fl, 0x16681281l,
                                       0x0e358829l, 0xc7e61fd6l, 0x96dedfa1l, 0x7858ba99l,
                                       0x57f584a5l, 0x1b227263l, 0x9b83c3ffl, 0x1ac24696l,
                                       0xcdb30aebl, 0x532e3054l, 0x8fd948e4l, 0x6dbc3128l,
                                       0x58ebf2efl, 0x34c6ffeal, 0xfe28ed61l, 0xee7c3c73l,
                                       0x5d4a14d9l, 0xe864b7e3l, 0x42105d14l, 0x203e13e0l,
                                       0x45eee2b6l, 0xa3aaabeal, 0xdb6c4f15l, 0xfacb4fd0l,
                                       0xc742f442l, 0xef6abbb5l, 0x654f3b1dl, 0x41cd2105l,
                                       0xd81e799el, 0x86854dc7l, 0xe44b476al, 0x3d816250l,
                                       0xcf62a1f2l, 0x5b8d2646l, 0xfc8883a0l, 0xc1c7b6a3l,
                                       0x7f1524c3l, 0x69cb7492l, 0x47848a0bl, 0x5692b285l,
                                       0x095bbf00l, 0xad19489dl, 0x1462b174l, 0x23820e00l,
                                       0x58428d2al, 0x0c55f5eal, 0x1dadf43el, 0x233f7061l,
                                       0x3372f092l, 0x8d937e41l, 0xd65fecf1l, 0x6c223bdbl,
                                       0x7cde3759l, 0xcbee7460l, 0x4085f2a7l, 0xce77326el,
                                       0xa6078084l, 0x19f8509el, 0xe8efd855l, 0x61d99735l,
                                       0xa969a7aal, 0xc50c06c2l, 0x5a04abfcl, 0x800bcadcl,
                                       0x9e447a2el, 0xc3453484l, 0xfdd56705l, 0x0e1e9ec9l,
                                       0xdb73dbd3l, 0x105588cdl, 0x675fda79l, 0xe3674340l,
                                       0xc5c43465l, 0x713e38d8l, 0x3d28f89el, 0xf16dff20l,
                                       0x153e21e7l, 0x8fb03d4al, 0xe6e39f2bl, 0xdb83adf7l
                                   },
                                   {
                                       0xe93d5a68l, 0x948140f7l, 0xf64c261cl, 0x94692934l,
                                       0x411520f7l, 0x7602d4f7l, 0xbcf46b2el, 0xd4a20068l,
                                       0xd4082471l, 0x3320f46al, 0x43b7d4b7l, 0x500061afl,
                                       0x1e39f62el, 0x97244546l, 0x14214f74l, 0xbf8b8840l,
                                       0x4d95fc1dl, 0x96b591afl, 0x70f4ddd3l, 0x66a02f45l,
                                       0xbfbc09ecl, 0x03bd9785l, 0x7fac6dd0l, 0x31cb8504l,
                                       0x96eb27b3l, 0x55fd3941l, 0xda2547e6l, 0xabca0a9al,
                                       0x28507825l, 0x530429f4l, 0x0a2c86dal, 0xe9b66dfbl,
                                       0x68dc1462l, 0xd7486900l, 0x680ec0a4l, 0x27a18deel,
                                       0x4f3ffea2l, 0xe887ad8cl, 0xb58ce006l, 0x7af4d6b6l,
                                       0xaace1e7cl, 0xd3375fecl, 0xce78a399l, 0x406b2a42l,
                                       0x20fe9e35l, 0xd9f385b9l, 0xee39d7abl, 0x3b124e8bl,
                                       0x1dc9faf7l, 0x4b6d1856l, 0x26a36631l, 0xeae397b2l,
                                       0x3a6efa74l, 0xdd5b4332l, 0x6841e7f7l, 0xca7820fbl,
                                       0xfb0af54el, 0xd8feb397l, 0x454056acl, 0xba489527l,
                                       0x55533a3al, 0x20838d87l, 0xfe6ba9b7l, 0xd096954bl,
                                       0x55a867bcl, 0xa1159a58l, 0xcca92963l, 0x99e1db33l,
                                       0xa62a4a56l, 0x3f3125f9l, 0x5ef47e1cl, 0x9029317cl,
                                       0xfdf8e802l, 0x04272f70l, 0x80bb155cl, 0x05282ce3l,
                                       0x95c11548l, 0xe4c66d22l, 0x48c1133fl, 0xc70f86dcl,
                                       0x07f9c9eel, 0x41041f0fl, 0x404779a4l, 0x5d886e17l,
                                       0x325f51ebl, 0xd59bc0d1l, 0xf2bcc18fl, 0x41113564l,
                                       0x257b7834l, 0x602a9c60l, 0xdff8e8a3l, 0x1f636c1bl,
                                       0x0e12b4c2l, 0x02e1329el, 0xaf664fd1l, 0xcad18115l,
                                       0x6b2395e0l, 0x333e92e1l, 0x3b240b62l, 0xeebeb922l,
                                       0x85b2a20el, 0xe6ba0d99l, 0xde720c8cl, 0x2da2f728l,
                                       0xd0127845l, 0x95b794fdl, 0x647d0862l, 0xe7ccf5f0l,
                                       0x5449a36fl, 0x877d48fal, 0xc39dfd27l, 0xf33e8d1el,
                                       0x0a476341l, 0x992eff74l, 0x3a6f6eabl, 0xf4f8fd37l,
                                       0xa812dc60l, 0xa1ebddf8l, 0x991be14cl, 0xdb6e6b0dl,
                                       0xc67b5510l, 0x6d672c37l, 0x2765d43bl, 0xdcd0e804l,
                                       0xf1290dc7l, 0xcc00ffa3l, 0xb5390f92l, 0x690fed0bl,
                                       0x667b9ffbl, 0xcedb7d9cl, 0xa091cf0bl, 0xd9155ea3l,
                                       0xbb132f88l, 0x515bad24l, 0x7b9479bfl, 0x763bd6ebl,
                                       0x37392eb3l, 0xcc115979l, 0x8026e297l, 0xf42e312dl,
                                       0x6842ada7l, 0xc66a2b3bl, 0x12754cccl, 0x782ef11cl,
                                       0x6a124237l, 0xb79251e7l, 0x06a1bbe6l, 0x4bfb6350l,
                                       0x1a6b1018l, 0x11caedfal, 0x3d25bdd8l, 0xe2e1c3c9l,
                                       0x44421659l, 0x0a121386l, 0xd90cec6el, 0xd5abea2al,
                                       0x64af674el, 0xda86a85fl, 0xbebfe988l, 0x64e4c3fel,
                                       0x9dbc8057l, 0xf0f7c086l, 0x60787bf8l, 0x6003604dl,
                                       0xd1fd8346l, 0xf6381fb0l, 0x7745ae04l, 0xd736fcccl,
                                       0x83426b33l, 0xf01eab71l, 0xb0804187l, 0x3c005e5fl,
                                       0x77a057bel, 0xbde8ae24l, 0x55464299l, 0xbf582e61l,
                                       0x4e58f48fl, 0xf2ddfda2l, 0xf474ef38l, 0x8789bdc2l,
                                       0x5366f9c3l, 0xc8b38e74l, 0xb475f255l, 0x46fcd9b9l,
                                       0x7aeb2661l, 0x8b1ddf84l, 0x846a0e79l, 0x915f95e2l,
                                       0x466e598el, 0x20b45770l, 0x8cd55591l, 0xc902de4cl,
                                       0xb90bace1l, 0xbb8205d0l, 0x11a86248l, 0x7574a99el,
                                       0xb77f19b6l, 0xe0a9dc09l, 0x662d09a1l, 0xc4324633l,
                                       0xe85a1f02l, 0x09f0be8cl, 0x4a99a025l, 0x1d6efe10l,
                                       0x1ab93d1dl, 0x0ba5a4dfl, 0xa186f20fl, 0x2868f169l,
                                       0xdcb7da83l, 0x573906fel, 0xa1e2ce9bl, 0x4fcd7f52l,
                                       0x50115e01l, 0xa70683fal, 0xa002b5c4l, 0x0de6d027l,
                                       0x9af88c27l, 0x773f8641l, 0xc3604c06l, 0x61a806b5l,
                                       0xf0177a28l, 0xc0f586e0l, 0x006058aal, 0x30dc7d62l,
                                       0x11e69ed7l, 0x2338ea63l, 0x53c2dd94l, 0xc2c21634l,
                                       0xbbcbee56l, 0x90bcb6del, 0xebfc7da1l, 0xce591d76l,
                                       0x6f05e409l, 0x4b7c0188l, 0x39720a3dl, 0x7c927c24l,
                                       0x86e3725fl, 0x724d9db9l, 0x1ac15bb4l, 0xd39eb8fcl,
                                       0xed545578l, 0x08fca5b5l, 0xd83d7cd3l, 0x4dad0fc4l,
                                       0x1e50ef5el, 0xb161e6f8l, 0xa28514d9l, 0x6c51133cl,
                                       0x6fd5c7e7l, 0x56e14ec4l, 0x362abfcel, 0xddc6c837l,
                                       0xd79a3234l, 0x92638212l, 0x670efa8el, 0x406000e0l
                                   },
                                   {
                                       0x3a39ce37l, 0xd3faf5cfl, 0xabc27737l, 0x5ac52d1bl,
                                       0x5cb0679el, 0x4fa33742l, 0xd3822740l, 0x99bc9bbel,
                                       0xd5118e9dl, 0xbf0f7315l, 0xd62d1c7el, 0xc700c47bl,
                                       0xb78c1b6bl, 0x21a19045l, 0xb26eb1bel, 0x6a366eb4l,
                                       0x5748ab2fl, 0xbc946e79l, 0xc6a376d2l, 0x6549c2c8l,
                                       0x530ff8eel, 0x468dde7dl, 0xd5730a1dl, 0x4cd04dc6l,
                                       0x2939bbdbl, 0xa9ba4650l, 0xac9526e8l, 0xbe5ee304l,
                                       0xa1fad5f0l, 0x6a2d519al, 0x63ef8ce2l, 0x9a86ee22l,
                                       0xc089c2b8l, 0x43242ef6l, 0xa51e03aal, 0x9cf2d0a4l,
                                       0x83c061bal, 0x9be96a4dl, 0x8fe51550l, 0xba645bd6l,
                                       0x2826a2f9l, 0xa73a3ae1l, 0x4ba99586l, 0xef5562e9l,
                                       0xc72fefd3l, 0xf752f7dal, 0x3f046f69l, 0x77fa0a59l,
                                       0x80e4a915l, 0x87b08601l, 0x9b09e6adl, 0x3b3ee593l,
                                       0xe990fd5al, 0x9e34d797l, 0x2cf0b7d9l, 0x022b8b51l,
                                       0x96d5ac3al, 0x017da67dl, 0xd1cf3ed6l, 0x7c7d2d28l,
                                       0x1f9f25cfl, 0xadf2b89bl, 0x5ad6b472l, 0x5a88f54cl,
                                       0xe029ac71l, 0xe019a5e6l, 0x47b0acfdl, 0xed93fa9bl,
                                       0xe8d3c48dl, 0x283b57ccl, 0xf8d56629l, 0x79132e28l,
                                       0x785f0191l, 0xed756055l, 0xf7960e44l, 0xe3d35e8cl,
                                       0x15056dd4l, 0x88f46dbal, 0x03a16125l, 0x0564f0bdl,
                                       0xc3eb9e15l, 0x3c9057a2l, 0x97271aecl, 0xa93a072al,
                                       0x1b3f6d9bl, 0x1e6321f5l, 0xf59c66fbl, 0x26dcf319l,
                                       0x7533d928l, 0xb155fdf5l, 0x03563482l, 0x8aba3cbbl,
                                       0x28517711l, 0xc20ad9f8l, 0xabcc5167l, 0xccad925fl,
                                       0x4de81751l, 0x3830dc8el, 0x379d5862l, 0x9320f991l,
                                       0xea7a90c2l, 0xfb3e7bcel, 0x5121ce64l, 0x774fbe32l,
                                       0xa8b6e37el, 0xc3293d46l, 0x48de5369l, 0x6413e680l,
                                       0xa2ae0810l, 0xdd6db224l, 0x69852dfdl, 0x09072166l,
                                       0xb39a460al, 0x6445c0ddl, 0x586cdecfl, 0x1c20c8ael,
                                       0x5bbef7ddl, 0x1b588d40l, 0xccd2017fl, 0x6bb4e3bbl,
                                       0xdda26a7el, 0x3a59ff45l, 0x3e350a44l, 0xbcb4cdd5l,
                                       0x72eacea8l, 0xfa6484bbl, 0x8d6612ael, 0xbf3c6f47l,
                                       0xd29be463l, 0x542f5d9el, 0xaec2771bl, 0xf64e6370l,
                                       0x740e0d8dl, 0xe75b1357l, 0xf8721671l, 0xaf537d5dl,
                                       0x4040cb08l, 0x4eb4e2ccl, 0x34d2466al, 0x0115af84l,
                                       0xe1b00428l, 0x95983a1dl, 0x06b89fb4l, 0xce6ea048l,
                                       0x6f3f3b82l, 0x3520ab82l, 0x011a1d4bl, 0x277227f8l,
                                       0x611560b1l, 0xe7933fdcl, 0xbb3a792bl, 0x344525bdl,
                                       0xa08839e1l, 0x51ce794bl, 0x2f32c9b7l, 0xa01fbac9l,
                                       0xe01cc87el, 0xbcc7d1f6l, 0xcf0111c3l, 0xa1e8aac7l,
                                       0x1a908749l, 0xd44fbd9al, 0xd0dadecbl, 0xd50ada38l,
                                       0x0339c32al, 0xc6913667l, 0x8df9317cl, 0xe0b12b4fl,
                                       0xf79e59b7l, 0x43f5bb3al, 0xf2d519ffl, 0x27d9459cl,
                                       0xbf97222cl, 0x15e6fc2al, 0x0f91fc71l, 0x9b941525l,
                                       0xfae59361l, 0xceb69cebl, 0xc2a86459l, 0x12baa8d1l,
                                       0xb6c1075el, 0xe3056a0cl, 0x10d25065l, 0xcb03a442l,
                                       0xe0ec6e0el, 0x1698db3bl, 0x4c98a0bel, 0x3278e964l,
                                       0x9f1f9532l, 0xe0d392dfl, 0xd3a0342bl, 0x8971f21el,
                                       0x1b0a7441l, 0x4ba3348cl, 0xc5be7120l, 0xc37632d8l,
                                       0xdf359f8dl, 0x9b992f2el, 0xe60b6f47l, 0x0fe3f11dl,
                                       0xe54cda54l, 0x1edad891l, 0xce6279cfl, 0xcd3e7e6fl,
                                       0x1618b166l, 0xfd2c1d05l, 0x848fd2c5l, 0xf6fb2299l,
                                       0xf523f357l, 0xa6327623l, 0x93a83531l, 0x56cccd02l,
                                       0xacf08162l, 0x5a75ebb5l, 0x6e163697l, 0x88d273ccl,
                                       0xde966292l, 0x81b949d0l, 0x4c50901bl, 0x71c65614l,
                                       0xe6c6c7bdl, 0x327a140al, 0x45e1d006l, 0xc3f27b9al,
                                       0xc9aa53fdl, 0x62a80f00l, 0xbb25bfe2l, 0x35bdd2f6l,
                                       0x71126905l, 0xb2040222l, 0xb6cbcf7cl, 0xcd769c2bl,
                                       0x53113ec0l, 0x1640e3d3l, 0x38abbd60l, 0x2547adf0l,
                                       0xba38209cl, 0xf746ce76l, 0x77afa1c5l, 0x20756060l,
                                       0x85cbfe4el, 0x8ae88dd8l, 0x7aaaf9b0l, 0x4cf9aa7el,
                                       0x1948c25cl, 0x02fb8a8cl, 0x01c36ae4l, 0xd6ebe1f9l,
                                       0x90d4f869l, 0xa65cdea0l, 0x3f09252dl, 0xc208e69fl,
                                       0xb74e6132l, 0xce77e25bl, 0x578fdfe3l, 0x3ac372e6l
                                   }
                                 };

    } BLOWFISH_CTX;

    QVector <FEATURE_RECORD> Record_List;
    QList <QStringList> Feature_StringList;
    double COMF = 1;
    double SOMF = 1;
    QVector <SPATIAL_FEATURES> Feature_List;
    QVector <DDR_DIRECTORY> DdrFields_List;
    QVector <QVector <DR_DIRECTORY>> DrEntry_List;

    uint32_t Enc_blowfish(BLOWFISH_CTX *ctx, uint32_t xIn);

    void Enc_Encrypt (BLOWFISH_CTX *ctx, unsigned long *xl, unsigned long *xr);
    void Enc_PlotRecordObjects();
    void Enc_FindPlotRange(double *xEncMin, double *xEncMax, double *yEncMin, double *yEncMax);
    void Enc_CreateSpatialObjectList();
    int Enc_ReadDdr (char *data_pointer, int BytesPointer);
    int Enc_ReadDataRecord (char *data_pointer, int BytesPointer);
    void Enc_ReadDescriptor (QVector<QString> &DrDataDesc, QVector <DR_DIRECTORY> DrFields_List, /*QVector<DDR_DIRECTORY> DdrFields_List,*/ int i, char *FieldArea, int *BytesRead);
    int Enc_convertToInt (QString &str);
    void Enc_plotFsptList();
    void Enc_plotAll2dObjects();
    void Enc_plotAllSoundPoints();
    int readTiffDataRecord(char *tiffDataPointer, int bytesPointer);
    int Chart_ReadTiff(char* tiffDataPointer, int bytesPointer);
    int Chart_ConvertToInt2 (char *dataPointer, int size, char tp);
    void plot_zoomBox_updatePosition(bool updateRequired);

    void plot_offset_verticalShiftCalculator();

    /****************XTF FILE VARIABLES****************/
    typedef int		    BOOL;
    typedef unsigned char	    BYTE;
    typedef unsigned short      WORD;
    typedef unsigned long       DWORD;
    typedef unsigned int	    UINT;
    typedef signed long	    LONG;

    typedef struct {
        BYTE TypeOfChannel = 0;     // PORT, STBD, SBOT or BATH
        BYTE SubChannelNumber = 0;
        WORD CorrectionFlags = 0;   // 1=raw, 2=Corrected
        WORD UniPolar = 0;          // 0=data is bipolar, 1=data is unipolar
        WORD BytesPerSample = 0;    // 1 or 2
        DWORD Reserved = 0;// Usually a multiple of 1024 unless bathymetry
        char ChannelName[16] = "";   // Text describing channel.  i.e., "Port 500"

        float VoltScale = 0;        // How many volts is represented by max
        // sample value.  Typically 5.0.
        float Frequency = 0;        // Center transmit frequency
        float HorizBeamAngle = 0;   // Typically 1 degree or so
        float TiltAngle = 0;        // Typically 30 degrees
        float BeamWidth = 0;        // 3dB beam width, Typically 50 degrees

        // Orientation of these offsets:
        // Positive Y is forward
        // Positive X is to starboard
        // Positive Z is down.  Just like depth.
        // Positive roll is lean to starboard
        // Positive pitch is nose up
        // Positive yaw is turn to right

        float OffsetX = 0;          // These offsets are entered in the
        float OffsetY = 0;          // Multibeam setup dialog box.
        float OffsetZ = 0;

        float OffsetYaw = 0;        // If the multibeam sensor is reverse
        // mounted (facing backwards), then
        // OffsetYaw will be around 180 degrees.
        float OffsetPitch = 0;
        float OffsetRoll = 0;

        WORD BeamsPerArray = 0;
        BYTE SampleFormat = 0;

        char ReservedArea2[53] = {0};  //usually set to zero

    } CHANINFO;

    // XTF File header.
    // Total of 1024 bytes.
    ///////////////////////////////////////////////////////////////////////////////
    typedef struct {
        BYTE FileFormat = 123;        // (0x7B)  50 for Q-MIPS file format, 51 for Isis format
        BYTE SystemType = 1;        // Type of system used to record this file.
        // 202=Isis
        char RecordingProgramName[8] = "Pathway";    // Example: "Isis"
        char RecordingProgramVersion[8] = "S1.0.10"; // Example: "1.72"
        char SonarName[16] = "Jetasonic Tech";     // Name of server used to access sonar.
        // Example: "C31_SERV.EXE"
        WORD SonarType = 0;         // K2000=5, DF1000=7, SEABAT=8
        char NoteString[64] = "";    // Notes as entered in the Sonar Setup dialog box
        char ThisFileName[64] = "";  // Name of this file. Example: "LINE12-B.SNR"

        WORD NavUnits = 3;          // 0=METERS or 3=DEGREES

        WORD NumberOfSonarChannels = 0;  // if > 60, header goes to 8K in size
        WORD NumberOfBathymetryChannels = 0;
        BYTE NumberOfSnippetChannels = 0;
        BYTE NumberOfForwardLookArrays = 0;
        WORD NumberOfEchoStrengthChannels = 0;
        BYTE NumberOfInterferometryChannels = 0;
        BYTE Reserved1 = 0;
        WORD Reserved2 = 0;
        float ReferencePointHeight = 0;


        // nav system parameters
        ///////////////////////////
        BYTE     ProjectionType[12] = {0};       // Not currently used
        BYTE     SpheriodType[10] = {0};         // Not currently used
        long     NavigationLatency = 0;        // milliseconds, latency of nav system
        // (usually GPS)
        // This value is entered on the
        // Serial port setup dialog box.
        // When computing a position, Isis will
        // take the time of the navigation
        // and subtract this value.

        float    OriginY = 0;                  // Not currently used
        float    OriginX = 0;                  // Not currently used

        // Orientation of these offsets:
        // Positive Y is forward
        // Positive X is to starboard
        // Positive Z is down.  Just like depth.
        // Positive roll is lean to starboard
        // Positive pitch is nose up
        // Positive yaw is turn to right

        float    NavOffsetY = 0;               // These offsets are entered in
        float    NavOffsetX = 0;               // the multibeam setup dialog box.
        float    NavOffsetZ = 0;
        float    NavOffsetYaw = 0;

        float    MRUOffsetY = 0;               // These offsets are entered in
        float    MRUOffsetX = 0;               // the multibeam setup dialog box
        float    MRUOffsetZ = 0;

        float    MRUOffsetYaw = 0;
        float    MRUOffsetPitch = 0;
        float    MRUOffsetRoll = 0;

        // note: even 128-byte boundary to here
    } XTFFILEHEADER;

    // Sonar or Bathy Ping header
    // The data here can change from ping to ping but will pertain to all
    // channels that are at the same time as this ping.  256 bytes in length.
    ///////////////////////////////////////////////////////////////////////////////
    typedef struct {

        //
        // Type of header
        //
        WORD MagicNumber = 0xFACE;      // Set to 0xFACE
        BYTE HeaderType = 0;       // XTF_HEADER_SONAR (0), XTF_HEADER_BATHY (2),
        //   XTF_HEADER_FORWARD or XTF_HEADER_ELAC (5)
        BYTE SubChannelNumber = 0; // When HeaderType is Bathy, indicates which head
        // When sonar, which ping of a batch (Klein 5000:
        // 0..4)

        WORD NumChansToFollow = 0; // If Sonar Ping, Number of channels to follow
        WORD Reserved1[2] = {0};

        DWORD NumBytesThisRecord = 0; // Total byte count for this ping including***************************************************************
        // this ping header

        //
        // Date and time of the ping
        //
        WORD  Year = 0;          // Computer date when this record was saved
        BYTE  Month = 0;
        BYTE  Day = 0;
        BYTE  Hour = 0;          // Computer time when this record was saved
        BYTE  Minute = 0;
        BYTE  Second = 0;
        BYTE  HSeconds = 0;      // hundredths of seconds (0-99)
        WORD  JulianDay = 0;     // Number of days since January 1

        //
        // General information
        //
        DWORD EventNumber = 0;       // [O] Last logged event number

        DWORD PingNumber = 0;       // Counts consecutively from 0 and increments ***********************************************************************
        //   for each update.  Note that the
        //   counters are different between sonar
        //   and bathymetery updates.

        float SoundVelocity = 0;    // m/s, Round trip, defaults to 750.
        //   Can be changed on Isis menu.  This
        //   value is never computed and can only be
        //   changed manually by the user. Also see
        //   ComputedSoundVelocity below.

        float OceanTide = 0;        // [{t}] Ocean tide in meters.  Can be
        // changed by the user on the Configure
        // menu in Isis.
        DWORD Reserved2 = 0;        // Reserved for future use

        //
        // Raw CTD information.  The Freq values are those sent up by the
        // SeaBird CTD.  The Falmouth Scientific CTD sends up computed data.
        float ConductivityFreq = 0; // [Q] Conductivity frequency in Hz
        float TemperatureFreq = 0;  // [b] Temperature frequency in Hz
        float PressureFreq = 0;     // [0] Pressure frequency in Hz
        float PressureTemp = 0;     // [;] Pressure Temperature (Degrees C)

        //
        // Computed CTD information.  When using a SeaBird CTD, these
        // values are computed from the raw Freq values (above).
        //
        float Conductivity = 0;     // [{c}] Conductivity in S/m can be computed from
        // [Q]
        float WaterTemperature = 0; // [{w}] Water temperature in C, can be computed
        // from [b]
        float Pressure = 0;         // [{p}] Water pressure in psia, can be computed
        // from [0]
        float ComputedSoundVelocity = 0;  // Meters per second, computed from
        // Conductivity, WaterTemperature and
        // Pressure using the Chen Millero
        // formula (1977) formula (JASA,62,1129-1135).


        //
        // Sensors information
        //
        float MagX = 0;             // [e] X-axis magnetometer data, mgauss
        float MagY = 0;             // [w] Y-axis magnetometer data, mgauss
        float MagZ = 0;             // [z] Z-axis magnetometer data, mgauss

        // Auxillary values can be used to store
        // and display any value at the user's
        // discretion.  The are not used in
        // any calculation in Isis, Target or Vista.

        float AuxVal1 = 0;          // [1] Auxillary value.  Displayed in the
        float AuxVal2 = 0;          // [2] Auxillary value   "Sensors" window
        float AuxVal3 = 0;          // [3] Auxillary value   available by selecting
        float AuxVal4 = 0;          // [4] Auxillary value   Window->Text->Sensors.
        float AuxVal5 = 0;          // [5] Auxillary value
        float AuxVal6 = 0;          // [6] Auxillary value

        float SpeedLog = 0;         // [s] Speed log sensor on towfish - knots. This
        // isn't fish speed!
        float Turbidity = 0;        // [|] turbidity sensor (0 to +5 volts) stored
        //times 10000

        //
        // Ship Navigation information.  These values are stored only
        // and are not part of any equation or computation in Isis.
        //
        float ShipSpeed = 0;        // [v] Speed of ship in knots.  Stored
        float ShipGyro = 0;         // [G] Ship gyro in degrees
        double ShipYcoordinate = 0; // [y] Ship latitude or northing
        double ShipXcoordinate = 0; // [x] Ship longitude or easting
        WORD ShipAltitude = 0;
        WORD ShipDepth = 0;
        //
        // Sensor Navigation information
        //
        BYTE FixTimeHour = 0;       // [H] Hour of most recent nav update
        BYTE FixTimeMinute = 0;     // [I] Minute of most recent nav update
        BYTE FixTimeSecond = 0;     // [S] Second of most recent nav update
        // Note that the time of the nav is
        // adjusted by the NavLatency stored in
        // the XTF file header.
        BYTE FixTimeHsecond = 0;
        float SensorSpeed = 0;      // [V] Speed of the in knots.  Used for
        //   speed correction and position calculation.
        float KP = 0;               // [{K}] Kilometers Pipe
        double SensorYcoordinate = 0; // [E] Sensor latitude or northing
        double SensorXcoordinate = 0; // [N] Sensor longitude or easting
        // Note: when NavUnits in the file header
        // is 0, values are in meters (northings
        // and eastings).  When NavUnits is 3,
        // values are in Lat/Long.  Also see
        // the Layback value, below.

        //
        // Tow Cable information
        //
        WORD SonarStatus = 0;
        WORD RangeToFish = 0;       // [?] Slant range to fish * 10.
        //    Not currently used.
        WORD BearingToFish = 0;     // [>] Bearing to towfish from ship * 100.
        //    Not currently used.
        WORD CableOut = 0;          // [o] Amount of cable payed out in meters
        //    Not currently used in Isis.
        float Layback = 0;          // [l] Distance over ground from ship to fish.
        //    When this value is non-zero, Isis
        //    assumes that SensorYcoordinate and
        //    SensorXcoordinate need to be
        //    adjusted with the Layback.  The sensor
        //    position is then computed using the
        //    current sensor heading and this layback
        //    value.  The result is displayed when a
        //    position is computed in Isis.

        float CableTension = 0;     // [P] Cable tension from serial port. Stored only.

        //
        // Sensor Attitude information
        //
        float SensorDepth = 0;        // [0] Distance from sea surface to
        //   sensor.  The deeper the sensor goes,
        //   the bigger (positive) this value becomes.

        float SensorPrimaryAltitude = 0;
        // [7] Distance from towfish to the sea
        //   floor.  This is the primary altitude as
        //   tracked by the Isis bottom tracker or
        //   entered manually by the user.
        //   Although not recommended, the user can
        //   override the Isis bottom tracker by
        //   sending the primary altitude over the
        //   serial port.  The user should turn the
        //   Isis bottom tracker Off when this is done.

        float SensorAuxAltitude = 0;  // [a] Auxillary altitude.  This is an
        //   auxillary altitude as transmitted by an
        //   altimeter and received over a serial port.
        //   The user can switch betwen the Primary and
        //   Aux altitudes via the "options" button in
        //   the Isis bottom track window.

        float SensorPitch = 0;        // [8] Pitch in degrees (positive=nose up)
        float SensorRoll = 0;         // [9] Roll in degrees (positive=roll to stbd)
        float SensorHeading = 0;      // [h] Fish heading in degrees

        // These Pitch, Roll, Heading, Heave and Yaw values are those received
        // closest in time to this sonar or bathymetry update.  If a TSS or MRU
        // is being used with a multibeam/bathymetry sensor, the user should
        // use the higher-resolution attitude data found in the XTFATTITUDEDATA
        // structures.


        //
        // additional attitude data
        //
        float Heave = 0;            // Sensor heave at start of ping.
        // Positive value means sensor moved up.
        float Yaw = 0;              // Sensor yaw.  Positive means turn to right.
        float AttitudeTimeTag = 0;

        //
        // Misc.
        //
        float DOT = 0;              // Distance Off Track
        DWORD NavFixMilliseconds = 0;
        BYTE ComputerClockHour = 0;
        BYTE ComputerClockMinute = 0;
        BYTE ComputerClockSecond = 0;
        BYTE ComputerClockHsec = 0;
        short FishPositionDeltaX = 0;
        short FishPositionDeltaY = 0;
        unsigned char FishPositionErrorCode = 0;
        BYTE ReservedSpace2[11] = {0};

    } XTFPINGHEADER, XTFBATHHEADER;

    // Ping Channel header
    // This is data that can be unique to each channel from ping to ping.
    // Is is stored at the front of each channel of sonar data.
    ///////////////////////////////////////////////////////////////////////////////
    typedef struct {

        WORD ChannelNumber = 0;     // Typically,
        // 0=port (low frequency)
        // 1=stbd (low frequency)
        // 2=port (high frequency)
        // 3=stbd (high frequency)

        WORD DownsampleMethod = 0;  // 2=MAX, 4=Rms
        float SlantRange = 0;       // Slant range of the data in meters
        float GroundRange = 0;      // Ground range of the data in meters
        //   (SlantRange^2 - Altitude^2)
        float TimeDelay = 0;        // Amount of time (in seconds) to the start of
        // recorded data
        //   almost always 0.0
        float TimeDuration = 0;     // Amount of time (in seconds) recorded
        float SecondsPerPing = 0;   // Amount of time (in seconds) from ping to ping

        WORD ProcessingFlags = 0;  // 4=TVG, 8=BAC and GAC, 16=Filter, etc...
        //   almost always 0
        WORD Frequency;
        WORD InitialGainCode = 0;   // Settings as transmitted by sonar
        WORD GainCode = 0;
        WORD BandWidth = 0;
        float ContactNumber = 0;
        WORD ContactClassification = 0;
        BYTE ContactSubNumber = 0;
        BYTE ContactType = 0;
        DWORD NumSamples = 0;
        WORD MillivoltScale = 0;
        float ContactTimeOffTrack = 0;
        BYTE ContactCloseNumber = 0;
        BYTE Reserved2 = 0;
        float FixedVSOP = 0;
        short Weight = 0;
        BYTE ReservedSpace[4] = {0};
    } XTFPINGCHANHEADER;

    XTFFILEHEADER fileHeaderObject;
    CHANINFO chanInfoObject;
    XTFPINGCHANHEADER pingChHeaderObject;
    XTFPINGHEADER pingHeaderObject;


    /*Slots should only be created if they're used through a connection,
     or if they're used for a GUI widget
  Otherwise, make it into a function near the top
  Slots requires slightly more space and time to execute than a function
 */
private slots:

    void plot_range_hideZoomRatioLabel();
    void plot_data_numberOfPingsToPlot(int numberOfPingsIndex);
    void plot_offset_depthSensorDefaultZoom(QMouseEvent* event);

    void plot_offset_zoomResetLF(QMouseEvent* event);
    void plot_offset_zoomResetImagePlot(QMouseEvent* event);
    void plot_offset_zoomResetHF(QMouseEvent* event);
    void plot_offset_zoomResetDepth(QMouseEvent* event);
    void plot_offset_zoomResetSensor(QMouseEvent* event);
    void plot_offset_zoomResetImage(QMouseEvent* event);
    void plot_offset_zoomResetGps(QMouseEvent* event);

    void receiveBoatSpeed(int i);
    void receiveDepthRange(int i);
    void receive_alt_length(int i);
    void receive_yaw_length(int i);

    void data_simulate();

    void Gnss_sendNmeaZda(QString data = "");
    void Gnss_sendNmeaGga(QString data = "");
    void Tcp_onConnected();
    void plot_data_addTrace();
    void Tcp_readData();
    void plot_range_zoomUpdate();

    void plot_click_activateMouseFlag(QMouseEvent *event);
    void plot_click_deactivateMouseFlag();
    void plot_range_zoomMouseEvent(QWheelEvent*);
    void plot_data_updateTVG(GainSettings::channelType chType, GainSettings gain_old, int gainType);

    void recorder_saveData(QByteArray);
    void setup_rangeButtonActivate(bool activate);
    bool recorder_startStop(bool activate);
    //void convertFile();
    bool xtfConverter_DialogSelectFile(QString browserPath, QString saveBrowser);
    void player_sortFileData();
    void player_selectFile();
    void player_reset();
    void player_restartFile();
    int player_loadToBuffer();
    void Chart_selectFile();
    void player_playData(bool play);
    void plot_colorScale_getCustomGrad(QCPColorGradient gradient);
    void plot_colorScale_getDepthGrad(QCPColorGradient gradient);
    void plot_updatePlot();
    int bottomTrack_computeAverage1(int crossPointSampleNum1);
    int bottomTrack_computeAverage2(int crossPointSampleNum1);
    bool bottomTrack_calculate_72(QList<quint16> list, int PlotType);
    int bottomTracking_computeAverage_72(int crossPointSampleNum1);
    void bottomTrack_setSignalStrength(int strength, int side);
    void bottomTrack_updateIcon();
    //void setExpect(double ping_Interval);
    void setCompressionRate360(double compRate360);
    void setCompressionRate480(double compRate480);
    void openSystemSettings();
    void showTime();
    void updateSpeedOfSound(double ss);
    void updateCableLength(double length);
    void setTvg(bool tvgSet);
    void plot_colorScale_hide(bool color_scale_hide);
    void setGainSliderVal(int value);
    void setHFGains(double starHF, double portHF);
    void applyGains(double upper, double lower, int bal);
    void changeNoiseThresh(double newThresh);
    void plot_replotAll(bool requiredReplot = 0);
    void setRecPath(QString path);
    void setPlaySpeed(int newPlot);
    void player_addColorMap();
    void resetGui();
    void plot_clearSonarPlots();
    void plot_clearAllPlots();
    void resetPlotView();
    //    void dataRecorderClosed();
    void setCompressionFactor(int factor);
    void setFilterSize(int size);
    void setFilterEnabled(bool enabled);
    void setHFWindowOpen(bool open);
    void setDepthWindowOpen(bool DepthOpen);
    void setScopeWindowOpen(bool ScopeOpen);
    void GainsFromUsermenu(bool tvg, QString unit, int Gain, int Balance, int Gain_HF);
    void ui_updateButtons();

    void compass_receiveClicked(bool bo);
    void compass_receive(int data);
    void receiveDataToSend(int data);

    void measuringTape_DeleteSelectedItems();
    void target_SaveToFile();
    void target_DialogOkay(QString, targetItem*, bool);
    void target_DeleteSelectedItems();
    void target_DeleteSelectedWaypoints();
    void target_GoToTarget();
    void target_renameTarget();
    void target_exportTarget();
    void target_sortByPing();
    void target_sortWaypoints();
    void target_DisplayWayPoint(int num);
    void target_setStartWaypoint();
    void receiveEthernetActive(bool b);
    //void hideKalmanStateChanged(int s); no longer used
    void Tcp_receiveDisconnection();
    void setTransmitter(int trans);
    void setEdgeAlign(bool);

    //Thread slots
    void receiveSysFromThread(float Altitude, float Depth);
    void Enc_checkToPlot();

    //Slots (widgets)
    void on_att_slider_valueChanged(int value);
    void on_gain_mult_slider_valueChanged(int value);
    void on_connect_btn_clicked();
    void Tcp_connect(); //a newfunction added containing code to setup the TCP soceket
    void on_actionActivate_triggered();
    void on_actionDeactivate_triggered();
    //void on_setYRangeCheckbox_clicked(); //function not used//
    void on_range_btn_clicked(int index);
    void on_actionAbout_3_triggered(); //function not used//
    void on_actionUserMenu_triggered();
    void on_add_gain_clicked();
    void on_sub_gain_clicked();
    void closeEvent(QCloseEvent *e);
    void on_actionSettings_triggered();
    void on_digitalGainBalance_valueChanged(int value);
    void plot_onMouseHover(QMouseEvent* event);
    void plot_tools_onMouseMoveGps(QMouseEvent* event);
    void plot_Lf_onMousePressed(QMouseEvent* event);
    void plot_Hf_onMousePressed(QMouseEvent* event);
    void plot_Gnss_onMousePressed(QMouseEvent* event);
    void plot_image_onMousePressed(QMouseEvent* event);
    void plot_Lf_onMouseReleased(QMouseEvent* event);
    void plot_Hf_onMouseReleased(QMouseEvent* event);
    void plot_Gnss_onMouseReleased(QMouseEvent* event);
    void plot_Lf_PlotItemSelectionChanged();
    void plot_Hf_PlotItemSelectionChanged();
    void plot_Gnss_PlotItemSelectionChanged();
    void on_scope_btn_clicked();
    void on_about_btn_clicked(int IDnum = 0, quint16 LFrequency = 0, quint16 HFrequency = 0, int Angle = 0);
    void on_actionQuit_triggered();
    void on_actionSystem_triggered();
    void on_actionSaveSettings_triggered();
    void on_actionLoadSettings_triggered();

    void on_btn_importWaypoints_clicked();
    void on_actionSaveTarget_triggered();
    void on_actionConverter_triggered();
    void showLabelOnAltGraph(double y);
    void depthLabelHide(bool show);
    void tractionStripHide(bool show);

    void on_actionMeasuring_Tape_triggered();
    void on_actionShadow_triggered();
    void on_actionFlag_triggered();
    void on_actionWaypoint_triggered();
    void on_actionCrop_File_triggered();
    void gainSlider_userMovedSliderLf(int value);
    void gainSlider_userMovedSliderHf(int value);
    void gainSlider_userMovedSliderDepth(int value);
    void on_mainGain_btn_clicked();
    void on_hfGain_btn_clicked();
    void keyPressEvent(QKeyEvent * e);
    void keyReleaseEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    void on_targetListWidget_itemDoubleClicked(QListWidgetItem *item);
    void on_targetListWidget_customContextMenuRequested(const QPoint &pos);
    void on_targetListWidget_itemSelectionChanged();
    void on_waypointListWidget_itemSelectionChanged();
    void on_waypointListWidget_customContextMenuRequested(const QPoint &pos);
    void on_flagBtn_clicked();
    void on_gnssBtn_clicked();
    void on_zoomIn_clicked();
    void on_zoomOut_clicked();
    void on_trackpointsBtn_clicked();
    void Tcp_connectError(int errorMsg); //show error msg for the status of ethernet connection
    void format_splitterHandleChanged ();


    void on_quick_recording_clicked();
    void on_quick_playpause_clicked();

    void on_openFileButton_clicked();
    bool  xtfConverter_SortFileData();

    //void on_quick_player_clicked();
    void on_quick_stop_btn_clicked();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSaveAs_triggered();
    void on_actionClose_triggered();
    void on_actionHelp_2_triggered();
    void simulate_Plot(bool action);
    void on_auto_btn_clicked();
    void on_btn_range1_clicked();
    void on_btn_range2_clicked();
    void on_btn_range3_clicked();
    void on_btn_range4_clicked();
    void status_setProgressBarValue(double val);
    void status_setFileSize(double size);
    void status_showFileName(QString Name);
    void format_actionTriggered();
    void format_showStatusBar(bool show);
    void format_showGap(bool show);
    void format_showBalance(bool show);
    void format_showGridOnPlot(bool show);
    void format_showZoomBox(bool show);
    void format_showCompass(bool show);
    void format_showCrosshair(bool show);
    void format_showColorScale(bool show);
    void format_showSignalPlot(bool show);
    void format_showGainSlider(bool show);
    void closeTvg();
    void on_gain_btn_clicked();
    void hideGainSliders();
    void on_tvg_btn_clicked();

    void setTransparency(int value, int i);
    void setAltGradient(int value);
    void xtfConverter_StartBtnClicked(bool);
    void xtfConverter_CancelBtnClicked(bool);
   void xtfConverter_changeDataFilePath(QList<QString>, QList<QString>);
   void xtfConverter_FileCounter(int num);
    void Chart_Values(double w, double x, double y, double z);
    void Chart_GpsPlotReplot();

    void depthAlarm_run();
    void depthAlarm_hide();
    void depthAlarm_selectHandle(QMouseEvent *e);
    void depthAlarm_selectHandle(bool selected);
    void depthAlarm_moveHandle(QMouseEvent *e);
    void depthAlarm_deselectHandle(QMouseEvent *e);
    void on_btn_depth_clicked();

    void depthAlarm_updateHandlePosition(int xPos);
    void depthAlarm_updateButtonState();
    void mouseCursorTimeout();
    void crop_saveFile();

signals:


    void plotChC_data(QList<quint32>);
    void record(QByteArray);
    void updateFileName(QString);
    void changeBoatSpeed(int);
    void lowFtoUsermenu(quint16);
    void highFtoUsermenu(quint16);
    void calculateDepth();
    void connected(bool connected);
    void checkFilePlay(bool play);
    void plot();
    void setRecordedTime(int time);
    void updateRecordedTime(int time);
    void hfGradient(QCPColorGradient gradient);
    void replotHF();
    void sendNav(QVector <float> nav);
    void sendSys(float x, float y);
    void freqChange(bool);
    void updateRecPath(QString);
    void updateConverterDirPath(QString path);
    void changeFreq(bool);
    void changeSS(double);
    void changeCL(double);
    void changeCSMaxHF(int);
    void sendLFDGain(double, double);
    void sendLFBalance(int);
    void sendZoom(bool);
    void sendNavigation(bool);
    void sendFileSize(double);
    void sendSBarSize(double);
    void sendCurrSBarVal(double);
    void triggerPause();
    void changeStart(bool);
    void unlockRecorder(bool);
    void updateFileSelected(bool);
    void sendGainAdd(int);
    void sendNoiseThresh(double);
    void sendFilterSize(int);
    void degreeChanged(double);
    void defaultAngle(double);
    void noAngleControl();
    void startClock();
    void drawLineOnPlot(double,double,double,double);
    void sendParametersToFlag(int row, bool newItem);
    void itemClicked (QListWidgetItem *item);
    void sendMessage(QString message);
    void sendDisableBoatSpeed(bool b);
    void sendKalmanValue(quint16);
    void sendEnableGPSBtn(bool b);
    void GainsToUsermenu(int GainSliderIndex, int BalanceSliderIndex, int GainSliderIndex_HF);
    void freqToUsermenu();
    void formatStatusBar(bool show);
    void formatAltimeter(bool show);

    //Config settings database value signals
    void setUpConfigDatabase();
    void requestConfigDatabaseClose();

    //Thread signals
    void sendNavFromThread(QVector<float> list);
    void sendSysFromThread(float Altitude, float Depth);
    void applyKalmanFilterFromThread(quint16 kalmanNum);
    void updateChartProgress(int);

    void showGnssClickable(bool show);
    void showHfClickable(bool show);
    void showAltimeterClickable(bool show);

    void showFormatGainSlider(bool show);

    void selectAltimeter(bool show);
    void selectGPS(bool);
    void selectSensor(bool);
    void selectLF(bool);
    void selectHF(bool);
    void updateSplitterSizeInfo(double, double, double, double, double);

    void tiffFileName(QString);

     void converterDialogProgressBar(double val);
     void converterFileSize(double size);
     void converterFileName(QString name);
     void fileNameToXtfConverterWindow(QString name);
     //void renameFileName(QString name);
     void progressBarRange(int size);

     void isConversionOver(bool show);
     void angleFileDetected(bool show);
     void updateConvertedFileSize(int num);

     void createHelpWindow();

     void clickCounter(int,int,int);
     void uncheckShowForwardRng(bool val);
     void untiePlots(bool val);
     void enableTvgTab(bool);
     void updateAlarmSettings();

protected:


    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::MainWindow *ui;
    QTimer simulationTimer, readFileTimer, replotTimer, sortFileTimer, zoomRatioTimer;
    QTimer EncTimer;
    QTimer UsbTimer;
    QTimer gainSliderTimer;
    QTimer mouseTimer;
    QTcpSocket *tcpSocket = new QTcpSocket(this);
    QSerialPort *sonarComPort = nullptr;
    bool sonarComPortReady = false;
    GainSettings *gain, *gain_old;
    QString legendTxt = "";
    QString displayName = "";
    QPointF mouseStartPos_lf, mouseStartPos_hf;
    QCPRange startRangeX_lf, startRangeY_lf, startRangeX_hf, startRangeY_hf;
};

#endif // MAINWINDOW_H
