#ifndef GPSMAP_H
#define GPSMAP_H

#include "qcustomplot.h"
#include <QDialog>
#include <QMoveEvent>
#include <QAction>
#include "modules.h"

namespace Ui {
class gpsMap;
}

class gpsMap : public QDialog
{
    Q_OBJECT

public:
    explicit gpsMap(QWidget *parent = nullptr);
    ~gpsMap();
    void plot_setupItems (QCustomPlot* plotPtr);
    void plot_reset();
    void plot_resize(QCPRange xAxis, QCPRange yAxis);
    void path_addData(double xCoord, double yCoord);
    void path_addTrackpoints(double xCoord, double yCoord);
    void path_resetTrackpoints();
    void target_addWaypoint (double Lattitude, double Longitude, double size, int index);
    void gnss_calculateAccumulatedDistance();
    void legend_updateHeading(double Heading);
    void legend_updateTarget(double Heading, double Distance, double RelativeHeading);
    double gnss_calculateHeading(double xCenter, double yCenter, double rot);
    void path_addBoundaryPoints(double xCenter, double yCenter, double rot, double range, double transducerAngle);
    void track_moveCurve(int pingDiff);
    int track_CursorToCurve(QMouseEvent *event, int pingDiff);
    void track_getCurveCoordinates(int pingDiff, targetItem item);
    void target_addFlag (double Lattitude, double Longitude, double size, int index);
    void target_resizeTarget (int index, bool Enlarged);
    void plot_updateRange();
    void target_removeItemFromPlot (int flagItem);
    double gnss_calculateSpeed (double lat, double lon, double speed);
    void legend_updateSpeedText(double speed);
    void legend_updateHeadingText(double heading);
    void addMapVector(double xCoord, double yCoord, int counter);
    void addMapVector2(double xCoord, double yCoord, int counter, int lineType);
    QCPItemText* addSoundingPoint (double xCoord, double yCoord, double zDepth);
    QCPItemText* addPointObject (double xCoord, double yCoord, QString text);
    void setPlotRange(double x1, double x2, double y1, double y2);
    void setPlotColorMap(double x1, double x2, double y1, double y2);
    void setPlotColorMap(double x1, double x2, double y1, double y2, int height, int width);
    void addMapVector3 (QList <double> xCoord, QList <double> yCoord, int lineType);
    void addMapVector4(double xCoord, double yCoord, int counter, int color, int thickness, int layer);
    void addMapVector5(double xCoord, double yCoord, int counter, int color, int thickness, int layer);
    void copyAreaToLandMap(double criteriaTopMap, double criteriaLandMap);
    void plot_configureBoatIcon (int iconType);
    void plot_correctBoatHeading ();
    void plot_showBoatIcon(bool show);

    QCPCurve* addCurves(QList <double> x, QList <double> y, int colorType);
    void clearEncChart();
    void soundingPointUpdate();
    void objectsUpdate();
    void legend_updateAll();
    void legend_updateLegendText(double latNew, double lonNew, double HdgNew, double SpeedNew, double distNew);

    void plot_replot();
    bool plot_isCentered();
    void paintMap();
    void paintTopMap(double criteria);
    double findFillColor (double xCoord, double yCoord);
    double findFillColor2 (int xCoord, int yCoord);
    double findFillColorTopMap (int xCoord, int yCoord, double criteria);
    void fillPaintTopMap(int xCoord, int yCoord, double color, double criteria);
    void fillPaint (double xCoord, double yCoord, double color);
    void fillPaint2 (int xCoord, int yCoord, double color);
    void clearmap();
    void plotPoint (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer);
    void determineAreaFill();
    void plotPoint2 (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer, double angle);
    void plotPoint3 (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer, double angle);
    void plotPoint4 (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer, double angle);
    QCustomPlot *mapPlotPtr = nullptr;
    double zoomScale = 1;
    QCPCurve *newCurve = nullptr, *newCurve2 = nullptr,
        *newCurve3 = nullptr, *vectorCurve = nullptr;
    QCPCurve *trackpoints = nullptr;
    QCPItemEllipse *cursorDot = nullptr;
    QCPItemLine *portLine, *stbdLine, *portLine2, *stbdLine2;
    QCPColorMap* getColorMapPointer();
    QCPItemEllipse *boatIcon = nullptr;
    QCPItemPixmap *boatIconPix = nullptr, *compassIcon = nullptr;
    double boatHeading = 0;
    double totalDistance = 0;
    double gpsSpeed = 0;
    QQueue<double> avgHdg_queue;
    bool lockToBoat = true;

signals:
    void splitterMoved();

public slots:
    void target_resizeAllTargets();
    void plot_gpsZoom(QWheelEvent *event);

private slots:
    void updateLoadingChartText (int state);

private:
    Ui::gpsMap *ui;
    double MaxXRange, MinXRange, MaxYRange, MinYRange;
    double xCenter, yCenter;
    bool zoomOn = false;

    QCPColorMap *colorMap, *landMap;
    QQueue <QCPColorMap*> colorMapNum_queue;
    QCPColorScale *colorScale;

    double xRes, yRes;
    int xCellMax, yCellMax;
    double xMeter = 1, yMeter = 1;

    QString textDistance = "N/A", textSpeed = "N/A", textHdg = "N/A", textLat = "Lat: N/A", textLon = "Lon: N/A";
    QString textTarget1 = "TGT", textTarget2 = "";
};

#endif // GPSMAP_H
