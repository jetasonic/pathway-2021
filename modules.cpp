#include "modules.h"

Modules::Modules()
{

}

GainSettings::GainSettings()
{

}

double GainSettings :: calculateTvg(int sampleNum, double resolutionLat, channelType type, bool findMax, bool clipGain) {
    // Calculate with sample number
    double dist = resolutionLat*sampleNum;//(speed_of_sound*(sampleNum*sampleInterv))/(2);
    double val;
    double tvg_a, tvg_b, tvg_c, tvg_c2, tvg_x, tvg_DR, tvg_min;
//    if (!tvg_on)    return 0;

    switch (type) {
    case channelType::LfPort:
        tvg_a = tvg_ALfP;
        tvg_b = tvg_BLf;
        tvg_c = tvg_CLfP;
        tvg_c2 = C_lfOffset;
        tvg_x = tvg_ratioLfP;
        tvg_DR = tvg_maxLf;
        tvg_min = tvg_minLf;
        break;
    case channelType::LfStbd:
        tvg_a = tvg_ALfS;
        tvg_b = tvg_BLf;
        tvg_c = tvg_CLfS;
        tvg_c2 = C_lfOffset;
        tvg_x = tvg_ratioLfP;
        tvg_DR = tvg_maxLf;
        tvg_min = tvg_minLf;
        break;
    case channelType::HfPort:
        tvg_a = tvg_AHfP;
        tvg_b = tvg_BHf;
        tvg_c = tvg_CHfP;
        tvg_c2 = C_hfOffset;
        tvg_x = tvg_ratioHfP;
        tvg_DR = tvg_maxHf;
        tvg_min = tvg_minHf;
        break;
    case channelType::HfStbd:
        tvg_a = tvg_AHfS;
        tvg_b = tvg_BHf;
        tvg_c = tvg_CHfS;
        tvg_c2 = C_hfOffset;
        tvg_x = tvg_ratioHfP;
        tvg_DR = tvg_maxHf;
        tvg_min = tvg_minHf;
        break;
    }
    val = (tvg_a*log(dist) + (dist*tvg_b)) + tvg_c + tvg_c2;
//    if (!findMax)       val *= tvg_x;
//    else                val += tvg_c;
    if (!tvg_on)    return tvg_c + tvg_c2;
    if (clipGain && val > tvg_DR)       val = tvg_DR;
    if (clipGain && val < tvg_min)      val = tvg_min;
    return val;
}

double GainSettings :: calculateTvg(double distance, channelType type, bool findMax, bool clipGain) {
    // Calculate with sample number
    double val;
    double tvg_a, tvg_b, tvg_c, tvg_c2, tvg_x, tvg_DR;
//    if (!tvg_on)    return 0;

    switch (type) {
    case channelType::LfPort:
        tvg_a = tvg_ALfP;
        tvg_b = tvg_BLf;
        tvg_c = tvg_CLfP;
        tvg_c2 = C_lfOffset;
        tvg_x = tvg_ratioLfP;
        tvg_DR = tvg_maxLf;
        break;
    case channelType::LfStbd:
        tvg_a = tvg_ALfS;
        tvg_b = tvg_BLf;
        tvg_c = tvg_CLfS;
        tvg_c2 = C_lfOffset;
        tvg_x = tvg_ratioLfP;
        tvg_DR = tvg_maxLf;
        break;
    case channelType::HfPort:
        tvg_a = tvg_AHfP;
        tvg_b = tvg_BHf;
        tvg_c = tvg_CHfP;
        tvg_c2 = C_hfOffset;
        tvg_x = tvg_ratioHfP;
        tvg_DR = tvg_maxHf;
        break;
    case channelType::HfStbd:
        tvg_a = tvg_AHfS;
        tvg_b = tvg_BHf;
        tvg_c = tvg_CHfS;
        tvg_c2 = C_hfOffset;
        tvg_x = tvg_ratioHfP;
        tvg_DR = tvg_maxHf;
        break;
    }
    val = (tvg_a*log(distance) + (distance*tvg_b)) + tvg_c + tvg_c2;
//    if (!findMax)       val *= tvg_x;
//    else                val += tvg_c;
    if (!tvg_on)    return tvg_c + tvg_c2;
    if (clipGain && val > tvg_DR)       val = tvg_DR;
    return val;
}

double GainSettings :: calculateBValue(channelType type, double maxRange) {
    double Bval = 0;
    switch (type) {
    case channelType::LfPort:
        calculateBLf = true;
        Bval = (80 - (tvg_ALfPInd*5) * log(maxRange)) / maxRange;
        break;
//        return 0.00017*tvg_frequencyLow*tvg_frequencyLow/(tvg_T+18.0);
    case channelType::LfStbd:
        calculateBLf = true;
        Bval = (80 - (tvg_ALfSInd*5) * log(maxRange)) / maxRange;
        break;
//        return 0.00017*tvg_frequencyLow*tvg_frequencyLow/(tvg_T+18.0);
    case channelType::HfPort:
        calculateBHf = true;
        Bval = (80 - (tvg_AHfPInd*5) * log(maxRange)) / maxRange;
        break;
//        return 0.00017*tvg_frequencyHigh*tvg_frequencyHigh/(tvg_T+18.0);
    case channelType::HfStbd:
        calculateBHf = true;
        Bval = (80 - (tvg_AHfSInd*5) * log(maxRange)) / maxRange;
        break;
//        return 0.00017*tvg_frequencyHigh*tvg_frequencyHigh/(tvg_T+18.0);
    }
    if (Bval < 0)       Bval = 0;
    return Bval;
}

SonarPing::SonarPing() {}

void GnssController :: updateCoords(double Lat, double Lon, int Ping) {
    if (abs(Lat) > 100 || abs(Lon) > 200)   return;
    latitude = Lat;
    longitude = Lon;
    currentPing = Ping;
    validCoords = true;
}

void GnssController :: updateTime(short Hour, short Minute, short Second) {
    hour = Hour;
    minute = Minute;
    second = Second;
    validTime = true;
}

void GnssController :: updateDate(short Day, short Month, int Year) {
    day = Day;
    month = Month;
    year = Year;
}

void GnssController :: updateSpeed(double Val) {
    if (Val < 0)        return;
    speed = Val;
    validSpeed = true;
}

void GnssController :: updateHeading(double Val) {
    if (Val < 0)        return;
    heading = Val;
    validHeading = true;
}

void GnssController :: reset() {
    validHeading = false;
    validSpeed = false;
    validCoords = false;
    validTime = false;

    latitude = -500;
    longitude = -500;
    speed = -1;
    heading = -900;
    year = 0;
    month = 0;
    day = 0;
    hour = 0;
    minute = 0;
    second = 0;
}

double GnssController :: Latitude()     { return latitude; }
double GnssController :: Longitude()    { return longitude; }
double GnssController :: Speed()        { return speed; }
double GnssController :: Heading()      { return heading; }
int GnssController :: Year()            { return year; }
short GnssController :: Second()        { return second; }
short GnssController :: Minute()        { return minute; }
short GnssController :: Hour()          { return hour; }
short GnssController :: Day()           { return day; }
short GnssController :: Month()         { return month; }
int GnssController :: Ping()            { return currentPing; }

void Crosshair :: add(QCustomPlot *plotPtr) {

    QPen line_color;
    line_color.setWidth(2);
    line_color.setColor(Qt::darkGray);

    line_x = new QCPItemStraightLine(plotPtr);
    line_x->setVisible(false);
    line_x->setSelectable(false);
    line_x->setPen(line_color);
    line_x->setLayer("boatLayer");

    line_y = new QCPItemStraightLine(plotPtr);
    line_y->setVisible(false);
    line_y->setSelectable(false);
    line_y->setPen(line_color);
    line_y->setLayer("boatLayer");

    text_x = new QCPItemText(plotPtr);
    text_x->setText("");
    text_x->setColor(Qt::white);
    text_x->setBrush(QBrush(Qt::black));
    text_x->setSelectedBrush(QBrush(Qt::black));
    text_x->setFont(QFont("sans", 10, QFont::Bold));
    text_x->setPositionAlignment(Qt::AlignLeft|Qt::AlignTop);
    text_x->setLayer("boatLayer");
    text_x->setSelectable(false);
    text_x->setVisible(false);

    text_y = new QCPItemText(plotPtr);
    text_y->setText("");
    text_y->setColor(Qt::white);
    text_y->setBrush(QBrush(Qt::black));
    text_y->setSelectedBrush(QBrush(Qt::black));
    text_y->setFont(QFont("sans", 10, QFont::Bold));
    text_y->setPositionAlignment(Qt::AlignRight|Qt::AlignTop);
    text_y->setLayer("boatLayer");
    text_y->setSelectable(false);
    text_y->setVisible(false);
}

void Crosshair :: remove() {
    if (parentPlot != nullptr) {
        parentPlot->removeItem(line_x);
        parentPlot->removeItem(line_y);
        parentPlot->removeItem(text_x);
        parentPlot->removeItem(text_y);
    }

    line_x = nullptr;
    line_y = nullptr;
    text_x = nullptr;
    text_y = nullptr;
}

void Crosshair :: setVisible(bool state) {
    line_x->setVisible(state);
    line_y->setVisible(state);
    text_x->setVisible(state);
    text_y->setVisible(state);
}

void Crosshair :: setPosition(double x, double y, double yDelta, double x_text, double y_text) {

    line_x->point1->setCoords(0, y);
    line_x->point2->setCoords(1, y);

    line_y->point1->setCoords(x, 0);
    line_y->point2->setCoords(x, 1);

    text_x->position->setCoords(x, y_text);
    text_y->position->setCoords(x_text, y);
    text_x->setText(" "+QString::number(x, 'f', 1)+" ");
    text_y->setText(" "+QString::number(yDelta, 'f', 1)+" ");
}

bool Crosshair :: isValid() {
    bool state = (line_x != nullptr && line_y != nullptr &&
            text_x != nullptr && text_y != nullptr);
    return state;
}

bool Crosshair :: isVisible() {
    if (line_x != nullptr)
        return false;
    return line_x->visible();
}

PingMapItem :: PingMapItem() {}

PingMapItem :: PingMapItem(double y, int ping, QString time, double temp, float d, double alt, double r, double p, double Yaw, double Azimuth,
                     double lat, double lon, double head, double Speed, double dist, double df) {
    yCoord = y;
    pingNumber = ping;
    temperature = temp;
    depth = d;
    rtcTime = time;
    altitude = alt;
    roll = r;
    pitch = p;
    yaw = Yaw;
    azimuth = Azimuth;
    altitudeFactor = df;
    lattitude = lat;
    longitude = lon;
    heading = head;
    speed = Speed;
    distance = dist;
}

void PingMap :: append(double yCoord, int ping, QString time, float depth, double alt, double roll,
                       double pitch, double yaw, double Azimuth, double lat, double lon, double head,
                       double Speed, double dist) {
    PingMapItem tmpItem(yCoord, ping, time, 0, depth, alt, roll, pitch, yaw, Azimuth, lat, lon, head, Speed, dist, 1);
    tmpItem.isValid = true;
    dataMap.insert(ping, tmpItem);
}

void PingMap :: append(PingMapItem item) {
    item.isValid = true;
    dataMap.insert(item.pingNumber, item);
}

int PingMap :: firstKey() {
    if (dataMap.size() == 0)    return 0;
    return      dataMap.firstKey();
}

int PingMap :: lastKey() {
    if (dataMap.size() == 0)    return 0;
    return      dataMap.lastKey();
}

PingMapItem PingMap :: findItem(int key) {
    if (dataMap.size() == 0 || key < dataMap.firstKey() || key > dataMap.lastKey()) {
        return PingMapItem();
    }
    if (dataMap.find(key) == dataMap.end())     return PingMapItem();
    return dataMap.find(key).value();
}

PingMapItem PingMap :: findItemWithY(double value) {
    if (dataMap.size() == 0) {
        return PingMapItem();
    }
    QMapIterator<int, PingMapItem> i(dataMap);
    while(i.hasNext()) {
        i.next();
        double diff = abs(i.value().yCoord - value);
        if (diff < 0.3) {
            if (i.hasNext()) {
                if (abs(i.peekNext().value().yCoord - value) < diff) {

                } else {
                    return i.value();
                }
            } else {
                return i.value();
            }
        }
    }
    return PingMapItem();
}

PingMapItem PingMap :: last() {
    if (dataMap.size() == 0) {
        return PingMapItem();
    }
    return dataMap.last();
}

void PingMap :: removeItems (int startKey, int endKey) {
    for (int i = startKey; i <= endKey; i++) {
        dataMap.remove(i);
    }
}

void PingMap :: removeItemsWithY (double startY, double endY) {
    startY = startY - 1;
//    foreach (const auto& dataMapKey, dataMap.keys()) {
//        double yCoord = dataMap.find(dataMapKey).value().yCoord;
//        if (yCoord > (startY - 1) && yCoord < endY)
//            dataMap.remove(dataMapKey);
//    }
    for (auto item = dataMap.begin(); item != dataMap.end();) {
        double y = item.value().yCoord;
        if (y > startY && y < endY)
            item = dataMap.erase(item);
        else
            ++item;
    }
}

void PingMap :: clearAll() {
    dataMap.clear();
}

bool PingMap :: containsPing(int pingNum) {
    if (dataMap.size() < 1)     return false;
    int start = dataMap.first().pingNumber;
    int end = dataMap.last().pingNumber;
    return (pingNum >= start && pingNum <= end);
}

// Target Items

targetItem :: targetItem() {
    // Create an empty target
}

targetItem :: targetItem(int index, QString name, double Lat, double Lon, int PingNum, int SampleNum, double x, double y) {
    //
    listIndex = index;
    Name = name;
    Lattitude = Lat;
    Longitude = Lon;
    ping = PingNum;
    sample = SampleNum;
    xCoord = x;
    yCoord = y;
}

void targetItem :: deleteItem() {
    removeItemFromList();
    deleteItemImage();
}

void targetItem :: removeItemFromList() {
    // Remove all icons
    if (icon_lf != nullptr) {
        icon_lf->parentPlot()->removeItem(icon_lf);
        icon_lf = nullptr;
    }
    if (text_lf != nullptr) {
        text_lf->parentPlot()->removeItem(text_lf);
        text_lf = nullptr;
    }
    if (icon_hf != nullptr) {
        icon_hf->parentPlot()->removeItem(icon_hf);
        icon_hf = nullptr;
    }
    if (text_hf != nullptr) {
        text_hf->parentPlot()->removeItem(text_hf);
        text_hf = nullptr;
    }
    if (icon_chart != nullptr) {
        icon_chart->parentPlot()->removeItem(icon_chart);
        icon_chart = nullptr;
    }
    if (text_chart != nullptr) {
        text_chart->parentPlot()->removeItem(text_chart);
        text_chart = nullptr;
    }
    if (line_chart_port != nullptr) {
        line_chart_port->parentPlot()->removeItem(line_chart_port);
        line_chart_port = nullptr;
    }
    if (line_chart_stbd != nullptr) {
        line_chart_stbd->parentPlot()->removeItem(line_chart_stbd);
        line_chart_stbd = nullptr;
    }
    if (line_alt != nullptr) {
        line_alt->parentPlot()->removeItem(line_alt);
        line_alt = nullptr;
    }
    if (line_sensor != nullptr) {
        line_sensor->parentPlot()->removeItem(line_sensor);
        line_sensor = nullptr;
    }
    if (text_altNum != nullptr) {
        text_altNum->parentPlot()->removeItem(text_altNum);
        text_altNum = nullptr;
    }
    if (text_sensor != nullptr) {
        text_sensor->parentPlot()->removeItem(text_sensor);
        text_sensor = nullptr;
    }

    // Remove the QListWidgetItem
    if (ListItem != nullptr) {
        delete ListItem;
        ListItem = nullptr;
    }
}

void targetItem :: deleteItemImage() {
    // Remove the snipped image
    if (imagePath != "") {
        QFile image(imagePath);
        image.remove();
//        QDir dir;
//        dir.setPath(recorder.recPath);
//        dir.remove(fullName);
    }
}

void targetItem :: selectItem(bool select, int type) {
    // Chage icon pixmap on plots
    QPixmap icon = getIconPixmap(select, type).scaled(40,40);
    qDebug() << "Found icon" << select << type;
    if (icon_lf != nullptr) {
        icon_lf->setPixmap(icon);
        icon_lf->setSelected(select);
    }
    if (icon_hf != nullptr) {
        icon_hf->setPixmap(icon);
        icon_hf->setSelected(select);
    }
    if (icon_chart != nullptr) {
        icon_chart->setPixmap(icon);
        icon_chart->setSelected(select);
    }
    if (line_chart_port != nullptr && line_chart_stbd != nullptr) {
//        QPen pen = getAltLinePen(select, type);
//        line_chart_port->setPen(pen);
//        line_chart_port->setSelectedPen(pen);
//        line_chart_stbd->setPen(pen);
//        line_chart_stbd->setSelectedPen(pen);
    }
    if (line_alt != nullptr) {
        QPen pen = getAltLinePen(select, type);
        line_alt->setPen(pen);
        line_alt->setSelectedPen(pen);
    }
    if (line_sensor != nullptr) {
        QPen pen = getAltLinePen(select, type);
        line_sensor->setPen(pen);
        line_sensor->setSelectedPen(pen);
    }
    // Select QListWidgetItem
    if (ListItem != nullptr) {
        // Cannot call setSelected because it causes GUI to crash
        ListItem->setSelected(select);
//        if (select)
//            ListItem->listWidget()->setCurrentItem(ListItem);
    }
    selected = select;
}

void targetItem :: addIconToPlot(QCustomPlot *plotLf, QCustomPlot *plotHf, QCustomPlot *plotAlt, QCustomPlot *plotGps, QCustomPlot *plotSensor, int type) {
    QPixmap pixmapLf = getIconPixmap(false, type).scaled(40,40);
    QPixmap pixmapHf = getIconPixmap(false, type).scaled(40,40);
    QString numberText = QString::number(listIndex+1);
    if (baseWpt != nullptr) {
        numberText = QString::number(baseWpt->listIndex+1);
        if (baseWptNum > 1) {
            QByteArray num = QByteArray::number(60+baseWptNum-1);
            QByteArray num2 = QByteArray::fromHex(num);
            numberText += QString::fromLatin1(num2);
            qDebug() << "Test";
        }
    }

    icon_lf = new QCPItemPixmap(plotLf);
    icon_lf->setPixmap(pixmapLf);
    icon_lf->setScaled(true);
    icon_lf->setLayer("tools2");
    icon_lf->setVisible(true);

    QFont font;
    text_lf = new QCPItemText(plotLf);
    text_lf->setText(" "+ numberText +" ");
    text_lf->setColor(Qt::yellow);
    text_lf->setBrush(QBrush(Qt::black));
    text_lf->setSelectedBrush(QBrush(Qt::black));
    text_lf->setFont(QFont(font.defaultFamily(),9));
    text_lf->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
    text_lf->setLayer("tools3");
    text_lf->setSelectable(false);

    icon_hf = new QCPItemPixmap(plotHf);
    icon_hf->setPixmap(pixmapHf);
    icon_hf->setScaled(true);
    icon_hf->setLayer("tools2");
    icon_hf->setVisible(true);

    text_hf = new QCPItemText(plotHf);
    text_hf->setText(" "+ numberText +" ");
    text_hf->setColor(Qt::yellow);
    text_hf->setBrush(QBrush(Qt::black));
    text_hf->setSelectedBrush(QBrush(Qt::black));
    text_hf->setFont(QFont(font.defaultFamily(),9));
    text_hf->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
    text_hf->setLayer("tools3");
    text_hf->setSelectable(false);

    QPen pen_gps = getAltLinePen(false, type);
    pen_gps.setWidthF(2);
    pen_gps.setStyle(Qt::DotLine);
    pen_gps.setColor(Qt::white);
    line_chart_port = new QCPItemLine(plotGps);
    line_chart_port->setPen(pen_gps);
    line_chart_port->setLayer("tools1");
    line_chart_port->setSelectedPen(pen_gps);
    line_chart_port->start->setCoords(0, 0);
    line_chart_port->end->setCoords(0, 0);
    line_chart_port->setVisible(false);
    line_chart_port->setSelectable(false);

    line_chart_stbd = new QCPItemLine(plotGps);
    line_chart_stbd->setPen(pen_gps);
    line_chart_stbd->setLayer("tools1");
    line_chart_stbd->setSelectedPen(pen_gps);
    line_chart_stbd->start->setCoords(0, 0);
    line_chart_stbd->end->setCoords(0, 0);
    line_chart_stbd->setVisible(false);
    line_chart_stbd->setSelectable(false);

    QPen pen = getAltLinePen(false, type);
    line_alt = new QCPItemLine(plotAlt);
    line_alt->setPen(pen);
    line_alt->setLayer("tools2");
    line_alt->setSelectedPen(pen);
    line_alt->start->setCoords(0, 0);
    line_alt->end->setCoords(0, 0);
    line_alt->setVisible(false);

    text_altNum = new QCPItemText(plotAlt);
    text_altNum->setText(" "+ numberText +" ");
    text_altNum->setColor(Qt::yellow);
    text_altNum->setBrush(QBrush(Qt::black));
    text_altNum->setSelectedBrush(QBrush(Qt::black));
    text_altNum->setFont(QFont(font.defaultFamily(),9));
    text_altNum->setPositionAlignment(Qt::AlignRight|Qt::AlignBottom);
    text_altNum->setLayer("tools3");
    text_altNum->setSelectable(false);
    text_altNum->setVisible(false);

    line_sensor = new QCPItemStraightLine(plotSensor);
    line_sensor->setPen(pen);
    line_sensor->setLayer("trace");
    line_sensor->setSelectedPen(pen);
    line_sensor->point1->setCoords(0, 0);
    line_sensor->point2->setCoords(1, 0);
    line_sensor->setVisible(false);

    text_sensor = new QCPItemText(plotSensor);
    text_sensor->setText(" "+ numberText +" ");
    text_sensor->setColor(Qt::yellow);
    text_sensor->setBrush(QBrush(Qt::black));
    text_sensor->setSelectedBrush(QBrush(Qt::black));
    text_sensor->setFont(QFont(font.defaultFamily(),9));
    text_sensor->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
    text_sensor->setLayer("trace");
    text_sensor->setSelectable(false);
    text_sensor->setVisible(false);
}

void targetItem :: resizeIcon(double width, double height, double x, double y) {
    if (y > 0) {
        xCoord = x;
        yCoord = y;
    }

    if (icon_lf != nullptr) {
        QCustomPlot *plotLf = icon_lf->parentPlot();
        double widthM = (plotLf->xAxis->range().upper - plotLf->xAxis->range().lower);
        double heightM = (plotLf->yAxis->range().upper - plotLf->yAxis->range().lower);
        double xLfRes = width, yLfRes = height;
        int plotHeight = (plotLf->height() - plotLf->axisRect()->margins().top() - plotLf->axisRect()->margins().bottom());
        int plotWidth = (plotLf->width() - plotLf->axisRect()->margins().left() - plotLf->axisRect()->margins().right());
        xLfRes *= widthM / (plotWidth*1.0);
        yLfRes *= heightM / (plotHeight*1.0);
        icon_lf->topLeft->setCoords(xCoord - xLfRes/2, yCoord + yLfRes);
        icon_lf->bottomRight->setCoords(xCoord + xLfRes/2, yCoord);
        if (text_lf != nullptr)
            text_lf->position->setCoords(xCoord, yCoord + yLfRes);
    }

    if (icon_hf != nullptr) {
        QCustomPlot *plotHf = icon_hf->parentPlot();
        double widthHfM = (plotHf->xAxis->range().upper - plotHf->xAxis->range().lower);
        double heighthFM = (plotHf->yAxis->range().upper - plotHf->yAxis->range().lower);
        double xHfRes = width, yHfRes = height;
        int plotHeight = (plotHf->height() - plotHf->axisRect()->margins().top() - plotHf->axisRect()->margins().bottom());
        int plotWidth = (plotHf->width() - plotHf->axisRect()->margins().left() - plotHf->axisRect()->margins().right());
        xHfRes *= widthHfM / (plotWidth*1.0);
        yHfRes *= heighthFM / (plotHeight*1.0);
        icon_hf->topLeft->setCoords(xCoord - xHfRes/2, yCoord + yHfRes);
        icon_hf->bottomRight->setCoords(xCoord + xHfRes/2, yCoord);
        if (text_hf != nullptr)
            text_hf->position->setCoords(xCoord, yCoord + yHfRes);
    }
}

void targetItem :: setGpsLinePosition(QPointF center, QPointF port, QPointF stbd) {
    if (line_chart_port != nullptr && line_chart_stbd != nullptr) {
        line_chart_port->start->setCoords(center);
        line_chart_stbd->start->setCoords(center);
        line_chart_port->end->setCoords(port);
        line_chart_stbd->end->setCoords(stbd);
        line_chart_port->setVisible(true);
        line_chart_stbd->setVisible(true);
    }
}

void targetItem :: setAltItemPosition(double maxAlt, double x_text, double y) {

    if (line_alt != nullptr) {
        line_alt->start->setCoords(0, y);
        line_alt->end->setCoords(maxAlt, y);
        if (text_altNum != nullptr)     text_altNum->position->setCoords(maxAlt, y);
    }
}

void targetItem :: setSensorItemPosition(double y) {
    if (line_sensor != nullptr) {
        line_sensor->point1->setCoords(0, y);
        line_sensor->point2->setCoords(1, y);
    }
    if (text_sensor != nullptr)     text_sensor->position->setCoords(0, y);
}

void targetItem :: showScanLines(bool state) {
    if (line_alt != nullptr)        line_alt->setVisible(state);
    if (text_altNum != nullptr)     text_altNum->setVisible(state);
    if (text_sensor != nullptr)     text_sensor->setVisible(state);
    if (line_sensor != nullptr)     line_sensor->setVisible(state);
    if (line_chart_port != nullptr)        line_chart_port->setVisible(state);
    if (line_chart_stbd != nullptr)        line_chart_stbd->setVisible(state);
}

void targetItem :: removeIconFromLfHfPlot() {
    if (icon_lf != nullptr) {
        icon_lf->parentPlot()->removeItem(icon_lf);
        icon_lf = nullptr;
    }
    if (text_lf != nullptr) {
        text_lf->parentPlot()->removeItem(text_lf);
        text_lf = nullptr;
    }
    if (icon_hf != nullptr) {
        icon_hf->parentPlot()->removeItem(icon_hf);
        icon_hf = nullptr;
    }
    if (text_hf != nullptr) {
        text_hf->parentPlot()->removeItem(text_hf);
        text_hf = nullptr;
    }
    if (line_chart_stbd != nullptr) {
        line_chart_stbd->parentPlot()->removeItem(line_chart_stbd);
        line_chart_stbd = nullptr;
    }
    if (line_chart_port != nullptr) {
        line_chart_port->parentPlot()->removeItem(line_chart_port);
        line_chart_port = nullptr;
    }
    if (line_alt != nullptr) {
        line_alt->parentPlot()->removeItem(line_alt);
        line_alt = nullptr;
    }
    if (line_sensor != nullptr) {
        line_sensor->parentPlot()->removeItem(line_sensor);
        line_sensor = nullptr;
    }
    if (text_altNum != nullptr) {
        text_altNum->parentPlot()->removeItem(text_altNum);
        text_altNum = nullptr;
    }
    if (text_sensor != nullptr) {
        text_sensor->parentPlot()->removeItem(text_sensor);
        text_sensor = nullptr;
    }

    yCoord = -1;
    selected = false;
}

void targetItem :: removeIconFromAllPlots() {
    if (icon_chart != nullptr) {
        icon_chart->parentPlot()->removeItem(icon_chart);
        icon_chart = nullptr;
    }
    if (text_chart != nullptr) {
        text_chart->parentPlot()->removeItem(text_chart);
        text_chart = nullptr;
    }
    removeIconFromLfHfPlot();
}

QPixmap targetItem :: getIconPixmap(bool solidIcon, int type) {
    if (solidIcon) {
        if (type == 0)   return QPixmap(":/icons/icons/pin_solid_red.png");
        else if (type == 1)   return QPixmap(":/icons/icons/pin_solid_yellow.png");
        else if (type == 2)   return QPixmap(":/icons/icons/pin_solid_blue.png");
        else if (type == 3)   return QPixmap(":/icons/icons/pin_solid_green.png");
        else   return QPixmap(":/icons/icons/pin_solid_grey.png");
    } else {
        if (type == 0)   return QPixmap(":/icons/icons/pin_hollow_red.png");
        else if (type == 1)   return QPixmap(":/icons/icons/pin_hollow_yellow.png");
        else if (type == 2)   return QPixmap(":/icons/icons/pin_hollow_blue.png");
        else if (type == 3)   return QPixmap(":/icons/icons/pin_hollow_green.png");
        else   return QPixmap(":/icons/icons/pin_hollow_grey.png");
    }
}

QPen targetItem :: getAltLinePen(bool selected, int type) {
    QPen pen;
    pen.setWidthF(2);
    pen.setStyle(Qt::SolidLine);
    if (selected == 0)      pen.setStyle(Qt::DotLine);
    else                    pen.setStyle(Qt::SolidLine);
    if (selected == 1)      pen.setWidthF(5);
    if (type == 0)          pen.setColor(QColor(255,0,0));
    else if (type == 1)     pen.setColor(QColor(255,255,0));
    else if (type == 2)     pen.setColor(QColor(0,0,255));
    else if (type == 3)     pen.setColor(QColor(0,255,0));
    else                    pen.setColor(QColor(255,0,0));
    return pen;
}

void targetItem :: updateTargetIndex(int newIndex, QFont font) {
    listIndex = newIndex;

    QString numberText = QString::number(listIndex+1);
    if (baseWpt != nullptr) {
        numberText = QString::number(baseWpt->listIndex+1);
        if (baseWptNum > 1) {
            QByteArray num = QByteArray::number(60+baseWptNum-1);
            QByteArray num2 = QByteArray::fromHex(num);
            numberText += QString::fromLatin1(num2);
        }
    }

    if (text_lf != nullptr)
        text_lf->setText(" "+ numberText +" ");
    if (text_hf != nullptr)
        text_hf->setText(" "+ numberText +" ");
    if (text_chart != nullptr)
        text_chart->setText(" "+ numberText +" ");
    if (text_altNum != nullptr)
        text_altNum->setText(" "+ numberText +" ");
    if (text_sensor != nullptr)
        text_sensor->setText(" "+ numberText +" ");
    QString text = determineName(Name, font);
    ListItem->setText(text + determineAreaText());
}

void targetItem :: renameTarget(QString newName) {
    QFile imageFile;
    imageFile.setFileName(imagePath);
    imageFile.open(QIODevice::ReadOnly);

    imageName.replace(Name, newName);
    imagePath.replace(Name, newName);
    imageFile.rename(imagePath);

    Name = newName;
}

void targetItem :: copyTarget(QString newName) {
    QFile imageFile;
    imageFile.setFileName(imagePath);
    imageFile.open(QIODevice::ReadOnly);

    imageName = newName.mid(newName.lastIndexOf("/")+1);
    imagePath = newName;
    imageFile.copy(newName);
}

QString targetItem :: determineName(QString text, QFont font, int wptListLength) {
    QFontMetrics fm(font);
    int tabWidth = fm.horizontalAdvance("\t\t");
    int spaceWidth = fm.horizontalAdvance("        ");
    QString numberText = QString::number(listIndex+1);
    if (baseWpt != nullptr) {
        numberText = QString::number(baseWpt->listIndex+1);
        if (baseWptNum > 1) {
            QByteArray num = QByteArray::number(60+baseWptNum-1);
            QByteArray num2 = QByteArray::fromHex(num);
            numberText += QString::fromLatin1(num2);
            qDebug() << "Test";
        }
    }
    int numberWidth = fm.horizontalAdvance(numberText);
    while (1) {
        numberText.append(" ");
        numberWidth = fm.horizontalAdvance(numberText);
        if (numberWidth > spaceWidth)   break;
    }
    text.prepend(numberText);

    int textWidth = fm.horizontalAdvance(text);
    if (textWidth < tabWidth/2) {
        return text + "\t\t";
    } else if (textWidth < tabWidth) {
        return text + "\t";
    } else {
        while (1) {
            text.remove(text.length()-1, 1);
            textWidth = fm.horizontalAdvance(text + "...  ");
            if (textWidth < tabWidth) {
                return text + "...\t";
            }
        }
    }
}

QString targetItem :: determineAreaText() {
    // QString::number(obj.area, 'f', 1) + " m²"
    if (area < 100)
        return "⬤";
    else if (area < 200)
        return "⬤⬤";
    else
        return "⬤⬤⬤";
}


waypointItem :: waypointItem() {};

waypointItem :: waypointItem(targetItem item) {
    this->targetItem::operator=(item);
}

waypointItem :: waypointItem(int index, QString name, double Lat, double Lon, int PingNum, int SampleNum, double x, double y) {
    //
    listIndex = index;
    Name = name;
    Lattitude = Lat;
    Longitude = Lon;
    ping = PingNum;
    sample = SampleNum;
    xCoord = x;
    yCoord = y;
}

void waypointItem :: removeItemFromList() {
    // Remove all icons
    if (icon_lf != nullptr) {
        icon_lf->parentPlot()->removeItem(icon_lf);
        icon_lf = nullptr;
    }
    if (text_lf != nullptr) {
        text_lf->parentPlot()->removeItem(text_lf);
        text_lf = nullptr;
    }
    if (icon_hf != nullptr) {
        icon_hf->parentPlot()->removeItem(icon_hf);
        icon_hf = nullptr;
    }
    if (text_hf != nullptr) {
        text_hf->parentPlot()->removeItem(text_hf);
        text_hf = nullptr;
    }
    if (icon_chart != nullptr) {
        icon_chart->parentPlot()->removeItem(icon_chart);
        icon_chart = nullptr;
    }
    if (text_chart != nullptr) {
        text_chart->parentPlot()->removeItem(text_chart);
        text_chart = nullptr;
    }
    if (line_chart_port != nullptr) {
        line_chart_port->parentPlot()->removeItem(line_chart_port);
        line_chart_port = nullptr;
    }
    if (line_chart_stbd != nullptr) {
        line_chart_stbd->parentPlot()->removeItem(line_chart_stbd);
        line_chart_stbd = nullptr;
    }
    if (line_alt != nullptr) {
        line_alt->parentPlot()->removeItem(line_alt);
        line_alt = nullptr;
    }
    if (line_sensor != nullptr) {
        line_sensor->parentPlot()->removeItem(line_sensor);
        line_sensor = nullptr;
    }
    if (text_altNum != nullptr) {
        text_altNum->parentPlot()->removeItem(text_altNum);
        text_altNum = nullptr;
    }
    if (text_sensor != nullptr) {
        text_sensor->parentPlot()->removeItem(text_sensor);
        text_sensor = nullptr;
    }
    if (scanArea_lf != nullptr) {
        scanArea_lf->parentPlot()->removeItem(scanArea_lf);
        scanArea_lf = nullptr;
    }

    for (int i = scanAreaCircle.count() - 1; i >= 0; i--) {
        scanAreaCircle[i]->parentPlot()->removeItem(scanAreaCircle[i]);
    }

    // Remove the QListWidgetItem
    if (ListItem != nullptr) {
        delete ListItem;
        ListItem = nullptr;
    }
}

void waypointItem :: deleteItem() {
    removeItemFromList();
    deleteItemImage();
    for (int i = 0; i < ConvertedTargets.length(); i++) {
        ConvertedTargets[i]->baseWptNum = 0;
        ConvertedTargets[i]->baseWpt = nullptr;
    }
}

void waypointItem :: addScanAreaToSonarPlot(QCustomPlot *plotLf, int type, double radius, double yCrossPoint, GnssController *gnss) {
    if (scanArea_lf == nullptr && 0) {
        QPen pen = getAltLinePen(false, type);
        pen.setWidthF(3);
        pen.setStyle(Qt::SolidLine);

        scanArea_lf = new QCPItemRect(plotLf);
        scanArea_lf->setSelectable(false);
        scanArea_lf->setPen(pen);
        scanArea_lf->setLayer("tools2");
        scanArea_lf->setVisible(true);
    }

//    if (text_lf == nullptr) {
//        QFont font;
//        text_lf = new QCPItemText(plotLf);
//        text_lf->setText(" "+ QString::number(listIndex+1) +" ");
//        text_lf->setColor(Qt::yellow);
//        text_lf->setBrush(QBrush(Qt::black));
//        text_lf->setSelectedBrush(QBrush(Qt::black));
//        text_lf->setFont(QFont(font.defaultFamily(),9));
//        text_lf->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
//        text_lf->setLayer("tools3");
//        text_lf->setSelectable(false);
//    }

    if (yCrossPoint > 0) {
        double y = yCrossPoint + previousDistance * cos(previousBearing * M_PI/180);
        double x = previousDistance;
        if (Tracker::wrapAngle(previousBearing) < 0)    x *= -1;
        xCoord = previousDistance;
        if (previousBearing < 0)    xCoord *= -1;
        yCoord = yCrossPoint + previousDistance * cos(previousBearing * M_PI/180);
        if (yMax < 0) {
            yMin = y - radius;
            yMax = y + radius;
            xMin = x - radius;
            xMax = x + radius;
            gnssInstance = *gnss;
        } else {
            int timeCurrent = gnss->Hour() * 3600 + gnss->Minute() * 60 + gnss->Second();
            int timeStart = gnssInstance.Hour() * 3600 + gnssInstance.Minute() * 60 + gnssInstance.Second();
            if (timeCurrent > timeStart + 60)   return;
            if (yMin > y - radius)      yMin = y - radius;
            if (yMax < y + radius)      yMax = y + radius;
            if (xMin > x - radius)      xMin = x - radius;
            if (xMax < x + radius)      xMax = x + radius;
        }
        xCoord = (xMin + xMax) / 2;
        yCoord = (yMin + yMax) / 2;

//        QPen pen = getAltLinePen(false, type);
//        pen.setWidthF(3);
//        pen.setStyle(Qt::SolidLine);

//        QCPItemEllipse *scanArea = new QCPItemEllipse(plotLf);
//        scanArea->setSelectable(false);
//        scanArea->setPen(pen);
//        scanArea->setLayer("tools2");
//        scanArea->setVisible(true);

//        scanArea->topLeft->setCoords(x - radius, y + radius);
//        scanArea->bottomRight->setCoords(x + radius, y - radius);
//        scanAreaCircle.append(scanArea);
    } else {
        scanArea_lf->setVisible(false);
        text_lf->setVisible(false);
    }

    if (scanArea_lf != nullptr) {
        scanArea_lf->topLeft->setCoords(xMin, yMax);
        scanArea_lf->bottomRight->setCoords(xMax, yMin);
        if (text_lf != nullptr)
            text_lf->position->setCoords(xCoord, yCoord);
    }

//    icon_hf = new QCPItemPixmap(plotHf);
//    icon_hf->setPixmap(pixmapHf);
//    icon_hf->setScaled(true);
//    icon_hf->setLayer("tools2");
//    icon_hf->setVisible(true);

//    text_hf = new QCPItemText(plotHf);
//    text_hf->setText(" "+ QString::number(itemIndex+1) +" ");
//    text_hf->setColor(Qt::yellow);
//    text_hf->setBrush(QBrush(Qt::black));
//    text_hf->setSelectedBrush(QBrush(Qt::black));
//    text_hf->setFont(QFont(font.defaultFamily(),9));
//    text_hf->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
//    text_hf->setLayer("tools3");
//    text_hf->setSelectable(false);
}

void waypointItem :: removeScanAreaFromPlot() {
    if (scanArea_lf != nullptr) {
        scanArea_lf->parentPlot()->removeItem(scanArea_lf);
        scanArea_lf = nullptr;
    }
    if (text_lf != nullptr) {
        text_lf->parentPlot()->removeItem(text_lf);
        text_lf = nullptr;
    }
    for (int i = scanAreaCircle.count() - 1; i >= 0; i--) {
        scanAreaCircle[i]->parentPlot()->removeItem(scanAreaCircle[i]);
    }

    yMin = -1;
    yMax = -1;
    xMin = 0;
    xMax = 0;
}

void waypointItem :: updateTargetIndex(int newIndex, QFont font) {
    listIndex = newIndex;
    QString text = " "+ QString::number(listIndex+1) +" ";
    //    if (startPoint)     text = " "+ QString::number(listIndex+1) +"* ";
    if (text_lf != nullptr)
        text_lf->setText(text);
    if (text_hf != nullptr)
        text_hf->setText(text);
    if (text_chart != nullptr)
        text_chart->setText(text);
    if (text_altNum != nullptr)
        text_altNum->setText(text);
    if (text_sensor != nullptr)
        text_sensor->setText(text);
    QString textFull = determineName(Name, font);
    ListItem->setText(textFull + determineAreaText());
}

QString waypointItem :: determineName(QString text, QFont font) {
    QFontMetrics fm(font);
    int tabWidth = fm.horizontalAdvance("\t\t");
    int spaceWidth = fm.horizontalAdvance("        ");
    QString numberText = QString::number(listIndex+1);
//    if (startPoint)     numberText += "*";
    int numberWidth = fm.horizontalAdvance(numberText);
    while (1) {
        numberText.append(" ");
        numberWidth = fm.horizontalAdvance(numberText);
        if (numberWidth > spaceWidth)   break;
    }
    text.prepend(numberText);

    int textWidth = fm.horizontalAdvance(text);
    if (textWidth < tabWidth/2) {
        return text + "\t\t";
    } else if (textWidth < tabWidth) {
        return text + "\t";
    } else {
        while (1) {
            text.remove(text.length()-1, 1);
            textWidth = fm.horizontalAdvance(text + "...  ");
            if (textWidth < tabWidth) {
                return text + "...\t";
            }
        }
    }
}

QString waypointItem :: determineAreaText() {
    return "★";
}


void Tracker :: setupPlotItems(QCustomPlot *plotPtr) {
    parentPlot = plotPtr;
    validWpt = false;
    nextWpt = nullptr;
    validBoat = false;

    QPen pen_gps, pen2, pen3;
    pen_gps.setWidthF(3);
    pen_gps.setStyle(Qt::DotLine);
    pen_gps.setColor(Qt::white);
    QFont font;
    pen2.setWidthF(5);
    pen2.setStyle(Qt::SolidLine);
    pen2.setColor(Qt::red);
    pen3 = pen_gps;
    pen3.setStyle(Qt::SolidLine);
    QCPLineEnding head(QCPLineEnding::esFlatArrow);

    distanceLine_back = new QCPItemLine(plotPtr);
    distanceLine_back->setPen(pen2);
    distanceLine_back->setLayer("tools3");
    distanceLine_back->setSelectedPen(pen2);
    distanceLine_back->start->setCoords(-500, -500);
    distanceLine_back->end->setCoords(-500, -500);
    distanceLine_back->setVisible(false);
    distanceLine_back->setSelectable(false);

    distanceLine = new QCPItemLine(plotPtr);
    distanceLine->setPen(pen_gps);
    distanceLine->setLayer("tools3");
    distanceLine->setSelectedPen(pen_gps);
    distanceLine->start->setCoords(-500, -500);
    distanceLine->end->setCoords(-500, -500);
    distanceLine->setHead(head);
    distanceLine->setVisible(false);
    distanceLine->setSelectable(false);

    distanceText = new QCPItemText(plotPtr);
    distanceText->setText(" ");
    distanceText->setColor(Qt::white);
    distanceText->setBrush(QBrush(Qt::black));
    distanceText->setSelectedBrush(QBrush(Qt::black));
    distanceText->setFont(QFont("sans", 20));//setFont(QFont(font.defaultFamily(),9));
    distanceText->setPositionAlignment(Qt::AlignLeft|Qt::AlignTop);
    distanceText->position->setTypeX(QCPItemPosition::ptAbsolute);
    distanceText->position->setTypeY(QCPItemPosition::ptAbsolute);
    distanceText->position->setCoords(130, 30);
    distanceText->setTextAlignment(Qt::AlignLeft);
    distanceText->setLayer("boatLayer");
    distanceText->setSelectable(false);
    distanceText->setVisible(false);

    bearingLine = new QCPItemLine(plotPtr);
    bearingLine->setPen(pen3);
    bearingLine->setLayer("boatLayer");
    bearingLine->setSelectedPen(pen3);
    bearingLine->start->setCoords(-500, -500);
    bearingLine->end->setCoords(-500, -500);
    bearingLine->setHead(head);
    bearingLine->setVisible(false);
    bearingLine->setSelectable(false);

    bearingIcon = new QCPItemPixmap(plotPtr);
    bearingIcon->setLayer("tools1");
    bearingIcon->setPixmap(QPixmap(":/icons/icons/arrow_heading.png").scaled(40,40));
    bearingIcon->topLeft->setCoords(-500, -499);
    bearingIcon->bottomRight->setCoords(-499, -500);
    bearingIcon->setVisible(false);
    bearingIcon->setSelectable(false);
    bearingIcon->setScaled(true);

//    bearingArc = new QCPCurve(plotPtr->xAxis, plotPtr->yAxis);
//    bearingArc->setPen(pen_gps);
//    bearingArc->setLayer("tools1");
//    bearingArc->setVisible(false);
}

void Tracker :: resetRoute() {
    for (int i = 0; i < routeLines.length(); i++) {
        routeLines[i]->parentPlot()->removeItem(routeLines[i]);
    }
    for (int i = 0; i < routeLines.length(); i++) {
        routeLinesBack[i]->parentPlot()->removeItem(routeLinesBack[i]);
    }
    routeLines.clear();
    routeLinesBack.clear();
    route.clear();
}

void Tracker :: appendRoute(waypointItem *waypoint) {
    if (abs(waypoint->Lattitude) > 100 && abs(waypoint->Longitude) > 200)
        return;
    route.append(waypoint);
}

void Tracker :: removeRoutePoint(waypointItem *waypoint) {
    for (int i = 0; i < route.length(); i++) {
        if (route[i] == waypoint) {
            route.removeAt(i);
            return;
        }
    }
}

void Tracker :: displayRoute() {
    if (distanceLine == nullptr)        return;

    QPen pen_gps, pen2;
    pen_gps.setWidthF(3);
    pen_gps.setStyle(Qt::DotLine);
    pen_gps.setColor(Qt::white);
    QFont font;
    pen2.setWidthF(5);
    pen2.setStyle(Qt::SolidLine);
    pen2.setColor(Qt::red);
    QCPLineEnding head(QCPLineEnding::esFlatArrow);

    // Clear List
    for (int i = 0; i < routeLines.length(); i++) {
        routeLines[i]->parentPlot()->removeItem(routeLines[i]);
        routeLinesBack[i]->parentPlot()->removeItem(routeLinesBack[i]);
    }
    routeLines.clear();
    routeLinesBack.clear();
    // Replot Route
    for (int i = 0; i < route.length() - 1; i++) {
        QCPItemLine *line2 = new QCPItemLine(distanceLine->parentPlot());
        line2->setPen(pen2);
        line2->setLayer("tools1");
        line2->setSelectedPen(pen2);
        line2->setSelectable(false);
        line2->start->setCoords(route[i]->Longitude, route[i]->Lattitude);
        line2->end->setCoords(route[i+1]->Longitude, route[i+1]->Lattitude);
        line2->setHead(head);
        routeLinesBack.append(line2);

        QCPItemLine *line = new QCPItemLine(distanceLine->parentPlot());
        line->setPen(pen_gps);
        line->setLayer("tools1");
        line->setSelectedPen(pen_gps);
        line->setSelectable(false);
        line->setVisible(false);
        line->start->setCoords(route[i]->Longitude, route[i]->Lattitude);
        line->end->setCoords(route[i+1]->Longitude, route[i+1]->Lattitude);
        routeLines.append(line);
    }
//    if (route.length() > 0)
//        anchorWaypoint(route.first());
//    else {
//        waypointItem item;
//        anchorWaypoint(&item);
//    }
}

waypointItem* Tracker :: passedWaypoint(waypointItem *item) {
    waypointItem *nullItem = nullptr;
    if (route.length() == 0)        return nullItem;

    // Find the next point along the route and start to track it
    //   If the item is part of the route, then track to the next item along the route
    //   If the item is not part of the route, then track the next item that is part of the route
    //   If the item is either the last item, or past the last item, then stop tracking

    int routeIndex = findWptIndex(item);
    if (routeIndex < 0) {
        // Not part of route
        int i = 0;
        for (i = 0; i < route.length(); i++) {
            if (route[i]->listIndex > item->listIndex) {
                anchorWaypoint(route[i]);
                return route[i];
            }
        }
        if (i == route.length()) {
            resetWptAnchor();
        }
    } else {
        // Part of route
        if (route.length() > routeIndex + 1) {
            // Go to next route point
            anchorWaypoint(route[routeIndex + 1]);
            return route[routeIndex + 1];
        } else {
            resetWptAnchor();
        }
    }
    return nullItem;

    if (item == route[0]) {
        // If it passed the first waypoint in the list, then remove and go to next
        route.removeFirst();
    } else {
        // If it passed another waypoint further in the list, then remove all points upto that point
        int i = 1;
        for (i = 1; i < route.length(); i++) {
            if (item == route[i]) {
                break;
            }
        }
        for (int j = i; j >= 0; j--) {
            route.removeAt(j);
        }
    }
    displayRoute();
    return nullItem;
}

int Tracker :: findWptIndex(waypointItem *item) {
    for (int i = 0; i < route.length(); i++) {
        if (route[i] == item)   return i;
    }
    return -1;
}

void Tracker :: anchorWaypoint(waypointItem *waypoint) {
    if (distanceLine == nullptr || distanceLine_back == nullptr)        return;
    validWpt = ((abs(waypoint->Longitude) < 200 || abs(waypoint->Lattitude) < 100) && waypoint->listIndex >= 0);
    if (!validWpt) {
        text_wpt = "";
        setVisibility();
        distanceLine->end->setCoords(-500, -500);
        distanceLine_back->end->setCoords(-500, -500);
        return;
    }
    nextWpt = waypoint;
    text_wpt = QString::number(waypoint->listIndex + 1);
    distanceLine->end->setCoords(waypoint->Longitude, waypoint->Lattitude);
    distanceLine_back->end->setCoords(waypoint->Longitude, waypoint->Lattitude);
    updateDistanceText();
    updateBearingDisplay(distanceLine->start->coords().x(), distanceLine->start->coords().y(), BoatHeading);
}

void Tracker :: updateBoatPosition(double x, double y, double heading) {
    if (distanceLine == nullptr || distanceLine_back == nullptr)        return;
    validBoat = (abs(x) < 200 || abs(y) < 100);
    distanceLine->start->setCoords(x, y);
    distanceLine_back->start->setCoords(x, y);
    if (heading > -400)     BoatHeading = heading;
    updateDistanceText();
    updateBearingDisplay(x, y, heading);
}

void Tracker :: updateDistanceText() {
    if (distanceLine == nullptr || distanceLine_back == nullptr || distanceText == nullptr)     return;
    if (!validWpt || !validBoat)        return;

    double xStart = distanceLine->start->coords().x(), xEnd = distanceLine->end->coords().x();
    double yStart = distanceLine->start->coords().y(), yEnd = distanceLine->end->coords().y();
    distanceLine->setVisible(true);
    distanceLine_back->setVisible(true);
    distanceText->setVisible(true);

    double distance = calculateGpsDistance(yStart, xStart, yEnd, xEnd);
    text_distance = QString::number(distance, 'f', 1);
    updateText();
}

void Tracker :: updateBearingDisplay(double x, double y, double boatHeading) {
    if (distanceLine == nullptr || bearingLine == nullptr)     return;
    if (!validBoat) {
        setVisibility();
        return;
    }
    // distance line goes from the boat position to the target position
    double xStart = x, xEnd = distanceLine->end->coords().x();
    double yStart = y, yEnd = distanceLine->end->coords().y();
    BoatHeading = boatHeading;
    boatHeading = wrapAngle(boatHeading);
    double targetBearing = calculateGpsBearing(yStart, xStart, yEnd, xEnd);
    targetBearing -= boatHeading;
    text_bearing = QString::number(wrapAngle(targetBearing), 'd', 0) + "º";
    updateText();

    // Convert from pixels to meters
    // x meters = (x range [degrees]/width [pixels])*(111000 meters / 1 degree N/S)*50 pixels
    double dist = parentPlot->xAxis->range().size()/parentPlot->width()*111000*30;
    double xNew = 0, yNew = 0;
    calculateGpsEndPoints(y, x, boatHeading, dist, &yNew, &xNew);
    bearingLine->start->setCoords(x, y);
    bearingLine->end->setCoords(xNew, yNew);
    bearingLine->setVisible(true);
}

void Tracker :: updateBearingLineSize() {
    if (distanceLine == nullptr || bearingLine == nullptr)     return;
    if (!validBoat) {
        setVisibility();
        return;
    }
    double xStart = bearingLine->start->coords().x();
    double yStart = bearingLine->start->coords().y();
    double dist = parentPlot->xAxis->range().size()/parentPlot->width()*111000*30;
    double xNew = 0, yNew = 0;
    calculateGpsEndPoints(yStart, xStart, BoatHeading, dist, &yNew, &xNew);

    bearingLine->end->setCoords(xNew, yNew);
    bearingLine->setVisible(true);
//    updateBearingIcon(xStart, yStart, BoatHeading);
}

void Tracker :: updateBearingIcon(double x, double y, double heading) {
    if (bearingIcon == nullptr)      return;
    QPixmap icon(":/icons/icons/arrow_heading.png");
    QMatrix rm;
    QMatrix scaleMat(1, 0, 1, 0, 0, 0);
    QMatrix origin(0, 0, -1, 0, 0, 0);
    rm.rotate(heading);
    scaleMat.rotate(heading);
    origin.rotate(heading);
    QPixmap pixmap2 = icon.transformed(rm);
    double rScale = 35*qMax(abs(scaleMat.m11()), abs(scaleMat.m21()));

    double dist = parentPlot->xAxis->range().size()/parentPlot->width()*1;
    double dx = dist*sin(BoatHeading*M_PI/180.0), dy = dist*cos(BoatHeading*M_PI/180.0);
    double width = (parentPlot->xAxis->range().upper - parentPlot->xAxis->range().lower)/(parentPlot->width()*1.0);
    double height = (parentPlot->yAxis->range().upper - parentPlot->yAxis->range().lower)/(parentPlot->height()*1.0);
    double dx_tl = width*(qMax(abs(scaleMat.m11()), abs(scaleMat.m21())))*40;
    double dy_tl = height*(qMax(abs(scaleMat.m11()), abs(scaleMat.m21())))*40;
    double x_org = 0.001*origin.m11(), y_org = 0.001*origin.m21();
    bearingIcon->setPixmap(pixmap2);
    bearingIcon->setVisible(false);//(true);
    bearingIcon->topLeft->setCoords(x - dx_tl, y + dy_tl);
    bearingIcon->bottomRight->setCoords(x + dx_tl, y - dy_tl);
}

void Tracker :: updateText() {
    if (distanceText == nullptr)     return;
    if (!validBoat || !validWpt)    return;
    distanceText->setText(text_wpt + " - " + text_distance + "\n" + text_bearing);
    distanceText->setVisible(true);
}

void Tracker :: setVisibility(bool hideAll) {
    if (hideAll) {
        if (distanceLine != nullptr)        distanceLine->setVisible(false);
        if (distanceLine_back != nullptr)   distanceLine_back->setVisible(false);
        if (distanceText != nullptr)        distanceText->setVisible(false);
        if (bearingLine != nullptr)        bearingLine->setVisible(false);
        return;
    }
    if (distanceLine != nullptr)        distanceLine->setVisible(validBoat && validWpt);
    if (distanceLine_back != nullptr)   distanceLine_back->setVisible(validBoat && validWpt);
    if (distanceText != nullptr)        distanceText->setVisible(validBoat && validWpt);
    if (bearingLine != nullptr)        bearingLine->setVisible(validBoat);
    if (bearingIcon != nullptr)        bearingIcon->setVisible(false);
}

void Tracker :: remove() {
    if (distanceLine != nullptr)        parentPlot->removeItem(distanceLine);
    if (distanceLine_back != nullptr)   parentPlot->removeItem(distanceLine_back);
    if (distanceText != nullptr)        parentPlot->removeItem(distanceText);
    if (bearingLine != nullptr)        parentPlot->removeItem(bearingLine);
    if (bearingIcon != nullptr)        parentPlot->removeItem(bearingIcon);
//    if (bearingArc != nullptr)        parentPlot->removePlottable(bearingArc);
    distanceLine = nullptr;
    distanceLine_back = nullptr;
    distanceText = nullptr;
    bearingLine = nullptr;
    bearingIcon = nullptr;
    validBoat = false;
    validWpt = false;
    nextWpt = nullptr;
//    bearingArc = nullptr;
}

void Tracker :: resetWptAnchor() {
    validWpt = false;
    nextWpt = nullptr;
    if (distanceLine != nullptr) {
//        distanceLine->start->setCoords(-500, -500);
        distanceLine->end->setCoords(-500, -500);
        distanceLine->setVisible(false);
    }
    if (distanceLine_back != nullptr) {
//        distanceLine_back->start->setCoords(-500, -500);
        distanceLine_back->end->setCoords(-500, -500);
        distanceLine_back->setVisible(false);
    }
    if (distanceText != nullptr) {
        distanceText->setText(" ");
        distanceText->setVisible(false);
    }
    if (bearingLine != nullptr) {
//        bearingLine->start->setCoords(-500, -500);
//        bearingLine->end->setCoords(-500, -500);
//        bearingLine->setVisible(false);
    }
    if (bearingIcon != nullptr) {
        bearingIcon->setVisible(false);
    }
}

double Tracker :: wrapAngle(double x){
    x = fmod(x + 180,360);
    if (x < 0)
        x += 360;
    return x - 180;
}

double Tracker :: calculateGpsDistance(double LatStart, double LonStart, double LatEnd, double LonEnd) {
    // Calculate the distance between two points
    double c;
    double r = 6371000.0; //the radius of the earth
    c = qSin(LatEnd * M_PI/180) * qSin(LatStart * M_PI/180) * qCos((LonEnd - LonStart) * M_PI/180)
            + qCos(LatEnd * M_PI/180) * qCos(LatStart * M_PI/180); //Calcs distance between boat gps and flag gps
    return r * qAcos(c);
}

double Tracker :: calculateGpsBearing(double LatOld, double LonOld, double LatNew, double LonNew) {
    double bearingNew = qAtan2(qSin((LonNew - LonOld) * M_PI/180) * qCos(LatNew * M_PI/180),
                               qCos(LatOld * M_PI/180) * qSin(LatNew*M_PI/180)
                               - qSin(LatOld * M_PI/180) * qCos(LatNew * M_PI/180) * qCos((LonNew - LonNew) * M_PI/180));
    return bearingNew * 180 / M_PI;
//    double bearing = qAtan((LonNew - LonOld) / (LatNew - LatOld)) / M_PI*180;
//    if (LatNew - LatOld < 0)        bearing += 180;
//    if (abs(LatNew - LatOld) < 0.00001) {
//        bearing = 90;
//        if (LonNew - LonOld < 0)
//            bearing = -90;
//    }
//    return bearing;
}

void Tracker :: calculateGpsEndPoints(double LatOld, double LonOld, double angle, double length, double *LatNew, double *LonNew) {
    //Convert lat1, lat2 and bearingToBoat to radians first
    double lat1 = qDegreesToRadians(LatOld);
    double lon1 = qDegreesToRadians(LonOld);
    angle *= M_PI/180;

    //Formula for calculating lat2, given lat1, bearing and distance
    double lat2;
    *LatNew = qRadiansToDegrees(qAsin(qSin(lat1) * qCos(length/6378137.0) + qCos(lat1) * qSin(length/6378137.0) * qCos(angle)));

    //Forumla for calculating lon2, given lat1, bearing, distance, and lat2
    double lon2;
    *LonNew = qRadiansToDegrees(lon1 + qAtan2((qSin(angle)) * qSin(length/6378137.0) * qCos(lat1), (qCos(length/6378137.0) - qSin(lat1) * qSin(qDegreesToRadians(*LatNew)))));
}

// Measuring Tape Items

void measuringItem :: setStartPoint(double x, double y, QCustomPlot *plotPtr) {
    coordinates.clear();
    start_point.setX(x);
    start_point.setY(y);
    end_point.setX(x);
    end_point.setY(y);
    start_measuring = true;

    //measuring tape on cursor move//
    QCPItemLine *tmpline1 = new QCPItemLine(plotPtr);
    QCPItemLine *tmpline2 = new QCPItemLine(plotPtr);

    line_back.append(tmpline1);
    line_top.append(tmpline2);
//    createText(x, y, plotPtr);
    setLine(x, y, x, y);
    coordinates.append(start_point);
}

void measuringItem :: addPoint(double x, double y, QCustomPlot *plotPtr) {

    double distance = qSqrt(qPow(y-end_point.y(),2)+qPow(x-end_point.x(),2));
    setLine(end_point.x(), end_point.y(), x, y);
    length += distance;

    end_point.setX(x);
    end_point.setY(y);

    start_measuring = true;
    start_point.setX(x);
    start_point.setY(y);
    coordinates.append(start_point);

    QCPItemLine *tmpline1 = new QCPItemLine(plotPtr);
    QCPItemLine *tmpline2 = new QCPItemLine(plotPtr);

    line_back.append(tmpline1);
    line_top.append(tmpline2);
    setLine(x, y, x, y);

}

void measuringItem :: setEndPoint(double x, double y, QCustomPlot *plotPtr) {

    double distance = qSqrt(qPow(y-end_point.y(),2)+qPow(x-end_point.x(),2));
    if (line_back.count() > 1 &&
            abs(plotPtr->xAxis->coordToPixel(x) - plotPtr->xAxis->coordToPixel(line_back.at(0)->start->coords().x())) < 10 &&
            abs(plotPtr->yAxis->coordToPixel(y) - plotPtr->yAxis->coordToPixel(line_back.at(0)->start->coords().y())) < 10) {
        // If the first end is within 10 pixels of the mouse, then snap tape
        x = line_back.at(0)->start->coords().x();
        y = line_back.at(0)->start->coords().y();
    } else if (abs(plotPtr->xAxis->coordToPixel(x) - plotPtr->xAxis->coordToPixel(start_point.x())) < 3 &&
               abs(plotPtr->yAxis->coordToPixel(y) - plotPtr->yAxis->coordToPixel(start_point.y())) < 3) {
        return;
    } else if (shadowTool) {
        // Set coords of end of measuring tape - it should line up with the ping
        // Restrict the user to only move inwards from the start point
        double tanDegree = 0;
        if (shadowAngle < 88)   tanDegree = qCos(shadowAngle * M_PI / 180);
        double xStart = start_point.x();
        double yStart = start_point.y();
        double xCoord1 = x - xStart;
        double yCoord1 = yStart;
        y = -abs(xCoord1*tanDegree) + yCoord1;

        if (abs(x) > abs(xStart)) {
            x = xStart+0.001;
            y = yStart+0.001;
        }
        else if ((x < 0 && xStart > 0) || (x > 0 && xStart < 0)) {
            x = 0;
            y = -abs(xStart*tanDegree) + yCoord1;
        }
    }
    setLine(end_point.x(), end_point.y(), x, y);
    length += distance;

    end_point.setX(x);
    end_point.setY(y);

    coordinates.append(end_point);

    // Calculate Area
    double totalArea = 0;
    if (abs(line_back.at(0)->start->coords().x() - x) < 0.001 &&
            abs(line_back.at(0)->start->coords().y() - y) < 0.001 &&
            line_back.length() > 1) {
        double xAvg = 0, yAvg = 0;
        for (int i = 0; i < line_back.length(); i++) {
            // Collect coordinates of each line
            double x_1 = line_back.at(i)->start->coords().x();
            double y_1 = line_back.at(i)->start->coords().y();
            double x_2 = line_back.at(i)->end->coords().x();
            double y_2 = line_back.at(i)->end->coords().y();

            xAvg += x_1;
            yAvg += y_1;

            // Calculate area using shoelace formula
            totalArea += (x_1*y_2 - x_2*y_1)/2.0;
            //if (x_1)
            //activeMeasuringTapeObject.
        }
        xAvg /= (line_back.length()*1.0);
        yAvg /= (line_back.length()*1.0);

        QFont font;
        areaText = new QCPItemText(plotPtr);
        areaText->setText(QString::number(abs(totalArea),'f',2)+" m²");
        areaText->setColor(Qt::yellow);
        areaText->setBrush(QBrush(Qt::black));
        areaText->setSelectedBrush(QBrush(Qt::black));
        areaText->setFont(QFont(font.family(),9));
        areaText->position->setCoords(xAvg,yAvg);
        areaText->setLayer("tools3");
        areaText->setSelectedColor(Qt::yellow);
    }

    QFont font;
    distanceText = new QCPItemText(plotPtr);
    distanceText->setText(QString::number(length,'f',2)+" m");
    distanceText->setColor(Qt::yellow);
    distanceText->setBrush(QBrush(Qt::black));
    distanceText->setSelectedBrush(QBrush(Qt::black));
    distanceText->setFont(QFont(font.defaultFamily(),9));
    distanceText->setSelectedColor(Qt::yellow);
    distanceText->setSelectedFont(QFont(font.defaultFamily(),9));
    distanceText->setSelected(false);
    if (start_point.y() > end_point.y()) {
        distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignTop);
        distanceText->position->setCoords(end_point.x(), end_point.y() - 0.5);
    } else {
        distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignBottom);
        distanceText->position->setCoords(end_point.x(), end_point.y() + 0.5);
    }
    distanceText->setLayer("tools3");

    start_measuring = false;
    length = 0;
}

bool measuringItem :: setShadowEndPoint(double x, double y, QCustomPlot *plotPtr, double shadowCoeff) {

    double distance = qSqrt(qPow(y-end_point.y(),2)+qPow(x-end_point.x(),2));
    length += distance;
    if (abs(plotPtr->xAxis->coordToPixel(x) - plotPtr->xAxis->coordToPixel(start_point.x())) < 3 &&
            abs(plotPtr->yAxis->coordToPixel(y) - plotPtr->yAxis->coordToPixel(start_point.y())) < 3)  {
        return false;
    }
    // Set coords of end of measuring tape - it should line up with the ping
    // Restrict the user to only move inwards from the start point
    double tanDegree = 0;
    if (shadowAngle < 88)   tanDegree = qCos(shadowAngle * M_PI / 180);
    double xStart = start_point.x();
    double yStart = start_point.y();
    double xCoord1 = x - xStart;
    double yCoord1 = yStart;
    y = -abs(xCoord1*tanDegree) + yCoord1;

    if (abs(x) > abs(xStart)) {
        x = xStart+0.001;
        y = yStart+0.001;
    } else if ((x < 0 && xStart > 0) || (x > 0 && xStart < 0)) {
        x = 0;
        y = -abs(xStart*tanDegree) + yCoord1;
    }

    double R2 = qSqrt(qPow(y-yStart,2)+qPow(x-xStart,2));
    double totalDist = abs(shadowCoeff*R2);
    if (totalDist < 0.1)    return false;

    setLine(end_point.x(), end_point.y(), x, y);

    end_point.setX(x);
    end_point.setY(y);

    coordinates.append(end_point);

    QFont font;
    distanceText = new QCPItemText(plotPtr);
    distanceText->setText("Height: "+QString::number(totalDist,'f',2)+" m");
    distanceText->setColor(Qt::yellow);
    distanceText->setBrush(QBrush(Qt::black));
    distanceText->setSelectedBrush(QBrush(Qt::black));
    distanceText->setFont(QFont(font.defaultFamily(),9));
    distanceText->setSelectedColor(Qt::yellow);
    distanceText->setSelectedFont(QFont(font.defaultFamily(),9));
    distanceText->setSelected(false);
    if (start_point.y() > end_point.y()) {
        distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignTop);
        distanceText->position->setCoords(end_point.x(), end_point.y() - 0.5);
    } else {
        distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignBottom);
        distanceText->position->setCoords(end_point.x(), end_point.y() + 0.5);
    }
    distanceText->setLayer("tools3");

    start_measuring = false;
    length = 0;
    return true;
}

void measuringItem :: createText(double x, double y, QCustomPlot *plotPtr) {

    QFont font;
    distanceText = new QCPItemText(plotPtr);
    distanceText->setColor(Qt::yellow);
    distanceText->setBrush(QBrush(Qt::black));
    distanceText->setSelectedBrush(QBrush(Qt::black));
    distanceText->setFont(QFont(font.defaultFamily(),9));
    distanceText->setSelectedColor(Qt::yellow);
    distanceText->setSelectedFont(QFont(font.defaultFamily(),9));
    distanceText->setSelected(false);
    distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignTop);
    distanceText->position->setCoords(x, y - 0.5);
    distanceText->setLayer("tools3");
}

void measuringItem :: setLine(double x1, double y1, double x2, double y2, double shadowCoeff) {

    QPen distancePen1;
    distancePen1.setColor(Qt::black);
    distancePen1.setWidth(5);
    distancePen1.setStyle(Qt::SolidLine);

    QPen distancePen2;
    distancePen2.setColor(Qt::white);
    distancePen2.setWidth(4);
    distancePen2.setStyle(Qt::DotLine);

    line_back.last()->setPen(distancePen1);
    line_back.last()->setSelectedPen(distancePen1);
    line_back.last()->setLayer("tools1");
    line_back.last()->start->setCoords(x1, y1);
    line_back.last()->end->setCoords(x2, y2);

    line_top.last()->setPen(distancePen2);
    line_top.last()->setSelectedPen(distancePen2);
    line_top.last()->setLayer("tools1");
    line_top.last()->start->setCoords(x1, y1);
    line_top.last()->end->setCoords(x2, y2);

    if (distanceText != nullptr) {
        double distance = qSqrt(qPow(y1-y2,2)+qPow(x1-x2,2));
        if (y1 > y2) {
            distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignTop);
            distanceText->position->setCoords(x2, y2 - 0.5);
        } else {
            distanceText->setPositionAlignment(Qt::AlignHCenter | Qt::AlignBottom);
            distanceText->position->setCoords(x2, y2 + 0.5);
        }
        if (shadowTool && shadowCoeff > -0.5) {
            double totalDist = abs(shadowCoeff*distance);
            distanceText->setText("Height: "+QString::number(totalDist,'f',2)+" m");
        } else
            distanceText->setText(QString::number(length+distance,'f',2)+" m");
    }
}

PlotTools :: PlotTools() {

}

void PlotTools :: appendTargetItem(targetItem item) {
    if (item.importedWaypoint) {
        // Convert from target to waypoint
        WaypointListWidget->addItem(item.ListItem);
        waypointItem item2(item);
        waypointList.append(item2);
    } else {
        TargetListWidget->addItem(item.ListItem);
        targetList.append(item);
    }
    renumberTargets();
//    updateListWidget();
}

void PlotTools :: convertWaypointToTarget(waypointItem *item) {
    targetItem newItem;
    newItem = *item;
    item->convertedTgts++;
//    newItem.Name = QString::number(determineNewName(false)).rightJustified(3, '0') + " Target";
    newItem.Name = item->Name;
    newItem.icon_lf = nullptr;
    newItem.text_lf = nullptr;
    newItem.icon_hf = nullptr;
    newItem.text_hf = nullptr;
    newItem.line_chart_stbd = nullptr;
    newItem.line_chart_port = nullptr;
    newItem.line_alt = nullptr;
    newItem.line_sensor = nullptr;
    newItem.text_altNum = nullptr;
    newItem.text_sensor = nullptr;
    newItem.icon_chart = nullptr;
    newItem.text_chart = nullptr;
    newItem.ListItem = nullptr;
    newItem.listIndex = targetList.length();
    newItem.baseWpt = item;
    newItem.baseWptNum = item->convertedTgts;
    newItem.ListItem = new QListWidgetItem(newItem.Name);
    newItem.ListItem->setText(newItem.determineName(newItem.Name, TargetListWidget->font()) + newItem.determineAreaText());
    newItem.ListItem->setIcon(QIcon(":/icons/icons/blankFlagIcon.png"));
    TargetListWidget->addItem(newItem.ListItem);
    item->ConvertedTargets.append(&newItem);
    targetList.append(newItem);
}

void PlotTools :: removeAllWaypointScanAreas() {
    for (int i = 0; i < waypointList.size(); i++){
        waypointList[i].removeScanAreaFromPlot();
    }
}

void PlotTools :: removeAllTargetIcons() {
    for (int i = 0; i < waypointList.size(); i++){
        waypointList[i].removeIconFromLfHfPlot();
        waypointList[i].removeScanAreaFromPlot();
    }
    for (int i = 0; i < targetList.size(); i++){
        targetList[i].removeIconFromLfHfPlot();
    }
}

void PlotTools :: removeAllTargetIconsFromAllPlots() {
    for (int i = 0; i < waypointList.size(); i++){
        waypointList[i].removeIconFromAllPlots();
        waypointList[i].removeScanAreaFromPlot();
    }
    for (int i = 0; i < targetList.size(); i++){
        targetList[i].removeIconFromAllPlots();
    }
    tracker.resetWptAnchor();
}

void PlotTools :: removeAllWaypointsFromList() {
    for (int i = waypointList.size() - 1; i >= 0; i--){
        waypointList[i].removeItemFromList();
        waypointList.removeAt(i);
    }
    tracker.resetWptAnchor();
    tracker.resetRoute();
}

void PlotTools :: removeAllTargetsFromList() {
    for (int i = targetList.size() - 1; i >= 0; i--){
        targetList[i].removeItemFromList();
        targetList.removeAt(i);
    }
    tracker.setVisibility();
}

QList <targetItem*> PlotTools :: selectedTargets() {
    QList <targetItem*> selected;
    for (int i = 0; i < targetList.length(); i++) {
        if (targetList[i].selected) {
            selected.append(&targetList[i]);
        }
    }
    if (selected.length() == 0) {
//        targetItem tmp;
//        selected.append(&tmp);
    }
    return selected;
}

QList <waypointItem*> PlotTools :: selectedWaypoints() {
    QList <waypointItem*> selected;
    for (int i = 0; i < waypointList.length(); i++) {
        if (waypointList[i].selected) {
            selected.append(&waypointList[i]);
        }
    }
    if (selected.length() == 0) {
//        waypointItem tmp;
//        selected.append(&tmp);
    }
    return selected;
}

void PlotTools :: deleteTarget(targetItem *item) {
    int index = item->listIndex;
    for (int i = 0; i < targetList.length(); i++) {
        if (&targetList[i] == item) {
            item->deleteItem();
            targetList.removeAt(i);
            return;
        }
    }
}

void PlotTools :: deleteWaypoint(waypointItem *item) {
    tracker.removeRoutePoint(item);
    for (int i = 0; i < waypointList.length(); i++) {
        if (&waypointList[i] == item) {
            item->deleteItem();
            waypointList.removeAt(i);
            return;
        }
    }
}

int PlotTools :: determineNewName(bool isWaypoint) {
    int maxNum = 0;
    if (isWaypoint) {
        for (int i = 0; i < waypointList.length(); i++) {
            QString name = waypointList[i].Name;
            if (name.mid(4, 12).toLower() == "waypoint") {
                int counterNum = name.mid(0, 3).toInt();
                if (counterNum > maxNum)
                    maxNum = counterNum;
            }
        }
    } else {
        for (int i = 0; i < targetList.length(); i++) {
            QString name = targetList[i].Name;
            if (name.mid(4, 10).toLower() == "target") {
                int counterNum = name.mid(0, 3).toInt();
                if (counterNum > maxNum)
                    maxNum = counterNum;
            }
        }
    }
    return maxNum + 1;
}

void PlotTools :: copyTargetImages(QString newBaseName) {
    for (int i = 0; i < targetList.size(); i++){
        targetList[i].copyTarget(newBaseName + " " + targetList[i].Name + ".png");
    }
}

void PlotTools :: renumberTargets() {
    int wptSize = waypointList.size();
    for (int j = 0; j < wptSize; j++) {
        waypointList[j].updateTargetIndex(j, WaypointListWidget->font());
    }
    for (int i = 0; i < targetList.size(); i++) {
        targetList[i].updateTargetIndex(i, TargetListWidget->font());
    }
}

bool PlotTools :: checkIfTargetNameExists(int index, QString text) {
    // returns 0 if name is available
    for (int i = 0; i < waypointList.size(); i++) {
        if (i == index)     continue;
        if (waypointList[i].Name == text)     return 1;
    }
    for (int i = 0; i < targetList.size(); i++) {
        if (i == index)     continue;
        if (targetList[i].Name == text)     return 1;
    }
    return 0;
}

void PlotTools :: updateAllTargetIcons(int type) {
    for (int i = 0; i < waypointList.size(); i++){
        waypointList[i].selectItem(waypointList[i].selected, type);
    }
    for (int i = 0; i < targetList.size(); i++){
        targetList[i].selectItem(targetList[i].selected, type);
    }
}

void PlotTools :: updateAllTargetSizes() {
    for (int i = 0; i < waypointList.size(); i++){
        waypointList[i].resizeIcon(40, 40);
    }
    for (int i = 0; i < targetList.size(); i++){
        targetList[i].resizeIcon(40, 40);
    }
}

void PlotTools :: selectAllTargets(bool setSelected, int iconType) {
    for (int j = 0; j < targetList.size(); j++) {
        targetList[j].selectItem(setSelected, iconType);
    }
}

void PlotTools :: selectAllWaypoints(bool setSelected, int iconType) {
    for (int j = 0; j < waypointList.size(); j++) {
        waypointList[j].selectItem(setSelected, iconType);
    }
}

bool PlotTools :: isTargetInList(targetItem item, QList<QListWidgetItem*> list) {
    if (list.length() == 0)     return false;
    if (item.importedWaypoint) {
        for (int j = 0; j < list.length(); j++) {
            if (item.ListItem == list[j])
                return true;
        }
    } else {
        for (int j = 0; j < list.length(); j++) {
            if (item.ListItem == list[j])
                return true;
        }
    }
    return false;
}

targetItem PlotTools :: findTargetItem(QListWidgetItem *item) {
    for (int i = 0; i < targetList.length(); i++) {
        if (targetList[i].ListItem == item)
            return targetList[i];
    }
    return targetItem();
}

void PlotTools :: sortAllTargets() {
    TargetListWidget->setCurrentRow(-1);
    for (int i = 0; i < targetList.length()-1; i++) {
        for (int j = i+1; j < targetList.length(); j++) {
            if (targetList[i].ping > targetList[j].ping) {
                QListWidgetItem *jItem = TargetListWidget->takeItem(targetList[j].listIndex);
                TargetListWidget->insertItem(i+1, jItem);
                QListWidgetItem *iItem = TargetListWidget->takeItem(targetList[i].listIndex);
                TargetListWidget->insertItem(j, iItem);

                targetList[j].listIndex = i;
                targetList[i].listIndex = j;

                targetList.swap(i, j);
            }
        }
    }
}

void PlotTools :: sortAllWaypoints(double currentLat, double currentLon) {
    if (currentLat < -100 || currentLon < -200)     return;
    WaypointListWidget->setCurrentRow(-1);
    if (0) {
        // Sort all waypoints by distance
        for (int i = 0; i < waypointList.length()-1; i++) {
            double waypointDistanceI = Tracker::calculateGpsDistance(currentLat, currentLon, waypointList[i].Lattitude, waypointList[i].Longitude);
            for (int j = i+1; j < waypointList.length(); j++) {
                double waypointDistanceJ = Tracker::calculateGpsDistance(currentLat, currentLon, waypointList[j].Lattitude, waypointList[j].Longitude);
                if (waypointDistanceI > waypointDistanceJ) {
                    QListWidgetItem *jItem = WaypointListWidget->takeItem(waypointList[j].listIndex);
                    WaypointListWidget->insertItem(i+1, jItem);
                    QListWidgetItem *iItem = WaypointListWidget->takeItem(waypointList[i].listIndex);
                    WaypointListWidget->insertItem(j, iItem);

                    waypointList[j].listIndex = i;
                    waypointList[i].listIndex = j;
                    waypointDistanceI = waypointDistanceJ;

                    waypointList.swap(i, j);
                }
            }
        }
    }
    // Sort waypoints by most efficient path
    // Find closest waypoint, then set as the current position, then find next closest waypoint, and so on
    for (int i = 0; i < waypointList.length(); i++) {
        double waypointDistanceI = Tracker::calculateGpsDistance(currentLat, currentLon, waypointList[i].Lattitude, waypointList[i].Longitude);
        for (int j = i+1; j < waypointList.length(); j++) {
            double waypointDistanceJ = Tracker::calculateGpsDistance(currentLat, currentLon, waypointList[j].Lattitude, waypointList[j].Longitude);
            if (waypointDistanceI > waypointDistanceJ) {
                QListWidgetItem *jItem = WaypointListWidget->takeItem(waypointList[j].listIndex);
                WaypointListWidget->insertItem(i+1, jItem);
                QListWidgetItem *iItem = WaypointListWidget->takeItem(waypointList[i].listIndex);
                WaypointListWidget->insertItem(j, iItem);

                waypointList[j].listIndex = i;
                waypointList[i].listIndex = j;
                waypointDistanceI = waypointDistanceJ;

                waypointList.swap(i, j);
            }
        }
        currentLat = waypointList[i].Lattitude;
        currentLon = waypointList[i].Longitude;
        waypointList[i].startPoint = false;
    }
    for (int i = 0; i < waypointList.length(); i++) {
        if (waypointList[i].selected)       waypointList[i].ListItem->setSelected(true);
    }

    tracker.resetRoute();
    for (int i = 0; i < waypointList.length(); i++) {
        tracker.appendRoute(&waypointList[i]);
    }
    tracker.displayRoute();
}

void PlotTools :: setStartPoint(waypointItem *item) {
    sortAllWaypoints(item->Lattitude, item->Longitude);
    for (int i = 0; i < waypointList.length()-1; i++) {
        if (&waypointList[i] != item)
            waypointList[i].startPoint = false;
    }
    item->startPoint = true;
    renumberTargets();
//    tracker.resetRoute();
}

void PlotTools :: addNewTapeMeasurement() {

    measuringTapeList.append(activeMeasuringTapeObject);

    activeMeasuringTapeObject.start_measuring = false;
    activeMeasuringTapeObject.distanceText = nullptr;
    activeMeasuringTapeObject.areaText = nullptr;
    activeMeasuringTapeObject.line_back.clear();
    activeMeasuringTapeObject.line_top.clear();
    activeMeasuringTapeObject.lineGps.clear();
    activeMeasuringTapeObject.length = 0;
    activeMeasuringTapeObject.start_point = QPointF(0, 0);
    activeMeasuringTapeObject.end_point = QPointF(0, 0);
}

void PlotTools :: notifyUser() {
    notificationAudio->setVolume(50);
    notificationAudio->play();
}

// Tool Panel

void PlotTools :: updateIconPanel(bool lfPlotClosed, bool target, bool tape, bool height, bool crop) {
    if (panel.legendModeIcon_lf == nullptr || panel.legendModeIcon_hf == nullptr)   return;
    if (panel.separateIcons) {
        panel.legendModeIcon_lf->setVisible(false);
        panel.legendModeIcon_hf->setVisible(false);
        if (target) {
            panel.target_lf->setPixmap(QPixmap(":/icons/icons/location_solid.png"));
            panel.target_hf->setPixmap(QPixmap(":/icons/icons/location_solid.png"));
            panel.target_higlighted = true;
        } else {
            panel.target_lf->setPixmap(QPixmap(":/icons/icons/location_outline.png"));
            panel.target_hf->setPixmap(QPixmap(":/icons/icons/location_outline.png"));
            panel.target_higlighted = false;
        }
        if (tape) {
            panel.tape_lf->setPixmap(QPixmap(":/icons/icons/tape_solid.png"));
            panel.tape_hf->setPixmap(QPixmap(":/icons/icons/tape_solid.png"));
            panel.tape_higlighted = true;
        } else {
            panel.tape_lf->setPixmap(QPixmap(":/icons/icons/tape_outline.png"));
            panel.tape_hf->setPixmap(QPixmap(":/icons/icons/tape_outline.png"));
            panel.tape_higlighted = false;
        }
        if (height) {
            panel.height_lf->setPixmap(QPixmap(":/icons/icons/height_solid.png"));
            panel.height_hf->setPixmap(QPixmap(":/icons/icons/height_solid.png"));
            panel.height_higlighted = true;
        } else {
            panel.height_lf->setPixmap(QPixmap(":/icons/icons/height_outline.png"));
            panel.height_hf->setPixmap(QPixmap(":/icons/icons/height_outline.png"));
            panel.height_higlighted = false;
        }
        if (crop) {
            panel.crop_lf->setPixmap(QPixmap(":/icons/icons/crop_solid.png"));
            panel.crop_hf->setPixmap(QPixmap(":/icons/icons/crop_solid.png"));
            panel.crop_higlighted = true;
        } else {
            panel.crop_lf->setPixmap(QPixmap(":/icons/icons/crop_outline.png"));
            panel.crop_hf->setPixmap(QPixmap(":/icons/icons/crop_outline.png"));
            panel.crop_higlighted = false;
        }
        panel.target_lf->setVisible(true);
        panel.tape_lf->setVisible(true);
        panel.height_lf->setVisible(true);
        panel.crop_lf->setVisible(true);
        if (lfPlotClosed) {
            panel.target_hf->setVisible(true);
            panel.tape_hf->setVisible(true);
            panel.height_hf->setVisible(true);
            panel.crop_hf->setVisible(true);
        } else {
            panel.target_hf->setVisible(false);
            panel.tape_hf->setVisible(false);
            panel.height_hf->setVisible(false);
            panel.crop_hf->setVisible(false);
        }
    } else {
        panel.target_higlighted = false;
        panel.tape_higlighted = false;
        panel.height_higlighted = false;
        panel.crop_higlighted = false;
        panel.target_lf->setVisible(false);
        panel.tape_lf->setVisible(false);
        panel.height_lf->setVisible(false);
        panel.crop_lf->setVisible(false);
        panel.target_hf->setVisible(false);
        panel.tape_hf->setVisible(false);
        panel.height_hf->setVisible(false);
        panel.crop_hf->setVisible(false);
        if (tape) {
            QPixmap mode(":/icons/icons/tape_solid.png");
            panel.legendModeIcon_lf->setPixmap(mode);
            panel.legendModeIcon_lf->setVisible(true);
            if (lfPlotClosed) {
                panel.legendModeIcon_hf->setPixmap(mode);
                panel.legendModeIcon_hf->setVisible(true);
            } else
                panel.legendModeIcon_hf->setVisible(false);
        } else if (height) {
            QPixmap mode(":/icons/icons/height_solid.png");
            panel.legendModeIcon_lf->setPixmap(mode);
            panel.legendModeIcon_lf->setVisible(true);
            if (lfPlotClosed) {
                panel.legendModeIcon_hf->setPixmap(mode);
                panel.legendModeIcon_hf->setVisible(true);
            } else
                panel.legendModeIcon_hf->setVisible(false);
        } else if (target) {
            QPixmap mode(":/icons/icons/location_solid.png");
            panel.legendModeIcon_lf->setPixmap(mode);
            panel.legendModeIcon_lf->setVisible(true);
            if (lfPlotClosed) {
                panel.legendModeIcon_hf->setPixmap(mode);
                panel.legendModeIcon_hf->setVisible(true);
            } else
                panel.legendModeIcon_hf->setVisible(false);
        } else if (crop) {
            QPixmap mode(":/icons/icons/crop_solid.png");
            panel.legendModeIcon_lf->setPixmap(mode);
            panel.legendModeIcon_lf->setVisible(true);
            if (lfPlotClosed) {
                panel.legendModeIcon_hf->setPixmap(mode);
                panel.legendModeIcon_hf->setVisible(true);
            } else
                panel.legendModeIcon_hf->setVisible(false);
        } else {
            QPixmap clear(20,20);
            panel.legendModeIcon_lf->setPixmap(clear);
            panel.legendModeIcon_hf->setPixmap(clear);
            panel.legendModeIcon_lf->setVisible(false);
            panel.legendModeIcon_hf->setVisible(false);
        }
    }
}

void PlotTools :: updatePanelPosition(double xPosition) {
    if (panel.legendModeIcon_lf != nullptr) {
        panel.legendModeIcon_lf->topLeft->setCoords(xPosition - 70,80);
        panel.legendModeIcon_lf->bottomRight->setCoords(xPosition - 30,120);
    }
    if (panel.legendModeIcon_hf != nullptr) {
        panel.legendModeIcon_hf->topLeft->setCoords(xPosition - 70,80);
        panel.legendModeIcon_hf->bottomRight->setCoords(xPosition - 30,120);
    }

    if (panel.target_lf != nullptr) {
        panel.target_lf->topLeft->setCoords(xPosition - 70,80);
        panel.target_lf->bottomRight->setCoords(xPosition - 30,120);
    }
    if (panel.target_hf != nullptr) {
        panel.target_hf->topLeft->setCoords(xPosition - 70,80);
        panel.target_hf->bottomRight->setCoords(xPosition - 30,120);
    }

    if (panel.tape_lf != nullptr) {
        panel.tape_lf->topLeft->setCoords(xPosition - 70,130);
        panel.tape_lf->bottomRight->setCoords(xPosition - 30,170);
    }
    if (panel.tape_hf != nullptr) {
        panel.tape_hf->topLeft->setCoords(xPosition - 70,130);
        panel.tape_hf->bottomRight->setCoords(xPosition - 30,170);
    }

    if (panel.height_lf != nullptr) {
        panel.height_lf->topLeft->setCoords(xPosition - 70,180);
        panel.height_lf->bottomRight->setCoords(xPosition - 30,220);
    }
    if (panel.height_hf != nullptr) {
        panel.height_hf->topLeft->setCoords(xPosition - 70,180);
        panel.height_hf->bottomRight->setCoords(xPosition - 30,220);
    }

    if (panel.crop_lf != nullptr) {
        panel.crop_lf->topLeft->setCoords(xPosition - 70,230);
        panel.crop_lf->bottomRight->setCoords(xPosition - 30,270);
    }
    if (panel.crop_hf != nullptr) {
        panel.crop_hf->topLeft->setCoords(xPosition - 70,230);
        panel.crop_hf->bottomRight->setCoords(xPosition - 30,270);
    }
}

void PlotTools :: addAllPanelIcons(QCustomPlot *plotLf, QCustomPlot *plotHf) {
    panel.legendModeIcon_lf = addPanelIcon(plotLf);
    panel.legendModeIcon_hf = addPanelIcon(plotHf);

    panel.target_lf = addPanelIcon(plotLf);
    panel.target_hf = addPanelIcon(plotHf);

    panel.tape_lf = addPanelIcon(plotLf);
    panel.tape_hf = addPanelIcon(plotHf);

    panel.height_lf = addPanelIcon(plotLf);
    panel.height_hf = addPanelIcon(plotHf);

    panel.crop_lf = addPanelIcon(plotLf);
    panel.crop_hf = addPanelIcon(plotHf);
}

QCPItemPixmap* PlotTools :: addPanelIcon(QCustomPlot *plot) {
    QCPItemPixmap *item = new QCPItemPixmap(plot);
    item->setParent(plot);
    item->setSelectable(false);
    item->setVisible(false);
    item->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    item->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    item->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    item->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    item->setScaled(true);
    item->setLayer(QLatin1String("grid"));
    return item;
}

void PlotTools :: setEnabled(bool enable) {
    if (panel.legendModeIcon_lf == nullptr || panel.legendModeIcon_hf == nullptr)   return;
    panel.target_lf->setSelectable(enable);
    panel.target_hf->setSelectable(enable);
    panel.tape_lf->setSelectable(enable);
    panel.tape_hf->setSelectable(enable);
    panel.height_lf->setSelectable(enable);
    panel.height_hf->setSelectable(enable);
    panel.crop_lf->setSelectable(enable);
    panel.crop_hf->setSelectable(enable);
}

int PlotTools :: checkToolSelection(bool target, bool tape, bool height, bool crop) {
    if (panel.target_lf->selected() || panel.target_hf->selected()) {
        if (!target) {
            panel.target_lf->setPixmap(QPixmap(":/icons/icons/location_solid.png"));
            panel.target_hf->setPixmap(QPixmap(":/icons/icons/location_solid.png"));
            panel.target_higlighted = true;
        } else {
            panel.target_lf->setPixmap(QPixmap(":/icons/icons/location_outline.png"));
            panel.target_hf->setPixmap(QPixmap(":/icons/icons/location_outline.png"));
            panel.target_higlighted = false;
        }
        panel.target_lf->setSelected(false);
        panel.target_hf->setSelected(false);
        return 0x1;
    }
    if (panel.tape_lf->selected() || panel.tape_hf->selected()) {
        if (!tape) {
            panel.tape_lf->setPixmap(QPixmap(":/icons/icons/tape_solid.png"));
            panel.tape_hf->setPixmap(QPixmap(":/icons/icons/tape_solid.png"));
            panel.tape_higlighted = false;
        } else {
            panel.tape_lf->setPixmap(QPixmap(":/icons/icons/tape_outline.png"));
            panel.tape_hf->setPixmap(QPixmap(":/icons/icons/tape_outline.png"));
            panel.tape_higlighted = false;
        }
        panel.tape_lf->setSelected(false);
        panel.tape_hf->setSelected(false);
        return 0x2;
    }
    if (panel.height_lf->selected() || panel.height_hf->selected()) {
        if (!height) {
            panel.height_lf->setPixmap(QPixmap(":/icons/icons/height_solid.png"));
            panel.height_hf->setPixmap(QPixmap(":/icons/icons/height_solid.png"));
            panel.height_higlighted = false;
        } else {
            panel.height_lf->setPixmap(QPixmap(":/icons/icons/height_outline.png"));
            panel.height_hf->setPixmap(QPixmap(":/icons/icons/height_outline.png"));
            panel.height_higlighted = false;
        }
        panel.height_lf->setSelected(false);
        panel.height_hf->setSelected(false);
        return 0x3;
    }
    if (panel.crop_lf->selected() || panel.crop_hf->selected()) {
        if (!crop) {
            panel.crop_lf->setPixmap(QPixmap(":/icons/icons/crop_solid.png"));
            panel.crop_hf->setPixmap(QPixmap(":/icons/icons/crop_solid.png"));
            panel.crop_higlighted = false;
        } else {
            panel.crop_lf->setPixmap(QPixmap(":/icons/icons/crop_outline.png"));
            panel.crop_hf->setPixmap(QPixmap(":/icons/icons/crop_outline.png"));
            panel.crop_higlighted = false;
        }
        panel.crop_lf->setSelected(false);
        panel.crop_hf->setSelected(false);
        return 0x4;
    }
    return 0;
}

int PlotTools :: checkToolHover(double x, double y, bool lfClosed, bool target, bool tape, bool height, bool crop) {
    if (!panel.separateIcons)       return 0;
    double xCen = panel.target_lf->top->pixelPosition().x();
    double yCen = panel.target_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.target_hf->top->pixelPosition().x();
        yCen = panel.target_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        if (!target && !panel.target_higlighted) {
            panel.target_lf->setPixmap(QPixmap(":/icons/icons/location_solid.png"));
            panel.target_hf->setPixmap(QPixmap(":/icons/icons/location_solid.png"));
            panel.target_higlighted = true;
            return 1;
        }
        return 3;
    } else if (!target && panel.target_higlighted) {
        panel.target_lf->setPixmap(QPixmap(":/icons/icons/location_outline.png"));
        panel.target_hf->setPixmap(QPixmap(":/icons/icons/location_outline.png"));
        panel.target_higlighted = false;
        return 2;
    }

    xCen = panel.tape_lf->top->pixelPosition().x();
    yCen = panel.tape_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.tape_hf->top->pixelPosition().x();
        yCen = panel.tape_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        if (!tape && !panel.tape_higlighted) {
            panel.tape_lf->setPixmap(QPixmap(":/icons/icons/tape_solid.png"));
            panel.tape_hf->setPixmap(QPixmap(":/icons/icons/tape_solid.png"));
            panel.tape_higlighted = true;
            return 1;
        }
        return 3;
    } else if (!tape && panel.tape_higlighted) {
        panel.tape_lf->setPixmap(QPixmap(":/icons/icons/tape_outline.png"));
        panel.tape_hf->setPixmap(QPixmap(":/icons/icons/tape_outline.png"));
        panel.tape_higlighted = false;
        return 2;
    }

    xCen = panel.height_lf->top->pixelPosition().x();
    yCen = panel.height_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.height_hf->top->pixelPosition().x();
        yCen = panel.height_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        if (!height && !panel.height_higlighted) {
            panel.height_lf->setPixmap(QPixmap(":/icons/icons/height_solid.png"));
            panel.height_hf->setPixmap(QPixmap(":/icons/icons/height_solid.png"));
            panel.height_higlighted = true;
            return 1;
        }
        return 3;
    } else if (!height && panel.height_higlighted) {
        panel.height_lf->setPixmap(QPixmap(":/icons/icons/height_outline.png"));
        panel.height_hf->setPixmap(QPixmap(":/icons/icons/height_outline.png"));
        panel.height_higlighted = false;
        return 2;
    }

    xCen = panel.crop_lf->top->pixelPosition().x();
    yCen = panel.crop_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.crop_hf->top->pixelPosition().x();
        yCen = panel.crop_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        if (!crop && !panel.crop_higlighted) {
            panel.crop_lf->setPixmap(QPixmap(":/icons/icons/crop_solid.png"));
            panel.crop_hf->setPixmap(QPixmap(":/icons/icons/crop_solid.png"));
            panel.crop_higlighted = true;
            return 1;
        }
        return 3;
    } else if (!crop && panel.crop_higlighted) {
        panel.crop_lf->setPixmap(QPixmap(":/icons/icons/crop_outline.png"));
        panel.crop_hf->setPixmap(QPixmap(":/icons/icons/crop_outline.png"));
        panel.crop_higlighted = false;
        return 2;
    }
    return 0;
}

int PlotTools :: checkMousePosition(double x, double y, bool lfClosed) {
    if (!panel.separateIcons)       return 0;
    double xCen = panel.target_lf->top->pixelPosition().x();
    double yCen = panel.target_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.target_hf->top->pixelPosition().x();
        yCen = panel.target_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        return 1;
    }

    xCen = panel.tape_lf->top->pixelPosition().x();
    yCen = panel.tape_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.tape_hf->top->pixelPosition().x();
        yCen = panel.tape_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        return 1;
    }

    xCen = panel.height_lf->top->pixelPosition().x();
    yCen = panel.height_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.height_hf->top->pixelPosition().x();
        yCen = panel.height_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        return 1;
    }

    xCen = panel.crop_lf->top->pixelPosition().x();
    yCen = panel.crop_lf->left->pixelPosition().y();
    if (lfClosed) {
        xCen = panel.crop_hf->top->pixelPosition().x();
        yCen = panel.crop_hf->left->pixelPosition().y();
    }
    if (abs(x - xCen) < 20 && abs(y - yCen) < 20) {
        return 1;
    }
    return 0;
}

int PlotTools :: findNearWaypoints(double currentLat, double currentLon, double currentHeading, double sonarRange) {
    for (int i = 0; i < waypointList.length(); i++) {
//        if (!waypointList[i].selected)      continue;

        // If the waypoint is not on the route, then pass
        bool onRoute = false;
        for (int j = 0; j < tracker.route.length(); j++) {
            if (tracker.route[j] == &waypointList[i]) {
                onRoute = true;
                break;
            }
        }
        if (!onRoute)   continue;

        // Find waypoints that are within 100 meters of current position
        double waypointDistance = Tracker::calculateGpsDistance(currentLat, currentLon, waypointList[i].Lattitude, waypointList[i].Longitude);
        if (waypointDistance > sonarRange)     continue;

        // If the waypoint passes a 90 degrees bearing limit, then add to plot
        double waypointBearing = Tracker::wrapAngle(Tracker::calculateGpsBearing(currentLat, currentLon,
                                                    waypointList[i].Lattitude, waypointList[i].Longitude) - tracker.BoatHeading);
        if (abs(waypointBearing) > 70 && abs(waypointList[i].previousBearing) < 70) {
//            waypointList[i].addCircleToPlot(plotLf, flagType);
            waypointList[i].previousDistance = waypointDistance;
            waypointList[i].previousBearing = waypointBearing;
            return i;
        }

        waypointList[i].previousDistance = waypointDistance;
        waypointList[i].previousBearing = waypointBearing;
    }
    return -1;
}

bool PlotTools :: findNearWaypoints(double currentLat, double currentLon, double currentHeading, double sonarRange, waypointItem *item) {
    // Find waypoints that are within 100 meters of current position
    double waypointDistance = Tracker::calculateGpsDistance(currentLat, currentLon, item->Lattitude, item->Longitude);
    if (waypointDistance > sonarRange)     return false;

    // If the waypoint passes a 90 degrees bearing limit, then add to plot
    double waypointBearing = Tracker::wrapAngle(Tracker::calculateGpsBearing(currentLat, currentLon,
                                                item->Lattitude, item->Longitude) - tracker.BoatHeading);
    if (abs(waypointBearing) > 70 && abs(item->previousBearing) < 70) {
//            waypointList[i].addCircleToPlot(plotLf, flagType);
        item->previousDistance = waypointDistance;
        item->previousBearing = waypointBearing;
        return true;
    }

    item->previousDistance = waypointDistance;
    item->previousBearing = waypointBearing;

    return false;
}
