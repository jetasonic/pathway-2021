#ifndef TVGWINDOW_H
#define TVGWINDOW_H

#include <QWidget>
#include "modules.h"
#include "qlineedit.h"
#include "qcustomplot.h"

namespace Ui {
class TvgWindow;
}

class TvgWindow : public QWidget
{
    Q_OBJECT

public:
    explicit TvgWindow(QWidget *parent = nullptr);
    ~TvgWindow();

    void updateTvgCurve();
    void adjustTvgCurve(GainSettings::channelType chType);
    void plotTvgCurve();
    void updateHandlePositions(GainSettings::channelType chType, int handleType);
    void moveTvgCurve(GainSettings::channelType chType);
    void addCValue(GainSettings::channelType chType, double val);
    void setNewTvg();

    bool emptyFieldWarning(QLineEdit *lineItem);
    void setLineIntValidator(QLineEdit *lineItem, int minimum, int maximum);
    void setLineDoubleValidator(QLineEdit *lineItem, double minimum, double maximum);


    GainSettings *gain, gain_old;
    SonarPing* ping;

signals:
    void GainsToMainwindow(bool tvg,  QString unit, int Gain, int Balance, int Gain_HF);
    void resetGainSettings();
    void saveGainSettings();
    void send_default_range(int range,int range_HF);
    void tvgWindowClosed();
    void updateMainPlots(GainSettings::channelType chType, GainSettings old, int gainType);

public slots:
    void updateTvgVariables();
    void updateTvgVariablesFromUserMenu();
    void get_Dynamic_range(int maxRange, int maxRange_HF);

private slots:
    void onMouseClick(QMouseEvent* event);
    void onMouseMove(QMouseEvent* event);
    void onMouseRelease(QMouseEvent* event);
    void on_btn_switchTvgFreq_clicked();
    void updateTemperatureLine(const QString &text);
    void updateBValueLine();
    void updateCValueLineLF(const QString &text);
    void updateCValueLineHF(const QString &text);
    void updateAValueLineLF(const QString &text);
    void updateAValueLineHF(const QString &text);

    void on_btn_CLfP_Add_clicked();
    void on_btn_CLfS_Add_clicked();
    void on_btn_CHfP_Add_clicked();
    void on_btn_CHfS_Add_clicked();
    void on_btn_CLfP_Sub_clicked();
    void on_btn_CLfS_Sub_clicked();
    void on_btn_CHfP_Sub_clicked();
    void on_btn_CHfS_Sub_clicked();

    void GainsFromMainwindow(int GainSliderIndex, int BalanceSliderIndex, int GainSliderIndex_HF);
    void freqFromMainwindow();
    void on_tvgBox_clicked();
    void on_box_sync_clicked();
    void on_btn_reset_clicked();
    void on_btn_ok_clicked();
    void on_btn_save_clicked();
    void closeEvent(QCloseEvent *e);

private:
    Ui::TvgWindow *ui;
    QCPItemEllipse *itemBHandle = nullptr;
    QCPItemEllipse *itemCHandle = nullptr;
    QCPItemText *itemLabelCurveMax = nullptr;
    QCPItemLine *itemLineTvgMax = nullptr, *itemLineTvgMin = nullptr;
    QCPItemText *itemLabelTvgMax = nullptr, *itemLabelTvgMin = nullptr;
    bool dragItemB = false, dragItemC = false;
    bool dragItemTvgMax = false, dragItemTvgMin = false;
    bool updatedMinTvgHandle = false;
};

#endif // TVGWINDOW_H
