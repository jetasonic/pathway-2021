#ifndef CONVERTERDIALOG_H
#define CONVERTERDIALOG_H

#include <QDialog>
//#include "mainwindow.h"
#include "qcustomplot.h"
#include<cmath>
#include<QtSql>
#include<QFileInfo>
namespace Ui {
class converterdialog;
}

class converterdialog : public QDialog
{
    Q_OBJECT

public:

    explicit converterdialog(QWidget *parent = nullptr);
    ~converterdialog();

    void closeEvent(QCloseEvent *event);
     QString converterSavePath, filePath;
     QStringList data_filePath;
     QString converterRecordPath;

     int listWidgetIndex=0;

     bool isAngleScanFile = false;

     int button = 1; // 1 -->close, 2-->start, 3-->cancel

     QList<QString> filePathName,xtfPathName, saveFileList;
     QString browsePath, saveFileName;


private:
    Ui::converterdialog *ui;

public slots:

    void on_saveFileBrowser_clicked();
    void on_converterBrowseButton_clicked();
    void on_converterButton_clicked();
    void converterDialogProgressBar(double val);
    void converterFileName(QString name);
    void progressBarRange(int size);
    void isConversionOver(bool show);
    void angleFileDetected(bool show);
    void updateConvertedFileSize(int num);
    void deleteFileSlot();
    void listWidgetCustomContextMenuRequested(const QPoint &pos);

signals:
  //
   void isBrowserButtonClicked(bool);
   void converterStartBtnClicked(bool);
   void changeDataFilePath(QList<QString>, QList<QString>);
   void converterCancelBtnClicked(bool);
   void convertFile();
   void convertFileCounter(int num);


};

#endif // CONVERTERDIALOG_H
