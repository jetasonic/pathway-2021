#ifndef DEPTHALARMWIDGET_H
#define DEPTHALARMWIDGET_H

#include <QWidget>
#include <QMediaPlayer>
#include <QFileInfo>
#include <QTimer>
#include "qcustomplot.h"

namespace Ui {
class DepthAlarmWidget;
}

class DepthAlarmWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DepthAlarmWidget(QWidget *parent = nullptr);
    ~DepthAlarmWidget();

    bool activated;
    int level = 10;
    int volume = 0;
    int audioIndex = 0;
    bool flashState = 0;
    int count = 0;
    bool onState = false;
    QMediaPlayer* audio;
    QTimer timer;
    QTimer hideTimer;
    QCPItemLine *handle = nullptr;

signals:
    void updateHandlePosition(int);
    void stopAlarm(void);
    void updateButtonIcon();

public slots:
    void controlAudioVolume(int value);

private slots:
    void on_btn_close_clicked();
    void on_check_alarmOn_clicked();
    void on_box_alarmLevel_currentTextChanged();
    void on_box_select_currentIndexChanged(int index);
    void updateAlarmSettings();
    void on_btn_testSound_clicked();
    void checkSoundTest(QMediaPlayer::State state);
    void loadAudio(int index);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    Ui::DepthAlarmWidget *ui;
};

#endif // DEPTHALARMWIDGET_H
