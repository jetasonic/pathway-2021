#include "kfilter.h"
#include <cmath>
#include <iostream>

using namespace std;

cPlaneEKF::cPlaneEKF()
{

        setDim(2, 1, 2, 2, 2);
        Period = 1;
}

void cPlaneEKF::makeProcess()
{
        Vector x_(x.size());
        x_(1) = x(1) + x(2)*Period;
        x_(2) = x(2);
        x.swap(x_);
}

void cPlaneEKF::makeMeasure()
{
        z(1) = x(1);
        z(2) = x(2);
}

void cPlaneEKF::makeA()
{
        A(1,1) = 1.0;
        A(1,2) = Period;

        A(2,1) = 0.0;
        A(2,2) = 1.0;
}

void cPlaneEKF::makeW()
{
        W(1,1) = 1.0;
        W(1,2) = 0.0;
        W(2,1) = 0.0;
        W(2,2) = 1.0;
}

void cPlaneEKF::makeQ()
{
        Q(1,1) = 0.04;
        Q(1,2) = 0.04;
        Q(2,1) = 0.0;
        Q(2,2) = 0.04;
}

void cPlaneEKF::makeH()
{
        H(1,1) = 1.0;
        H(1,2) = 0.0;

        H(2,1) = 0.0;
        H(2,2) = 1.0;
}

void cPlaneEKF::makeV()
{
        V(1,1) = 1.0;
        V(1,2) = 0.0;
        V(2,1) = 0.0;
        V(2,2) = 1.0;
}

void cPlaneEKF::makeR()
{
        R(1,1) = 4000.0;
        R(1,2) = 0.0;
        R(2,1) = 0.0;
        R(2,2) = 4000.0;
}
