#include "mainwindow.h"
#include "gpsmap.h"
#include "ui_gpsmap.h"

QCPItemPixmap *flagIcon = nullptr, *mapImage;
QCPTextElement *legendText1, *legendText2, *legendText3, *legendText4, *legendText5;
QCPItemText *loadingChartText = nullptr;
QCPItemText *leftColumn, *rightColumn;

MainWindow* pMainWindow;

gpsMap::gpsMap(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::gpsMap)
{
    ui->setupUi(this);
    pMainWindow = qobject_cast<MainWindow*>(this->parent());

    /*  Define Colors */
    QColor lightBlue;
    lightBlue.setRgb(98, 228, 255,255);
    QColor red;
    red.setRgb(255, 0, 0, 50);
    QColor grey;
    grey.setRgb(50, 50, 50,255);
    QColor silver;
    silver.setRgb(225, 225, 225);
    QColor white;
    white.setRgb(255, 255, 255);
    QColor green;
    green.setRgb(0, 96, 40,255);

    // Configure QCustomPlot Appearance
    ui->mapPlot->setBackground(QBrush(grey));
    ui->mapPlot->axisRect()->setBackground(QBrush(Qt::black));
    ui->mapPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectItems); // this will also allow rescaling the color scale by dragging/zooming
    ui->mapPlot->axisRect()->setupFullAxesBox(true);
    ui->mapPlot->axisRect()->setRangeZoomFactor(0.9, 0.9); //Sets the zoom ratio, so when zooming in with mouse wheel both axis are zoomed by the same factor (0.9), re-taining a square zoom

//    QPixmap pixmap_map(":/icons/Map.PNG");
//    QPixmap pixmap_map2 = pixmap_map.scaled(1294, 607);
//    mapImage->setPixmap(pixmap_map2);
//    mapImage->setScaled(true, Qt::IgnoreAspectRatio);
//    mapImage->topLeft->setCoords(-63.5492, 44.66375);
//    mapImage->bottomRight->setCoords(-63.53535, 44.65913);

    //customize the line or roll/pitch traceline
    QPen line_color;
    line_color.setWidth(2);
    line_color.setColor(Qt::green);
    ui->mapPlot->xAxis->setLabelColor(white);
    ui->mapPlot->yAxis->setLabelColor(white);

    QFont preferredBold, preferredSizeAxis, preferredSizeLabel;
    QFont secondSize;//change the bold type of X-Axis and Y-Axis
    preferredSizeAxis.setPointSize(12);
    secondSize.setPointSize(10);
    ui->mapPlot->xAxis->setTickLabelFont(secondSize);//change to size 12
    ui->mapPlot->yAxis->setTickLabelFont(secondSize);
    ui->mapPlot->xAxis->setLabelFont(secondSize);
    ui->mapPlot->yAxis->setLabelFont(secondSize);
    ui->mapPlot->xAxis->setLabelColor(white);
    ui->mapPlot->yAxis->setLabelColor(white);
    ui->mapPlot->xAxis->setTickLabelColor(white);
    ui->mapPlot->yAxis->setTickLabelColor(white);

    ui->mapPlot->xAxis->grid()->setVisible(true);
    ui->mapPlot->yAxis->grid()->setVisible(true);

    ui->TargetList->setRowCount(10);
    for (int i = 0; i < 10; i++) {
        QTableWidgetItem *item = new QTableWidgetItem("NULL");
        QTableWidgetItem *item2 = new QTableWidgetItem("NULL");
        ui->TargetList->setItem(i, 0, item);
        ui->TargetList->setItem(i, 1, item2);
    }
}

gpsMap::~gpsMap()
{
    delete ui;
}

/************** GNSS Plotting Feature ******************/

void gpsMap :: plot_setupItems (QCustomPlot* plotPtr) {
    mapPlotPtr = plotPtr;

    QPen bluePen;
    bluePen.setColor(QColor(3, 37, 126));
    bluePen.setWidthF(5);

    QPen blackPen;
    blackPen.setColor(QColor(0,0,0));
    blackPen.setWidth(5);

    QPen redPen;
    redPen.setColor(QColor(225, 6, 0));
    redPen.setWidthF(5);

    QPen greenPen;
    greenPen.setColor(QColor(8, 255, 8));
    greenPen.setWidthF(5);

    QPen greyPen;
    greyPen.setColor(QColor(128, 128, 128));
    greyPen.setWidthF(3);

    QPen whitePen;
    whitePen.setColor(QColor(255, 255, 255));
    whitePen.setWidthF(3);

    QBrush whiteBrush;
    whiteBrush.setColor(QColor(255, 255, 255));
    whiteBrush.setStyle(Qt::SolidPattern);

    QBrush grayBrush;
    grayBrush.setColor(QColor(50,50,50));
    grayBrush.setStyle(Qt::SolidPattern);

    portLine = new QCPItemLine(mapPlotPtr);
    portLine->setPen(whitePen);
    portLine->setLayer("trace");
    stbdLine = new QCPItemLine(mapPlotPtr);
    stbdLine->setPen(whitePen);
    stbdLine->setLayer("trace");
    portLine->start->setCoords(-100, -100);
    stbdLine->start->setCoords(-100, -100);
    portLine->end->setCoords(-100.1, -100.1);
    stbdLine->end->setCoords(-100.1, -100.1);
    portLine->setVisible(false);
    stbdLine->setVisible(false);

    QPen whitePen2;
    whitePen2.setColor(QColor(255, 255, 255));
    whitePen2.setWidthF(3);
    whitePen2.setStyle(Qt::DashLine);
    portLine2 = new QCPItemLine(mapPlotPtr);
    portLine2->setPen(whitePen2);
    portLine2->setLayer("trace");
    stbdLine2 = new QCPItemLine(mapPlotPtr);
    stbdLine2->setPen(whitePen2);
    stbdLine2->setLayer("trace");
    portLine2->start->setCoords(-100, -100);
    stbdLine2->start->setCoords(-100, -100);
    portLine2->end->setCoords(-100.1, -100.1);
    stbdLine2->end->setCoords(-100.1, -100.1);
    portLine2->setVisible(false);
    stbdLine2->setVisible(false);

    newCurve = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    newCurve->setPen(bluePen);
    newCurve->setLayer("tools1");

    newCurve2 = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    newCurve2->setPen(redPen);
    newCurve2->setLayer("tools1");

    newCurve3 = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    newCurve3->setPen(greenPen);
    newCurve3->setLayer("tools1");

    vectorCurve = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    vectorCurve->setPen(redPen);
    vectorCurve->setLayer("tools1");

    trackpoints = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    trackpoints->setPen(greyPen);
    trackpoints->setLayer("tools1");

    QPen whitePen3;
    whitePen3.setColor(QColor(255, 255, 255));
    whitePen3.setWidthF(10);
    whitePen3.setBrush(QBrush(Qt::white));

    cursorDot = new QCPItemEllipse(mapPlotPtr);
    cursorDot->setBrush(QBrush(Qt::white));
    cursorDot->setLayer("tools2");
    cursorDot->setVisible(false);

    boatIcon = new QCPItemEllipse(mapPlotPtr);
    boatIcon->setParent(mapPlotPtr);
    boatIcon->setSelectable(false);
    boatIcon->setPen(whitePen);
    boatIcon->setBrush(whiteBrush);
    boatIcon->topLeft->setCoords(-100,-100);
    boatIcon->bottomRight->setCoords(-100.1,-100.1);
    boatIcon->setLayer("boatLayer");
    boatIcon->setVisible(false);

    QPixmap pixmapArrow(":/icons/icons/rocket.png");
//    static double rot = 0;
//    QMatrix rm;
//    rot += 15;
//    rm.rotate(rot);
    QPixmap pixmap2 = pixmapArrow.scaled(40,40);//.transformed(rm);

    boatIconPix = new QCPItemPixmap(mapPlotPtr);
    boatIconPix->setPixmap(pixmap2);
    boatIconPix->setScaled(true);
    boatIconPix->setSelectable(false);
    boatIconPix->setLayer("boatLayer");
    boatIconPix->topLeft->setCoords(-100.1,-100.1);
    boatIconPix->bottomRight->setCoords(-100.1,-100.1);

//    legendText1 = new QCPTextElement(mapPlotPtr);
//    legendText2 = new QCPTextElement(mapPlotPtr);
//    legendText3 = new QCPTextElement(mapPlotPtr);
//    legendText4 = new QCPTextElement(mapPlotPtr);
//    legendText5 = new QCPTextElement(mapPlotPtr);

////    mapPlotPtr->legend = new QCPLegend;
//    mapPlotPtr->legend->setVisible(true);
//    mapPlotPtr->legend->clearItems();
//    mapPlotPtr->legend->setBrush(grayBrush);
//    mapPlotPtr->legend->setLayer(QLatin1String("legend"));
//    legendText1->setLayer(mapPlotPtr->legend->layer());
//    legendText2->setLayer(mapPlotPtr->legend->layer());
//    legendText3->setLayer(mapPlotPtr->legend->layer());
//    legendText4->setLayer(mapPlotPtr->legend->layer());
//    legendText5->setLayer(mapPlotPtr->legend->layer());
//    legendText1->setTextColor(Qt::white);
//    legendText2->setTextColor(Qt::white);
//    legendText3->setTextColor(Qt::white);
//    legendText4->setTextColor(Qt::white);
//    legendText5->setTextColor(Qt::white);
//    legendText1->setFont(QFont("sans", 9));
//    legendText2->setFont(QFont("sans", 9));
//    legendText3->setFont(QFont("sans", 9));
//    legendText4->setFont(QFont("sans", 9));
//    legendText5->setFont(QFont("sans", 9));
//    legendText1->setText("La: N/A");
//    legendText2->setText("Lo: N/A");
//    legendText3->setText("HDT: N/A");
//    legendText4->setText("Spd: N/A");
//    legendText5->setText("Dst: N/A");
//    legendText1->setTextFlags(Qt::AlignRight);
//    legendText2->setTextFlags(Qt::AlignRight);
//    legendText3->setTextFlags(Qt::AlignRight);
//    legendText4->setTextFlags(Qt::AlignRight);
//    legendText5->setTextFlags(Qt::AlignRight);

//    mapPlotPtr->legend->addElement(0,0,legendText1);
//    mapPlotPtr->legend->addElement(1,0,legendText2);
//    mapPlotPtr->legend->addElement(2,0,legendText3);
//    mapPlotPtr->legend->addElement(3,0,legendText4);
//    mapPlotPtr->legend->addElement(4,0,legendText5);
//    QCPLegend* legend2 = new QCPLegend();
//    mapPlotPtr->axisRect(0)->insetLayout()->addElement(legend2, Qt::AlignBottom|Qt::AlignLeft);
//    QCPMarginGroup *group = new QCPMarginGroup(mapPlotPtr);
//    mapPlotPtr->legend->elementAt(0)->setMarginGroup(QCP::msRight, group);
//    mapPlotPtr->legend->elementAt(1)->setMarginGroup(QCP::msRight, group);
//    mapPlotPtr->legend->elementAt(0)->setAutoMargins(QCP::msRight|QCP::msLeft);
//    mapPlotPtr->legend->elementAt(1)->setAutoMargins(QCP::msRight|QCP::msLeft);
//    legendText1->setText("La: N/A");
//    legendText2->setText("Lo: N/A");
//    legendText3->setText("HDT: N/A");
//    legendText4->setText("Spd: N/A");
//    legendText5->setText("Dst: N/A");
//    //mapPlotPtr->legend->setLayer("grid");

    leftColumn = new QCPItemText(mapPlotPtr);
    leftColumn->setLayer("fixedIcons");
    leftColumn->setFont(QFont("sans", 10));
    leftColumn->setColor(QColor(255, 255, 255));
    leftColumn->position->setTypeX(QCPItemPosition::ptAbsolute);
    leftColumn->position->setTypeY(QCPItemPosition::ptAbsolute);
    leftColumn->position->setCoords(mapPlotPtr->width() - 10, 20);
    leftColumn->setPositionAlignment(Qt::AlignRight|Qt::AlignTop);
    leftColumn->setTextAlignment(Qt::AlignRight);
    leftColumn->setBrush(QBrush(Qt::NoBrush));
    leftColumn->setSelectable(false);
    leftColumn->setText("La: N/A\nHDT\nSPD\nDST\nTGT");

    rightColumn = new QCPItemText(mapPlotPtr);
    rightColumn->setLayer("fixedIcons");
    rightColumn->setFont(QFont("sans", 10));
    rightColumn->setColor(QColor(255, 255, 255));
    rightColumn->position->setTypeX(QCPItemPosition::ptAbsolute);
    rightColumn->position->setTypeY(QCPItemPosition::ptAbsolute);
    rightColumn->position->setCoords(mapPlotPtr->width() - 10, 20);
    rightColumn->setPositionAlignment(Qt::AlignLeft|Qt::AlignTop);
    rightColumn->setTextAlignment(Qt::AlignLeft);
    rightColumn->setBrush(QBrush(Qt::NoBrush));
    rightColumn->setSelectable(false);
    rightColumn->setText("Lo: N/A\nN/A\nN/A\nN/A\nN/A");

    compassIcon = new QCPItemPixmap(mapPlotPtr);
    QPixmap pixmap (":/icons/icons/Compass.png", nullptr, Qt::NoOpaqueDetection);
    compassIcon->setPixmap(pixmap);
    compassIcon->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    compassIcon->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    compassIcon->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    compassIcon->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    compassIcon->setScaled(true);
    compassIcon->setLayer("fixedIcons");
//    compassIcon->

    compassIcon->topLeft->setCoords(30,30);
    compassIcon->bottomRight->setCoords(100,100);

    //mapPlotPtr->xAxis->setVisible(false);
    //mapPlotPtr->yAxis->setVisible(false);
    //mapPlotPtr->replot();
}

void gpsMap :: plot_reset() {
    boatIconPix->setVisible(false);
    mapPlotPtr->xAxis->setTickLabels(false);
    mapPlotPtr->yAxis->setTickLabels(false);
    if (leftColumn != nullptr)
        leftColumn->setText("La: N/A\nHDT\nSPD\nDST\nTGT");
    if (rightColumn != nullptr)
        rightColumn->setText("Lo: N/A\nN/A\nN/A\nN/A\nN/A");
}

void gpsMap :: plot_replot() {
    mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void gpsMap :: plot_showBoatIcon(bool show) {
    if (boatIconPix != nullptr) {
        boatIconPix->setVisible(show);
    }
}

void gpsMap :: plot_updateRange() {
    if (mapPlotPtr == nullptr)  return;

    if (zoomOn) {
        return;
    }

    QSize sz = mapPlotPtr->axisRect()->size();
    int height = sz.rheight();
    int width = sz.width();
    double xWidth = MaxXRange - MinXRange;
    double yHeight = MaxYRange - MinYRange;
//    double centerX = (MaxXRange + MinXRange)/2;
//    double centerY = (MaxYRange + MinYRange)/2;
//    double scaledViewY = ((height * (xWidth)) / width);
//    double scaledViewX = (width * yHeight) / height;
    double xRes = 0.001 * zoomScale;
    double yRes = 0.001 * zoomScale;

    mapPlotPtr->xAxis->moveRange(xCenter - (mapPlotPtr->xAxis->range().upper + mapPlotPtr->xAxis->range().lower)/2.0);
    mapPlotPtr->yAxis->moveRange(yCenter - (mapPlotPtr->yAxis->range().upper + mapPlotPtr->yAxis->range().lower)/2.0);

//    if (xWidth > yHeight) {
//        yRes *= height/(width*1.0);
//        mapPlotPtr->xAxis->setRange(xCenter - xRes, xCenter + xRes);//MinXRange - 0.001, MaxXRange + 0.001);
//        mapPlotPtr->yAxis->setRange(yCenter - yRes, yCenter + yRes);//centerY - scaledViewY/2 - 0.001, centerY + scaledViewY/2 + 0.001);
//    }
//    else {
//        xRes *= width/(height*1.0);
//        mapPlotPtr->xAxis->setRange(xCenter - xRes, xCenter + xRes);
//        mapPlotPtr->yAxis->setRange(yCenter - yRes, yCenter + yRes);
//    }
}

bool gpsMap :: plot_isCentered() {
    double xPos = (mapPlotPtr->xAxis->range().upper + mapPlotPtr->xAxis->range().lower)/2.0;
    double xRange = mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower;
    double yPos = (mapPlotPtr->yAxis->range().upper + mapPlotPtr->yAxis->range().lower)/2.0;
    double yRange = mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower;
    if ((xCenter < xPos + xRange*0.05) && (xCenter > xPos - xRange*0.05) &&
            (yCenter < yPos + yRange*0.05) && (yCenter > yPos - yRange*0.05))
        return true;
    else    return false;
}

void gpsMap :: plot_resize(QCPRange xAxis, QCPRange yAxis) {
    double xMin = xAxis.lower, xMax = xAxis.upper,
            yMin = yAxis.lower, yMax = yAxis.upper;
    QSize gpsPlotSz = mapPlotPtr->axisRect()->size();

    int gpsHeight = gpsPlotSz.rheight();
    int gpsWidth = gpsPlotSz.rwidth();

    double updatedView1 = (gpsHeight * 1.0) / (gpsWidth * 1.0);

    qDebug()<<"updated view: "<<updatedView1;

    // Find the current distance (in degrees) of x/y of the gpsPlot
    double xGpsView2 = xAxis.upper + 0.0025;
    double xGpsView1 = xAxis.lower - 0.0025;
    double yGpsView2 = yAxis.upper + 0.0025;
    double yGpsView1 = yAxis.lower - 0.0025;
    double distanceX = 0, distanceY = 0, bearing;

    // Find the distance (in meters) of a 0.0025 degree difference of x/y
    pMainWindow->Gnss_calcDistanceAndBearing(yGpsView2, xGpsView1, yGpsView2, xGpsView1+0.0025, &distanceX, &bearing);
    pMainWindow->Gnss_calcDistanceAndBearing(yGpsView2, xGpsView1, yGpsView2+0.0025, xGpsView1, &distanceY, &bearing);

    // Determine the correct upper y range to give a 1m:1m ratio for x:y
    yGpsView1 = yGpsView2 - updatedView1 * (xGpsView2 - xGpsView1)
            * distanceX/distanceY;

    // set the new range values
    mapPlotPtr->xAxis->setRange(xGpsView1,xGpsView2);
    mapPlotPtr->yAxis->setRange(yGpsView1,yGpsView2);

    target_resizeAllTargets();
}

void gpsMap :: plot_configureBoatIcon (int iconType) {
    static int prevIcon = 2;
    if (iconType == prevIcon)       return;
    else                            prevIcon = iconType;
    if (iconType == 1) {
        // Angle scan
        QPixmap pixmapArrow(":/icons/icons/rocket.png");
        QMatrix rm;
        rm.rotate(boatHeading);
        QPixmap pixmap2 = pixmapArrow.scaled(40,40).transformed(rm);
        boatIconPix->setPixmap(pixmap2);
    } else {
        // Sidescan
        QPixmap pixmapArrow(":/icons/icons/BoatSidescanIcon.png");
        QMatrix rm;
        rm.rotate(boatHeading);
        QPixmap pixmap2 = pixmapArrow.scaled(40,40).transformed(rm);
        boatIconPix->setPixmap(pixmap2);
    }
}

void gpsMap :: plot_correctBoatHeading () {
    if (newCurve->dataCount() > 10 && newCurve2->dataCount() > 10) {
        double xPort = newCurve2->data()->at(newCurve2->dataCount() - 1)->key;
        double yPort = newCurve2->data()->at(newCurve2->dataCount() - 1)->value;
        double xStbd = newCurve3->data()->at(newCurve3->dataCount() - 1)->key;
        double yStbd = newCurve3->data()->at(newCurve3->dataCount() - 1)->value;
        portLine->end->setCoords(xPort, yPort);
        stbdLine->end->setCoords(xStbd, yStbd);
        avgHdg_queue.clear();
    } else {
        portLine->setVisible(false);
        stbdLine->setVisible(false);
        avgHdg_queue.clear();
    }
}

void gpsMap :: plot_gpsZoom(QWheelEvent *event) {
    // Mouse range zooming interaction:
    double factor;
    double wheelSteps = event->delta()/120; // a single step delta is +/-120 usually
    factor = qPow(0.9, wheelSteps);
    QSize sz = mapPlotPtr->axisRect()->size();
    int height = sz.rheight();
    int width = sz.width();
    mapPlotPtr->xAxis->scaleRange(factor);
    mapPlotPtr->yAxis->scaleRange(factor);
    target_resizeAllTargets();
    pMainWindow->Chart_Zoom();
    soundingPointUpdate();
    objectsUpdate();
    mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void gpsMap :: track_moveCurve(int pingDiff) {
    pingDiff++;
    if (newCurve2->dataCount() < pingDiff)   return;
    if (portLine2 == nullptr || stbdLine2 == nullptr)     return;

    if (pingDiff == 1) {
        portLine2->setVisible(false);
        stbdLine2->setVisible(false);
        cursorDot->setVisible(false);
        return;
    } else {
        portLine2->setVisible(false);
        stbdLine2->setVisible(false);
        cursorDot->setVisible(true);
    }

    double xCen = newCurve->data()->at(newCurve->dataCount()-pingDiff)->key;
    double yCen = newCurve->data()->at(newCurve->dataCount()-pingDiff)->value;
    double xPort = newCurve2->data()->at(newCurve2->dataCount()-pingDiff)->key;
    double yPort = newCurve2->data()->at(newCurve2->dataCount()-pingDiff)->value;
    double xStbd = newCurve3->data()->at(newCurve3->dataCount()-pingDiff)->key;
    double yStbd = newCurve3->data()->at(newCurve3->dataCount()-pingDiff)->value;
    portLine2->start->setCoords(xCen, yCen);
    portLine2->end->setCoords(xPort, yPort);
    stbdLine2->start->setCoords(xCen, yCen);
    stbdLine2->end->setCoords(xStbd, yStbd);
    if (cursorDot != nullptr) {
        double xInc = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/mapPlotPtr->width() * 5;
        double yInc = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/mapPlotPtr->height() * 5;

        cursorDot->bottomRight->setCoords(xCen + xInc, yCen - yInc);
        cursorDot->topLeft->setCoords(xCen - xInc, yCen + yInc);
    }
}

int gpsMap :: track_CursorToCurve(QMouseEvent *event, int pingDiff) {
    if (newCurve == nullptr)        return 0;
    if (newCurve->dataCount() < 1)  return 0;
    if (pingDiff > newCurve->dataCount())   return 0;
    double xMouse = mapPlotPtr->xAxis->pixelToCoord(event->pos().x());
    double yMouse = mapPlotPtr->yAxis->pixelToCoord(event->pos().y());
    double dist = 5;
    int trackedIndex = 0;

    for (int i = newCurve->dataCount() - pingDiff - 1; i < newCurve->dataCount(); i++) {
        double dist2 = (pow(abs(newCurve->data()->at(i)->key - xMouse), 2) +
                pow(abs(newCurve->data()->at(i)->value - yMouse), 2));
        if (dist2 < dist) {
            dist = dist2;
            trackedIndex = i;
        }
    }
    double xCen = newCurve->data()->at(trackedIndex)->key;
    double yCen = newCurve->data()->at(trackedIndex)->value;
    if (cursorDot != nullptr) {
        double xInc = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/mapPlotPtr->width() * 5;
        double yInc = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/mapPlotPtr->height() * 5;

        cursorDot->bottomRight->setCoords(xCen + xInc, yCen - yInc);
        cursorDot->topLeft->setCoords(xCen - xInc, yCen + yInc);
        cursorDot->setVisible(true);
    }
    return newCurve->dataCount() - trackedIndex - 1;
}

void gpsMap :: track_getCurveCoordinates(int pingDiff, targetItem item) {
    pingDiff++;
    if (newCurve2->dataCount() < pingDiff)   return;
    if (portLine2 == nullptr || stbdLine2 == nullptr)     return;

    double xCen = newCurve->data()->at(newCurve->dataCount()-pingDiff)->key;
    double yCen = newCurve->data()->at(newCurve->dataCount()-pingDiff)->value;
    double xPort = newCurve2->data()->at(newCurve2->dataCount()-pingDiff)->key;
    double yPort = newCurve2->data()->at(newCurve2->dataCount()-pingDiff)->value;
    double xStbd = newCurve3->data()->at(newCurve3->dataCount()-pingDiff)->key;
    double yStbd = newCurve3->data()->at(newCurve3->dataCount()-pingDiff)->value;
    QPointF centre(xCen, yCen);
    QPointF port(xPort, yPort);
    QPointF stbd(xStbd, yStbd);
    item.setGpsLinePosition(centre, port, stbd);
}

void gpsMap :: legend_updateHeading(double Heading) {
    ui->Heading->setText(QString::number(Heading, 'd', 3));
}

void gpsMap :: legend_updateTarget(double Heading, double Distance, double RelativeHeading) {
    ui->HeadingToTarget->setText(QString::number(Heading, 'd', 3));
    ui->DistanceToTarget->setText(QString::number(Distance, 'd', 3));
    ui->RelativeBearingToTarget->setText(QString::number(RelativeHeading, 'd', 3));
}

void gpsMap :: legend_updateAll() {
    if (pMainWindow->state.GUI_MODE == 2) {
        textTarget1 = "TGT";
        textTarget2 = QString::number(pMainWindow->tools->targetList.length());
    } else {
        textTarget1 = "WPT";
        textTarget2 = QString::number(pMainWindow->tools->waypointList.length());
    }
    leftColumn->setText(textLat+"\nHDT\nSPD\nDST\n"+textTarget1);
    rightColumn->setText(textLon+"\n"+textHdg+"\n"+textSpeed+"\n"+textDistance+"\n"+textTarget2);
}

void gpsMap :: legend_updateLegendText(double latNew, double lonNew, double HdgNew, double SpeedNew, double distNew) {
    textDistance = QString::number(distNew/1000, 'f', 2) + " km";
    if (HdgNew < -360)
        textHdg = "N/A";
    else
        textHdg = QString::number(HdgNew, 'f', 1) + "º";
    textSpeed = QString::number(SpeedNew, 'f', 1) + " kn";
    if (SpeedNew < 0)       textSpeed = "N/A";
    QString latNumber = QString::number(abs(latNew), 'f', 4);
    QString longNumber = QString::number(abs(lonNew), 'f', 4);
    if(latNew < 0){
        textLat = latNumber + " S";
//        legendText1->setText(QString::number(qFabs(yCoord), 'f', 5) + " S");
    }else{
        textLat = latNumber + " N";
//        legendText1->setText(QString::number(qFabs(yCoord), 'f', 5) + " N");
    }

    if (lonNew < 0) {
        textLon = longNumber + " W";
//        legendText2->setText(QString::number(qFabs(xCoord), 'f', 5) + " W");
    } else {
        textLon = longNumber + " E";
//        legendText2->setText(QString::number(qFabs(xCoord), 'f', 5) + " E");
    }
    if (abs(latNew) > 200 || abs(lonNew) > 200) {
        textLat = "La: N/A";
        textLon = "Lo: N/A";
    }
    legend_updateAll();
}

void gpsMap :: legend_updateSpeedText(double speed) {
    gpsSpeed = speed;
    if (gpsSpeed < -0.5) {
        textSpeed = "N/A";
    } else {
        textSpeed = QString::number(gpsSpeed, 'f', 1) + " kn";
    }
    legend_updateAll();
}

void gpsMap :: legend_updateHeadingText(double heading) {
    boatHeading = heading;
    if (abs(boatHeading) > 360) {
        textHdg = "N/A";
    } else {
        textHdg = QString::number(boatHeading, 'f', 1) + "º";
    }
    legend_updateAll();
}

double gpsMap :: gnss_calculateHeading(double xCenter, double yCenter, double rot) {
    static double heading = 0;

    if (mapPlotPtr == nullptr)  return heading;

    static int counter = 0;
    counter++;
    if (counter < 3) { return -900; }

    int Size = 30;
    double distance = 0;
    if (newCurve->data()->size() > Size) {
        // If the main curve has over 10 samples, then plot boundary curves based on the main curve
        int prevPoint = 40;
        if (newCurve->data()->size() < prevPoint)   prevPoint = newCurve->data()->size();
        rot = Tracker::calculateGpsBearing(newCurve->data()->at(newCurve->data()->size() - prevPoint)->value,
                                               newCurve->data()->at(newCurve->data()->size() - prevPoint)->key,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->value,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->key);//, &distance, &rot);
        distance = Tracker::calculateGpsBearing(newCurve->data()->at(newCurve->data()->size() - prevPoint)->value,
                                               newCurve->data()->at(newCurve->data()->size() - prevPoint)->key,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->value,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->key);
        if (newCurve2->data()->size() == 0) {
            avgHdg_queue.clear();
        }
        if (avgHdg_queue.size() > 20) {
            avgHdg_queue.dequeue();
        }
        avgHdg_queue.enqueue(rot);
        double sumOfQueue = 0;
        double xSum = 0, ySum = 0;
        for (int i = 0; i< avgHdg_queue.size(); i++) {
            sumOfQueue += avgHdg_queue.at(i);
            xSum += sin(avgHdg_queue.at(i)*M_PI/180.0);
            ySum += cos(avgHdg_queue.at(i)*M_PI/180.0);
        }
        rot = qAcos(ySum/avgHdg_queue.size())/M_PI*180;
        if (xSum < 0)   rot *= -1;

        if (distance < 5) {
            if (newCurve->data()->size() > 2*Size) {
                // If the main curve has over 10 samples, then plot boundary curves based on the main curve
                rot = Tracker::calculateGpsBearing(newCurve->data()->at(newCurve->data()->size() - 2*Size)->value,
                                                       newCurve->data()->at(newCurve->data()->size() - 2*Size)->key,
                                                       newCurve->data()->at(newCurve->data()->size() - 1)->value,
                                                       newCurve->data()->at(newCurve->data()->size() - 1)->key);//, &distance, &rot);
                distance = Tracker::calculateGpsDistance(newCurve->data()->at(newCurve->data()->size() - prevPoint)->value,
                                                       newCurve->data()->at(newCurve->data()->size() - prevPoint)->key,
                                                       newCurve->data()->at(newCurve->data()->size() - 1)->value,
                                                       newCurve->data()->at(newCurve->data()->size() - 1)->key);
            }
        }
        // if the distance covered over 10 samples is less than 10 meters, then do not plot
        if (distance < 2) {
            rot = heading;
           // heading = rot;
        }
    } else {
        return -900;
    }

    if (abs(rot) < 0.001)  {
        return heading; // Strange bug where rot is == to 0;
    }

    heading = rot;
    if (heading < 0)            heading += 360;
    else if (heading > 360)     heading -= 360;

    textHdg = QString::number(heading, 'f', 1) + "º";
    legend_updateAll();
//    legendText3->setText("HDT: " + QString::number(heading, 'f', 1) + "º");
    rot *= M_PI/180.0;
    boatHeading = heading;
    return heading;
}

void gpsMap :: gnss_calculateAccumulatedDistance() {
    if ((newCurve->data()->size()-5) % 10 == 0) {
        double distance = 0, rot = 0;
        int prevPoint = 5;
        if (newCurve->data()->size() > 15)      prevPoint = 11;
        pMainWindow->Gnss_calcDistanceAndBearing(newCurve->data()->at(newCurve->data()->size() - prevPoint)->value,
                                               newCurve->data()->at(newCurve->data()->size() - prevPoint)->key,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->value,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->key,
                                               &distance, &rot);
        totalDistance += distance;
        textDistance = QString::number(totalDistance/1000, 'f', 2) + " km";
        legend_updateAll();
//        legendText5->setText("Dst: " + QString::number(totalDistance/1000, 'f', 1) + " km");
    }
}

double gpsMap :: gnss_calculateSpeed (double lat, double lon, double time) {
    static QQueue<double> timeQueue, speedQueue;
    double firstTime = 0, distance, headingDirectionS;
    timeQueue.enqueue(time);
    double avgSpd = 0;
    if (timeQueue.length() > 20) {
        firstTime = timeQueue.at(0);//.dequeue();
        int prevPoint = 40;
        if (newCurve->data()->size() < prevPoint)   prevPoint = newCurve->data()->size();
        if (timeQueue.length() > 40)                firstTime = timeQueue.dequeue();
        pMainWindow->Gnss_calcDistanceAndBearing(newCurve->data()->at(newCurve->data()->size() - prevPoint)->value,
                                               newCurve->data()->at(newCurve->data()->size() - prevPoint)->key,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->value,
                                               newCurve->data()->at(newCurve->data()->size() - 1)->key,
                                               &distance, &headingDirectionS);

        speedQueue.enqueue(distance/(time-firstTime)*1.94384);
        for (int i = 0; i< speedQueue.size(); i++) {
            avgSpd += speedQueue.at(i);
        }
        avgSpd /= (speedQueue.size()*1.0);
//        gpsSpeed = avgSpd;
        if (speedQueue.size() > 20)     speedQueue.dequeue();
    }
    return avgSpd;//gpsSpeed;
}

/*************** Flag Feature **********************/

void gpsMap :: path_addBoundaryPoints(double xCenter, double yCenter, double rot, double range, double transducerAngle) {
    if (rot < -360)     return;

    double xPort, xStbd, yPort, yStbd;

    //Formula for calculating lat2, given lat1, bearing and distance
    Tracker::calculateGpsEndPoints(yCenter, xCenter, rot + transducerAngle, range, &yStbd, &xStbd);
    Tracker::calculateGpsEndPoints(yCenter, xCenter, rot - transducerAngle, range, &yPort, &xPort);
    newCurve2->addData(xPort, yPort);
    newCurve3->addData(xStbd, yStbd);
    portLine->end->setCoords(xPort, yPort);
    stbdLine->end->setCoords(xStbd, yStbd);
    if (!portLine->visible()) {
        portLine->setVisible(true);
        stbdLine->setVisible(true);
    }
}

void gpsMap :: path_addData(double xCoord, double yCoord) {
//    resizeEvent(NULL);
//    ChartWindowInstance->resizeAllTargets();
    if (mapPlotPtr == nullptr)  return;

    if (abs(xCoord) < 1 || abs(yCoord) < 1) {
        return;
    }

    static int functionCallCount = 0;

    if (newCurve->data()->size() == 0) {
        mapPlotPtr->xAxis->setTickLabels(true);
        mapPlotPtr->yAxis->setTickLabels(true);
        if (pMainWindow->customPlotLF->yAxis->grid()->visible()) {
            mapPlotPtr->xAxis->grid()->setVisible(true);
            mapPlotPtr->yAxis->grid()->setVisible(true);
        }
        boatIconPix->setVisible(true);
    }
    newCurve->addData(xCoord, yCoord);
    QString latNumber = QString::number(abs(yCoord), 'f', 4);
    QString longNumber = QString::number(abs(xCoord), 'f', 4);
    if(yCoord < 0){
        textLat = latNumber + " S";
//        legendText1->setText(QString::number(qFabs(yCoord), 'f', 5) + " S");
    }else{
        textLat = latNumber + " N";
//        legendText1->setText(QString::number(qFabs(yCoord), 'f', 5) + " N");
    }

    if (xCoord < 0){
        textLon = longNumber + " W";
//        legendText2->setText(QString::number(qFabs(xCoord), 'f', 5) + " W");
    } else {
        textLon = longNumber + " E";
//        legendText2->setText(QString::number(qFabs(xCoord), 'f', 5) + " E");
    }
    legend_updateAll();

    gnss_calculateAccumulatedDistance();

    double width = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/mapPlotPtr->width()*10;
    double height = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/mapPlotPtr->height()*10;
    double xInc = width;
    double yInc = height;
    boatIcon->setLayer("boatLayer");
//    qDebug()<<"layer index:"<<boatIcon->layer()->index();
    boatIcon->topLeft->setCoords(xCoord-xInc, yCoord+yInc);
    boatIcon->bottomRight->setCoords(xCoord+xInc, yCoord-yInc);

    // Update direction of boat arrow
    QPixmap pixmapArrow(":/icons/icons/rocket.png");
    QMatrix rm;
    rm.rotate(boatHeading);
    QPixmap pixmap2 = pixmapArrow.scaled(40,40).transformed(rm);
    double scaleFactor = (abs(cos(boatHeading*M_PI/180)) + abs(sin(boatHeading*M_PI/180)))*1.5;

    boatIconPix->setPixmap(pixmap2);
    boatIconPix->setScaled(true);
    boatIconPix->setLayer("boatLayer");
    boatIconPix->topLeft->setCoords(xCoord-xInc*scaleFactor, yCoord+yInc*scaleFactor);
    boatIconPix->bottomRight->setCoords(xCoord+xInc*scaleFactor, yCoord-yInc*scaleFactor);
    pMainWindow->tools->tracker.updateBoatPosition(xCoord, yCoord, boatHeading);
    double sonarRange = pMainWindow->ping->rangeLf_double;
    if (pMainWindow->ping->rangeLf_int == 0)    sonarRange = pMainWindow->ping->rangeHf_double;
    if (!pMainWindow->flag.showGap)             sonarRange -= pMainWindow->ping->lastTraceAlt;
    waypointItem* nextWpt = pMainWindow->tools->tracker.nextWpt;
    if (nextWpt != nullptr) {
        bool index = pMainWindow->tools->findNearWaypoints(yCoord, xCoord, boatHeading, sonarRange, nextWpt);
        if (index) {
            nextWpt->addScanAreaToSonarPlot(pMainWindow->customPlotLF,
                   pMainWindow->flag.targetIconType, 2, pMainWindow->ping->lastTracePos, pMainWindow->gnssController);
//            pMainWindow->tools->tracker.passedWaypoint(item);
            pMainWindow->target_convertToTarget(nextWpt);

            pMainWindow->tools->targetAudio->setVolume(50);
            pMainWindow->tools->targetAudio->play();
        }
    }

    // Update position of the white line. Update start position here
    // and end position in addBoundaryPoints
    portLine->start->setCoords(xCoord, yCoord);
    stbdLine->start->setCoords(xCoord, yCoord);
    portLine2->setVisible(false);
    stbdLine2->setVisible(false);

    if (lockToBoat)     plot_updateRange();

    xCenter = xCoord;
    yCenter = yCoord;

    if (functionCallCount == 0) {
        MaxXRange = xCoord;
        MinXRange = xCoord;
        MaxYRange = yCoord;
        MinYRange = yCoord;

        QSize gpsPlotSz = mapPlotPtr->axisRect()->size();

        int gpsHeight = gpsPlotSz.rheight();
        int gpsWidth = gpsPlotSz.rwidth();

        double updatedView1 = (gpsHeight * 1.0) / (gpsWidth * 1.0);

        qDebug()<<"updated view: "<<updatedView1;

        // Find the current distance (in degrees) of x/y of the gpsPlot
        double xGpsView2 = xCoord + 0.0025;
        qDebug()<<"x2: "<<xGpsView2;
        double xGpsView1 = xCoord - 0.0025;
        qDebug()<<"x1: "<<xGpsView1;
        double yGpsView2 = yCoord + 0.0025;
        qDebug()<<"y2: "<<yGpsView2;
        double yGpsView1 = yCoord - 0.0025;
        double distanceX = 0, distanceY = 0, bearing;

        // Find the distance (in meters) of a 0.0025 degree difference of x/y
        pMainWindow->Gnss_calcDistanceAndBearing(yGpsView2, xGpsView1, yGpsView2, xGpsView1+0.0025, &distanceX, &bearing);
        pMainWindow->Gnss_calcDistanceAndBearing(yGpsView2, xGpsView1, yGpsView2+0.0025, xGpsView1, &distanceY, &bearing);

        // Determine the correct upper y range to give a 1m:1m ratio for x:y
        yGpsView1 = yGpsView2 - updatedView1 * (xGpsView2 - xGpsView1)
                * distanceX/distanceY;

        qDebug()<<"y1: "<<yGpsView1;

        // set the new range values
        mapPlotPtr->xAxis->setRange(xGpsView1,xGpsView2);
        mapPlotPtr->yAxis->setRange(yGpsView1,yGpsView2);

        //mapPlotPtr->replot();
        target_resizeAllTargets();

    } else {
        if (xCoord > MaxXRange)         MaxXRange = xCoord;
        else if (xCoord < MinXRange)    MinXRange = xCoord;

        if (yCoord > MaxYRange)         MaxYRange = yCoord;
        else if (yCoord < MinYRange)    MinYRange = yCoord;
    }
    functionCallCount++;
}

void gpsMap :: path_addTrackpoints(double xCoord, double yCoord) {
    if (mapPlotPtr == nullptr)  return;

    if (abs(xCoord) > 200 || abs(yCoord) > 100) {
        return;
    }

    if (trackpoints->data()->size() == 0) {
        mapPlotPtr->xAxis->setTickLabels(true);
        mapPlotPtr->yAxis->setTickLabels(true);
        if (pMainWindow->customPlotLF->yAxis->grid()->visible()) {
            mapPlotPtr->xAxis->grid()->setVisible(true);
            mapPlotPtr->yAxis->grid()->setVisible(true);
        }
    }
    trackpoints->addData(xCoord, yCoord);
}

void gpsMap :: path_resetTrackpoints() {
    if (mapPlotPtr == nullptr || trackpoints == nullptr)  return;

    trackpoints->data()->clear();
}

void gpsMap :: target_addFlag (double Lattitude, double Longitude, double size, int index) {
    if (mapPlotPtr == nullptr)  return;

    if (abs(Lattitude) < 1 || abs(Longitude) < 1) {
        return;
    }
    if (Longitude > MaxXRange)         MaxXRange = Longitude;
    else if (Longitude < MinXRange)    MinXRange = Longitude;

    if (Lattitude > MaxYRange)         MaxYRange = Lattitude;
    else if (Lattitude < MinYRange)    MinYRange = Lattitude;

    double xInc = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/mapPlotPtr->width() * 40;
    double yInc = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/mapPlotPtr->height() * 40;

    QPixmap pixmap2 = targetItem::getIconPixmap(false, pMainWindow->flag.targetIconType).scaled(40,40);
    flagIcon = new QCPItemPixmap(mapPlotPtr);
    flagIcon->setScaled(true, Qt::IgnoreAspectRatio);
    flagIcon->setPixmap(pixmap2);
    flagIcon->setLayer("tools2");
    flagIcon->topLeft->setCoords(Longitude - xInc/2, Lattitude + yInc);
    flagIcon->bottomRight->setCoords(Longitude + xInc/2, Lattitude);
    QCPItemText *flagText = new QCPItemText(mapPlotPtr);
    flagText->setText(" "+ QString::number(index+1) +" ");
    flagText->setColor(Qt::yellow);
    flagText->setBrush(QBrush(Qt::black));
    flagText->setSelectedBrush(QBrush(Qt::black));
    flagText->setFont(QFont(font().family(),9));
    flagText->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
    flagText->setLayer("tools3");
    flagText->setSelectable(false);
    flagText->position->setCoords(Longitude, Lattitude + yInc);

    qDebug() << "flagLongitude" << QString::number(Lattitude, 'd', 8) << QString::number(Longitude, 'd', 8);
    mapPlotPtr->replot();

    pMainWindow->tools->targetList[index].icon_chart = flagIcon;
    pMainWindow->tools->targetList[index].text_chart = flagText;
    legend_updateAll();
}

void gpsMap :: target_addWaypoint (double Lattitude, double Longitude, double size, int index) {
    if (mapPlotPtr == nullptr)  return;

    if (abs(Lattitude) > 100 || abs(Longitude) > 200) {
        return;
    }

    double xInc = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/mapPlotPtr->width() * 40;
    double yInc = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/mapPlotPtr->height() * 40;

    QPixmap pixmap = targetItem::getIconPixmap(false, pMainWindow->flag.targetIconType);
    QPixmap pixmap2 = pixmap.scaled(40,40);
    flagIcon = new QCPItemPixmap(mapPlotPtr);
    flagIcon->setScaled(true, Qt::IgnoreAspectRatio);
    flagIcon->setPixmap(pixmap2);
    flagIcon->setLayer("tools2");
    flagIcon->topLeft->setCoords(Longitude - xInc/2, Lattitude + yInc);
    flagIcon->bottomRight->setCoords(Longitude + xInc/2, Lattitude);
    QCPItemText *flagText = new QCPItemText(mapPlotPtr);
    flagText->setText(" "+ QString::number(index+1) +" ");
    flagText->setColor(Qt::yellow);
    flagText->setBrush(QBrush(Qt::black));
    flagText->setSelectedBrush(QBrush(Qt::black));
    flagText->setFont(QFont(font().family(),9));
    flagText->setPositionAlignment(Qt::AlignHCenter|Qt::AlignBottom);
    flagText->setLayer("tools3");
    flagText->setSelectable(false);
    flagText->position->setCoords(Longitude, Lattitude + yInc);

    qDebug() << "flagLongitude" << QString::number(Lattitude, 'd', 8) << QString::number(Longitude, 'd', 8);
    mapPlotPtr->replot();

    pMainWindow->tools->waypointList[index].icon_chart = flagIcon;
    pMainWindow->tools->waypointList[index].text_chart = flagText;
    legend_updateAll();
}

void gpsMap :: target_resizeTarget (int index, bool Enlarged) {

    QPixmap pixmap = targetItem::getIconPixmap(false, pMainWindow->flag.targetIconType);
    QPixmap pixmap1 = targetItem::getIconPixmap(true, pMainWindow->flag.targetIconType);

    if (mapPlotPtr == nullptr)  return;

    double xInc = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/mapPlotPtr->width() * 40;
    double yInc = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/mapPlotPtr->height() * 40;

    double size;
    if (Enlarged) {
        xInc *= 1.5;
        yInc *= 1.5;
        if (pMainWindow->tools->targetList[index].icon_chart != nullptr){
            pMainWindow->tools->targetList[index].icon_chart->setPixmap(pixmap1);
        }
    }

    double xCoord = pMainWindow->tools->targetList[index].Longitude;
    double yCoord = pMainWindow->tools->targetList[index].Lattitude;

    if (pMainWindow->tools->targetList[index].icon_chart != nullptr) {
        pMainWindow->tools->targetList[index].icon_chart->setPixmap(pixmap);
        pMainWindow->tools->targetList[index].icon_chart->topLeft->setCoords(xCoord - xInc/2, yCoord + yInc);
        pMainWindow->tools->targetList[index].icon_chart->bottomRight->setCoords(xCoord + xInc/2, yCoord);
        if (pMainWindow->tools->targetList[index].text_chart != nullptr) {
            pMainWindow->tools->targetList[index].text_chart->position->setCoords(xCoord, yCoord + yInc);
        }
    }
    mapPlotPtr->replot();
}

void gpsMap :: target_resizeAllTargets () {
   //  QPixmap pixmap1(":/icons/icons/flags.png");
    mapPlotPtr->axisRect()->setAutoMargins(QCP::msAll);

    static int counter = 0;
    counter++;
//    if (counter == 2)       counter = 0;
//    else                    return;

    double width = (mapPlotPtr->xAxis->range().upper - mapPlotPtr->xAxis->range().lower)/(mapPlotPtr->width()*1.0);
    double height = (mapPlotPtr->yAxis->range().upper - mapPlotPtr->yAxis->range().lower)/(mapPlotPtr->height()*1.0);
//    double xCorner = mapPlotPtr->xAxis->range().lower;
//    double yCorner = mapPlotPtr->yAxis->range().lower;
//    compassIcon->topLeft->setCoords(xCorner, yCorner + 40*height);
//    compassIcon->bottomRight->setCoords(xCorner + width*40, yCorner);

    double xInc = width * 40.0;
    double yInc = height * 40.0;

    // Resize all flag items on gpsPlot
    for (int i = 0; i < pMainWindow->tools->targetList.length(); i++) {
        if (pMainWindow->tools->targetList[i].icon_chart == nullptr)   continue;
        double centerX = pMainWindow->tools->targetList[i].Longitude;
        double centerY = pMainWindow->tools->targetList[i].Lattitude;
        //pMainWindow->flag_items[i].pixmap_Chart->setPixmap(pixmap1);
        pMainWindow->tools->targetList[i].icon_chart->topLeft->setCoords(centerX-xInc/2, centerY+yInc);
        pMainWindow->tools->targetList[i].icon_chart->bottomRight->setCoords(centerX+xInc/2, centerY);
        if (pMainWindow->tools->targetList[i].text_chart != nullptr)
            pMainWindow->tools->targetList[i].text_chart->position->setCoords(centerX, centerY + yInc);
    }
    for (int i = 0; i < pMainWindow->tools->waypointList.length(); i++) {
        if (pMainWindow->tools->waypointList[i].icon_chart == nullptr)   continue;
        double centerX = pMainWindow->tools->waypointList[i].Longitude;
        double centerY = pMainWindow->tools->waypointList[i].Lattitude;
        //pMainWindow->flag_items[i].pixmap_Chart->setPixmap(pixmap1);
        pMainWindow->tools->waypointList[i].icon_chart->topLeft->setCoords(centerX-xInc/2, centerY+yInc);
        pMainWindow->tools->waypointList[i].icon_chart->bottomRight->setCoords(centerX+xInc/2, centerY);
        if (pMainWindow->tools->waypointList[i].text_chart != nullptr)
            pMainWindow->tools->waypointList[i].text_chart->position->setCoords(centerX, centerY + yInc);
    }

    // Resize boat icon
    xInc = width * 10.0;
    yInc = height * 10.0;
    double boatX = (boatIcon->topLeft->key() + boatIcon->bottomRight->key())/2;
    double boatY = (boatIcon->topLeft->value() + boatIcon->bottomRight->value())/2;
   // boatIcon->setLayer("colormap");
    boatIcon->topLeft->setCoords(boatX-xInc, boatY+yInc);
    boatIcon->bottomRight->setCoords(boatX+xInc, boatY-yInc);

    double scaleFactor = (abs(cos(boatHeading*M_PI/180)) + abs(sin(boatHeading*M_PI/180)))*1.5;
    boatIconPix->topLeft->setCoords(boatX-xInc*scaleFactor, boatY+yInc*scaleFactor);
    boatIconPix->bottomRight->setCoords(boatX+xInc*scaleFactor, boatY-yInc*scaleFactor);

    // Resize tracking point
    xInc = width * 5.0;
    yInc = height * 5.0;
    double xCen = (cursorDot->bottomRight->coords().x() + cursorDot->topLeft->coords().x()) / 2;
    double yCen = (cursorDot->bottomRight->coords().y() + cursorDot->topLeft->coords().y()) / 2;
    cursorDot->bottomRight->setCoords(xCen + xInc, yCen - yInc);
    cursorDot->topLeft->setCoords(xCen - xInc, yCen + yInc);

    leftColumn->position->setCoords(mapPlotPtr->width() - 140, 20);
    rightColumn->position->setCoords(mapPlotPtr->width() - 130, 20);
    pMainWindow->tools->tracker.updateBearingLineSize();

    //mapPlotPtr->replot(QCustomPlot::rpQueuedReplot);
}

void gpsMap :: target_removeItemFromPlot (int flagItem) {
    if (mapPlotPtr == nullptr)  return;
    if (pMainWindow->tools->targetList[flagItem].icon_chart == nullptr)  return;
//    if (pMainWindow->flag_items[flagItem].onplot_Chart == 0)  return;

//    if (pMainWindow->flag_items[flagItem].onplot_Chart) {
        mapPlotPtr->removeItem(pMainWindow->tools->targetList[flagItem].icon_chart);
        mapPlotPtr->removeItem(pMainWindow->tools->targetList[flagItem].text_chart);
        mapPlotPtr->replot();
//    }
    pMainWindow->tools->targetList[flagItem].icon_chart = nullptr;
    pMainWindow->tools->targetList[flagItem].text_chart = nullptr;
}

/************** S-57 File Plotting *******************/

QCPItemText* gpsMap :: addSoundingPoint (double xCoord, double yCoord, double zDepth) {
    if (mapPlotPtr == nullptr)  return nullptr;

//    if (MaxXRange - MinXRange > 0.1)         return;
//    else if (MaxYRange - MinYRange > 0.1)    return;
//    else if (xCoord < MinXRange || xCoord > MaxXRange
//              || yCoord < MinYRange || yCoord > MaxYRange)  return;

    QCPItemText *distanceText = new QCPItemText(mapPlotPtr);
    distanceText->setText(QString::number(zDepth,'f',1));
    distanceText->setColor(Qt::black);
    distanceText->setFont(QFont(font().family(),10));
    distanceText->position->setCoords(xCoord, yCoord);
    distanceText->setLayer("main");
    distanceText->setSelectedColor(Qt::black);
    distanceText->setVisible(false);
    return distanceText;
}

QCPItemText* gpsMap :: addPointObject (double xCoord, double yCoord, QString text) {
    if (mapPlotPtr == nullptr)  return nullptr;

//    if (MaxXRange - MinXRange > 0.1)         return;
//    else if (MaxYRange - MinYRange > 0.1)    return;
//    else if (xCoord < MinXRange || xCoord > MaxXRange
//              || yCoord < MinYRange || yCoord > MaxYRange)  return;

    QCPItemText *textItem = new QCPItemText(mapPlotPtr);
    textItem->setText(text);
    textItem->setColor(Qt::black);
    textItem->setFont(QFont(font().family(),10));
    textItem->position->setCoords(xCoord, yCoord);
    textItem->setLayer("main");
    textItem->setSelectedColor(Qt::black);
    textItem->setVisible(false);
    return textItem;
}

void gpsMap :: updateLoadingChartText (int state) {
    if (loadingChartText == nullptr || !mapPlotPtr->hasItem(loadingChartText)) {
        loadingChartText = new QCPItemText(mapPlotPtr);
        loadingChartText->setColor(Qt::white);
        loadingChartText->setFont(QFont(font().family(),30));
        loadingChartText->setLayer("fixedIcons");
        loadingChartText->setSelectedColor(Qt::white);
    }
    double xCoord = (mapPlotPtr->xAxis->range().upper + mapPlotPtr->xAxis->range().lower)/2;
    double yCoord = (mapPlotPtr->yAxis->range().upper + mapPlotPtr->yAxis->range().lower)/2;
    loadingChartText->position->setCoords(xCoord, yCoord);
    loadingChartText->setText("Loading Chart... " + QString::number(state) + " %");
    if (state > 100) {
        mapPlotPtr->removeItem(loadingChartText);
        loadingChartText = nullptr;
    }
    mapPlotPtr->replot();
}

void gpsMap :: addMapVector(double xCoord, double yCoord, int counter) {
//    if (abs(xCoord) < 1 || abs(yCoord) < 1) {
//        return;
//    }
//    else if (xCoord < -72 || xCoord > -70 || yCoord < 41 || yCoord > 43) {
//        return;
//    }

    bool PlotCurves = 1;
    static double lastXCoord = 0, lastYCoord = 0;

    if (PlotCurves) {
        if (counter == 0) {
            QPen redPen;
            redPen.setColor(QColor(250, 214, 214));
            redPen.setWidthF(5);

            vectorCurve = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
            vectorCurve->setPen(redPen);
            vectorCurve->setLayer("main");

            if (vectorCurve->dataCount() == 1) {
                MaxXRange = xCoord;
                MinXRange = xCoord;
                MaxYRange = yCoord;
                MinYRange = yCoord;
            } else {
                if (xCoord > MaxXRange)         MaxXRange = xCoord;
                else if (xCoord < MinXRange)    MinXRange = xCoord;

                if (yCoord > MaxYRange)         MaxYRange = yCoord;
                else if (yCoord < MinYRange)    MinYRange = yCoord;
            }
        }

        vectorCurve->addData(xCoord, yCoord);
        ui->GPS_Coords->setText(QString::number(xCoord, 'f', 5) + ", " + QString::number(yCoord, 'f', 5));
    }
    else {
        int xCell, yCell;
        colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        colorMap->data()->setAlpha(xCell, yCell, 255);
        colorMap->data()->setCell(xCell, yCell, 255);

        if (counter == 0) {

        }
        else {
            // Plot from previous point to current point
            int steps = abs((yCoord-lastYCoord)/yRes) + abs((xCoord-lastXCoord)/xRes);
            double xCoordInt, yCoordInt;
            for (int i = 1; i < steps; i++) {
                xCoordInt = lastXCoord + (xCoord - lastXCoord)/steps*i;
                yCoordInt = lastYCoord + (yCoord - lastYCoord)/steps*i;

                colorMap->data()->coordToCell(xCoordInt, yCoordInt, &xCell, &yCell);
                colorMap->data()->setAlpha(xCell, yCell, 255);
                colorMap->data()->setCell(xCell, yCell, 255);
            }
        }
        lastXCoord = xCoord;
        lastYCoord = yCoord;
    }

    mapPlotPtr->replot();
}

void gpsMap :: clearmap() {
    colorMap->data()->fillAlpha(0);
}

void gpsMap :: plotPoint (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer) {
    int xCell = 0, yCell = 0;
    static int lastXCell = 0, lastYCell = 0;
    bool onBorder = 0;

    if (xCoord < MinXRange) {
        xCoord = MinXRange;
        onBorder = 1;
    }
    if (xCoord > MaxXRange) {
        xCoord = MaxXRange;
        onBorder = 1;
    }
    if (yCoord < MinYRange) {
        yCoord = MinYRange;
        onBorder = 1;
    }
    if (yCoord > MaxYRange) {
        yCoord = MaxYRange;
        onBorder = 1;
    }

    if (onBorder == 1) {
        color = 255;
    }

    mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);

    if (xCell >= 700)    xCell = 699;
    if (xCell < 0)    xCell = 0;
    if (yCell >= 700)    yCell = 699;
    if (yCell < 0)    yCell = 0;

    mapPointer->data()->setAlpha(xCell, yCell, 255);
    mapPointer->data()->setCell(xCell, yCell, color);

    if (thickness > 0 && onBorder != 1) {
        int xDiff = xCell - lastXCell;
        int yDiff = yCell - lastYCell;
        int x0 = 0, y0 = 0;

        if (xDiff > 0) {
            y0 = -1;
        }
        else if (xDiff < 0) {
            y0 = 1;
        }
        else {
            y0 = 0;
        }
        if (yDiff > 0) {
            x0 = 1;
        }
        else if (yDiff < 0) {
            x0 = -1;
        }
        else {
            x0 = 0;
        }
        mapPointer->data()->setAlpha(xCell, yCell + y0, 255);
        mapPointer->data()->setCell(xCell, yCell + y0, color);
        mapPointer->data()->setAlpha(xCell + x0, yCell, 255);
        mapPointer->data()->setCell(xCell + x0, yCell, color);
        mapPointer->data()->setAlpha(xCell + x0, yCell + y0, 255);
        mapPointer->data()->setCell(xCell + x0, yCell + y0, color);
    }

    lastXCell = xCell;
    lastYCell = yCell;

//    int NumPoints = thickness*2 + 1;
//    int NumIterations = NumPoints*NumPoints;
//    if (thickness > 1) {
//        int xOffset = 0;//;points[8] = {-1, -1, -1, 0, 1, 1, 1, 0};
//        int yOffset = 0;//ypoints[8] = {-1, 0, 1, 1, 1, 0, -1, -1};
//        for (int i = 0; i < NumIterations; i++) {
//            xOffset = (i % NumPoints) - thickness;
//            yOffset = (i / NumPoints) - thickness;
//            mapPointer->data()->setAlpha(xCell+xOffset, yCell+yOffset, 255);
//            mapPointer->data()->setCell(xCell+xOffset, yCell+yOffset, color);
//        }
//    }
}

void gpsMap :: plotPoint2 (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer, double angle) {
    int xCell = 0, yCell = 0;
    static int lastXCell = 0, lastYCell = 0;
    bool onBorder = 0;

    if (xCoord < MinXRange) {
        xCoord = MinXRange;
        onBorder = 1;
    }
    if (xCoord > MaxXRange) {
        xCoord = MaxXRange;
        onBorder = 1;
    }
    if (yCoord < MinYRange) {
        yCoord = MinYRange;
        onBorder = 1;
    }
    if (yCoord > MaxYRange) {
        yCoord = MaxYRange;
        onBorder = 1;
    }

    if (onBorder == 1) {
        color = 255;
    }

    mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);

    if (xCell >= 700)    xCell = 699;
    if (xCell < 0)    xCell = 0;
    if (yCell >= 700)    yCell = 699;
    if (yCell < 0)    yCell = 0;

    if (mapPointer->data()->alpha(xCell, yCell) < 100) {
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);
    }

    lastXCell = xCell;
    lastYCell = yCell;

    if (thickness > 1 && onBorder != 1) {
        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

    }
}

void gpsMap :: plotPoint3 (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer, double angle) {
    int xCell = 0, yCell = 0;
    static int lastXCell = 0, lastYCell = 0;
    bool onBorder = 0;

    if (xCoord < MinXRange) {
        xCoord = MinXRange;
        onBorder = 1;
    }
    if (xCoord > MaxXRange) {
        xCoord = MaxXRange;
        onBorder = 1;
    }
    if (yCoord < MinYRange) {
        yCoord = MinYRange;
        onBorder = 1;
    }
    if (yCoord > MaxYRange) {
        yCoord = MaxYRange;
        onBorder = 1;
    }

    if (onBorder == 1) {
        color = 255;
    }

    mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);

    if (xCell >= 700)    xCell = 699;
    if (xCell < 0)    xCell = 0;
    if (yCell >= 700)    yCell = 699;
    if (yCell < 0)    yCell = 0;

    if (mapPointer->data()->alpha(xCell, yCell) < 100) {
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);
    }

    lastXCell = xCell;
    lastYCell = yCell;

    if (thickness > 1 && onBorder != 1) {
        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        if (mapPointer->data()->cell(xCell, yCell) > 250) return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        if (mapPointer->data()->cell(xCell, yCell) > 250) return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        if (mapPointer->data()->cell(xCell, yCell) > 250) return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

    }
}

void gpsMap :: plotPoint4 (double xCoord, double yCoord, double color, int thickness, QCPColorMap *mapPointer, double angle) {
    int xCell = 0, yCell = 0;
    static int lastXCell = 0, lastYCell = 0;
    bool onBorder = 0;

    if (xCoord < MinXRange) {
        xCoord = MinXRange;
        onBorder = 1;
    }
    if (xCoord > MaxXRange) {
        xCoord = MaxXRange;
        onBorder = 1;
    }
    if (yCoord < MinYRange) {
        yCoord = MinYRange;
        onBorder = 1;
    }
    if (yCoord > MaxYRange) {
        yCoord = MaxYRange;
        onBorder = 1;
    }

    if (onBorder == 1) {
        color = 255;
    }

    mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);

    if (xCell >= 700)    xCell = 699;
    if (xCell < 0)    xCell = 0;
    if (yCell >= 700)    yCell = 699;
    if (yCell < 0)    yCell = 0;

    if (mapPointer->data()->alpha(xCell, yCell) < 100) {
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);
    }

    lastXCell = xCell;
    lastYCell = yCell;

    if (thickness > 1 && onBorder != 1) {
        mapPointer->data()->coordToCell(xCoord-2*xRes*sin(angle), yCoord-2*yRes*cos(angle), &xCell, &yCell);
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color+10);

        mapPointer->data()->coordToCell(xCoord-xRes*sin(angle), yCoord-yRes*cos(angle), &xCell, &yCell);
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color+10);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        if (mapPointer->data()->cell(xCell, yCell) > 250) return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        if (mapPointer->data()->cell(xCell, yCell) > 250) return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

        xCoord += xRes*sin(angle);
        yCoord += yRes*cos(angle);
        mapPointer->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
        if (xCell >= 700 || xCell < 0 || yCell >= 700 || yCell < 0)    return;
        if (mapPointer->data()->cell(xCell, yCell) > 250) return;
        mapPointer->data()->setAlpha(xCell, yCell, 255);
        mapPointer->data()->setCell(xCell, yCell, color);

    }
}

void gpsMap :: addMapVector2(double xCoord, double yCoord, int counter, int lineType) {
//    qDebug() << xCoord << yCoord;
    static int counterLast = 0;
    static bool CompletedLast = 0;

//    if (xCoord < MinXRange
//             || xCoord > MaxXRange
//             || yCoord < MinYRange
//             || yCoord > MaxYRange) {
//        CompletedLast = 0;
//        return;
//    }

    QCPColorMap *mapPointer = colorMap;
    static double lastXCoord = xCoord, lastYCoord = yCoord;
    CompletedLast = 1;

    int xCell, yCell;
    double color = 255;
    int thickness = 0;
    // 50 - Land, 100 - Building Outline, 150 - Sea,  200 - Water Outline
    // 255 -
    if (lineType == 0) {
        return;
        color = 255;
        mapPointer = colorMap;
    }
    else if (lineType == 1) { // Map Border
        color = 200;
        mapPointer = colorMap;
        thickness = 3;
    }
    else if (lineType == 2) {
        color = 100;
        mapPointer = colorMap;
    }
    else if (lineType == 3) {
        color = 200;
        mapPointer = colorMap;
        thickness = 1;
    }
    else if (lineType == 4) {
        color = 50;
        mapPointer = landMap;
        thickness = 3;
    }
    else if (lineType == 5) {
        return;
        color = 150;
        mapPointer = landMap;
        thickness = 1;
    }
    else if (lineType == 6) {
        color = 150;
        mapPointer = landMap;
        thickness = 3;
    }
    else if (lineType == 7) {
        color = 10;
        mapPointer = landMap;
        thickness = 0;
    }
    else if (lineType == 8) {
        color = 10;
        mapPointer = landMap;
        thickness = 0;
    }
    else if (lineType == 9) {
        color = 10;
        mapPointer = landMap;
        thickness = 0;
    }
    else if (lineType == 10) {
        color = 10;
        mapPointer = landMap;
        thickness = 0;
    }
    double xDiff = xCoord - lastXCoord;
    double yDiff = yCoord - lastYCoord;
    double length = sqrt(xDiff*xDiff + yDiff*yDiff);

    double angleX = asin(xDiff/length);
    double angleY = acos(yDiff/length);
    double angle = angleY;
    if (xDiff < 0)  angle = -angleY;
    plotPoint2(xCoord, yCoord, color, thickness, mapPointer, angle + M_PI/2);

    if (counter == 0) {

    }
    else {
        // Plot from previous point to current point
        int steps = 2 * abs((yCoord-lastYCoord)/yRes) + 2 * abs((xCoord-lastXCoord)/xRes);
        double xCoordInt, yCoordInt;
        for (int i = 0; i < steps; i++) {
            xCoordInt = lastXCoord + (xCoord - lastXCoord) / steps*i;
            yCoordInt = lastYCoord + (yCoord - lastYCoord) / steps*i;

            plotPoint2(xCoordInt, yCoordInt, color, thickness, mapPointer, angle + M_PI/2);
        }
    }
    lastXCoord = xCoord;
    lastYCoord = yCoord;
}

void gpsMap :: addMapVector3 (QList <double> xCoord, QList <double> yCoord, int lineType) {
    static int counterLast = 0;
    static bool CompletedLast = 0;

//    if (xCoord < MinXRange
//             || xCoord > MaxXRange
//             || yCoord < MinYRange
//             || yCoord > MaxYRange) {
//        CompletedLast = 0;
//        return;
//    }

    QCPColorMap *mapPointer = colorMap;
    double lastXCoord = 0, lastYCoord = 0;
    CompletedLast = 1;

    int xCell, yCell;
    double color = 255;
    int thickness = 0;
    if (lineType == 0) {
        color = 255;
        mapPointer = colorMap;
    }
    else if (lineType == 1) {
        color = 200;
        mapPointer = colorMap;
    }
    else if (lineType == 2) {
        color = 100;
        mapPointer = colorMap;
    }
    else if (lineType == 3) {
        color = 200;
        mapPointer = colorMap;
    }
    else if (lineType == 4) {
        color = 50;
        mapPointer = landMap;
        thickness = 1;
    }
    else if (lineType == 5) {
        color = 150;
        mapPointer = landMap;
        thickness = 1;
    }
    else if (lineType == 6) {
        color = 10;
        mapPointer = landMap;
        thickness = 0;
    }
    for (int j = 0; j < xCoord.length(); j++) {
        double xDiff = xCoord[j] - lastXCoord;
        double yDiff = yCoord[j] - lastYCoord;
        double length = sqrt(xDiff*xDiff + yDiff*yDiff);

        double angleX = asin(xDiff/length);
        double angleY = acos(yDiff/length);
        double angle = angleY;
        if (xDiff < 0)  angle = -angleY;

        if (j == 0) {
            plotPoint2(xCoord[j], yCoord[j], color, 0, mapPointer, angle + M_PI/2);
        }
        else {
            plotPoint2(xCoord[j], yCoord[j], color, thickness, mapPointer, angle + M_PI/2);

            // Plot from previous point to current point
            int steps = 2 * abs((yCoord[j]-lastYCoord)/yRes) + 2 * abs((xCoord[j]-lastXCoord)/xRes);
            double xCoordInt, yCoordInt;
            for (int i = 0; i < steps; i++) {
                xCoordInt = lastXCoord + (xCoord[j] - lastXCoord) / steps*i;
                yCoordInt = lastYCoord + (yCoord[j] - lastYCoord) / steps*i;

                plotPoint2(xCoordInt, yCoordInt, color, thickness, mapPointer, angle + M_PI/2);
            }
        }
        lastXCoord = xCoord[j];
        lastYCoord = yCoord[j];
    }
}

void gpsMap :: addMapVector4(double xCoord, double yCoord, int counter, int color, int thickness, int layer) {
    QCPColorMap *mapPointer = colorMap;
    static double lastXCoord = xCoord, lastYCoord = yCoord;

    int xCell, yCell;
    if (layer == 0) {
        mapPointer = landMap;
    }
    else if (layer == 1) {
        mapPointer = colorMap;
    }
    double xDiff = xCoord - lastXCoord;
    double yDiff = yCoord - lastYCoord;
    double length = sqrt(xDiff*xDiff + yDiff*yDiff);

    double angleX = asin(xDiff/length);
    double angleY = acos(yDiff/length);
    double angle = angleY;
    if (xDiff < 0)  angle = -angleY;
    plotPoint3(xCoord, yCoord, color, 0, mapPointer, angle + M_PI/2);

    if (counter == 0) {

    }
    else {
        // Plot from previous point to current point
        int steps = 2 * abs((yCoord-lastYCoord)/yRes) + 2 * abs((xCoord-lastXCoord)/xRes);
        double xCoordInt, yCoordInt;
        for (int i = 0; i < steps; i++) {
            xCoordInt = lastXCoord + (xCoord - lastXCoord) / steps*i;
            yCoordInt = lastYCoord + (yCoord - lastYCoord) / steps*i;

            plotPoint3(xCoordInt, yCoordInt, color, thickness, mapPointer, angle + M_PI/2);
        }
    }
    lastXCoord = xCoord;
    lastYCoord = yCoord;
}

void gpsMap :: addMapVector5(double xCoord, double yCoord, int counter, int color, int thickness, int layer) {
    QCPColorMap *mapPointer = colorMap;
    static double lastXCoord = xCoord, lastYCoord = yCoord;

    int xCell, yCell;
    if (layer == 0) {
        mapPointer = landMap;
    }
    else if (layer == 1) {
        mapPointer = colorMap;
    }
    double xDiff = xCoord - lastXCoord;
    double yDiff = yCoord - lastYCoord;
    double length = sqrt(xDiff*xDiff + yDiff*yDiff);

    double angleX = asin(xDiff/length);
    double angleY = acos(yDiff/length);
    double angle = angleY;
    if (xDiff < 0)  angle = -angleY;
    plotPoint3(xCoord, yCoord, color, 0, mapPointer, angle + M_PI/2);

    if (counter == 0) {

    }
    else {
        // Plot from previous point to current point
        int steps = 2 * abs((yCoord-lastYCoord)/yRes) + 2 * abs((xCoord-lastXCoord)/xRes);
        double xCoordInt, yCoordInt;
        for (int i = 0; i < steps; i++) {
            xCoordInt = lastXCoord + (xCoord - lastXCoord) / steps*i;
            yCoordInt = lastYCoord + (yCoord - lastYCoord) / steps*i;

            plotPoint4(xCoordInt, yCoordInt, color, thickness, mapPointer, angle + M_PI/2);
        }
    }
    lastXCoord = xCoord;
    lastYCoord = yCoord;
}

double gpsMap :: findFillColor (double xCoord, double yCoord) {
    int xCell, yCell;
    int topColor, bottomColor, rightColor, leftColor;

    colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
    if (colorMap->data()->alpha(xCell, yCell) > 100)    return 0;

    colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
    while (1) {
        if (colorMap->data()->alpha(xCell, yCell) < 100) {
            xCell++;
        } else {
            rightColor = colorMap->data()->cell(xCell, yCell);
            break;
        }
    }

    colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
    while (1) {
        if (colorMap->data()->alpha(xCell, yCell) < 100) {
            xCell--;
        } else {
            leftColor = colorMap->data()->cell(xCell, yCell);
            break;
        }
    }

    colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
    while (1) {
        if (colorMap->data()->alpha(xCell, yCell) < 100) {
            yCell++;
        } else {
            topColor = colorMap->data()->cell(xCell, yCell);
            break;
        }
    }

    colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);
    while (1) {
        if (colorMap->data()->alpha(xCell, yCell) < 100) {
            yCell--;
        } else {
            bottomColor = colorMap->data()->cell(xCell, yCell);
            break;
        }
    }

    if ((bottomColor & topColor & rightColor & leftColor) == bottomColor) {
        return bottomColor - 20;
    }
    else {
        return 0;
    }
}

double gpsMap :: findFillColor2 (int xCoord, int yCoord) {
    int xCell, yCell;
    int topColor = 0, bottomColor = 0, rightColor = 0, leftColor = 0;
    bool topBorder = 0, bottomBorder = 0, rightBorder = 0, leftBorder = 0;
    QVector <int> colors;

    xCell = xCoord;
    yCell = yCoord;
//    if (landMap->data()->alpha(xCell, yCell) > 100)    return 0;

    for (int i = 0; i < 3; i++) {
        if (landMap->data()->alpha(xCell, yCell) > 100)    break;

        xCoord += i;
        yCoord += i;

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (xCell > 700 - 4) {
                if (landMap->data()->alpha(xCell, yCell) > 100) {
                    if (landMap->data()->cell(xCell, yCell) < 20)   break;
                    rightBorder = 1;
                    break;
                }
                else if (xCell >= 699) {
                    return 0;
                }
                xCell++;
            }
            else if (landMap->data()->alpha(xCell, yCell) < 100) {
                xCell++;
            }
            else {
                rightColor = landMap->data()->cell(xCell, yCell);
                break;
            }
        }

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (xCell < 5) {
                if (landMap->data()->alpha(xCell, yCell) > 100) {
                    if (landMap->data()->cell(xCell, yCell) < 20)   break;
                    leftBorder = 1;
                    break;
                }
                else if (xCell < 0) {
                    return 0;
                }
                xCell--;
            }
            else if (landMap->data()->alpha(xCell, yCell) < 100) {
                xCell--;
            }
            else {
                leftColor = landMap->data()->cell(xCell, yCell);
                break;
            }
        }

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (yCell > 700 - 5) {
                if (landMap->data()->alpha(xCell, yCell) > 100) {
                    if (landMap->data()->cell(xCell, yCell) < 20)   break;
                    topBorder = 1;
                    break;
                }
                else if (yCell >= 699) {
                    return 0;
                }
                yCell++;
            }
            else if (landMap->data()->alpha(xCell, yCell) < 100) {
                yCell++;
            }
            else {
                topColor = landMap->data()->cell(xCell, yCell);
                break;
            }
        }

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (yCell < 5) {
                if (landMap->data()->alpha(xCell, yCell) > 100) {
                    if (landMap->data()->cell(xCell, yCell) < 20)   break;
                    bottomBorder = 1;
                    break;
                }
                else if (yCell < 0) {
                    return 0;
                }
                yCell--;
            }
            else if (landMap->data()->alpha(xCell, yCell) < 100) {
                yCell--;
            }
            else {
                bottomColor = landMap->data()->cell(xCell, yCell);
                break;
            }
        }

        int maxColor = 0;
    //    if (bottomColor == 255)

        if (bottomColor > maxColor)     maxColor = bottomColor;
        if (topColor > maxColor)     maxColor = topColor;
        if (leftColor > maxColor)     maxColor = leftColor;
        if (rightColor > maxColor)     maxColor = rightColor;

        if (bottomBorder == 1)       bottomColor = maxColor;
        if (topBorder == 1)       topColor = maxColor;
        if (leftBorder == 1)       leftColor = maxColor;
        if (rightBorder == 1)       rightColor = maxColor;

        if ((bottomColor & topColor & rightColor & leftColor) == bottomColor) {
//            return bottomColor;
            colors.append(bottomColor);
        }
        else {
            return 0;
        }
    }
    int color = 0;;
    if (colors.length() == 0)   return 0;
    for (int j = 0; j < colors.length(); j++) {
        if (j == 0)     color = colors[0];
        else {
            if (colors[j] != color)     return 0;
        }
    }
    return color;
}

double gpsMap :: findFillColorTopMap (int xCoord, int yCoord, double criteria) {
    int xCell, yCell;
    int topColor = 0, bottomColor = 0, rightColor = 0, leftColor = 0;
    bool topBorder = 0, bottomBorder = 0, rightBorder = 0, leftBorder = 0;
    QVector <int> colors;

    xCell = xCoord;
    yCell = yCoord;
//    if (landMap->data()->alpha(xCell, yCell) > 100)    return 0;
    double landMapPixelColor = landMap->data()->cell(xCell, yCell);
    if (landMapPixelColor > (criteria + 3) || landMapPixelColor < (criteria - 3))    return 0;

    for (int i = 0; i < 3; i++) {
        if (colorMap->data()->alpha(xCell, yCell) > 100)    break;

        xCoord += i;
        yCoord += i;

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (xCell > 700 - 4) {
                if (colorMap->data()->alpha(xCell, yCell) > 100) {
                    if (colorMap->data()->cell(xCell, yCell) < 20)   break;
                    rightBorder = 1;
                    break;
                }
                else if (xCell >= 699) {
                    return 0;
                }
                xCell++;
            }
            else if (colorMap->data()->alpha(xCell, yCell) < 100) {
                xCell++;
            }
            else {
                rightColor = colorMap->data()->cell(xCell, yCell);
                break;
            }
        }

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (xCell < 5) {
                if (colorMap->data()->alpha(xCell, yCell) > 100) {
                    if (colorMap->data()->cell(xCell, yCell) < 20)   break;
                    leftBorder = 1;
                    break;
                }
                else if (xCell < 0) {
                    return 0;
                }
                xCell--;
            }
            else if (colorMap->data()->alpha(xCell, yCell) < 100) {
                xCell--;
            }
            else {
                leftColor = colorMap->data()->cell(xCell, yCell);
                break;
            }
        }

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (yCell > 700 - 5) {
                if (colorMap->data()->alpha(xCell, yCell) > 100) {
                    if (colorMap->data()->cell(xCell, yCell) < 20)   break;
                    topBorder = 1;
                    break;
                }
                else if (yCell >= 699) {
                    return 0;
                }
                yCell++;
            }
            else if (colorMap->data()->alpha(xCell, yCell) < 100) {
                yCell++;
            }
            else {
                topColor = colorMap->data()->cell(xCell, yCell);
                break;
            }
        }

        xCell = xCoord;
        yCell = yCoord;
        while (1) {
            if (yCell < 5) {
                if (colorMap->data()->alpha(xCell, yCell) > 100) {
                    if (colorMap->data()->cell(xCell, yCell) < 20)   break;
                    bottomBorder = 1;
                    break;
                }
                else if (yCell < 0) {
                    return 0;
                }
                yCell--;
            }
            else if (colorMap->data()->alpha(xCell, yCell) < 100) {
                yCell--;
            }
            else {
                bottomColor = colorMap->data()->cell(xCell, yCell);
                break;
            }
        }

        int maxColor = 0;
    //    if (bottomColor == 255)

        if (bottomColor > maxColor)     maxColor = bottomColor;
        if (topColor > maxColor)     maxColor = topColor;
        if (leftColor > maxColor)     maxColor = leftColor;
        if (rightColor > maxColor)     maxColor = rightColor;

        if (bottomBorder == 1)       bottomColor = maxColor;
        if (topBorder == 1)       topColor = maxColor;
        if (leftBorder == 1)       leftColor = maxColor;
        if (rightBorder == 1)       rightColor = maxColor;

        if ((bottomColor & topColor & rightColor & leftColor) == bottomColor) {
//            return bottomColor;
            colors.append(bottomColor);
        }
        else {
            return 0;
        }
    }
    int color = 0;
    if (colors.length() == 0)   return 0;
    for (int j = 0; j < colors.length(); j++) {
        if (j == 0)     color = colors[0];
        else {
            if (colors[j] != color)     return 0;
        }
    }
    return color;
}

void gpsMap :: fillPaint (double xCoord, double yCoord, double color) {
    int xCell, yCell;

    colorMap->data()->coordToCell(xCoord, yCoord, &xCell, &yCell);

    QList <int> xList, yList;
    xList.append(xCell);    yList.append(yCell);
    int n_x, n_y;

    while (xList.length() > 0) {
        n_x = xList.at(0);
        n_y = yList.at(0);
        xList.removeAt(0);
        yList.removeAt(0);

        if (colorMap->data()->alpha(n_x, n_y) < 100) {
            colorMap->data()->setAlpha(n_x, n_y, 255);
            colorMap->data()->setCell(n_x, n_y, color);

            xList.append(n_x + 1);
            yList.append(n_y);

            xList.append(n_x);
            yList.append(n_y + 1);

            xList.append(n_x - 1);
            yList.append(n_y);

            xList.append(n_x);
            yList.append(n_y - 1);
        }
    }
}

void gpsMap :: fillPaint2 (int xCoord, int yCoord, double color) {
    int xCell, yCell;
    xCell = xCoord;
    yCell = yCoord;

    QList <int> xList, yList;
    xList.append(xCell);    yList.append(yCell);
    int n_x, n_y;

    while (xList.length() > 0) {
        n_x = xList.at(0);
        n_y = yList.at(0);
        xList.removeAt(0);
        yList.removeAt(0);

        if (n_x > 700 || n_x < 0 || n_y < 0 || n_y > 700)    return;
        if (landMap->data()->cell(n_x, n_y) > 250)    return;

        if (landMap->data()->alpha(n_x, n_y) < 100) {
            landMap->data()->setAlpha(n_x, n_y, 255);
            landMap->data()->setCell(n_x, n_y, color);

            xList.append(n_x + 1);
            yList.append(n_y);

            xList.append(n_x);
            yList.append(n_y + 1);

            xList.append(n_x - 1);
            yList.append(n_y);

            xList.append(n_x);
            yList.append(n_y - 1);
        }
    }
}

void gpsMap :: fillPaintTopMap (int xCoord, int yCoord, double color, double criteria) {
    int xCell, yCell;
    xCell = xCoord;
    yCell = yCoord;

    QList <int> xList, yList;
    xList.append(xCell);    yList.append(yCell);
    int n_x, n_y;

    while (xList.length() > 0) {
        n_x = xList.at(0);
        n_y = yList.at(0);
        xList.removeAt(0);
        yList.removeAt(0);

        if (n_x > 700 || n_x < 0 || n_y < 0 || n_y > 700)    return;
        double landMapPixelColor = landMap->data()->cell(n_x, n_y);
        if (landMapPixelColor > (criteria + 3) || landMapPixelColor < (criteria - 3))    return;

        if (colorMap->data()->alpha(n_x, n_y) < 100) {
            colorMap->data()->setAlpha(n_x, n_y, 255);
            colorMap->data()->setCell(n_x, n_y, color);

            xList.append(n_x + 1);
            yList.append(n_y);

            xList.append(n_x);
            yList.append(n_y + 1);

            xList.append(n_x - 1);
            yList.append(n_y);

            xList.append(n_x);
            yList.append(n_y - 1);
        }
    }
}

void gpsMap :: determineAreaFill () {
    /*
    // Check cell positions of 3 consecutive nodes
    for (int i = 0; i < pMainWindow->LandArea.length(); i++) {
        if (pMainWindow->LandArea[i].x.length() < 5)   continue;
        for (int j = 1; j < pMainWindow->LandArea[i].x.length()-1; j++) {
            int xCell0, xCell1, xCell2;
            int yCell0, yCell1, yCell2;
            colorMap->data()->coordToCell(pMainWindow->LandArea[i].x[j-1],
                    pMainWindow->LandArea[i].y[j-1], &xCell0, &yCell0);
            colorMap->data()->coordToCell(pMainWindow->LandArea[i].x[j],
                    pMainWindow->LandArea[i].y[j], &xCell1, &yCell1);
            colorMap->data()->coordToCell(pMainWindow->LandArea[i].x[j+1],
                    pMainWindow->LandArea[i].y[j+1], &xCell2, &yCell2);
            if (xCell1 < 3 || xCell1 > 700 - 3)   continue;
            if (yCell1 < 3 || yCell1 > 700 - 3)   continue;

            int xDiff = xCell2 - xCell0;
            int yDiff = yCell2 - yCell0;
            double length = sqrt((abs(xDiff*xDiff) + abs(yDiff*yDiff))^2);
            if (length < 6)     continue;
//            if (abs(cos(xDiff/length) + cos((xCell2-xCell1)/length))
            int count = 0;
            for (int k = -1; k <= 1; k++) {
                for (int l = -1; l <= 1; l++) {
                    if (colorMap->data()->alpha(xCell1 + k, yCell1 + l) > 100)
                        count++;
                }
            }
            if (count > 3)  continue;

            int x0 = 0, y0 = 0;
            if (xCell2 > xCell1 && xCell1 > xCell0) {
                if (yCell2 > yCell1 && yCell1 > yCell0) {
                    x0 = 1;
                    y0 = -1;
                }
                else if (yCell2 < yCell1 && yCell1 < yCell0) {
                    x0 = -1;
                    y0 = -1;
                }
                else    continue;
            }
            else  {
                if (xCell2 < xCell1 && xCell1 < xCell0) {
                    if (yCell2 > yCell1 && yCell1 > yCell0) {
                        x0 = -1;
                        y0 = 1;
                    }
                    else if (yCell2 < yCell1 && yCell1 < yCell0) {
                        x0 = 1;
                        y0 = 1;
                    }
                }
                else    continue;
            }

            fillPaint2(xCell1 + x0, yCell1 + y0, 50);
//            return;
            mapPlotPtr->replot();

        }
    }

    // return if:
    // - the cells are not in a fairly straight line
    // - the cells are within 10 cells
    // - more than two cells are connected to the center node
    // Find the direction towards the inside of the loop
    // Paint one cell towards the inside, from the node
*/
    // Fill colorMap with blue color to show water
    for (int i = 3; i < 700 - 3; i++) {
        int count = 0;
        int LastPos = 0;
        for (int j = 3; j < 700 - 5; j++) {
            if (colorMap->data()->alpha(i, j) < 100) {
                colorMap->data()->setAlpha(i, j, 255);
                colorMap->data()->setCell(i, j, 150);
            }
        }
    }
}

void gpsMap :: paintMap() {
    bool foundIt = 0;
    double xBoundMin = colorMap->data()->keyRange().lower;
    double xBoundMax = colorMap->data()->keyRange().upper;

    double yBoundMin = colorMap->data()->valueRange().lower;
    double yBoundMax = colorMap->data()->valueRange().upper;
    int xCell, yCell;
    int fillColor;


    // Paint chart frame first

//    int fillColor = findFillColor(-71.1677, 41.705);
//    if (fillColor > 0)  fillPaint(-71.1677, 41.705, fillColor);

//    fillColor = findFillColor(-71.1668, 41.7063);
//    if (fillColor > 0)  fillPaint(-71.1668, 41.7063, fillColor);
//    mapPlotPtr->replot();

    for (int x = 5; x < 700 - 5; x++) {
        for (int y = 5; y < 700 - 5; y++) {
            fillColor = findFillColor2(x, y);
            if (fillColor > 0)  {
                fillPaint2(x, y, fillColor);
//                mapPlotPtr->replot();
            }
        }
    }
//    mapPlotPtr->replot();

//    colorMap->data()->setAlpha(xCell, yCell, 255);
//    colorMap->data()->setCell(xCell, yCell, 200);
}

void gpsMap :: paintTopMap(double criteria) {
    bool foundIt = 0;
    double xBoundMin = colorMap->data()->keyRange().lower;
    double xBoundMax = colorMap->data()->keyRange().upper;

    double yBoundMin = colorMap->data()->valueRange().lower;
    double yBoundMax = colorMap->data()->valueRange().upper;
    int xCell, yCell;
    int fillColor;


    // Paint chart frame first

//    int fillColor = findFillColor(-71.1677, 41.705);
//    if (fillColor > 0)  fillPaint(-71.1677, 41.705, fillColor);

//    fillColor = findFillColor(-71.1668, 41.7063);
//    if (fillColor > 0)  fillPaint(-71.1668, 41.7063, fillColor);
//    mapPlotPtr->replot();

    for (int x = 5; x < 700 - 5; x++) {
        for (int y = 5; y < 700 - 5; y++) {
            fillColor = findFillColorTopMap(x, y, criteria);
            if (fillColor > 0)  {
                fillPaintTopMap(x, y, fillColor, criteria);
            }
        }
    }
//    mapPlotPtr->replot();

//    colorMap->data()->setAlpha(xCell, yCell, 255);
//    colorMap->data()->setCell(xCell, yCell, 200);
}

void gpsMap :: copyAreaToLandMap(double criteriaTopMap, double criteriaLandMap) {
    for (int i = 0; i < 700; i++) {
        for (int j = 0; j < 700; j++) {
            double pixelColor = colorMap->data()->cell(i,j);
            if (abs(pixelColor - criteriaTopMap) > 3) continue;
            else if (abs(landMap->data()->cell(i,j) - criteriaLandMap) > 3) continue;
            else    landMap->data()->setCell(i,j,pixelColor);
        }
    }
    colorMap->data()->fill(0);
    colorMap->data()->fillAlpha(0);
}

QCPColorMap* gpsMap :: getColorMapPointer() {
    return colorMap;
}

void gpsMap :: setPlotColorMap(double x1, double x2, double y1, double y2) {
    if (mapPlotPtr == nullptr)  return;

    if (zoomOn) {
        return;
    }

    double xDiff = 0;//(x2-x1)/2;
    double yDiff = 0;//(y2-y1)/2;
    x1 -= xDiff;
    x2 += xDiff;
    y1 -= yDiff;
    y2 += yDiff;

    xCellMax = 700;//(x2-x1)*1000;//500;
    yCellMax = 700;//(y2-y1)*1000;//400;

    xRes = (x2-x1)/xCellMax;
    yRes = (y2-y1)/yCellMax;

    landMap = new QCPColorMap(mapPlotPtr->xAxis, mapPlotPtr->yAxis);//assign colormap to customplot
    landMap->data()->setSize(xCellMax, yCellMax); // we want the color map to have nx * ny data points
    landMap->data()->setRange(QCPRange(x1, x2), QCPRange(y1, y2));//sets physical size of colormap
    landMap->setInterpolate(false);//to get visible pixel pattern
    landMap->data()->fillAlpha(0); // make the color map transparent

    colorMap = new QCPColorMap(mapPlotPtr->xAxis, mapPlotPtr->yAxis);//assign colormap to customplot
    colorMap->data()->setSize(xCellMax, yCellMax); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(x1, x2), QCPRange(y1, y2));//sets physical size of colormap
    colorMap->setInterpolate(false);//to get visible pixel pattern
    colorMap->data()->fillAlpha(0); // make the color map transparent
    // add a color scale:
    QCPColorGradient color = QCPColorGradient::gpThermal;

    QCPColorGradient depthCustomColor;
    //setup depth plot colorscale gradient
    depthCustomColor.clearColorStops();
    //    depthCustomColor.setColorStopAt(0.2,QColor(111,255,40));
        //    depthCustomColor.setColorStopAt(0.2,QColor(51,204,255));
        //    depthCustomColor.setColorStopAt(0.2,QColor(150,220,170));

   // For ENC
    depthCustomColor.setColorStopAt(0,QColor(0,0,0));
    depthCustomColor.setColorStopAt(0.2,QColor(251,247,77));
    depthCustomColor.setColorStopAt(0.4,QColor(167,146,76));
    depthCustomColor.setColorStopAt(0.6,QColor(130,190,237));
    depthCustomColor.setColorStopAt(0.8,QColor(204,204,0));
    depthCustomColor.setColorStopAt(1.0,QColor(255,255,255));


    depthCustomColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);

    colorScale = new QCPColorScale(mapPlotPtr);
    colorScale->setType(QCPAxis::atRight);
    colorScale->setDataRange(QCPRange(0,255));
    colorScale->setGradient(depthCustomColor);
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    landMap->setColorScale(colorScale); // associate the color map with the color scale

    mapPlotPtr->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect

    mapPlotPtr->xAxis->setRange(x1, x2);
    mapPlotPtr->yAxis->setRange(y1, y2);
   //mapPlotPtr->replot();

    MaxXRange = x2;
    MinXRange = x1;

    MaxYRange = y2;
    MinYRange = y1;

//    QPixmap pixmap(":/icons/icons/boat.png");
//    QPixmap pixmap2 = pixmap.scaled(40,40);
//    boatIcon->setScaled(true, Qt::IgnoreAspectRatio);
//    boatIcon->setPixmap(pixmap2);
//    boatIcon->setLayer("colormap");
}

void gpsMap :: setPlotColorMap(double x1, double x2, double y1, double y2,int height, int width) {
    if (mapPlotPtr == nullptr)  return;

    if (zoomOn) {
        return;
    }

    double xDiff = 0;//(x2-x1)/2;
    double yDiff = 0;//(y2-y1)/2;
    x1 -= xDiff;
    x2 += xDiff;
    y1 -= yDiff;
    y2 += yDiff;

    double widthPixel = (width / 20) * (x2-x1);
    double heightPixel = (height/20) * (y2-y1);

    if(widthPixel > 700 && heightPixel > 700){
        xCellMax = 700;
        yCellMax = 700;
    }else{
        xCellMax = widthPixel;
        yCellMax = heightPixel;
    }

    xRes = (x2-x1)/xCellMax;
    yRes = (y2-y1)/yCellMax;


//    landMap = new QCPColorMap(mapPlotPtr->xAxis, mapPlotPtr->yAxis);//assign colormap to customplot
//    landMap->data()->setSize(xCellMax, yCellMax); // we want the color map to have nx * ny data points
//    landMap->data()->setRange(QCPRange(x1, x2), QCPRange(y1, y2));//sets physical size of colormap
//    landMap->setInterpolate(false);//to get visible pixel pattern
//    landMap->data()->fillAlpha(0); // make the color map transparent

    colorMap = new QCPColorMap(mapPlotPtr->xAxis, mapPlotPtr->yAxis);//assign colormap to customplot
    colorMap->data()->setSize(xCellMax, yCellMax); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(x1, x2), QCPRange(y1, y2));//sets physical size of colormap
    colorMap->setInterpolate(false);//to get visible pixel pattern
    colorMap->data()->fillAlpha(0); // make the color map transparent
    // add a color scale:
    QCPColorGradient color = QCPColorGradient::gpThermal;

    QCPColorGradient depthCustomColor;
    //setup depth plot colorscale gradient
    depthCustomColor.clearColorStops();
    //    depthCustomColor.setColorStopAt(0.2,QColor(111,255,40));
        //    depthCustomColor.setColorStopAt(0.2,QColor(51,204,255));
        //    depthCustomColor.setColorStopAt(0.2,QColor(150,220,170));

   // For ENC
    depthCustomColor.setColorStopAt(0,QColor(0,0,0));
    depthCustomColor.setColorStopAt(0.2,QColor(251,247,77));
    depthCustomColor.setColorStopAt(0.4,QColor(167,146,76));
    depthCustomColor.setColorStopAt(0.6,QColor(130,190,237));
    depthCustomColor.setColorStopAt(0.8,QColor(204,204,0));
    depthCustomColor.setColorStopAt(1.0,QColor(255,255,255));


    depthCustomColor.setColorInterpolation(QCPColorGradient::ColorInterpolation::ciRGB);

    colorScale = new QCPColorScale(mapPlotPtr);
    colorScale->setType(QCPAxis::atRight);
    colorScale->setDataRange(QCPRange(0,255));
    colorScale->setGradient(depthCustomColor);
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    //landMap->setColorScale(colorScale); // associate the color map with the color scale

    mapPlotPtr->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect

    mapPlotPtr->xAxis->setRange(x1, x2);
    mapPlotPtr->yAxis->setRange(y1, y2);
    //mapPlotPtr->replot();

    MaxXRange = x2;
    MinXRange = x1;

    MaxYRange = y2;
    MinYRange = y1;

//    QPixmap pixmap(":/icons/icons/boat.png");
//    QPixmap pixmap2 = pixmap.scaled(40,40);
//    boatIcon->setScaled(true, Qt::IgnoreAspectRatio);
//    boatIcon->setPixmap(pixmap2);
//    boatIcon->setLayer("colormap");
}

void gpsMap :: setPlotRange(double x1, double x2, double y1, double y2) {
    if (zoomOn) {
        return;
    }
    mapPlotPtr->xAxis->setRange(x1, x2);
    mapPlotPtr->yAxis->setRange(y1, y2);
   // mapPlotPtr->replot();
}

/*************** Try plotting using QCPCurves ********************/

QCPCurve* gpsMap :: addCurves(QList <double> x, QList <double> y, int colorType) {
    QCPCurve *curve = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);

    QPen pen;
    QBrush brush;
    QColor color = QColor(255, 255, 255);
    if (colorType == 4) {
        color = QColor(251,247,77);
    } else if (colorType == 5) {
        color = QColor(167,146,76);
    } else if (colorType == 6) {
        color = QColor(170,220,248);
    } else if (colorType == 7) {
        color = QColor(130,190,237);
    } else if (colorType == 8) {
        color = QColor(130,190,237);
    } else if (colorType == 9) {
        color = QColor(100,140,190);
    }
    pen.setColor(QColor(0, 0, 0));
    pen.setWidthF(1);
    brush.setColor(color);
    brush.setStyle(Qt::SolidPattern);
    if (colorType < 4 || colorType == 7)      brush.setStyle(Qt::NoBrush);

    curve->setPen(pen);
    curve->setBrush(brush);

    for (int i = 0; i < x.length(); i++) {
        curve->addData(x[i], y[i]);
    }
    curve->setLayer("background");
    return curve;
}

void gpsMap :: soundingPointUpdate () {
    // Hide the sounding points when the user is zoomed out by a reasonable amount
    double xPlot1 = mapPlotPtr->xAxis->range().lower, xPlot2 = mapPlotPtr->xAxis->range().upper;
    double yPlot1 = mapPlotPtr->yAxis->range().lower, yPlot2 = mapPlotPtr->yAxis->range().upper;
    double width = mapPlotPtr->width(), height = mapPlotPtr->height();

    // Points should be roughly at least 50 pixels apart.
    double pixelRatio = width / (xPlot2 - xPlot1);
    if (pixelRatio < 6000)  {
        for (int i = 0; i < pMainWindow->SG3D_List.length(); i++) {
            for (int j = 0; j < pMainWindow->SG3D_List[i].textItem.length(); j++) {
                pMainWindow->SG3D_List[i].textItem[j]->setVisible(false);
            }
        }
    }
    else {
        double xPlotDiff = xPlot2 - xPlot1, yPlotDiff = yPlot2 - yPlot1;
        xPlot1 -= 0.5*xPlotDiff;
        yPlot1 -= 0.5*yPlotDiff;
        xPlot2 += 0.5*xPlotDiff;
        yPlot2 += 0.5*yPlotDiff;

        for (int i = 0; i < pMainWindow->SG3D_List.length(); i++) {
            for (int j = 0; j < pMainWindow->SG3D_List[i].textItem.length(); j++) {
                if (pMainWindow->SG3D_List[i].X[j] > xPlot2 || pMainWindow->SG3D_List[i].X[j] < xPlot1 ||
                        pMainWindow->SG3D_List[i].Y[j] > yPlot2 || pMainWindow->SG3D_List[i].Y[j] < yPlot1)
                    pMainWindow->SG3D_List[i].textItem[j]->setVisible(false);
                else
                    pMainWindow->SG3D_List[i].textItem[j]->setVisible(true);
            }
        }
    }
}

void gpsMap :: objectsUpdate () {
    // Hide the sounding points when the user is zoomed out by a reasonable amount
    double xPlot1 = mapPlotPtr->xAxis->range().lower, xPlot2 = mapPlotPtr->xAxis->range().upper;
    double yPlot1 = mapPlotPtr->yAxis->range().lower, yPlot2 = mapPlotPtr->yAxis->range().upper;
    double width = mapPlotPtr->width(), height = mapPlotPtr->height();

    // Points should be roughly at least 50 pixels apart.
    double pixelRatio = width / (xPlot2 - xPlot1);
    if (pixelRatio < 3000)  {
        for (int i = 0; i < pMainWindow->SinglePointObjects_List.length(); i++) {
            if (pMainWindow->SinglePointObjects_List[i].text != nullptr)
                pMainWindow->SinglePointObjects_List[i].text->setVisible(false);
        }
    }
    else {
        double xPlotDiff = xPlot2 - xPlot1, yPlotDiff = yPlot2 - yPlot1;
        xPlot1 -= 0.5*xPlotDiff;
        yPlot1 -= 0.5*yPlotDiff;
        xPlot2 += 0.5*xPlotDiff;
        yPlot2 += 0.5*yPlotDiff;

        for (int i = 0; i < pMainWindow->SinglePointObjects_List.length(); i++) {
            if (pMainWindow->SinglePointObjects_List[i].xCoord > xPlot2 || pMainWindow->SinglePointObjects_List[i].xCoord < xPlot1 ||
                    pMainWindow->SinglePointObjects_List[i].yCoord > yPlot2 || pMainWindow->SinglePointObjects_List[i].yCoord < yPlot1) {
                if (pMainWindow->SinglePointObjects_List[i].text != nullptr)
                    pMainWindow->SinglePointObjects_List[i].text->setVisible(false);
            }
            else {
                if (pMainWindow->SinglePointObjects_List[i].text != nullptr)
                    pMainWindow->SinglePointObjects_List[i].text->setVisible(true);
            }
        }
    }
}

/*************** Other **********************/

void gpsMap :: clearEncChart () {
    mapPlotPtr->clearPlottables();
    mapPlotPtr->clearItems();
//    mapPlotPtr->legend->clearItems();
//    mapPlotPtr->legend = new QCPLegend;
//    mapPlotPtr->legend->setVisible(true);
//    setupPlotItems(mapPlotPtr);
    QPen bluePen;
    bluePen.setColor(QColor(3, 37, 126));
    bluePen.setWidthF(5);

    QPen blackPen;
    blackPen.setColor(QColor(0,0,0));
    blackPen.setWidth(5);

    QPen redPen;
    redPen.setColor(QColor(225, 6, 0));
    redPen.setWidthF(5);

    QPen greenPen;
    greenPen.setColor(QColor(8, 255, 8));
    greenPen.setWidthF(5);

    QPen whitePen;
    whitePen.setColor(QColor(255, 255, 255));
    whitePen.setWidthF(3);

    QPen greyPen;
    greyPen.setColor(QColor(128, 128, 128));
    greyPen.setWidthF(3);

    QPen whitePen2;
    whitePen2.setColor(QColor(255, 255, 255));
    whitePen2.setWidthF(10);
    whitePen2.setBrush(QBrush(Qt::white));

    QBrush whiteBrush;
    whiteBrush.setColor(QColor(255, 255, 255));
    whiteBrush.setStyle(Qt::SolidPattern);

    QBrush grayBrush;
    grayBrush.setColor(QColor(50,50,50));
    grayBrush.setStyle(Qt::SolidPattern);

    portLine = new QCPItemLine(mapPlotPtr);
    portLine->setPen(whitePen);
    portLine->setLayer("trace");
    stbdLine = new QCPItemLine(mapPlotPtr);
    stbdLine->setPen(whitePen);
    stbdLine->setLayer("trace");

    portLine2 = new QCPItemLine(mapPlotPtr);
    portLine2->setPen(whitePen);
    portLine2->setLayer("trace");
    stbdLine2 = new QCPItemLine(mapPlotPtr);
    stbdLine2->setPen(whitePen);
    stbdLine2->setLayer("trace");

    newCurve = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    newCurve->setPen(bluePen);
    newCurve->setLayer("tools1");

    newCurve2 = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    newCurve2->setPen(redPen);
    newCurve2->setLayer("tools1");

    newCurve3 = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    newCurve3->setPen(greenPen);
    newCurve3->setLayer("tools1");

    vectorCurve = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    vectorCurve->setPen(redPen);
    vectorCurve->setLayer("main");

    trackpoints = new QCPCurve(mapPlotPtr->xAxis, mapPlotPtr->yAxis);
    trackpoints->setPen(greyPen);
    trackpoints->setLayer("tools1");

    cursorDot = new QCPItemEllipse(mapPlotPtr);
    cursorDot->setBrush(QBrush(Qt::white));
    cursorDot->setLayer("tools2");
    cursorDot->setVisible(false);

    boatIcon = new QCPItemEllipse(mapPlotPtr);
    boatIcon->setParent(mapPlotPtr);
    boatIcon->setSelectable(false);
    boatIcon->setPen(whitePen);
    boatIcon->setBrush(whiteBrush);
    boatIcon->setLayer("boatLayer");

    QPixmap pixmapArrow(":/icons/icons/rocket.png");
    QPixmap pixmap2 = pixmapArrow.scaled(40,40);//.transformed(rm);
    boatIconPix = new QCPItemPixmap(mapPlotPtr);
    boatIconPix->setPixmap(pixmap2);
    boatIconPix->setScaled(true);
    boatIconPix->setLayer("boatLayer");
    boatIconPix->topLeft->setCoords(-100.1,-100.1);
    boatIconPix->bottomRight->setCoords(-100.1,-100.1);

    compassIcon = new QCPItemPixmap(mapPlotPtr);
    QPixmap pixmap (":/icons/icons/Compass.png");
    compassIcon->setPixmap(pixmap);
    compassIcon->topLeft->setTypeX(QCPItemPosition::ptAbsolute);
    compassIcon->topLeft->setTypeY(QCPItemPosition::ptAbsolute);
    compassIcon->bottomRight->setTypeX(QCPItemPosition::ptAbsolute);
    compassIcon->bottomRight->setTypeY(QCPItemPosition::ptAbsolute);
    compassIcon->setScaled(true);
    compassIcon->setLayer("fixedIcons");

    compassIcon->topLeft->setCoords(30,30);
    compassIcon->bottomRight->setCoords(100,100);

//    for (int i = mapPlotPtr->itemCount() - 1; i >= 0; i--) {
//        if (mapPlotPtr->item(i) == boatIcon)    continue;
//        else if (mapPlotPtr->item(i) == portLine)    continue;
//        else if (mapPlotPtr->item(i) == stbdLine)    continue;
//        else        mapPlotPtr->removeItem(i);
//    }
//    for (int i = mapPlotPtr->plottableCount() - 1; i >= 0; i--) {
//        if (mapPlotPtr->plottable(i) == newCurve)    continue;
//        else if (mapPlotPtr->plottable(i) == newCurve2)    continue;
//        else if (mapPlotPtr->plottable(i) == newCurve3)    continue;
//        else if (mapPlotPtr->plottable(i) == vectorCurve)    continue;
//        else        mapPlotPtr->removePlottable(mapPlotPtr->plottable(i));
//    }
}
