/*graphdialog.cpp, Header comment written August 9th 2019
 * This object acts as the scope for the board
 * This is only used for development and testing, we plan to remove it on release
 */
#include "graphdialog.h"
#include "ui_graphdialog.h"
#include "mainwindow.h"

graphDialog::graphDialog(QWidget *parent) :
    QDialog(parent),
    graph_ui(new Ui::graphDialog)
{
    graph_ui->setupUi(this);
    this->setWindowFlags(Qt::Window);
     setupPlots();
     graph_ui->fullRange->clicked();

    this->setWindowFlags(this->windowFlags() &~Qt::WindowContextHelpButtonHint|Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint|Qt::MSWindowsFixedSizeDialogHint);
    QColor white;
    white.setRgb(255,255,255);
    graph_ui->label->setStyleSheet("color: white");
    graph_ui->label_2->setStyleSheet("color: white");
    graph_ui->label_3->setStyleSheet("color: white");
    graph_ui->label_4->setStyleSheet("color: white");
    graph_ui->fullRange->setStyleSheet("color: white");
    graph_ui->customRange->setStyleSheet("color: white");
    graph_ui->to_label->setStyleSheet("color: white");
    graph_ui->portSide_graph3->hide();
}

graphDialog::~graphDialog()
{
    delete graph_ui;
}

/**
 * @brief graphDialog::setupPlots
 * To Initialise the graphs of all 3 channels.
 */
void graphDialog :: setupPlots() {
    //axis configuration of portside graph
    graph_ui->portSide_graph->yAxis->setRange(0,1000);
    graph_ui->portSide_graph->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    graph_ui->portSide_graph->xAxis->setRange(0, 1000);
    graph_ui->portSide_graph->addGraph(graph_ui->portSide_graph->xAxis, graph_ui->portSide_graph->yAxis);
    graph_ui->portSide_graph->graph()->setPen(QPen(Qt::blue));
    graph_ui->portSide_graph->graph()->setName("Port Side");

    //axis configuration of starboard side graph
    graph_ui->starBoardSide_graph->yAxis->setRange(0, 1000);
    graph_ui->starBoardSide_graph->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    graph_ui->starBoardSide_graph->xAxis->setRange(0, 1000);
    graph_ui->starBoardSide_graph->addGraph(graph_ui->starBoardSide_graph->xAxis, graph_ui->starBoardSide_graph->yAxis);
    graph_ui->starBoardSide_graph->graph()->setPen(QPen(Qt::red));

    graph_ui->portSide_graph2->yAxis->setRange(0,1000);
    graph_ui->portSide_graph2->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    graph_ui->portSide_graph2->xAxis->setRange(0, 1000);
    graph_ui->portSide_graph2->addGraph(graph_ui->portSide_graph2->xAxis, graph_ui->portSide_graph2->yAxis);
    graph_ui->portSide_graph2->graph()->setPen(QPen(Qt::blue));
    graph_ui->portSide_graph2->graph()->setName("Port Side");

    graph_ui->portSide_graph2->yAxis->setRange(0,1000);
    graph_ui->portSide_graph2->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    graph_ui->portSide_graph2->xAxis->setRange(0, 1000);
    graph_ui->portSide_graph2->addGraph(graph_ui->portSide_graph2->xAxis, graph_ui->portSide_graph2->yAxis);
    graph_ui->portSide_graph2->graph()->setPen(QPen(Qt::red));
    graph_ui->portSide_graph2->graph()->setName("Port Side");

    graph_ui->portSide_graph3->yAxis->setRange(0,1000);
    graph_ui->portSide_graph3->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    graph_ui->portSide_graph3->xAxis->setRange(0, 1000);
    graph_ui->portSide_graph3->addGraph(graph_ui->portSide_graph3->xAxis, graph_ui->portSide_graph3->yAxis);
    graph_ui->portSide_graph3->graph()->setPen(QPen(Qt::blue));
    graph_ui->portSide_graph3->graph()->setName("Port Side");

    //axis configuration of starboard side graph
    graph_ui->starBoardSide_graph2->yAxis->setRange(0, 1000);
    graph_ui->starBoardSide_graph2->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    graph_ui->starBoardSide_graph2->xAxis->setRange(0, 1000);
    graph_ui->starBoardSide_graph2->addGraph(graph_ui->starBoardSide_graph2->xAxis, graph_ui->starBoardSide_graph2->yAxis);
    graph_ui->starBoardSide_graph2->graph()->setPen(QPen(Qt::red));
    //axis configuration of the crosspoint graph
    setActivation(false);
}

/**
 * @brief graphDialog::addDataToPortSideGraph
 * @param sampleNum
 * @param color
 * To Plot the port side  data to the graph. This function is called everytime
 * a trace is plotted.
 */
void graphDialog :: addDataToPortSideGraph1(int sampleNum, int color) {
    double xCoord = sampleNum;
    double yCoord = color;
    if(customRange == 1 && xCoord > lowerRange && xCoord < upperRange) {
        graph_ui->portSide_graph->graph()->addData(xCoord,yCoord);
        //emit XYfromGraphP(xCoord*(-1),yCoord);
    }
    else if (customRange == 0) {
        graph_ui->portSide_graph->graph()->addData(xCoord,yCoord);
        //emit XYfromGraphP(xCoord*(-1),yCoord);
    }
}

/**
 * @brief graphDialog::addDataToPortSideGraph
 * @param sampleNum
 * @param color
 * To Plot the port side  data to the graph. This function is called everytime
 * a trace is plotted.
 */
void graphDialog :: addDataToPortSideGraph2(int sampleNum, int color) {
    double xCoord = sampleNum;
    double yCoord = color;
    if(customRange == 1 && xCoord > lowerRange && xCoord < upperRange) {
        graph_ui->portSide_graph2->graph(0)->addData(xCoord,yCoord);
    }
    else if (customRange == 0) {
        graph_ui->portSide_graph2->graph(0)->addData(xCoord,yCoord);
    }
//    graph_ui->portSide_graph2->replot();
}

void graphDialog :: addDataToPortSideGraph3(int sampleNum, int color, int graphInt) {
    double xCoord = sampleNum;
    double yCoord = color;
    if(customRange == 1 && xCoord > lowerRange && xCoord < upperRange) {
        graph_ui->portSide_graph2->graph(graphInt)->addData(xCoord,yCoord);
    }
    else if (customRange == 0) {
        graph_ui->portSide_graph2->graph(graphInt)->addData(xCoord,yCoord);
    }
//    graph_ui->portSide_graph2->replot();
}

void graphDialog :: addDataToPortSideGraph4(int sampleNum, int color) {
    double xCoord = sampleNum;
    double yCoord = color;
    if(customRange == 1 && xCoord > lowerRange && xCoord < upperRange) {
        graph_ui->portSide_graph3->graph(0)->addData(xCoord,yCoord);
    }
    else if (customRange == 0) {
        graph_ui->portSide_graph3->graph(0)->addData(xCoord,yCoord);
    }
    graph_ui->portSide_graph3->show();
//    graph_ui->portSide_graph2->replot();
}

/**
 * @brief graphDialog::addDataToStarBoardSideGraph
 * @param sampleNum
 * @param color
 * To Plot the starboard side  data to the graph. This function is called
 * everytime a trace is plotted.
 */
void graphDialog ::addDataToStarBoardSideGraph1(int sampleNum, int color) {
    double xCoord = sampleNum;
    double yCoord = color;
    if(customRange == 1 && xCoord > lowerRange && xCoord < upperRange) {
        graph_ui->starBoardSide_graph->graph()->addData(xCoord,yCoord);
        //emit XYfromGraph(xCoord,yCoord);
    }
    else if (customRange == 0) {
        graph_ui->starBoardSide_graph->graph()->addData(xCoord,yCoord);
        //emit XYfromGraph(xCoord,yCoord);
    }
}

/**
 * @brief graphDialog::addDataToStarBoardSideGraph
 * @param sampleNum
 * @param color
 * To Plot the starboard side  data to the graph. This function is called
 * everytime a trace is plotted.
 */
void graphDialog ::addDataToStarBoardSideGraph2(int sampleNum, int color) {
    double xCoord = sampleNum;
    double yCoord = color;
    if(customRange == 1 && xCoord > lowerRange && xCoord < upperRange) {
        graph_ui->starBoardSide_graph2->graph()->addData(xCoord,yCoord);
    }
    else if (customRange == 0) {
        graph_ui->starBoardSide_graph2->graph()->addData(xCoord,yCoord);
    }
}

/**
 * @brief graphDialog::addDataToCrossChannelGraph
 * @param sample
 * @param value
 * To Plot the crosspoint(third channel) data to the graph. This function is called
 * everytime a trace is plotted.
 */
void graphDialog :: addDataToCrossChannelGraph(int sample, int value) {
//    double x = sample;
//    double y = value;
//    if(customRange == 1 && x > lowerRange && x<upperRange) {
//        graph_ui->crossPoint_graph->graph()->addData(x,y);
//    }
//    else if (customRange == 0) {
//        graph_ui->crossPoint_graph->graph()->addData(x,y);
//    }
}

/**
 * @brief graphDialog::removeData
 * Clears the data plotted on all 3 graphs.
 */
void graphDialog :: removeData1() {
    graph_ui->portSide_graph->graph()->data()->clear();
    graph_ui->starBoardSide_graph->graph()->data()->clear();
}

/**
 * @brief graphDialog::removeData
 * Clears the data plotted on all 3 graphs.
 */
void graphDialog :: removeData2() {
    graph_ui->portSide_graph2->graph(0)->data()->clear();
    graph_ui->portSide_graph2->graph(1)->data()->clear();
    graph_ui->portSide_graph3->graph(0)->data()->clear();
    graph_ui->starBoardSide_graph2->graph()->data()->clear();
}

/**
 * @brief graphDialog::refresh
 * Rescales all the graphs according to the data.
 */
void graphDialog :: refreshPlot1() {

    emit getView();
//    graph_ui->crossPoint_graph->xAxis->setRange(lowerRange,upperRange);
//    graph_ui->crossPoint_graph->xAxis->setRange(lowerRange,upperRange);
    if(customRange) {
        graph_ui->portSide_graph->graph()->rescaleValueAxis();
        graph_ui->portSide_graph->xAxis->setRange(lowerRange,upperRange);
        graph_ui->starBoardSide_graph->graph()->rescaleValueAxis();
        graph_ui->starBoardSide_graph->xAxis->setRange(lowerRange,upperRange);
        emit LFRangesToMain(lowerRange,upperRange);
    }
    else{
        graph_ui->portSide_graph->rescaleAxes();
        graph_ui->starBoardSide_graph->rescaleAxes();
        emit rescaleToMain();
    }
    graph_ui->portSide_graph->replot();
    graph_ui->starBoardSide_graph->replot();
    emit replotToMain();
}

/**
 * @brief graphDialog::refresh
 * Rescales all the graphs according to the data.
 */
void graphDialog :: refreshPlot2() {

    emit getView();
//    graph_ui->crossPoint_graph->xAxis->setRange(lowerRange,upperRange);
//    graph_ui->crossPoint_graph->xAxis->setRange(lowerRange,upperRange);
    if(customRange) {
        graph_ui->portSide_graph2->graph()->rescaleValueAxis();
        graph_ui->portSide_graph2->xAxis->setRange(lowerRange,upperRange);
        graph_ui->starBoardSide_graph2->graph()->rescaleValueAxis();
        graph_ui->starBoardSide_graph2->xAxis->setRange(lowerRange,upperRange);
    }
    else{
        graph_ui->portSide_graph2->rescaleAxes();
        graph_ui->starBoardSide_graph2->rescaleAxes();
    }
    graph_ui->portSide_graph2->replot();
    graph_ui->starBoardSide_graph2->replot();
    graph_ui->portSide_graph3->replot();
}

void graphDialog::on_fullRange_clicked()
{
    customRange = 0;
    setActivation(false);
}

void graphDialog::on_customRange_clicked()
{
    customRange =1;
    setActivation(true);

}

void graphDialog :: setActivation(bool active) {
    graph_ui->setRange_btn->setEnabled(active);
    graph_ui->lowerRange->setEnabled(active);
    graph_ui->upperRange->setEnabled(active);
}

void graphDialog::on_setRange_btn_clicked()
{
    QString lower = graph_ui->lowerRange->text();
    QString upper = graph_ui->upperRange->text();
    lowerRange = lower.toDouble();
    upperRange = upper.toDouble();
}

/**
 * @brief graphDialog::closeEvent
 * @param event
 * Called when window closes
 * Sets graphDialogActive to false in main window
 * This is done so the scope button can open the window again without having to be pressed twice
 */
void graphDialog :: closeEvent(QCloseEvent *) {
    emit graph_windowClosed(false);
}
