#-------------------------------------------------
#
# Project created by QtCreator 2018-02-08T12:13:10
#
#-------------------------------------------------

#QT       += core gui
QT       += core gui sql serialport
QT += network
QT += concurrent
QT += help
QT += multimedia
#QT += core gui webkit


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Jaimi
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#The following define prevents some warnings from appearing in release mode
#See Le's change log and see the November 11th, 2019 entry
#DEFINES += QT_NO_WARNING_OUTPUT QT_NO_WARNING

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    chartdialog.cpp \
    converterdialog.cpp \
    createFlag.cpp \
    croppingtool.cpp \
    depthalarmwidget.cpp \
    format.cpp \
    gpsmap.cpp \
    helpbrowser.cpp \
    helpdialog.cpp \
        main.cpp \
    graphdialog.cpp \
    modules.cpp \
    navigationsettings.cpp \
    tvgwindow.cpp \
    usermenu.cpp \
    systemsettings.cpp \
    mainwindow.cpp \
    kfilter.cpp \
    devwindow.cpp \
    qcustomplot.cpp

HEADERS += \
    chartdialog.h \
    converterdialog.h \
    createFlag.h \
    croppingtool.h \
    depthalarmwidget.h \
    format.h \
    gpsmap.h \
    helpbrowser.h \
    helpdialog.h \
    libusb/libusb.h \
        mainwindow.h \
    graphdialog.h \
    modules.h \
    navigationsettings.h \
    tvgwindow.h \
    usermenu.h \
    systemsettings.h \
    kfilter.h \
    kalman/kvector_impl.hpp \
    kalman/kvector.hpp \
    kalman/ktypes.hpp \
    kalman/kmatrix_impl.hpp \
    kalman/kmatrix.hpp \
    kalman/ekfilter_impl.hpp \
    kalman/ekfilter.hpp \
    devwindow.h \
    qcustomplot.h

FORMS += \
    chartdialog.ui \
    converterdialog.ui \
    createFlag.ui \
    croppingtool.ui \
    depthalarmwidget.ui \
    format.ui \
    gpsmap.ui \
    helpdialog.ui \
        mainwindow.ui \
    graphdialog.ui \
    navigationsettings.ui \
    tvgwindow.ui \
    usermenu.ui \
    systemsettings.ui \
    devwindow.ui

RESOURCES += \
    icons.qrc

RC_ICONS = icon.ico

req_files.path = $$OUT_PWD
req_files.files = required_files/*
INSTALLS += \
    req_files

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/libusb/ -llibusb-1.0
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/libusb/ -llibusb-1.0
else:unix: LIBS += -L$$PWD/libusb/ -llibusb-1.0
CONFIG += qt warn_on rtti exceptions

INCLUDEPATH += $$PWD/libusb
DEPENDPATH += $$PWD/libusb

DISTFILES += \
    icons/pin2.png
